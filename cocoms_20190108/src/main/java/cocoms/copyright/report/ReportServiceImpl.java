/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.report;

import java.util.*;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.initech.cpv.crl.fetcher.impl.FileTransporter;

import cocoms.copyright.application.PermKindMapper;
import cocoms.copyright.application.RpMgmMapper;
import cocoms.copyright.application.WritingKindMapper;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.*;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Service("reportService")
public class ReportServiceImpl extends EgovAbstractServiceImpl implements ReportService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name="reportInfoMapper")
	private ReportInfoMapper reportInfoMapper;

	@Resource(name="reportStatMapper")
	private ReportStatMapper reportStatMapper;

	@Resource(name="trustInfoMapper")
	private TrustInfoMapper trustInfoMapper;

	@Resource(name="trustStatMapper")
	private TrustStatMapper trustStatMapper;

	@Resource(name="memberMapper")
	private MemberMapper memberDAO;

	@Resource(name="writingKindMapper")
	private WritingKindMapper writingKindDAO;

	@Resource(name="permKindMapper")
	private PermKindMapper permKindDAO;

	@Override
	public Map<String, Object> getSubReports(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);
		
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4))-1;
		filter.put("year1", String.valueOf(year));
		filter.put("year2", String.valueOf(year-1));
		filter.put("year3", String.valueOf(year-2));
		filter.put("year4", String.valueOf(year-3));
		filter.put("year5", String.valueOf(year-4));
		
		List<ReportVO> list = (List<ReportVO>) reportInfoMapper.selectList(filter); 
		int total = reportInfoMapper.selectTotalCount(filter);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);
		
		return map;
	}

	@Override
	public Map<String, Object> getSubReports(String year, String type) throws Exception {
		LOGGER.info("year [{}]", year);
		
		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("firstRecordIndex", 0);
		filter.put("lastRecordIndex", 9999);
		filter.put("year1", year);
		filter.put("year2", year);	// not used
		filter.put("year3", year);  // not used
		filter.put("year4", year);  // not used
		filter.put("year5", year);  // not used

		List<ReportVO> list = (List<ReportVO>) reportInfoMapper.selectList(filter); 
		int total = reportInfoMapper.selectTotalCount(filter);

		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		for (ReportVO item : list) {
			
			if ("1".equals(type)) {
				if (!"1".equals(item.getYear1())) {
					continue;
				}
			}
			else {
				if ("1".equals(item.getYear1())) {
					continue;
				}
			}
			
			MemberVO member = memberDAO.selectByPk(item.getMemberSeqNo());
			
			Map<String, Object> map = CommonUtils.pojoToMap(item);
			map.putAll(CommonUtils.pojoToMap(member));
			list2.add(map);
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list2);
		map.put("total", total);
		
		return map;
	}
	
	@Override
	public Map<String, Object> getTrustReports(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);
		
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4))-1;
		filter.put("year1", String.valueOf(year));
		filter.put("year2", String.valueOf(year-1));
		filter.put("year3", String.valueOf(year-2));
		filter.put("year4", String.valueOf(year-3));
		filter.put("year5", String.valueOf(year-4));

		List<ReportVO> list = (List<ReportVO>) trustInfoMapper.selectList(filter); 
		int total = trustInfoMapper.selectTotalCount(filter);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);
		
		return map;
	}
	
	@Override
	public List<Map<String, Object>> getSubAllReports(String rptYear) throws Exception {
		LOGGER.info("year [{}]", rptYear);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) reportInfoMapper.selectAllList(rptYear);
		
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> map : list) {
			
			Map<String, Object> map2 = new HashMap<String, Object>();
			map2.put("memberSeqNo"    , map.get("MEMBERSEQNO"    ));
			map2.put("rptYear"        , map.get("RPTYEAR"        ));
			map2.put("writingKind"    , map.get("WRITINGKIND"    ));
			map2.put("inWorkNum"      , map.get("IN_WORK_NUM"    ));
			map2.put("outWorkNum"     , map.get("OUT_WORK_NUM"   ));
			map2.put("workSum"        , map.get("WORK_SUM"       ));
			map2.put("inRental"       , map.get("IN_RENTAL"      ));
			map2.put("outRental"      , map.get("OUT_RENTAL"     ));
			map2.put("rentalSum"      , map.get("RENTAL_SUM"     ));
			map2.put("inChrg"         , map.get("IN_CHRG"        ));
			map2.put("outChrg"        , map.get("OUT_CHRG"       ));
			map2.put("chrgSum"        , map.get("CHRG_SUM"       ));
			map2.put("inChrgRate"     , map.get("IN_CHRG_RATE"   ));
			map2.put("outChrgRate"    , map.get("OUT_CHRG_RATE"  ));
			map2.put("avgChrgRate"    , map.get("AVG_CHRG_RATE"  ));
			map2.put("writingKindDesc", map.get("WRITINGKINDDESC"));
			map2.put("writingOrder"   , map.get("WRITINGORDER"   ));
			map2.put("coName"         , map.get("CONAME"         ));
			map2.put("appNo"          , map.get("APPNO"          ));
			list2.add(map2);
		}
		
		return list2;
	}
	
	@Override
	public Map<String, Object> getSubReport(ReportInfoVO vo) throws Exception {
		LOGGER.info("vo [{}]", vo);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("report", reportInfoMapper.select(vo));
		map.put("yearList", reportInfoMapper.selectYearList(vo.getMemberSeqNo()));

		return map;
	}
	
	@Override
	public Map<String, Object> getSubReport(String memberSeqNo, String rptYear) throws Exception {
		LOGGER.info("params [{}] [{}]", memberSeqNo, rptYear);
		
		return getSubReport(new ReportInfoVO(memberSeqNo, rptYear));
	}
	
	@Override
	public Map<String, Object> getTrustReport(TrustInfoVO vo) throws Exception {
		LOGGER.info("vo [{}]", vo);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("report", trustInfoMapper.select(vo));
		map.put("yearList", trustInfoMapper.selectYearList(vo.getMemberSeqNo()));

		return map;
	}
	
	@Override
	public Map<String, Object> getTrustReport(String memberSeqNo, String rptYear) throws Exception {
		LOGGER.info("params [{}] [{}]", memberSeqNo, rptYear);
		
		return getTrustReport(new TrustInfoVO(memberSeqNo, rptYear));
	}
	
	@Override
	public void saveSubReport(ReportInfoVO vo) throws Exception {
		LOGGER.info("report info [{}]", vo);
		
//		reportInfoMapper.update(vo);
		reportInfoMapper.upsert(vo);
	}
	
	@Override
	public void saveSubReportStatuses(List<ReportStatVO> list) throws Exception {
		LOGGER.info("list [{}]", list.size());

		int updated = 0;
		for (ReportStatVO item : list) {
			/*LOGGER.info("item [{}][{}][{}][{}]", item.getMemberSeqNo(), item.getRptYear(), item.getWritingKind(), item.getPermKind());
			LOGGER.info("item [{}][{}][{}]", item.getInWorkNum(), item.getOutWorkNum(), item.getWorkSum());*/

//			int cnt = reportStatMapper.update(item);
			int cnt = reportStatMapper.upsert(item);
			LOGGER.info("cnt [{}]", cnt);

			updated++;
		}
		LOGGER.info("updated [{}]", updated);
	}
	
	@Override
	public void saveTrustReport(TrustInfoVO vo) throws Exception {
		LOGGER.info("report info [{}]", vo);
		
//		trustInfoMapper.update(vo);
		trustInfoMapper.upsert(vo);
		
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename1(), vo.getFilepath1(), "1");
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename2(), vo.getFilepath2(), "2");
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename3(), vo.getFilepath3(), "3");
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename4(), vo.getFilepath4(), "4");
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename5(), vo.getFilepath5(), "5");
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename6(), vo.getFilepath6(), "6");
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename7(), vo.getFilepath7(), "7");
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename8(), vo.getFilepath8(), "8");
		saveFile(vo.getTrustInfoSeqno(), vo.getFilename9(), vo.getFilepath9(), "9");
	}
	
	private void saveFile(String trustInfoSeqno, String filename, String filepath, String seq) throws Exception {
		
		TrustInfoVO vo = new TrustInfoVO();
		vo.setTrustInfoSeqno(trustInfoSeqno);
		
		if (StringUtils.isEmpty(filename)) {
			
			vo.setCodeValue(seq);
			
			trustInfoMapper.deleteFile(vo);
		}
		else {
			
			vo.setCodeValue(seq);
			vo.setFilename1(filename);
			vo.setFilepath1(filepath);
			vo.setFilesize("0");	// TODO
			
//			trustInfoMapper.updateFile(vo);
			trustInfoMapper.upsertFile(vo);
		}
	}
	
	@Override
	public List<ReportStat2VO> getSubReportStats(Map<String, Object> filter, int type) throws Exception {
	
		int fromYear = Integer.valueOf((String) filter.get("fromYear"));
		int toYear = Integer.valueOf((String) filter.get("toYear"));
		
		filter.put("nextYear", toYear + 1);
		filter.put("subYear", toYear - fromYear + 1);
		
		switch (type) {
		case 1:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList01(filter); 
		case 2:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList02(filter); 
		case 3:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList03(filter); 
		case 4:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList04(filter); 
		case 5:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList05(filter); 
		case 6:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList06(filter); 
		case 7:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList07(filter); 
		}
		
		return (List<ReportStat2VO>) reportStatMapper.selectSubStatList01(filter); 
	}
	@Override
	public List<ReportStat2VO> getSubReportStatsDetail(Map<String, Object> filter, int type) throws Exception {
	
		int fromYear = Integer.valueOf((String) filter.get("fromYear"));
		int toYear = Integer.valueOf((String) filter.get("toYear"));
		
		
		filter.put("subYear", fromYear);
		
		switch (type) {
		case 1:
			/*return (List<ReportStat2VO>) reportStatMapper.selectSubStatList01(filter);*/ 
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList01Detail(filter); 
		case 2:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList02Detail(filter); 
		case 3:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList03Detail(filter); 
		case 4:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList04Detail(filter); 
		case 5:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList05Detail(filter); 
		case 6:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList06Detail(filter); 
		case 7:
			return (List<ReportStat2VO>) reportStatMapper.selectSubStatList07Detail(filter); 
		}
		
		return (List<ReportStat2VO>) reportStatMapper.selectSubStatList01(filter); 
	}
	@Override
	public List<ReportStat2VO> getTrustReportStats(Map<String, Object> filter, int type) throws Exception {
		LOGGER.info("filter [{}]", filter);
		
		int fromYear = Integer.valueOf((String) filter.get("fromYear"));
		int toYear = Integer.valueOf((String) filter.get("toYear"));

		filter.put("nextYear", toYear + 1);
		filter.put("subYear", toYear - fromYear + 1);

		
		switch (type) {
		case 1:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList01(filter); 
		case 2:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList02(filter); 
		case 3:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList03(filter); 
		case 4:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList04(filter); 
		case 5:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList05(filter); 
		case 6:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList06(filter); 
		case 7:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList07(filter); 
		case 8:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList08(filter); 
		case 9:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList09(filter); 
		case 10:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList10(filter); 
		case 11:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList11(filter);
		case 12:
			return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList12(filter); 
		}
		
		return (List<ReportStat2VO>) reportStatMapper.selectTrustStatList01(filter); 
	}
	
	@Override
	public List<ReportStatVO> getSubReportStatuses(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);

		List<ReportStatVO> list = (List<ReportStatVO>) reportStatMapper.selectList(filter);
		LOGGER.info("wwlist [{}]", list);
		
		// set default value
		if (list == null || list.size() == 0) {
			
			list = new ArrayList<ReportStatVO>();
			
			List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) writingKindDAO.selectList(filter);
			List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) permKindDAO.selectList(filter);

			for (ChgWritingKindVO writingKind : writingKindList) {
				
				ReportStatVO vo = new ReportStatVO();
				
				vo.setWritingKind(writingKind.getWritingKind());
				vo.setWritingKindDesc(writingKind.getWritingKindStr());
				//vo.setPermKind(permKind.getPermKind());
				//vo.setPermKindDesc(permKind.getPermKindStr());
				
				vo.setInWorkNum("0");
				vo.setOutWorkNum("0");
				vo.setWorkSum("0");
				
				vo.setInRental("0");
				vo.setOutRental("0");
				vo.setRentalSum("0");
				
				vo.setInChrg("0");
				vo.setOutChrg("0");
				vo.setChrgSum("0");
				
				vo.setInChrgRate("0");
				vo.setOutChrgRate("0");
				vo.setAvgChrgRate("0");
				
				list.add(vo);

/*				for (ChgPermKindVO permKind : permKindList) {
					if (permKind.getWritingKind().equals(writingKind.getWritingKind())) {

						ReportStatVO vo = new ReportStatVO();
						
						vo.setWritingKind(writingKind.getWritingKind());
						vo.setWritingKindDesc(writingKind.getWritingKindStr());
						vo.setPermKind(permKind.getPermKind());
						vo.setPermKindDesc(permKind.getPermKindStr());
						
						vo.setInWorkNum("0");
						vo.setOutWorkNum("0");
						vo.setWorkSum("0");
						
						vo.setInRental("0");
						vo.setOutRental("0");
						vo.setRentalSum("0");
						
						vo.setInChrg("0");
						vo.setOutChrg("0");
						vo.setChrgSum("0");
						
						vo.setInChrgRate("0");
						vo.setOutChrgRate("0");
						vo.setAvgChrgRate("0");
						
						list.add(vo);
					}
				}*/
			}
		}

		return list; 
	}
	
	@Override
	public List<ReportStatVO> getSubReportStatuses(String memberSeqNo, String rptYear) throws Exception {
		LOGGER.info("member seq no [{}], rpt year [{}]", memberSeqNo, rptYear);
		
		Map<String, Object> filter = CommonUtils.pojoToMap(new ReportStatVO(memberSeqNo, rptYear));
		
		return getSubReportStatuses(filter); 
	}
	
	@Override
	public List<ReportStatVO> getSubReportSumStatuses(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);
		
		return (List<ReportStatVO>) reportStatMapper.selectSumList(filter); 
	}
	
	@Override
	public List<TrustStatVO> getTrustReportSumStatuses(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);
		
		return (List<TrustStatVO>) trustStatMapper.selectSumList(filter); 
	}

	@Override
	public Map<String, Object> getTrustReportRoyaltyStatuses(Map<String, Object> filter) throws Exception {

		return getTrustReportRoyaltyStatuses(filter, false);
	}
	
	@Override
	public Map<String, Object> getTrustReportRoyaltyStatuses(Map<String, Object> filter, boolean init) throws Exception {
		LOGGER.info("filter [{}]", filter);
		
		List<TrustStat2VO> royaltyList = (List<TrustStat2VO>) trustStatMapper.selectRoyaltyList(filter);
		List<TrustStat6VO> copyrightList = (List<TrustStat6VO>) trustStatMapper.selectCopyrightList(filter); 
		List<TrustStat6VO> executeList = (List<TrustStat6VO>) trustStatMapper.selectExecuteList(filter); 
		List<TrustStat6VO> makerList = (List<TrustStat6VO>) trustStatMapper.selectMakerList(filter);
		
		// set default value
		if (init && (royaltyList == null || royaltyList.size() == 0)) {
			royaltyList = new ArrayList<TrustStat2VO>();
		
			{
				TrustStat2VO vo = new TrustStat2VO();
				vo.setGubunKind("1");
				vo.setGubunName("복제.배포");
				royaltyList.add(vo);
			}
			{
				TrustStat2VO vo = new TrustStat2VO();
				vo.setGubunKind("1");
				vo.setGubunName("전송사용료(디음송)");
				royaltyList.add(vo);
			}
			{
				TrustStat2VO vo = new TrustStat2VO();
				vo.setGubunKind("1");
				vo.setGubunName("공연");
				royaltyList.add(vo);
			}
			{
				TrustStat2VO vo = new TrustStat2VO();
				vo.setGubunKind("1");
				vo.setGubunName("방송");
				royaltyList.add(vo);
			}
			{
				TrustStat2VO vo = new TrustStat2VO();
				vo.setGubunKind("0");
				vo.setGubunName("기타");
				royaltyList.add(vo);
			}
			/*{
				TrustStat2VO vo = new TrustStat2VO();
				vo.setGubunKind("0");
				vo.setGubunName("기타");
				royaltyList.add(vo);
			}*/
		}
		
		// set default value
		if (init && (copyrightList == null || copyrightList.size() == 0)) {
			copyrightList = new ArrayList<TrustStat6VO>();

			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("1");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("2");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("3");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("4");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("5");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("6");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("7");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("8");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("9");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("10");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("11");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("12");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("13");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("14");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("15");	
				copyrightList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("16");	
				copyrightList.add(vo);
			}
		}
		if(copyrightList.size()>11){
		
			for (TrustStat6VO item : copyrightList) {
				
				String[] gubunName = {"", "어문", "미술", "사진", "음악", "계", "복제", "공연", "방송", "전송", "계","", "복제", "공연", "방송", "전송", "계"};
				int gubun = Integer.valueOf(item.getGubun());
				item.setGubunName(gubunName[gubun]);
			}		
		}else{
		
				for (TrustStat6VO item : copyrightList) {
				
				String[] gubunName = {"", "어문", "미술", "사진", "음악", "계", "복제", "공연", "방송", "전송", "계",""};
				int gubun = Integer.valueOf(item.getGubun());
				item.setGubunName(gubunName[gubun]);
			}	
		}
		
	

		// set default value
		if (init && (executeList == null || executeList.size() == 0)) {
			executeList = new ArrayList<TrustStat6VO>();

			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("1");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("2");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("3");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("4");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("5");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("6");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("7");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("8");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("9");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("10");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("11");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("12");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("13");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("14");	
				executeList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("15");	
				executeList.add(vo);
			}
		}
		
		for (TrustStat6VO item : executeList) {
			String[] gubunName = {"", 
					"주실연자<br>(가수)", "주실연자<br>(연주)", "부실연자", "지휘자", "계", 
					"주실연자<br>(가수)", "주실연자<br>(연주)", "부실연자", "지휘자", "계", 
					"주실연자<br>(가수)", "주실연자<br>(연주)", "부실연자", "지휘자", "계"};
			int gubun = Integer.valueOf(item.getGubun());
			item.setGubunName(gubunName[gubun]);
		}

		// set default value
		if (init && (makerList == null || makerList.size() == 0)) {
			makerList = new ArrayList<TrustStat6VO>();

			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("1");	
				makerList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("2");	
				makerList.add(vo);
			}
			{
				TrustStat6VO vo = new TrustStat6VO();
				vo.setGubun("3");	
				makerList.add(vo);
			}
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("royaltyList", royaltyList);		// 신탁사용료
		map.put("copyrightList", copyrightList);	// 저작권자
		map.put("executeList", executeList);		// 실연자
		map.put("makerList", makerList);			// 음반제작자
		
		return map; 
	}

	@Override
	public void saveTrustReportRoyaltyStatuses(List<TrustStat2VO> list) throws Exception {
		LOGGER.info("list [{}]", list);

		for (TrustStat2VO item : list) {

			// insert / update, set trustGubunSeqNo
			trustStatMapper.upsertTrustGubun(item);
			
//			trustStatMapper.updateRoyalty(item);
			trustStatMapper.upsertRoyalty(item);
		}
	}
	
	@Override
	public void saveTrustReportCopyrightStatuses(List<TrustStat6VO> list) throws Exception {
		LOGGER.info("list [{}]", list);

		for (TrustStat6VO item : list) {

			trustStatMapper.upsertCopyright(item);
		}
	}
	
	@Override
	public void saveTrustReportExecuteStatuses(List<TrustStat6VO> list) throws Exception {
		LOGGER.info("list [{}]", list);

		for (TrustStat6VO item : list) {

			trustStatMapper.upsertExecute(item);
		}
	}
	
	@Override
	public void saveTrustReportMakerStatuses(List<TrustStat6VO> list) throws Exception {
		LOGGER.info("list [{}]", list);

		for (TrustStat6VO item : list) {

			trustStatMapper.upsertMaker(item);
		}
	}
	
	@Override
	public ReportInfoVO getReportManageInfo() throws Exception {

		return reportInfoMapper.selectReportManageInfo();
	}
	
	@Override
	public ReportInfoVO getTrustManageInfo() throws Exception {

		return reportInfoMapper.selectTrustManageInfo();
	}
}
