/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.report;

import java.util.*;

import cocoms.copyright.entity.*;

public interface ReportService {

	Map<String, Object> getSubReports(Map<String, Object> filter) throws Exception;
	Map<String, Object> getSubReports(String year, String type) throws Exception;
	
	Map<String, Object> getTrustReports(Map<String, Object> filter) throws Exception;

	List<Map<String, Object>> getSubAllReports(String rptYear) throws Exception;
	
	Map<String, Object> getSubReport(ReportInfoVO vo) throws Exception;
	Map<String, Object> getSubReport(String memberSeqNo, String rptYear) throws Exception;

	Map<String, Object> getTrustReport(TrustInfoVO vo) throws Exception;
	Map<String, Object> getTrustReport(String memberSeqNo, String rptYear) throws Exception;

	void saveSubReport(ReportInfoVO vo) throws Exception;
	void saveSubReportStatuses(List<ReportStatVO> list) throws Exception;

	void saveTrustReport(TrustInfoVO vo) throws Exception;

	List<ReportStat2VO> getSubReportStats(Map<String, Object> filter, int type) throws Exception;
	List<ReportStat2VO> getSubReportStatsDetail(Map<String, Object> filter, int type) throws Exception;
	List<ReportStat2VO> getTrustReportStats(Map<String, Object> filter, int type) throws Exception;
	
	List<ReportStatVO> getSubReportStatuses(Map<String, Object> filter) throws Exception;
	List<ReportStatVO> getSubReportStatuses(String memberSeqNo, String rptYear) throws Exception;
	List<ReportStatVO> getSubReportSumStatuses(Map<String, Object> filter) throws Exception;

	List<TrustStatVO> getTrustReportSumStatuses(Map<String, Object> filter) throws Exception;
	
	Map<String, Object> getTrustReportRoyaltyStatuses(Map<String, Object> filter) throws Exception;
	Map<String, Object> getTrustReportRoyaltyStatuses(Map<String, Object> filter, boolean init) throws Exception;

	void saveTrustReportRoyaltyStatuses(List<TrustStat2VO> list) throws Exception;
	void saveTrustReportCopyrightStatuses(List<TrustStat6VO> list) throws Exception;
	void saveTrustReportExecuteStatuses(List<TrustStat6VO> list) throws Exception;
	void saveTrustReportMakerStatuses(List<TrustStat6VO> list) throws Exception;
	
	ReportInfoVO getReportManageInfo() throws Exception;
	ReportInfoVO getTrustManageInfo() throws Exception;
}
