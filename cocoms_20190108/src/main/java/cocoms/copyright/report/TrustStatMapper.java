/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.report;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("trustStatMapper")
public interface TrustStatMapper {

	List<?> selectSumList(Map<String, Object> filter) throws Exception;
	
	List<?> selectRoyaltyList(Map<String, Object> filter) throws Exception;
	List<?> selectCopyrightList(Map<String, Object> filter) throws Exception;
	List<?> selectExecuteList(Map<String, Object> filter) throws Exception;
	List<?> selectMakerList(Map<String, Object> filter) throws Exception;
	
	void upsertTrustGubun(TrustStat2VO vo) throws Exception;

	void updateRoyalty(TrustStat2VO vo) throws Exception;
	void upsertRoyalty(TrustStat2VO vo) throws Exception;
	
	void upsertCopyright(TrustStat6VO vo) throws Exception;
	
	void upsertExecute(TrustStat6VO vo) throws Exception;

	void upsertMaker(TrustStat6VO vo) throws Exception;
}
