/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.report;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("reportStatMapper")
public interface ReportStatMapper {

	List<?> selectList(Map<String, Object> filter) throws Exception;
	List<?> selectSumList(Map<String, Object> filter) throws Exception;

	List<?> selectTrustStatList01(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList02(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList03(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList04(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList05(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList06(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList07(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList08(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList09(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList10(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList11(Map<String, Object> filter) throws Exception;
	List<?> selectTrustStatList12(Map<String, Object> filter) throws Exception;
	
	List<?> selectSubStatList01(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList02(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList03(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList04(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList05(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList06(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList07(Map<String, Object> filter) throws Exception;
	
	List<?> selectSubStatList01Detail(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList02Detail(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList03Detail(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList04Detail(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList05Detail(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList06Detail(Map<String, Object> filter) throws Exception;
	List<?> selectSubStatList07Detail(Map<String, Object> filter) throws Exception;
	
	void insert(ReportStatVO vo) throws Exception;
	
	int update(ReportStatVO vo) throws Exception;
	int upsert(ReportStatVO vo) throws Exception;

	void delete(ReportStatVO vo) throws Exception;
	
}
