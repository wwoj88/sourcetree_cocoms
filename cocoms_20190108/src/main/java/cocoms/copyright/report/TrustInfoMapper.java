/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.report;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("trustInfoMapper")
public interface TrustInfoMapper {

	List<?> selectList(Map<String, Object> filter) throws Exception;
	int selectTotalCount(Map<String, ?> filter) throws Exception;

	List<?> selectAllList(String rptYear) throws Exception;
	List<?> selectYearList(String memberSeqNo) throws Exception;
	
	TrustInfoVO select(TrustInfoVO vo) throws Exception;

	void insert(TrustInfoVO vo) throws Exception;
	
	void update(TrustInfoVO vo) throws Exception;
	void updateFile(TrustInfoVO vo) throws Exception;
	void upsertFile(TrustInfoVO vo) throws Exception;

	void upsert(TrustInfoVO vo) throws Exception;

	void delete(TrustInfoVO vo) throws Exception;
	void deleteFile(TrustInfoVO vo) throws Exception;
	
}
