/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.application;

import java.util.*;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.entity.*;

@Service("applHistoryService")
public class ApplHistoryServiceImpl extends EgovAbstractServiceImpl implements ApplHistoryService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name="applHistoryMapper")
	private ApplHistoryMapper applHistoryDAO;

	@Override
	public Map<String, Object> getApplHistories(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);

		List<ApplHistoryVO> list = (List<ApplHistoryVO>) applHistoryDAO.selectList(filter);
		int total = applHistoryDAO.selectTotalCount(filter);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);
		
		return map;
	}

	@Override
	public ApplHistoryVO getApplHistory(ApplHistoryVO vo) throws Exception {
		LOGGER.info("appl history [{}]", vo);

		return applHistoryDAO.select(vo);
	}

	@Override
	public ApplHistoryVO getApplHistory(String applSeqNo) throws Exception {
		LOGGER.info("applSeqNo[{}]", applSeqNo);

		return getApplHistory(new ApplHistoryVO(applSeqNo));
	}

	@Override
	public void saveApplHistory(ApplHistoryVO vo) throws Exception {
		LOGGER.info("appl history [{}]", vo);
try{
		applHistoryDAO.insert(vo);
}catch(Exception e){
	e.printStackTrace();
	
}
	}

}
