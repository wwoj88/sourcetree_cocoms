/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.application;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("chgHistoryMapper")
public interface ChgHistoryMapper {

	List<?> selectList(Map<String, Object> filter) throws Exception;
	List<?> selectChgWriteKindList(Map<String, Object> filter) throws Exception;
	List<?> selectChgPermKindList(Map<String, Object> filter) throws Exception;
	List<?> selectChgCdList(Map<String, Object> filter) throws Exception;
	List<?> selectChgFileList(Map<String, Object> filter) throws Exception;
	
	ChgHistoryVO selectByMemberInfo(Map<String, Object> filter) throws Exception;
	ChgHistoryVO selectByMemberInfoTmp(String memberseqno) throws Exception;
	ChgHistoryVO select(Map<String, Object> filter) throws Exception;
	
	void insert(ChgHistoryVO vo) throws Exception;
	void insertChgCd(ChgCdVO vo) throws Exception;
    
	void delete(ChgHistoryVO vo) throws Exception;
	void insertTemp(ChgHistoryVO chgHistory) throws Exception;
	
	void deleteChgTemp(String memberSeqNo)throws Exception;
	Integer selectChgTempcount(String memberSeqNo)throws Exception;
	
	
	ChgHistoryVO selectByMemberInfoSub(String memberSeqNo)throws Exception;
	void insertChgcdTemp(ChgCdVO vo) throws Exception;
	List<?> selectChgCdListTmp(String memberSeqNo)throws Exception;
	void deleteChgcdTemp(String memberSeqNo)throws Exception;
	void deleteChghfileTmp(String memberSeqNo) throws Exception;
	String selectChghistornoTmp(String memberSeqNo) throws Exception;
	void insertChghFileTmp(ChghFileVO chghFileVO) throws Exception;
	ChgHistoryVO selectSubAdmin(String memberSeqNo) throws Exception;
	String selectChghistoryseq(String rpmgmseqno)throws Exception;
	List<?> selectChgFileListTmp(Map<String, Object> filter) throws Exception;
	void insertChghFile(ChghFileVO fileList) throws Exception;
	void insertChgcd(ChgCdVO chgCdVO)throws Exception;
	void insertChgCdTmp(ChgCdVO chgCdVO) throws Exception;
	void upsert(Map<String, Object> filter) throws Exception;
	void updateChgcd(ChgWritingKindVO chgWritingKind) throws Exception;
	List<ChgCdVO> selectChgCdList2(String chgHistoryseqno)  throws Exception;

	void insertChgWritingList(Map<String, Object> map) throws Exception;
	void insertChgPermList(Map<String, Object> map)throws Exception;
	void insertChgCdList(Map<String, Object> map)throws Exception;
	List<ChgCdVO> selectChgCdLists(Map<String, Object> filter) throws Exception;
	String selectChghistoryseq2(String rpmgmseqno)throws Exception;
	ChgHistoryVO selectSubAdmin2(ChgHistoryVO vo)throws Exception;
	ChgHistoryVO selectByMemberInfo2(Map<String, Object> filter) throws Exception;
	
	
	
	
	
    
	
	
	

}
