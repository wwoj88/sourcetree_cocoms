package cocoms.copyright.application;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import javax.transaction.Transactional;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("excelReportService")
@Transactional
public class ExcelReportServiceImpl implements ExcelReportService {

	@Resource(name = "ExcelMapper")
	private ExcelMapper excelDao;

	@Autowired
	@Qualifier("sqlSession")
	private SqlSessionFactory sqlSessionFactory;

	@Transactional
	public Map<String, Object> insertnew(ArrayList inList) throws Exception {

		Map<String, Object> resultmap = new HashMap<String, Object>();

		boolean result = true;
		int inlistSize = inList.size();
		int checkReport = 0;
		String GENRE_CD = null;
		SqlSession session = null;
		Map<String, Object> checkmap = new HashMap<String, Object>();

		ArrayList<Map<String, Object>> uploadList_data = new ArrayList<Map<String, Object>>();

		checkmap = (Map<String, Object>) inList.get(0);
		 System.out.println(checkmap.toString());
		// REPORT TABLE CHECK 

		checkReport = excelDao.checkReport(checkmap);

		GENRE_CD = (String) checkmap.get("GENRE_CD");
		if (checkReport == 0) {
			checkmap.put("REPT_WORKS_CONT", inList.size());
			
			excelDao.commWorksReportInsert(checkmap);
		} else {
			result = false;
			resultmap.put("isSuccess", result);
			resultmap.put("overlap", 0);
			return resultmap;
		}
		int index = 1;
		// COMM_DATA INSERT 
		long startTime1 = System.currentTimeMillis();
		for (int i = 0; i < inlistSize; i++) {
			Map<String, Object> checkmap2 = new HashMap<String, Object>();
			checkmap2 = (Map<String, Object>) inList.get(i);

			if (excelDao.getExistCommWorks(checkmap2) == 0) {
				int works_id = excelDao.getCommWorksId();
				checkmap2.put("REPT_CNT", index);
				checkmap2.put("WORKS_ID", works_id);
				uploadList_data.add(checkmap2);
				index++;
			} else {
				//중복제거
				checkmap2.remove(i);
			}

		}
		int uploadList_datasize = uploadList_data.size();
		int overlap = inlistSize - uploadList_datasize;
		long endTime1 = System.currentTimeMillis();
		long resutTime2 = endTime1 - startTime1;

		int size = 0;
		size = uploadList_data.size();

		checkmap.put("REPT_WORKS_CONT", index - 1);

		excelDao.commWorksReportUpdateCnt(checkmap);

		long startTime = System.currentTimeMillis();
		try {
			int idx = 0;
			session = sqlSessionFactory.openSession(ExecutorType.BATCH);
			ExcelMapper mapper = session.getMapper(ExcelMapper.class);

			for (int i = 0; i < size; i++) {
				//System.out.println(uploadList_data.get(i).toString());
				mapper.commWorksInsertBacth(uploadList_data.get(i));
				if (idx % 1000 == 0) {
					session.commit();
				}

				idx++;
			}

		} catch (Exception e) {
			result = false;
			session.rollback();
			e.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}

		try {

			int idx = 0;
			session = sqlSessionFactory.openSession(ExecutorType.BATCH);
			ExcelMapper mapper = session.getMapper(ExcelMapper.class);

			if (GENRE_CD.equals("1")) {
				//System.out.println("음악 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commMusicInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}

			} else if (GENRE_CD.equals("2")) {
				//System.out.println("어문 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commBookInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}

			} else if (GENRE_CD.equals("3")) {
				//System.out.println("방송대본 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commScriptInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("4")) {
				//System.out.println("영화 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commMovieInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("5")) {
				//System.out.println("방송 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commBroadCastInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("6")) {
				//System.out.println("뉴스 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commNewsInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("7")) {
				//System.out.println("미술 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commArtInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("8")) {
				//System.out.println("이미지 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commImageInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("99")) {
				//System.out.println("기타 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commEtcInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			}

		} catch (Exception e) {
			result = false;
			session.rollback();
			e.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}

		uploadList_data.clear();
		/*
		 * long endTime = System.currentTimeMillis(); long resutTime = endTime -
		 * startTime; System.out.println("트랜젝션 배치" + " 소요시간  : " + resutTime /
		 * 1000 + "(ms)"); System.out.println("트랜젝션 배치" + endTime);
		 * System.out.println("트랜젝션 배치" + startTime);
		 */
		resultmap.put("isSuccess", result);
		resultmap.put("overlap", overlap);
		return resultmap;

	}

	@Transactional
	public Map<String, Object> modiReport(ArrayList inList) throws Exception {

		Map<String, Object> resultmap = new HashMap<String, Object>();

		boolean result = true;
		int inlistSize = inList.size();
		int checkReport = 0;
		String GENRE_CD = null;
		SqlSession session = null;

		Map<String, Object> checkmap = new HashMap<String, Object>();

		ArrayList<Map<String, Object>> uploadList_data = new ArrayList<Map<String, Object>>();

		checkmap = (Map<String, Object>) inList.get(0);

		checkReport = excelDao.checkReport(checkmap);
		System.out.println(checkmap.toString());
		System.out.println(checkmap.toString());
		System.out.println(checkmap.toString());
		System.out.println(checkmap.toString());
		if (checkReport == 0) {
			result = false;
			resultmap.put("isSuccess", result);
			resultmap.put("overlap", 0);
			return resultmap;
		}

		GENRE_CD = (String) checkmap.get("GENRE_CD");

		/*
		 * switch (GENRE_CD) { case "1": excelDao.commMusicDelete(checkmap);
		 * break;// 음악 case "2": excelDao.commBookDelete(checkmap); break;// 어문
		 * case "3": excelDao.commScripDelete(checkmap); break;// 방송대본 case "4":
		 * excelDao.commMovieDelete(checkmap); break;// 영화 case "5":
		 * excelDao.commBroadcastDelete(checkmap); break;// 방송 case "6":
		 * excelDao.commNewsDelete(checkmap); break;// 뉴스 case "7":
		 * excelDao.commArtDelete(checkmap); break;// 미술 case "8":
		 * excelDao.commImageDelete(checkmap); break;// 이미지 case "99":
		 * excelDao.commSideDelete(checkmap); break;// 기타 default: break; }
		 */
		//excelDao.deleteReport(checkmap);

		//excelDao.deleteReportData(checkmap);

		//excelDao.commWorksReportInsert(checkmap);

		int index = 1;
		// COMM_DATA INSERT 
		long startTime1 = System.currentTimeMillis();
		for (int i = 0; i < inlistSize; i++) {
			Map<String, Object> checkmap2 = new HashMap<String, Object>();
			checkmap2 = (Map<String, Object>) inList.get(i);

			if (excelDao.getExistCommWorks(checkmap2) == 0) {
				int works_id = excelDao.getCommWorksId();
				checkmap2.put("REPT_CNT", index);
				checkmap2.put("WORKS_ID", works_id);
				uploadList_data.add(checkmap2);
				index++;
			} else {
				//중복제거
				checkmap2.remove(i);
			}

		}
		checkmap.put("REPT_WORKS_CONT", uploadList_data.size());
		excelDao.commWorksReportModiUpdate(checkmap);

		int uploadList_datasize = uploadList_data.size();
		int overlap = inlistSize - uploadList_datasize;
		long endTime1 = System.currentTimeMillis();
		long resutTime2 = endTime1 - startTime1;

		int size = 0;
		size = uploadList_data.size();

		//checkmap.put("REPT_WORKS_CONT", index - 1);

		//excelDao.commWorksReportUpdateCnt(checkmap);

		long startTime = System.currentTimeMillis();
		try {
			int idx = 0;
			session = sqlSessionFactory.openSession(ExecutorType.BATCH);
			ExcelMapper mapper = session.getMapper(ExcelMapper.class);

			for (int i = 0; i < size; i++) {
				//System.out.println(uploadList_data.get(i).toString());
				mapper.commWorksInsertBacth(uploadList_data.get(i));
				if (idx % 1000 == 0) {
					session.commit();
				}

				idx++;
			}

		} catch (Exception e) {
			result = false;
			session.rollback();
			e.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}

		try {

			int idx = 0;
			session = sqlSessionFactory.openSession(ExecutorType.BATCH);
			ExcelMapper mapper = session.getMapper(ExcelMapper.class);

			if (GENRE_CD.equals("1")) {
				System.out.println("음악 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commMusicInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}

			} else if (GENRE_CD.equals("2")) {
				System.out.println("어문 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commBookInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}

			} else if (GENRE_CD.equals("3")) {
				System.out.println("방송대본 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commScriptInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("4")) {
				System.out.println("영화 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commMovieInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("5")) {
				System.out.println("방송 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commBroadCastInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("6")) {
				System.out.println("뉴스 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commNewsInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("7")) {
				System.out.println("미술 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commArtInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("8")) {
				System.out.println("이미지 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commImageInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			} else if (GENRE_CD.equals("99")) {
				System.out.println("기타 저작물 저장");
				for (int i = 0; i < size; i++) {
					//System.out.println(uploadList_data.get(i).toString());
					mapper.commEtcInsertBacth(uploadList_data.get(i));
					if (idx % 1000 == 0) {
						session.commit();
					}
					idx++;
				}
			}

		} catch (Exception e) {
			result = false;
			session.rollback();
			e.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}

		uploadList_data.clear();
		/*
		 * long endTime = System.currentTimeMillis(); long resutTime = endTime -
		 * startTime; System.out.println("트랜젝션 배치" + " 소요시간  : " + resutTime /
		 * 1000 + "(ms)"); System.out.println("트랜젝션 배치" + endTime);
		 * System.out.println("트랜젝션 배치" + startTime);
		 */
		resultmap.put("isSuccess", result);
		resultmap.put("overlap", overlap);
		return resultmap;

	}

	@Transactional
	public Map<String, Object> insertNodata(ArrayList inList) throws Exception {

		int checkReport = 0;
		boolean result = true;

		Map<String, Object> resultmap = new HashMap<String, Object>();
		Map<String, Object> checkmap = new HashMap<String, Object>();

		ArrayList<Map<String, Object>> uploadList_data = new ArrayList<Map<String, Object>>();

		checkmap = (Map<String, Object>) inList.get(0);

		// REPORT TABLE CHECK 

		checkReport = excelDao.checkReport(checkmap);
		try {
			if (checkReport == 0) {
				//checkmap.put("REPT_WORKS_CONT", null);
				checkmap.put("GENRE_CD", 0);
				excelDao.commWorksReportInsert(checkmap);
			} else {
				result = false;
				resultmap.put("isSuccess", result);
				resultmap.put("overlap", 0);
				return resultmap;
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			resultmap.put("isSuccess", result);
			resultmap.put("overlap", 0);
			return resultmap;
		}
		resultmap.put("isSuccess", result);

		return resultmap;

	}

	@Override
	public List selectYearList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return excelDao.selectYearList(map);
	}

	@Override
	public List getExcelReportsMonthCoCnt(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return excelDao.getExcelReportsMonthCoCnt(map);
	}

	@Override
	public List getExcelReportsMonthView(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return excelDao.getExcelReportsMonthView(map);
	}

	@Override
	public List selectExcelReportList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return excelDao.selectExcelReportList(map);
	}

	@Override
	public List selectExcelReportList_0(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return excelDao.selectExcelReportList_0(map);
	}

	@Override
	@Transactional
	public boolean deleteList(List<String> chArr) throws Exception {
		boolean result = false;
		// TODO Auto-generated method stub
		String[] tableNameList = { "CR_WORKS_MUSIC", "CR_WORKS_BOOK", "CR_WORKS_SCRIPT", "CR_WORKS_MOVIE", "CR_WORKS_BROADCAST", "CR_WORKS_NEWS", "CR_WORKS_ART", "CR_WORKS_IMAGE", "CR_WORKS_SIDE" };
		List<List> reportCountList = new ArrayList<List>();
		for (String i : chArr) {
			List<Map<String, Object>> resultList = excelDao.excelSelectWork(i);
			/*System.out.println(resultList.toString());*/
			int GENRE_CD = Integer.parseInt(resultList.get(0).get("GENRE_CD").toString());
			if (GENRE_CD == 99) {
				GENRE_CD = 9;
			}
			String tableName = tableNameList[GENRE_CD - 1];
			if (GENRE_CD == 9) {
				GENRE_CD = 99;
			}

			Map model = new HashMap<String, String>();
			model.put("tableName", tableName);
			model.put("works_id", i);
			model.put("GENRE_CD", GENRE_CD);
			model.put("REPT_YMD", resultList.get(0).get("REPT_YMD"));
			model.put("RGST_DTTM", resultList.get(0).get("RGST_DTTM"));
			model.put("TRST_ORGN_CODE", resultList.get(0).get("TRST_ORGN_CODE"));
			model.put("RGST_ORGN_NAME", resultList.get(0).get("RGST_ORGN_NAME"));

			reportCountList.add(resultList);

			try {
				excelDao.deleteReprtDetail(model);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();

			}
			try {
				excelDao.deleteReportData(model);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				excelDao.updateReport(model);
			} catch (Exception e) {
				e.printStackTrace();
			}
			result = true;
		}

		return result;
	}

	@Override
	public Map<String, Object> getExcelReports(Map<String, Object> filter) throws Exception {

		List<?> list = (List<?>) excelDao.selectExcelListStatus(filter);
		int total = excelDao.selectTotalCountStatus(filter);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);

		return map;
	}

	@Override
	public List getExcelReportsInfo(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return excelDao.getExcelReportsInfo(map);
	}

	@Override
	public List getExcelReportsList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return excelDao.getExcelReportsList(map);
	}

}
