/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.application;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.antlr.grammar.v3.ANTLRParser.exceptionGroup_return;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.metadata.GenericTableMetaDataProvider;
import org.springframework.stereotype.Service;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.common.XCrypto;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.MemberMapper;
import cocoms.copyright.web.form.ApplicationForm;

@Service("applicationService")
@Transactional
public class ApplicationServiceImpl extends EgovAbstractServiceImpl implements ApplicationService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name = "memberMapper")
	private MemberMapper memberDAO;

	@Resource(name = "writingKindMapper")
	private WritingKindMapper writingKindDAO;

	@Resource(name = "permKindMapper")
	private PermKindMapper permKindDAO;

	@Resource(name = "rpMgmMapper")
	private RpMgmMapper rpMgmDAO;

	@Resource(name = "chgHistoryMapper")
	private ChgHistoryMapper chgHistoryDAO;

	/** ID Generation */
	@Resource(name = "egovIdGnrService")
	private EgovIdGnrService egovIdGnrService;

	private ChgWritingKindVO ChgWritingKindVO;
	private ChgPermKindVO ChgPermKindVO;

	@Override
	public Map<String, Object> getAppliation(String memberSeqNo, String conditionCd) throws Exception {

		MemberVO params = new MemberVO();
		params.setMemberSeqNo(memberSeqNo);
		params.setConditionCd(conditionCd);

		/*
		 * execute services
		 */
		MemberVO member = memberDAO.select(params);
		List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) writingKindDAO.selectList(CommonUtils.pojoToMap(params));
		List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) permKindDAO.selectList(CommonUtils.pojoToMap(params));

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("member", member);
		map.put("writingKindList", writingKindList);
		map.put("permKindList", permKindList);
		return map;
	}

	@Override
	public Map<String, Object> getChgAppliation(String memberSeqNo, String conditionCd, String docType) throws Exception {

		MemberVO params = new MemberVO();
		params.setMemberSeqNo(memberSeqNo);
		params.setConditionCd(conditionCd);

		RpMgmVO params2 = new RpMgmVO();
		params2.setMemberSeqNo(memberSeqNo);
		RpMgmVO rpMgm = rpMgmDAO.selectByMemberInfofile(memberSeqNo);
		String Rpmgmseqno = "";
		if (("1").equals(docType) || ("2".equals(docType))) {
			Rpmgmseqno = rpMgmDAO.selectSubStatCd2(memberSeqNo);
			if (Rpmgmseqno == null) {
				Rpmgmseqno = rpMgmDAO.selectSubStatCd3(memberSeqNo);
			}
		} else {
			Rpmgmseqno = rpMgmDAO.selectSubStatCd(memberSeqNo);
			if (Rpmgmseqno == null) {
				Rpmgmseqno = rpMgmDAO.selectSubStatCd5(memberSeqNo);
			}
		}

		String ChgHistoryseqno = chgHistoryDAO.selectChghistoryseq(Rpmgmseqno);
		ChgHistoryVO Chghistory = new ChgHistoryVO();

		if (ChgHistoryseqno == null) {

			Rpmgmseqno = rpMgmDAO.selectSubStatCd3(memberSeqNo);
			if (Rpmgmseqno == null) {

				Rpmgmseqno = rpMgmDAO.selectSubStatCd4(memberSeqNo);

			}
			ChgHistoryseqno = chgHistoryDAO.selectChghistoryseq(Rpmgmseqno);
			if (ChgHistoryseqno == null) {
				Rpmgmseqno = rpMgmDAO.selectSubStatCd4(memberSeqNo);
				ChgHistoryseqno = chgHistoryDAO.selectChghistoryseq(Rpmgmseqno);
			}
			if (ChgHistoryseqno == null) {
				Rpmgmseqno = rpMgmDAO.selectSubStatCd5(memberSeqNo);
				ChgHistoryseqno = chgHistoryDAO.selectChghistoryseq(Rpmgmseqno);
			}

			ChgHistoryVO params3 = new ChgHistoryVO();
			params3.setMemberseqno(memberSeqNo);
			params3.setChgHistorySeqNo(ChgHistoryseqno);

			Chghistory = chgHistoryDAO.selectSubAdmin2(params3);
		} else {
			ChgHistoryVO params3 = new ChgHistoryVO();
			params3.setMemberseqno(memberSeqNo);
			params3.setChgHistorySeqNo(ChgHistoryseqno);

			Chghistory = chgHistoryDAO.selectSubAdmin2(params3);

		}
		/*
		 * execute services
		 */

		MemberVO member = memberDAO.select(params);
		/* ChgHistoryVO Chghistory = chgHistoryDAO.selectSubAdmin2(params3); */
		/* Chghistory = chgHistoryDAO.selectSubAdmin(memberSeqNo); */

		List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) writingKindDAO.selectListChg(ChgHistoryseqno);
		List<ChgWritingKindVO> writingKindList2 = (List<ChgWritingKindVO>) writingKindDAO.selectListChg1(ChgHistoryseqno);
		System.out.println(writingKindList2);
		List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) permKindDAO.selectListChg(ChgHistoryseqno);
		List<ChgPermKindVO> permKindList2 = (List<ChgPermKindVO>) permKindDAO.selectListChg3(ChgHistoryseqno);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rpMgm", rpMgm);
		map.put("member", member);
		map.put("writingKindList", writingKindList);
		map.put("writingKindList2", writingKindList2);
		map.put("permKindList", permKindList);
		map.put("permKindList2", permKindList2);
		return map;
	}

	@Override
	public Map<String, Object> getmodiAppliation(String memberSeqNo, String conditionCd) throws Exception {

		/*
		 * execute services
		 */
		// 변경신청 저장이있으면
		Integer result = -2;
		result = chgHistoryDAO.selectChgTempcount(memberSeqNo);

		if (result >= 1) {

			//tmp에있는걸 보여준다.
			String ConDetailCd = memberDAO.selectConDetailCdTmp(memberSeqNo);
			String conditioncd = memberDAO.selectConditioncdTmp(memberSeqNo);

			String statcd = memberDAO.selectStatCd(memberSeqNo);
			ChgHistoryVO chgHistory = chgHistoryDAO.selectByMemberInfoTmp(memberSeqNo);
			List<ChgCdVO> ChgCdList = (List<ChgCdVO>) chgHistoryDAO.selectChgCdListTmp(memberSeqNo);
			List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) writingKindDAO.selectListChgTemp(memberSeqNo);
			List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) permKindDAO.selectListChgTemp(memberSeqNo);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ChgCdVO", ChgCdList);
			map.put("condetailcd", ConDetailCd);
			map.put("statcd", statcd);
			map.put("tmpcd", "1");
			map.put("conditioncd", conditioncd);
			map.put("chgHistory", chgHistory);
			map.put("writingKindList", writingKindList);
			map.put("permKindList", permKindList);
			return map;

		} else {

			MemberVO params = new MemberVO();
			params.setMemberSeqNo(memberSeqNo);
			params.setConditionCd(conditionCd);
			String statcd = memberDAO.selectStatCd(memberSeqNo);
			/*
			 * execute services
			 */
			MemberVO member = memberDAO.select(params);

			List<ChgCdVO> ChgCdList = (List<ChgCdVO>) chgHistoryDAO.selectChgCdLists(CommonUtils.pojoToMap(params));
			List<PermKindVO> permKindList = (List<PermKindVO>) permKindDAO.selectList(CommonUtils.pojoToMap(params));
			List<WritingKindVO> writingKindList = (List<WritingKindVO>) writingKindDAO.selectList(CommonUtils.pojoToMap(params));

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ChgCdVO2", ChgCdList);
			map.put("member", member);
			map.put("tmpcd", "2");
			map.put("writingKindList", writingKindList);
			map.put("permKindList", permKindList);
			return map;
		}

	}

	@Override
	public Map<String, Object> getAppliationTemp(String memberSeqNo, String conditionCd) throws Exception {

		MemberVO params = new MemberVO();
		params.setMemberSeqNo(memberSeqNo);
		params.setConditionCd(conditionCd);

		/*
		 * execute services
		 */
		MemberVO member = memberDAO.selectTemp(params);
		if (member == null) {
			member = memberDAO.selectByPk(memberSeqNo);
		}
		List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) writingKindDAO.selectListTemp(CommonUtils.pojoToMap(params));
		List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) permKindDAO.selectListTemp(CommonUtils.pojoToMap(params));

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("member", member);
		map.put("writingKindList", writingKindList);
		map.put("permKindList", permKindList);
		return map;
	}

	@Override
	public Map<String, Object> getChgHistory(String memberSeqNo, String statCd) throws Exception {
		LOGGER.info("member seq no [{}], stat cd [{}]", memberSeqNo, statCd);

		/*
		 * execute services
		 */
		//		ChgHistoryVO chgHistory = new ChgHistoryVO();
		//		chgHistory.setChgHistorySeqNo(chgHistorySeqNo);

		RpMgmVO params = new RpMgmVO();
		params.setMemberSeqNo(memberSeqNo);
		params.setStatCd(statCd);

		/*
		 * execute services
		 */
		MemberVO member = memberDAO.selectByPk(memberSeqNo);
		RpMgmVO rpMgm = rpMgmDAO.selectByMemberInfo(params);
		if (rpMgm == null) {

			params.setStatCd("6");
			rpMgm = rpMgmDAO.selectByMemberInfo(params);
		}
		params.setStatCd(statCd);

		ChgHistoryVO chgHistory = chgHistoryDAO.selectByMemberInfo(CommonUtils.pojoToMap(params));

		if (chgHistory == null) {
			chgHistory = chgHistoryDAO.selectByMemberInfo2(CommonUtils.pojoToMap(params));
		}
		List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) chgHistoryDAO.selectChgWriteKindList(CommonUtils.pojoToMap(params));
		List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) chgHistoryDAO.selectChgPermKindList(CommonUtils.pojoToMap(params));
		List<String> chgCdList = (List<String>) chgHistoryDAO.selectChgCdList(CommonUtils.pojoToMap(params));
		List<ChgHistoryFileVO> chgHistoryFileList = (List<ChgHistoryFileVO>) chgHistoryDAO.selectChgFileList(CommonUtils.pojoToMap(params));

		List<ChgWritingKindVO> prewritingKindList = (List<ChgWritingKindVO>) writingKindDAO.selectList(CommonUtils.pojoToMap(params));
		List<ChgPermKindVO> prepermKindList = (List<ChgPermKindVO>) permKindDAO.selectList(CommonUtils.pojoToMap(params));

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("member", member);
		map.put("rpMgm", rpMgm);
		map.put("chgHistory", chgHistory);
		map.put("writingKindList", writingKindList);
		map.put("permKindList", permKindList);
		map.put("chgCdList", chgCdList);
		map.put("chgHistoryFileList", chgHistoryFileList);
		map.put("prewritingKindList", prewritingKindList);
		map.put("prepermKindList", prepermKindList);
		return map;
	}

	@Override
	public Map<String, Object> getRpMgms(Map<String, Object> filter) throws Exception {

		List<?> list = rpMgmDAO.selectList(filter);
		int total = rpMgmDAO.selectTotalCount(filter);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);

		return map;
	}

	@Override
	public List<RpMgmVO> getRpMgms(String memberSeqNo) throws Exception {

		RpMgmVO params = new RpMgmVO();
		params.setMemberSeqNo(memberSeqNo);

		Map<String, Object> filter = CommonUtils.pojoToMap(params);
		filter.put("firstRecordIndex", 0);
		filter.put("lastRecordIndex", 9999);

		return (List<RpMgmVO>) rpMgmDAO.selectList(CommonUtils.pojoToMap(filter));
	}

	@Override
	public List<Map<String, Object>> getChgHistories(String memberSeqNo, String statCd) throws Exception {

		RpMgmVO params = new RpMgmVO();
		params.setMemberSeqNo(memberSeqNo);
		params.setStatCd(statCd);

		/*
		 * execute services
		 */
		List<ChgHistoryVO> chgHistoryList = (List<ChgHistoryVO>) chgHistoryDAO.selectList(CommonUtils.pojoToMap(params));

		/*
		 * make return value
		 */
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (ChgHistoryVO item : chgHistoryList) {

			String rpMgmSeqNo = item.getRpMgmSeqNo();

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("rpMgm", rpMgmDAO.select(new RpMgmVO(rpMgmSeqNo)));
			map.put("chgHistory", item);
			map.put("chgCdList", chgHistoryDAO.selectChgCdList(CommonUtils.pojoToMap(item)));
			list.add(map);
		}

		return list;
	}

	@Override
	public void save(String memberSeqNo, String conditionCd, MemberVO member, List<WritingKindVO> writingKindList, List<PermKindVO> permKindList) throws Exception {

		save(memberSeqNo, conditionCd, member, writingKindList, permKindList, null, null);
	}

	@Override
	public void Chargesave(String memberSeqNo, String conditionCd, String condetailcd, MemberVO member) throws Exception {

		/* insert 허가/신고 관리 */
		RpMgmVO rpMgm = new RpMgmVO();
		BeanUtils.copyProperties(member, rpMgm);
		rpMgm.setRegDate(member.getRegDate2());

		rpMgmDAO.insert(rpMgm);
		memberDAO.updateinfo(member);
	}

	@Override
	@Transactional
	public void save(String memberSeqNo, String conditionCd, MemberVO member, List<WritingKindVO> writingKindList, List<PermKindVO> permKindList, List<String> chgCdList, String chgMemo) throws Exception {
		LOGGER.info("memberSeqNo [{}]", memberSeqNo);
		LOGGER.info("member [{}]", member.toString());
		LOGGER.info("writing kind [{}] perm kind [{}]", writingKindList.size(), permKindList.size());

		if (StringUtils.isEmpty(memberSeqNo)) {
			memberSeqNo = memberDAO.selectNewMemberSeqNo();
			LOGGER.info("memberSeqNo [{}]", memberSeqNo);

			member.setMemberSeqNo(memberSeqNo);
			for (WritingKindVO item : writingKindList) {
				item.setMemberSeqNo(memberSeqNo);
			}
			for (PermKindVO item : permKindList) {
				item.setMemberSeqNo(memberSeqNo);
			}
		}
		String nStatCd = memberDAO.selectStatCd(memberSeqNo);
		/* insert 허가/신고 관리 rpmg인서트 */
		RpMgmVO rpMgm = new RpMgmVO();
		BeanUtils.copyProperties(member, rpMgm);
		rpMgm.setRegDate(member.getRegDate2());
		if ("7".equals(nStatCd) || "9".equals(nStatCd)) {
			String certinfoseqno = rpMgmDAO.selectCertinfoSeqNo(memberSeqNo);
			rpMgm.setCertInfoSeqNo(certinfoseqno);
		}
		rpMgmDAO.insert(rpMgm);
		String rpMgmSeqNo = rpMgm.getRpMgmSeqNo();

		/* insert change history 히스토리 인서트 */
		ChgHistoryVO chgHistory = new ChgHistoryVO();
		BeanUtils.copyProperties(member, chgHistory);
		chgHistory.setRpMgmSeqNo(rpMgmSeqNo);

		chgHistory.setPostCeoName(member.getCeoName());
		chgHistory.setPostCeoRegNo(member.getCeoRegNo());
		chgHistory.setPostCeoEmail(member.getCeoEmail());
		chgHistory.setPostCeoZipcode(member.getCeoZipcode());
		chgHistory.setPostCeoSido(member.getCeoSido());
		chgHistory.setPostCeoGugun(member.getCeoGugun());
		chgHistory.setPostCeoDong(member.getCeoDong());
		chgHistory.setPostCeoBunji(member.getCeoBunji());
		chgHistory.setPostCeoDetailAddr(member.getCeoDetailAddr());
		chgHistory.setPostCeoTel(member.getCeoTel());
		chgHistory.setPostCeoFax(member.getCeoFax());
		chgHistory.setPostCoName(member.getCoName());
		chgHistory.setPostTrustZipcode(member.getTrustZipcode());
		chgHistory.setPostTrustSido(member.getTrustSido());
		chgHistory.setPostTrustGugun(member.getTrustGugun());
		chgHistory.setPostTrustDong(member.getTrustDong());
		chgHistory.setPostTrustBunji(member.getTrustBunji());
		chgHistory.setPostTrustDetailAddr(member.getTrustDetailAddr());
		chgHistory.setPostTrustTel(member.getTrustTel());
		chgHistory.setPostTrustFax(member.getTrustFax());
		chgHistory.setPostCeoMobile(member.getCeoMobile());
		chgHistory.setPostTrustUrl(member.getTrustUrl());
		chgHistory.setPostBubNo(member.getBubNo());
		chgHistory.setPostCoNo(member.getCoNo());
		chgHistory.setPostSms(member.getSms());

		chgHistory.setChgMemo(chgMemo);
		chgHistory.setRegdate(DateUtils.getCurrDateNumber());

		chgHistory.setPreBubNo(member.getPrebubNo());
		chgHistory.setPreCeoMobile(member.getPreceoMobile());
		chgHistory.setPreCoNo(member.getPrecoNo());
		chgHistory.setPreSms(member.getPreSms());
		chgHistory.setPreCeoEmail(member.getPreceoEmail());
		chgHistory.setPreCeoName(member.getPreceoName());
		chgHistory.setPreCeoRegNo(member.getPreceoRegNo());
		chgHistory.setPreCeoZipcode(member.getPreceoZipcode());
		chgHistory.setPreCeoSido(member.getPreceoSido());
		chgHistory.setPreCeoGugun(member.getPreceoGugun());
		chgHistory.setPreCeoBunji(member.getPreceoBunji());
		chgHistory.setPreCeoDong(member.getPreceoDong());
		chgHistory.setPreCeoDetailAddr(member.getPreceoDetailAddr());
		chgHistory.setPreCeoTel(member.getPreceoTel());
		chgHistory.setPreCeoFax(member.getPreceoFax());
		chgHistory.setPreCoName(member.getPrecoName());
		chgHistory.setPreTrustUrl(member.getPretrustUrl());
		chgHistory.setPreTrustZipcode(member.getPretrustZipcode());
		chgHistory.setPreTrustSido(member.getPretrustSido());
		chgHistory.setPreTrustGugun(member.getPretrustGugun());
		chgHistory.setPreTrustDong(member.getPretrustDong());
		chgHistory.setPreTrustBunji(member.getPretrustBunji());
		chgHistory.setPreTrustDetailAddr(member.getPretrustDetailAddr());
		chgHistory.setPreTrustTel(member.getPretrustTel());
		chgHistory.setPreTrustFax(member.getPretrustFax());
		//}
		//	if("2".equals(chgcdvo)){

		//}
		//	if("3".equals(chgcdvo)){

		//}
		//	if("6".equals(chgcdvo)){

		//	}
		//	if("7".equals(chgcdvo)){

		//}
		if (member.getCeoRegNo() != null && member.getCeoRegNo().length() <= 13) {

			String regNo = XCrypto.encPatternReg(member.getCeoRegNo());
			member.setCeoRegNo(regNo);
			//
		}

		//}

		chgHistoryDAO.insert(chgHistory);
		String chgHistoryseqno = chgHistory.getChgHistorySeqNo();

		//memberDAO.upsert(member); //멤버상태업데이트
		memberDAO.updateinfo(member);

		// TODO update CR_MEMBER.STATCD, appno ...
		PermKindVO permKindVO = new PermKindVO();
		permKindVO.setMemberSeqNo(memberSeqNo);
		permKindVO.setConditionCd(conditionCd);
		permKindDAO.deleteTemp(permKindVO);
		permKindDAO.deleteChgTemp(permKindVO);
		/* delete writing kind */
		WritingKindVO writingKindVO = new WritingKindVO();
		writingKindVO.setMemberSeqNo(memberSeqNo);
		writingKindVO.setConditionCd(conditionCd);
		writingKindDAO.deleteTemp(writingKindVO);
		writingKindDAO.deleteChgTemp(writingKindVO);
		/* delete perm kind */

		memberDAO.deleteTemp(member);

		MemberVO params = new MemberVO();
		params.setMemberSeqNo(memberSeqNo);

		List<ChgHistoryFileVO> chgHistoryFileList = (List<ChgHistoryFileVO>) chgHistoryDAO.selectChgFileListTmp(CommonUtils.pojoToMap(params));
		List<WritingKindVO> ChgWritingKindList = (List<WritingKindVO>) writingKindDAO.selectList2(CommonUtils.pojoToMap(params));
		List<PermKindVO> ChgPermKindList = (List<PermKindVO>) permKindDAO.selectList2(CommonUtils.pojoToMap(params));

		for (ChgHistoryFileVO item : chgHistoryFileList) {

			ChghFileVO Chg = new ChghFileVO();
			Chg.setFilename(item.getFilename());
			Chg.setFilepath(item.getFilepath());
			Chg.setFsize(item.getFilesize());
			Chg.setRegdate(DateUtils.getCurrDateNumber());
			Chg.setRegtime(DateUtils.getCurrTimeNumber());
			Chg.setChghistoryseqno(chgHistoryseqno);

			chgHistoryDAO.insertChghFile(Chg);

		}

		chgHistoryDAO.deleteChgcdTemp(memberSeqNo);
		chgHistoryDAO.deleteChghfileTmp(memberSeqNo);
		chgHistoryDAO.deleteChgTemp(memberSeqNo);

		/* insert writing kind */
		for (WritingKindVO item : writingKindList) {
			LOGGER.info("writing kind [{}]", item);

			ChgWritingKindVO chgwritingmvo = new ChgWritingKindVO();
			chgwritingmvo.setChghistoryseqno(chgHistoryseqno);
			chgwritingmvo.setWritingchgcd("2");
			chgwritingmvo.setWritingKind(item.getWritingKind());
			chgwritingmvo.setWritingEtc(item.getWritingEtc());
			chgwritingmvo.setMemberSeqNo(memberSeqNo);
			writingKindDAO.insertChg(chgwritingmvo);

			item.setMemberSeqNo(memberSeqNo);
			item.setConditionCd(conditionCd);

			//writingKindDAO.insert(item);	

		}

		for (WritingKindVO item : ChgWritingKindList) {
			ChgWritingKindVO writingkind = new ChgWritingKindVO();
			writingkind.setChghistoryseqno(chgHistoryseqno);
			writingkind.setWritingchgcd("1");
			writingkind.setWritingKind(item.getWritingKind());
			writingkind.setWritingEtc(item.getWritingEtc());
			writingKindDAO.insertChg(writingkind);
		}

		/* insert perm kind */
		for (PermKindVO item : permKindList) {
			LOGGER.info("perm kind [{}]", item);

			item.setMemberSeqNo(memberSeqNo);
			item.setConditionCd(conditionCd);

			//permKindDAO.insert(item);

			ChgPermKindVO chgpermvo = new ChgPermKindVO();

			Map<String, String> param = new HashMap<String, String>();
			param.put("writingkind", item.getWritingKind());
			param.put("chghistoryseqno", chgHistoryseqno);
			String seqno = writingKindDAO.selectchgseqno(param);

			chgpermvo.setChgwritingkindseqno(seqno);
			chgpermvo.setChghistoryseqno(chgHistoryseqno);
			chgpermvo.setPermKind(item.getPermKind());
			chgpermvo.setWritingKind(item.getWritingKind());
			chgpermvo.setPermEtc(item.getPermEtc());
			permKindDAO.insertChg(chgpermvo);

		}
		for (PermKindVO item : ChgPermKindList) {
			LOGGER.info("ChgPermKindList kind [{}]", item);

			ChgPermKindVO chgpermvo = new ChgPermKindVO();

			Map<String, String> param = new HashMap<String, String>();
			param.put("writingkind", item.getWritingKind());
			param.put("chghistoryseqno", chgHistoryseqno);
			String seqno = writingKindDAO.selectchgseqno2(param);

			chgpermvo.setChgwritingkindseqno(seqno);
			chgpermvo.setChghistoryseqno(chgHistoryseqno);
			chgpermvo.setPermKind(item.getPermKind());
			chgpermvo.setPermEtc(item.getPermEtc());
			permKindDAO.insertChg(chgpermvo);

		}

		/* insert chgcd */
		if (chgCdList != null) {
			for (String chgCd : chgCdList) {

				chgHistoryDAO.insertChgCd(new ChgCdVO(chgHistory.getChgHistorySeqNo(), chgCd));
			}
		}

	}

	@Override
	@Transactional
	public void saveTemp(String memberSeqNo, String conditionCd, MemberVO member, List<WritingKindVO> writingKindList, List<PermKindVO> permKindList) throws Exception {
		LOGGER.info("member [{}]", member.toString());
		LOGGER.info("writing kind [{}] perm kind [{}]", writingKindList.size(), permKindList.size());

		//		member.setMemberSeqNo(memberSeqNo);
		//		member.setConditionCd(conditionCd);
		//		MemberVO memberOrg = memberDAO.selectTemp(CommonUtils.pojoToMap(member));

		/* delete perm kind temp */
		PermKindVO permKindVO = new PermKindVO();
		permKindVO.setMemberSeqNo(memberSeqNo);
		permKindVO.setConditionCd(conditionCd);
		permKindDAO.deleteTemp(permKindVO);

		/* delete writing kind temp */
		WritingKindVO writingKindVO = new WritingKindVO();
		writingKindVO.setMemberSeqNo(memberSeqNo);
		writingKindVO.setConditionCd(conditionCd);
		writingKindDAO.deleteTemp(writingKindVO);

		/* delete member temp */
		memberDAO.deleteTemp(member);

		/* insert member temp */
		//		member.setCoGubunCd(memberOrg.getCoGubunCd());
		//		member.setCoNo(memberOrg.getCoNo());
		//		member.setBubNo(memberOrg.getBubNo());
		if (member.getCeoRegNo() != null && member.getCeoRegNo().length() <= 13) {

			String regNo = XCrypto.encPatternReg(member.getCeoRegNo());
			member.setCeoRegNo(regNo);
		}
		String statcd = memberDAO.selectStatCd(member.getMemberSeqNo());
		member.setStatCd(statcd);
		memberDAO.insertTemp(member);
		memberDAO.ceoUpdateTemp(member);
		/* insert writing kind temp */
		/* insert writing kind */
		for (WritingKindVO item : writingKindList) {
			LOGGER.info("writing kind [{}]", item);

			item.setMemberSeqNo(memberSeqNo);
			item.setConditionCd(conditionCd);

			writingKindDAO.insertTemp(item);

		}

		/* insert perm kind */
		for (PermKindVO item : permKindList) {
			LOGGER.info("perm tettettekind [{}]", item);
			ChgPermKindVO chgpermvo = new ChgPermKindVO();

			item.setMemberSeqNo(memberSeqNo);
			item.setConditionCd(conditionCd);

			permKindDAO.insertTemp(item);

		}

	}

	@Override
	@Transactional
	public void savesubTemp(List<ChghFileVO> ChghFileList, String memberSeqNo, String conditionCd, MemberVO member, List<WritingKindVO> writingKindList, List<PermKindVO> permKindList, List<PermKindVO> prepermKindList,
			List<WritingKindVO> prewritingKindList, List<ChgCdVO> chgCdList, String chgMemo) throws Exception {
		/* delete perm kind temp */
		PermKindVO permKindVO2 = new PermKindVO();
		permKindVO2.setMemberSeqNo(memberSeqNo);
		permKindVO2.setConditionCd(conditionCd);
		permKindDAO.deleteTemp(permKindVO2);

		/* delete writing kind temp */
		WritingKindVO writingKindVO2 = new WritingKindVO();
		writingKindVO2.setMemberSeqNo(memberSeqNo);
		writingKindVO2.setConditionCd(conditionCd);
		writingKindDAO.deleteTemp(writingKindVO2);

		memberDAO.deleteTemp(member);
		/* insert member temp */
		//		member.setCoGubunCd(memberOrg.getCoGubunCd());
		//		member.setCoNo(memberOrg.getCoNo());
		//		member.setBubNo(memberOrg.getBubNo());
		if (member.getCeoRegNo() != null && member.getCeoRegNo().length() <= 13) {

			String regNo = XCrypto.encPatternReg(member.getCeoRegNo());
			member.setCeoRegNo(regNo);
		}
		String statcd = memberDAO.selectStatCd(member.getMemberSeqNo());
		member.setStatCd(statcd);
		memberDAO.insertTemp(member);

		String rpMgmseq = rpMgmDAO.selectrpmgseqno(member.getMemberSeqNo());

		String memberseqno = member.getMemberSeqNo();
		ChgHistoryVO chgHistory = new ChgHistoryVO();
		BeanUtils.copyProperties(member, chgHistory);
		chgHistory.setRpMgmSeqNo(rpMgmseq);
		chgHistory.setMemberseqno(member.getMemberSeqNo());
		chgHistory.setPostCeoName(member.getCeoName());
		chgHistory.setPostCeoRegNo(member.getCeoRegNo());
		chgHistory.setPostCeoEmail(member.getCeoEmail());
		chgHistory.setPostCeoZipcode(member.getCeoZipcode());
		chgHistory.setPostCeoSido(member.getCeoSido());
		chgHistory.setPostCeoGugun(member.getCeoGugun());
		chgHistory.setPostCeoDong(member.getCeoDong());
		chgHistory.setPostCeoBunji(member.getCeoBunji());
		chgHistory.setPostCeoDetailAddr(member.getCeoDetailAddr());
		chgHistory.setPostCeoTel(member.getCeoTel());
		chgHistory.setPostCeoFax(member.getCeoFax());
		chgHistory.setPostCoName(member.getCoName());
		chgHistory.setPostTrustZipcode(member.getTrustZipcode());
		chgHistory.setPostTrustSido(member.getTrustSido());
		chgHistory.setPostTrustGugun(member.getTrustGugun());
		chgHistory.setPostTrustDong(member.getTrustDong());
		chgHistory.setPostTrustBunji(member.getTrustBunji());
		chgHistory.setPostTrustDetailAddr(member.getTrustDetailAddr());
		chgHistory.setPostTrustTel(member.getTrustTel());
		chgHistory.setPostTrustFax(member.getTrustFax());
		chgHistory.setPostCeoMobile(member.getCeoMobile());
		chgHistory.setPostTrustUrl(member.getTrustUrl());
		chgHistory.setPostBubNo(member.getBubNo());
		chgHistory.setPostCoNo(member.getCoNo());
		chgHistory.setPostSms(member.getSms());

		chgHistory.setChgMemo(chgMemo);
		chgHistory.setRegdate(DateUtils.getCurrDateNumber());

		String Chgcd = "";

		for (ChgCdVO chgcdvo : chgCdList) {

			String Chgcd2 = chgcdvo.getChgCd();

			if ("1".equals(Chgcd2)) {

				chgHistory.setPreCeoName(member.getPreceoName());
				chgHistory.setPreCeoRegNo(member.getPreceoRegNo());
				chgHistory.setPreCeoZipcode(member.getPreceoZipcode());
				chgHistory.setPreCeoSido(member.getPreceoSido());
				chgHistory.setPreCeoGugun(member.getPreceoGugun());
				chgHistory.setPreCeoBunji(member.getCeoBunji());
				chgHistory.setPreCeoDetailAddr(member.getPreceoDetailAddr());
				chgHistory.setPreCeoTel(member.getPreceoTel());
				chgHistory.setPreCeoFax(member.getPreceoFax());
			}
			if ("2".equals(Chgcd2)) {
				chgHistory.setPreCoName(member.getPrecoName());
			}
			if ("3".equals(Chgcd2)) {
				chgHistory.setPreTrustZipcode(member.getPretrustZipcode());
				chgHistory.setPreTrustSido(member.getPretrustSido());
				chgHistory.setPreTrustGugun(member.getPretrustGugun());
				chgHistory.setPreTrustDong(member.getPretrustDong());
				chgHistory.setPreTrustBunji(member.getPretrustBunji());
				chgHistory.setPreTrustDetailAddr(member.getPretrustDetailAddr());
				chgHistory.setPreTrustTel(member.getPretrustTel());
				chgHistory.setPreTrustFax(member.getPretrustFax());
			}
			if ("6".equals(Chgcd2)) {

			}
			if ("7".equals(Chgcd2)) {

			}
			if (member.getCeoRegNo() != null && member.getCeoRegNo().length() <= 13) {

				String regNo = XCrypto.encPatternReg(member.getCeoRegNo());
				member.setCeoRegNo(regNo);
			}

		}

		/* delete perm kind temp */
		PermKindVO permKindVO = new PermKindVO();
		permKindVO.setMemberSeqNo(memberSeqNo);
		permKindDAO.deleteChgTemp(permKindVO);

		/* delete writing kind temp */
		WritingKindVO writingKindVO = new WritingKindVO();
		writingKindVO.setMemberSeqNo(memberSeqNo);
		writingKindDAO.deleteChgTemp(writingKindVO);

		/* delete member temp */
		chgHistoryDAO.deleteChghfileTmp(member.getMemberSeqNo());
		chgHistoryDAO.deleteChgcdTemp(member.getMemberSeqNo());
		chgHistoryDAO.deleteChgTemp(member.getMemberSeqNo());

		chgHistoryDAO.insertTemp(chgHistory);
		String chgHistoryseqno = chgHistory.getChgHistorySeqNo();
		/* insert writing kind */

		for (ChghFileVO item : ChghFileList) {
			ChghFileVO fileList = new ChghFileVO();

			fileList.setFilename(item.getFilename());
			fileList.setFilepath(item.getFilepath());
			fileList.setFsize(item.getFsize());
			fileList.setChghistoryseqno(chgHistoryseqno);
			fileList.setDelgubun("N");
			fileList.setRegdate(DateUtils.getCurrDateNumber());
			fileList.setRegtime(DateUtils.getCurrTimeNumber());
			fileList.setMemberseqno(memberSeqNo);

			chgHistoryDAO.insertChghFileTmp(fileList);

		}

		for (ChgCdVO chgcd : chgCdList) {
			LOGGER.info("chgCdList  [{}]", chgcd);

			ChgCdVO ChgCdVO = new ChgCdVO();
			ChgCdVO.setChgHistorySeqNo(chgHistoryseqno);
			ChgCdVO.setMemberseqno(memberSeqNo);
			ChgCdVO.setChgCd(chgcd.getChgCd());

			chgHistoryDAO.insertChgCdTmp(ChgCdVO);

		}

		/* insert writing kind */
		for (WritingKindVO item : writingKindList) {
			LOGGER.info("writing kind [{}]", item);

			ChgWritingKindVO chgwritingmvo = new ChgWritingKindVO();
			chgwritingmvo.setChghistoryseqno(chgHistoryseqno);
			chgwritingmvo.setWritingchgcd("2");
			chgwritingmvo.setWritingKind(item.getWritingKind());
			chgwritingmvo.setWritingEtc(item.getWritingEtc());
			chgwritingmvo.setMemberSeqNo(memberseqno);

			item.setMemberSeqNo(memberSeqNo);
			item.setConditionCd(conditionCd);

			writingKindDAO.insertChgTemp(chgwritingmvo);

		}

		/* insert perm kind */
		for (PermKindVO item : permKindList) {
			LOGGER.info("perm kind [{}]", item);

			ChgPermKindVO chgpermvo = new ChgPermKindVO();

			Map<String, String> param = new HashMap<String, String>();
			param.put("writingkind", item.getWritingKind());
			param.put("chghistoryseqno", chgHistoryseqno);
			String seqno = writingKindDAO.selectchgtempseqno(param);

			chgpermvo.setChgwritingkindseqno(seqno);
			chgpermvo.setChghistoryseqno(chgHistoryseqno);
			chgpermvo.setPermKind(item.getPermKind());
			chgpermvo.setWritingKind(item.getWritingKind());
			chgpermvo.setPermEtc(item.getPermEtc());
			chgpermvo.setMemberSeqNo(memberseqno);

			permKindDAO.insertChgTemp(chgpermvo);

		}

	}

	

}
