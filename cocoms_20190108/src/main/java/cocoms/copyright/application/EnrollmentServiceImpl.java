/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.application;

import java.util.*;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;


import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import cocoms.copyright.entity.*;

@Service("enrollmentService")
public class EnrollmentServiceImpl extends EgovAbstractServiceImpl implements EnrollmentService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name="enrollmentMapper")
	private EnrollmentMapper enrollmentDAO;

	@Override
	public Map<String, Object> getIssues(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);
		
			
		
		List<EnrollmentVO> list = (List<EnrollmentVO>) enrollmentDAO.selectList(filter);
	
		int total = enrollmentDAO.selectTotalCount(filter);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);
		
		return map;
	}
	
	@Override
	public void saveEnrollment(EnrollmentVO vo) throws Exception {
		LOGGER.info(vo.toString());
		try{
			enrollmentDAO.insert(vo);	
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public String getContent(String memberSeqNo) throws Exception {
		// TODO Auto-generated method stub
		return enrollmentDAO.selectContent(memberSeqNo);
	}
	

}
