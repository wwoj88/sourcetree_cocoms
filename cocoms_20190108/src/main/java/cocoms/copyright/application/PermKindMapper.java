/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.application;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("permKindMapper")
public interface PermKindMapper {

	List<?> selectList(Map<String, Object> filter) throws Exception;
	List<?> selectListTemp(Map<String, Object> filter) throws Exception;
	
	void insert(PermKindVO vo) throws Exception;
	void insertTemp(PermKindVO vo) throws Exception;
	void insertChg(ChgPermKindVO vo) throws Exception;
	void delete(PermKindVO vo) throws Exception;
	void deleteTemp(PermKindVO vo) throws Exception;
	void insertChgTemp(ChgPermKindVO vo) throws Exception;
	void deleteChgTemp(PermKindVO permKindVO) throws Exception;
	List<?> selectListChgTemp(String memberSeqNo)throws Exception;
    String selectWritingKindTmp(String chgwritingkindseqno)throws Exception;
	List<ChgPermKindVO> selectListChg(String chgHistoryseqno)throws Exception;
	String selectWritingKindChg(String chgwritingkindseqno) throws Exception;
	List<?> selectList2(Map<String, Object> filter)throws Exception;
	List<ChgPermKindVO> selectListPreChg(String chgHistorysedno)throws Exception;
	List<ChgPermKindVO> selectListPostChg(String chgHistorysedno)throws Exception;
	List<ChgPermKindVO> selectListChg3(String chgHistoryseqno)throws Exception;

}
