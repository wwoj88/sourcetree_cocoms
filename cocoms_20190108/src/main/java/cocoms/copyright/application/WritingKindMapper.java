/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.application;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("writingKindMapper")
public interface WritingKindMapper {

	List<?> selectList(Map<String, Object> filter) throws Exception;
	List<?> selectListTemp(Map<String, Object> filter) throws Exception;
	void insert(WritingKindVO vo) throws Exception;
	void insertTemp(WritingKindVO vo) throws Exception;
    void insertChg(ChgWritingKindVO vo) throws Exception;
	void delete(WritingKindVO vo) throws Exception;
	void deleteTemp(WritingKindVO vo) throws Exception;
	String selectchgseqno(Map<String, String> param)  throws Exception;
	void insertChgTemp(ChgWritingKindVO vo) throws Exception;
	String selectchgtempseqno(Map<String, String> param) throws Exception;
	void deleteChgTemp(WritingKindVO writingKindVO) throws Exception;
	List<?> selectListChgTemp(String memberSeqNo);
	List<ChgWritingKindVO> selectListChg(String ChgHistoryseqno) throws Exception;
	List<ChgWritingKindVO> selectListChg1(String ChgHistoryseqno) throws Exception;
	List<?> selectList2(Map<String, Object> filter) throws Exception;
	void insertChg2(ChgWritingKindVO vo) throws Exception;
	String selectchgseqno2(Map<String, String> filter) throws Exception;
	String selectchgseqno3(String chghistoryseqnoNew, String permKind)throws Exception;
	
	

}
