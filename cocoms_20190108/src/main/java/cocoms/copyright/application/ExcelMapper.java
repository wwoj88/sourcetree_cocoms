package cocoms.copyright.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("ExcelMapper")
public interface ExcelMapper {

	//void commMusicInsertBacth(ArrayList inList) throws Exception;

	//void commMusicInsertBacth(Object object) throws Exception;

	int ExistreptCnt(Map<String, Object> checkmap) throws Exception;

	int getCommWorksId() throws Exception;

	String getReptSeqn(Map<String, Object> checkmap) throws Exception;

	int getReptWorksCont(Map<String, Object> checkmap) throws Exception;

	int getModiWorksCont(Map<String, Object> checkmap) throws Exception;

	void commWorksReportUpdate(ArrayList<Map<String, Object>> uploadList) throws Exception;

	int getExistCommWorks(Map<String, Object> checkmap2) throws Exception;

	void commWorksReportInsert(Map<String, Object> checkmap2) throws Exception;

	int checkReport(Map<String, Object> checkmap) throws Exception;

	void insertReport(Map<String, Object> checkmap) throws Exception;

	void commWorksInsertBacth(Map<String, Object> map) throws Exception;

	void commWorksReportUpdateCnt(Map<String, Object> checkmap) throws Exception;

	void commMusicInsertBacth(Map<String, Object> checkmap) throws Exception;

	void commBookInsertBacth(Map<String, Object> checkmap) throws Exception;

	void commScriptInsertBacth(Map<String, Object> checkmap) throws Exception;

	void commMovieInsertBacth(Map<String, Object> checkmap) throws Exception;

	void commBroadCastInsertBacth(Map<String, Object> checkmap) throws Exception;

	void commNewsInsertBacth(Map<String, Object> checkmap) throws Exception;

	void commArtInsertBacth(Map<String, Object> checkmap) throws Exception;

	void commImageInsertBacth(Map<String, Object> checkmap) throws Exception;

	void commEtcInsertBacth(Map<String, Object> checkmap) throws Exception;

	void deleteReport(Map<String, Object> checkmap) throws Exception;

	void commMusicDelete(Map<String, Object> checkmap) throws Exception;

	void commBookDelete(Map<String, Object> checkmap) throws Exception;

	void commScripDelete(Map<String, Object> checkmap) throws Exception;

	void commMovieDelete(Map<String, Object> checkmap) throws Exception;

	void commBroadcastDelete(Map<String, Object> checkmap) throws Exception;

	void commNewsDelete(Map<String, Object> checkmap) throws Exception;

	void commArtDelete(Map<String, Object> checkmap)throws Exception;

	void commImageDelete(Map<String, Object> checkmap)throws Exception;


	void commSideDelete(Map<String, Object> checkmap) throws Exception;

	void deleteReportData(Map<String, Object> checkmap) throws Exception;

	List selectYearList(Map<String, Object> map)  throws Exception;

	List<?> selectExcelListStatus(Map<String, Object> filter) throws Exception;

	int selectTotalCountStatus(Map<String, Object> filter) throws Exception;

	List<Map<String, Object>> excelSelectWork(String i) throws Exception;

	void deleteReprtDetail(Map model) throws Exception;

	void updateReport(Map model) throws Exception;

	List selectExcelReportList(Map<String, Object> map)  throws Exception;

	List getExcelReportsInfo(Map<String, Object> map) throws Exception;

	List getExcelReportsList(Map<String, Object> map) throws Exception;

	List selectExcelReportList_0(Map<String, Object> map) throws Exception;

	List getExcelReportsMonthView(Map<String, Object> map) throws Exception;

	List getExcelReportsMonthCoCnt(Map<String, Object> map) throws Exception;

	void commWorksReportModiUpdate(Map<String, Object> checkmap)throws Exception;

}
