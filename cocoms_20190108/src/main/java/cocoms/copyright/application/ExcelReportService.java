package cocoms.copyright.application;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cocoms.copyright.entity.MemberVO;

public interface ExcelReportService {

	//public boolean insertnew(ArrayList inList) throws Exception;
	Map<String, Object> insertnew(ArrayList inList) throws Exception;

	Map<String, Object> insertNodata(ArrayList<Map<String, Object>> uploadList) throws Exception;

	Map<String, Object> modiReport(ArrayList<Map<String, Object>> uploadList)throws Exception;

	List selectYearList(Map<String, Object> map) throws Exception;

	Map<String, Object> getExcelReports(Map<String, Object> filter) throws Exception;

	boolean deleteList(List<String> chArr) throws Exception;

	List selectExcelReportList(Map<String, Object> map) throws Exception;

	List getExcelReportsInfo(Map<String, Object> map) throws Exception;

	List getExcelReportsList(Map<String, Object> map) throws Exception;

	List selectExcelReportList_0(Map<String, Object> map) throws Exception;

	List getExcelReportsMonthView(Map<String, Object> map) throws Exception;

	List getExcelReportsMonthCoCnt(Map<String, Object> map) throws Exception;


	
}
