/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.application;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("rpMgmMapper")
public interface RpMgmMapper {

	List<?> selectList(Map<String, Object> filter) throws Exception;
	int selectTotalCount(Map<String, ?> filter) throws Exception;
	
	RpMgmVO select(RpMgmVO vo) throws Exception;
	RpMgmVO selectByMemberInfo(RpMgmVO vo) throws Exception;
	String selectPreStatCd(String memberSeqNo) throws Exception;
	
	void insert(RpMgmVO vo) throws Exception;

	void delete(RpMgmVO vo) throws Exception;
	String selectrpmgseqno(String memberSeqNo) throws Exception;
	RpMgmVO selectByMemberInfofile(String memberSeqNo)throws Exception;
	String selectSubStatCd(String memberSeqNo)throws Exception;
	String selectSubStatCd2(String memberSeqNo)throws Exception;
	String selectprerpmgseqno(String memberSeqNo)throws Exception;
	String selectCertinfoSeqNo(String memberSeqNo) throws Exception;
	String selectSubStatCd3(String memberSeqNo)throws Exception;
	String selectSubStatCd4(String memberSeqNo)throws Exception;
	String selectSubStatCd5(String memberSeqNo)throws Exception;

}
