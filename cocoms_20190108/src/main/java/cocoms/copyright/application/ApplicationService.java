/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.application;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import cocoms.copyright.entity.*;
import cocoms.copyright.web.form.ApplicationForm;

public interface ApplicationService {

	Map<String, Object> getAppliation(String memberSeqNo, String conditionCd) throws Exception;
	Map<String, Object> getmodiAppliation(String memberSeqNo, String conditionCd) throws Exception;
	Map<String, Object> getAppliationTemp(String memberSeqNo, String conditionCd) throws Exception;
	
	Map<String, Object> getChgHistory(String memberSeqNo, String statCd) throws Exception;
	
	List<Map<String, Object>> getChgHistories(String memberSeqNo, String statCd) throws Exception;
	
	Map<String, Object> getRpMgms(Map<String, Object> filter) throws Exception;
	List<RpMgmVO> getRpMgms(String memberSeqNo) throws Exception;
	
	void save(String memberSeqNo, String conditionCd, MemberVO member, List<WritingKindVO> writingKindList, List<PermKindVO> permKindList) throws Exception;
	void save(String memberSeqNo, String conditionCd, MemberVO member, List<WritingKindVO> writingKindList, List<PermKindVO> permKindList, List<String> chgCdList,String chgMemo) throws Exception;
	void saveTemp(String memberSeqNo, String conditionCd, MemberVO member, List<WritingKindVO> writingKindList, List<PermKindVO> permKindList) throws Exception;
	void savesubTemp(List<ChghFileVO>ChghFileList,String memberSeqNo, String conditionCd, MemberVO member,List<WritingKindVO> writingKindList, List<PermKindVO> permKindList, List<PermKindVO> prepermKindList,	List<WritingKindVO> prewritingKindList, List<ChgCdVO> chgCdList, String chgMemo) throws Exception;
	void Chargesave(String memberSeqNo, String conditionCd, String condetailcd, MemberVO member)throws Exception;
	Map<String, Object> getChgAppliation(String key, String conditionCd, String docType) throws Exception;

	
	
	

}
