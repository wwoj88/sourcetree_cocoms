/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.sms;

import java.io.*;
import java.util.*;

import javax.annotation.Resource;
import javax.mail.*;
import javax.mail.internet.*;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.sms.SmsMapper;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;

@Service("smsService")
public class SmsServiceImpl extends EgovAbstractServiceImpl implements SmsService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name = "smsHistoryMapper")
	private SmsHistoryMapper smsHistoryDAO;

	@Resource(name = "smsMemberMapper")
	private SmsMemberMapper smsMemberDAO;

	@Resource(name = "smsTemplateMapper")
	private SmsTemplateMapper smsTemplateDAO;

	@Resource(name = "smsMapper")
	private SmsMapper smsDAO;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Override
	public Map<String, Object> getSmsHistories(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);

		List<SmsHistoryVO> list = (List<SmsHistoryVO>) smsHistoryDAO.selectList(filter);
		int total = smsHistoryDAO.selectTotalCount(filter);

		for (SmsHistoryVO item : list) {

			filter = CommonUtils.pojoToMap(item);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);

			List<SmsMemberVO> msmberList = (List<SmsMemberVO>) smsMemberDAO.selectList(filter);
			LOGGER.info("member list count [{}]", msmberList.size());
			//int memberTotal = smsMemberDAO.selectTotalCount(filter);
			//LOGGER.info("member list count [{}] [{}]", msmberList.size(), memberTotal);

			item.setSmsMemberList(msmberList);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);

		return map;
	}

	@Override
	public Map<String, Object> getSmsMembers(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);

		List<?> list = smsMemberDAO.selectList(filter);
		int total = smsMemberDAO.selectTotalCount(filter);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);

		return map;
	}

	@Override
	public List<SmsTemplateVO> getSmsTemplates(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);

		return (List<SmsTemplateVO>) smsTemplateDAO.selectList(filter);
	}

	@Override
	public SmsTemplateVO getSmsTemplate(SmsTemplateVO vo) throws Exception {
		LOGGER.info("vo [{}]", vo);

		return smsTemplateDAO.select(vo);
	}

	@Override
	public SmsTemplateVO getSmsTemplate(String code, String smsType) throws Exception {
		LOGGER.info("code [{}] sms type [{}]", code, smsType);

		SmsTemplateVO vo = new SmsTemplateVO();
		vo.setDetailCode(code);
		vo.setSmsType(smsType);

		return smsTemplateDAO.select(vo);
	}

	@Override
	public SmsHistoryVO getSmsHistory(SmsHistoryVO vo) throws Exception {
		LOGGER.info(vo.toString());

		SmsHistoryVO data = smsHistoryDAO.select(vo);

		Map<String, Object> filter = CommonUtils.pojoToMap(data);
		filter.put("firstRecordIndex", 0);
		filter.put("lastRecordIndex", 9999);
		List<SmsMemberVO> msmberList = (List<SmsMemberVO>) smsMemberDAO.selectList(filter);
		data.setSmsMemberList(msmberList);

		return data;
	}

	@Override
	public SmsHistoryVO getSmsHistory(String smsSeqNo) throws Exception {
		LOGGER.info("sms seq no [{}]", smsSeqNo);

		return getSmsHistory(new SmsHistoryVO(smsSeqNo));
	}

	@Override
	public String saveSmsHistory(SmsHistoryVO vo) throws Exception {
		LOGGER.info("sms history [{}]", vo);

		// insert
		if (StringUtils.isEmpty(vo.getSmsSeqNo())) {

			smsHistoryDAO.insert(vo);

			for (SmsMemberVO member : vo.getSmsMemberList()) {

				member.setSmsSeqNo(vo.getSmsSeqNo());

				smsMemberDAO.insert(member);
			}
		}
		// update
		else {

			smsHistoryDAO.update(vo);
		}

		return vo.getSmsSeqNo();
	}

	@Override
	public void saveSmsTemplate(SmsTemplateVO vo) throws Exception {
		LOGGER.info(vo.toString());

		smsTemplateDAO.update(vo);
	}

	@Override
	public void removeSmsHistory(String smsHistorySeqNo) throws Exception {
		LOGGER.info("boardSeqNo [{}]", smsHistorySeqNo);

		smsHistoryDAO.delete(new SmsHistoryVO(smsHistorySeqNo));
	}

	@Override
	public void sendSms(String to, String content, String from) throws Exception {
		/*
		 * LOGGER.info("to [{}], content [{}], from [{}]", to, content, from);
		 */
		LOGGER.info("content length [{}], [{}]", content.length(), content.getBytes().length);

		//		if (content.length() > 160) {	// db size
		if (content.length() > 90) {
			throw new Exception("메시지내용이 너무 큽니다.");
		}

		smsDAO.insert(new SmsVO(to, content, from));
	}

	@Override
	public void sendSms(String to, String content, String from, String date) throws Exception {
		/*
		 * LOGGER.info("to [{}], content [{}], from [{}]", to, content, from);
		 */
		LOGGER.info("content length [{}], [{}]", content.length(), content.getBytes().length);

		SmsVO sms = new SmsVO(to, content, from);
		sms.setSendDate(date);

		if (content.length() > 160) {
			throw new Exception("메시지내용이 너무 큽니다.");
		}

		smsDAO.insert(sms);
	}

	@Override
	public void sendMail(String to, String toName, String subject, String content, String host, String from, String fromName) throws Exception {

		content = content.replaceAll("\n", "<br/>");

		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		Session session2 = Session.getInstance(props);
		try {
			MimeMessage msg = new MimeMessage(session2);

			if (fromName != null && !fromName.equals("")) {
				msg.setFrom(new InternetAddress(from, MimeUtility.encodeText(fromName, "EUC-KR", "B")));
			} else {
				msg.setFrom(new InternetAddress(from));
			}

			if (toName != null && !toName.equals("")) {
				msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to, MimeUtility.encodeText(toName, "EUC-KR", "B")));
			} else {
				msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
			}
			msg.setSubject(MimeUtility.encodeText(subject, "EUC-KR", "B"));
			msg.setSentDate(new Date());
			msg.setContent(content, "text/html; charset=EUC-KR");
			Transport.send(msg);
		} catch (MessagingException mex) {
			mex.printStackTrace();
		} catch (UnsupportedEncodingException ux) {
			ux.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void noti(String code, LoginVO login, MemberVO member) throws Exception {
		LOGGER.info("login [{}], member [{}]", login, member);

		if ("Y".equals(member.getSmsAgree()) && StringUtils.isNotEmpty(member.getSms())) {

			SmsTemplateVO vo = getSmsTemplate(code, "1");
			LOGGER.info("vo [{}]", vo);
			String content = vo.getContent();
			content = content.replaceAll("user_id", login.getLoginId());
			content = content.replaceAll("user_name", member.getCeoName());
			LOGGER.info("content [{}]", content);

			String smsSender = propertiesService.getString("smsSender");

			if ("Y".equals(vo.getUseYn())) {
				//노티 주석 메일 문자 테스트시 문자
				sendSms(member.getSms(), content, smsSender);
			}
		}

		if ("Y".equals(member.getSmsAgree()) && StringUtils.isNotEmpty(member.getCeoEmail())) {

			SmsTemplateVO vo = getSmsTemplate(code, "2");
			LOGGER.info("vo [{}]", vo);
			String content = vo.getContent();
			content = content.replaceAll("user_id", login.getLoginId());
			content = content.replaceAll("user_name", member.getCeoName());
			LOGGER.info("content [{}]", content);

			String toName = StringUtils.isNotEmpty(member.getCeoName()) ? member.getCeoName() : member.getCeoEmail();
			String host = propertiesService.getString("smtpHost");
			String from = propertiesService.getString("smtpEmail");
			String fromName = propertiesService.getString("smtpSender");

			if ("Y".equals(vo.getUseYn())) {
				sendMail(member.getCeoEmail(), toName, vo.getSubject(), content, host, from, fromName);
			}
		}

	}
}
