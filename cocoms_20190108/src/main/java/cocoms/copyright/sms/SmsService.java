/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.sms;

import java.util.*;

import cocoms.copyright.entity.*;

public interface SmsService {

	Map<String, Object> getSmsHistories(Map<String, Object> filter) throws Exception;
	Map<String, Object> getSmsMembers(Map<String, Object> filter) throws Exception;
	List<SmsTemplateVO> getSmsTemplates(Map<String, Object> filter) throws Exception;
	
	SmsHistoryVO getSmsHistory(SmsHistoryVO vo) throws Exception;
	SmsHistoryVO getSmsHistory(String smsSeqNo) throws Exception;
	SmsTemplateVO getSmsTemplate(SmsTemplateVO vo) throws Exception;
	SmsTemplateVO getSmsTemplate(String code, String smsType) throws Exception;
	
	String saveSmsHistory(SmsHistoryVO vo) throws Exception;
	void saveSmsTemplate(SmsTemplateVO vo) throws Exception;

	void removeSmsHistory(String smsHistorySeqNo) throws Exception;
	
	////
	
	void sendSms(String to, String content, String from) throws Exception;
	void sendSms(String to, String content, String from, String date) throws Exception;
	void sendMail(String to, String toName, String subject, String content, String host, String from, String fromName) throws Exception;
	
	void noti(String code, LoginVO login, MemberVO member) throws Exception;
	
}
