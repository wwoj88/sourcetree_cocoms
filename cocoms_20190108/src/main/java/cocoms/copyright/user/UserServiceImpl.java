/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.user;

import java.util.*;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import cocoms.copyright.application.ChgHistoryMapper;
import cocoms.copyright.application.PermKindMapper;
import cocoms.copyright.application.RpMgmMapper;
import cocoms.copyright.application.WritingKindMapper;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.*;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Service("userService")
public class UserServiceImpl extends EgovAbstractServiceImpl implements UserService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name = "loginMapper")
	private LoginMapper loginDAO;

	@Resource(name = "memberMapper")
	private MemberMapper memberDAO;

	@Resource(name = "rpMgmMapper")
	private RpMgmMapper rpMgmDAO;

	@Resource(name = "chgHistoryMapper")
	private ChgHistoryMapper chgHistoryDAO;

	@Resource(name = "certMapper")
	private CertMapper certDAO;

	@Resource(name = "writingKindMapper")
	private WritingKindMapper writingKindDAO;

	@Resource(name = "permKindMapper")
	private PermKindMapper permKindDAO;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Override
	public Map<String, Object> getLogins(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);

		List<?> list = loginDAO.selectList(filter);
		int total = loginDAO.selectTotalCount(filter);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);

		return map;
	}

	@Override
	public Map<String, Object> getLogins(String regNo) throws Exception {
		LOGGER.info("regNo [{}]", regNo);

		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("firstRecordIndex", 0);
		filter.put("lastRecordIndex", 9999);
		filter.put("delGubun", "N");
		if (StringUtils.isNotEmpty(regNo)) {
			new XCrypto(propertiesService.getString("xcry.path"), propertiesService.getBoolean("xcry.use"));
			filter.put("regNo", XCrypto.encPatternReg(regNo));
		}

		List<?> list = loginDAO.selectList(filter);
		int total = loginDAO.selectTotalCount(filter);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);

		return map;
	}

	@Override
	public Map<String, Object> getMembers(Map<String, Object> filter) throws Exception {
		//LOGGER.info("filter [{}]", filter);

		List<?> list = memberDAO.selectList(filter);
		int total = memberDAO.selectTotalCount(filter);
		String minDate = memberDAO.selectMinDate();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);
		map.put("minDate", minDate);

		return map;
	}

	@Override
	public LoginVO getUser(LoginVO vo) throws Exception {
		LOGGER.info(vo.toString());

		new XCrypto(propertiesService.getString("xcry.path"), propertiesService.getBoolean("xcry.use"));

		// password enc
		String pwd = vo.getPwd();
		if (StringUtils.isNotEmpty(pwd)) {
			pwd = XCrypto.encHashPw(pwd);
			vo.setPwd(pwd);
		}

		// reg no enc
		String regNo = vo.getRegNo();
		if (StringUtils.isNotEmpty(regNo)) {
			regNo = XCrypto.encPatternReg(regNo);
			vo.setRegNo(regNo);
		}
		LOGGER.info("enc reg no [{}]", vo.getRegNo());

		return loginDAO.select(vo);
	}

	@Override
	public LoginVO getUserByLoginId(String loginId) throws Exception {
		LOGGER.info("login id [{}]", loginId);

		return getUser(new LoginVO(loginId));
	}

	@Override
	public LoginVO getUserByLoginId(String loginId, String certSn) throws Exception {
		LOGGER.info("login id [{}]", loginId);

		LoginVO vo = new LoginVO();
		vo.setLoginId(loginId);

		LoginVO login = getUser(vo);
		if (login == null)
			return null;

		CertVO params = new CertVO();
		params.setCertSn(certSn);
		params.setLoginId(login.getLoginId());

		CertVO cert = certDAO.select(params);
		if (cert == null)
			return null;

		return login;
	}

	@Override
	public List<LoginVO> getUserByRegNo(String regNo, String certSn) throws Exception {
		LOGGER.info("reg no [{}], cert sn [{}]", regNo, certSn);

		List<LoginVO> list = new ArrayList<LoginVO>();

		LoginVO vo = new LoginVO();
		vo.setRegNo(regNo);

		/*
		 * check login info
		 */
		//		LoginVO login = getUser(vo);
		//		LOGGER.info("login [{}]", login);
		//		if (login == null) {
		//			return null;
		//		}

		/*
		 * check cert
		 */
		//		CertVO params = new CertVO();
		//		params.setCertSn(certSn);
		//		params.setLoginId(login.getLoginId());
		//		
		//		CertVO cert = certDAO.select(params);
		//		LOGGER.info("cert [{}]", cert);
		//		if (cert == null) {
		//			return null;
		//		}

		/*
		 * get login infos
		 */
		Map<String, Object> ret = getLogins(regNo);
		list = (List<LoginVO>) ret.get("list");
		LOGGER.info("list [{}]", list);

		return list != null ? list : new ArrayList<LoginVO>();
	}

	@Override
	public Map<String, Object> getUserInfo(String loginId) throws Exception {
		LOGGER.info("login id [{}]", loginId);

		LoginVO login = getUser(new LoginVO(loginId));

		MemberVO member = getMember(login.getMemberSeqNo());

		CertVO cert = getCert(new CertVO(login.getLoginId(), "1"));

		Map<String, Object> map = new HashMap<String, Object>();
		LOGGER.info("login [{}]", login);
		LOGGER.info("member [{}]", member);
		LOGGER.info("cert [{}]", cert);

		map.put("login", login);
		map.put("member", member);
		map.put("cert", cert);

		return map;
	}

	@Override
	public MemberVO getMember(MemberVO vo) throws Exception {
		LOGGER.info(vo.toString());

		return memberDAO.select(vo);
	}

	@Override
	public MemberVO getMember(String memberSeqNo) throws Exception {
		LOGGER.info("memberSeqNo [{}]", memberSeqNo);

		return memberDAO.select(new MemberVO(memberSeqNo));
	}

	@Override
	public MemberVO getMemberTemp(String memberSeqNo) throws Exception {
		LOGGER.info("memberSeqNo [{}]", memberSeqNo);

		return memberDAO.selectTemp(new MemberVO(memberSeqNo));
	}

	@Override
	public MemberVO getMemberTemp(String memberSeqNo, String conditionCd) throws Exception {
		LOGGER.info("memberSeqNo [{}]", memberSeqNo);
		MemberVO params = new MemberVO();
		params.setMemberSeqNo(memberSeqNo);
		params.setConditionCd(conditionCd);
		return memberDAO.selectTemp(params);
	}

	@Override
	public String getMemberSeqNo(MemberVO vo) throws Exception {
		LOGGER.info(vo.toString());

		new XCrypto(propertiesService.getString("xcry.path"), propertiesService.getBoolean("xcry.use"));

		String regNo = vo.getCeoRegNo();
		if (StringUtils.isNotEmpty(regNo) && regNo.length() <= 13) {
			regNo = XCrypto.encPatternReg(regNo);
			vo.setCeoRegNo(regNo);
		}

		String memberSeqNo = memberDAO.selectMemberSeqNo(vo);

		return memberSeqNo;
	}

	@Override
	public void saveLogin(LoginVO vo) throws Exception {
		LOGGER.info("login [{}]", vo);

		loginDAO.update(vo);
	}

	@Override
	public String saveUser(LoginVO login, MemberVO member, CertVO cert) throws Exception {
		LOGGER.info("login [{}], member [{}]", login, member);

		return saveUser(null, login, member, cert);
	}

	@Override
	public String saveUser(String memberSeqNo, LoginVO login, MemberVO member, CertVO cert) throws Exception {
		LOGGER.info("login [{}], member [{}]", login, member);

		// get memberSeqNo
		if (StringUtils.isEmpty(memberSeqNo)) {
			memberSeqNo = memberDAO.selectNewMemberSeqNo();
		}
		LOGGER.info("memberSeqNo [{}]", memberSeqNo);

		new XCrypto(propertiesService.getString("xcry.path"), propertiesService.getBoolean("xcry.use"));

		// save login
		if (login != null) {
			login.setMemberSeqNo(memberSeqNo);
			if (login.getRegNo().length() <= 13)
				login.setRegNo(XCrypto.encPatternReg(login.getRegNo()));
			if (StringUtils.isNotEmpty(login.getPwd()))
				login.setPwd(XCrypto.encHashPw(login.getPwd()));
			//			loginDAO.insert(login);
			loginDAO.upsert(login);

		}

		// save member
		if (member != null) {
			System.out.println("????????????? 1  : "+member.getCeoRegNo());
			System.out.println("????????????? 2  : "+member.getTerms1Agree());
			System.out.println("????????????? 3  : "+member.getTerms1AgreeDate());
			member.setMemberSeqNo(memberSeqNo);
			if (member.getCeoRegNo().length() <= 13)
				member.setCeoRegNo(XCrypto.encPatternReg(member.getCeoRegNo()));

			if (StringUtils.isNotEmpty(member.getTerms1Agree()) && "Y".equals(member.getTerms1Agree())) {
				member.setTerms2Agree("Y");
				member.setTerms3Agree("Y");
				
				if (StringUtils.isEmpty(member.getTerms1AgreeDate())) {
					member.setTerms1AgreeDate(DateUtils.getCurrDateNumber());
					member.setTerms2AgreeDate(DateUtils.getCurrDateNumber());
					member.setTerms3AgreeDate(DateUtils.getCurrDateNumber());
				}
			} else if (StringUtils.isNotEmpty(member.getTerms1AgreeDate())) {
				member.setTerms1Agree("Y");
				member.setTerms2Agree("Y");
				member.setTerms3Agree("Y");

				member.setTerms2AgreeDate(member.getTerms1AgreeDate());
				member.setTerms3AgreeDate(member.getTerms1AgreeDate());
			}

			//			memberDAO.insert(member);
			memberDAO.upsert(member);
		}

		// save cert
		if (cert != null) {

			// dn 2:폐기 업데이트
			CertVO params = new CertVO();
			params.setUserDn(cert.getUserDn());
			params.setCertStatus("2");
			certDAO.update(params);

			// insert cert
			if (cert.getRegNo().length() <= 13)
				cert.setRegNo(XCrypto.encPatternReg(cert.getRegNo()));
			if (cert.getCertificate().indexOf("BEGIN CERTIFICATE") < 0)
				cert.setCertificate("-----BEGIN CERTIFICATE-----\n" + cert.getCertificate() + "-----END CERTIFICATE-----\n");
			certDAO.insert(cert);
		}

		return memberSeqNo;
	}

	@Override
	public String saveAgree(String memberSeqNo) throws Exception {
		LOGGER.info("memberSeqNo [{}]", memberSeqNo);

		MemberVO member = new MemberVO();
		member.setTerms1Agree("Y");
		if (member != null) {

			member.setMemberSeqNo(memberSeqNo);

			if (StringUtils.isNotEmpty(member.getTerms1Agree()) && "Y".equals(member.getTerms1Agree())) {
				member.setTerms2Agree("Y");
				member.setTerms3Agree("Y");

				if (StringUtils.isEmpty(member.getTerms1AgreeDate())) {
					member.setTerms1AgreeDate(DateUtils.getCurrDateNumber());
					member.setTerms2AgreeDate(DateUtils.getCurrDateNumber());
					member.setTerms3AgreeDate(DateUtils.getCurrDateNumber());
				}
			} else if (StringUtils.isNotEmpty(member.getTerms1AgreeDate())) {
				member.setTerms1Agree("Y");
				member.setTerms2Agree("Y");
				member.setTerms3Agree("Y");

				member.setTerms2AgreeDate(member.getTerms1AgreeDate());
				member.setTerms3AgreeDate(member.getTerms1AgreeDate());
			}
			//			memberDAO.insert(member);
			memberDAO.update(member);
		}
		return memberSeqNo;
	}

	@Override
	public void saveStatCd(MemberVO member) throws Exception {

		String memberSeqNo = member.getMemberSeqNo();

		/* insert 허가/신고 관리 */
		RpMgmVO rpMgm = new RpMgmVO();
		BeanUtils.copyProperties(member, rpMgm);

		rpMgm.setRegDate(DateUtils.getCurrDateNumber());
		member.setAppRegDate(DateUtils.getCurrDateNumber());

		rpMgmDAO.insert(rpMgm);
		String rpMgmSeqNo = rpMgm.getRpMgmSeqNo();

		//
		memberDAO.update(member);
		memberDAO.deleteTemp(member);
		// 
		member.setAppRegDate(DateUtils.getCurrDateNumber());

		if ("2".equals(member.getStatCd()) || "4".equals(member.getStatCd())) {
			if ("2".equals(member.getStatCd())) {
				memberDAO.updateAppInfo(member);
			} else {

				memberDAO.updateAppInfo2(member);
			}

			String rpmgmseqno2 = rpMgmDAO.selectprerpmgseqno(memberSeqNo);

			String chgHistoryseqno = chgHistoryDAO.selectChghistoryseq(rpmgmseqno2);

			ChgHistoryVO params = new ChgHistoryVO();
			params.setChgHistorySeqNo(chgHistoryseqno);
			params.setRpMgmSeqNo(rpMgmSeqNo);
			params.setStatCd(member.getStatCd());

			chgHistoryDAO.upsert(CommonUtils.pojoToMap(params));

			// 1,3 의 CHGPERMKIND , WRITINGKIND 
			List<ChgWritingKindVO> writingKindList = writingKindDAO.selectListChg(chgHistoryseqno);
			List<ChgPermKindVO> permKindList = permKindDAO.selectListChg(chgHistoryseqno);

			String chghistoryseqnoNew = params.getChgHistorySeqNo();

			memberDAO.updateDetail(memberSeqNo);

			/* delete perm kind */
			PermKindVO permKindVO = new PermKindVO();
			permKindVO.setMemberSeqNo(memberSeqNo);
			permKindDAO.delete(permKindVO);
			permKindDAO.deleteChgTemp(permKindVO);
			permKindDAO.deleteTemp(permKindVO);

			WritingKindVO writingKindVO = new WritingKindVO();
			writingKindVO.setMemberSeqNo(memberSeqNo);
			writingKindDAO.delete(writingKindVO);
			writingKindDAO.deleteChgTemp(writingKindVO);
			writingKindDAO.deleteTemp(writingKindVO);

			for (ChgWritingKindVO item : writingKindList) {
				LOGGER.info("ChgWritingKindVO kind [{}]", item);

				WritingKindVO WritingKind = new WritingKindVO();

				WritingKind.setMemberSeqNo(memberSeqNo);
				WritingKind.setWritingKind(item.getWritingKind());
				WritingKind.setWritingEtc(item.getWritingEtc());

				writingKindDAO.insert(WritingKind);

			}

			for (ChgPermKindVO item : permKindList) {
				LOGGER.info("ChgPermKindVO kind [{}]", item);

				String chgwritingkindseqno = item.getChgwritingkindseqno();
				String writingKind = permKindDAO.selectWritingKindChg(chgwritingkindseqno);
				PermKindVO permKind = new PermKindVO();

				permKind.setMemberSeqNo(memberSeqNo);
				permKind.setWritingKind(writingKind);
				permKind.setPermEtc(item.getPermEtc());
				permKind.setPermKind(item.getPermKind());
				permKindDAO.insert(permKind);

			}

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("chgHistoryseqno", chgHistoryseqno);
			map.put("chghistoryseqnoNew", chghistoryseqnoNew);

			/*
			 * chgHistoryDAO.insertChgWritingList(map);
			 * chgHistoryDAO.insertChgPermList(map);
			 * chgHistoryDAO.insertChgCdList(map);
			 */
		}

		//
		String preStatCd = rpMgmDAO.selectPreStatCd(memberSeqNo);

		// 정지해제의 경우 상태 완료처리
		if ("59".equals(member.getStatCd())) {
			if ("19".equals(preStatCd)) {
				member.setStatCd("2");
			}
			if ("39".equals(preStatCd)) {
				member.setStatCd("4");
			}

			//
			memberDAO.update(member);
		}

		// 신고취소, 최소신청 반려 시 권리정보를 삭제
		if ("6".equals(member.getStatCd()) || ("9".equals(member.getStatCd()) && "19".equals(rpMgmSeqNo))) {

			/* delete writing kind */
			WritingKindVO writingKindVO = new WritingKindVO();
			writingKindVO.setMemberSeqNo(memberSeqNo);
			writingKindDAO.delete(writingKindVO);

			/* delete perm kind */
			PermKindVO permKindVO = new PermKindVO();
			permKindVO.setMemberSeqNo(memberSeqNo);
			permKindDAO.delete(permKindVO);
		}
	}

	@Override
	public String getPreStatCd(String memberSeqNo) throws Exception {

		return rpMgmDAO.selectPreStatCd(memberSeqNo);
	}

	@Override
	public boolean existsId(String loginId) throws Exception {

		LoginVO loginVO = new LoginVO();
		loginVO.setLoginId(loginId);

		return loginDAO.select(loginVO) == null ? false : true;
	}

	@Override
	public CertVO getCert(CertVO vo) throws Exception {
		LOGGER.info(vo.toString());

		return certDAO.select(vo);
	}

	@Override
	public CertVO getCert(String loginId) throws Exception {
		LOGGER.info("login id [{}]", loginId);

		return certDAO.select(new CertVO(loginId, "1"));
	}

	@Override
	public void saveCert(CertVO cert) throws Exception {
		LOGGER.info("cert [{}]", cert);

		new XCrypto(propertiesService.getString("xcry.path"), propertiesService.getBoolean("xcry.use"));

		// login id 2:폐기 업데이트
		cert.setCertStatus("2");
		certDAO.update(cert);

		// insert cert
		cert.setCertStatus("1");
		if (cert.getRegNo().length() <= 13)
			cert.setRegNo(XCrypto.encPatternReg(cert.getRegNo()));
		certDAO.insert(cert);
	}

	@Override
	public void removeUser(String loginId) throws Exception {

		LoginVO login = new LoginVO(loginId);
		login.setDelGubun("Y");
		login.setRegNo(null);
		loginDAO.update(login);
	}

}
