/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.user;

import java.util.*;

import cocoms.copyright.entity.*;

public interface UserService {

	Map<String, Object> getLogins(Map<String, Object> filter) throws Exception;
	Map<String, Object> getLogins(String regNo) throws Exception;
	Map<String, Object> getMembers(Map<String, Object> filter) throws Exception;
	
	LoginVO getUser(LoginVO vo) throws Exception;
	LoginVO getUserByLoginId(String loginId) throws Exception;
	LoginVO getUserByLoginId(String loginId, String certSn) throws Exception;
	List<LoginVO> getUserByRegNo(String regNo, String certSn) throws Exception;
	
	Map<String, Object> getUserInfo(String loginId) throws Exception;

	MemberVO getMember(MemberVO vo) throws Exception;
	MemberVO getMember(String memberSeqNo) throws Exception;
	String getMemberSeqNo(MemberVO vo) throws Exception;
	String getPreStatCd(String memberSeqNo) throws Exception;
	
	boolean existsId(String loginId) throws Exception;

	CertVO getCert(CertVO vo) throws Exception;
	CertVO getCert(String loginId) throws Exception;

	void saveLogin(LoginVO vo) throws Exception;

	String saveUser(LoginVO login, MemberVO member, CertVO cert) throws Exception;
	String saveUser(String memberSeqNo, LoginVO login, MemberVO member, CertVO cert) throws Exception;
	String saveAgree(String memberSeqNo) throws Exception;
	void saveStatCd(MemberVO member) throws Exception;
	
	void saveCert(CertVO vo) throws Exception;

	void removeUser(String loginId) throws Exception;
	MemberVO getMemberTemp(String memberSeqNo) throws Exception;
	MemberVO getMemberTemp(String memberSeqNo, String conditionCd)throws Exception;
}
