/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.log;

import java.util.*;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.entity.*;

@Service("actionLogService")
public class ActionLogServiceImpl extends EgovAbstractServiceImpl implements ActionLogService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name = "actionLogMapper")
	private ActionLogMapper actionLogDAO;

	@Override
	public Map<String, Object> getLogs(Map<String, Object> filter) throws Exception {
		LOGGER.info("filter [{}]", filter);

		List<ActionLogVO> list = (List<ActionLogVO>) actionLogDAO.selectList(filter);
		int total = actionLogDAO.selectTotalCount(filter);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);

		return map;
	}

	@Override
	public void saveLog(ActionLogVO vo) throws Exception {
		LOGGER.info(vo.toString());

		actionLogDAO.insert(vo);
	}

	@Override
	public void saveLog(HttpServletRequest request, String action) throws Exception {

		String loginId = CommonUtils.getLoginId(request);
		String userIp = request.getRemoteAddr();
		String url = request.getRequestURI();

		ActionLogVO vo = new ActionLogVO();
		vo.setLoginId(loginId);
		vo.setUserIp(userIp);
		vo.setAction(action);
		vo.setUrl(url);

		if (StringUtils.isNotEmpty(loginId)) {

			saveLog(vo);
		}
	}

	@Override
	public void hitDownCount(String boardSeqNo) throws Exception {
		// TODO Auto-generated method stub
		actionLogDAO.hitdownCount(boardSeqNo);
		
	}

}
