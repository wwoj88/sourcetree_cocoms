/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.cert;

import java.util.*;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.XCrypto;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.*;

@Service("certInfoService")
public class CertInfoServiceImpl extends EgovAbstractServiceImpl implements CertInfoService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name="certInfoMapper")
	private CertInfoMapper certInfoDAO;

	@Override
	public Map<String, Object> getCertInfos(Map<String, Object> filter) throws Exception {
		//LOGGER.info("filter [{}]", filter);

		List<CertInfoVO> list = (List<CertInfoVO>) certInfoDAO.selectList(filter);
		int total = certInfoDAO.selectTotalCount(filter);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);
		
		return map;
	}
	
	@Override
	public CertInfoVO getCertInfo(CertInfoVO vo) throws Exception {
		LOGGER.info(vo.toString());
		
		return certInfoDAO.select(vo);
	}

	@Override
	public String saveCertInfo(CertInfoVO vo) throws Exception {
		LOGGER.info(vo.toString());
		
		if (StringUtils.isEmpty(vo.getCertInfoSeqNo())) {

			if (vo.getSource() != null 
					&& vo.getSource().indexOf("BEGIN CERTIFICATE") < 0) {
				vo.setSource("-----BEGIN CERTIFICATE-----\n"+vo.getSource()+"-----END CERTIFICATE-----\n");
			}

			certInfoDAO.insert(vo);	
		}
		else {
			
			certInfoDAO.update(vo);
		}
		
		return vo.getCertInfoSeqNo();
	}

}
