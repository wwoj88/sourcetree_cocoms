/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.cert;

import java.util.*;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.XCrypto;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.*;

@Service("certProbeService")
public class CertProbeServiceImpl extends EgovAbstractServiceImpl implements CertProbeService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name="certProbeMapper")
	private CertProbeMapper certProbeDAO;

	@Override
	public CertProbeXmlVO getCertProbe(CertProbeXmlVO vo) throws Exception {
		LOGGER.info(vo.toString());
		
		return certProbeDAO.select(vo);
	}

	@Override
	public boolean checkCertProbe(String printId) throws Exception {
		LOGGER.info("print id [{}]", printId);
		
		CertProbeXmlVO certProbe = certProbeDAO.select(new CertProbeXmlVO(printId));
		LOGGER.info("certProbe [{}]", certProbe);

		return certProbe != null && StringUtils.isNotEmpty(certProbe.getPrintId());
	}

}
