/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.member;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("memberMapper")
public interface MemberMapper {

	List<?> selectList(Map<String, ?> filter) throws Exception;
	int selectTotalCount(Map<String, ?> filter) throws Exception;
	String selectMinDate() throws Exception;
	
	MemberVO select(MemberVO vo) throws Exception;
	MemberVO selectByPk(String memberSeqNo) throws Exception;
	MemberVO selectTemp(MemberVO vo) throws Exception;
	Integer selectTempcount(MemberVO vo) throws Exception;
	String selectMemberSeqNo(MemberVO vo) throws Exception;
	
	void insert(MemberVO vo) throws Exception;
	void insertTemp(MemberVO vo) throws Exception;
	String selectNewMemberSeqNo() throws Exception;

	void update(MemberVO vo) throws Exception;
	void upsert(MemberVO vo) throws Exception;
	void updateAppInfo(MemberVO vo) throws Exception;
	void updateAppInfo2(MemberVO vo) throws Exception;
	void ceoUpdate(MemberVO vo) throws Exception;

	void delete(MemberVO vo) throws Exception;
	void deleteTemp(MemberVO vo) throws Exception;
	void ceoUpdateTemp(MemberVO vo)  throws Exception;
	String selectStatCd(String memberSeqNo) throws Exception;
	String selectConditioncdTmp(String memberSeqNo) throws Exception;
	String selectConditioncd(String memberSeqNo)  throws Exception;
	String selectConDetailCd(String memberSeqNo) throws Exception;
	String selectConDetailCdTmp(String memberSeqNo) throws Exception;
	void updateinfo(MemberVO vo) throws Exception;
	MemberVO selectByPkTmp(String memberSeqNo)throws Exception;
	
	void updateDetail(String memberSeqNo)throws Exception;
	void updateAgree(MemberVO vo)throws Exception;
	
}
