/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.member;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("loginMapper")
public interface LoginMapper {

	List<?> selectList(Map<String, Object> filter) throws Exception;
	int selectTotalCount(Map<String, ?> filter) throws Exception;

	LoginVO select(LoginVO vo) throws Exception;
	
	void insert(LoginVO vo) throws Exception;
	
	void update(LoginVO vo) throws Exception;
	void upsert(LoginVO vo) throws Exception;

	void delete(LoginVO vo) throws Exception;

}
