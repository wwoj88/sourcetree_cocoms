/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.code;

import java.util.*;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.*;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Service("codeService")
public class CodeServiceImpl extends EgovAbstractServiceImpl implements CodeService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Resource(name="commonCodeMapper")
	private CommonCodeMapper commonCodeDAO;

	@Resource(name="zipCodeMapper")
	private ZipCodeMapper zipCodeDAO;

	@Resource(name="certMapper")
	private CertMapper certDAO;

	@Override
	public List<CommonCodeVO> getCodes(String codeGubun) throws Exception {

		CommonCodeVO params = new CommonCodeVO();
		params.setCodeGubun(codeGubun);
		
		List<?> list = commonCodeDAO.selectList(CommonUtils.pojoToMap(params));
		
		return (List<CommonCodeVO>) list;
	}

	@Override
	public CommonCodeVO getCode(String codeGubun, String codeValue) throws Exception {

		CommonCodeVO params = new CommonCodeVO();
		params.setCodeGubun(codeGubun);
		params.setCodeValue(codeValue);
		
		return commonCodeDAO.select(params);
	}

	@Override
	public List<ZipCodeVO> getZipCodes(String sido) throws Exception {

		ZipCodeVO params = new ZipCodeVO();
		params.setSido(sido);
		
		if (StringUtils.isEmpty(sido)) {
			return (List<ZipCodeVO>) zipCodeDAO.selectSidoList(null);
		}
		
		return (List<ZipCodeVO>) zipCodeDAO.selectGugunList(CommonUtils.pojoToMap(params));
	}

}
