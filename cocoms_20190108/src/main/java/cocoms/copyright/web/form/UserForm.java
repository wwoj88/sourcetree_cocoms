package cocoms.copyright.web.form;

import java.util.*;

import cocoms.copyright.entity.*;

public class UserForm extends MemberVO {

	private static final long serialVersionUID = 1L;

	/* login */
	private String loginId     = "";	//
	private String memberSeqNo = "";	//
	private String pwd         = "";	//
	private String regNo       = "";	//
	private String coGubun     = "";	//

	/* cert */
	private String userDn        = "";	//
	private String certSn        = "";	//
//	private String loginId       = "";	//
	private String certType      = "";	// 1: 개인, 2: 법인
	private String certificate   = "";	//
	private String certStatus    = "";	// 1: 유효, 2: 폐기, 3: 정지
//	private String regNo         = "";	//
	private String certStartDate = "";	//
	private String certEndDate   = "";	//
	private String regDate       = "";	//
	
	/* form */
	private String ceoRegNo1 = ""; 			// 
	private String ceoRegNo2 = ""; 			// 
	
	private String agree1 = ""; 			// 서비스 이용약관
	private String agree1b = ""; 			// 권리자찾기정보시스템 이용약관
	private String agree2 = ""; 			// 개인정보 수집 및 이용동의
	private String agree3 = ""; 			// 저작권위탁관리업시스템 정보 및 안내자료 SMS / 메일 수신 (선택)
	
	private String coNo1 = ""; 				// 사업자번호
	private String coNo2 = ""; 				// 사업자번호
	private String coNo3 = ""; 				// 사업자번호
	
	private String trustAddr1 = "";			//
	private String trustAddr2 = "";			//
	private String trustAddr3 = "";			//
	
	private String trustTel1 = "";			//
	private String trustTel2 = "";			//
	private String trustTel3 = "";			//

	private String trustFax1 = "";			//
	private String trustFax2 = "";			//
	private String trustFax3 = "";			//

	private String ceoAddr1 = "";			//
	private String ceoAddr2 = "";			//
	private String ceoAddr3 = "";			//
	
	private String ceoMobile1 = "";
	private String ceoMobile2 = "";
	private String ceoMobile3 = "";

	private String ceoEmail1 = ""; 			// 전자우편
	private String ceoEmail2 = ""; 			// 전자우편

	private String smsAgree = ""; 			// SMS수신동의
	
	private String newPwd = ""; 			//
	
	private String bubNo1 = "";	//
	private String bubNo2 = "";	//

	private List<String> writingKind = new ArrayList<String>();
	
	private List<String> permkind1 = new ArrayList<String>();		// 어문 저작물
	private List<String> permkind2 = new ArrayList<String>();		// 음악 저작물
	private List<String> permkind3 = new ArrayList<String>();		// 영상 저작물
	private List<String> permkind21 = new ArrayList<String>();		// 연극 저작물
	private List<String> permkind4 = new ArrayList<String>();		// 미술 저작물
	private List<String> permkind5 = new ArrayList<String>();		// 사진 저작물
	private List<String> permkind22 = new ArrayList<String>();		// 건축 저작물
	private List<String> permkind23 = new ArrayList<String>();		// 도형 저작물
	private List<String> permkind24 = new ArrayList<String>();		// 컴퓨터프로그램 저작물
	private List<String> permkind0 = new ArrayList<String>();		// 기타 저작물
	
	private List<String> permkind10 = new ArrayList<String>();		// 실연자의 권리
	private List<String> permkind11 = new ArrayList<String>();		// 음반제작자의 권리
	private List<String> permkind12 = new ArrayList<String>();		// 출판권

	/*
	 * Special Getters
	 */
	public String getRegDateStr() {
		
		if (regDate != null && regDate.length() == 8) {
			return regDate.substring(0,  4)+"-"+regDate.substring(4, 6)+"-"+regDate.substring(6, 8);
		}
		
		return regDate;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getTrustTel1() {
		return trustTel1;
	}
	public void setTrustTel1(String trustTel1) {
		this.trustTel1 = trustTel1;
	}
	public String getAgree1b() {
		return agree1b;
	}
	public void setAgree1b(String agree1b) {
		this.agree1b = agree1b;
	}
	public String getBubNo1() {
		return bubNo1;
	}
	public void setBubNo1(String bubNo1) {
		this.bubNo1 = bubNo1;
	}
	public String getBubNo2() {
		return bubNo2;
	}
	public void setBubNo2(String bubNo2) {
		this.bubNo2 = bubNo2;
	}
	public String getNewPwd() {
		return newPwd;
	}
	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}
	public String getUserDn() {
		return userDn;
	}
	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}
	public String getCertSn() {
		return certSn;
	}
	public void setCertSn(String certSn) {
		this.certSn = certSn;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public String getCertStatus() {
		return certStatus;
	}
	public void setCertStatus(String certStatus) {
		this.certStatus = certStatus;
	}
	public String getCertStartDate() {
		return certStartDate;
	}
	public void setCertStartDate(String certStartDate) {
		this.certStartDate = certStartDate;
	}
	public String getCertEndDate() {
		return certEndDate;
	}
	public void setCertEndDate(String certEndDate) {
		this.certEndDate = certEndDate;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getAgree1() {
		return agree1;
	}
	public void setAgree1(String agree1) {
		this.agree1 = agree1;
	}
	public String getAgree2() {
		return agree2;
	}
	public void setAgree2(String agree2) {
		this.agree2 = agree2;
	}
	public String getAgree3() {
		return agree3;
	}
	public void setAgree3(String agree3) {
		this.agree3 = agree3;
	}
	public String getCeoRegNo1() {
		return ceoRegNo1;
	}
	public void setCeoRegNo1(String ceoRegNo1) {
		this.ceoRegNo1 = ceoRegNo1;
	}
	public String getCeoRegNo2() {
		return ceoRegNo2;
	}
	public void setCeoRegNo2(String ceoRegNo2) {
		this.ceoRegNo2 = ceoRegNo2;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getCoGubun() {
		return coGubun;
	}
	public void setCoGubun(String coGubun) {
		this.coGubun = coGubun;
	}
	public List<String> getPermkind1() {
		return permkind1;
	}
	public void setPermkind1(List<String> permkind1) {
		this.permkind1 = permkind1;
	}
	public List<String> getPermkind2() {
		return permkind2;
	}
	public void setPermkind2(List<String> permkind2) {
		this.permkind2 = permkind2;
	}
	public List<String> getPermkind3() {
		return permkind3;
	}
	public void setPermkind3(List<String> permkind3) {
		this.permkind3 = permkind3;
	}
	public List<String> getPermkind21() {
		return permkind21;
	}
	public void setPermkind21(List<String> permkind21) {
		this.permkind21 = permkind21;
	}
	public List<String> getPermkind4() {
		return permkind4;
	}
	public void setPermkind4(List<String> permkind4) {
		this.permkind4 = permkind4;
	}
	public List<String> getPermkind5() {
		return permkind5;
	}
	public void setPermkind5(List<String> permkind5) {
		this.permkind5 = permkind5;
	}
	public List<String> getPermkind22() {
		return permkind22;
	}
	public void setPermkind22(List<String> permkind22) {
		this.permkind22 = permkind22;
	}
	public List<String> getPermkind23() {
		return permkind23;
	}
	public void setPermkind23(List<String> permkind23) {
		this.permkind23 = permkind23;
	}
	public List<String> getPermkind24() {
		return permkind24;
	}
	public void setPermkind24(List<String> permkind24) {
		this.permkind24 = permkind24;
	}
	public List<String> getPermkind0() {
		return permkind0;
	}
	public void setPermkind0(List<String> permkind0) {
		this.permkind0 = permkind0;
	}
	public List<String> getPermkind10() {
		return permkind10;
	}
	public void setPermkind10(List<String> permkind10) {
		this.permkind10 = permkind10;
	}
	public List<String> getPermkind11() {
		return permkind11;
	}
	public void setPermkind11(List<String> permkind11) {
		this.permkind11 = permkind11;
	}
	public List<String> getPermkind12() {
		return permkind12;
	}
	public void setPermkind12(List<String> permkind12) {
		this.permkind12 = permkind12;
	}
	public List<String> getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(List<String> writingKind) {
		this.writingKind = writingKind;
	}
	public String getTrustTel2() {
		return trustTel2;
	}
	public void setTrustTel2(String trustTel2) {
		this.trustTel2 = trustTel2;
	}
	public String getTrustTel3() {
		return trustTel3;
	}
	public void setTrustTel3(String trustTel3) {
		this.trustTel3 = trustTel3;
	}
	public String getTrustFax1() {
		return trustFax1;
	}
	public void setTrustFax1(String trustFax1) {
		this.trustFax1 = trustFax1;
	}
	public String getTrustFax2() {
		return trustFax2;
	}
	public void setTrustFax2(String trustFax2) {
		this.trustFax2 = trustFax2;
	}
	public String getTrustFax3() {
		return trustFax3;
	}
	public void setTrustFax3(String trustFax3) {
		this.trustFax3 = trustFax3;
	}
	public String getCeoMobile1() {
		return ceoMobile1;
	}
	public void setCeoMobile1(String ceoMobile1) {
		this.ceoMobile1 = ceoMobile1;
	}
	public String getCeoMobile2() {
		return ceoMobile2;
	}
	public void setCeoMobile2(String ceoMobile2) {
		this.ceoMobile2 = ceoMobile2;
	}
	public String getCeoMobile3() {
		return ceoMobile3;
	}
	public void setCeoMobile3(String ceoMobile3) {
		this.ceoMobile3 = ceoMobile3;
	}
	public String getCeoEmail1() {
		return ceoEmail1;
	}
	public void setCeoEmail1(String ceoEmail1) {
		this.ceoEmail1 = ceoEmail1;
	}
	public String getCeoEmail2() {
		return ceoEmail2;
	}
	public void setCeoEmail2(String ceoEmail2) {
		this.ceoEmail2 = ceoEmail2;
	}
	public String getSmsAgree() {
		return smsAgree;
	}
	public void setSmsAgree(String smsAgree) {
		this.smsAgree = smsAgree;
	}
	public String getTrustAddr1() {
		return trustAddr1;
	}
	public void setTrustAddr1(String trustAddr1) {
		this.trustAddr1 = trustAddr1;
	}
	public String getTrustAddr2() {
		return trustAddr2;
	}
	public void setTrustAddr2(String trustAddr2) {
		this.trustAddr2 = trustAddr2;
	}
	public String getTrustAddr3() {
		return trustAddr3;
	}
	public void setTrustAddr3(String trustAddr3) {
		this.trustAddr3 = trustAddr3;
	}
	public String getCeoAddr1() {
		return ceoAddr1;
	}
	public void setCeoAddr1(String ceoAddr1) {
		this.ceoAddr1 = ceoAddr1;
	}
	public String getCeoAddr2() {
		return ceoAddr2;
	}
	public void setCeoAddr2(String ceoAddr2) {
		this.ceoAddr2 = ceoAddr2;
	}
	public String getCeoAddr3() {
		return ceoAddr3;
	}
	public void setCeoAddr3(String ceoAddr3) {
		this.ceoAddr3 = ceoAddr3;
	}
	public String getCoNo1() {
		return coNo1;
	}
	public void setCoNo1(String coNo1) {
		this.coNo1 = coNo1;
	}
	public String getCoNo2() {
		return coNo2;
	}
	public void setCoNo2(String coNo2) {
		this.coNo2 = coNo2;
	}
	public String getCoNo3() {
		return coNo3;
	}
	public void setCoNo3(String coNo3) {
		this.coNo3 = coNo3;
	}

}