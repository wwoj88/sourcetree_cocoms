package cocoms.copyright.web.form;

import java.util.*;

import cocoms.copyright.entity.*;

public class ReportForm extends ReportInfoVO {

	private static final long serialVersionUID = 1L;

	private List<String> writingKind = new ArrayList<String>();
	private List<String> permKind = new ArrayList<String>();

	private List<String> inWorkNum = new ArrayList<String>();
	private List<String> outWorkNum = new ArrayList<String>();
	private List<String> workSum = new ArrayList<String>();

	private List<String> inRental = new ArrayList<String>();
	private List<String> outRental = new ArrayList<String>();
	private List<String> rentalSum = new ArrayList<String>();
	private List<String> inChrg = new ArrayList<String>();
	private List<String> outChrg = new ArrayList<String>();
	private List<String> chrgSum = new ArrayList<String>();
	private List<String> inChrgRate = new ArrayList<String>();
	private List<String> outChrgRate = new ArrayList<String>();
	private List<String> avgChrgRate = new ArrayList<String>();

	/*
	 * Getters and Setters
	 */
	public List<String> getOutWorkNum() {
		return outWorkNum;
	}
	public void setOutWorkNum(List<String> outWorkNum) {
		this.outWorkNum = outWorkNum;
	}
	public List<String> getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(List<String> writingKind) {
		this.writingKind = writingKind;
	}
	public List<String> getPermKind() {
		return permKind;
	}
	public void setPermKind(List<String> permKind) {
		this.permKind = permKind;
	}
	public List<String> getInWorkNum() {
		return inWorkNum;
	}
	public void setInWorkNum(List<String> inWorkNum) {
		this.inWorkNum = inWorkNum;
	}
	public List<String> getWorkSum() {
		return workSum;
	}
	public void setWorkSum(List<String> workSum) {
		this.workSum = workSum;
	}
	public List<String> getInRental() {
		return inRental;
	}
	public void setInRental(List<String> inRental) {
		this.inRental = inRental;
	}
	public List<String> getOutRental() {
		return outRental;
	}
	public void setOutRental(List<String> outRental) {
		this.outRental = outRental;
	}
	public List<String> getRentalSum() {
		return rentalSum;
	}
	public void setRentalSum(List<String> rentalSum) {
		this.rentalSum = rentalSum;
	}
	public List<String> getInChrg() {
		return inChrg;
	}
	public void setInChrg(List<String> inChrg) {
		this.inChrg = inChrg;
	}
	public List<String> getOutChrg() {
		return outChrg;
	}
	public void setOutChrg(List<String> outChrg) {
		this.outChrg = outChrg;
	}
	public List<String> getChrgSum() {
		return chrgSum;
	}
	public void setChrgSum(List<String> chrgSum) {
		this.chrgSum = chrgSum;
	}
	public List<String> getInChrgRate() {
		return inChrgRate;
	}
	public void setInChrgRate(List<String> inChrgRate) {
		this.inChrgRate = inChrgRate;
	}
	public List<String> getOutChrgRate() {
		return outChrgRate;
	}
	public void setOutChrgRate(List<String> outChrgRate) {
		this.outChrgRate = outChrgRate;
	}
	public List<String> getAvgChrgRate() {
		return avgChrgRate;
	}
	public void setAvgChrgRate(List<String> avgChrgRate) {
		this.avgChrgRate = avgChrgRate;
	}

}