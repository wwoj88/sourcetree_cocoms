package cocoms.copyright.web.form;

import java.util.*;

import cocoms.copyright.entity.*;

public class CertForm extends CertInfoVO {

	private static final long serialVersionUID = 1L;

	private String remark2 = "";	//

	private String returnUrl = "";	//

	/*
	 * Getters and Setters
	 */
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getReturnUrl() {
		return returnUrl;
	}
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
}