package cocoms.copyright.web.form;

import java.util.*;

import cocoms.copyright.entity.*;

public class SmsForm extends SmsHistoryVO {

	private static final long serialVersionUID = 1L;

	private List<String> memberSeqNo = new ArrayList<String>();
	private List<String> receiver = new ArrayList<String>();

	private String sendTime1 = "";
	private String sendTime2 = "";

	private String useYn_11 = "";
	private String useYn_12 = "";
	private String useYn_13 = "";

	private String useYn_21 = "";
	private String useYn_22 = "";
	private String useYn_23 = "";
	private String useYn_24 = "";
	private String useYn_25 = "";
	private String useYn_26 = "";
	private String useYn_27 = "";
	private String useYn_28 = "";

	private String useYn_31 = "";
	private String useYn_32 = "";
	private String useYn_33 = "";
	
	private List<String> mainCodes = new ArrayList<String>();
	private List<String> detailCodes = new ArrayList<String>();
	private List<String> contents = new ArrayList<String>();

	private String ceoName = "";
	private String loginId = "";

	/*
	 * Getters and Setters
	 */
	public List<String> getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(List<String> memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public List<String> getReceiver() {
		return receiver;
	}
	public void setReceiver(List<String> receiver) {
		this.receiver = receiver;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public List<String> getMainCodes() {
		return mainCodes;
	}
	public void setMainCodes(List<String> mainCodes) {
		this.mainCodes = mainCodes;
	}
	public List<String> getDetailCodes() {
		return detailCodes;
	}
	public void setDetailCodes(List<String> detailCodes) {
		this.detailCodes = detailCodes;
	}
	public List<String> getContents() {
		return contents;
	}
	public void setContents(List<String> contents) {
		this.contents = contents;
	}
	public String getSendTime1() {
		return sendTime1;
	}
	public void setSendTime1(String sendTime1) {
		this.sendTime1 = sendTime1;
	}
	public String getSendTime2() {
		return sendTime2;
	}
	public void setSendTime2(String sendTime2) {
		this.sendTime2 = sendTime2;
	}
	public String getUseYn_11() {
		return useYn_11;
	}
	public void setUseYn_11(String useYn_11) {
		this.useYn_11 = useYn_11;
	}
	public String getUseYn_12() {
		return useYn_12;
	}
	public void setUseYn_12(String useYn_12) {
		this.useYn_12 = useYn_12;
	}
	public String getUseYn_13() {
		return useYn_13;
	}
	public void setUseYn_13(String useYn_13) {
		this.useYn_13 = useYn_13;
	}
	public String getUseYn_21() {
		return useYn_21;
	}
	public void setUseYn_21(String useYn_21) {
		this.useYn_21 = useYn_21;
	}
	public String getUseYn_22() {
		return useYn_22;
	}
	public void setUseYn_22(String useYn_22) {
		this.useYn_22 = useYn_22;
	}
	public String getUseYn_23() {
		return useYn_23;
	}
	public void setUseYn_23(String useYn_23) {
		this.useYn_23 = useYn_23;
	}
	public String getUseYn_24() {
		return useYn_24;
	}
	public void setUseYn_24(String useYn_24) {
		this.useYn_24 = useYn_24;
	}
	public String getUseYn_25() {
		return useYn_25;
	}
	public void setUseYn_25(String useYn_25) {
		this.useYn_25 = useYn_25;
	}
	public String getUseYn_26() {
		return useYn_26;
	}
	public void setUseYn_26(String useYn_26) {
		this.useYn_26 = useYn_26;
	}
	public String getUseYn_27() {
		return useYn_27;
	}
	public void setUseYn_27(String useYn_27) {
		this.useYn_27 = useYn_27;
	}
	public String getUseYn_28() {
		return useYn_28;
	}
	public void setUseYn_28(String useYn_28) {
		this.useYn_28 = useYn_28;
	}
	public String getUseYn_31() {
		return useYn_31;
	}
	public void setUseYn_31(String useYn_31) {
		this.useYn_31 = useYn_31;
	}
	public String getUseYn_32() {
		return useYn_32;
	}
	public void setUseYn_32(String useYn_32) {
		this.useYn_32 = useYn_32;
	}
	public String getUseYn_33() {
		return useYn_33;
	}
	public void setUseYn_33(String useYn_33) {
		this.useYn_33 = useYn_33;
	}
	
}