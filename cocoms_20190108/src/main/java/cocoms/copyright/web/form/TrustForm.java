package cocoms.copyright.web.form;

import java.util.*;

import cocoms.copyright.entity.*;

public class TrustForm extends TrustInfoVO {

	private static final long serialVersionUID = 1L;

	// TrustStat2VO
	private List<String> trustRoyaltySeqNo	  = new ArrayList<String>();	//
	private List<String> trustGubunSeqNo      = new ArrayList<String>();	//
	private List<String> trustInfoSeqNo       = new ArrayList<String>();	//
	private List<String> contract             = new ArrayList<String>();	//
	private List<String> inCollected          = new ArrayList<String>();	//
	private List<String> outCollected         = new ArrayList<String>();	//
	private List<String> collectedSum         = new ArrayList<String>();	//
	private List<String> inDividend           = new ArrayList<String>();	//
	private List<String> outDividend          = new ArrayList<String>();	//
	private List<String> dividendSum          = new ArrayList<String>();	//
	private List<String> inDividendOff        = new ArrayList<String>();	//
	private List<String> outDividendOff       = new ArrayList<String>();	//
	private List<String> dividendOffSum       = new ArrayList<String>();	//
	private List<String> inChrg               = new ArrayList<String>();	//
	private List<String> outChrg              = new ArrayList<String>();	//
	private List<String> chrgSum              = new ArrayList<String>();	//
	private List<String> inChrgRate           = new ArrayList<String>();	//
	private List<String> outChrgRate          = new ArrayList<String>();	//
	private List<String> chrgRateSum          = new ArrayList<String>();	//
	private List<String> trustMemo            = new ArrayList<String>();	//
	
	private List<String> gubunName            = new ArrayList<String>();	//
	private List<String> gubunKind            = new ArrayList<String>();	//

	private List<String> trustCopyrightSeqNo  = new ArrayList<String>();	//
	private List<String> gubun            	  = new ArrayList<String>();	//
	private List<String> organization     	  = new ArrayList<String>();	//
	private List<String> collected        	  = new ArrayList<String>();	//
	private List<String> dividend         	  = new ArrayList<String>();	//
	private List<String> dividendOff      	  = new ArrayList<String>();	//
	private List<String> chrg             	  = new ArrayList<String>();	//
	private List<String> chrgRate         	  = new ArrayList<String>();	//
	private List<String> memo             	  = new ArrayList<String>();	//

	private List<String> trustExecSeqNo       = new ArrayList<String>();	//
	private List<String> gubun2            	  = new ArrayList<String>();	//
	private List<String> organization2     	  = new ArrayList<String>();	//
	private List<String> collected2        	  = new ArrayList<String>();	//
	private List<String> dividend2         	  = new ArrayList<String>();	//
	private List<String> dividendOff2      	  = new ArrayList<String>();	//
	private List<String> chrg2             	  = new ArrayList<String>();	//
	private List<String> chrgRate2         	  = new ArrayList<String>();	//
	private List<String> memo2             	  = new ArrayList<String>();	//

	private List<String> trustMakerSeqNo       = new ArrayList<String>();	//
	private List<String> gubun3            	  = new ArrayList<String>();	//
	private List<String> organization3     	  = new ArrayList<String>();	//
	private List<String> collected3        	  = new ArrayList<String>();	//
	private List<String> dividend3         	  = new ArrayList<String>();	//
	private List<String> dividendOff3      	  = new ArrayList<String>();	//
	private List<String> chrg3             	  = new ArrayList<String>();	//
	private List<String> chrgRate3         	  = new ArrayList<String>();	//
	private List<String> memo3             	  = new ArrayList<String>();	//
	
	private String returnUrl = "";	//

	/*
	 * Getters and Setters
	 */
	public List<String> getTrustRoyaltySeqNo() {
		return trustRoyaltySeqNo;
	}
	public void setTrustRoyaltySeqNo(List<String> trustRoyaltySeqNo) {
		this.trustRoyaltySeqNo = trustRoyaltySeqNo;
	}
	public List<String> getTrustMakerSeqNo() {
		return trustMakerSeqNo;
	}
	public void setTrustMakerSeqNo(List<String> trustMakerSeqNo) {
		this.trustMakerSeqNo = trustMakerSeqNo;
	}
	public List<String> getGubun3() {
		return gubun3;
	}
	public void setGubun3(List<String> gubun3) {
		this.gubun3 = gubun3;
	}
	public List<String> getOrganization3() {
		return organization3;
	}
	public void setOrganization3(List<String> organization3) {
		this.organization3 = organization3;
	}
	public List<String> getCollected3() {
		return collected3;
	}
	public void setCollected3(List<String> collected3) {
		this.collected3 = collected3;
	}
	public List<String> getDividend3() {
		return dividend3;
	}
	public void setDividend3(List<String> dividend3) {
		this.dividend3 = dividend3;
	}
	public List<String> getDividendOff3() {
		return dividendOff3;
	}
	public void setDividendOff3(List<String> dividendOff3) {
		this.dividendOff3 = dividendOff3;
	}
	public List<String> getChrg3() {
		return chrg3;
	}
	public void setChrg3(List<String> chrg3) {
		this.chrg3 = chrg3;
	}
	public List<String> getChrgRate3() {
		return chrgRate3;
	}
	public void setChrgRate3(List<String> chrgRate3) {
		this.chrgRate3 = chrgRate3;
	}
	public List<String> getMemo3() {
		return memo3;
	}
	public void setMemo3(List<String> memo3) {
		this.memo3 = memo3;
	}
	public List<String> getOrganization2() {
		return organization2;
	}
	public void setOrganization2(List<String> organization2) {
		this.organization2 = organization2;
	}
	public List<String> getCollected2() {
		return collected2;
	}
	public void setCollected2(List<String> collected2) {
		this.collected2 = collected2;
	}
	public List<String> getTrustExecSeqNo() {
		return trustExecSeqNo;
	}
	public void setTrustExecSeqNo(List<String> trustExecSeqNo) {
		this.trustExecSeqNo = trustExecSeqNo;
	}
	public List<String> getGubun2() {
		return gubun2;
	}
	public void setGubun2(List<String> gubun2) {
		this.gubun2 = gubun2;
	}
	public List<String> getDividend2() {
		return dividend2;
	}
	public void setDividend2(List<String> dividend2) {
		this.dividend2 = dividend2;
	}
	public List<String> getDividendOff2() {
		return dividendOff2;
	}
	public void setDividendOff2(List<String> dividendOff2) {
		this.dividendOff2 = dividendOff2;
	}
	public List<String> getChrg2() {
		return chrg2;
	}
	public void setChrg2(List<String> chrg2) {
		this.chrg2 = chrg2;
	}
	public List<String> getChrgRate2() {
		return chrgRate2;
	}
	public void setChrgRate2(List<String> chrgRate2) {
		this.chrgRate2 = chrgRate2;
	}
	public List<String> getMemo2() {
		return memo2;
	}
	public void setMemo2(List<String> memo2) {
		this.memo2 = memo2;
	}
	public List<String> getTrustCopyrightSeqNo() {
		return trustCopyrightSeqNo;
	}
	public void setTrustCopyrightSeqNo(List<String> trustCopyrightSeqNo) {
		this.trustCopyrightSeqNo = trustCopyrightSeqNo;
	}
	public List<String> getGubun() {
		return gubun;
	}
	public void setGubun(List<String> gubun) {
		this.gubun = gubun;
	}
	public List<String> getOrganization() {
		return organization;
	}
	public void setOrganization(List<String> organization) {
		this.organization = organization;
	}
	public List<String> getCollected() {
		return collected;
	}
	public void setCollected(List<String> collected) {
		this.collected = collected;
	}
	public List<String> getDividend() {
		return dividend;
	}
	public void setDividend(List<String> dividend) {
		this.dividend = dividend;
	}
	public List<String> getDividendOff() {
		return dividendOff;
	}
	public void setDividendOff(List<String> dividendOff) {
		this.dividendOff = dividendOff;
	}
	public List<String> getChrg() {
		return chrg;
	}
	public void setChrg(List<String> chrg) {
		this.chrg = chrg;
	}
	public List<String> getChrgRate() {
		return chrgRate;
	}
	public void setChrgRate(List<String> chrgRate) {
		this.chrgRate = chrgRate;
	}
	public List<String> getMemo() {
		return memo;
	}
	public void setMemo(List<String> memo) {
		this.memo = memo;
	}
	public String getReturnUrl() {
		return returnUrl;
	}
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
	public List<String> getGubunName() {
		return gubunName;
	}
	public void setGubunName(List<String> gubunName) {
		this.gubunName = gubunName;
	}
	public List<String> getGubunKind() {
		return gubunKind;
	}
	public void setGubunKind(List<String> gubunKind) {
		this.gubunKind = gubunKind;
	}
	public List<String> getTrustGubunSeqNo() {
		return trustGubunSeqNo;
	}
	public void setTrustGubunSeqNo(List<String> trustGubunSeqNo) {
		this.trustGubunSeqNo = trustGubunSeqNo;
	}
	public List<String> getTrustInfoSeqNo() {
		return trustInfoSeqNo;
	}
	public void setTrustInfoSeqNo(List<String> trustInfoSeqNo) {
		this.trustInfoSeqNo = trustInfoSeqNo;
	}
	public List<String> getContract() {
		return contract;
	}
	public void setContract(List<String> contract) {
		this.contract = contract;
	}
	public List<String> getInCollected() {
		return inCollected;
	}
	public void setInCollected(List<String> inCollected) {
		this.inCollected = inCollected;
	}
	public List<String> getOutCollected() {
		return outCollected;
	}
	public void setOutCollected(List<String> outCollected) {
		this.outCollected = outCollected;
	}
	public List<String> getCollectedSum() {
		return collectedSum;
	}
	public void setCollectedSum(List<String> collectedSum) {
		this.collectedSum = collectedSum;
	}
	public List<String> getInDividend() {
		return inDividend;
	}
	public void setInDividend(List<String> inDividend) {
		this.inDividend = inDividend;
	}
	public List<String> getOutDividend() {
		return outDividend;
	}
	public void setOutDividend(List<String> outDividend) {
		this.outDividend = outDividend;
	}
	public List<String> getDividendSum() {
		return dividendSum;
	}
	public void setDividendSum(List<String> dividendSum) {
		this.dividendSum = dividendSum;
	}
	public List<String> getInDividendOff() {
		return inDividendOff;
	}
	public void setInDividendOff(List<String> inDividendOff) {
		this.inDividendOff = inDividendOff;
	}
	public List<String> getOutDividendOff() {
		return outDividendOff;
	}
	public void setOutDividendOff(List<String> outDividendOff) {
		this.outDividendOff = outDividendOff;
	}
	public List<String> getDividendOffSum() {
		return dividendOffSum;
	}
	public void setDividendOffSum(List<String> dividendOffSum) {
		this.dividendOffSum = dividendOffSum;
	}
	public List<String> getInChrg() {
		return inChrg;
	}
	public void setInChrg(List<String> inChrg) {
		this.inChrg = inChrg;
	}
	public List<String> getOutChrg() {
		return outChrg;
	}
	public void setOutChrg(List<String> outChrg) {
		this.outChrg = outChrg;
	}
	public List<String> getChrgSum() {
		return chrgSum;
	}
	public void setChrgSum(List<String> chrgSum) {
		this.chrgSum = chrgSum;
	}
	public List<String> getInChrgRate() {
		return inChrgRate;
	}
	public void setInChrgRate(List<String> inChrgRate) {
		this.inChrgRate = inChrgRate;
	}
	public List<String> getOutChrgRate() {
		return outChrgRate;
	}
	public void setOutChrgRate(List<String> outChrgRate) {
		this.outChrgRate = outChrgRate;
	}
	public List<String> getChrgRateSum() {
		return chrgRateSum;
	}
	public void setChrgRateSum(List<String> chrgRateSum) {
		this.chrgRateSum = chrgRateSum;
	}
	public List<String> getTrustMemo() {
		return trustMemo;
	}
	public void setTrustMemo(List<String> trustMemo) {
		this.trustMemo = trustMemo;
	}
}