package cocoms.copyright.web.form;

import java.util.*;

import cocoms.copyright.entity.*;


public class ApplicationForm extends MemberVO {
	private static final long serialVersionUID = 1L;

	private String procType = "";			// 처리구분 (sub_regist, turst_regist, ...)
	
	
	private String coNo1 = ""; 				// 사업자번호
	private String coNo2 = ""; 				// 사업자번호
	private String coNo3 = ""; 				// 사업자번호
	
	private String precoNo1 = ""; 				// 사업자번호
	private String precoNo2 = ""; 				// 사업자번호
	private String precoNo3 = ""; 				// 사업자번호
	
	private String bubNo1 = ""; 			// 
	private String bubNo2 = ""; 			// 
	
	private String prebubNo1 = ""; 			// 
	private String prebubNo2 = ""; 			// 
	
//	private String trustAddr1 = "";			//
//	private String trustAddr2 = "";			//
	
	private String trustTel1 = "";			//
	private String trustTel2 = "";			//
	private String trustTel3 = "";			//

	private String pretrustTel1 = "";			//
	private String pretrustTel2 = "";			//
	private String pretrustTel3 = "";			//
	
	private String trustFax1 = "";			//
	private String trustFax2 = "";			//
	private String trustFax3 = "";			//

	private String pretrustFax1 = "";			//
	private String pretrustFax2 = "";			//
	private String pretrustFax3 = "";			//
	
	private String ceoRegNo1 = "";			//
	private String ceoRegNo2 = "";			//
	
	private String preceoRegNo1 = "";			//
	private String preceoRegNo2 = "";			//
	
//	private String ceoAddr1 = "";			//
//	private String ceoAddr2 = "";			//

	private String ceoTel1 = "";			//
	private String ceoTel2 = "";			//
	private String ceoTel3 = "";			//

	private String preceoTel1 = "";			//
	private String preceoTel2 = "";			//
	private String preceoTel3 = "";			//

	private String ceoFax1 = "";			//
	private String ceoFax2 = "";			//
	private String ceoFax3 = "";			//

    
	private String preceoFax1 = "";			//
	private String preceoFax2 = "";			//
	private String preceoFax3 = "";			//
	
	private String ceoMobile1 = "";
	private String ceoMobile2 = "";
	private String ceoMobile3 = "";

	private String preceoMobile1 = "";
	private String preceoMobile2 = "";
	private String preceoMobile3 = "";

	private String chgMemo="";
	private String ceoEmail1 = ""; 			// 전자우편
	private String ceoEmail2 = ""; 			// 전자우편
	
	private String preceoEmail1 = ""; 			// 전자우편
	private String preceoEmail2 = ""; 			// 전자우편
	private String precoName =	"";
	private String preceoName= "";
	private String preList = "";
	
	
	private String smsAgree = ""; 			// SMS수신동의
	
	private List<String> writingKind = new ArrayList<String>();
	private List<String> prewritingKind = new ArrayList<String>();
	
	private List<String> permkind1  = new ArrayList<String>();		// 어문 저작물
	private List<String> permkind2  = new ArrayList<String>();		// 음악 저작물
	private List<String> permkind3  = new ArrayList<String>();		// 영상 저작물
	private List<String> permkind21 = new ArrayList<String>();		// 연극 저작물
	private List<String> permkind4  = new ArrayList<String>();		// 미술 저작물
	private List<String> permkind5  = new ArrayList<String>();		// 사진 저작물
	private List<String> permkind22 = new ArrayList<String>();		// 건축 저작물
	private List<String> permkind23 = new ArrayList<String>();		// 도형 저작물
	private List<String> permkind24 = new ArrayList<String>();		// 컴퓨터프로그램 저작물
	private List<String> permkind0  = new ArrayList<String>();		// 기타 저작물
	
	private List<String> permkind10 = new ArrayList<String>();		// 실연자의 권리
	private List<String> permkind11 = new ArrayList<String>();		// 음반제작자의 권리
	private List<String> permkind12 = new ArrayList<String>();		// 출판권
	
	private String permEtc1  = "";
	private String permEtc2  = "";
	private String permEtc3  = "";
	private String permEtc21 = "";
	private String permEtc4  = "";
	private String permEtc5  = "";
	private String permEtc22 = "";
	private String permEtc23 = "";
	private String permEtc24 = "";
	private String permEtc0  = "";
	private String permEtc10 = "";
	private String permEtc11 = "";
//	private String permEtc12 = "";
    
	private List<String> prepermkind1  = new ArrayList<String>();		// 어문 저작물
	private List<String> prepermkind2  = new ArrayList<String>();		// 음악 저작물
	private List<String> prepermkind3  = new ArrayList<String>();		// 영상 저작물
	private List<String> prepermkind21 = new ArrayList<String>();		// 연극 저작물
	private List<String> prepermkind4  = new ArrayList<String>();		// 미술 저작물
	private List<String> prepermkind5  = new ArrayList<String>();		// 사진 저작물
	private List<String> prepermkind23 = new ArrayList<String>();		// 도형 저작물
	private List<String> prepermkind24 = new ArrayList<String>();		// 컴퓨터프로그램 저작물
	private List<String> prepermkind0  = new ArrayList<String>();		// 기타 저작물

	private List<String> prepermkind10 = new ArrayList<String>();		// 실연자의 권리
	private List<String> prepermkind11 = new ArrayList<String>();		// 음반제작자의 권리
	private List<String> prepermkind12 = new ArrayList<String>();		// 출판권
	
	private String prepermEtc1  = "";
	private List<String> prepermkind22 = new ArrayList<String>();		// 건축 저작물
	private String prepermEtc2  = "";
	private String prepermEtc3  = "";
	private String prepermEtc21 = "";
	private String prepermEtc4  = "";
	private String prepermEtc5  = "";
	private String prepermEtc22 = "";
	private String prepermEtc23 = "";
	private String prepermEtc24 = "";
	private String prepermEtc0  = "";
	private String prepermEtc10 = "";
	private String prepermEtc11 = "";
//	private String permEtc12 = "";

	private String g4cssCK = "";
	
	public String getPreceoName() {
		return preceoName;
	}
	public void setPreceoName(String preceoName) {
		this.preceoName = preceoName;
	}
	public String getG4cssCK() {
		return g4cssCK;
	}
	public void setG4cssCK(String g4cssCK) {
		this.g4cssCK = g4cssCK;
	}
	
	private MemberVO member = new MemberVO();
	
	public String getPrecoName() {
		return precoName;
	}
	public void setPrecoName(String precoName) {
		this.precoName = precoName;
	}
	public String getPreList() {
		return preList;
	}
	public void setPreList(String preList) {
		this.preList = preList;
	}
	public String getChgMemo() {
		return chgMemo;
	}
	public void setChgMemo(String chgMemo) {
		this.chgMemo = chgMemo;
	}
	public String getPrecoNo1() {
		return precoNo1;
	}
	public void setPrecoNo1(String precoNo1) {
		this.precoNo1 = precoNo1;
	}
	public String getPrecoNo2() {
		return precoNo2;
	}
	public void setPrecoNo2(String precoNo2) {
		this.precoNo2 = precoNo2;
	}
	public String getPrecoNo3() {
		return precoNo3;
	}
	public void setPrecoNo3(String precoNo3) {
		this.precoNo3 = precoNo3;
	}
	public String getPrebubNo1() {
		return prebubNo1;
	}
	public void setPrebubNo1(String prebubNo1) {
		this.prebubNo1 = prebubNo1;
	}
	public String getPrebubNo2() {
		return prebubNo2;
	}
	public void setPrebubNo2(String prebubNo2) {
		this.prebubNo2 = prebubNo2;
	}
	public String getPretrustTel1() {
		return pretrustTel1;
	}
	public void setPretrustTel1(String pretrustTel1) {
		this.pretrustTel1 = pretrustTel1;
	}
	public String getPretrustTel2() {
		return pretrustTel2;
	}
	public void setPretrustTel2(String pretrustTel2) {
		this.pretrustTel2 = pretrustTel2;
	}
	public String getPretrustTel3() {
		return pretrustTel3;
	}
	public void setPretrustTel3(String pretrustTel3) {
		this.pretrustTel3 = pretrustTel3;
	}
	public String getPretrustFax1() {
		return pretrustFax1;
	}
	public void setPretrustFax1(String pretrustFax1) {
		this.pretrustFax1 = pretrustFax1;
	}
	public String getPretrustFax2() {
		return pretrustFax2;
	}
	public void setPretrustFax2(String pretrustFax2) {
		this.pretrustFax2 = pretrustFax2;
	}
	public String getPretrustFax3() {
		return pretrustFax3;
	}
	public void setPretrustFax3(String pretrustFax3) {
		this.pretrustFax3 = pretrustFax3;
	}
	public String getPreceoRegNo1() {
		return preceoRegNo1;
	}
	public void setPreceoRegNo1(String preceoRegNo1) {
		this.preceoRegNo1 = preceoRegNo1;
	}
	public String getPreceoRegNo2() {
		return preceoRegNo2;
	}
	public void setPreceoRegNo2(String preceoRegNo2) {
		this.preceoRegNo2 = preceoRegNo2;
	}
	public String getPreceoTel1() {
		return preceoTel1;
	}
	public void setPreceoTel1(String preceoTel1) {
		this.preceoTel1 = preceoTel1;
	}
	public String getPreceoTel2() {
		return preceoTel2;
	}
	public void setPreceoTel2(String preceoTel2) {
		this.preceoTel2 = preceoTel2;
	}
	public String getPreceoTel3() {
		return preceoTel3;
	}
	public void setPreceoTel3(String preceoTel3) {
		this.preceoTel3 = preceoTel3;
	}
	public String getPreceoFax1() {
		return preceoFax1;
	}
	public void setPreceoFax1(String preceoFax1) {
		this.preceoFax1 = preceoFax1;
	}
	public String getPreceoFax2() {
		return preceoFax2;
	}
	public void setPreceoFax2(String preceoFax2) {
		this.preceoFax2 = preceoFax2;
	}
	public String getPreceoFax3() {
		return preceoFax3;
	}
	public void setPreceoFax3(String preceoFax3) {
		this.preceoFax3 = preceoFax3;
	}
	public String getPreceoMobile1() {
		return preceoMobile1;
	}
	public void setPreceoMobile1(String preceoMobile1) {
		this.preceoMobile1 = preceoMobile1;
	}
	public String getPreceoMobile2() {
		return preceoMobile2;
	}
	public void setPreceoMobile2(String preceoMobile2) {
		this.preceoMobile2 = preceoMobile2;
	}
	public String getPreceoMobile3() {
		return preceoMobile3;
	}
	public void setPreceoMobile3(String preceoMobile3) {
		this.preceoMobile3 = preceoMobile3;
	}
	public String getPreceoEmail1() {
		return preceoEmail1;
	}
	public void setPreceoEmail1(String preceoEmail1) {
		this.preceoEmail1 = preceoEmail1;
	}
	public String getPreceoEmail2() {
		return preceoEmail2;
	}
	public void setPreceoEmail2(String preceoEmail2) {
		this.preceoEmail2 = preceoEmail2;
	}
	public List<String> getPrewritingKind() {
		return prewritingKind;
	}
	public void setPrewritingKind(List<String> prewritingKind) {
		this.prewritingKind = prewritingKind;
	}
	public List<String> getPrepermkind1() {
		return prepermkind1;
	}
	public void setPrepermkind1(List<String> prepermkind1) {
		this.prepermkind1 = prepermkind1;
	}
	public List<String> getPrepermkind2() {
		return prepermkind2;
	}
	public void setPrepermkind2(List<String> prepermkind2) {
		this.prepermkind2 = prepermkind2;
	}
	public List<String> getPrepermkind3() {
		return prepermkind3;
	}
	public void setPrepermkind3(List<String> prepermkind3) {
		this.prepermkind3 = prepermkind3;
	}
	public List<String> getPrepermkind21() {
		return prepermkind21;
	}
	public void setPrepermkind21(List<String> prepermkind21) {
		this.prepermkind21 = prepermkind21;
	}
	public List<String> getPrepermkind4() {
		return prepermkind4;
	}
	public void setPrepermkind4(List<String> prepermkind4) {
		this.prepermkind4 = prepermkind4;
	}
	public List<String> getPrepermkind5() {
		return prepermkind5;
	}
	public void setPrepermkind5(List<String> prepermkind5) {
		this.prepermkind5 = prepermkind5;
	}
	public List<String> getPrepermkind22() {
		return prepermkind22;
	}
	public void setPrepermkind22(List<String> prepermkind22) {
		this.prepermkind22 = prepermkind22;
	}
	public List<String> getPrepermkind23() {
		return prepermkind23;
	}
	public void setPrepermkind23(List<String> prepermkind23) {
		this.prepermkind23 = prepermkind23;
	}
	public List<String> getPrepermkind24() {
		return prepermkind24;
	}
	public void setPrepermkind24(List<String> prepermkind24) {
		this.prepermkind24 = prepermkind24;
	}
	public List<String> getPrepermkind0() {
		return prepermkind0;
	}
	public void setPrepermkind0(List<String> prepermkind0) {
		this.prepermkind0 = prepermkind0;
	}
	public List<String> getPrepermkind10() {
		return prepermkind10;
	}
	public void setPrepermkind10(List<String> prepermkind10) {
		this.prepermkind10 = prepermkind10;
	}
	public List<String> getPrepermkind11() {
		return prepermkind11;
	}
	public void setPrepermkind11(List<String> prepermkind11) {
		this.prepermkind11 = prepermkind11;
	}
	public List<String> getPrepermkind12() {
		return prepermkind12;
	}
	public void setPrepermkind12(List<String> prepermkind12) {
		this.prepermkind12 = prepermkind12;
	}
	public String getPrepermEtc1() {
		return prepermEtc1;
	}
	public void setPrepermEtc1(String prepermEtc1) {
		this.prepermEtc1 = prepermEtc1;
	}
	public String getPrepermEtc2() {
		return prepermEtc2;
	}
	public void setPrepermEtc2(String prepermEtc2) {
		this.prepermEtc2 = prepermEtc2;
	}
	public String getPrepermEtc3() {
		return prepermEtc3;
	}
	public void setPrepermEtc3(String prepermEtc3) {
		this.prepermEtc3 = prepermEtc3;
	}
	public String getPrepermEtc21() {
		return prepermEtc21;
	}
	public void setPrepermEtc21(String prepermEtc21) {
		this.prepermEtc21 = prepermEtc21;
	}
	public String getPrepermEtc4() {
		return prepermEtc4;
	}
	public void setPrepermEtc4(String prepermEtc4) {
		this.prepermEtc4 = prepermEtc4;
	}
	public String getPrepermEtc5() {
		return prepermEtc5;
	}
	public void setPrepermEtc5(String prepermEtc5) {
		this.prepermEtc5 = prepermEtc5;
	}
	public String getPrepermEtc22() {
		return prepermEtc22;
	}
	public void setPrepermEtc22(String prepermEtc22) {
		this.prepermEtc22 = prepermEtc22;
	}
	public String getPrepermEtc23() {
		return prepermEtc23;
	}
	public void setPrepermEtc23(String prepermEtc23) {
		this.prepermEtc23 = prepermEtc23;
	}
	public String getPrepermEtc24() {
		return prepermEtc24;
	}
	public void setPrepermEtc24(String prepermEtc24) {
		this.prepermEtc24 = prepermEtc24;
	}
	public String getPrepermEtc0() {
		return prepermEtc0;
	}
	public void setPrepermEtc0(String prepermEtc0) {
		this.prepermEtc0 = prepermEtc0;
	}
	public String getPrepermEtc10() {
		return prepermEtc10;
	}
	public void setPrepermEtc10(String prepermEtc10) {
		this.prepermEtc10 = prepermEtc10;
	}
	public String getPrepermEtc11() {
		return prepermEtc11;
	}
	public void setPrepermEtc11(String prepermEtc11) {
		this.prepermEtc11 = prepermEtc11;
	}
	private List<WritingKindVO> writingKindList = new ArrayList<WritingKindVO>();
	private List<WritingKindVO> prewritingKindList = new ArrayList<WritingKindVO>();
	private List<PermKindVO> permKindList = new ArrayList<PermKindVO>();
	private List<PermKindVO> prepermKindList = new ArrayList<PermKindVO>();

	private List<ChgWritingKindVO> prewritingKindListChg = new ArrayList<ChgWritingKindVO>();
	private List<ChgPermKindVO> prepermKindListChg = new ArrayList<ChgPermKindVO>();

	
	
	
	
	
	public List<ChgWritingKindVO> getPrewritingKindListChg() {
		return prewritingKindListChg;
	}
	public void setPrewritingKindListChg(List<ChgWritingKindVO> prewritingKindListChg) {
		this.prewritingKindListChg = prewritingKindListChg;
	}
	public List<ChgPermKindVO> getPrepermKindListChg() {
		return prepermKindListChg;
	}
	public void setPrepermKindListChg(List<ChgPermKindVO> prepermKindListChg) {
		this.prepermKindListChg = prepermKindListChg;
	}
	private List<String> chgCd = new ArrayList<String>();
	
	private String certInfoSeqNo = "";

	/*
	 * Getters and Setters
	 */
	public String getTrustTel1() {
		return trustTel1;
	}
	public List<WritingKindVO> getPrewritingKindList() {
		return prewritingKindList;
	}
	public void setPrewritingKindList(List<WritingKindVO> prewritingKindList) {
		this.prewritingKindList = prewritingKindList;
	}
	public List<PermKindVO> getPrepermKindList() {
		return prepermKindList;
	}
	public void setPrepermKindList(List<PermKindVO> prepermKindList) {
		this.prepermKindList = prepermKindList;
	}
	public void setTrustTel1(String trustTel1) {
		this.trustTel1 = trustTel1;
	}
	public String getCertInfoSeqNo() {
		return certInfoSeqNo;
	}
	public void setCertInfoSeqNo(String certInfoSeqNo) {
		this.certInfoSeqNo = certInfoSeqNo;
	}
	public List<String> getChgCd() {
		return chgCd;
	}
	public void setChgCd(List<String> chgCd) {
		this.chgCd = chgCd;
	}
	public String getCeoTel1() {
		return ceoTel1;
	}
	public void setCeoTel1(String ceoTel1) {
		this.ceoTel1 = ceoTel1;
	}
	public String getCeoTel2() {
		return ceoTel2;
	}
	public void setCeoTel2(String ceoTel2) {
		this.ceoTel2 = ceoTel2;
	}
	public String getCeoTel3() {
		return ceoTel3;
	}
	public void setCeoTel3(String ceoTel3) {
		this.ceoTel3 = ceoTel3;
	}
	public String getCeoFax1() {
		return ceoFax1;
	}
	public void setCeoFax1(String ceoFax1) {
		this.ceoFax1 = ceoFax1;
	}
	public String getCeoFax2() {
		return ceoFax2;
	}
	public void setCeoFax2(String ceoFax2) {
		this.ceoFax2 = ceoFax2;
	}
	public String getCeoFax3() {
		return ceoFax3;
	}
	public void setCeoFax3(String ceoFax3) {
		this.ceoFax3 = ceoFax3;
	}
	public String getBubNo1() {
		return bubNo1;
	}
	public void setBubNo1(String bubNo1) {
		this.bubNo1 = bubNo1;
	}
	public String getBubNo2() {
		return bubNo2;
	}
	public void setBubNo2(String bubNo2) {
		this.bubNo2 = bubNo2;
	}
	public MemberVO getMember() {
		return member;
	}
	public void setMember(MemberVO member) {
		this.member = member;
	}
	public List<WritingKindVO> getWritingKindList() {
		return writingKindList;
	}
	public void setWritingKindList(List<WritingKindVO> writingKindList) {
		this.writingKindList = writingKindList;
	}
	public List<PermKindVO> getPermKindList() {
		return permKindList;
	}
	public void setPermKindList(List<PermKindVO> permKindList) {
		this.permKindList = permKindList;
	}
	public String getProcType() {
		return procType;
	}
	public void setProcType(String procType) {
		this.procType = procType;
	}
	public String getCeoRegNo1() {
		return ceoRegNo1;
	}
	public void setCeoRegNo1(String ceoRegNo1) {
		this.ceoRegNo1 = ceoRegNo1;
	}
	public String getCeoRegNo2() {
		return ceoRegNo2;
	}
	public void setCeoRegNo2(String ceoRegNo2) {
		this.ceoRegNo2 = ceoRegNo2;
	}
	public String getPermEtc1() {
		return permEtc1;
	}
	public void setPermEtc1(String permEtc1) {
		this.permEtc1 = permEtc1;
	}
	public String getPermEtc2() {
		return permEtc2;
	}
	public void setPermEtc2(String permEtc2) {
		this.permEtc2 = permEtc2;
	}
	public String getPermEtc3() {
		return permEtc3;
	}
	public void setPermEtc3(String permEtc3) {
		this.permEtc3 = permEtc3;
	}
	public String getPermEtc21() {
		return permEtc21;
	}
	public void setPermEtc21(String permEtc21) {
		this.permEtc21 = permEtc21;
	}
	public String getPermEtc4() {
		return permEtc4;
	}
	public void setPermEtc4(String permEtc4) {
		this.permEtc4 = permEtc4;
	}
	public String getPermEtc5() {
		return permEtc5;
	}
	public void setPermEtc5(String permEtc5) {
		this.permEtc5 = permEtc5;
	}
	public String getPermEtc22() {
		return permEtc22;
	}
	public void setPermEtc22(String permEtc22) {
		this.permEtc22 = permEtc22;
	}
	public String getPermEtc23() {
		return permEtc23;
	}
	public void setPermEtc23(String permEtc23) {
		this.permEtc23 = permEtc23;
	}
	public String getPermEtc24() {
		return permEtc24;
	}
	public void setPermEtc24(String permEtc24) {
		this.permEtc24 = permEtc24;
	}
	public String getPermEtc0() {
		return permEtc0;
	}
	public void setPermEtc0(String permEtc0) {
		this.permEtc0 = permEtc0;
	}
	public String getPermEtc10() {
		return permEtc10;
	}
	public void setPermEtc10(String permEtc10) {
		this.permEtc10 = permEtc10;
	}
	public String getPermEtc11() {
		return permEtc11;
	}
	public void setPermEtc11(String permEtc11) {
		this.permEtc11 = permEtc11;
	}
	public List<String> getPermkind1() {
		return permkind1;
	}
	public void setPermkind1(List<String> permkind1) {
		this.permkind1 = permkind1;
	}
	public List<String> getPermkind2() {
		return permkind2;
	}
	public void setPermkind2(List<String> permkind2) {
		this.permkind2 = permkind2;
	}
	public List<String> getPermkind3() {
		return permkind3;
	}
	public void setPermkind3(List<String> permkind3) {
		this.permkind3 = permkind3;
	}
	public List<String> getPermkind21() {
		return permkind21;
	}
	public void setPermkind21(List<String> permkind21) {
		this.permkind21 = permkind21;
	}
	public List<String> getPermkind4() {
		return permkind4;
	}
	public void setPermkind4(List<String> permkind4) {
		this.permkind4 = permkind4;
	}
	public List<String> getPermkind5() {
		return permkind5;
	}
	public void setPermkind5(List<String> permkind5) {
		this.permkind5 = permkind5;
	}
	public List<String> getPermkind22() {
		return permkind22;
	}
	public void setPermkind22(List<String> permkind22) {
		this.permkind22 = permkind22;
	}
	public List<String> getPermkind23() {
		return permkind23;
	}
	public void setPermkind23(List<String> permkind23) {
		this.permkind23 = permkind23;
	}
	public List<String> getPermkind24() {
		return permkind24;
	}
	public void setPermkind24(List<String> permkind24) {
		this.permkind24 = permkind24;
	}
	public List<String> getPermkind0() {
		return permkind0;
	}
	public void setPermkind0(List<String> permkind0) {
		this.permkind0 = permkind0;
	}
	public List<String> getPermkind10() {
		return permkind10;
	}
	public void setPermkind10(List<String> permkind10) {
		this.permkind10 = permkind10;
	}
	public List<String> getPermkind11() {
		return permkind11;
	}
	public void setPermkind11(List<String> permkind11) {
		this.permkind11 = permkind11;
	}
	public List<String> getPermkind12() {
		return permkind12;
	}
	public void setPermkind12(List<String> permkind12) {
		this.permkind12 = permkind12;
	}
	public List<String> getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(List<String> writingKind) {
		this.writingKind = writingKind;
	}
	public String getTrustTel2() {
		return trustTel2;
	}
	public void setTrustTel2(String trustTel2) {
		this.trustTel2 = trustTel2;
	}
	public String getTrustTel3() {
		return trustTel3;
	}
	public void setTrustTel3(String trustTel3) {
		this.trustTel3 = trustTel3;
	}
	public String getTrustFax1() {
		return trustFax1;
	}
	public void setTrustFax1(String trustFax1) {
		this.trustFax1 = trustFax1;
	}
	public String getTrustFax2() {
		return trustFax2;
	}
	public void setTrustFax2(String trustFax2) {
		this.trustFax2 = trustFax2;
	}
	public String getTrustFax3() {
		return trustFax3;
	}
	public void setTrustFax3(String trustFax3) {
		this.trustFax3 = trustFax3;
	}
	public String getCeoMobile1() {
		return ceoMobile1;
	}
	public void setCeoMobile1(String ceoMobile1) {
		this.ceoMobile1 = ceoMobile1;
	}
	public String getCeoMobile2() {
		return ceoMobile2;
	}
	public void setCeoMobile2(String ceoMobile2) {
		this.ceoMobile2 = ceoMobile2;
	}
	public String getCeoMobile3() {
		return ceoMobile3;
	}
	public void setCeoMobile3(String ceoMobile3) {
		this.ceoMobile3 = ceoMobile3;
	}
	public String getCeoEmail1() {
		return ceoEmail1;
	}
	public void setCeoEmail1(String ceoEmail1) {
		this.ceoEmail1 = ceoEmail1;
	}
	public String getCeoEmail2() {
		return ceoEmail2;
	}
	public void setCeoEmail2(String ceoEmail2) {
		this.ceoEmail2 = ceoEmail2;
	}
	public String getSmsAgree() {
		return smsAgree;
	}
	public void setSmsAgree(String smsAgree) {
		this.smsAgree = smsAgree;
	}
	public String getCoNo1() {
		return coNo1;
	}
	public void setCoNo1(String coNo1) {
		this.coNo1 = coNo1;
	}
	public String getCoNo2() {
		return coNo2;
	}
	public void setCoNo2(String coNo2) {
		this.coNo2 = coNo2;
	}
	public String getCoNo3() {
		return coNo3;
	}
	public void setCoNo3(String coNo3) {
		this.coNo3 = coNo3;
	}

}