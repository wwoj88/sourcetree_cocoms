package cocoms.copyright.web.form;

import java.util.*;

import cocoms.copyright.entity.*;

public class BoardForm extends BoardVO {

	private static final long serialVersionUID = 1L;

	private List<String> filename           = new ArrayList<String>(); 	// 
	private List<String> maskName           = new ArrayList<String>(); 	// 
	private List<String> filesize           = new ArrayList<String>(); 	// 

	private List<String> boardSeqNoList     = new ArrayList<String>(); 	//
	
	/*
	 * Getters and Setters
	 */
	public List<String> getFilename() {
		return filename;
	}
	public void setFilename(List<String> filename) {
		this.filename = filename;
	}
	public List<String> getBoardSeqNoList() {
		return boardSeqNoList;
	}
	public void setBoardSeqNoList(List<String> boardSeqNoList) {
		this.boardSeqNoList = boardSeqNoList;
	}
	public List<String> getMaskName() {
		return maskName;
	}
	public void setMaskName(List<String> maskName) {
		this.maskName = maskName;
	}
	public List<String> getFilesize() {
		return filesize;
	}
	public void setFilesize(List<String> filesize) {
		this.filesize = filesize;
	}	
}