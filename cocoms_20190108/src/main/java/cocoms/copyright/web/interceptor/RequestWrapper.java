package cocoms.copyright.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public final class RequestWrapper extends HttpServletRequestWrapper {

	public RequestWrapper(HttpServletRequest servletRequest) {
		super(servletRequest);
	}

	public String[] getParameterValues(String parameter) {

		String[] values = super.getParameterValues(parameter);
		if (values == null) {
			return null;
		}
		int count = values.length;
		String[] encodedValues = new String[count];
		for (int i = 0; i < count; i++) {
			encodedValues[i] = cleanXSS(values[i]);
		}
		return encodedValues;
	}

	public String getParameter(String parameter) {
		String value = super.getParameter(parameter);
		if (value == null) {
			return null;
		}
		return cleanXSS(value);
	}

	public String getHeader(String name) {
		String value = super.getHeader(name);
		if (value == null)
			return null;
		return cleanXSS(value);

	}

	private String cleanXSS(String value) {
		//System.out.println("VALUE1 :: "+value);
		value = value.replaceAll("<(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");
		value = value.replaceAll("</(S|s)(C|c)(R|r)(I|i)(P|p)(T|t)", "");

		value = value.replaceAll("<(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");
		value = value.replaceAll("</(O|o)(B|b)(J|j)(E|e)(C|c)(T|t)", "");

		value = value.replaceAll("<(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");
		value = value.replaceAll("</(A|a)(P|p)(P|p)(L|l)(E|e)(T|t)", "");

		value = value.replaceAll("<(E|e)(M|m)(B|b)(E|e)(D|d)", "");
		value = value.replaceAll("</(E|e)(M|m)(B|b)(E|e)(D|d)", "");

		value = value.replaceAll("<(F|f)(O|o)(R|r)(M|m)", "");
		value = value.replaceAll("</(F|f)(O|o)(R|r)(M|m)", "");
		value = value.replaceAll("<", "&lt;");
		value = value.replaceAll(">", "&gt;");
		value = value.replaceAll("\"", "&quot;");
		value = value.replaceAll("\'", "&#39;");
		

		value = value.replaceAll("\\\"", "");
		value = value.replaceAll("'", "'");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		value = value.replaceAll("[\\\"\\\'][\\s]*vbscript:(.*)[\\\"\\\']", "\"\"");
		value = value.replaceAll("/script", "");
		value = value.replaceAll("script", "");

		value = value.replaceAll("onload", "no_onload");
		value = value.replaceAll("expression", "no_expression");
		value = value.replaceAll("onmouseover", "no_onmouseover");
		value = value.replaceAll("onmouseout", "no_onmouseout");
		value = value.replaceAll("onclick", "no_onclick");
		value = value.replaceAll("iframe", "");
		value = value.replaceAll("object", "");
		value = value.replaceAll("embed", "");
		value = value.replaceAll("document.cookie", "");
		value = value.replaceAll("%", "");

		/*value = value.replaceAll("\"", "&quot;");
		value = value.replaceAll("\'", "&#39;");*/
		//System.out.println("VALUE :: "+value);
		return value;
	}
}
