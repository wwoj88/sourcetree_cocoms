package cocoms.copyright.web.interceptor;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cocoms.copyright.entity.LoginVO;

public class UserSecurityInterceptor extends HandlerInterceptorAdapter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		try {

			HttpSession session = request.getSession();
			
			
			Enumeration se = session.getAttributeNames();

		    

			while(se.hasMoreElements()){



			String getse = se.nextElement()+"";

			
			}


			LoginVO user = (LoginVO) session.getAttribute("SESS_USER");

			String requestUrl = request.getRequestURL().toString();

			/*
			 * if(requestUrl.contains("/companies")){
			 * 0E7B9DB02D003703A987F7A37572C471 return true;
			 * 
			 * }
			 */

			if (user == null) {

				String contextRoot = request.getContextPath();
				response.sendRedirect(contextRoot + "/login");

				return false;
			}

			
			if (MultiLoginPreventor.isUsing(user.getLoginId())) {

				String ip = request.getRemoteAddr();
		
				if (!ip.equals(session.getAttribute("SESS_IP"))) {
					String contextRoot = request.getContextPath();
					response.sendRedirect(contextRoot + "/login");
					session.invalidate();
					return false;

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		super.afterConcurrentHandlingStarted(request, response, handler);
	}
}
