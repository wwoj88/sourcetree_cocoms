package cocoms.copyright.web.interceptor;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpRequest;
 
 
public class CrossScriptingFilter implements Filter {
 
   

	private FilterConfig filterConfig =null;

	public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }
 
    public void destroy() {
        this.filterConfig = null;
    }
 
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
    	
		//((HttpServletResponse)response).setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload");
    	//((HttpServletResponse)response).setHeader("Set-Cookie", "SameSite=None; Secure;");
        chain.doFilter(new RequestWrapper((HttpServletRequest) request), response);
        
    }

}

