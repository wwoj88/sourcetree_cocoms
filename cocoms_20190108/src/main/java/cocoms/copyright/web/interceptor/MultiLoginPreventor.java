package cocoms.copyright.web.interceptor;

import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

public class MultiLoginPreventor {

	public static ConcurrentHashMap<String, HttpSession> loginUsers = new ConcurrentHashMap<String, HttpSession>();

	public static boolean findByLoginId(String loginId) {
		return loginUsers.containsKey(loginId);
	}

	public static void invalidateByLoginId(String loginId) {
		Enumeration<String> e = loginUsers.keys();

		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			
			if (key.equals(loginId)) {
				System.out.println("INVALIDATE ID!!");
				loginUsers.get(key).invalidate();

			}
		}
	}

    public static boolean isUsing(String userID){
        boolean isUsing = false;
        Enumeration e = loginUsers.keys();
      
        
        String key = "";
        while(e.hasMoreElements())
        {
            key = (String)e.nextElement();
 
            if (key.equals(userID)) {
                    isUsing = true;
            }
        }
        return isUsing;
    }
		
    
    
    
}


