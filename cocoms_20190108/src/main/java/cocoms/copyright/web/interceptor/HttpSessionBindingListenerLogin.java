package cocoms.copyright.web.interceptor;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
 
public class HttpSessionBindingListenerLogin implements HttpSessionBindingListener{
 
    @Override
    public void valueBound(HttpSessionBindingEvent event){
   
        if (MultiLoginPreventor.findByLoginId(event.getName())){
            MultiLoginPreventor.invalidateByLoginId(event.getName());
        }
        
        MultiLoginPreventor.loginUsers.put(event.getName(), event.getSession());
        
        
    }
 
    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        MultiLoginPreventor.loginUsers.remove(event.getName(), event.getSession());
        
    }

  
}

