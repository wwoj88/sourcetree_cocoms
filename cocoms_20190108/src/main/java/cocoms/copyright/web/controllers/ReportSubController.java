package cocoms.copyright.web.controllers;

import java.net.URLEncoder;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.entity.*;
import cocoms.copyright.report.ReportService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.ReportForm;

@Controller
@RequestMapping({"/report", "/reports"})
public class ReportSubController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** ReportService */
	@Resource(name = "reportService")
	protected ReportService reportService;
	
	/** UserService */
	@Resource(name = "userService")
	protected UserService userService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/sub")
	public String reportSub(HttpServletRequest request,
			@ModelAttribute ReportInfoVO searchVO, 
			ModelMap model) throws Exception {
		
		MemberVO member = CommonUtils.getMemberSession(request);
		System.out.println("MEMBER ::"+member.getAppNo2());
		if(member.getAppNo2()==null || member.getAppNo2().equals("")){
			model.put("icon", "icon_notice_n_3");
			model.put("message", "신고증이 발급된 업체만 이용할 수 있습니다.");
			return "comm/error";
		}
		if (!"1".equals(member.getConditionCd())) {
			model.put("icon", "icon_notice_n_3");
			model.put("message", "대리중개업체만 이용할 수 있습니다.");
			return "comm/error";
		}
		
		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("memberSeqNo", member.getMemberSeqNo());
		
		List<ReportStatVO> list = new ArrayList<ReportStatVO>();
		try {
			
			list = reportService.getSubReportSumStatuses(filter);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4))-1;
		boolean check = false;
		try {
			
			/*String rptYear = DateUtils.getCurrDateNumber().substring(0, 4);*/
			
			String rptYear = String.valueOf(year);
			Map<String, Object> map = reportService.getSubReport(member.getMemberSeqNo(), rptYear);
			ReportInfoVO report = (ReportInfoVO) map.get("report");
			
			LOGGER.info("report [{}]", report);
			if (report == null || StringUtils.isEmpty(report.getMemberSeqNo())) {
				check = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		try {
			
			ReportInfoVO report = reportService.getReportManageInfo();
			int start = Integer.valueOf(report.getStartDate());
			int end = Integer.valueOf(report.getEndDate());

			int date = Integer.valueOf(DateUtils.getCurrDateNumber().substring(4, 8));
			/*LOGGER.info("start [{}], end [{}], date [{}]", start, end, date);*/
			
			if (start <= date && end >= date) {
				check = check && true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("data", searchVO);
		model.put("list", list);
		model.put("member", member);
		model.put("reg_year", year);
		model.put("reg_auth", check);

		return "usr/report/sub_report";
	}

	@RequestMapping(value = "/sub/years/{year}", method = RequestMethod.GET)
	public String reportSubDetail(HttpServletRequest request,
			@PathVariable String year,
			
			@ModelAttribute ReportInfoVO searchVO,
			ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);
		
		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "usr/report/sub_report_view_year";
	}
	
	@RequestMapping(value = "/sub/years/{year}/save", method = RequestMethod.GET)
	public String reportSubExcel(HttpServletRequest request,
			@PathVariable String year,
			ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		ReportVO report = new ReportVO();
		List<ReportStatVO> list = new ArrayList<ReportStatVO>(); 
		try {
			
			Map<String, Object> data = reportService.getSubReport(memberSeqNo, year);
			report = (ReportVO) data.get("report");
			
			list = reportService.getSubReportStatuses(memberSeqNo, year);
		    
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		int inWorkNum = 0, outWorkNum = 0, workSum = 0;
		int inRental = 0, outRental = 0, rentalSum = 0, inChrg = 0, outChrg = 0, chrgSum = 0;
		float inChrgRate = 0, outChrgRate = 0, avgChrgRate = 0;
		try {
			System.out.println("testsetste+ :"+list);
			for (ReportStatVO item : list) {
				//System.out.println("testetst"+item.getInWorkNum());
				inWorkNum += Integer.valueOf(item.getInWorkNum());
				outWorkNum += Integer.valueOf(item.getOutWorkNum());
				workSum += Integer.valueOf(item.getWorkSum());
				
				inRental += Integer.valueOf(item.getInRental());
				outRental += Integer.valueOf(item.getOutRental());
				rentalSum += Integer.valueOf(item.getRentalSum());

				inChrg += Integer.valueOf(item.getInChrg());
				outChrg += Integer.valueOf(item.getOutChrg());
				chrgSum += Integer.valueOf(item.getChrgSum());

				inChrgRate += Float.valueOf(item.getInChrgRate());
				outChrgRate += Float.valueOf(item.getOutChrgRate());
				avgChrgRate += Float.valueOf(item.getAvgChrgRate());
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		String filename = "대리중개업체_실적보고_";
		filename += DateUtils.getCurrDateNumber()+".xls";
		filename = URLEncoder.encode(filename, "UTF-8");
		
		model.put("report", report);
		model.put("list", list);
		
		model.put("filename", filename);
		model.put("date", DateUtils.getCurDate());
		model.put("writingKindList", "전체");		// TODO 
		
/*		model.put("inWorkNum", "");		// TODO
		model.put("outWorkNum", "");		// TODO
		model.put("workSumCnt", "");		// TODO

		model.put("inRentalCnt", "");		// TODO
		model.put("outRentalCnt", "");		// TODO
		model.put("rentalSumCnt", "");		// TODO
		model.put("inChrgCnt", "");		// TODO
		model.put("outChrgCnt", "");		// TODO
		model.put("chrgSumCnt", "");		// TODO
*/
		
		Map<String, Object> sum = new HashMap<String, Object>();
		sum.put("inWorkNum", inWorkNum);
		sum.put("outWorkNum", outWorkNum);
		sum.put("workSum", workSum);

		sum.put("inRental", inRental);
		sum.put("outRental", outRental);
		sum.put("rentalSum", rentalSum);

		sum.put("inChrg", inChrg);
		sum.put("outChrg", outChrg);
		sum.put("chrgSum", chrgSum);

		sum.put("inChrgRate", inChrgRate);
		sum.put("outChrgRate", outChrgRate);
		sum.put("avgChrgRate", avgChrgRate);

		model.put("sum", sum);
		return "adm/report/sub_report_excel2";
	}
	
	@RequestMapping(value = {"/sub/years/{year}/edit", "/sub/years/{year}/new"}, method = RequestMethod.GET)
	public String reportSubDetailModifyForm(HttpServletRequest request,
			@PathVariable String year,
			
			@ModelAttribute ReportInfoVO searchVO,
			ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);
		
		return "usr/report/sub_report_view_year_edit";
	}
	
	@RequestMapping(value = "/sub/years/{year}", method = RequestMethod.POST)
	public String reportSubDetailModify(HttpServletRequest request,			@PathVariable String year,			@ModelAttribute ReportForm form,			
			@ModelAttribute ReportInfoVO searchVO,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		String memberSeqNo = CommonUtils.getMemberSeqNo(request);
        
		form.setMemberSeqNo(memberSeqNo);
		form.setRptYear(year);
		
		List<ReportStatVO> reportStatusList = new ArrayList<ReportStatVO>(); 
		for (int i=0; i<form.getWritingKind().size(); i++) {
			
			ReportStatVO reportStat = new ReportStatVO();
			reportStat.setMemberSeqNo(memberSeqNo);
			reportStat.setRptYear(year);
			reportStat.setWritingKind(form.getWritingKind().get(i));
			reportStat.setPermKind(form.getPermKind().get(i));

			reportStat.setInWorkNum(form.getInWorkNum().get(i));
			reportStat.setOutWorkNum(form.getOutWorkNum().get(i));
			reportStat.setWorkSum(form.getWorkSum().get(i));

			reportStat.setInRental(form.getInRental().get(i));
			reportStat.setOutRental(form.getOutRental().get(i));
			reportStat.setRentalSum(form.getRentalSum().get(i));
			reportStat.setInChrg(form.getInChrg().get(i));
			reportStat.setOutChrg(form.getOutChrg().get(i));
			reportStat.setChrgSum(form.getChrgSum().get(i));
			reportStat.setInChrgRate(form.getInChrgRate().get(i));
			reportStat.setOutChrgRate(form.getOutChrgRate().get(i));
			reportStat.setAvgChrgRate(form.getAvgChrgRate().get(i));

			reportStatusList.add(reportStat);
		}

		try {

			reportService.saveSubReport(form);
			
			reportService.saveSubReportStatuses(reportStatusList);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "redirect:/report/sub/years/"+year;
	}
	
	/*private Map<String, Object> getReportSubDetail(String memberSeqNo, String year) throws Exception {

//		ReportInfoVO report = new ReportInfoVO();
		List<ReportStatVO> list = new ArrayList<ReportStatVO>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = reportService.getSubReport(memberSeqNo, year);
			
			list = reportService.getSubReportStatuses(memberSeqNo, year);
			
			map.put("member", userService.getMember(memberSeqNo));
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		int inWorkNum = 0, outWorkNum = 0, workSum = 0;
		int inRental = 0, outRental = 0, rentalSum = 0, inChrg = 0, outChrg = 0, chrgSum = 0;
		float inChrgRate = 0, outChrgRate = 0, avgChrgRate = 0;
		for (ReportStatVO item : list) {
			inWorkNum += Integer.valueOf(item.getInWorkNum());
			outWorkNum += Integer.valueOf(item.getOutWorkNum());
			workSum = +Integer.valueOf(item.getWorkSum());
			
			inRental = +Integer.valueOf(item.getInRental());
			outRental = +Integer.valueOf(item.getOutRental());
			rentalSum = +Integer.valueOf(item.getRentalSum());

			inChrg = +Integer.valueOf(item.getInChrg());
			outChrg = +Integer.valueOf(item.getOutChrg());
			chrgSum = +Integer.valueOf(item.getChrgSum());

			inChrgRate = +Float.valueOf(item.getInChrgRate());
			outChrgRate = +Float.valueOf(item.getOutChrgRate());
			avgChrgRate = +Float.valueOf(item.getAvgChrgRate());
		}
		
		Map<String, Object> ret = new HashMap<String, Object>();
//		ret.put("report", report);
		ret.putAll(map);
		ret.put("list", list);
		ret.put("year", year);
		
		Map<String, Object> sum = new HashMap<String, Object>();
		sum.put("inWorkNum", inWorkNum);
		sum.put("outWorkNum", outWorkNum);
		sum.put("workSum", workSum);

		sum.put("inRental", inRental);
		sum.put("outRental", outRental);
		sum.put("rentalSum", rentalSum);

		sum.put("inChrg", inChrg);
		sum.put("outChrg", outChrg);
		sum.put("chrgSum", chrgSum);

		sum.put("inChrgRate", inChrgRate);
		sum.put("outChrgRate", outChrgRate);
		sum.put("avgChrgRate", avgChrgRate);

		ret.put("sum", sum);

		return ret;
	}*/
	
	private Map<String, Object> getReportSubDetail(String memberSeqNo, String year) throws Exception {
		LOGGER.info("member seq no [{}], year [{}]", memberSeqNo, year);

//		ReportInfoVO report = new ReportInfoVO();
		List<ReportStatVO> list = new ArrayList<ReportStatVO>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = reportService.getSubReport(memberSeqNo, year);
			
			list = reportService.getSubReportStatuses(memberSeqNo, year);
			
			map.put("member", userService.getMember(memberSeqNo));
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		int inWorkNum = 0, outWorkNum = 0, workSum = 0;
		int inRental = 0, outRental = 0, rentalSum = 0, inChrg = 0, outChrg = 0, chrgSum = 0;
		float inChrgRate = 0, outChrgRate = 0, avgChrgRate = 0;
		try {

			for (ReportStatVO item : list) {
				inWorkNum += Integer.valueOf(item.getInWorkNum());
				outWorkNum += Integer.valueOf(item.getOutWorkNum());
				workSum += Integer.valueOf(item.getWorkSum());
				
				inRental += Integer.valueOf(item.getInRental());
				outRental += Integer.valueOf(item.getOutRental());
				rentalSum += Integer.valueOf(item.getRentalSum());

				inChrg += Integer.valueOf(item.getInChrg());
				outChrg += Integer.valueOf(item.getOutChrg());
				chrgSum += Integer.valueOf(item.getChrgSum());

				inChrgRate += Float.valueOf(item.getInChrgRate());
				outChrgRate += Float.valueOf(item.getOutChrgRate());
				avgChrgRate += Float.valueOf(item.getAvgChrgRate());
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		Map<String, Object> ret = new HashMap<String, Object>();
//		ret.put("report", report);
		ret.putAll(map);
		ret.put("list", list);
		ret.put("year", year);
		
		Map<String, Object> sum = new HashMap<String, Object>();
		sum.put("inWorkNum", inWorkNum);
		sum.put("outWorkNum", outWorkNum);
		sum.put("workSum", workSum);

		sum.put("inRental", inRental);
		sum.put("outRental", outRental);
		sum.put("rentalSum", rentalSum);

		sum.put("inChrg", inChrg);
		sum.put("outChrg", outChrg);
		sum.put("chrgSum", chrgSum);

		sum.put("inChrgRate", inChrgRate);
		sum.put("outChrgRate", outChrgRate);
		sum.put("avgChrgRate", avgChrgRate);

		ret.put("sum", sum);

		return ret;
	}
	
	
}
