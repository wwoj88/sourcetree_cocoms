package cocoms.copyright.web.controllers;

import java.io.*;
import java.util.*;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import cocoms.copyright.board.BoardFileDataMapper;
import cocoms.copyright.board.BoardMapper;
import cocoms.copyright.board.BoardService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.entity.BoardVO;
import cocoms.copyright.log.ActionLogService;
import egovframework.rte.fdl.property.EgovPropertyService;

@Controller
public class DownloadController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;
	@Resource(name = "onlineService")
	private BoardService onlineService;

	@Resource(name = "boardMapper")
	private BoardMapper boardDAO;

	@Resource(name = "boardFileDataMapper")
	private BoardFileDataMapper boardFileDataDAO;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public void downloadFile(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "filename", defaultValue = "") String filename, @RequestParam(value = "boardSeqNo", defaultValue = "") String boardSeqNo,
			@RequestParam(value = "origName", defaultValue = "") String origName) throws Exception {
		//System.out.println("filename++: +" + filename);

		filename = filename.replace("..", "");
		filename = filename.replaceAll("%", "");
		filename = filename.replaceAll("%2F", "");
		filename = filename.replaceAll("\\\\", "");

		//System.out.println("file++:" + filename);
		String rootPath = propertiesService.getString("filepath");

		File file = new File(rootPath + "/" + filename);

		if (!file.exists()) {
			/*
			 * String errorMessage = "파일이 존재하지 않습니다.";
			 * logger.error(errorMessage);
			 * 
			 * response.setContentType("text/html;charset=utf-8");
			 * 
			 * OutputStream outputStream = response.getOutputStream();
			 * outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")
			 * )); outputStream.close();
			 */
			throw new Exception();
		}
		
		
		if (!boardSeqNo.equals("") && boardSeqNo != null) {
			Map<String, Object> filter = new HashMap<String, Object>();
			filter.put("boardSeqNo", boardSeqNo);
			filter.put("filename", filename);
			actionLogService.hitDownCount(boardSeqNo);

			String orgFName = boardFileDataDAO.selectName(filter);
			//System.out.println(orgFName);

			String header = request.getHeader("User-Agent");

			if (header.contains("MSIE") || header.contains("Trident")) {
				orgFName = URLEncoder.encode(orgFName, "UTF-8").replaceAll("\\+", "%20");
				response.setHeader("Content-Disposition", "attachment;filename=" + orgFName + ";");
			} else {
				orgFName = new String(orgFName.getBytes("UTF-8"), "ISO-8859-1");
				response.setHeader("Content-Disposition", "attachment; filename=\"" + orgFName + "\"");
			}

			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			if (mimeType == null) {
				//			logger.info("mimetype is not detectable, will take default");
				mimeType = "application/octet-stream";
			}
			logger.info("DownLoad");
			logger.info("mimetype : " + mimeType);

			/* response.setContentType(mimeType); */

			/*
			 * response.setHeader("Content-Disposition",
			 * String.format("attachment; filename=\"" + file.getName() +
			 * "\""));
			 */
			//response.setHeader("Content-Disposition", "attachment; filename="+orgFName);
			//response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + origfilename +"\""));
			response.setContentType("application/octet-stream");
			response.setContentLength((int) file.length());

			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

			FileCopyUtils.copy(inputStream, response.getOutputStream());

			// save log
			actionLogService.saveLog(request, "download");

		} else if (boardSeqNo.equals("") || boardSeqNo == null) {

			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			if (mimeType == null) {
				//			logger.info("mimetype is not detectable, will take default");
				mimeType = "application/octet-stream";
			}
			logger.info("DownLoad");
			logger.info("mimetype : " + mimeType);

			/* response.setContentType(mimeType); */

			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
			//response.setHeader("Content-Disposition", "attachment; filename="+orgFName);
			//response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + origfilename +"\""));
			response.setContentType("application/octet-stream");
			response.setContentLength((int) file.length());

			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

			FileCopyUtils.copy(inputStream, response.getOutputStream());

			// save log
			actionLogService.saveLog(request, "download");

		}
		
	}

	@RequestMapping(value = "/download2", method = RequestMethod.GET)
	public void downloadFile2(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "filename", defaultValue = "") String filename) throws Exception {

		//System.out.println("filename++: +" + filename);

		filename = filename.replace("..", "");
		filename = filename.replaceAll("%", "");
		filename = filename.replaceAll("%2F", "");
		filename = filename.replaceAll("\\\\", "");

		//System.out.println("file++:" + filename);

		String rootPath = propertiesService.getString("filepath2");

		File file = new File(rootPath + "/" + filename);
		//System.out.println(rootPath + "/" + filename);
		if (!file.exists()) {
			/*
			 * String errorMessage = "파일이 존재하지 않습니다.";
			 * logger.error(errorMessage);
			 * 
			 * response.setContentType("text/html;charset=utf-8");
			 * 
			 * OutputStream outputStream = response.getOutputStream();
			 * outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")
			 * )); outputStream.close();
			 * 
			 * return;
			 */

			throw new Exception();
		}

		String mimeType = URLConnection.guessContentTypeFromName(file.getName());
		if (mimeType == null) {
			//			logger.info("mimetype is not detectable, will take default");
			mimeType = "application/octet-stream";
		}
		logger.info("DownLoad2");
		logger.info("mimetype : " + mimeType);

		/* response.setContentType(mimeType); */

		response.setHeader("Accept-Ranges", "bytes");

		String userAgent = request.getHeader("User-Agent");

		boolean ie = (userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("Trident") > -1);

		if (filename.equals("1.zip")) {
			filename = "년도별 저작권대리중개업 업무보고(실적보고) 매뉴얼 안내문.zip";
		} else if (filename.equals("2.zip")) {
			filename = "저작권대리중개업 폐업신고서.zip";
		} else if (filename.equals("3.zip")) {
			filename = "(표준약관) 저작권대리중개업 업무규정 및 계약약관.zip";
		} else if (filename.equals("4.zip")) {
			filename = "(안내) 저작권대리중개업 금지행위.zip";
		} else if (filename.equals("5.zip")) {
			filename = "(안내) 저작권대리중개업_표준계약서 작성방법.zip";
		} else if (filename.equals("6.zip")) {
			filename = "(작성양식) 이력서양식.zip";
		} else if (filename.equals("7.zip")) {
			filename = "(작성양식) 재무제표_미제출_사유서.zip";
		} else if (filename.equals("8.zip")) {
			filename = "저작권대리중개업 신고증 분실신고서.zip";
		} else if (filename.equals("8.xls")) {
			filename = "저작물서식.xls";
		} else if (filename.equals("9.zip")) {
			filename = "국내저작물서식.zip";
		} else if (filename.equals("10.zip")) {
			filename = "해외저작물서식.zip";
		}else if (filename.equals("11.zip")) {
			filename = "대리중개계약서 및 이용허락계약서.zip";
		}

		if (ie) {
			filename = URLEncoder.encode(filename, "utf-8").replaceAll("\\+", "%20");
		} else {
			filename = new String(String.valueOf(filename).getBytes("utf-8"), "iso-8859-1");
		}

		//System.out.println(filename);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");

		//response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + origfilename +"\""));
		response.setContentType("application/octet-stream");
		response.setContentLength((int) file.length());

		InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

		FileCopyUtils.copy(inputStream, response.getOutputStream());

		// save log
		actionLogService.saveLog(request, "download");
	}

	@RequestMapping(value = "/download3", method = RequestMethod.GET)
	public void downloadFile3(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "gercd", defaultValue = "") String gercd) throws Exception {

		//System.out.println("filename++: +" + gercd);

		gercd = gercd.replace("..", "");
		gercd = gercd.replaceAll("%", "");
		gercd = gercd.replaceAll("%2F", "");
		gercd = gercd.replaceAll("\\\\", "");

		//System.out.println("gercd++:" + gercd);

		String rootPath = propertiesService.getString("filepath2");

		if (gercd.equals("1")) {
			gercd = "upload_sample01_music.xlsx";
		} else if (gercd.equals("2")) {
			gercd = "upload_sample02_literary_work.xlsx";
		} else if (gercd.equals("3")) {
			gercd = "upload_sample03_script.xlsx";
		} else if (gercd.equals("4")) {
			gercd = "upload_sample04_movie.xlsx";
		} else if (gercd.equals("5")) {
			gercd = "upload_sample05_broadcast.xlsx";
		} else if (gercd.equals("6")) {
			gercd = "upload_sample06_news.xlsx";
		} else if (gercd.equals("7")) {
			gercd = "upload_sample07_art.xlsx";
		} else if (gercd.equals("8")) {
			gercd = "upload_sample08_image.xlsx";
		} else if (gercd.equals("99")) {
			gercd = "upload_sample99_orher.xlsx";
		}

		File file = new File(rootPath + "/" + gercd);
		//System.out.println(rootPath + "/" + gercd);
		if (!file.exists()) {
			/*
			 * String errorMessage = "파일이 존재하지 않습니다.";
			 * logger.error(errorMessage);
			 * 
			 * response.setContentType("text/html;charset=utf-8");
			 * 
			 * OutputStream outputStream = response.getOutputStream();
			 * outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")
			 * )); outputStream.close();
			 * 
			 * return;
			 */

			throw new Exception();
		}

		String mimeType = URLConnection.guessContentTypeFromName(file.getName());
		if (mimeType == null) {
			//			logger.info("mimetype is not detectable, will take default");
			mimeType = "application/octet-stream";
		}
		logger.info("DownLoad3");
		logger.info("mimetype : " + mimeType);

		/* response.setContentType(mimeType); */

		response.setHeader("Accept-Ranges", "bytes");

		String userAgent = request.getHeader("User-Agent");

		boolean ie = (userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("Trident") > -1);

		if (ie) {
			gercd = URLEncoder.encode(gercd, "utf-8").replaceAll("\\+", "%20");
		} else {
			gercd = new String(String.valueOf(gercd).getBytes("utf-8"), "iso-8859-1");
		}

		//System.out.println(filename);
		response.setHeader("Content-Disposition", "attachment; filename=\"" + gercd + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");

		//response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + origfilename +"\""));
		response.setContentType("application/octet-stream");
		response.setContentLength((int) file.length());

		InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

		FileCopyUtils.copy(inputStream, response.getOutputStream());

		// save log
		//actionLogService.saveLog(request, "양식다운로드");
	}

	//application/json
	@RequestMapping(value = "/getUrl", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String test22(HttpServletRequest request, @RequestParam(value = "data", required = false, defaultValue = "") String data) throws Exception {

		if (data.equals("") || data == null) {
			data = "http://222.231.43.109:8085/rxent/cdoc/eform/rxent/run/html.jsp";

		} else {
			data = "http://222.231.43.109:8085/smartcert/cdoc/eform/rxscert/run/smartcert.jsp";
		}

		return data;

	}

}
