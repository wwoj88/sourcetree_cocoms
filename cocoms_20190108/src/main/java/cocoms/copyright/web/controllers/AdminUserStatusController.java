package cocoms.copyright.web.controllers;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.application.ApplHistoryService;
import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.report.ReportService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin/status")
public class AdminUserStatusController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** ApplicationService */
	@Resource(name = "applicationService")
	private ApplicationService applicationService;

	/** ReportService */
	@Resource(name = "reportService")
	private ReportService reportService;

	/** CodeService */
	@Resource(name = "codeService")
	private CodeService codeService;

	/** ApplHistoryService */
	@Resource(name = "applHistoryService")
	private ApplHistoryService applHistoryService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = "/companies", method = RequestMethod.GET)
	public String companyList(HttpServletRequest request,
			//			@ModelAttribute UserForm searchVO,
			@ModelAttribute MemberVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		/** execute service */
		List<MemberVO> list = new ArrayList<MemberVO>();
		try {

			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("order", "appNo");

			Map<String, Object> data = userService.getMembers(filter);
			list = (List<MemberVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		/** make return value */
		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "adm/stat/stat_company";
	}

	@RequestMapping(value = "/companies/excel", method = RequestMethod.GET)
	public void companyListExcel(HttpServletRequest request, HttpServletResponse response, @ModelAttribute MemberVO searchVO, Model model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		List<MemberVO> list = new ArrayList<MemberVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);
			filter.put("order", "appNo");

			Map<String, Object> data = userService.getMembers(filter);
			list = (List<MemberVO>) data.get("list");
			int total = (int) data.get("total");

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = DateUtils.getCurDateTime() + ".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("신고번호");
			col.add("회사명");
			col.add("대표자명");
			col.add("전화번호");
			col.add("홈페이지");
			col.add("주소");

			list2.add(col);
		}
		int cnt = 0;
		for (MemberVO item : list) {
			String no = String.valueOf(list.size() - cnt);
			String addr = StringUtils.isNotEmpty(item.getTrustZipcode()) ? "(" + item.getTrustZipcode() + ") " : "";
			addr += item.getTrustSido();
			addr += item.getTrustGugun();
			addr += item.getTrustDong();
			addr += item.getTrustBunji();
			addr += item.getTrustDetailAddr();

			List<String> col = new ArrayList<String>();
			col.add(no); // 번호
			col.add(item.getAppNoStr()); // 신고번호
			col.add(item.getCoName()); // 신고번호
			col.add(item.getCeoName()); // 회사명
			col.add(item.getTrustTel()); // 전화번호
			col.add(item.getTrustUrl()); // 홈페이지
			col.add(addr); // 주소

			list2.add(col);
			cnt++;
		}

		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}

	public static void detectCharset(String sido) throws Exception {
		String text = sido;
		String encode = "";
		String[] charsets = { "UTF-8", "EUC-KR", "ISO-8859-1", "CP1251", "KSC5601" };

		for (String charset : charsets) {
			encode = URLEncoder.encode(text, charset);
			System.out.println(("origin[" + text + "], " + "encoded[" + encode + "], charset[" + charset + "]"));
		}
	}

	@RequestMapping(value = "/zip_code", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getZipCode(HttpServletRequest request, @RequestParam(value = "sido", defaultValue = "", required = false) String sido, ModelMap model) throws Exception {
		LOGGER.info("sido [{}]", sido);

		List<ZipCodeVO> list = new ArrayList<ZipCodeVO>();
		try {

			list = codeService.getZipCodes(sido);

			for (ZipCodeVO item : list) {
				item.setSido(URLEncoder.encode(item.getSido(), "UTF-8"));
				item.setGugun(URLEncoder.encode(item.getGugun(), "UTF-8"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		return CommonUtils.makeSuccess(map);
	}

	@RequestMapping(value = "/stats", method = RequestMethod.GET)
	public String stats(HttpServletRequest request, @RequestParam(value = "conditionCd", defaultValue = "1", required = false) String conditionCd, @RequestParam(value = "fromYear", defaultValue = "", required = false) String fromYear,
			@RequestParam(value = "toYear", defaultValue = "", required = false) String toYear, @RequestParam(value = "type", defaultValue = "1", required = false) String type, ModelMap model) throws Exception {
		/* LOGGER.info("params [{}] [{}] [{}] ", fromYear, toYear, type); */

		model.putAll(statsSub(conditionCd, fromYear, toYear, type));

		
		// 2: 신탁단체
		if ("2".equals(conditionCd)) {

			switch (type) {
			case "1":
				return "adm/stat/stat_trust_stat01";
			case "2":
				return "adm/stat/stat_trust_stat02";
			case "3":
				return "adm/stat/stat_trust_stat03";
			case "4":
				return "adm/stat/stat_trust_stat04";
			case "5":
				return "adm/stat/stat_trust_stat05";
			case "6":
				return "adm/stat/stat_trust_stat06";
			case "7":
				return "adm/stat/stat_trust_stat07";
			case "8":
				return "adm/stat/stat_trust_stat08";
			case "9":
				return "adm/stat/stat_trust_stat09";
			case "10":
				return "adm/stat/stat_trust_stat10";
			case "11":
				return "adm/stat/stat_trust_stat11";
			case "12":
				return "adm/stat/stat_trust_stat12";
			}
		}
		// 1: 대리중개
		else {

			switch (type) {
			case "1":
				return "adm/stat/stat_sub_stat01";
			case "2":
				return "adm/stat/stat_sub_stat02";
			case "3":
				return "adm/stat/stat_sub_stat03";
			case "4":
				return "adm/stat/stat_sub_stat04";
			case "5":
				return "adm/stat/stat_sub_stat05";
			case "6":
				return "adm/stat/stat_sub_stat06";
			case "7":
				return "adm/stat/stat_sub_stat07";

			default:
				model.put("type", "1");
				return "adm/stat/stat_sub_stat01";
			}
		}

		return "adm/stat/stat_trust_stat01";
	}

	@RequestMapping(value = "/stats/detail", method = RequestMethod.GET)
	public String statsdetail(HttpServletRequest request, @RequestParam(value = "conditionCd", defaultValue = "1", required = false) String conditionCd, @RequestParam(value = "fromYear", defaultValue = "", required = false) String fromYear,
			@RequestParam(value = "toYear", defaultValue = "", required = false) String toYear, @RequestParam(value = "type", defaultValue = "1", required = false) String type, ModelMap model) throws Exception {

		model.putAll(statsSubDetail(conditionCd, fromYear, toYear, type));

		// 2: 신탁단체
		if ("2".equals(conditionCd)) {

			switch (type) {
			case "1":
				return "adm/stat/stat_trust_stat01";
			case "2":
				return "adm/stat/stat_trust_stat02";
			case "3":
				return "adm/stat/stat_trust_stat03";
			case "4":
				return "adm/stat/stat_trust_stat04";
			case "5":
				return "adm/stat/stat_trust_stat05";
			case "6":
				return "adm/stat/stat_trust_stat06";
			case "7":
				return "adm/stat/stat_trust_stat07";
			case "8":
				return "adm/stat/stat_trust_stat08";
			case "9":
				return "adm/stat/stat_trust_stat09";
			case "10":
				return "adm/stat/stat_trust_stat10";
			case "11":
				return "adm/stat/stat_trust_stat11";
			case "12":
				return "adm/stat/stat_trust_stat12";
			}
		}
		// 1: 대리중개
		else {

			switch (type) {
			case "1":
				return "adm/stat/stat_sub_stat_detail01";
			case "2":
				return "adm/stat/stat_sub_stat_detail02";
			case "3":
				return "adm/stat/stat_sub_stat_detail03";
			case "4":
				return "adm/stat/stat_sub_stat_detail04";
			case "5":
				return "adm/stat/stat_sub_stat_detail05";
			case "6":
				return "adm/stat/stat_sub_stat_detail06";
			case "7":
				return "adm/stat/stat_sub_stat07";

			default:
				model.put("type", "1");
				return "adm/stat/stat_sub_stat01";
			}
		}

		return "adm/stat/stat_trust_stat01";

	}

	@RequestMapping(value = "/stats/save", method = RequestMethod.GET)
	public String statsExcel(HttpServletRequest request, @RequestParam(value = "conditionCd", defaultValue = "2", required = false) String conditionCd, @RequestParam(value = "fromYear", defaultValue = "", required = false) String fromYear,
			@RequestParam(value = "toYear", defaultValue = "", required = false) String toYear, @RequestParam(value = "type", defaultValue = "1", required = false) String type, ModelMap model) throws Exception {
		/* LOGGER.info("params [{}] [{}] [{}] ", fromYear, toYear, type); */

		model.putAll(statsSub(conditionCd, fromYear, toYear, type));

		// 2: 신탁단체
		if ("2".equals(conditionCd)) {

			switch (type) {
			case "1":
				model.put("filename", getFilename("저작권종류별신탁사용료"));
				return "adm/stat/stat_trust_stat01_excel";
			case "2":
				model.put("filename", getFilename("신탁단체별신탁사용료"));
				return "adm/stat/stat_trust_stat02_excel";
			case "3":
				model.put("filename", getFilename("연도별신탁사용료"));
				return "adm/stat/stat_trust_stat03_excel";
			case "4":
				model.put("filename", getFilename("저작권종류별보상금"));
				return "adm/stat/stat_trust_stat04_excel";
			case "5":
				model.put("filename", getFilename("신탁단체별보상금"));
				return "adm/stat/stat_trust_stat05_excel";
			case "6":
				model.put("filename", getFilename("연도별보상금"));
				return "adm/stat/stat_trust_stat06_excel";
			case "7":
				model.put("filename", getFilename("저작권종류별보상금"));
				return "adm/stat/stat_trust_stat07_excel";
			case "8":
				model.put("filename", getFilename("신탁단체별보상금"));
				return "adm/stat/stat_trust_stat08_excel";
			case "9":
				model.put("filename", getFilename("연도별보상금"));
				return "adm/stat/stat_trust_stat09_excel";
			case "10":
				model.put("filename", getFilename("판매용음반보상금"));
				return "adm/stat/stat_trust_stat10_excel";
			case "11":
				model.put("filename", getFilename("신탁단체별보상금"));
				return "adm/stat/stat_trust_stat11_excel";
			case "12":
				model.put("filename", getFilename("연도별보상금"));
				return "adm/stat/stat_trust_stat12_excel";
			}
		}
		// 1: 대리중개
		else {

			switch (type) {
			case "1":
				model.put("filename", getFilename("직원수별_대리중개_업체수"));
				return "adm/stat/stat_sub_stat01_excel";
			case "2":
				model.put("filename", getFilename("매출액_별대리중개_업체수"));
				return "adm/stat/stat_sub_stat02_excel";
			case "3":
				model.put("filename", getFilename("대리중개_저작물종류별저작물수"));
				return "adm/stat/stat_sub_stat03_excel";
			case "4":
				model.put("filename", getFilename("대리중개_저작권유형별저작물수"));
				return "adm/stat/stat_sub_stat04_excel";
			case "5":
				model.put("filename", getFilename("대리중개_저작물종류별사용료"));
				return "adm/stat/stat_sub_stat05_excel";
			case "6":
				model.put("filename", getFilename("대리중개_저작물종류별수수료"));
				return "adm/stat/stat_sub_stat06_excel";
			case "7":
				model.put("filename", getFilename("대리중개_사업실적"));
				return "adm/stat/stat_sub_stat07_excel";

			default:
				model.put("type", "1");
				return "adm/stat/stat_sub_stat01_excel";
			}
		}

		model.put("filename", getFilename("저작권종류별신탁사용료"));
		return "adm/stat/stat_trust_stat01_excel";
	}

	@RequestMapping(value = "/statsdetail/save", method = RequestMethod.GET)
	public String statsdeatilExcel(HttpServletRequest request, @RequestParam(value = "conditionCd", defaultValue = "1", required = false) String conditionCd,
			@RequestParam(value = "fromYear", defaultValue = "", required = false) String fromYear, @RequestParam(value = "toYear", defaultValue = "", required = false) String toYear,
			@RequestParam(value = "type", defaultValue = "1", required = false) String type, ModelMap model) throws Exception {

		model.putAll(statsSubDetail(conditionCd, fromYear, toYear, type));

		// 2: 신탁단체
		if ("2".equals(conditionCd)) {

			switch (type) {
			case "1":
				model.put("filename", getFilename("저작권종류별신탁사용료"));
				return "adm/stat/stat_trust_stat01_excel";
			case "2":
				model.put("filename", getFilename("신탁단체별신탁사용료"));
				return "adm/stat/stat_trust_stat02_excel";
			case "3":
				model.put("filename", getFilename("연도별신탁사용료"));
				return "adm/stat/stat_trust_stat03_excel";
			case "4":
				model.put("filename", getFilename("저작권종류별보상금"));
				return "adm/stat/stat_trust_stat04_excel";
			case "5":
				model.put("filename", getFilename("신탁단체별보상금"));
				return "adm/stat/stat_trust_stat05_excel";
			case "6":
				model.put("filename", getFilename("연도별보상금"));
				return "adm/stat/stat_trust_stat06_excel";
			case "7":
				model.put("filename", getFilename("저작권종류별보상금"));
				return "adm/stat/stat_trust_stat07_excel";
			case "8":
				model.put("filename", getFilename("신탁단체별보상금"));
				return "adm/stat/stat_trust_stat08_excel";
			case "9":
				model.put("filename", getFilename("연도별보상금"));
				return "adm/stat/stat_trust_stat09_excel";
			case "10":
				model.put("filename", getFilename("판매용음반보상금"));
				return "adm/stat/stat_trust_stat10_excel";
			case "11":
				model.put("filename", getFilename("신탁단체별보상금"));
				return "adm/stat/stat_trust_stat11_excel";
			case "12":
				model.put("filename", getFilename("연도별보상금"));
				return "adm/stat/stat_trust_stat12_excel";
			}
		}
		// 1: 대리중개
		else {

			switch (type) {
			case "1":
				model.put("filename", getFilename("대리중개_업체별_직원수"));
				return "adm/stat/stat_sub_stat01_detail_excel";
			case "2":
				model.put("filename", getFilename("대리중개_업체별_매출액"));
				return "adm/stat/stat_sub_stat02_detail_excel";
			case "3":
				model.put("filename", getFilename("대리중개_업체별_저작물종류별저작물수"));
				return "adm/stat/stat_sub_stat03_detail_excel";
			case "4":
				model.put("filename", getFilename("대리중개_저작권유형별저작물수"));
				return "adm/stat/stat_sub_stat04_detail_excel";
			case "5":
				model.put("filename", getFilename("대리중개_저작물종류별사용료"));
				return "adm/stat/stat_sub_stat05_detail_excel";
			case "6":
				model.put("filename", getFilename("대리중개_저작물종류별수수료"));
				return "adm/stat/stat_sub_stat06_detail_excel";
			case "7":
				model.put("filename", getFilename("대리중개_사업실적"));
				return "adm/stat/stat_sub_stat07_excel";

			default:
				model.put("type", "1");
				return "adm/stat/stat_sub_stat01_excel";
			}
		}

		model.put("filename", getFilename("저작권종류별신탁사용료"));
		return "adm/stat/stat_trust_stat01_excel";
	}

	private String getFilename(String str) {

		String filename = str + "_" + DateUtils.getCurrDateNumber() + ".xls";

		try {
			filename = URLEncoder.encode(filename, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return filename;
	}

	private Map<String, Object> statsSub(String conditionCd, String fromYear, String toYear, String type) throws Exception {
		/* LOGGER.info("params [{}] [{}] [{}] ", fromYear, toYear, type); */
	
		if (StringUtils.isEmpty(fromYear)) {
			fromYear =   DateUtils.getCurrDateNumber().substring(0, 4);
			int fr=Integer.parseInt(fromYear)-1;
			fromYear = Integer.toString(fr);
			toYear = DateUtils.getCurrDateNumber().substring(0, 4);
	
		}

		/** execute service */
		List<ReportStat2VO> list = new ArrayList<ReportStat2VO>();
		try {

			Map<String, Object> filter = new HashMap<String, Object>();
			filter.put("fromYear", fromYear);
			filter.put("toYear", toYear);
			filter.put("type", type);

			list = "2".equals(conditionCd) ? reportService.getTrustReportStats(filter, Integer.valueOf(type)) : reportService.getSubReportStats(filter, Integer.valueOf(type));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> sum = new HashMap<String, Object>();
		float sumCnt = 0;
		float sum11 = 0, sum12 = 0, sum13 = 0;
		float sum21 = 0, sum22 = 0, sum23 = 0;
		float sum31 = 0, sum32 = 0, sum33 = 0;
		float sum41 = 0, sum42 = 0, sum43 = 0;
		float sum51 = 0, sum52 = 0, sum53 = 0;
		for (ReportStat2VO item : list) {

			sumCnt += Float.valueOf(StringUtils.defaultIfEmpty(item.getCnt(), "0"));

			sum11 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal11(), "0"));
			sum12 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal12(), "0"));
			sum13 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal13(), "0"));

			sum21 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal21(), "0"));
			sum22 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal22(), "0"));
			sum23 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal23(), "0"));

			sum31 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal31(), "0"));
			sum32 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal32(), "0"));
			sum33 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal33(), "0"));

			sum41 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal41(), "0"));
			sum42 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal42(), "0"));
			sum43 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal43(), "0"));

			sum51 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal51(), "0"));
			sum52 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal52(), "0"));
			sum53 += Float.valueOf(StringUtils.defaultIfEmpty(item.getVal53(), "0"));
		}
		sum.put("cnt", sumCnt);

		sum.put("val11", sum11);
		sum.put("val12", sum12);
		sum.put("val13", sum13);

		sum.put("val21", sum21);
		sum.put("val22", sum22);
		sum.put("val23", sum23);

		sum.put("val31", sum31);
		sum.put("val32", sum32);
		sum.put("val33", sum33);

		sum.put("val41", sum41);
		sum.put("val42", sum42);
		sum.put("val43", sum43);

		sum.put("val51", sum51);
		sum.put("val52", sum52);
		sum.put("val53", sum53);

		/** make return value */
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("conditionCd", conditionCd);
		map.put("fromYear", fromYear);
		map.put("toYear", toYear);
		map.put("type", type);
		map.put("list", list);
		map.put("sum", sum);

		List<String> yearList = new ArrayList<String>();
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4));
		for (int i = year - 20; i <= year; i++) {
			yearList.add(String.valueOf(i));
		}
		map.put("yearList", yearList);

		return map;
	}

	private Map<String, Object> statsSubDetail(String conditionCd, String fromYear, String toYear, String type) throws Exception {
		/* LOGGER.info("params [{}] [{}] [{}] ", fromYear, toYear, type); */
			if (StringUtils.isEmpty(fromYear)) {
			fromYear = DateUtils.getCurrDateNumber().substring(0, 4);
			
			toYear = DateUtils.getCurrDateNumber().substring(0, 4);
		}

		/** execute service */
		List<ReportStat2VO> list = new ArrayList<ReportStat2VO>();
		try {

			Map<String, Object> filter = new HashMap<String, Object>();
			filter.put("fromYear", fromYear);
			filter.put("toYear", toYear);
			filter.put("type", type);

			list = "2".equals(conditionCd) ? reportService.getTrustReportStats(filter, Integer.valueOf(type)) : reportService.getSubReportStatsDetail(filter, Integer.valueOf(type));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		/** make return value */
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("conditionCd", conditionCd);
		map.put("fromYear", fromYear);
		map.put("toYear", toYear);
		map.put("type", type);
		map.put("list", list);

		List<String> yearList = new ArrayList<String>();
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4));
		for (int i = year - 20; i <= year; i++) {
			yearList.add(String.valueOf(i));
		}
		map.put("yearList", yearList);

		return map;
	}

	@RequestMapping(value = "/applications", method = RequestMethod.GET)
	public String applicationList(HttpServletRequest request, @ModelAttribute ApplHistoryVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = searchVO.getSearchFromDate().replaceAll("-", "");
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = searchVO.getSearchToDate().replaceAll("-", "");
			searchVO.setSearchToDate(searchKeyword);
		}

		List<ApplHistoryVO> list = new ArrayList<ApplHistoryVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = applHistoryService.getApplHistories(filter);
			list = (List<ApplHistoryVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchFromDate());
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchToDate());
			searchVO.setSearchToDate(searchKeyword);
		}

		/** make return value */
		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "adm/stat/stat_appl";
	}

	@RequestMapping(value = "/applications/excel", method = RequestMethod.GET)
	public void applicationListExcel(HttpServletRequest request, HttpServletResponse response, @ModelAttribute ApplHistoryVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = searchVO.getSearchFromDate().replaceAll("-", "");
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = searchVO.getSearchToDate().replaceAll("-", "");
			searchVO.setSearchToDate(searchKeyword);
		}

		List<ApplHistoryVO> list = new ArrayList<ApplHistoryVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);

			Map<String, Object> data = applHistoryService.getApplHistories(filter);
			list = (List<ApplHistoryVO>) data.get("list");
			int total = (int) data.get("total");

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = DateUtils.getCurDateTime() + ".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("신청일자");
			col.add("신청내용");
			col.add("회사명");
			col.add("사업자번호");
			col.add("처리상태");
			//			col.add("발급증");
			col.add("처리일자");

			list2.add(col);
		}
		int cnt = 0;
		for (ApplHistoryVO item : list) {
			String no = String.valueOf(list.size() - cnt);
			String docType = "1".equals(item.getDocType()) ? "저작권대리중개업 신고서"
					: "2".equals(item.getDocType()) ? "저작권중개업 신고증"
							: "3".equals(item.getDocType()) ? "저작권중개업 변경신고서"
									: "4".equals(item.getDocType()) ? "저작권중개업 신고증"
											: "5".equals(item.getDocType()) ? "저작권신탁관리업 신고서"
													: "6".equals(item.getDocType()) ? "저작권신탁관리업 신고증" : "7".equals(item.getDocType()) ? "저작권신탁관리업 허가증 변경교부 신청서" : "8".equals(item.getDocType()) ? "저작권신탁관리업 변경교부 허가증" : "";

			List<String> col = new ArrayList<String>();
			col.add(no); // 번호
			col.add(item.getRegDateStr()); // 신청일자
			col.add(docType); // 신청내용
			col.add(item.getCoName()); // 회사명
			col.add(item.getCoNoStr()); // 사업자번호
			col.add("발급완료"); // 처리상태
			col.add(item.getRegDateStr()); // 처리일자

			list2.add(col);
			cnt++;
		}

		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}

	@RequestMapping(value = "/applications/{applSeqNo}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public String saveApplHistory(HttpServletRequest request, @PathVariable String applSeqNo, ModelMap model) throws Exception {
		LOGGER.info("applSeqNo [{}]", applSeqNo);

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map.put("data", applHistoryService.getApplHistory(applSeqNo));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
      
		/** make return value */
		model.putAll(map);

		return "adm/stat/stat_appl_popup";
	}

}
