package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.entity.*;

@Controller
public class PermController {

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/perm/intro1")
	public String intro1(@ModelAttribute("searchVO") DefaultVO searchVO, 
			ModelMap model) throws Exception {

		return "usr/perm/intro1";
	}

	@RequestMapping(value = "/perm/intro2")
	public String intro2(@ModelAttribute("searchVO") DefaultVO searchVO, 
			ModelMap model) throws Exception {

		return "usr/perm/intro2";
	}

	@RequestMapping(value = "/perm/downloads")
	public String docDownloads(@ModelAttribute("searchVO") DefaultVO searchVO, 
			ModelMap model) throws Exception {

		return "usr/perm/downloads";
	}

}
