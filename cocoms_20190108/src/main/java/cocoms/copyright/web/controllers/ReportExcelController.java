package cocoms.copyright.web.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import cocoms.copyright.application.ExcelReportService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.entity.LoginVO;
import cocoms.copyright.entity.MemberVO;
import cocoms.copyright.entity.ReportInfoVO;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
@RequestMapping({ "/report", "/reports" })
public class ReportExcelController {

	@Resource(name = "excelReportService")
	private ExcelReportService excelReportService;

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/excel")
	public String reportExcel(HttpServletRequest request, @ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		MemberVO member = CommonUtils.getMemberSession(request);

		if (member.getAppNo2() == null || member.getAppNo2().equals("")) {
			model.put("icon", "icon_notice_n_3");
			model.put("message", "신고증이 발급된 업체만 이용할 수 있습니다.");
			return "comm/error";
		}

		model.put("member", member);

		return "usr/report/excel_report";
	}

	@RequestMapping(value = "/exceledit")
	public String reportExcelEdit(HttpServletRequest request, ModelMap model) throws Exception {

		MemberVO member = CommonUtils.getMemberSession(request);

		if (member.getAppNo2() == null || member.getAppNo2().equals("")) {
			model.put("icon", "icon_notice_n_3");
			model.put("message", "신고증이 발급된 업체만 이용할 수 있습니다.");
			return "comm/error";
		}

		model.put("member", member);

		return "usr/report/excel_edit";
	}

	@RequestMapping(value = "/excelstatus")
	public String reportExcelStatus(HttpServletRequest request, ModelMap model, @ModelAttribute MemberVO searchVO, @RequestParam(value = "year", defaultValue = "", required = false) String year) throws Exception {
	
		MemberVO member = CommonUtils.getMemberSession(request);

		if (member.getAppNo2() == null || member.getAppNo2().equals("")) {
			model.put("icon", "icon_notice_n_3");
			model.put("message", "신고증이 발급된 업체만 이용할 수 있습니다.");
			return "comm/error";
		}
		
		if (year == null || year.equals("")) {
		
			List<String> yearList = new ArrayList<String>();
			int year1 = Integer.parseInt(DateUtils.getCurrDateNumber().substring(0, 4));
		
		          
			for (int i = year1 ;  i>=year1-10; i--) {
				//System.out.println(i);
				yearList.add(String.valueOf(i));
			}
			model.put("yearList", yearList);
	
	/*		
		for (int i = year1 + 10; i <= year1; i--) {
			System.out.println(year1);
			yearList.add(String.valueOf(i));
		}
			model.put("yearList", yearList);*/

			/** pageing setting */
			PaginationInfo paginationInfo = new PaginationInfo();
			paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
			paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
			paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

			searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
			searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
			searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

			model.put("paginationInfo", paginationInfo);

			return "usr/report/excel_status";
		}

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("year", year);
		map.put("type", Integer.parseInt(member.getAppNo2()) + 300);
		map.put("DIV", "10");
		map.put("CONDITIONCD", member.getConditionCd());
		try {
			List list = excelReportService.selectExcelReportList(map);
			//System.out.println(list.toString());
			model.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<String> yearList = new ArrayList<String>();
		int year1 = Integer.parseInt(DateUtils.getCurrDateNumber().substring(0, 4));
		for (int i = year1 ;  i>=year1-10; i--) {
			//System.out.println(i);
			yearList.add(String.valueOf(i));
		}
		model.put("yearList", yearList);
		model.put("year", year);

		return "usr/report/excel_status";
	}

	@RequestMapping(value = "/excellist")
	public String reportExcelList(HttpServletRequest request, ModelMap model, @ModelAttribute MemberVO searchVO, @RequestParam(value = "fromYear", defaultValue = "", required = false) String fromYear,
			@RequestParam(value = "toYear", defaultValue = "", required = false) String toYear, @RequestParam(value = "gubun", defaultValue = "", required = false) String gubun,
			@RequestParam(value = "TITLE", defaultValue = "", required = false) String title) throws Exception {

		MemberVO member = CommonUtils.getMemberSession(request);
		LoginVO login2 = CommonUtils.getLoginSession(request);
		if (member.getAppNo2() == null || member.getAppNo2().equals("")) {
			model.put("icon", "icon_notice_n_3");
			model.put("message", "신고증이 발급된 업체만 이용할 수 있습니다.");
			return "comm/error";
		}

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		if (fromYear == null || fromYear.equals("")) {

			Map<String, Object> map = new HashMap<String, Object>();

			map.put("fromYear", fromYear);
			map.put("toYear", toYear);
			map.put("type", Integer.parseInt(member.getAppNo2()) + 300);
			map.put("conditioncd",member.getConditionCd());
			List timeList = excelReportService.selectYearList(map);

			model.put("paginationInfo", paginationInfo);
			model.put("member", member);

			model.put("yearList", timeList);

			return "usr/report/excel_list";
		}

		List<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("appNo2", Integer.parseInt(member.getAppNo2()) + 300);
			filter.put("fromYear", fromYear);
			filter.put("toYear", toYear);
			filter.put("gubun", gubun);
			filter.put("title", title);
			filter.put("conditioncd",member.getConditionCd());
			Map<String, Object> data = excelReportService.getExcelReports(filter);
			uploadList = (List<Map<String, Object>>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("fromYear", fromYear);
		map.put("toYear", toYear);
		map.put("type", Integer.parseInt(member.getAppNo2()) + 300);
		map.put("conditioncd",member.getConditionCd());
		List timeList = excelReportService.selectYearList(map);

		model.put("yearList", timeList);
		model.put("fromYear", fromYear);
		model.put("toYear", toYear);
		model.put("gubun", gubun);
		model.put("title", title);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("uploadList", uploadList);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "usr/report/excel_list";
	}

	@RequestMapping(value = "/exceldetail/{year}")
	public String reportExcelDetail(HttpServletRequest request, ModelMap model, @ModelAttribute MemberVO searchVO, @PathVariable String year) throws Exception {

		MemberVO member = CommonUtils.getMemberSession(request);

		if (member.getAppNo2() == null || member.getAppNo2().equals("")) {
			model.put("icon", "icon_notice_n_3");
			model.put("message", "신고증이 발급된 업체만 이용할 수 있습니다.");
			return "comm/error";
		}
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("year", year);
		map.put("type", Integer.parseInt(member.getAppNo2()) + 300);
		map.put("DIV", "10");
		map.put("CONDITIONCD", member.getConditionCd());
		try {
			List info = excelReportService.getExcelReportsInfo(map);
			model.put("info", info);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			List list = excelReportService.getExcelReportsList(map);
			model.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "usr/report/excel_detail";

	}

	@ResponseBody
	@RequestMapping(value = "/deleteData", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public String reportExcelDelete(HttpServletRequest request, ModelMap model, @RequestParam(value = "chbox[]") List<String> chArr) throws Exception {

		MemberVO member = CommonUtils.getMemberSession(request);
		boolean result = true;
		result = excelReportService.deleteList(chArr);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", result);
		//map.put("errorMessage", "보고 완료");
		return CommonUtils.makeSuccess(map);
	}

	@RequestMapping(value = "/excel/noData", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String reportNodata(MultipartHttpServletRequest request, @RequestParam(value = "gercd", required = false, defaultValue = "") String name) throws Exception {

		LoginVO login2 = CommonUtils.getLoginSession(request);
		MemberVO login = CommonUtils.getMemberSession(request);

		String REPT_CHRR_NAME = request.getParameter("REPT_CHRR_NAME");
		String REPT_CHRR_POSI = request.getParameter("REPT_CHRR_POSI");
		String REPT_CHRR_TELX = request.getParameter("REPT_CHRR_TELX");
		String REPT_CHRR_MAIL = request.getParameter("REPT_CHRR_MAIL");
		String COMM_NAME = null;
		String COMM_TELX = null;
		String COMM_REPS_NAME = null;

		GregorianCalendar d = new GregorianCalendar();
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) + String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));
		String reptYmd = reptYmdTmp.replaceAll(" ", "");
		String rgst_idnt = null;
		String trst_orgn_code = null;
		String conditioncd = login.getConditionCd();
		if (login == null || login2 == null) {

			login2 = CommonUtils.getAdminLoginSession(request);
			COMM_NAME = "관리자";
			COMM_TELX = "044-203-2468";
			COMM_REPS_NAME = "관리자";
			trst_orgn_code = login2.getAppno2();
			rgst_idnt = login2.getLoginId();
		} else {
			COMM_NAME = login.getCoName();
			COMM_TELX = login.getTrustTel();
			COMM_REPS_NAME = login.getCeoName();
			trst_orgn_code = login.getAppNo2();
			rgst_idnt = login2.getLoginId();
		}

		ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
		Map<String, Object> exMap = new HashMap<String, Object>();

		exMap.put("TOT_CNT", 0);
		exMap.put("REPT_YMD", reptYmd);
		//exMap.put("GENRE_CD", 0);
		//임시
		exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code) + 300);
		exMap.put("RGST_IDNT", rgst_idnt);
		exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
		exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
		exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
		exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
		exMap.put("RGST_IDNT", rgst_idnt);
		exMap.put("COMM_NAME", COMM_NAME);
		exMap.put("COMM_TELX", COMM_TELX);
		exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
		exMap.put("CONDITIONCD", conditioncd);
		uploadList.add(exMap);

		Map<String, Object> data = excelReportService.insertNodata(uploadList);
		Map<String, Object> map = new HashMap<String, Object>();
		boolean isSuccess = (boolean) data.get("isSuccess");

		if (isSuccess == false) {

			return CommonUtils.makeError("해당월 보고내역이 이미 존재 합니다.");

		}
		map.put("success", isSuccess);
		map.put("errorMessage", "보고 완료");

		return CommonUtils.makeSuccess(map);
	}

}
