package cocoms.copyright.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.board.BoardService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.entity.BoardVO;
import cocoms.copyright.entity.DefaultVO;

import cocoms.copyright.user.UserService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class RelationController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	

	/** CodeService */
	@Resource(name = "codeService")
	private CodeService codeService;
	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	
	/** BoardService */
	@Resource(name = "relationService")
	private BoardService relationService;
	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/relation", method = RequestMethod.GET)
	public String relationList(HttpServletRequest request,
			@ModelAttribute DefaultVO searchVO, 
			ModelMap model) throws Exception {

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<BoardVO> list = new ArrayList<BoardVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = relationService.getNotices(filter);
			list = (List<BoardVO>) data.get("list");
			int total = (int) data.get("total");
			
			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("today", DateUtils.getCurrDateNumber());
		
		model.put("search", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo()-1) * paginationInfo.getRecordCountPerPage());
		
		return "usr/relation/relation_list";
	}

	@RequestMapping(value = "/relation/{boardSeqNo}", method = RequestMethod.GET)
	public String relationDetail(HttpServletRequest request,
			@ModelAttribute BoardVO searchVO,
			@PathVariable String boardSeqNo,
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		BoardVO board = new BoardVO();
		try {
			relationService.hitCount(searchVO);
			board = relationService.getNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("search", searchVO);
		model.put("data", board);

		return "usr/relation/relation_view";
	}
	
	
	
	

}
