package cocoms.copyright.web.controllers;

import java.io.*;
import java.util.*;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public abstract class UploadCommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	protected List saveFiles(MultipartHttpServletRequest request, String name) throws Exception {

		LinkedList<Map<String, Object>> files = new LinkedList<Map<String, Object>>();

		Iterator<String> it = request.getFileNames();
		MultipartFile mpf = null;

		while (it.hasNext()) {

			// get next MultipartFile
			mpf = request.getFile(it.next());
			LOGGER.info("org file [{}]", mpf.getOriginalFilename());

			//
			String orgFilename = mpf.getOriginalFilename();

			if (StringUtils.isEmpty(orgFilename)) {
				continue;
			}

			// if files > 10, remove the first from the list
			if (files.size() >= 10) {
				files.pop();
			}

			String root = getRootPath();
			String path = getSubPath();
			//String ext = orgFilename.split("\\.")[1];
			String ext = FilenameUtils.getExtension(orgFilename);
			String filename = getFilename(ext);
		
			System.out.println( FilenameUtils.getExtension(orgFilename) );
			System.out.println("etx:::: " +ext);
			System.out.println("filename:::: " +filename);
			// 
			// 
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name", orgFilename);
			map.put("size", mpf.getSize());
			/* map.put("type", mpf.getContentType()); */
			map.put("type", "text/plain");
			map.put("url", path + "/" + filename);
			LOGGER.info("file [{}] [{}]", map.get("name"), mpf.getName());

			if (StringUtils.isNotEmpty(name) && name.indexOf(mpf.getName()) < 0) {
				continue;
			}

			String allowExt = getAllowExt() + ",";
		
			if (StringUtils.isNotEmpty(getAllowExt()) && allowExt.indexOf(ext.toLowerCase() + ",") < 0) {
				continue;
			}

			try {

				// copy file to local disk (make sure the path "e.g. D:/temp/files" exists)
				makeDirs(root + path);

				String savePath = root + path + "/" + filename;
				LOGGER.info("file saved path [{}]", savePath);
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(savePath));

			} catch (IOException e) {
				e.printStackTrace();
			}

			// add to files
			files.add(map);

			//			Thread.sleep(200);
		}

		return files;
	}

	abstract protected String getRootPath();

	abstract protected String getSubPath();

	abstract protected String getFilename(String ext);

	abstract protected String getAllowExt();

	private void makeDirs(String savePath) throws IOException {
		LOGGER.info("save path [{}]", savePath);

		// make dirs
		String[] s = savePath.split("/");
		String path = "";
		for (int i = 0; i < s.length; i++) {
			path += s[i] + "/";

			File file = new File(path);
			if (!file.exists()) {
				LOGGER.info("make path [{}]", file.getAbsolutePath());
				file.mkdir();
			}
		}
	}

}
