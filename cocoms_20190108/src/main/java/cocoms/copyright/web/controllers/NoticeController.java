package cocoms.copyright.web.controllers;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.board.BoardService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.entity.BoardVO;
import cocoms.copyright.entity.DefaultVO;
import cocoms.copyright.entity.MemberVO;
import cocoms.copyright.entity.ZipCodeVO;
import cocoms.copyright.user.UserService;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class NoticeController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** CodeService */
	@Resource(name = "codeService")
	private CodeService codeService;
	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** BoardService */
	@Resource(name = "noticeService")
	private BoardService noticeService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = "/notice", method = RequestMethod.GET)
	public String noticeList(HttpServletRequest request, @ModelAttribute DefaultVO searchVO, ModelMap model) throws Exception {

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<BoardVO> list = new ArrayList<BoardVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = noticeService.getNotices(filter);
			list = (List<BoardVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("today", DateUtils.getCurrDateNumber());

		model.put("search", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "usr/notice/notice_list";
	}

	@RequestMapping(value = "/notice/{boardSeqNo}", method = RequestMethod.GET)
	public String noticeDetail(HttpServletRequest request, @ModelAttribute BoardVO searchVO, @PathVariable String boardSeqNo, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		BoardVO board = new BoardVO();
		try {

			board = noticeService.getNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("data", board);

		return "usr/notice/notice_view";
	}

	@RequestMapping(value = "/notice/status/companies", method = RequestMethod.GET)
	public String companyList(HttpServletRequest request,
			//			@ModelAttribute UserForm searchVO,
			@ModelAttribute MemberVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		/** execute service */
		List<MemberVO> list = new ArrayList<MemberVO>();
		try {

			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("order", "appNo");

			Map<String, Object> data = userService.getMembers(filter);
			list = (List<MemberVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		/** make return value */
		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "usr/stat/stat_company";
	}

	@RequestMapping(value = "/notice/status/zip_code", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getZipCode(HttpServletRequest request, @RequestParam(value = "sido", defaultValue = "", required = false) String sido, ModelMap model) throws Exception {
		LOGGER.info("sido [{}]", sido);

		List<ZipCodeVO> list = new ArrayList<ZipCodeVO>();
		try {

			list = codeService.getZipCodes(sido);

			for (ZipCodeVO item : list) {
				item.setSido(URLEncoder.encode(item.getSido(), "UTF-8"));
				item.setGugun(URLEncoder.encode(item.getGugun(), "UTF-8"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		return CommonUtils.makeSuccess(map);
	}

}
