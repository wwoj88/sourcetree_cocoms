package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.user.*;
import cocoms.copyright.web.form.*;

@Controller
public class ActionLogController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	/** member form */
	@ModelAttribute("member")
	public UserForm createUserForm() {
		return new UserForm();
	}

	/**
	 * 
	 */
	@RequestMapping(value = "/log/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveLog(HttpServletRequest request, @RequestParam(value = "action", defaultValue = "") String action,

			ModelMap model) throws Exception {
		LOGGER.info("save log [{}]" + action);

		// save log
		actionLogService.saveLog(request, action);

		return CommonUtils.makeSuccess();
	}

}
