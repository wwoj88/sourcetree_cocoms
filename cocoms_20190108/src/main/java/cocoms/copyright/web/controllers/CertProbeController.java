package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.board.BoardService;
import cocoms.copyright.cert.CertProbeService;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.user.*;
import cocoms.copyright.web.form.*;

@Controller
public class CertProbeController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** CertProbeService */
	@Resource(name = "certProbeService")
	private CertProbeService certProbeService;
	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/cert/probe", method = RequestMethod.GET)
	public String probeForm(HttpServletRequest request,
			ModelMap model) throws Exception {

		return "usr/cert/cert_probe";
	}
	
	@RequestMapping(value = "/cert/info1", method = RequestMethod.GET)
	public String info1(HttpServletRequest request,
			ModelMap model) throws Exception {

		return "usr/cert/cert_info1";
	}

	@RequestMapping(value = "/cert/info2", method = RequestMethod.GET)
	public String info2(HttpServletRequest request,
			ModelMap model) throws Exception {

		return "usr/cert/cert_info2";
	}

	@RequestMapping(value = "/cert/info3", method = RequestMethod.GET)
	public String info3(HttpServletRequest request,
			ModelMap model) throws Exception {

		return "usr/cert/cert_info3";
	}

	@RequestMapping(value = "/cert/info4", method = RequestMethod.GET)
	public String info4(HttpServletRequest request,
			ModelMap model) throws Exception {

		return "usr/cert/cert_info4";
	}

	@RequestMapping(value = "/cert/probe/{printId}", method = RequestMethod.POST)
	@ResponseBody
	public String probe(HttpServletRequest request,
			@PathVariable String printId,
			ModelMap model) throws Exception {

		boolean check = false;
		try {
			
			check = certProbeService.checkCertProbe(printId);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}
		
		// return value
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", check);

		return CommonUtils.makeSuccess(map);
	}

}
