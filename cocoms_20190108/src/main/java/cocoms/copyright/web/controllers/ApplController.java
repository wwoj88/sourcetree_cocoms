package cocoms.copyright.web.controllers;

import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.application.ApplHistoryService;
import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.application.ChgHistoryMapper;
import cocoms.copyright.application.PermKindMapper;
import cocoms.copyright.application.RpMgmMapper;
import cocoms.copyright.application.WritingKindMapper;
import cocoms.copyright.cert.CertInfoService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

import crosscert.*;

@Controller
@SessionAttributes({ "member" })
public class ApplController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** CodeService */
	@Resource(name = "codeService")
	private CodeService codeService;

	@Resource(name = "chgHistoryMapper")
	private ChgHistoryMapper chgHistoryDAO;

	@Resource(name = "rpMgmMapper")
	private RpMgmMapper rpMgmDAO;

	/** ApplicationService */
	@Resource(name = "applicationService")
	private ApplicationService applicationService;

	@Resource(name = "permKindMapper")
	private PermKindMapper permKindDAO;

	@Resource(name = "writingKindMapper")
	private WritingKindMapper writingKindDAO;

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** ApplHistoryService */
	@Resource(name = "applHistoryService")
	private ApplHistoryService applHistoryService;

	/** CertInfoService */
	@Resource(name = "certInfoService")
	private CertInfoService certInfoService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	/** member form */
	@ModelAttribute("member")
	public ApplicationForm createApplicationForm() {
		return new ApplicationForm();
	}

	@RequestMapping(value = "/appl/sub/new", method = RequestMethod.GET)
	public String subRegistForm(HttpServletRequest request, ModelMap model) throws Exception {

		String conditionCd = "1"; //1 저작권대리 , 2 저작권신탁
		String statCd = "1"; // 1: 신고신청

		MemberVO member = CommonUtils.getMemberSession(request);
		try {
			member = userService.getMember(member.getMemberSeqNo());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		if ("7".equals(member.getStatCd()) || "9".equals(member.getStatCd())) {
			if (!"1".equals(member.getConditionCd())) {
				model.put("message", "신고현황을 확인해주세요.");
				model.put("subMessage", "(계정당 신탁관리업 또는 대리중개업 한가지만 신청 가능합니다.)");
				return "comm/error";
			} else {
				String result = "";
				result = rpMgmDAO.selectrpmgseqno(member.getMemberSeqNo());
				statCd = member.getStatCd();
				if (result != null) {
					model.put("message", "신고현황을 확인해주세요.");
					model.put("subMessage", "보완 또는 반려상태입니다.");
					return "comm/error";
				}
			}
		}

		// 6: 신고취소 빈값이아니고 6도아니면 
		if (!"".equals(member.getStatCd()) && !"6".equals(member.getStatCd()) && !"7".equals(member.getStatCd()) && !"9".equals(member.getStatCd())) {

			if ("5".equals(member.getStatCd()) || "6".equals(member.getStatCd())) {
				model.put("message", "신고현황을 확인해주세요.");
				model.put("subMessage", "신고정지 또는 취소상태입니다.");
				return "comm/error";
			}
			model.put("message", "신고현황을 확인해주세요.");
			model.put("subMessage", "신고처리/신고완료 상태입니다.");
			return "comm/error";
		}

		ApplicationForm appl = getApplication(request, model, conditionCd, statCd);
		RpMgmVO rpMgm = rpMgmDAO.selectByMemberInfofile(member.getMemberSeqNo());
		appl.setStatCd(statCd);

		model.addAttribute("rpMgm", rpMgm);
		model.addAttribute("member", appl);
		model.addAttribute("type", "sub_regist");
		return "usr/appl/appl_regist";
	}

	@RequestMapping(value = "/appl/trust/new", method = RequestMethod.GET)
	public String trustRegistForm(HttpServletRequest request, ModelMap model) throws Exception {

		String conditionCd = "2";
		String statCd = "1"; // 1: 신고신청

		MemberVO member = CommonUtils.getMemberSession(request);
		try {
			member = userService.getMember(member.getMemberSeqNo());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		if ("7".equals(member.getStatCd()) || "9".equals(member.getStatCd())) {
			if (!"2".equals(member.getConditionCd())) {
				model.put("message", "신고현황을 확인해주세요.");
				model.put("subMessage", "(계정당 신탁관리업 또는 대리중개업 한가지만 신청 가능합니다.)");
				return "comm/error";
			} else {
				String result = "";
				result = rpMgmDAO.selectrpmgseqno(member.getMemberSeqNo());
				statCd = member.getStatCd();
				if (result != null) {
					model.put("message", "신고현황을 확인해주세요.");
					model.put("subMessage", "보완 또는 반려상태입니다.");
					return "comm/error";
				}
			}
		}
		// 6: 신고취소
		if (!"".equals(member.getStatCd()) && !"6".equals(member.getStatCd()) && !"7".equals(member.getStatCd())) {
			if ("5".equals(member.getStatCd()) || "6".equals(member.getStatCd())) {
				model.put("message", "신고정지 또는 취소상태입니다.");
				return "comm/error";
			}
			model.put("message", "신고현황을 확인해주세요.");
			model.put("subMessage", "신고처리/신고완료 상태입니다.");
			return "comm/error";
		}

		ApplicationForm appl = getApplication(request, model, conditionCd, statCd);
		appl.setStatCd(statCd);
		RpMgmVO rpMgm = rpMgmDAO.selectByMemberInfofile(member.getMemberSeqNo());
		model.addAttribute("rpMgm", rpMgm);
		model.addAttribute("member", appl);
		model.addAttribute("type", "trust_regist");
		return "usr/appl/appl_regist";
	}

	@RequestMapping(value = "/appl/sub/edit", method = RequestMethod.GET)
	public String subModifyForm(HttpServletRequest request, ModelMap model) throws Exception {

		String conditionCd = "1";
		String statCd = "3"; // 3: 변경신고신청
		String memberseqno = "";
		ApplicationForm appl = new ApplicationForm();
		try {
			appl = getmodiApplication(request, model, conditionCd, statCd);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
		}
		LOGGER.info("appl [{}]", appl);

		if (!conditionCd.equals(appl.getConditionCd())) {
			
			model.put("message", "신고현황을 확인해주세요.");
			model.put("subMessage", "(계정당 신탁관리업 또는 대리중개업 한가지만 신청 가능합니다.)");

			return "comm/error";
		}
		// 2: 신고완료, 4: 변경신고완료
		if (!"2".equals(appl.getStatCd()) && !"4".equals(appl.getStatCd()) && !"7".equals(appl.getStatCd()) && !"9".equals(appl.getStatCd())) {
			if ("5".equals(appl.getStatCd()) || "6".equals(appl.getStatCd())) {
				model.put("message", "신고정지 또는 취소상태입니다.");
				return "comm/error";
			}
			model.put("message", "신고현황을 확인해주세요.");
			model.put("subMessage", "신고처리/신고완료 상태입니다.");
			return "comm/error";
		}
		if ("7".equals(appl.getStatCd()) || "9".equals(appl.getStatCd())) {
			if (!"1".equals(appl.getConditionCd())) {
				model.put("message", "신고현황을 확인해주세요.");
				model.put("subMessage", "(계정당 신탁관리업 또는 대리중개업 한가지만 신청 가능합니다.)");
				return "comm/error";
			} else {
				String result = "";
				result = rpMgmDAO.selectrpmgseqno(appl.getMemberSeqNo());

				String statcd2 = appl.getStatCd();
				appl = getApplication(request, model, conditionCd, statcd2);
				if (result == null) {
					model.put("message", "신고현황을 확인해주세요.");
					model.put("subMessage", "보완 또는 반려상태입니다.");
					return "comm/error";
				}
			}
		}
		appl.setStatCd(statCd);
		RpMgmVO params = new RpMgmVO();
		params.setMemberSeqNo(appl.getMemberSeqNo());
		params.setStatCd(statCd);

		RpMgmVO rpMgm = rpMgmDAO.selectByMemberInfofile(appl.getMemberSeqNo());
		List<ChgHistoryFileVO> chgHistoryFileList = (List<ChgHistoryFileVO>) chgHistoryDAO.selectChgFileList(CommonUtils.pojoToMap(params));
		model.addAttribute("chgHistoryFileList", chgHistoryFileList);
		model.addAttribute("rpMgm", rpMgm);
		model.addAttribute("member", appl);
		model.addAttribute("type", "sub_modify");
		return "usr/appl/appl_sub_modify";
	}

	@RequestMapping(value = "/appl/trust/edit", method = RequestMethod.GET)
	public String trustModifyForm(HttpServletRequest request, ModelMap model) throws Exception {

		String memberseqno = "";
		String conditionCd = "2";
		String statCd = "3"; // 3: 변경신고신청

		ApplicationForm appl = new ApplicationForm();
		try {
			appl = getmodiApplication(request, model, conditionCd, statCd);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
		}
		//System.out.println(appl.getMemberSeqNo());
		if (!conditionCd.equals(appl.getConditionCd())) {

			model.put("message", "신고현황을 확인해주세요.");
			model.put("subMessage", "(계정당 신탁관리업 또는 대리중개업 한가지만 신청 가능합니다.)");
			return "comm/error";
		}

		// 2: 신고완료, 4: 변경신고완료
		if (!"2".equals(appl.getStatCd()) && !"4".equals(appl.getStatCd()) && !"7".equals(appl.getStatCd()) && !"9".equals(appl.getStatCd())) {
			if ("5".equals(appl.getStatCd()) || "6".equals(appl.getStatCd())) {
				model.put("message", "신고정지 또는 취소상태입니다.");
				return "comm/error";
			}
			model.put("message", "신고현황을 확인해주세요.");
			model.put("subMessage", "신고처리 중입니다.");
			return "comm/error";
		}

		if ("7".equals(appl.getStatCd()) || "9".equals(appl.getStatCd())) {
			if (!"2".equals(appl.getConditionCd())) {
				model.put("message", "신고현황을 확인해주세요.");
				model.put("subMessage", "(계정당 신탁관리업 또는 대리중개업 한가지만 신청 가능합니다.)");
				return "comm/error";
			} else {
				String result = "";
				result = rpMgmDAO.selectrpmgseqno(appl.getMemberSeqNo());
				String statcd2 = appl.getStatCd();

				appl = getApplication(request, model, conditionCd, statcd2);

				if (result == null) {
					model.put("message", "신고현황을 확인해주세요.");
					model.put("subMessage", "보완 또는 반려상태입니다.");
					return "comm/error";
				}
			}
		}

		RpMgmVO params = new RpMgmVO();
		params.setMemberSeqNo(appl.getMemberSeqNo());
		params.setStatCd(statCd);

		RpMgmVO rpMgm = rpMgmDAO.selectByMemberInfofile(appl.getMemberSeqNo());

		List<ChgHistoryFileVO> chgHistoryFileList = (List<ChgHistoryFileVO>) chgHistoryDAO.selectChgFileList(CommonUtils.pojoToMap(params));
		model.addAttribute("chgHistoryFileList", chgHistoryFileList);
		model.addAttribute("rpMgm", rpMgm);
		appl.setStatCd(statCd);

		model.addAttribute("member", appl);
		model.addAttribute("type", "trust_modify");
		return "usr/appl/appl_trust_modify";
	}

	private ApplicationForm getApplication(HttpServletRequest request, ModelMap model, String conditionCd, String statCd) {

		LoginVO sessionUser = CommonUtils.getLoginSession(request);
		MemberVO sessionMember = CommonUtils.getMemberSession(request);
		LOGGER.info("session user [{}]", sessionUser);
		LOGGER.info("session member [{}]", sessionMember);
		String url = request.getRequestURI();
		String memberSeqNo = sessionUser.getMemberSeqNo();

		LOGGER.info("memberSeqNo2 [{}]", url);

		ApplicationForm appl = new ApplicationForm();
		try {
			if ("7".equals(statCd) || "9".equals(statCd)) {
				Map<String, Object> data = applicationService.getChgAppliation(memberSeqNo, conditionCd, "1"); //
				/* ChgHistoryVO member= (ChgHistoryVO) data.get("member"); */
				MemberVO member = (MemberVO) data.get("member");

				List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) data.get("writingKindList");
				List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) data.get("permKindList");
				/* appl = CommonUtils.ChgHistoryToApplication(member); */
				appl = CommonUtils.memberToApplication(member);
				appl.setPrewritingKindListChg(writingKindList);
				appl.setPrepermKindListChg(permKindList);
				appl.setMemberSeqNo(memberSeqNo);
				for (ChgWritingKindVO item : writingKindList) {
					LOGGER.info("writing kind [{}]", item);
					String writingKind = item.getWritingKind();
					String writingEtc = item.getWritingEtc();

					model.addAttribute("writingKind" + writingKind, "checked");
					model.addAttribute("writingEtc" + writingKind, writingEtc);
				}

				/* perm kind list */
				for (ChgPermKindVO item : permKindList) {
					LOGGER.info("perm kind [{}]", item);
					/* String writingKind = item.getWritingKind(); */
					String permKind = item.getPermKind();
					String permEtc = item.getPermEtc();
					String writingKind = permKindDAO.selectWritingKindChg(item.getChgwritingkindseqno());

					model.addAttribute("permkind" + writingKind + "_" + permKind, "checked");

					model.addAttribute("permEtc" + writingKind, permEtc);
				}

			} else {
				// execute service
				Map<String, Object> map = "1".equals(statCd) ? applicationService.getAppliationTemp(memberSeqNo, conditionCd) : applicationService.getAppliation(memberSeqNo, conditionCd);
				MemberVO member = (MemberVO) map.get("member");
				List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) map.get("writingKindList");

				List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) map.get("permKindList");
				
				appl = CommonUtils.memberToApplication(member);

				appl.setPrewritingKindListChg(writingKindList);
				appl.setPrepermKindListChg(permKindList);
				/* writing kind list */
				for (ChgWritingKindVO item : writingKindList) {
					LOGGER.info("writing kind [{}]", item);
					String writingKind = item.getWritingKind();
					String writingEtc = item.getWritingEtc();

					model.addAttribute("writingKind" + writingKind, "checked");
					model.addAttribute("writingEtc" + writingKind, writingEtc);
				}

				/* perm kind list */
				for (ChgPermKindVO item : permKindList) {
					LOGGER.info("perm kind [{}]", item);
					String writingKind = item.getWritingKind();
					String permKind = item.getPermKind();
					String permEtc = item.getPermEtc();

					model.addAttribute("permkind" + writingKind + "_" + permKind, "checked");
					model.addAttribute("permEtc" + writingKind, permEtc);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
		}
		// 암호화
		//String ceoRegNo = XCrypto.decPatternReg(appl.getCeoRegNo());
		String ceoRegNo = appl.getCeoRegNo();
		appl.setCeoRegNo(ceoRegNo);
		if (StringUtils.isEmpty(appl.getCoGubunCd())) {
			appl.setCoGubunCd("2");
		}
		if (url.equals("/cocoms/appl/trust/new") || url.equals("/cocoms/appl/sub/new")) {

			// default value

			if (StringUtils.isNotEmpty(sessionMember.getCoName())) {
				appl.setCoName(sessionMember.getCoName());
			}
			if (StringUtils.isNotEmpty(sessionMember.getCoNo()) && sessionMember.getCoNo().length() >= 10) {
				appl.setCoNo1(sessionMember.getCoNo().substring(0, 3));
				appl.setCoNo2(sessionMember.getCoNo().substring(3, 5));
				appl.setCoNo3(sessionMember.getCoNo().substring(5, 10));
			}
			if (StringUtils.isNotEmpty(sessionMember.getCeoRegNo()) && sessionMember.getCeoRegNo().length() >= 13) {
				//암호화
				//String ceoRegNo2 = XCrypto.decPatternReg(sessionMember.getCeoRegNo());
				String ceoRegNo2 = sessionMember.getCeoRegNo();
				appl.setCeoRegNo1(ceoRegNo2.substring(0, 6));
				appl.setCeoRegNo2(ceoRegNo2.substring(6, 13));
			}
			if (StringUtils.isNotEmpty(sessionMember.getCeoName())) {
				appl.setCeoName(sessionMember.getCeoName());
			}
			if (StringUtils.isNotEmpty(sessionMember.getCeoMobile()) && sessionMember.getCeoMobile().indexOf("-") > -1) {
				StringTokenizer st = new StringTokenizer(sessionMember.getCeoMobile(), "-");
				if (st.hasMoreTokens())
					appl.setCeoMobile1(st.nextToken());
				if (st.hasMoreTokens())
					appl.setCeoMobile2(st.nextToken());
				if (st.hasMoreTokens())
					appl.setCeoMobile3(st.nextToken());
			}
			if (StringUtils.isNotEmpty(sessionMember.getCeoEmail()) && sessionMember.getCeoEmail().indexOf("@") > -1) {
				String email = sessionMember.getCeoEmail();
				appl.setCeoEmail1(email.substring(0, email.indexOf("@")));
				appl.setCeoEmail2(email.substring(email.indexOf("@") + 1));
			}
			if ("Y".equals(sessionMember.getSmsAgree())) {
				appl.setSmsAgree("Y");
				appl.setAgreeDate(DateUtils.getCurrDateNumber());
			} else {
				appl.setSmsAgree("N");
				appl.setAgreeDate("");
			}

			if (StringUtils.isEmpty(appl.getRegDate2())) {
				appl.setRegDate2(DateUtils.getCurrDateNumber());
			}
		}
		return appl;
	}

	@RequestMapping(value = { "/appl/sub", "/appl/trust" }, method = RequestMethod.POST)
	public String save(HttpServletRequest request, @ModelAttribute("member") ApplicationForm form, BindingResult bindingResult, SessionStatus status, ModelMap model) throws Exception {
		LOGGER.info("regist proc form [{}], perm kind [{}]", form, form.getPermkind1());

		LoginVO user = CommonUtils.getLoginSession(request);
		String chgMemo = form.getChgMemo();
		String memberSeqNo = user.getMemberSeqNo();
		form.setMemberSeqNo(memberSeqNo);
		LOGGER.info("memberSeqNo [{}]", memberSeqNo);
		new XCrypto(propertiesService.getString("xcry.path"), propertiesService.getBoolean("xcry.use"));
		if (form.getCeoRegNo2().equals("*******")) {
			
			MemberVO member = userService.getMember(memberSeqNo);
			System.out.println("REG"+member.getCeoRegNo());
			String regNo = XCrypto.decPatternReg(member.getCeoRegNo());
			System.out.println("regNo:"+regNo);
			System.out.println("member :" + regNo.substring(6, 13));
			form.setCeoRegNo2(regNo.substring(6, 13));
			System.out.println("CEOREG :"+form.getCeoRegNo2());
		}
		String conditionCd = form.getConditionCd();
		
		Map<String, Object> map = CommonUtils.applicationToMember(form);
		MemberVO member = (MemberVO) map.get("member");
		System.out.println("FORM::::::" + member.getCeoRegNo());
		List<WritingKindVO> writingKindList = (List<WritingKindVO>) map.get("writingKindList");
		List<PermKindVO> permKindList = (List<PermKindVO>) map.get("permKindList");
		List<WritingKindVO> prewritingKindList = (List<WritingKindVO>) map.get("prewritingKindList");
		List<PermKindVO> prepermKindList = (List<PermKindVO>) map.get("prepermKindList");
		List<ChgCdVO> chgCdList = (List<ChgCdVO>) map.get("chgCdList");

		List<ChghFileVO> ChghFileList = new ArrayList<ChghFileVO>();

		if (request.getParameterValues("fileName") != null) {
			if (request.getParameterValues("fileName").length > 0) {
				for (int i = 0; i < request.getParameterValues("fileName").length; i++) {
					/*
					 * List<ChghFileVO> ChghFile = new ArrayList<ChghFileVO>();
					 */
					ChghFileVO ChghFile = new ChghFileVO();
					ChghFile.setFilename(request.getParameterValues("fileName")[i]);
					ChghFile.setFilepath(request.getParameterValues("filePath")[i]);
					ChghFile.setFsize(request.getParameterValues("fileSize")[i]);
					ChghFileList.add(ChghFile);

				}
			}
		}

		BeanUtils.copyProperties(member, form);

		if ("1".equals(form.getStatCd())) {
			/* save temp */

			try {
				applicationService.saveTemp(memberSeqNo, conditionCd, member, writingKindList, permKindList);

				member = userService.getMemberTemp(memberSeqNo, conditionCd);
				HttpSession session = request.getSession();
				session.removeAttribute("SESS_MEMBER");
				session.setAttribute("SESS_MEMBER", member);
				System.out.println(session.getAttribute("SESS_MEMBER"));
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}
			if ("sub_regist".equals(form.getProcType()) || "sub_modify".equals(form.getProcType())) {

				return "redirect:/appl/sub/view";
			} else {
				return "redirect:/appl/trust/view";
			}
		} else {

			try {

				applicationService.savesubTemp(ChghFileList, memberSeqNo, conditionCd, member, writingKindList, permKindList, prepermKindList, prewritingKindList, chgCdList, chgMemo);

				member = userService.getMemberTemp(memberSeqNo, conditionCd);
				HttpSession session = request.getSession();
				session.removeAttribute("SESS_MEMBER");
				session.setAttribute("SESS_MEMBER", member);
				
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}
			if ("sub_regist".equals(form.getProcType()) || "sub_modify".equals(form.getProcType())) {

				return "redirect:/appl/sub/view";
			} else {
				return "redirect:/appl/trust/view";
			}
		}

	}

	@RequestMapping(value = { "/appl/sub/view", "/appl/trust/view" })
	public String preview(HttpServletRequest request, @ModelAttribute("member") ApplicationForm form, SessionStatus status, ModelMap model) throws Exception {
		LOGGER.info("preview form [{}], perm kind [{}]", form, form.getPermkind1());
		LOGGER.info("member [{}]", form.getMember());

		CertVO cert = CommonUtils.getCertSession(request);

		MemberVO params = new MemberVO();
		params.setMemberSeqNo(form.getMemberSeqNo());

		RpMgmVO rpMgm = rpMgmDAO.selectByMemberInfofile(form.getMemberSeqNo());

		List<WritingKindVO> prewritingKindList = (List<WritingKindVO>) writingKindDAO.selectList2(CommonUtils.pojoToMap(params));
		List<PermKindVO> prepermKindList = (List<PermKindVO>) permKindDAO.selectList2(CommonUtils.pojoToMap(params));

		params = userService.getMember(form.getMemberSeqNo());
		String RealStacd=params.getStatCd();
		if ("3".equals(form.getStatCd())) {
		
			List<ChgHistoryFileVO> chgHistoryFileList = (List<ChgHistoryFileVO>) chgHistoryDAO.selectChgFileListTmp(CommonUtils.pojoToMap(params));

			model.put("chgHistoryFileList", chgHistoryFileList);
			
			//params = userService.getMemberTemp(form.getMemberSeqNo(), form.getConditionCd());
			params = userService.getMember(form.getMemberSeqNo());
			if(RealStacd.equals("7") || RealStacd.equals("9")){
				params.setStatCd(RealStacd);
			}
			System.out.println("PARAMS :::: " + params);
		}

		params = CommonUtils.memberToApplication(params);

		if (StringUtils.isEmpty(form.getCeoRegNo()) && StringUtils.isNotEmpty(form.getCeoRegNo1()) && StringUtils.isNotEmpty(form.getCeoRegNo2())) {
			form.setCeoRegNo(form.getCeoRegNo1() + form.getCeoRegNo2());
		}

		if (StringUtils.isEmpty(form.getTrustTel()) && StringUtils.isNotEmpty(form.getTrustTel1()) && StringUtils.isNotEmpty(form.getTrustTel2()) && StringUtils.isNotEmpty(form.getTrustTel3())) {
			form.setTrustTel(form.getTrustTel1() + "-" + form.getTrustTel2() + "-" + form.getTrustTel3());
		}

		if (StringUtils.isEmpty(form.getTrustFax()) && StringUtils.isNotEmpty(form.getTrustFax1()) && StringUtils.isNotEmpty(form.getTrustFax2()) && StringUtils.isNotEmpty(form.getTrustFax3())) {
			form.setTrustFax(form.getTrustFax1() + "-" + form.getTrustFax2() + "-" + form.getTrustFax3());
		}

		if (StringUtils.isEmpty(form.getCeoTel()) && StringUtils.isNotEmpty(form.getCeoTel1()) && StringUtils.isNotEmpty(form.getCeoTel2()) && StringUtils.isNotEmpty(form.getCeoTel3())) {
			form.setCeoTel(form.getCeoTel1() + "-" + form.getCeoTel2() + "-" + form.getCeoTel3());
		}

		if (StringUtils.isEmpty(form.getCeoFax()) && StringUtils.isNotEmpty(form.getCeoFax1()) && StringUtils.isNotEmpty(form.getCeoFax2()) && StringUtils.isNotEmpty(form.getCeoFax3())) {
			form.setCeoFax(form.getCeoFax1() + "-" + form.getCeoFax2() + "-" + form.getCeoFax3());
		}

		if (StringUtils.isEmpty(form.getCeoEmail()) && StringUtils.isNotEmpty(form.getCeoEmail1()) && StringUtils.isNotEmpty(form.getCeoEmail2())) {
			form.setCeoEmail(form.getCeoEmail1() + "@" + form.getCeoEmail2());
		}

		if (form.getCeoRegNo() != null && form.getCeoRegNo().length() <= 13) {

			String regNo = XCrypto.decPatternReg(form.getCeoRegNo());
			form.setCeoRegNo(regNo);
		}
		// birthday
		String ceoRegNo = form.getCeoRegNo();

		String year = "1,2,5,6,".indexOf(ceoRegNo.substring(6, 7)) > -1 ? "19" : "20";
		String birthday = CommonUtils.toFormat(year + ceoRegNo.substring(0, 6), "####.##.##");

		//
		String postwritingKind = "", postpermKind = "", prewritingKind = "", prepermKind = "";
		List<String> postpermKindList = new ArrayList<String>();
		List<String> prepermKindList2 = new ArrayList<String>();

		try {

			for (CommonCodeVO item : codeService.getCodes("WRITINGKIND")) {
				for (WritingKindVO item2 : form.getWritingKindList()) {
					if (item2.getWritingKind().equals(item.getCodeValue())) {
						postwritingKind += ", " + item.getCodeValueDesc();
					}
				}
			}

			if (StringUtils.isNotBlank(postwritingKind))
				postwritingKind = postwritingKind.substring(2);

			for (CommonCodeVO item : codeService.getCodes("PERMKIND")) {
				String str = "";
				for (PermKindVO item2 : form.getPermKindList()) {
					if (item2.getPermKind().equals(item.getCodeValue())) {
						if (item2.getPermEtc() == null || ("").equals(item2.getPermEtc())) {
							str = item.getCodeValueDesc();
						} else {
							str = item.getCodeValueDesc() + "(" + item2.getPermEtc() + ")";
						}
						/* String str = item.getCodeValueDesc(); */
						if (!postpermKindList.contains(str)) {
							postpermKindList.add(str);
							postpermKind += ", " + str;

						}
					}
				}
			}
			if (StringUtils.isNotBlank(postpermKind))
				postpermKind = postpermKind.substring(2);

			for (CommonCodeVO item : codeService.getCodes("WRITINGKIND")) {
				for (WritingKindVO item2 : prewritingKindList) {
					if (item2.getWritingKind().equals(item.getCodeValue())) {
						prewritingKind += ", " + item.getCodeValueDesc();
					}
				}
			}

			if (StringUtils.isNotBlank(prewritingKind))
				prewritingKind = prewritingKind.substring(2);

			for (CommonCodeVO item : codeService.getCodes("PERMKIND")) {
				String str = "";
				for (PermKindVO item2 : prepermKindList) {
					if (item2.getPermKind().equals(item.getCodeValue())) {
						if (item2.getPermEtc() == null || ("").equals(item2.getPermEtc())) {
							str = item.getCodeValueDesc();
						} else {
							str = item.getCodeValueDesc() + "(" + item2.getPermEtc() + ")";
						}

						if (!prepermKindList.contains(str)) {
							prepermKindList2.add(str);
							prepermKind += ", " + str;

						}
					}
				}
			}
			if (StringUtils.isNotBlank(prepermKind))
				prepermKind = prepermKind.substring(2);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		//
		model.addAttribute("member", form);
		model.addAttribute("premember", params);
		model.addAttribute("cert", cert);
		model.addAttribute("birthday", birthday);
		model.put("rpMgm", rpMgm);
		model.put("postwritingKind", postwritingKind);
		model.put("postpermKind", postpermKind);
		model.put("prepermKind", prepermKind);
		model.put("prewritingKind", prewritingKind);

		return "usr/appl/appl_preview";
	}

	@RequestMapping(value = { "/appl/sub/reg_print", "/appl/sub/mod_print", "/appl/trust/reg_print", "/appl/trust/mod_print" })
	public String print(HttpServletRequest request, @ModelAttribute("member") ApplicationForm form,
			//			BindingResult bindingResult, 
			SessionStatus status, ModelMap model) throws Exception {
		LOGGER.info("preview form [{}], perm kind [{}]", form, form.getPermkind1());
		LOGGER.info("member [{}]", form.getMember());
		String url = request.getRequestURI();
		MemberVO params = new MemberVO();
		params.setMemberSeqNo(form.getMemberSeqNo());

		CertVO cert = CommonUtils.getCertSession(request);
		if ("3".equals(form.getStatCd())) {

			List<ChgHistoryFileVO> chgHistoryFileList = (List<ChgHistoryFileVO>) chgHistoryDAO.selectChgFileListTmp(CommonUtils.pojoToMap(params));

			model.put("chgHistoryFileList", chgHistoryFileList);
		}
		if (StringUtils.isEmpty(form.getCeoRegNo()) && StringUtils.isNotEmpty(form.getCeoRegNo1()) && StringUtils.isNotEmpty(form.getCeoRegNo2())) {
			form.setCeoRegNo(form.getCeoRegNo1() + form.getCeoRegNo2());
		}

		if (StringUtils.isEmpty(form.getTrustTel()) && StringUtils.isNotEmpty(form.getTrustTel1()) && StringUtils.isNotEmpty(form.getTrustTel2()) && StringUtils.isNotEmpty(form.getTrustTel3())) {
			form.setTrustTel(form.getTrustTel1() + "-" + form.getTrustTel2() + "-" + form.getTrustTel3());
		}

		if (StringUtils.isEmpty(form.getTrustFax()) && StringUtils.isNotEmpty(form.getTrustFax1()) && StringUtils.isNotEmpty(form.getTrustFax2()) && StringUtils.isNotEmpty(form.getTrustFax3())) {
			form.setTrustFax(form.getTrustFax1() + "-" + form.getTrustFax2() + "-" + form.getTrustFax3());
		}

		if (StringUtils.isEmpty(form.getCeoTel()) && StringUtils.isNotEmpty(form.getCeoTel1()) && StringUtils.isNotEmpty(form.getCeoTel2()) && StringUtils.isNotEmpty(form.getCeoTel3())) {
			form.setCeoTel(form.getCeoTel1() + "-" + form.getCeoTel2() + "-" + form.getCeoTel3());
		}

		if (StringUtils.isEmpty(form.getCeoFax()) && StringUtils.isNotEmpty(form.getCeoFax1()) && StringUtils.isNotEmpty(form.getCeoFax2()) && StringUtils.isNotEmpty(form.getCeoFax3())) {
			form.setCeoFax(form.getCeoFax1() + "-" + form.getCeoFax2() + "-" + form.getCeoFax3());
		}

		if (StringUtils.isEmpty(form.getCeoEmail()) && StringUtils.isNotEmpty(form.getCeoEmail1()) && StringUtils.isNotEmpty(form.getCeoEmail2())) {
			form.setCeoEmail(form.getCeoEmail1() + "@" + form.getCeoEmail2());
		}

		// birthday
		String ceoRegNo = form.getCeoRegNo();
		String year = "1,2,5,6,".indexOf(ceoRegNo.substring(6, 7)) > -1 ? "19" : "20";
		String birthday = CommonUtils.toFormat(year + ceoRegNo.substring(0, 6), "####.##.##");

		//
		String writingKind = "", permKind = "";
		List<String> permKindList = new ArrayList<String>();

		try {

			for (CommonCodeVO item : codeService.getCodes("WRITINGKIND")) {
				for (WritingKindVO item2 : form.getWritingKindList()) {
					if (item2.getWritingKind().equals(item.getCodeValue())) {

						writingKind += ", " + item.getCodeValueDesc();
					}
				}
			}
			if (StringUtils.isNotBlank(writingKind))
				writingKind = writingKind.substring(2);

			for (CommonCodeVO item : codeService.getCodes("PERMKIND")) {
				String str = "";
				for (PermKindVO item2 : form.getPermKindList()) {
					if (item2.getPermKind().equals(item.getCodeValue())) {
						if (item2.getPermEtc() == null || ("").equals(item2.getPermEtc())) {
							str = item.getCodeValueDesc();
						} else {
							str = item.getCodeValueDesc() + "(" + item2.getPermEtc() + ")";
						}

						if (!permKindList.contains(str)) {

							permKindList.add(str);
							permKind += ", " + str;
						}
					}
				}
			}
			if (StringUtils.isNotBlank(permKind))
				permKind = permKind.substring(2);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		List<String> getChgCd = form.getChgCd();
		String conditionCd = form.getConditionCd();
		String statCd = form.getStatCd();
		ApplicationForm preform = getApplication(request, model, conditionCd, statCd);
		String prewritingKind = "", prepermKind = "";
		List<String> prepermKindList = new ArrayList<String>();

		if (url.equals("/cocoms/appl/trust/mod_print") || url.equals("/cocoms/appl/sub/mod_print")) {
			try {

				for (CommonCodeVO item : codeService.getCodes("WRITINGKIND")) {
					for (ChgWritingKindVO item2 : preform.getPrewritingKindListChg()) {

						if (item2.getWritingKind().equals(item.getCodeValue())) {
							prewritingKind += ", " + item.getCodeValueDesc();
						}
					}
				}
				if (StringUtils.isNotBlank(prewritingKind))
					prewritingKind = prewritingKind.substring(2);

				for (CommonCodeVO item : codeService.getCodes("PERMKIND")) {
					String str = "";
					for (ChgPermKindVO item2 : preform.getPrepermKindListChg()) {
						if (item2.getPermKind().equals(item.getCodeValue())) {
							if (item2.getPermEtc() == null || ("").equals(item2.getPermEtc())) {
								str = item.getCodeValueDesc();
							} else {
								str = item.getCodeValueDesc() + "(" + item2.getPermEtc() + ")";
							}

							if (!prepermKindList.contains(str)) {

								prepermKindList.add(str);
								prepermKind += ", " + str;
							}
						}
					}
				}
				if (StringUtils.isNotBlank(prepermKind))
					prepermKind = prepermKind.substring(2);
				/* preform.setPreList(""); */
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}

		} else {

			try {

				for (CommonCodeVO item : codeService.getCodes("WRITINGKIND")) {
					for (WritingKindVO item2 : preform.getWritingKindList()) {
						System.out.println("????" + preform.getWritingKindList());
						if (item2.getWritingKind().equals(item.getCodeValue())) {
							System.out.println("??????" + item2.getWritingKind());
							prewritingKind += ", " + item.getCodeValueDesc();
						}
					}
				}
				if (StringUtils.isNotBlank(prewritingKind))
					prewritingKind = prewritingKind.substring(2);

				for (CommonCodeVO item : codeService.getCodes("PERMKIND")) {
					String str = "";
					for (PermKindVO item2 : preform.getPermKindList()) {
						if (item2.getPermKind().equals(item.getCodeValue())) {
							if (item2.getPermEtc() == null || ("").equals(item2.getPermEtc())) {
								str = item.getCodeValueDesc();
							} else {
								str = item.getCodeValueDesc() + "(" + item2.getPermEtc() + ")";
							}

							if (!prepermKindList.contains(str)) {

								prepermKindList.add(str);
								prepermKind += ", " + str;
							}
						}
					}
				}
				if (StringUtils.isNotBlank(prepermKind))
					prepermKind = prepermKind.substring(2);

			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}

		}
		//
		model.addAttribute("member", form);
		model.addAttribute("cert", cert);
		model.addAttribute("premember", preform);
		model.addAttribute("birthday", birthday);
		model.addAttribute("ChgCd", getChgCd);
		model.put("postwritingKind", writingKind);
		model.put("postpermKind", permKind);
		model.put("prepermKind", prepermKind);
		model.put("prewritingKind", prewritingKind);

		if (request.getRequestURI().indexOf("sub/reg_print") > -1) {
			return "usr/appl/appl_print1";
		} else if (request.getRequestURI().indexOf("sub/mod_print") > -1) {
			return "usr/appl/appl_print2";
		} else if (request.getRequestURI().indexOf("trust/reg_print") > -1) {
			return "usr/appl/appl_print3";
		} else {
			return "usr/appl/appl_print4";
		}
	}

	@RequestMapping(value = "/appl/{certInfoSeqNo}/charge", method = RequestMethod.POST)
		public String chargeForm(HttpServletRequest request, @ModelAttribute("member") ApplicationForm form,			@PathVariable String certInfoSeqNo,			//			BindingResult bindingResult, 
			SessionStatus status, ModelMap model) throws Exception {
		LOGGER.info("charge form");
		LOGGER.info("certInfoSeqNo [{}] [{}]", form.getCertInfoSeqNo(), certInfoSeqNo);
		LOGGER.info("g4cssCK [{}]", form.getG4cssCK());

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		boolean gpki_use = propertiesService.getBoolean("gpki.use");
		Properties prop = new Properties();
		prop.setProperty("cms.ldapUrl", propertiesService.getString("gpki.ldapUrl"));
		prop.setProperty("cms.certForSignFile", propertiesService.getString("gpki.certForSignFile"));
		prop.setProperty("cms.keyForSignFile", propertiesService.getString("gpki.keyForSignFile"));
		prop.setProperty("cms.certForEnvFile", propertiesService.getString("gpki.certForEnvFile"));
		prop.setProperty("cms.keyForEnvFile", propertiesService.getString("gpki.keyForEnvFile"));
		prop.setProperty("cms.pinForSign", propertiesService.getString("gpki.pinForSign"));
		prop.setProperty("cms.pinForEnv", propertiesService.getString("gpki.pinForEnv"));
		prop.setProperty("cms.DN", propertiesService.getString("gpki.DN"));
		prop.setProperty("cms.OU", propertiesService.getString("gpki.OU"));
		prop.setProperty("cms.AT", propertiesService.getString("gpki.AT"));
		prop.setProperty("cms.LIC", propertiesService.getString("gpki.LIC"));

		/* return value */  //테스트결제시 true변경 try문 주석처리 암호화
		boolean result =true;
		/*try {
			LOGGER.info("g4cssCK real [{}]", form.getG4cssCK());
			String g4cssCK = new GPKICms(prop, gpki_use).cmsDec(form.getG4cssCK());
			LOGGER.info("g4cssCK result [{}]", g4cssCK);

			String[] val = g4cssCK.split("\\^");

			CertInfoVO certInfo = new CertInfoVO();

			// error
			if (g4cssCK.indexOf("\\^") == 1 && val.length > 3) {
				result = false;
				certInfo.setIsSuccess(val[0]);
				certInfo.setReqFindTime(val[1]);
				//				certInfo.setMallSeq(val[2]);
				certInfo.setIsFail(val[3]);
			}
			// success 
			else if (g4cssCK.indexOf("\\^") != 1 && val.length > 4) {
				result = true;
				certInfo.setIsSuccess(val[0]);
				certInfo.setReqFindTime(val[1]);
				//				certInfo.setMallSeq(val[2]);
				//				certInfo.setPayMthdCd(val[3]);
				//				certInfo.setPgFee(val[4]);
			}

			// update CERT_INFO
			certInfo.setCertInfoSeqNo(certInfoSeqNo);
			certInfoService.saveCertInfo(certInfo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}*/

		MemberVO member = form.getMember();
		List<WritingKindVO> writingKindList = form.getWritingKindList();
		List<PermKindVO> permKindList = form.getPermKindList();
		LOGGER.info("member [{}]", member);
		LOGGER.info("writingKindList [{}]", writingKindList);
		LOGGER.info("permKindList [{}]", permKindList);

		member.setStatCd(form.getStatCd());
		member.setRegDate2(DateUtils.getCurrDateNumber());

		if (result) {

			/* save */
			try {
				applicationService.save(memberSeqNo, form.getConditionCd(), member, writingKindList, permKindList, form.getChgCd(), form.getChgMemo());
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}

			// save log
			actionLogService.saveLog(request, "charge");

			// noti
			if ("1".equals(form.getConditionCd())) {
				if ("1".equals(form.getStatCd())) {
					super.noti("31", CommonUtils.getLoginSession(request));
				} else if ("2".equals(form.getStatCd())) {
					super.noti("32", CommonUtils.getLoginSession(request));
				}
			} else if ("2".equals(form.getConditionCd())) {
				if ("1".equals(form.getStatCd())) {
					super.noti("41", CommonUtils.getLoginSession(request));
				} else if ("2".equals(form.getStatCd())) {
					super.noti("42", CommonUtils.getLoginSession(request));
				}
			}
		}

		// TODO remove temp

		status.setComplete();

		model.put("result", result);
		return "usr/appl/appl_charge";
	}

	@RequestMapping(value = "/appl/recharge", method = RequestMethod.POST)
	public String rechargeForm(HttpServletRequest request, @ModelAttribute("member") ApplicationForm form,

			//			@PathVariable String certInfoSeqNo,
			//			BindingResult bindingResult, 
			SessionStatus status, ModelMap model) throws Exception {
		LOGGER.info("recharge form");

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		/* return value */
		boolean result = true;

		MemberVO member = form.getMember();
		List<WritingKindVO> writingKindList = form.getWritingKindList();
		List<PermKindVO> permKindList = form.getPermKindList();
		LOGGER.info("member [{}]", member);
		LOGGER.info("writingKindList [{}]", writingKindList);
		LOGGER.info("permKindList [{}]", permKindList);

		member.setStatCd(form.getStatCd());
		member.setRegDate2(DateUtils.getCurrDateNumber());

		if (result) {

			/* save */
			try {
				applicationService.save(memberSeqNo, form.getConditionCd(), member, writingKindList, permKindList, form.getChgCd(), form.getChgMemo());
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}

			// save log
			actionLogService.saveLog(request, "resubmit");

			// noti
			if ("1".equals(form.getConditionCd())) {
				if ("1".equals(form.getStatCd())) {
					super.noti("31", CommonUtils.getLoginSession(request));
				} else if ("2".equals(form.getStatCd())) {
					super.noti("32", CommonUtils.getLoginSession(request));
				}
			} else if ("2".equals(form.getConditionCd())) {
				if ("1".equals(form.getStatCd())) {
					super.noti("41", CommonUtils.getLoginSession(request));
				} else if ("2".equals(form.getStatCd())) {
					super.noti("42", CommonUtils.getLoginSession(request));
				}
			}
		}

		try {
			status.setComplete();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		model.put("result", result);
		return "usr/appl/appl_recharge";
	}

	private ApplicationForm getmodiApplication(HttpServletRequest request, ModelMap model, String conditionCd, String statCd) {

		LoginVO sessionUser = CommonUtils.getLoginSession(request);
		MemberVO sessionMember = CommonUtils.getMemberSession(request);
		LOGGER.info("session user [{}]", sessionUser);
		LOGGER.info("session member [{}]", sessionMember);

		String memberSeqNo = sessionUser.getMemberSeqNo();
		LOGGER.info("memberSeqNo2 [{}]", memberSeqNo);

		ApplicationForm appl = new ApplicationForm();
		try {
			String tmpcd = "";
			// execute service

			Map<String, Object> map = applicationService.getmodiAppliation(memberSeqNo, conditionCd);
			tmpcd = (String) map.get("tmpcd");
			System.out.println("tmpcd" + tmpcd);
			if ("1".equals(tmpcd)) {

				ChgHistoryVO ChgHistoryVO = (ChgHistoryVO) map.get("chgHistory");
				List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) map.get("writingKindList");
				List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) map.get("permKindList");
				List<ChgCdVO> ChgCdList = (List<ChgCdVO>) map.get("ChgCdVO");

				String conditioncd = (String) map.get("conditioncd");

				String statcd = (String) map.get("statcd");
				String conDetailCd = (String) map.get("condetailcd");

				appl = CommonUtils.ChgHistoryToApplication(ChgHistoryVO);
				appl.setConditionCd(conditioncd);
				appl.setStatCd(statcd);
				appl.setConDetailCd(conDetailCd);
				appl.setMemberSeqNo(memberSeqNo);

				/* writing kind list */
				for (ChgWritingKindVO item : writingKindList) {
					LOGGER.info("writing kind [{}]", item);
					String writingKind = item.getWritingKind();
					String writingEtc = item.getWritingEtc();

					model.addAttribute("writingKind" + writingKind, "checked");
					model.addAttribute("writingEtc" + writingKind, writingEtc);
				}

				/* perm kind list */
				for (ChgPermKindVO item : permKindList) {
					LOGGER.info("perm kind [{}]", item);

					String writingKind = permKindDAO.selectWritingKindTmp(item.getChgwritingkindseqno());
					String permKind = item.getPermKind();
					String permEtc = item.getPermEtc();

					model.addAttribute("permkind" + writingKind + "_" + permKind, "checked");
					model.addAttribute("permEtc" + writingKind, permEtc);
				}
				/* chgcd list */

				for (ChgCdVO item : ChgCdList) {

					String chgcd = item.getChgCd();

					model.addAttribute("chgcd" + chgcd, "checked");

				}
			} else {

				MemberVO member = (MemberVO) map.get("member");
				List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) map.get("writingKindList");
				List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) map.get("permKindList");
				List<ChgCdVO> ChgCdList2 = (List<ChgCdVO>) map.get("ChgCdVO2");
				appl = CommonUtils.memberToApplication(member);

				if (!("2").equals(member.getStatCd()) && !("4").equals(member.getStatCd())) {
					System.out.println(member.getStatCd());
					for (ChgCdVO item : ChgCdList2) {

						String chgcd = item.getChgCd();

						model.addAttribute("chgcd" + chgcd, "checked");

					}
				}
				/* writing kind list */
				for (ChgWritingKindVO item : writingKindList) {
					LOGGER.info("chgwriting kind [{}]", item);
					String writingKind = item.getWritingKind();
					String writingEtc = item.getWritingEtc();

					model.addAttribute("writingKind" + writingKind, "checked");
					model.addAttribute("writingEtc" + writingKind, writingEtc);
				}

				/* perm kind list */
				for (ChgPermKindVO item : permKindList) {
					LOGGER.info("chgperm kind [{}]", item);
					String writingKind = item.getWritingKind();
					String permKind = item.getPermKind();
					String permEtc = item.getPermEtc();

					model.addAttribute("permkind" + writingKind + "_" + permKind, "checked");
					model.addAttribute("permEtc" + writingKind, permEtc);
				}

			}
		} catch (Exception e) {
			//			e.printStackTrace();
			//			LOGGER.error(e.getMessage());
			//			throw e;
		}

		/*
		 * // default value if (StringUtils.isEmpty(appl.getCoGubunCd())) {
		 * appl.setCoGubunCd("2"); }
		 * 
		 * if (StringUtils.isNotEmpty(sessionMember.getCoName())) {
		 * appl.setCoName(sessionMember.getCoName()); } if
		 * (StringUtils.isNotEmpty(sessionMember.getCoNo()) &&
		 * sessionMember.getCoNo().length() >= 10) {
		 * appl.setCoNo1(sessionMember.getCoNo().substring(0, 3));
		 * appl.setCoNo2(sessionMember.getCoNo().substring(3, 5));
		 * appl.setCoNo3(sessionMember.getCoNo().substring(5, 10)); } if
		 * (StringUtils.isNotEmpty(sessionMember.getCeoRegNo()) &&
		 * sessionMember.getCeoRegNo().length() >= 13) {
		 * appl.setCeoRegNo1(sessionMember.getCeoRegNo().substring(0, 6));
		 * appl.setCeoRegNo2(sessionMember.getCeoRegNo().substring(6, 13)); } if
		 * (StringUtils.isNotEmpty(sessionMember.getCeoName())) {
		 * appl.setCeoName(sessionMember.getCeoName()); } if
		 * (StringUtils.isNotEmpty(sessionMember.getCeoMobile()) &&
		 * sessionMember.getCeoMobile().indexOf("-") > -1) { StringTokenizer st
		 * = new StringTokenizer(sessionMember.getCeoMobile(), "-"); if
		 * (st.hasMoreTokens()) appl.setCeoMobile1(st.nextToken()); if
		 * (st.hasMoreTokens()) appl.setCeoMobile2(st.nextToken()); if
		 * (st.hasMoreTokens()) appl.setCeoMobile3(st.nextToken()); } if
		 * (StringUtils.isNotEmpty(sessionMember.getCeoEmail()) &&
		 * sessionMember.getCeoEmail().indexOf("@") > -1) { String email =
		 * sessionMember.getCeoEmail(); appl.setCeoEmail1(email.substring(0,
		 * email.indexOf("@")));
		 * appl.setCeoEmail2(email.substring(email.indexOf("@")+1)); }
		 */
		if ("Y".equals(sessionMember.getSmsAgree())) {
			appl.setSmsAgree("Y");
			appl.setAgreeDate(DateUtils.getCurrDateNumber());
		} else {
			appl.setSmsAgree("N");
			appl.setAgreeDate("");
		}

		if (StringUtils.isEmpty(appl.getRegDate2())) {
			appl.setRegDate2(DateUtils.getCurrDateNumber());
		}

		return appl;
	}

	@RequestMapping(value = "/appl/status/companies", method = RequestMethod.GET)
	public String companyList(HttpServletRequest request,
			//			@ModelAttribute UserForm searchVO,
			@ModelAttribute MemberVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		/** execute service */
		List<MemberVO> list = new ArrayList<MemberVO>();
		try {

			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("order", "appNo");

			Map<String, Object> data = userService.getMembers(filter);
			list = (List<MemberVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		/** make return value */
		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "usr/stat/stat_company";
	}

	@RequestMapping(value = "/appl/status/zip_code", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getZipCode(HttpServletRequest request, @RequestParam(value = "sido", defaultValue = "", required = false) String sido, ModelMap model) throws Exception {
		LOGGER.info("sido [{}]", sido);

		List<ZipCodeVO> list = new ArrayList<ZipCodeVO>();
		try {

			list = codeService.getZipCodes(sido);

			for (ZipCodeVO item : list) {
				item.setSido(URLEncoder.encode(item.getSido(), "UTF-8"));
				item.setGugun(URLEncoder.encode(item.getGugun(), "UTF-8"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);

		return CommonUtils.makeSuccess(map);
	}

	@RequestMapping(value = "/appl_histories", method = RequestMethod.POST)
	@ResponseBody
	public String saveApplHistory(HttpServletRequest request, ApplHistoryVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		LoginVO login = CommonUtils.getLoginSession(request);
		searchVO.setMemberSeqNo(login.getMemberSeqNo());
		searchVO.setCoName(login.getCoName());
		searchVO.setCoNo(login.getCoNo());

		//		searchVO.setApplContent("");
		//		searchVO.setPdfContent("");

		//
		applHistoryService.saveApplHistory(searchVO);

		// noti
		super.noti("22", login);

		return CommonUtils.makeSuccess();
	}

}
