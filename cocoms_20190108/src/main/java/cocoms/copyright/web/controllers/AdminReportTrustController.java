package cocoms.copyright.web.controllers;

import java.net.URLEncoder;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.report.ReportService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin/reports")
public class AdminReportTrustController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** ReportService */
	@Resource(name = "reportService")
	protected ReportService reportService;

	/** UserService */
	@Resource(name = "userService")
	protected UserService userService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = "/trust", method = RequestMethod.GET)
	public String reportTrust(HttpServletRequest request, @ModelAttribute ReportInfoVO searchVO, @RequestParam(value = "sort", defaultValue = "") String sort, ModelMap model) throws Exception {

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<ReportVO> list = new ArrayList<ReportVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("order", sort);

			Map<String, Object> data = reportService.getTrustReports(filter);
			list = (List<ReportVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		List<String> yearList = new ArrayList<String>();
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4)) - 1;
		for (int i = 2007; i < year; i++) {
			yearList.add(String.valueOf(i));
		}

		List<String> yearList2 = new ArrayList<String>();
		for (int i = year - 4; i <= year; i++) {
			yearList2.add(String.valueOf(i));
		}

		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		model.put("sort", sort);

		model.put("yearList", yearList);
		model.put("yearList2", yearList2);

		return "adm/report/trust_report";
	}

	@RequestMapping(value = "/trust/excel", method = RequestMethod.GET)
	public void reportTrustExcel(HttpServletRequest request, HttpServletResponse response, @ModelAttribute ReportInfoVO searchVO, @RequestParam(value = "sort", defaultValue = "") String sort, ModelMap model) throws Exception {

		List<ReportVO> list = new ArrayList<ReportVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);
			filter.put("order", sort);

			Map<String, Object> data = reportService.getTrustReports(filter);
			list = (List<ReportVO>) data.get("list");
			int total = (int) data.get("total");

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4)) - 1;

		List<String> yearList2 = new ArrayList<String>();
		for (int i = year - 4; i <= year; i++) {
			yearList2.add(String.valueOf(i));
		}

		String filename = DateUtils.getCurDateTime() + ".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("허가번호");
			col.add("업체명");
			for (String yyyy : yearList2) {
				col.add(yyyy + "년");
			}

			list2.add(col);
		}
		int cnt = 0;
		for (ReportVO item : list) {
			String no = String.valueOf(list.size() - cnt);
			String appNoStr = StringUtils.isNotEmpty(item.getAppNo2()) ? "허가" + item.getAppNoStr() : "";
			String year5 = "1".equals(item.getYear5()) ? "O" : "X";
			String year4 = "1".equals(item.getYear4()) ? "O" : "X";
			String year3 = "1".equals(item.getYear3()) ? "O" : "X";
			String year2 = "1".equals(item.getYear2()) ? "O" : "X";
			String year1 = "1".equals(item.getYear1()) ? "O" : "X";

			List<String> col = new ArrayList<String>();
			col.add(no); // 번호    
			col.add(appNoStr); // 허가번호 
			col.add(item.getCoName()); // 업체명
			col.add(year5); // 
			col.add(year4); // 
			col.add(year3); // 
			col.add(year2); // 
			col.add(year1); // 

			list2.add(col);
			cnt++;
		}

		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}

	@RequestMapping(value = "/trust/all/{year}", method = RequestMethod.GET)
	public String reportTrustAll(HttpServletRequest request, @PathVariable String year, ModelMap model) throws Exception {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {

			list = reportService.getSubAllReports(year);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("list", list);

		return "adm/report/sub_report_all";
	}

	@RequestMapping(value = "/trust/excel/{year}/{type}", method = RequestMethod.GET)
	public String reportTrustAllExcel(HttpServletRequest request, @PathVariable String year, @PathVariable String type, ModelMap model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			// TODO
			map = reportService.getSubReports(year, type);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = "1".equals(type) ? "실적보고등록업체_" : "실적보고미등록업체_";
		filename += DateUtils.getCurrDateNumber() + ".xls";
		filename = URLEncoder.encode(filename, "UTF-8");

		model.putAll(map);
		model.put("filename", filename);

		return "adm/report/sub_report_excel";
	}

	@RequestMapping(value = "/trust/{memberSeqNo}", method = RequestMethod.GET)
	public String reportTrustDetail(HttpServletRequest request, @PathVariable String memberSeqNo,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("memberSeqNo", memberSeqNo);

		List<TrustStatVO> list = new ArrayList<TrustStatVO>();
		MemberVO member = new MemberVO();
		try {

			list = reportService.getTrustReportSumStatuses(filter);

			member = userService.getMember(memberSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4)) - 1;
		boolean check = false;
		try {

			/*
			 * String rptYear = DateUtils.getCurrDateNumber().substring(0, 4);
			 */
			String rptYear = String.valueOf(year);
			Map<String, Object> map = reportService.getTrustReport(member.getMemberSeqNo(), rptYear);
			TrustInfoVO report = (TrustInfoVO) map.get("report");

			LOGGER.info("report [{}]", report);
			if (report == null || StringUtils.isEmpty(report.getMemberSeqNo())) {
				check = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", searchVO);
		model.put("list", list);
		model.put("member", member);
		model.put("reg_year", year);
		model.put("reg_auth", check);

		return "adm/report/trust_report_view";
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/excel", method = RequestMethod.GET)
	public void reportTrustDetailExcel(HttpServletRequest request, HttpServletResponse response, @PathVariable String memberSeqNo,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("memberSeqNo", memberSeqNo);

		List<TrustStatVO> list = new ArrayList<TrustStatVO>();
		try {

			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);
			list = reportService.getTrustReportSumStatuses(filter);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = DateUtils.getCurDateTime() + ".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			//			col.add("번호");
			col.add("년도");
			col.add("실적정보");
			col.add("저작권료 징수분배 내역");
			col.add("사업계획");

			list2.add(col);
		}
		int cnt = 0;
		for (TrustStatVO item : list) {
			LOGGER.info("item.getTrustInfoSeqNo() [{}]", item.getTrustInfoSeqNo());

			LOGGER.info("item.getTrustCopyrightSeqNo() [{}]", item.getTrustCopyrightSeqNo());
			LOGGER.info("item.getTrustExecSeqNo() [{}]", item.getTrustExecSeqNo());
			LOGGER.info("item.getTrustMakerSeqNo() [{}]", item.getTrustMakerSeqNo());
			LOGGER.info("item.getTrustRoyaltySeqNo() [{}]", item.getTrustRoyaltySeqNo());

			LOGGER.info("item.getBusinessGoal() [{}]", item.getBusinessGoal());
			LOGGER.info("item.getProp() [{}]", item.getProp());

			String no = String.valueOf(list.size() - cnt);
			String trustInfoSeqNo = StringUtils.isNotEmpty(item.getTrustInfoSeqNo()) && Integer.valueOf(item.getTrustInfoSeqNo()) > 0 ? "O" : "X";
			String trustCopyrightSeqNo = (StringUtils.isNotEmpty(item.getTrustCopyrightSeqNo()) && Integer.valueOf(item.getTrustCopyrightSeqNo()) > 0)
					|| (StringUtils.isNotEmpty(item.getTrustExecSeqNo()) && Integer.valueOf(item.getTrustExecSeqNo()) > 0) || (StringUtils.isNotEmpty(item.getTrustMakerSeqNo()) && Integer.valueOf(item.getTrustMakerSeqNo()) > 0)
					|| (StringUtils.isNotEmpty(item.getTrustRoyaltySeqNo()) && Integer.valueOf(item.getTrustRoyaltySeqNo()) > 0) ? "O" : "X";
			String businessGoal = StringUtils.isNotEmpty(item.getBusinessGoal()) || StringUtils.isNotEmpty(item.getProp()) ? "O" : "X";

			List<String> col = new ArrayList<String>();
			//			col.add(no);					// 번호    
			col.add(item.getRptYear()); // 년도 
			col.add(trustInfoSeqNo); // 실적정보 
			col.add(trustCopyrightSeqNo); // 저작권료 징수분배 내역 
			col.add(businessGoal); // 사업계획

			list2.add(col);
			cnt++;
		}

		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}", method = RequestMethod.GET)
	public String reportTrustDetail(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "adm/report/trust_report_view_year";
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}/tab2", method = RequestMethod.GET)
	public String reportTrustDetail2(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "adm/report/trust_report_view_year2";
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}/tab3", method = RequestMethod.GET)
	public String reportTrustDetail3(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "adm/report/trust_report_view_year3";
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}/save", method = RequestMethod.GET)
	public String reportTrustExcel(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year, ModelMap model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.putAll(getReportSubDetail(memberSeqNo, year));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = "신탁단체_실적보고_";
		filename += DateUtils.getCurrDateNumber() + ".xls";
		filename = URLEncoder.encode(filename, "UTF-8");

		model.putAll(map);
		model.put("filename", filename);
		model.put("date", DateUtils.getCurDate());

		return "adm/report/trust_report_excel2";
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}/tab2/save", method = RequestMethod.GET)
	public String reportTrustExcel2(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year, ModelMap model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.putAll(getReportSubDetail(memberSeqNo, year));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = "신탁단체_저작권징수분배내역_";
		filename += DateUtils.getCurrDateNumber() + ".xls";
		filename = URLEncoder.encode(filename, "UTF-8");

		model.putAll(map);
		model.put("filename", filename);
		model.put("date", DateUtils.getCurDate());

		return "adm/report/trust_report_excel3";
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}/tab3/save", method = RequestMethod.GET)
	public String reportTrustExcel3(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year, ModelMap model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.putAll(getReportSubDetail(memberSeqNo, year));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = "신탁단체_사업계획_";
		filename += DateUtils.getCurrDateNumber() + ".xls";
		filename = URLEncoder.encode(filename, "UTF-8");

		model.putAll(map);
		model.put("filename", filename);
		model.put("date", DateUtils.getCurDate());

		return "adm/report/trust_report_excel4";
	}

	@RequestMapping(value = { "/trust/{memberSeqNo}/years/{year}/edit", "/trust/{memberSeqNo}/years/{year}/new" }, method = RequestMethod.GET)
	public String reportTrustDetailModifyForm(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "adm/report/trust_report_view_year_edit";
	}

	@RequestMapping(value = { "/trust/{memberSeqNo}/years/{year}/tab2/edit", "/trust/{memberSeqNo}/years/{year}/tab2/new" }, method = RequestMethod.GET)
	public String reportTrustDetailModifyForm2(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "adm/report/trust_report_view_year2_edit";
	}

	@RequestMapping(value = { "/trust/{memberSeqNo}/years/{year}/tab3/edit", "/trust/{memberSeqNo}/years/{year}/tab3/new" }, method = RequestMethod.GET)
	public String reportTrustDetailModifyForm3(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "adm/report/trust_report_view_year3_edit";
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}", method = RequestMethod.POST)
	public String reportTrustDetailModify(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year, @ModelAttribute TrustForm form,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		form.setMemberSeqNo(memberSeqNo);
		form.setRptYear(year);

		try {

			Map<String, Object> map = reportService.getTrustReport(memberSeqNo, year);
			if (map.get("report") != null) {

				TrustInfoVO report = (TrustInfoVO) map.get("report");

				//				BeanUtils.copyProperties(form, report);
				form.setBusinessGoal(report.getBusinessGoal());
				form.setProp(report.getProp());
				form.setFilename5(report.getFilename5());
				form.setFilepath5(report.getFilepath5());
				form.setFilename6(report.getFilename6());
				form.setFilepath6(report.getFilepath6());
				form.setFilename7(report.getFilename7());
				form.setFilepath7(report.getFilepath7());
				form.setFilename8(report.getFilename8());
				form.setFilepath8(report.getFilepath8());
				form.setFilename9(report.getFilename9());
				form.setFilepath9(report.getFilepath9());
			}

			reportService.saveTrustReport(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "redirect:/admin/reports/trust/" + memberSeqNo + "/years/" + year;
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}/tab2", method = RequestMethod.POST)
	public String reportTrustDetailModify2(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year, @ModelAttribute TrustForm form,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		form.setMemberSeqNo(memberSeqNo);
		form.setRptYear(year);
		
		try {

			List<TrustStat2VO> list = new ArrayList<TrustStat2VO>();
			int idx = 0;
			for (String trustRoyaltySeqNo : form.getTrustRoyaltySeqNo()) {
				
				TrustStat2VO item = new TrustStat2VO();
				item.setTrustRoyaltySeqNo(form.getTrustRoyaltySeqNo().get(idx));
				item.setTrustGubunSeqNo(form.getTrustGubunSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());
				item.setContract(form.getContract().get(idx));
				item.setInCollected(form.getInCollected().get(idx));
				item.setOutCollected(form.getOutCollected().get(idx));
				item.setCollectedSum(form.getCollectedSum().get(idx));
				item.setInDividend(form.getInDividend().get(idx));
				item.setOutDividend(form.getOutDividend().get(idx));
				item.setDividendSum(form.getDividendSum().get(idx));
				item.setInDividendOff(form.getInDividendOff().get(idx));
				item.setOutDividendOff(form.getOutDividendOff().get(idx));
				item.setDividendOffSum(form.getDividendOffSum().get(idx));
				item.setInChrg(form.getInChrg().get(idx));
				item.setOutChrg(form.getOutChrg().get(idx));
				item.setChrgSum(form.getChrgSum().get(idx));
				item.setInChrgRate(form.getInChrgRate().get(idx));
				item.setOutChrgRate(form.getOutChrgRate().get(idx));
				item.setChrgRateSum(form.getChrgRateSum().get(idx));
				item.setTrustMemo(form.getTrustMemo().get(idx));
				
				item.setGubunName(form.getGubunName().get(idx));
				item.setGubunKind(form.getGubunKind().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportRoyaltyStatuses(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustCopyrightSeqNo : form.getTrustCopyrightSeqNo()) {
				
				TrustStat6VO item = new TrustStat6VO();
				item.setTrustCopyrightSeqNo(form.getTrustCopyrightSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());
				
				item.setGubun(form.getGubun().get(idx));
				item.setOrganization(form.getOrganization().get(idx));
				item.setCollected(form.getCollected().get(idx));
				item.setDividend(form.getDividend().get(idx));
				item.setDividendOff(form.getDividendOff().get(idx));
				item.setChrg(form.getChrg().get(idx));
				item.setChrgRate(form.getChrgRate().get(idx));
				item.setMemo(form.getMemo().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportCopyrightStatuses(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustExecSeqNo : form.getTrustExecSeqNo()) {
				
				TrustStat6VO item = new TrustStat6VO();
				item.setTrustExecSeqNo(form.getTrustExecSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());
				
				item.setGubun(form.getGubun2().get(idx));
				item.setOrganization(form.getOrganization2().get(idx));
				item.setCollected(form.getCollected2().get(idx));
				item.setDividend(form.getDividend2().get(idx));
				item.setDividendOff(form.getDividendOff2().get(idx));
				item.setChrg(form.getChrg2().get(idx));
				item.setChrgRate(form.getChrgRate2().get(idx));
				item.setMemo(form.getMemo2().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportExecuteStatuses(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustMakerSeqNo : form.getTrustMakerSeqNo()) {
				
				TrustStat6VO item = new TrustStat6VO();
				item.setTrustMakerSeqNo(form.getTrustMakerSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());
				
				item.setGubun(form.getGubun3().get(idx));
				item.setOrganization(form.getOrganization3().get(idx));
				item.setCollected(form.getCollected3().get(idx));
				item.setDividend(form.getDividend3().get(idx));
				item.setDividendOff(form.getDividendOff3().get(idx));
				item.setChrg(form.getChrg3().get(idx));
				item.setChrgRate(form.getChrgRate3().get(idx));
				item.setMemo(form.getMemo3().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportMakerStatuses(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
	


		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "redirect:/admin/reports/trust/" + memberSeqNo + "/years/" + year + "/tab2";
		/*try {

			List<TrustStat2VO> list = new ArrayList<TrustStat2VO>();
			int idx = 0;
			TrustStat2VO item = new TrustStat2VO();
			String TrustinfoSeqno = form.getTrustInfoSeqno();

			for (String trustRoyaltySeqNo : form.getTrustRoyaltySeqNo()) {

				item.setTrustRoyaltySeqNo(form.getTrustRoyaltySeqNo().get(idx));
				item.setTrustGubunSeqNo(form.getTrustGubunSeqNo().get(idx));
				item.setTrustInfoSeqNo(TrustinfoSeqno);
				item.setContract(form.getContract().get(idx));
				item.setInCollected(form.getInCollected().get(idx));
				item.setOutCollected(form.getOutCollected().get(idx));
				item.setCollectedSum(form.getCollectedSum().get(idx));
				item.setInDividend(form.getInDividend().get(idx));
				item.setOutDividend(form.getOutDividend().get(idx));
				item.setDividendSum(form.getDividendSum().get(idx));
				item.setInDividendOff(form.getInDividendOff().get(idx));
				item.setOutDividendOff(form.getOutDividendOff().get(idx));
				item.setDividendOffSum(form.getDividendOffSum().get(idx));
				item.setInChrg(form.getInChrg().get(idx));
				item.setOutChrg(form.getOutChrg().get(idx));
				item.setChrgSum(form.getChrgSum().get(idx));
				item.setInChrgRate(form.getInChrgRate().get(idx));
				item.setOutChrgRate(form.getOutChrgRate().get(idx));
				item.setChrgRateSum(form.getChrgRateSum().get(idx));
				item.setTrustMemo(form.getTrustMemo().get(idx));
		
				item.setGubunName(form.getGubunName().get(idx));
				item.setGubunKind(form.getGubunKind().get(idx));
				item.setTrustMemo(form.getTrustMemo().get(idx));
				list.add(item);
				idx++;
				reportService.saveTrustReportRoyaltyStatuses(list);
			}
			 reportService.saveTrustReportRoyaltyStatuses(list); 

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustCopyrightSeqNo : form.getTrustCopyrightSeqNo()) {
				
				TrustStat6VO item = new TrustStat6VO();
				item.setTrustCopyrightSeqNo(form.getTrustCopyrightSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());
				
				item.setGubun(form.getGubun().get(idx));
				item.setOrganization(form.getOrganization().get(idx));
				item.setCollected(form.getCollected().get(idx));
				item.setDividend(form.getDividend().get(idx));
				item.setDividendOff(form.getDividendOff().get(idx));
				item.setChrg(form.getChrg().get(idx));
				item.setChrgRate(form.getChrgRate().get(idx));
				item.setMemo(form.getMemo().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportCopyrightStatuses(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustExecSeqNo : form.getTrustExecSeqNo()) {
				
				TrustStat6VO item = new TrustStat6VO();
				item.setTrustExecSeqNo(form.getTrustExecSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());
				
				item.setGubun(form.getGubun2().get(idx));
				item.setOrganization(form.getOrganization2().get(idx));
				item.setCollected(form.getCollected2().get(idx));
				item.setDividend(form.getDividend2().get(idx));
				item.setDividendOff(form.getDividendOff2().get(idx));
				item.setChrg(form.getChrg2().get(idx));
				item.setChrgRate(form.getChrgRate2().get(idx));
				item.setMemo(form.getMemo2().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportExecuteStatuses(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustMakerSeqNo : form.getTrustMakerSeqNo()) {
				
				TrustStat6VO item = new TrustStat6VO();
				item.setTrustMakerSeqNo(form.getTrustMakerSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());
				
				item.setGubun(form.getGubun3().get(idx));
				item.setOrganization(form.getOrganization3().get(idx));
				item.setCollected(form.getCollected3().get(idx));
				item.setDividend(form.getDividend3().get(idx));
				item.setDividendOff(form.getDividendOff3().get(idx));
				item.setChrg(form.getChrg3().get(idx));
				item.setChrgRate(form.getChrgRate3().get(idx));
				item.setMemo(form.getMemo3().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportMakerStatuses(list);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "redirect:/admin/reports/trust/" + memberSeqNo + "/years/" + year + "/tab2";*/
	}

	@RequestMapping(value = "/trust/{memberSeqNo}/years/{year}/tab3", method = RequestMethod.POST)
	public String reportTrustDetailModify3(HttpServletRequest request, @PathVariable String memberSeqNo, @PathVariable String year, @ModelAttribute TrustForm form,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		form.setMemberSeqNo(memberSeqNo);
		form.setRptYear(year);

		try {

			Map<String, Object> map = reportService.getTrustReport(memberSeqNo, year);
			TrustInfoVO report = (TrustInfoVO) map.get("report");

			//			BeanUtils.copyProperties(form, report);
			report.setBusinessGoal(form.getBusinessGoal());
			report.setProp(form.getProp());
			report.setFilename5(form.getFilename5());
			report.setFilepath5(form.getFilepath5());
			report.setFilename6(form.getFilename6());
			report.setFilepath6(form.getFilepath6());
			report.setFilename7(form.getFilename7());
			report.setFilepath7(form.getFilepath7());
			report.setFilename8(form.getFilename8());
			report.setFilepath8(form.getFilepath8());
			report.setFilename9(form.getFilename9());
			report.setFilepath9(form.getFilepath9());

			reportService.saveTrustReport(report);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "redirect:/admin/reports/trust/" + memberSeqNo + "/years/" + year + "/tab3";
	}

	private Map<String, Object> getReportSubDetail(String memberSeqNo, String year) throws Exception {

		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("memberSeqNo", memberSeqNo);
		filter.put("rptYear", year);

		//		ReportInfoVO report = new ReportInfoVO();
		Map<String, Object> map = new HashMap<String, Object>();
		//		List<TrustStat2VO> list = new ArrayList<TrustStat2VO>();
		Map<String, Object> map2 = new HashMap<String, Object>();
		try {

			map = reportService.getTrustReport(memberSeqNo, year);

			map2 = reportService.getTrustReportRoyaltyStatuses(filter, true);
			List<TrustStat2VO> list = (List<TrustStat2VO>) map2.get("royaltyList");
			int contractSum = 0;
			int inCollectedSum = 0, outCollectedSum = 0;
			int inDividendSum = 0, outDividendSum = 0;
			int inDividendOffSum = 0, outDividendOffSum = 0;
			int inChrgSum = 0, outChrgSum = 0;
			float inChrgRateSum = 0, outChrgRateSum = 0;
			int collectedSum = 0;
			int dividendOffSum = 0;
			int dividendSum = 0;
			int chrgSum = 0;
			float chrgRateSum = 0;
			
			String trustMemo = "";

			for (TrustStat2VO item : list) {
				contractSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getContract(), "0"));

				inCollectedSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getInCollected(), "0"));

				outCollectedSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getOutCollected(), "0"));

				collectedSum = inCollectedSum + outCollectedSum;

				inDividendSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getInDividend(), "0"));
				outDividendSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getOutDividend(), "0"));

				dividendSum = inDividendSum + outDividendSum;

				inDividendOffSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getInDividendOff(), "0"));
				outDividendOffSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getOutDividendOff(), "0"));

				dividendOffSum = inDividendOffSum + outDividendOffSum;

				inChrgSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getInChrg(), "0"));
				outChrgSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getOutChrg(), "0"));

				chrgSum = inChrgSum + outChrgSum;

				inChrgRateSum += Float.valueOf(StringUtils.defaultIfEmpty(item.getInChrgRate(), "0"));
				outChrgRateSum += Float.valueOf(StringUtils.defaultIfEmpty(item.getOutChrgRate(), "0"));

				chrgRateSum = inChrgRateSum + outChrgRateSum;
				trustMemo = StringUtils.defaultIfEmpty(item.getTrustMemo(), "");
			}
			map2.put("contractSum", contractSum);
			map2.put("inCollectedSum", inCollectedSum);
			map2.put("outCollectedSum", outCollectedSum);
			map2.put("inDividendSum", inDividendSum);
			map2.put("outDividendSum", outDividendSum);
			map2.put("inDividendOffSum", inDividendOffSum);
			map2.put("outDividendOffSum", outDividendOffSum);
			map2.put("inChrgSum", inChrgSum);
			map2.put("outChrgSum", outChrgSum);
			map2.put("inChrgRateSum", inChrgRateSum);
			map2.put("outChrgRateSum", outChrgRateSum);
			map2.put("collectedSum", collectedSum);
			map2.put("dividendSum", dividendSum);
			map2.put("dividendOffSum", dividendOffSum);
			map2.put("chrgSum", chrgSum);
			map2.put("chrgRateSum", chrgRateSum);
			map2.put("trustmemo", trustMemo);
			map.put("member", userService.getMember(memberSeqNo));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> ret = new HashMap<String, Object>();
		//		ret.put("report", report);
		ret.putAll(map);
		//		ret.put("list", list);
		ret.putAll(map2);
		ret.put("year", year);
		ret.put("lastYear", Integer.valueOf(year) - 1);

		return ret;
	}

}
