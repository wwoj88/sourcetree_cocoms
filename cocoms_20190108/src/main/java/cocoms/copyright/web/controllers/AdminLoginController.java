package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin")
public class AdminLoginController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String loginForm(@ModelAttribute UserForm form, ModelMap model, HttpServletRequest request) throws Exception {
		String ip = request.getRemoteAddr();
		//
		//System.out.println("IP ADD   ::" + ip);
		/*
		 * if(!ip.equals("219.250.104.102") && !ip.equals("125.128.75.222") &&
		 * !ip.equals("0:0:0:0:0:0:0:1") && !ip.equals("192.168.44.11") &&
		 * !ip.equals("192.168.44.10")){ return "redirect:/main"; }
		 */
		return "adm/login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpServletRequest request, @ModelAttribute UserForm form, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		LoginVO login = new LoginVO();
		BeanUtils.copyProperties(form, login);
		login.setCoGubun("admin");

		try {
			login = userService.getUser(login);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		if (login == null) {
			model.addAttribute("message", "아이디 또는 비밀번호가 틀립니다.");
			return "adm/login";
		}

		// session
		HttpSession session = request.getSession();
		session.setAttribute("SESS_ADMIN", login);
		session.setMaxInactiveInterval(12000); // sec

		// save log
		actionLogService.saveLog(request, "user login");

		return "redirect:/admin/main";
	}

	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request, ModelMap model) throws Exception {

		// save log
		actionLogService.saveLog(request, "user logout");

		// session
		HttpSession session = request.getSession();
		session.invalidate();

		return "redirect:/admin/login";
	}

	@RequestMapping(value = "/main")
	public String moveMain(HttpServletRequest request, ModelMap model) throws Exception {

		return "redirect:/admin/members1";
	}

}
