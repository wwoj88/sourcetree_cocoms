package cocoms.copyright.web.controllers;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.cert.CertInfoService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.GPKICms;
import cocoms.copyright.entity.*;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

import crosscert.*;

@Controller
public class CertController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** CertInfoService */
	@Resource(name = "certInfoService")
	private CertInfoService certInfoService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = "/cert", method = RequestMethod.POST)
	public @ResponseBody String save(HttpServletRequest request, CertForm form,
			//			BindingResult bindingResult, 
			//			SessionStatus status,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		//, produces = "application/json;charset=UTF-8"
		// data
		HttpSession session = request.getSession();
		LoginVO user = CommonUtils.getLoginSession(request);
		MemberVO member = CommonUtils.getMemberSession(request);
		LOGGER.info("user [{}]", user);
		LOGGER.info("member [{}]", member);

		String remark2 = member.getCoName() + " " + member.getCeoName();
		String reqManRrn = member.getCeoRegNo();
		String hddReqManName = user.getLoginId().length() > 9 ? user.getLoginId().substring(0, 9) : user.getLoginId();

		form.setRemark2(remark2);
		form.setReqManRrn(reqManRrn);
		form.setHddReqManName(hddReqManName);

		// insert CR_CERTINFO
		String certInfoSeqNo = "0";
		try {
			CertInfoVO certInfo = new CertInfoVO();
			BeanUtils.copyProperties(form, certInfo);

			certInfo.setGubun(user.getCoGubun());
			certInfo.setMemberSeqNo(user.getMemberSeqNo());

			String signature = user.getPwd() + user.getRegNo().substring(0, 7) + user.getLoginId();
			certInfo.setSignature(signature);

			certInfoSeqNo = certInfoService.saveCertInfo(certInfo);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}

		//
		String url = form.getReturnUrl().replace("/charge", "/" + certInfoSeqNo + "/charge");
		form.setReturnUrl(url);

		// g4cssCK
		String g4cssCK = "";

		String str = "";
		str += form.getUseSysCd().replaceAll("\\^", "<>") + "^";
		str += form.getMallSeq().replaceAll("\\^", "<>") + "^";
		str += form.getCappTot().replaceAll("\\^", "<>") + "^";
		str += form.getPgFee().replaceAll("\\^", "<>") + "^";
		str += form.getUseIncCd().replaceAll("\\^", "<>") + "^";

		str += form.getSetlIncCd().replaceAll("\\^", "<>") + "^";
		str += form.getRecvIncCd().replaceAll("\\^", "<>") + "^";
		str += form.getRemark().replaceAll("\\^", "<>") + "^";
		//		str += form.getRemark2().replaceAll("\\^", "<>")+"^";
		str += form.getReqManRrn().substring(0, 7).replaceAll("\\^", "<>") + "^";
		str += form.getHddReqManName().replaceAll("\\^", "<>") + "^";

		str += form.getPayMthdCd().replaceAll("\\^", "<>") + "^";
		str += form.getReturnUrl().replaceAll("\\^", "<>");
		LOGGER.info("strtest [{}]", str);

		LOGGER.info("str [{}]", str);
		boolean gpki_use = propertiesService.getBoolean("gpki.use");
		Properties prop = new Properties();
		prop.setProperty("cms.ldapUrl", propertiesService.getString("gpki.ldapUrl"));
		prop.setProperty("cms.certForSignFile", propertiesService.getString("gpki.certForSignFile"));
		prop.setProperty("cms.keyForSignFile", propertiesService.getString("gpki.keyForSignFile"));
		prop.setProperty("cms.certForEnvFile", propertiesService.getString("gpki.certForEnvFile"));
		prop.setProperty("cms.keyForEnvFile", propertiesService.getString("gpki.keyForEnvFile"));
		prop.setProperty("cms.pinForSign", propertiesService.getString("gpki.pinForSign"));
		prop.setProperty("cms.pinForEnv", propertiesService.getString("gpki.pinForEnv"));
		prop.setProperty("cms.DN", propertiesService.getString("gpki.DN"));
		prop.setProperty("cms.OU", propertiesService.getString("gpki.OU"));
		prop.setProperty("cms.AT", propertiesService.getString("gpki.AT"));
		prop.setProperty("cms.LIC", propertiesService.getString("gpki.LIC"));

		/*
		 * str = new String(str.getBytes("euc-kr"), "KSC5601");
		 * System.out.println("str <br>"+str);
		 */

		str = new String(str.getBytes("KSC5601"), "iso-8859-1");
		//str=new String(str, "ksc5601");

		String[] vals = str.split("\\^");

		for (int i = 0; i < vals.length; i++) {
			System.out.println("===vals[" + i + "] = " + vals[i]);
		}

		//str = new String(str.getBytes("UTF-8"),"ISO8859-1");
		//new String(orgMsg.getBytes("KSC5601"), "ISO8859-1");
		try {
			g4cssCK = new GPKICms(prop, gpki_use).cmsEnc(str);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}
		LOGGER.info("g4cssCK [{}]", g4cssCK);

		// return value
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("g4cssCK", g4cssCK);
		map.put("certInfoSeqNo", certInfoSeqNo);

		return CommonUtils.makeSuccess(map);
	}

	@RequestMapping(value = "/cert/enroll", method = RequestMethod.GET)
	public String certRegForm(HttpServletRequest request, ModelMap model) throws Exception {

		return "usr/login/cert_reg";
	}

	@RequestMapping(value = "/cert/enroll", method = RequestMethod.POST)
	public String certReg(HttpServletRequest request, @ModelAttribute UserForm form, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		try {

			String regNo = "1".equals(form.getCertType()) ? form.getCeoRegNo1() + form.getCeoRegNo2() : form.getCoNo1() + form.getCoNo2() + form.getCoNo3();
			LOGGER.info("reg no [{}]", regNo);

			LoginVO login = new LoginVO();
			login.setRegNo(regNo);
			login.setLoginId(form.getLoginId());
			login.setDelGubun("N");
			System.out.println("LoginID :::" + form.getLoginId());
			System.out.println("DelGubun :::" + form.getLoginId());
			login = userService.getUser(login);
			LOGGER.info("login [{}]", login);

			if (login == null) {
				model.put("message", "회원정보가 존재하지 않습니다.");
				return "comm/error";
			}

			CertVO cert = new CertVO();
			BeanUtils.copyProperties(form, cert);
			cert.setLoginId(login.getLoginId());
			cert.setRegNo(regNo);

			//
			userService.saveCert(cert);

			// noti
			super.noti("12", login);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		//return "redirect:/login2";
		model.put("message", "공인인증서 등록이 완료되었습니다.");
		model.put("url", "/login");
		return "comm/message";
	}

}
