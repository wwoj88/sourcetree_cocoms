package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.user.*;
import cocoms.copyright.web.form.*;

@Controller
@SessionAttributes({ "member" })
public class UserController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** MemberService */
	//	@Resource(name = "memberService")
	//	private MemberService memberService;

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	/** member form */
	@ModelAttribute("member")
	public UserForm createUserForm() {
		return new UserForm();
	}

	/**
	 * 등록폼
	 */
	@RequestMapping(value = "/user/new", method = RequestMethod.GET)
	public String regForm(HttpServletRequest request, ModelMap model) throws Exception {
		LOGGER.info("step is 0");

		return "usr/user/user_reg00";
	}

	@RequestMapping(value = "/user/new", method = RequestMethod.POST)
	public String regForm2(HttpServletRequest request, @ModelAttribute("member") UserForm form, @RequestParam(value = "step", defaultValue = "0", required = false) String step, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		LOGGER.info("step [{}]", step);

		/*
		 * // default value if (StringUtils.isEmpty(form.getCoGubunCd())) {
		 * form.setCoGubunCd("2"); }
		 */
		String bubNo  = form.getBubNo();
		String cogubuncd = form.getCoGubunCd();
		String regNo = form.getRegNo();
		if ("0".equals(step)) {
			return "usr/user/user_reg00";
		} else if ("1".equals(step)) {

			if (cogubuncd.equals("") || cogubuncd == null) {
				form.setCoNo1("");
				form.setCoNo2("");
				form.setCoNo3("");
				form.setCoName("");
				form.setCeoName("");
				form.setCeoRegNo1("");
				form.setCeoRegNo2("");
				form.setUserDn("");
				form.setCoName("");
				form.setCoGubunCd("");

				HttpSession session = request.getSession();
				session.invalidate();
				session = request.getSession(true);
				return "redirect:/main";
			}
	/*		if(bubNo =="2"|| bubNo.equals("2")){
				HttpSession session = request.getSession();
				session.invalidate();
				session = request.getSession(true);
				session.setAttribute("SSES_COCD", cogubuncd);
				return "usr/user/user_regBub";
			}*/
			HttpSession session = request.getSession();
			session.invalidate();
			session = request.getSession(true);
			session.setAttribute("SSES_COCD", cogubuncd);
			return "usr/user/user_reg01";

		} else if ("2".equals(step)) {

			HttpSession session = request.getSession();
			String cocheck = (String) session.getAttribute("SSES_COCD");
			String regNocheck = (String) session.getAttribute("SSES_REG");
			String coNochekc =  form.getCoNo1()+form.getCoNo2()+form.getCoNo3();
			String regCheck = form.getCeoRegNo1()+form.getCeoRegNo2();
			if(cocheck.equals("2")){

				if(!regNocheck.equals(coNochekc)){
					form.setCoNo1("");
					form.setCoNo2("");
					form.setCoNo3("");
					form.setCoName("");
					form.setCeoName("");
					form.setCeoRegNo1("");
					form.setCeoRegNo2("");
					form.setUserDn("");
					form.setCoName("");
					form.setCoGubunCd("");
					session.invalidate();
					session = request.getSession(true);

					return "redirect:/main";
				}
			}else if(cocheck.equals("1")){

				if(!regNocheck.equals(regCheck)){
					form.setCoNo1("");
					form.setCoNo2("");
					form.setCoNo3("");
					form.setCoName("");
					form.setCeoName("");
					form.setCeoRegNo1("");
					form.setCeoRegNo2("");
					form.setUserDn("");
					form.setCoName("");
					form.setCoGubunCd("");
					session.invalidate();
					session = request.getSession(true);

					return "redirect:/main";
				}
			}
		
			
			if (!cocheck.equals(form.getCoGubunCd()) || cocheck.equals("")) {
				form.setCoNo1("");
				form.setCoNo2("");
				form.setCoNo3("");
				form.setCoName("");
				form.setCeoName("");
				form.setCeoRegNo1("");
				form.setCeoRegNo2("");
				form.setUserDn("");
				form.setCoName("");
				form.setCoGubunCd("");
				session.invalidate();
				session = request.getSession(true);

				return "redirect:/main";
			} else {
				if (form.getCoGubunCd().equals("1") && !form.getCoName().equals("")) {
					form.setCoNo1("");
					form.setCoNo2("");
					form.setCoNo3("");
					form.setCoName("");
					form.setCeoName("");
					form.setCeoRegNo1("");
					form.setCeoRegNo2("");
					form.setUserDn("");
					form.setCoName("");
					form.setCoGubunCd("");

					session.invalidate();
					session = request.getSession(true);
					return "redirect:/main";
				} else if (form.getCoGubunCd().equals("2") && !form.getCeoName().equals("")) {
					form.setCoNo1("");
					form.setCoNo2("");
					form.setCoNo3("");
					form.setCoName("");
					form.setCeoName("");
					form.setCeoRegNo1("");
					form.setCeoRegNo2("");
					form.setUserDn("");
					form.setCoName("");
					form.setCoGubunCd("");
					session.invalidate();
					session = request.getSession(true);
					return "redirect:/main";

				}
			}

			String ceoRegNo = form.getCeoRegNo1() + form.getCeoRegNo2();
			ceoRegNo = XCrypto.encPatternReg(ceoRegNo);
			form.setCeoRegNo(ceoRegNo);

			String coNo = form.getCoNo1() + form.getCoNo2() + form.getCoNo3();
			//			coNo = XCrypto.encPatternReg(coNo);
			form.setCoNo(coNo);

			return "usr/user/user_reg02";
		}

		else if ("4".equals(step)) {
			return "usr/user/user_reg04";
		}

		return "usr/user/user_reg01";
	}

	/**
	 * 기가입체크
	 */
	@RequestMapping(value = "/user", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getMemberSeqNo(HttpServletRequest request, MemberVO vo, ModelMap model) throws Exception {
		LOGGER.info("member [{}]", vo);

		String memberSeqNo = "";

		try {
			memberSeqNo = userService.getMemberSeqNo(vo);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("memberSeqNo", memberSeqNo);

		return CommonUtils.makeSuccess(map);
	}

	/**
	 * 기가입 아이디 갯수
	 */
	@RequestMapping(value = "/userid", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getMemberSeqNo2(HttpServletRequest request, LoginVO vo, ModelMap model) throws Exception {
		LOGGER.info("member [{}]", vo);

		int total = 0;

		try {

			Map<String, Object> map = userService.getLogins(vo.getRegNo());
			List<LoginVO> list = (List<LoginVO>) map.get("list");

			if (list != null) {
				total = list.size();
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		HttpSession session = request.getSession();
		session.setAttribute("SSES_REG", vo.getRegNo());
		return CommonUtils.makeSuccess(map);
	}

	/**
	 * 아이디 중복체크
	 */
	@RequestMapping(value = "/user/{key}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String checkId(HttpServletRequest request, @PathVariable String key, ModelMap model) throws Exception {
		LOGGER.info("key [{}]", key);

		boolean exists = true;

		try {
			exists = userService.existsId(key);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("loginId", key);
		map.put("exists", exists);

		String str = new ObjectMapper().writeValueAsString(map);
		LOGGER.info("str [" + str + "]");

		return CommonUtils.makeSuccess(map);
	}

	/**
	 * 가입처리
	 */
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String register(HttpServletRequest request, @ModelAttribute("member") UserForm form, BindingResult bindingResult, SessionStatus status, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		// Server-Side Validation
			//	beanValidator.validate(userVO, bindingResult);

		//		if (bindingResult.hasErrors()) {
		//			model.addAttribute("userVO", userVO);
		//			return "usr/user/user_reg04";
		//		}
		HttpSession session = request.getSession();
		String cocheck = (String) session.getAttribute("SSES_COCD");
		String regNocheck = (String) session.getAttribute("SSES_REG");
		String coNochekc =  form.getCoNo1()+form.getCoNo2()+form.getCoNo3();
		String regCheck = form.getCeoRegNo1()+form.getCeoRegNo2();
		if(cocheck.equals("2")){
			if(!regNocheck.equals(coNochekc)){
				form.setCoNo1("");
				form.setCoNo2("");
				form.setCoNo3("");
				form.setCoName("");
				form.setCeoName("");
				form.setCeoRegNo1("");
				form.setCeoRegNo2("");
				form.setUserDn("");
				form.setCoName("");
				form.setCoGubunCd("");
				session.invalidate();
				session = request.getSession(true);

				return "redirect:/main";
			}
		}else if(cocheck.equals("1")){
			if(!regNocheck.equals(regCheck)){
				form.setCoNo1("");
				form.setCoNo2("");
				form.setCoNo3("");
				form.setCoName("");
				form.setCeoName("");
				form.setCeoRegNo1("");
				form.setCeoRegNo2("");
				form.setUserDn("");
				form.setCoName("");
				form.setCoGubunCd("");
				session.invalidate();
				session = request.getSession(true);

				return "redirect:/main";
			}
		}
		if (!cocheck.equals(form.getCoGubunCd()) || cocheck.equals("")) {
			
			form.setCoNo1("");
			form.setCoNo2("");
			form.setCoNo3("");
			form.setCoName("");
			form.setCeoName("");
			form.setCeoRegNo1("");
			form.setCeoRegNo2("");
			form.setUserDn("");
			form.setCoName("");
			form.setCoGubunCd("");
			session.invalidate();
			session = request.getSession(true);

			return "redirect:/main";
		} 
		
		
		
		try {

			// set login
			LoginVO login = new LoginVO();
			BeanUtils.copyProperties(form, login);
			login.setCoGubun(form.getCoGubunCd());
			login.setRegNo(form.getCeoRegNo());
			if ("2".equals(form.getCoGubunCd())) {
				login.setRegNo(form.getCoNo());
			}

			// set member
			MemberVO member = new MemberVO();
			BeanUtils.copyProperties(form, member);
			if ("Y".equals(form.getAgree3()) || "Y".equals(form.getSmsAgree())) {
				member.setSms(form.getCeoMobile());
				member.setSmsAgree("Y");
				member.setAgreeDate(DateUtils.getCurrDateNumber());
			} else {
				member.setSmsAgree("N");
				member.setAgreeDate("");
			}

			// set cert
			CertVO cert = new CertVO();
			BeanUtils.copyProperties(form, cert);
			cert.setCertType(form.getCoGubunCd()); // 1: 개인, 2: 법인
			cert.setCertStatus("1"); // 1: 유효
			cert.setRegNo(form.getCeoRegNo());
			if ("2".equals(form.getCoGubunCd())) {
				cert.setRegNo(form.getCoNo());
			}

			// save user
			userService.saveUser(login, member, cert);

			// noti
			super.noti("11", login, member);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		status.setComplete();
	
		return "redirect:/user/result";
	}

	@RequestMapping(value = "/user/result")
	public String result(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		session.invalidate();
		session = request.getSession(true);
		return "usr/user/user_reg05";
	}

}
