package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.application.ChgHistoryMapper;
import cocoms.copyright.application.EnrollmentService;
import cocoms.copyright.application.RpMgmMapper;
import cocoms.copyright.board.BoardService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.entity.*;
import cocoms.copyright.user.UserService;

@Controller
public class MainController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** BoardService */
	@Resource(name = "noticeService")
	private BoardService noticeService;
	
	/** FaqService */
	@Resource(name = "faqService")
	private BoardService faqService;
	
	@Resource(name="chgHistoryMapper")
	private ChgHistoryMapper chgHistoryDAO;
	/** UserService */
	@Resource(name = "userService")
	private UserService userService;
	
	@Resource(name="rpMgmMapper")
	private RpMgmMapper rpMgmDAO;

	/** EnrollmentService */
	@Resource(name = "enrollmentService")
	private EnrollmentService enrollmentService;
	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = {"/","/main"}, method = RequestMethod.GET)
	public String mainPage(HttpServletRequest request,
			@ModelAttribute DefaultVO searchVO, 
			ModelMap model) throws Exception {

		MemberVO member = CommonUtils.getMemberSession(request);
		//LOGGER.info("member [{}]", member);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
//		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setRecordCountPerPage(5);
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		List<BoardVO> noticeList = new ArrayList<BoardVO>();
		List<BoardVO> faqList = new ArrayList<BoardVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = noticeService.getNotices(filter);
			noticeList = (List<BoardVO>) data.get("list");
			
			for (BoardVO item : noticeList) {
				String title = item.getTitle();
				if (title.length() > 25) item.setTitle(title.substring(0, 25) + "...");
			}
			
			Map<String, Object> data2 = faqService.getNotices(filter);
			faqList = (List<BoardVO>) data2.get("list");
			
			for (BoardVO item : faqList) {
				String title = item.getTitle();
				if (title.length() > 30) item.setTitle(title.substring(0, 30) + "...");
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		/*
		 * 
		 */
		boolean popupNotice = false;

		if (member != null && ("7".equals(member.getStatCd()) || "9".equals(member.getStatCd()))) {

			popupNotice = true;
			String url = "/appl/sub/new";
			if ("1".equals(member.getConditionCd())) {
		  		String result="";
            	result = rpMgmDAO.selectrpmgseqno(member.getMemberSeqNo());
            	if(result!=null){
            		url="/appl/sub/edit";
            	}else{
				url = "/appl/sub/new";
            	}
			}
			else if ("2".equals(member.getConditionCd())) {
				String result="";
            	result = rpMgmDAO.selectrpmgseqno(member.getMemberSeqNo());
            	if(result!=null){
            		url="/appl/trust/edit";
            	}else{
				url = "/appl/trust/new";
            	}
			}
		 String Content=enrollmentService.getContent(member.getMemberSeqNo());
			model.put("url", url);
			model.put("Content", Content);
			
			
		}
		
		/*
		 * 
		 */
		boolean popupAgree = false;
		if (member != null 
				&& !"Y".equals(member.getTerms1Agree())) {
			popupAgree = true;
		}
		
		model.put("noticeList", noticeList);
		model.put("faqList", faqList);
		model.put("popupNotice", popupNotice);
		model.put("popupAgree", popupAgree);
		
		

		return "usr/main";
	}
	
	@RequestMapping(value = "/main", method = RequestMethod.POST)
	public String agree(HttpServletRequest request,
			@ModelAttribute DefaultVO searchVO, 
			ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);
		
		String agree = "N";
		
		try {

			userService.saveAgree(memberSeqNo);
			agree = "Y";

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

	
		MemberVO member = CommonUtils.getMemberSession(request);
		member = userService.getMember(memberSeqNo);
		member.setTerms1Agree(agree);
		
	
		HttpSession session = request.getSession();
		session.setAttribute("SESS_MEMBER", member);
		session.setMaxInactiveInterval(12000);
		return "redirect:/main";
	}
	
}
