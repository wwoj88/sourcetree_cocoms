package cocoms.copyright.web.controllers;

import java.util.ArrayList;

import java.util.List;

import java.io.InputStream;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;

import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;

import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class SheetHandler implements XSSFSheetXMLHandler.SheetContentsHandler {
	//header를 제외한 데이터부분
	private List<List<String>> rows = new ArrayList<>();
	//cell 호출시마다 쌓아놓을 1 row List
	private List<String> row = new ArrayList<>();
	//Header 정보를 입력
	private List<String> header = new ArrayList<>();
	//빈 값을 체크하기 위해 사용할 셀번호
	private int currentCol = -1;
	//현재 읽고 있는 Cell의 Col
	private int currRowNum = 0;

	public void startRow(int rowNum) {

		this.currentCol = -1;

		this.currRowNum = rowNum;

	}

	public void endRow(int rowNum) {

		if (rowNum == 0) {

			header = new ArrayList(row);

		} else {

			if (row.size() < header.size()) {

				for (int i = row.size(); i < header.size(); i++) {

					row.add("");

				}

			}

			rows.add(new ArrayList(row));

		}

		row.clear();

	}

	public void cell(String columnName, String value, XSSFComment var3) {

		int iCol = (new CellReference(columnName)).getCol();
		//int iRow = (new CellReference(columnName)).getRow();
		int emptyCol = iCol - currentCol - 1;

		for (int i = 0; i < emptyCol; i++) {

			row.add("");

		}

		currentCol = iCol;

		row.add(value);

	}

	public List<List<String>> getRows() {
		return rows;
	}

	public List<String> getHeader() {
		return row;
	}

	public void headerFooter(String text, boolean isHeader, String tagName) {

	}

	public static SheetHandler readExcel(MultipartFile multiPart) throws Exception {

		SheetHandler sheetHandler = new SheetHandler();

		try {
			
			OPCPackage pkg = OPCPackage.open(multiPart.getInputStream());

			XSSFReader xssfReader = new XSSFReader(pkg);

			ReadOnlySharedStringsTable data = new ReadOnlySharedStringsTable(pkg);

			StylesTable styles = xssfReader.getStylesTable();

			InputStream sheetStream = xssfReader.getSheetsData().next();

			InputSource sheetSource = new InputSource(sheetStream);

			ContentHandler handler = new XSSFSheetXMLHandler(styles, data, sheetHandler, false);

			XMLReader sheetParser = SAXHelper.newXMLReader();

			sheetParser.setContentHandler(handler);

			sheetParser.parse(sheetSource);
			sheetStream.close();

			List ms = new ArrayList();
			listToList(sheetHandler.getRows(), ms);

		} catch (RuntimeException e) {

			throw new RuntimeException(e);

		}

		return sheetHandler;

	}

	public static List listToList(List fromList, List toList) {

		for (int i = 0; i < fromList.size(); i++) {

			toList.add(fromList.get(i));
		}

		return toList;
	}

}