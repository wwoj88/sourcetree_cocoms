package cocoms.copyright.web.controllers;


import java.text.SimpleDateFormat;
import java.util.*;

import javax.annotation.Resource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.application.ExcelReportService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.ExcelParser;
import cocoms.copyright.common.DateUtils;
import egovframework.rte.fdl.property.EgovPropertyService;
import cocoms.copyright.web.controllers.SheetHandler;

@Controller
public class UploadController extends UploadCommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	private String ALLOW_FILE_EXT = "pdf,jpg,jpeg";

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "applicationService")
	private ApplicationService applicationService;

	@Resource(name = "excelReportService")
	private ExcelReportService excelReportService;

	@RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String upload(MultipartHttpServletRequest request, @RequestParam(value = "name", required = false, defaultValue = "") String name) throws Exception {
		LOGGER.info("call upload [{}]", name);

		ALLOW_FILE_EXT = "pdf,jpg,jpeg,zip";

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.put("files", saveFiles(request, name));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}

		return CommonUtils.makeSuccess(map);
	}

	//application/json
	@RequestMapping(value = "/upload2", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String upload2(MultipartHttpServletRequest request, @RequestParam(value = "name", required = false, defaultValue = "") String name) throws Exception {
		LOGGER.info("call upload [{}]", name);

		ALLOW_FILE_EXT = "pptx,pdf,jpg,jpeg,hwp,doc,xls,xlsx,mp4,wmv,mp3,wav,zip";

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.put("files", saveFiles(request, name));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}

		return CommonUtils.makeSuccess(map);
	}

	//application/json
	@RequestMapping(value = "/upload3", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String upload3(MultipartHttpServletRequest request, @RequestParam(value = "name", required = false, defaultValue = "") String name) throws Exception {
		LOGGER.info("call upload [{}]", name);

		ALLOW_FILE_EXT = "pptx,pdf,jpg,jpeg,hwp,doc,xls,xlsx";

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.put("files", saveFiles(request, name));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//				throw e;
			return CommonUtils.makeError(e.getMessage());
		}

		return CommonUtils.makeSuccess(map);
	}

	//application/json
	@RequestMapping(value = "/upload4", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String upload4(MultipartHttpServletRequest request, @RequestParam(value = "gercd", required = false, defaultValue = "") String name) throws Exception {

		String gercd = request.getParameter("gercd");
	
		int header = 0;
		int header2 = 0;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Iterator<String> it = request.getFileNames();
			MultipartFile mpf = null;

			while (it.hasNext()) {
				mpf = request.getFile(it.next());
			}

			SheetHandler excelSheetHandler = SheetHandler.readExcel(mpf);

			if (gercd.equals("1")) {
				header = 11;
				header2 = 22;
			} else if (gercd.equals("2")) {
				header = 2;
				header2 = 11;
			} else if (gercd.equals("3")) {
				header = 2;
				header2 = 15;
			} else if (gercd.equals("4")) {
				header = 2;
				header2 = 13;
			} else if (gercd.equals("5")) {
				header = 2;
				header2 = 13;
			} else if (gercd.equals("6")) {
				header = 2;
				header2 = 12;
			} else if (gercd.equals("7")) {
				header = 2;
				header2 = 18;
			} else if (gercd.equals("8")) {
				header = 2;
				header2 = 13;
			} else if (gercd.equals("99")) {
				header = 2;
				header2 = 10;
			}

			List ms = new ArrayList();
			listToList(excelSheetHandler.getRows(), ms);
			List<List<String>> excelDatas = ms;

			try {

				for (int i = 0; i < header; i++) {
					excelDatas.remove(0);
				}

			} catch (Exception e) {
				e.printStackTrace();
				return CommonUtils.makeError("장르와 다른 엑셀을 업로드하였습니다. 오래된 양식일 경우 새 양식파일을 받아 업로드해주세요. ");
			}

			long time2 = System.currentTimeMillis();

			SimpleDateFormat dayTime2 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

			uploadList = ExcelParser.parse(excelDatas, header2, gercd, request);

			if (uploadList == null || uploadList.size() == 0) {

				return CommonUtils.makeError("데이터 필수 값중 빈값이 존재합니다.  확인후 다시 업로드해주세요 \n( 빈칸 행이 존재하지않도록 작성해주세요 )");
			}

			String st2r = dayTime2.format(new Date(time2));

			Map<String, Object> data = excelReportService.insertnew(uploadList);
			boolean isSuccess = (boolean) data.get("isSuccess");
			int overlap = (int) data.get("overlap");

			if (isSuccess == false) {

				return CommonUtils.makeError("이미 업로드한 데이터 또는 장르가 다른 데이터입니다.  \n수정시에는 일괄수정을 사용해주세요.\n( 금월내 동일종류 보고이력 존재 )");

			}

			map.put("success", isSuccess);
			if (overlap == 0) {
				map.put("errorMessage", "보고 완료");
			} else {
				map.put("errorMessage", "중복데이터 " + overlap + "건 제외 보고 완료");
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//				throw e;

			return CommonUtils.makeError(e.getMessage());
		}

		return CommonUtils.makeSuccess(map);
	}
	
	

	//application/json
	@RequestMapping(value = "/upload5", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String upload5(MultipartHttpServletRequest request, @RequestParam(value = "gercd", required = false, defaultValue = "") String name) throws Exception {

		String gercd = request.getParameter("gercd");

		int header = 0;
		int header2 = 0;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Iterator<String> it = request.getFileNames();
			MultipartFile mpf = null;

			while (it.hasNext()) {
				mpf = request.getFile(it.next());
			}

			SheetHandler excelSheetHandler = SheetHandler.readExcel(mpf);

			if (gercd.equals("1")) {
				header = 11;
				header2 = 22;
			} else if (gercd.equals("2")) {
				header = 2;
				header2 = 11;
			} else if (gercd.equals("3")) {
				header = 2;
				header2 = 15;
			} else if (gercd.equals("4")) {
				header = 2;
				header2 = 13;
			} else if (gercd.equals("5")) {
				header = 2;
				header2 = 13;
			} else if (gercd.equals("6")) {
				header = 2;
				header2 = 12;
			} else if (gercd.equals("7")) {
				header = 2;
				header2 = 18;
			} else if (gercd.equals("8")) {
				header = 2;
				header2 = 13;
			} else if (gercd.equals("99")) {
				header = 2;
				header2 = 10;
			}

			List ms = new ArrayList();
			listToList(excelSheetHandler.getRows(), ms);
			List<List<String>> excelDatas = ms;

			try {

				for (int i = 0; i < header; i++) {
					excelDatas.remove(0);
				}

			} catch (Exception e) {
				e.printStackTrace();
				return CommonUtils.makeError("장르와 다른 엑셀을 업로드하였습니다. 오래된 양식일 경우 새 양식파일을 받아 업로드해주세요. ");
			}

			long time2 = System.currentTimeMillis();

			SimpleDateFormat dayTime2 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();

			uploadList = ExcelParser.parse(excelDatas, header2, gercd, request);

			if (uploadList == null || uploadList.size() == 0) {

				return CommonUtils.makeError("데이터 필수 값중 빈값이 존재합니다.  확인후 다시 업로드해주세요 \n( 빈칸 행이 존재하지않도록 작성해주세요 )");
			}

			String st2r = dayTime2.format(new Date(time2));

			Map<String, Object> data = excelReportService.modiReport(uploadList);
			boolean isSuccess = (boolean) data.get("isSuccess");
			int overlap = (int) data.get("overlap");

			if (isSuccess == false) {

				return CommonUtils.makeError("해당 월에 보고한 내역이 없습니다. \n 일괄등록으로 등록해주세요.");

			}

			map.put("success", isSuccess);
			if (overlap == 0) {
				map.put("errorMessage", "보고 완료");
			} else {
				map.put("errorMessage", "중복데이터 " + overlap + "건 제외 보고 완료");
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//				throw e;

			return CommonUtils.makeError(e.getMessage());
		}

		return CommonUtils.makeSuccess(map);
	}

	@Override
	protected String getRootPath() {

		return propertiesService.getString("filepath");
	}

	@Override
	protected String getSubPath() {

		return "/" + DateUtils.getCurrDateNumber().substring(0, 4) + "/" + DateUtils.getCurrDateNumber().substring(4, 6);
	}

	@Override
	protected String getFilename(String ext) {

		return DateUtils.getCurrDateNumber() + System.currentTimeMillis() + "." + ext;
	}

	@Override
	protected String getAllowExt() {

		return ALLOW_FILE_EXT;
	}

	public static List listToList(List fromList, List toList) {

		for (int i = 0; i < fromList.size(); i++) {

			toList.add(fromList.get(i));
		}

		return toList;
	}
}
