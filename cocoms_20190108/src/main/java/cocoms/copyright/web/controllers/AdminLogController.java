package cocoms.copyright.web.controllers;

import java.net.URLEncoder;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.report.ReportService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin")
public class AdminLogController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** ActionLogService */
	@Resource(name = "actionLogService")
	protected ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/logs", method = RequestMethod.GET)
	public String list(HttpServletRequest request,
			@ModelAttribute ReportInfoVO searchVO,
			@RequestParam(value="sort", defaultValue="") String sort,
			ModelMap model) throws Exception {
		System.out.println(XCrypto.encPatternReg("5610251011012"));
		System.out.println(XCrypto.encPatternReg("5610251011012"));
		System.out.println(XCrypto.encPatternReg("5610251011012"));
		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<ActionLogVO> list = new ArrayList<ActionLogVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = actionLogService.getLogs(filter);
			list = (List<ActionLogVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		for (ActionLogVO item : list) {
			String type = item.getUrl().indexOf("/admin") > -1 ? "admin" : "user";

			Map<String, Object> map = CommonUtils.pojoToMap(item);
			map.put("type", type);
			
			list2.add(map);
		}

		model.put("data", searchVO);
		model.put("list", list2);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo()-1) * paginationInfo.getRecordCountPerPage());

		return "adm/log/log_list";
	}
	
	@RequestMapping(value = "/logs/excel", method = RequestMethod.GET)
	public void excel(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute ReportInfoVO searchVO,
			@RequestParam(value="sort", defaultValue="") String sort,
			ModelMap model) throws Exception {

		List<ActionLogVO> list = new ArrayList<ActionLogVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);

			Map<String, Object> data = actionLogService.getLogs(filter);
			list = (List<ActionLogVO>) data.get("list");
			int total = (int) data.get("total");

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		String filename = DateUtils.getCurDateTime()+".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");

			list2.add(col);
		}
		int cnt = 0;
		for (ActionLogVO item : list) {
			String no = String.valueOf(list.size() - cnt);
			
			List<String> col = new ArrayList<String>();
			col.add(no);				// 번호    
			
			list2.add(col);
			cnt++;
		}
		
		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}
	
}
