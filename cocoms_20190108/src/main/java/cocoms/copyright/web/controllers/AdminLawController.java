package cocoms.copyright.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.board.BoardService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.entity.BoardFileDataVO;
import cocoms.copyright.entity.BoardVO;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.web.form.BoardForm;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
@RequestMapping("/admin")
public class AdminLawController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** BoardService */
	@Resource(name = "lawService")
	private BoardService lawService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = "/law", method = RequestMethod.GET)
	public String row(HttpServletRequest request, @ModelAttribute BoardVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("row list vo [{}]", searchVO);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<BoardVO> list = new ArrayList<BoardVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = lawService.getNotices(filter);
			list = (List<BoardVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount()
				- (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "adm/law/law_list";
	}

	@RequestMapping(value = "/law/{boardSeqNo}", method = RequestMethod.GET)
	public String rowDetail(HttpServletRequest request, @ModelAttribute BoardVO searchVO,
			@PathVariable String boardSeqNo, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		BoardVO board = new BoardVO();
		try {

			board = lawService.getNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("data", board);

		return "adm/law/law_view";
	}

	@RequestMapping(value = "/law_new", method = RequestMethod.GET)
	public String row_new(HttpServletRequest request, @ModelAttribute BoardVO searchVO, ModelMap model)
			throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		model.put("search", searchVO);

		return "adm/law/law_write";
	}

	@RequestMapping(value = "/law", method = RequestMethod.POST)
	public String rowWrite(HttpServletRequest request, @ModelAttribute BoardForm form, ModelMap model)
			throws Exception {
		LOGGER.info("form [{}]", form);

		form.setBoardSeqNo("");
		form.setRegId("Admin"); // TODO

		List<BoardFileDataVO> boardFileDataList = new ArrayList<BoardFileDataVO>();
		for (int i = 0; i < form.getFilename().size(); i++) {
			BoardFileDataVO file = new BoardFileDataVO();
			String filename = form.getFilename().get(i);
			String maskName = form.getMaskName().get(i);
			String filesize = form.getFilesize().get(i);

			file.setFilename(filename);
			file.setMaskName(maskName);
			file.setFilesize(filesize);
			file.setRegId(form.getRegId());

			boardFileDataList.add(file);
		}
		form.setBoardFileDataList(boardFileDataList);

		try {

			lawService.saveNotice(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", form);

		return "redirect:/admin/law";
	}

	@RequestMapping(value = "/law/{boardSeqNo}/edit", method = RequestMethod.GET)
	public String rowModifyForm(HttpServletRequest request, @ModelAttribute BoardVO searchVO,
			@PathVariable String boardSeqNo, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		BoardVO board = new BoardVO();
		try {

			board = lawService.getNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("data", board);

		return "adm/law/law_write";
	}

	@RequestMapping(value = "/law/{boardSeqNo}", method = RequestMethod.POST)
	public String rowModify(HttpServletRequest request, @ModelAttribute BoardForm form, @PathVariable String boardSeqNo,
			@RequestParam(value = "_method", defaultValue = "") String method,

			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		if ("DELETE".equals(method.toUpperCase())) {

			// save log
			actionLogService.saveLog(request, "remove law");

			return rowRemove(boardSeqNo);
		}

		form.setBoardSeqNo(boardSeqNo);
		form.setRegId("Admin"); // TODO

		List<BoardFileDataVO> boardFileDataList = new ArrayList<BoardFileDataVO>();
		for (int i = 0; i < form.getFilename().size(); i++) {
			BoardFileDataVO file = new BoardFileDataVO();
			String filename = form.getFilename().get(i);
			String maskName = form.getMaskName().get(i);
			String filesize = form.getFilesize().get(i);

			file.setFilename(filename);
			file.setMaskName(maskName);
			file.setFilesize(filesize);
			file.setRegId(form.getRegId());

			boardFileDataList.add(file);
		}
		form.setBoardFileDataList(boardFileDataList);

		try {

			lawService.saveNotice(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", form);

		return "redirect:/admin/law/" + boardSeqNo;
	}

	private String rowRemove(String boardSeqNo) throws Exception {
		LOGGER.info("board seq no [{}]", boardSeqNo);

		try {

			lawService.removeNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		return "redirect:/admin/law";
	}

}
