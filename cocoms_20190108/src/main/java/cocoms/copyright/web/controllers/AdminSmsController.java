package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.board.BoardService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.sms.SmsService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin")
public class AdminSmsController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** SmsService */
	@Resource(name = "smsService")
	private SmsService smsService;
	
	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/sms/send", method = RequestMethod.GET)
	public String smsForm(HttpServletRequest request,
			@ModelAttribute UserForm searchVO,
			
			@RequestParam(value="type", defaultValue="") String type,
			ModelMap model) throws Exception {
		LOGGER.info("faq list vo [{}]", searchVO);
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = smsMemberListSub(searchVO, type);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}

		model.putAll(map);

		if ("email".equals(type)) {
			
			return "adm/sms/email_form";
		}

		return "adm/sms/sms_form";
	}
	
	@RequestMapping(value = "/sms/members", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String smsMemberList(HttpServletRequest request,
			@ModelAttribute UserForm searchVO,
			
			@RequestParam(value="type", defaultValue="") String type,
			ModelMap model) throws Exception {
		LOGGER.info("sms members form [{}]", searchVO);
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = smsMemberListSub(searchVO, type);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}
		
		return CommonUtils.makeSuccess(map);
	}
	
	private Map<String, Object> smsMemberListSub(UserForm searchVO, String type) throws Exception {
		LOGGER.info("sms members form [{}]", searchVO);

		// default value
		if (StringUtils.isEmpty(searchVO.getSmsAgree())) {
			searchVO.setSmsAgree("Y");
		}
		
		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		List<MemberVO> list = new ArrayList<MemberVO>();
		List<MemberVO> allList = new ArrayList<MemberVO>();
		try {
			
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			if ("4".equals(searchVO.getConDetailCd())) {
				filter.put("conditionCd", "2");
				filter.put("conDetailCd", "");
			}
			else if ("1".equals(searchVO.getConDetailCd())
					|| "2".equals(searchVO.getConDetailCd())
					|| "3".equals(searchVO.getConDetailCd())) {
				filter.put("conditionCd", "1");
			}

			if ("Y".equals(searchVO.getSmsAgree()) && !"email".equals(type)) {
				filter.put("NOT_NULL_SMS", "Y");
			}
			if ("Y".equals(searchVO.getSmsAgree()) && "email".equals(type)) {
				filter.put("NOT_NULL_CEOEMAIL", "Y");
			}

			Map<String, Object> data = userService.getMembers(filter);
			list = (List<MemberVO>) data.get("list");			
			int total = (int) data.get("total");

			// all list
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);
			Map<String, Object> allData = userService.getMembers(filter);
			allList = (List<MemberVO>) allData.get("list");	
			LOGGER.info("members count [{}] [{}]", allData.get("total"), allList.size());

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchVO);
		map.put("list", list);
		map.put("allList", allList);
		map.put("page", paginationInfo.getCurrentPageNo());
		map.put("total", paginationInfo.getTotalRecordCount());
		map.put("paginationInfo", paginationInfo);
		
		return map;
	}
	
	@RequestMapping(value = "/sms", method = RequestMethod.POST)
	public String smsSend(HttpServletRequest request,
			@ModelAttribute SmsForm form,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		String smsType = "1"; // 1: sms, 2: email
		String loginId = "admin"; 	// TODO session data
		
		try {

			send(smsType, loginId, form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return "redirect:/admin/sms/send";
	}
	
	@RequestMapping(value = "/email", method = RequestMethod.POST)
	public String emailSend(HttpServletRequest request,
			@ModelAttribute SmsForm form,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		String smsType = "2"; // 1: sms, 2: email
		String loginId = "admin"; 	// TODO session data
		
		try {

			send(smsType, loginId, form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return "redirect:/admin/sms/send?type=email";
	}
	
	@RequestMapping(value = "/sms.json", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String smsSend2(HttpServletRequest request,
			@ModelAttribute SmsForm form,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		String smsType = "1"; // 1: sms, 2: email
		String loginId = CommonUtils.getAdminLoginId(request);
		
		try {

			send(smsType, loginId, form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}
		
		return CommonUtils.makeSuccess();
	}
	
	@RequestMapping(value = "/email.json", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String emailSend2(HttpServletRequest request,
			@ModelAttribute SmsForm form,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		String smsType = "2"; // 1: sms, 2: email
		String loginId = "admin"; 	// TODO session data
		
		try {

			send(smsType, loginId, form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}
		
		return CommonUtils.makeSuccess();
	}
	
	private void send(String smsType, String loginId, SmsForm form) throws Exception {
		LOGGER.info("form [{}]", form);
		
		try {

			LOGGER.info("sms member list count [{}]", form.getMemberSeqNo().size());

			if (StringUtils.isNotEmpty(form.getSendDate())) {
				String sendDate = form.getSendDate().replaceAll("-", "");
				form.setSendDate(sendDate);
			}
			else {
				form.setSendDate(DateUtils.getCurrDateNumber());
			}
			
			if (StringUtils.isNotEmpty(form.getSendTime1())
					&& StringUtils.isNotEmpty(form.getSendTime2())) {
				String sendTime = form.getSendTime1() + form.getSendTime2() + "00";
				form.setSendTime(sendTime);
			}
			else {
				form.setSendTime(DateUtils.getCurrTimeNumber());
			}
			
			if ("1".equals(form.getSendType())) {
				form.setSendYn("Y");
				
				if ("1".equals(smsType)) {
					// send sms
					String smsSender = propertiesService.getString("smsSender");
					for (String receiver : form.getReceiver()) {
						smsService.sendSms(receiver, form.getContent(), smsSender);
					}
				}
				else {
					// send mail
					String host = propertiesService.getString("smtpHost");
					String from = propertiesService.getString("smtpEmail");
					String fromName = propertiesService.getString("smtpSender");
					for (String receiver : form.getReceiver()) {
						smsService.sendMail(receiver, "", form.getSubject(), form.getContent(),
								host, from, fromName);
					}
				}
			}
			else {
				form.setSendYn("N");
				
				if ("1".equals(smsType)) {
					// send sms
					String smsSender = propertiesService.getString("smsSender");
					String date = "";	// TODO
					for (String receiver : form.getReceiver()) {
						smsService.sendSms(receiver, form.getSubject(), smsSender, date);
					}
				}
				else {
					// TODO send mail
					//String host = propertiesService.getString("smtpHost");
					//String from = propertiesService.getString("smtpEmail");
					//String fromName = propertiesService.getString("smtpSender");
					//for (String receiver : form.getReceiver()) {
					//	smsService.sendMail(receiver, "", form.getSubject(), form.getContent(),
					//			host, from, fromName);
					//}
				}
			}
			
			form.setSmsType(smsType);
			form.setRegId(loginId);
			
			List<SmsMemberVO> smsMemberList = new ArrayList<SmsMemberVO>(); 
			for (int i=0; i<form.getMemberSeqNo().size(); i++) {
				String memberSeqNo = form.getMemberSeqNo().get(i);
				String receiver = form.getReceiver().get(i);
				
				SmsMemberVO smsMember = new SmsMemberVO();
				smsMember.setMemberSeqNo(memberSeqNo);
				smsMember.setReceiver(receiver);
				
				smsMemberList.add(smsMember);
			}
			form.setSmsMemberList(smsMemberList);
			
			smsService.saveSmsHistory(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
	}
	
	@RequestMapping(value = "/sms/setup", method = RequestMethod.GET)
	public String smsList(HttpServletRequest request,
			ModelMap model) throws Exception {
		LOGGER.info("sms setup");
		
		String smsType = "1"; // 1: sms, 2: email

		List<SmsTemplateVO> list = new ArrayList<SmsTemplateVO>();
		List<SmsTemplateVO> list2 = new ArrayList<SmsTemplateVO>();
		List<SmsTemplateVO> list3 = new ArrayList<SmsTemplateVO>();
		List<SmsTemplateVO> list4 = new ArrayList<SmsTemplateVO>();
		List<SmsTemplateVO> list5 = new ArrayList<SmsTemplateVO>();
		List<SmsTemplateVO> list6 = new ArrayList<SmsTemplateVO>();
		List<SmsTemplateVO> list7 = new ArrayList<SmsTemplateVO>();
		try {

			Map<String, Object> filter = new HashMap<String, Object>();
			filter.put("smsType", smsType);
			
			filter.put("mainCode", "1");
			list = smsService.getSmsTemplates(filter);
			
			filter.put("mainCode", "2");
			list2 = smsService.getSmsTemplates(filter);
			
			filter.put("mainCode", "3");
			list3 = smsService.getSmsTemplates(filter);

			filter.put("mainCode", "4");
			list4 = smsService.getSmsTemplates(filter);

			filter.put("mainCode", "5");
			list5 = smsService.getSmsTemplates(filter);

			filter.put("mainCode", "6");
			list6 = smsService.getSmsTemplates(filter);

			filter.put("mainCode", "7");
			list7 = smsService.getSmsTemplates(filter);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("list", list);
		model.put("list2", list2);
		model.put("list3", list3);
		model.put("list4", list4);
		model.put("list5", list5);
		model.put("list6", list6);
		model.put("list7", list7);

		return "adm/sms/sms_setup";
	}
	
	@RequestMapping(value = "/sms/all", method = RequestMethod.POST)
	public String smsSave(HttpServletRequest request,
			@ModelAttribute SmsForm form,
			ModelMap model) throws Exception {
		LOGGER.info("sms save form [{}]", form);
		
		String smsType = "1"; // 1: sms, 2: email

		List<SmsTemplateVO> list = new ArrayList<SmsTemplateVO>();
		for (int i=0; i<form.getContents().size(); i++) {
			String detailCode = form.getDetailCodes().get(i);
			String content = form.getContents().get(i);

			SmsTemplateVO vo = new SmsTemplateVO();
			vo.setDetailCode(detailCode);
			vo.setContent(content);
			
			if ("11".equals(detailCode)) vo.setUseYn(form.getUseYn_11());
			if ("12".equals(detailCode)) vo.setUseYn(form.getUseYn_12());
			if ("13".equals(detailCode)) vo.setUseYn(form.getUseYn_13());

			if ("21".equals(detailCode)) vo.setUseYn(form.getUseYn_21());
			if ("22".equals(detailCode)) vo.setUseYn(form.getUseYn_22());
			if ("23".equals(detailCode)) vo.setUseYn(form.getUseYn_23());
			if ("24".equals(detailCode)) vo.setUseYn(form.getUseYn_24());
			if ("25".equals(detailCode)) vo.setUseYn(form.getUseYn_25());
			if ("26".equals(detailCode)) vo.setUseYn(form.getUseYn_26());
			if ("27".equals(detailCode)) vo.setUseYn(form.getUseYn_27());
			if ("28".equals(detailCode)) vo.setUseYn(form.getUseYn_28());

			if ("31".equals(detailCode)) vo.setUseYn(form.getUseYn_31());
			if ("32".equals(detailCode)) vo.setUseYn(form.getUseYn_32());
			if ("33".equals(detailCode)) vo.setUseYn(form.getUseYn_33());

			list.add(vo);
		}
		
		try {

			for (SmsTemplateVO item : list) {
				item.setSmsType(smsType);
				LOGGER.info("sms [{}]", item);

				smsService.saveSmsTemplate(item);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return "redirect:/admin/sms/setup";
	}
	
	@RequestMapping(value = "/email/setup", method = RequestMethod.GET)
	public String emailList(HttpServletRequest request,
			ModelMap model) throws Exception {
		LOGGER.info("start setup");
		
		String smsType = "2"; // 1: sms, 2: email

		List<SmsTemplateVO> list = new ArrayList<SmsTemplateVO>();
		try {

			Map<String, Object> filter = new HashMap<String, Object>();
			filter.put("smsType", smsType);
			
			list = smsService.getSmsTemplates(filter);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("list", list);
		
		return "adm/sms/email_setup";
	}
	
	@RequestMapping(value = "/email/{key}", method = RequestMethod.POST)
	public String emailSave(HttpServletRequest request,
			@ModelAttribute SmsForm form,
			@PathVariable String key,
			ModelMap model) throws Exception {
		LOGGER.info("key [{}]", key);
		
		String smsType = "2"; // 1: sms, 2: email

		try {

			SmsTemplateVO smsTemplate = new SmsTemplateVO();
			BeanUtils.copyProperties(form, smsTemplate);
			smsTemplate.setDetailCode(key);
			smsTemplate.setSmsType(smsType);
			
			smsService.saveSmsTemplate(smsTemplate);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return "redirect:/admin/email/setup";
	}
	
	@RequestMapping(value = "/sms/histories", method = RequestMethod.GET)
	public String smsHistoryList(HttpServletRequest request,
			@ModelAttribute SmsForm searchVO,
			
			@RequestParam(value="type", defaultValue="") String type,
			ModelMap model) throws Exception {
		LOGGER.info("sms form [{}]", searchVO);
		
		String smsType = "1"; // 1: sms, 2: email
		
		searchVO.setSmsType(smsType);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = searchVO.getSearchFromDate().replaceAll("-", "");
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = searchVO.getSearchToDate().replaceAll("-", "");
			searchVO.setSearchToDate(searchKeyword);
		}
		
		List<SmsHistoryVO> list = new ArrayList<SmsHistoryVO>();
		try {

			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("type", type);
			
			Map<String, Object> data = smsService.getSmsHistories(filter);
			list = (List<SmsHistoryVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchFromDate());
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchToDate());
			searchVO.setSearchToDate(searchKeyword);
		}
		
		/** make return value */
		model.put("search", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo()-1) * paginationInfo.getRecordCountPerPage());

		return "adm/sms/sms_list";
	}
	
	@RequestMapping(value = "/sms/histories/{key}", method = RequestMethod.GET)
	public String smsHistoryDetail(HttpServletRequest request,
			@ModelAttribute SmsForm searchVO,
			@PathVariable String key,
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		SmsHistoryVO sms = new SmsHistoryVO();
		try {
			
			sms = smsService.getSmsHistory(key);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("data", sms);

		return "adm/sms/sms_view";
	}
	
	@RequestMapping(value = "/sms/histories/{key}", method = RequestMethod.POST)
	public String smsHistoryRemove(HttpServletRequest request,
			@ModelAttribute SmsForm searchVO,
			@PathVariable String key,
			
			@RequestParam(value="type", defaultValue="") String type,

			@RequestParam(value="_method", defaultValue="") String method,
			@RequestParam(value="returnUrl", defaultValue="") String returnUrl,

			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		try {
			
			smsService.removeSmsHistory(key);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		// save log
		actionLogService.saveLog(request, "remove sms history");

		return StringUtils.isEmpty(returnUrl) ? "redirect:/admin/sms/histories?type="+type
				: "redirect:"+returnUrl;
	}
	
	@RequestMapping(value = "/email/histories", method = RequestMethod.GET)
	public String emailHistoryList(HttpServletRequest request,
			@ModelAttribute SmsForm searchVO,
			
			@RequestParam(value="type", defaultValue="") String type,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", searchVO);
		
		String smsType = "2"; // 1: sms, 2: email
		
		searchVO.setSmsType(smsType);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));
		
		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = searchVO.getSearchFromDate().replaceAll("-", "");
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = searchVO.getSearchToDate().replaceAll("-", "");
			searchVO.setSearchToDate(searchKeyword);
		}

		List<SmsHistoryVO> list = new ArrayList<SmsHistoryVO>();
		try {

			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("type", type);
			
			Map<String, Object> data = smsService.getSmsHistories(filter);
			list = (List<SmsHistoryVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchFromDate());
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchToDate());
			searchVO.setSearchToDate(searchKeyword);
		}
		
		/** make return value */
		model.put("search", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo()-1) * paginationInfo.getRecordCountPerPage());

		return "adm/sms/email_list";
	}
	
	@RequestMapping(value = "/email/histories/{key}", method = RequestMethod.GET)
	public String emailHistoryDetail(HttpServletRequest request,
			@ModelAttribute BoardVO searchVO,
			@PathVariable String key,			
			ModelMap model) throws Exception {
		LOGGER.info("faq list vo [{}]", searchVO);
		
		SmsHistoryVO sms = new SmsHistoryVO();
		try {
			
			sms = smsService.getSmsHistory(key);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("data", sms);
		
		return "adm/sms/email_view";
	}
	
	@RequestMapping(value = "/email/histories/{key}", method = RequestMethod.POST)
	public String emailHistoryRemove(HttpServletRequest request,
			@ModelAttribute BoardVO searchVO,
			@PathVariable String key,			
			
			@RequestParam(value="type", defaultValue="") String type,

			@RequestParam(value="_method", defaultValue="") String method,
			@RequestParam(value="returnUrl", defaultValue="") String returnUrl,
			
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		try {
			
			smsService.removeSmsHistory(key);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return StringUtils.isEmpty(returnUrl) ? "redirect:/admin/email/histories?type="+type
				: "redirect:"+returnUrl;
	}
	
}
