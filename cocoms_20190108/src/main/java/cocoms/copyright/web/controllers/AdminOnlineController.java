package cocoms.copyright.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.board.BoardService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.entity.BoardFileDataVO;
import cocoms.copyright.entity.BoardVO;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.web.form.BoardForm;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
@RequestMapping("/admin")
public class AdminOnlineController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** BoardService */
	@Resource(name = "onlineService")
	private BoardService onlineService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = "/online", method = RequestMethod.GET)
	public String online(HttpServletRequest request, @ModelAttribute BoardVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("online list vo [{}]", searchVO);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<BoardVO> list = new ArrayList<BoardVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = onlineService.getNotices(filter);
			list = (List<BoardVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount()
				- (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "adm/online/online_list";
	}

	@RequestMapping(value = "/online/{boardSeqNo}", method = RequestMethod.GET)
	public String onlineDetail(HttpServletRequest request, @ModelAttribute BoardVO searchVO,
			@PathVariable String boardSeqNo, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		BoardVO board = new BoardVO();
		try {

			board = onlineService.getNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("data", board);

		return "adm/online/online_view";
	}

	@RequestMapping(value = "/online_new", method = RequestMethod.GET)
	public String online_new(HttpServletRequest request, @ModelAttribute BoardVO searchVO, ModelMap model)
			throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		model.put("search", searchVO);

		return "adm/online/online_write";
	}

	@RequestMapping(value = "/online", method = RequestMethod.POST)
	public String onlineWrite(HttpServletRequest request, @ModelAttribute BoardForm form, ModelMap model)
			throws Exception {
		LOGGER.info("form [{}]", form);

		form.setBoardSeqNo("");
		form.setRegId("Admin"); // TODO

		List<BoardFileDataVO> boardFileDataList = new ArrayList<BoardFileDataVO>();
		for (int i = 0; i < form.getFilename().size(); i++) {
			BoardFileDataVO file = new BoardFileDataVO();
			String filename = form.getFilename().get(i);
			String maskName = form.getMaskName().get(i);
			String filesize = form.getFilesize().get(i);

			file.setFilename(filename);
			file.setMaskName(maskName);
			file.setFilesize(filesize);
			file.setRegId(form.getRegId());

			boardFileDataList.add(file);
		}
		form.setBoardFileDataList(boardFileDataList);

		try {

			onlineService.saveNotice(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", form);

		return "redirect:/admin/online";
	}

	@RequestMapping(value = "/online/{boardSeqNo}/edit", method = RequestMethod.GET)
	public String onlineModifyForm(HttpServletRequest request, @ModelAttribute BoardVO searchVO,
			@PathVariable String boardSeqNo, ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);

		BoardVO board = new BoardVO();
		try {

			board = onlineService.getNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("data", board);

		return "adm/online/online_write";
	}

	@RequestMapping(value = "/online/{boardSeqNo}", method = RequestMethod.POST)
	public String onlineModify(HttpServletRequest request, @ModelAttribute BoardForm form, @PathVariable String boardSeqNo,
			@RequestParam(value = "_method", defaultValue = "") String method,

			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		if ("DELETE".equals(method.toUpperCase())) {

			// save log
			actionLogService.saveLog(request, "remove online");

			return onlineRemove(boardSeqNo);
		}

		form.setBoardSeqNo(boardSeqNo);
		form.setRegId("Admin"); // TODO

		List<BoardFileDataVO> boardFileDataList = new ArrayList<BoardFileDataVO>();
		for (int i = 0; i < form.getFilename().size(); i++) {
			BoardFileDataVO file = new BoardFileDataVO();
			String filename = form.getFilename().get(i);
			String maskName = form.getMaskName().get(i);
			String filesize = form.getFilesize().get(i);

			file.setFilename(filename);
			file.setMaskName(maskName);
			file.setFilesize(filesize);
			file.setRegId(form.getRegId());

			boardFileDataList.add(file);
		}
		form.setBoardFileDataList(boardFileDataList);

		try {

			onlineService.saveNotice(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", form);

		return "redirect:/admin/online/" + boardSeqNo;
	}

	private String onlineRemove(String boardSeqNo) throws Exception {
		LOGGER.info("board seq no [{}]", boardSeqNo);

		try {

			onlineService.removeNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		return "redirect:/admin/online";
	}

}
