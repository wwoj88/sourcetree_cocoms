package cocoms.copyright.web.controllers;

import java.net.URLEncoder;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.report.ReportService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin/reports")
public class AdminReportSubController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** ReportService */
	@Resource(name = "reportService")
	protected ReportService reportService;

	/** UserService */
	@Resource(name = "userService")
	protected UserService userService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/sub", method = RequestMethod.GET)
	public String reportSub(HttpServletRequest request,
			@ModelAttribute ReportInfoVO searchVO,
			@RequestParam(value="sort", defaultValue="") String sort,
			ModelMap model) throws Exception {

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<ReportVO> list = new ArrayList<ReportVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("order", sort);

			Map<String, Object> data = reportService.getSubReports(filter);
			list = (List<ReportVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		List<String> yearList = new ArrayList<String>();
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4))-1;
		for (int i=2007; i<year; i++) {
			yearList.add(String.valueOf(i));	
		}
		
		List<String> yearList2 = new ArrayList<String>();
		for (int i=year-4; i<=year; i++) {
			yearList2.add(String.valueOf(i));	
		}

		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo()-1) * paginationInfo.getRecordCountPerPage());

		model.put("sort", sort);

		model.put("yearList", yearList);
		model.put("yearList2", yearList2);

		return "adm/report/sub_report";
	}
	
	@RequestMapping(value = "/sub/excel", method = RequestMethod.GET)
	public void reportSubExcel(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute ReportInfoVO searchVO,
			@RequestParam(value="sort", defaultValue="") String sort,
			ModelMap model) throws Exception {

		List<ReportVO> list = new ArrayList<ReportVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);
			filter.put("order", sort);

			Map<String, Object> data = reportService.getSubReports(filter);
			list = (List<ReportVO>) data.get("list");
			int total = (int) data.get("total");

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4))-1;
		
		List<String> yearList2 = new ArrayList<String>();
		for (int i=year-4; i<=year; i++) {
			yearList2.add(String.valueOf(i));	
		}

		String filename = DateUtils.getCurDateTime()+".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("신고번호");
			col.add("업체명");
			for (String yyyy : yearList2) {
				col.add(yyyy+"년");
			}

			list2.add(col);
		}
		int cnt = 0;
		for (ReportVO item : list) {
			String no = String.valueOf(list.size() - cnt);
			String appNoStr = StringUtils.isNotEmpty(item.getAppNo2()) ? "신고 " + item.getAppNoStr() : "";
			String year5 = "1".equals(item.getYear5()) ? "O" : "X";
			String year4 = "1".equals(item.getYear4()) ? "O" : "X";
			String year3 = "1".equals(item.getYear3()) ? "O" : "X";
			String year2 = "1".equals(item.getYear2()) ? "O" : "X";
			String year1 = "1".equals(item.getYear1()) ? "O" : "X";
			
			List<String> col = new ArrayList<String>();
			col.add(no);				// 번호    
			col.add(appNoStr);    		// 신고번호 
			col.add(item.getCoName());  // 업체명
			col.add(year5);    			// 
			col.add(year4);    			// 
			col.add(year3);    			// 
			col.add(year2);    			// 
			col.add(year1);    			// 
			
			list2.add(col);
			cnt++;
		}
		
		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}
	
	@RequestMapping(value = "/sub/all/{year}", method = RequestMethod.GET)
	public String reportSubAll(HttpServletRequest request,
			@PathVariable String year,
			ModelMap model) throws Exception {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		try {
			
			list = reportService.getSubAllReports(year);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("list", list);

		return "adm/report/sub_report_all";
	}
	
	@RequestMapping(value = "/sub/excel/{year}/{type}", method = RequestMethod.GET)
	public String reportSubAllExcel(HttpServletRequest request,
			@PathVariable String year,
			@PathVariable String type,
			ModelMap model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = reportService.getSubReports(year, type);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		String filename = "1".equals(type) ? "실적보고등록업체_" : "실적보고미등록업체_";
		filename += DateUtils.getCurrDateNumber()+".xls";
		filename = URLEncoder.encode(filename, "UTF-8");
		
		model.putAll(map);
		model.put("filename", filename);

		return "adm/report/sub_report_excel";
	}
	
	@RequestMapping(value = "/sub/{memberSeqNo}", method = RequestMethod.GET)
	public String reportSubDetail(HttpServletRequest request,
			@PathVariable String memberSeqNo,
			
			@ModelAttribute ReportInfoVO searchVO,
			ModelMap model) throws Exception {

		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("memberSeqNo", memberSeqNo);
		
		List<ReportStatVO> list = new ArrayList<ReportStatVO>();
		MemberVO member = new MemberVO(); 
		try {
			
			list = reportService.getSubReportSumStatuses(filter);
			
			member = userService.getMember(memberSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4))-1;
		boolean check = false;
		try {
			
			/*String rptYear = DateUtils.getCurrDateNumber().substring(0, 4);*/
			String rptYear = String.valueOf(year);
			Map<String, Object> map = reportService.getSubReport(member.getMemberSeqNo(), rptYear);
			ReportInfoVO report = (ReportInfoVO) map.get("report");
			
			LOGGER.info("report [{}]", report);
			if (report == null || StringUtils.isEmpty(report.getMemberSeqNo())) {
				check = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("data", searchVO);
		model.put("list", list);
		model.put("member", member);
		model.put("reg_year", year);
		model.put("reg_auth", check);

		return "adm/report/sub_report_view";
	}
	
	@RequestMapping(value = "/sub/{memberSeqNo}/excel", method = RequestMethod.GET)
	public void reportSubDetailExcel(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String memberSeqNo,
			
			@ModelAttribute ReportInfoVO searchVO,
			ModelMap model) throws Exception {

		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("memberSeqNo", memberSeqNo);
		
		List<ReportStatVO> list = new ArrayList<ReportStatVO>();
		try {

			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);
			list = reportService.getSubReportSumStatuses(filter);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		String filename = DateUtils.getCurDateTime()+".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
//			col.add("번호");
			col.add("년도");
			col.add("저작물수");
			col.add("사용료");
			col.add("수수료");

			list2.add(col);
		}
		int cnt = 0;
		for (ReportStatVO item : list) {
			String no = String.valueOf(list.size() - cnt);
			
			List<String> col = new ArrayList<String>();
//			col.add(no);					// 번호    
			col.add(item.getRptYear());		// 년도 
			col.add(item.getWorkSum());    	// 저작물수 
			col.add(item.getRentalSum());   // 사용료 
			col.add(item.getChrgSum());   	// 수수료 
			
			list2.add(col);
			cnt++;
		}
		
		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}
	
	@RequestMapping(value = "/sub/{memberSeqNo}/years/{year}", method = RequestMethod.GET)
	public String reportSubDetail(HttpServletRequest request,
			@PathVariable String memberSeqNo,
			@PathVariable String year,
			
			@ModelAttribute ReportInfoVO searchVO,
			ModelMap model) throws Exception {

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "adm/report/sub_report_view_year";
	}

	@RequestMapping(value = "/sub/{memberSeqNo}/years/{year}/save", method = RequestMethod.GET)
	public String reportSubExcel(HttpServletRequest request,
			@PathVariable String memberSeqNo,
			@PathVariable String year,
			ModelMap model) throws Exception {

		ReportVO report = new ReportVO();
		List<ReportStatVO> list = new ArrayList<ReportStatVO>(); 
		try {
			
			Map<String, Object> data = reportService.getSubReport(memberSeqNo, year);
			report = (ReportVO) data.get("report");
			
			list = reportService.getSubReportStatuses(memberSeqNo, year);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		String filename = "대리중개업체_실적보고_";
		filename += DateUtils.getCurrDateNumber()+".xls";
		filename = URLEncoder.encode(filename, "UTF-8");

		int inWorkNum = 0, outWorkNum = 0, workSum = 0;
		int inRental = 0, outRental = 0, rentalSum = 0, inChrg = 0, outChrg = 0, chrgSum = 0;
		float inChrgRate = 0, outChrgRate = 0, avgChrgRate = 0;
		try {
			
			for (ReportStatVO item : list) {
				//System.out.println("testetst"+item.getInWorkNum());
				inWorkNum += Integer.valueOf(item.getInWorkNum());
				outWorkNum += Integer.valueOf(item.getOutWorkNum());
				workSum += Integer.valueOf(item.getWorkSum());
				
				inRental += Integer.valueOf(item.getInRental());
				outRental += Integer.valueOf(item.getOutRental());
				rentalSum += Integer.valueOf(item.getRentalSum());

				inChrg += Integer.valueOf(item.getInChrg());
				outChrg += Integer.valueOf(item.getOutChrg());
				chrgSum += Integer.valueOf(item.getChrgSum());

				inChrgRate += Float.valueOf(item.getInChrgRate());
				outChrgRate += Float.valueOf(item.getOutChrgRate());
				avgChrgRate += Float.valueOf(item.getAvgChrgRate());
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		Map<String, Object> sum = new HashMap<String, Object>();
		sum.put("inWorkNum", inWorkNum);
		sum.put("outWorkNum", outWorkNum);
		sum.put("workSum", workSum);

		sum.put("inRental", inRental);
		sum.put("outRental", outRental);
		sum.put("rentalSum", rentalSum);

		sum.put("inChrg", inChrg);
		sum.put("outChrg", outChrg);
		sum.put("chrgSum", chrgSum);

		sum.put("inChrgRate", inChrgRate);
		sum.put("outChrgRate", outChrgRate);
		sum.put("avgChrgRate", avgChrgRate);

		model.put("sum", sum);
		model.put("report", report);
		model.put("list", list);
		
		model.put("filename", filename);
		model.put("date", DateUtils.getCurDate());
		model.put("writingKindList", "전체");		// TODO 
		
/*		model.put("inWorkNum", "");		// TODO
		model.put("outWorkNum", "");		// TODO
		model.put("workSumCnt", "");		// TODO

		model.put("inRentalCnt", "");		// TODO
		model.put("outRentalCnt", "");		// TODO
		model.put("rentalSumCnt", "");		// TODO
		model.put("inChrgCnt", "");		// TODO
		model.put("outChrgCnt", "");		// TODO
		model.put("chrgSumCnt", "");		// TODO
*/
		return "adm/report/sub_report_excel2";
	}
	
	@RequestMapping(value = {"/sub/{memberSeqNo}/years/{year}/edit", "/sub/{memberSeqNo}/years/{year}/new"}, method = RequestMethod.GET)
	public String reportSubDetailModifyForm(HttpServletRequest request,
			@PathVariable String memberSeqNo,
			@PathVariable String year,
			
			@ModelAttribute ReportInfoVO searchVO,
			ModelMap model) throws Exception {

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "adm/report/sub_report_view_year_edit";
	}
	
	@RequestMapping(value = "/sub/{memberSeqNo}/years/{year}", method = RequestMethod.POST)
	public String reportSubDetailModify(HttpServletRequest request,
			@PathVariable String memberSeqNo,
			@PathVariable String year,
			@ModelAttribute ReportForm form,
			
			@ModelAttribute ReportInfoVO searchVO,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		form.setMemberSeqNo(memberSeqNo);
		form.setRptYear(year);
		
		List<ReportStatVO> reportStatusList = new ArrayList<ReportStatVO>(); 
		for (int i=0; i<form.getWritingKind().size(); i++) {
			
			ReportStatVO reportStat = new ReportStatVO();
			reportStat.setMemberSeqNo(memberSeqNo);
			reportStat.setRptYear(year);
			reportStat.setWritingKind(form.getWritingKind().get(i));
			reportStat.setPermKind(form.getPermKind().get(i));

			reportStat.setInWorkNum(form.getInWorkNum().get(i));
			reportStat.setOutWorkNum(form.getOutWorkNum().get(i));
			reportStat.setWorkSum(form.getWorkSum().get(i));

			reportStat.setInRental(form.getInRental().get(i));
			reportStat.setOutRental(form.getOutRental().get(i));
			reportStat.setRentalSum(form.getRentalSum().get(i));
			reportStat.setInChrg(form.getInChrg().get(i));
			reportStat.setOutChrg(form.getOutChrg().get(i));
			reportStat.setChrgSum(form.getChrgSum().get(i));
			reportStat.setInChrgRate(form.getInChrgRate().get(i));
			reportStat.setOutChrgRate(form.getOutChrgRate().get(i));
			reportStat.setAvgChrgRate(form.getAvgChrgRate().get(i));

			reportStatusList.add(reportStat);
		}

		try {

			reportService.saveSubReport(form);
			
			reportService.saveSubReportStatuses(reportStatusList);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "redirect:/admin/reports/sub/"+memberSeqNo+"/years/"+year;
	}
	
	private Map<String, Object> getReportSubDetail(String memberSeqNo, String year) throws Exception {
		LOGGER.info("member seq no [{}], year [{}]", memberSeqNo, year);

//		ReportInfoVO report = new ReportInfoVO();
		List<ReportStatVO> list = new ArrayList<ReportStatVO>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = reportService.getSubReport(memberSeqNo, year);
			
			list = reportService.getSubReportStatuses(memberSeqNo, year);
			
			map.put("member", userService.getMember(memberSeqNo));
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		int inWorkNum = 0, outWorkNum = 0, workSum = 0;
		int inRental = 0, outRental = 0, rentalSum = 0, inChrg = 0, outChrg = 0, chrgSum = 0;
		float inChrgRate = 0, outChrgRate = 0, avgChrgRate = 0;
		try {

			for (ReportStatVO item : list) {
				inWorkNum += Integer.valueOf(item.getInWorkNum());
				outWorkNum += Integer.valueOf(item.getOutWorkNum());
				workSum += Integer.valueOf(item.getWorkSum());
				
				inRental += Integer.valueOf(item.getInRental());
				outRental += Integer.valueOf(item.getOutRental());
				rentalSum += Integer.valueOf(item.getRentalSum());

				inChrg += Integer.valueOf(item.getInChrg());
				outChrg += Integer.valueOf(item.getOutChrg());
				chrgSum += Integer.valueOf(item.getChrgSum());

				inChrgRate += Float.valueOf(item.getInChrgRate());
				outChrgRate += Float.valueOf(item.getOutChrgRate());
				avgChrgRate += Float.valueOf(item.getAvgChrgRate());
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		Map<String, Object> ret = new HashMap<String, Object>();
//		ret.put("report", report);
		ret.putAll(map);
		ret.put("list", list);
		ret.put("year", year);
		
		Map<String, Object> sum = new HashMap<String, Object>();
		sum.put("inWorkNum", inWorkNum);
		sum.put("outWorkNum", outWorkNum);
		sum.put("workSum", workSum);

		sum.put("inRental", inRental);
		sum.put("outRental", outRental);
		sum.put("rentalSum", rentalSum);

		sum.put("inChrg", inChrg);
		sum.put("outChrg", outChrg);
		sum.put("chrgSum", chrgSum);

		sum.put("inChrgRate", inChrgRate);
		sum.put("outChrgRate", outChrgRate);
		sum.put("avgChrgRate", avgChrgRate);

		ret.put("sum", sum);

		return ret;
	}
	
}
