package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.board.BoardService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin")
public class AdminFaqController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** BoardService */
	@Resource(name = "faqService")
	private BoardService faqService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/faq", method = RequestMethod.GET)
	public String noticeList(HttpServletRequest request,
			@ModelAttribute BoardVO searchVO,
			ModelMap model) throws Exception {
		LOGGER.info("faq list vo [{}]", searchVO);
		
		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<BoardVO> list = new ArrayList<BoardVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = faqService.getNotices(filter);
			list = (List<BoardVO>) data.get("list");
			int total = (int) data.get("total");
			
			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo()-1) * paginationInfo.getRecordCountPerPage());

		return "adm/faq/faq_list";
	}
	
	@RequestMapping(value = "/faq/{boardSeqNo}", method = RequestMethod.GET)
	public String noticeDetail(HttpServletRequest request,
			@ModelAttribute BoardVO searchVO,
			@PathVariable String boardSeqNo,
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		BoardVO board = new BoardVO();
		try {

			board = faqService.getNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("search", searchVO);
		model.put("data", board);

		return "adm/faq/faq_view";
	}
	
	@RequestMapping(value = "/faq/new", method = RequestMethod.GET)
	public String noticeWriteForm(HttpServletRequest request,
			@ModelAttribute BoardVO searchVO,
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		model.put("search", searchVO);

		return "adm/faq/faq_write";
	}
	
	@RequestMapping(value = "/faq", method = RequestMethod.POST)
	public String noticeWrite(HttpServletRequest request,
			@ModelAttribute BoardForm form,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		form.setBoardSeqNo("");
		form.setRegId("Admin");	// TODO 
		String contents=form.getContent2().replace("\n", "<br>");
		form.setContent2(contents);
		System.out.println("content  :" +form.getContent2());
		List<BoardFileDataVO> boardFileDataList = new ArrayList<BoardFileDataVO>();
		for (int i=0; i<form.getFilename().size(); i++) {
			BoardFileDataVO file = new BoardFileDataVO(); 
			String filename = form.getFilename().get(i);
			String maskName = form.getMaskName().get(i);
			String filesize = form.getFilesize().get(i);
			
			file.setFilename(filename);
			file.setMaskName(maskName);
			file.setFilesize(filesize);
			file.setRegId(form.getRegId());
			
			boardFileDataList.add(file);
		}
		form.setBoardFileDataList(boardFileDataList);

		try {
			
			faqService.saveNotice(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("data", form);

		return "redirect:/admin/faq";
	}
	
	@RequestMapping(value = "/faq/{boardSeqNo}/edit", method = RequestMethod.GET)
	public String noticeModifyForm(HttpServletRequest request,
			@ModelAttribute BoardVO searchVO,
			@PathVariable String boardSeqNo,
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		BoardVO board = new BoardVO();
		try {

			board = faqService.getNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("search", searchVO);
		model.put("data", board);

		return "adm/faq/faq_write";
	}
	
	@RequestMapping(value = "/faq/{boardSeqNo}", method = RequestMethod.POST)
	public String noticeModify(HttpServletRequest request,
			@ModelAttribute BoardForm form,
			@PathVariable String boardSeqNo,
			@RequestParam(value="_method", defaultValue="") String method,
			
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		if ("DELETE".equals(method.toUpperCase())) {
			
			// save log
			actionLogService.saveLog(request, "remove faq");
			
			return noticeRemove(boardSeqNo);
		}
		
		form.setBoardSeqNo(boardSeqNo);
		form.setRegId("Admin");	// TODO 

		List<BoardFileDataVO> boardFileDataList = new ArrayList<BoardFileDataVO>();
		for (int i=0; i<form.getFilename().size(); i++) {
			BoardFileDataVO file = new BoardFileDataVO(); 
			String filename = form.getFilename().get(i);
			String maskName = form.getMaskName().get(i);
			String filesize = form.getFilesize().get(i);
			
			file.setFilename(filename);
			file.setMaskName(maskName);
			file.setFilesize(filesize);
			file.setRegId(form.getRegId());
			
			boardFileDataList.add(file);
		}
		form.setBoardFileDataList(boardFileDataList);
		
		try {

			faqService.saveNotice(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("data", form);

		return "redirect:/admin/faq/"+boardSeqNo;
	}
	
	private String noticeRemove(String boardSeqNo) throws Exception {
		LOGGER.info("board seq no [{}]", boardSeqNo);
		
		try {

			faqService.removeNotice(boardSeqNo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return "redirect:/admin/faq";
	}
	
}
