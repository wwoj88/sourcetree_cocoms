package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.board.BoardService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.web.form.BoardForm;

@Controller
public class InquiryController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** QnAService */
	@Resource(name = "qnaService")
	private BoardService qnaService;

	@RequestMapping(value = "/inquiry/new", method = RequestMethod.GET)
	public String inquiryForm(ModelMap model) throws Exception {

		return "usr/inq/inquiry";
	}
	
	@RequestMapping(value = "/inquiry", method = RequestMethod.POST)
	public String inquiry(HttpServletRequest request,
			@ModelAttribute BoardForm form,
			ModelMap model) throws Exception {

		String loginId = CommonUtils.getLoginSession(request).getLoginId();
		
		form.setRegId(loginId);
		
		try {
			
			qnaService.saveNotice(form);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
//		return "redirect:/inquiry/new"; 
		model.put("message", "1:1문의 등록이 완료되었습니다.");
		model.put("url", "/main");
		return "comm/message";
	}
	
}
