package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.application.ApplHistoryService;
import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.board.BoardService;
import cocoms.copyright.cert.CertInfoService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.common.XCrypto;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
public class MyPageController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** ApplicationService */
	@Resource(name = "applicationService")
	private ApplicationService applicationService;

	/** CodeService */
	@Resource(name = "codeService")
	private CodeService codeService;

	/** CodeService */
	@Resource(name = "qnaService")
	private BoardService qnaService;

	/** CertInfoService */
	@Resource(name = "certInfoService")
	private CertInfoService certInfoService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** ApplHistoryService */
	@Resource(name = "applHistoryService")
	private ApplHistoryService applHistoryService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = "/mypage", method = RequestMethod.GET)
	public String mypageDetail(HttpServletRequest request, @ModelAttribute DefaultVO searchVO, ModelMap model) throws Exception {

		/*
		 * String loginId = CommonUtils.getLoginSession(request).getLoginId();
		 * 
		 * Map<String, Object> map = new HashMap<String, Object>(); try {
		 * 
		 * map = getUserInfo(loginId);
		 * 
		 * } catch (Exception e) { e.printStackTrace();
		 * LOGGER.error(e.getMessage()); throw e; }
		 * 
		 * model.putAll(map);
		 */
		return "usr/mypage/check_user";
	}

	@RequestMapping(value = "/mypage/check", method = RequestMethod.POST)
	public String mypagecheck(HttpServletRequest request, @ModelAttribute UserForm form, ModelMap model) throws Exception {

		try {
			HttpSession session = request.getSession();
			LoginVO user = (LoginVO) session.getAttribute("SESS_USER");
			LoginVO login = new LoginVO();
			//System.out.println(form.getPwd());
			login.setLoginId(user.getLoginId());
			login.setPwd(form.getPwd());

			login = userService.getUser(login);
			//System.out.println(login);
			if (login == null) {
				model.put("message", "현재 비밀번호가 틀렸습니다.");

				return "usr/mypage/check_user";
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String loginId = CommonUtils.getLoginSession(request).getLoginId();

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map = getUserInfo(loginId);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(map);

		return "usr/mypage/mypage_view";
	}

	@RequestMapping(value = "/mypage/edit", method = RequestMethod.GET)
	public String mypageModifyForm(HttpServletRequest request, @ModelAttribute DefaultVO searchVO, ModelMap model) throws Exception {

		String loginId = CommonUtils.getLoginSession(request).getLoginId();

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map = getUserInfo(loginId);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(map);

		return "usr/mypage/mypage_form";
	}

	private Map<String, Object> getUserInfo(String loginId) throws Exception {

		Map<String, Object> map = userService.getUserInfo(loginId);

		//		LoginVO login = (LoginVO) map.get("login");
		MemberVO member = (MemberVO) map.get("member");
		//		CertVO cert = (CertVO) map.get("cert");

		//		map.put("login", login);
		map.put("member", CommonUtils.memberToApplication(member));
		//		map.put("cert", cert);

		return map;
	}

	@RequestMapping(value = "/mypage", method = RequestMethod.POST)
	public String mypageModify(HttpServletRequest request, @ModelAttribute UserForm form, @RequestParam(value = "_method", defaultValue = "") String method, ModelMap model) throws Exception {
		LOGGER.info("method [{}]", method);

		if ("DELETE".equals(method.toUpperCase())) {

			// save log
			actionLogService.saveLog(request, "remove user");
			HttpSession session = request.getSession();
			LoginVO user = (LoginVO) session.getAttribute("SESS_USER");
			form.setLoginId(user.getLoginId());
			form.setMemberSeqNo(user.getMemberSeqNo());
			return mypageRemove(form);
		}

		try {
			HttpSession session = request.getSession();
			LoginVO user = (LoginVO) session.getAttribute("SESS_USER");
			LoginVO login = new LoginVO();

			login.setLoginId(user.getLoginId());
			login.setPwd(form.getPwd());

			login = userService.getUser(login);

			if (login == null) {
				model.put("message", "현재 비밀번호가 틀렸습니다.");
				model.putAll(getUserInfo(user.getLoginId()));
				return "usr/mypage/mypage_form";
			}

			login.setPwd(form.getNewPwd());
			userService.saveUser(user.getMemberSeqNo(), login, null, null);
			actionLogService.saveLog(request, "Change PWD");
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			HttpSession session = request.getSession();
			LoginVO user = (LoginVO) session.getAttribute("SESS_USER");
			map = getUserInfo(user.getLoginId());

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(map);
		model.put("chg", "1");
		return "usr/mypage/mypage_view";
	}

	private String mypageRemove(UserForm form) throws Exception {

		try {

			LoginVO login = new LoginVO();
			login.setLoginId(form.getLoginId());
			login.setDelGubun("Y");

			userService.saveUser(form.getMemberSeqNo(), login, null, null);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		return "redirect:/logout";
	}

	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public String report(HttpServletRequest request, @ModelAttribute DefaultVO searchVO, ModelMap model) throws Exception {

		String loginId = CommonUtils.getLoginSession(request).getLoginId();
		String regNo2 = "";
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> map2 = new HashMap<String, Object>();
		try {

			map = getUserInfo(loginId);

			MemberVO member = (MemberVO) map.get("member");
			if (StringUtils.isNotEmpty(member.getCeoRegNo())) {
				//암호화
				String regNo = XCrypto.decPatternReg(member.getCeoRegNo());
				member.setCeoRegNo(regNo.substring(0, 6));
				regNo2 = regNo.substring(6, 13);
			}
			map.put("member", member);
			map.put("regNo2", regNo2);
			CommonCodeVO code = new CommonCodeVO();
			CommonCodeVO code2 = new CommonCodeVO();
			try {
				if (StringUtils.isNotEmpty(member.getStatCd())) {
					code = "1".equals(member.getConditionCd()) ? codeService.getCode("DSTAT", member.getStatCd()) : codeService.getCode("SSTAT", member.getStatCd());

				}
				if (StringUtils.isNotEmpty(member.getConDetailCd())) {
					code2 = "1".equals(member.getConditionCd()) ? codeService.getCode("CONDETAIL", member.getConDetailCd()) : codeService.getCode("CONDITION", member.getConditionCd());

				}
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
				throw e;
			}

			map2 = applicationService.getAppliation(member.getMemberSeqNo(), member.getConditionCd());

			MemberVO member2 = (MemberVO) map2.get("member");

			ApplicationForm appl = CommonUtils.memberToApplication(member2);
			Map<String, Object> data = CommonUtils.pojoToMap(appl);

			data.put("statCdStr", code.getCodeValueDesc());
			data.put("conditionCdStr", code2.getCodeValueDesc());

			String preStatCd = userService.getPreStatCd(member.getMemberSeqNo());
			data.put("preStatCd", preStatCd);
			data.putAll(CommonUtils.getApplication(member.getMemberSeqNo(), member.getStatCd(), preStatCd));
			data.put("CogubunCd", member.getCoGubunCd());
			map2.put("data", data);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(map2);
		model.putAll(map);

		return "usr/mypage/report";
	}

	@RequestMapping(value = "/report/docs/1", method = RequestMethod.GET)
	public String companyDoc(HttpServletRequest request, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getLoginSession(request).getMemberSeqNo();
		String conditionCd = CommonUtils.getMemberSession(request).getConditionCd();

		model.putAll(super.getDoc1(memberSeqNo, conditionCd));

		return "1".equals(conditionCd) ? "usr/mypage/report_doc1" : "usr/mypage/report_doc5";
	}

	@RequestMapping(value = "/report/docs/2", method = RequestMethod.GET)
	public String companyDoc2(HttpServletRequest request, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getLoginSession(request).getMemberSeqNo();
		String conditionCd = CommonUtils.getLoginSession(request).getConditionCd();

		model.putAll(super.getDoc2(memberSeqNo, conditionCd));

		return "1".equals(conditionCd) ? "usr/mypage/report_doc2" : "usr/mypage/report_doc6";
	}

	@RequestMapping(value = "/report/docs/3", method = RequestMethod.GET)
	public String companyDoc3(HttpServletRequest request, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getLoginSession(request).getMemberSeqNo();
		String conditionCd = CommonUtils.getMemberSession(request).getConditionCd();

		model.putAll(super.getDoc3(memberSeqNo, conditionCd));

		return "1".equals(conditionCd) ? "usr/mypage/report_doc3" : "usr/mypage/report_doc7";
	}

	@RequestMapping(value = "/report/docs/4", method = RequestMethod.GET)
	public String companyDoc4(HttpServletRequest request, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getLoginSession(request).getMemberSeqNo();
		String conditionCd = CommonUtils.getMemberSession(request).getConditionCd();

		model.putAll(super.getDoc4(memberSeqNo, conditionCd));

		return "1".equals(conditionCd) ? "usr/mypage/report_doc4" : "usr/mypage/report_doc8";
	}

	@RequestMapping(value = "/qna")
	public String qnaList(HttpServletRequest request, @ModelAttribute DefaultVO searchVO, ModelMap model) throws Exception {

		String loginId = CommonUtils.getLoginSession(request).getLoginId();

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<BoardVO> list = new ArrayList<BoardVO>();
		try {

			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("regId", loginId);

			Map<String, Object> data = qnaService.getNotices(filter);

			list = (List<BoardVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "usr/mypage/qna_list";
	}

	@RequestMapping(value = "/qna/{boardSeqNo}", method = RequestMethod.GET)
	public String qnaDetail(HttpServletRequest request, @ModelAttribute DefaultVO searchVO, @PathVariable String boardSeqNo, ModelMap model) throws Exception {

		BoardVO board = new BoardVO();
		List<BoardVO> list = new ArrayList<BoardVO>();
		try {

			board = qnaService.getNotice(boardSeqNo);

			String content = board.getContent2();
			board.setContent2(content.replaceAll("\n", "<br/>"));

			BoardVO params = new BoardVO();
			params.setBoardSeqNo(boardSeqNo);
			params.setRef(board.getRef());
			params.setDepth("1");

			Map<String, Object> filter = CommonUtils.pojoToMap(params);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);

			Map<String, Object> data = qnaService.getNotices(filter);
			list = (List<BoardVO>) data.get("list");

			for (BoardVO item : list) {
				String content2 = item.getContent2();
				item.setContent2(content2.replaceAll("\n", "<br/>"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("data", board);
		model.put("list", list);

		return "usr/mypage/qna_view";
	}

	@RequestMapping(value = "/qna/{boardSeqNo}", method = RequestMethod.POST)
	public String qnaRemove(HttpServletRequest request, @ModelAttribute BoardForm form, @PathVariable String boardSeqNo, ModelMap model) throws Exception {

		try {

			if ("checked".equals(boardSeqNo)) {
				for (String str : form.getBoardSeqNoList()) {
					qnaService.removeNotice(str);
				}
			} else {
				qnaService.removeNotice(boardSeqNo);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		// save log
		actionLogService.saveLog(request, "remove qna");

		return "redirect:/qna";
	}

	@RequestMapping(value = "/charge", method = RequestMethod.GET)
	public String charge(HttpServletRequest request, @ModelAttribute DefaultVO searchVO, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getLoginSession(request).getMemberSeqNo();

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<CertInfoVO> list = new ArrayList<CertInfoVO>();
		try {

			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("memberSeqNo", memberSeqNo);

			Map<String, Object> data = certInfoService.getCertInfos(filter);

			list = (List<CertInfoVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("search", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "usr/mypage/charge";
	}

	@RequestMapping(value = "/charge/{key}", method = RequestMethod.POST)
	public String chargeCancel(HttpServletRequest request, @ModelAttribute DefaultVO searchVO, @PathVariable String key, ModelMap model) throws Exception {

		return "redirect:/charge";
	}

	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public String history(HttpServletRequest request, @ModelAttribute ApplHistoryVO searchVO, ModelMap model) throws Exception {

		LOGGER.info("vo [{}]", searchVO);

		MemberVO member = CommonUtils.getMemberSession(request);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = searchVO.getSearchFromDate().replaceAll("-", "");
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = searchVO.getSearchToDate().replaceAll("-", "");
			searchVO.setSearchToDate(searchKeyword);
		}

		//
		searchVO.setMemberSeqNo(member.getMemberSeqNo());

		List<BoardVO> list = new ArrayList<BoardVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = applHistoryService.getApplHistories(filter);
			list = (List<BoardVO>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchFromDate());
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchToDate());
			searchVO.setSearchToDate(searchKeyword);
		}

		/** make return value */
		model.put("data", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		return "usr/mypage/history";
	}

	@RequestMapping(value = "/history/{applSeqNo}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public String saveApplHistory(HttpServletRequest request, @PathVariable String applSeqNo, ModelMap model) throws Exception {
		LOGGER.info("applSeqNo [{}]", applSeqNo);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.put("data", applHistoryService.getApplHistory(applSeqNo));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		/** make return value */
		model.putAll(map);

		return "adm/stat/stat_appl_popup";
	}

}
