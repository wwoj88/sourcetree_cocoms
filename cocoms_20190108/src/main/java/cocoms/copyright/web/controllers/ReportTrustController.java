package cocoms.copyright.web.controllers;

import java.net.URLEncoder;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.entity.*;
import cocoms.copyright.report.ReportService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.TrustForm;

@Controller
@RequestMapping({ "/report", "/reports" })
public class ReportTrustController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** ReportService */
	@Resource(name = "reportService")
	protected ReportService reportService;

	/** UserService */
	@Resource(name = "userService")
	protected UserService userService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = "/trust")
	public String reportTrust(HttpServletRequest request, @ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		MemberVO member = CommonUtils.getMemberSession(request);
		
		if (member.getAppNo2() == null || member.getAppNo2().equals("")) {
			model.put("icon", "icon_notice_n_3");
			model.put("message", "신고증이 발급된 업체만 이용할 수 있습니다.");
			return "comm/error";
		}
		if (!"2".equals(member.getConditionCd())) {
			model.put("icon", "icon_notice_n_3");
			model.put("message", "신탁단체만 이용할 수 있습니다.");
			return "comm/error";
		}

		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("memberSeqNo", member.getMemberSeqNo());

		List<TrustStatVO> list = new ArrayList<TrustStatVO>();
		try {

			list = reportService.getTrustReportSumStatuses(filter);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		int year = Integer.valueOf(DateUtils.getCurrDateNumber().substring(0, 4)) - 1;
		boolean check = false;
		try {

			/*
			 * String rptYear = DateUtils.getCurrDateNumber().substring(0, 4);
			 */

			String rptYear = String.valueOf(year);
			Map<String, Object> map = reportService.getTrustReport(member.getMemberSeqNo(), rptYear);
			TrustInfoVO report = (TrustInfoVO) map.get("report");

			LOGGER.info("report [{}]", report);
			if (report == null || StringUtils.isEmpty(report.getMemberSeqNo())) {
				check = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		try {

			ReportInfoVO report = reportService.getTrustManageInfo();
			int start = Integer.valueOf(report.getStartDate());
			int end = Integer.valueOf(report.getEndDate());

			int date = Integer.valueOf(DateUtils.getCurrDateNumber().substring(4, 8));
			/*
			 * LOGGER.info("start [{}], end [{}], date [{}]", start, end, date);
			 */

			if (start <= date && end >= date) {
				check = check && true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", searchVO);
		model.put("list", list);
		model.put("member", member);
		model.put("reg_year", year);
		model.put("reg_auth", check);

		return "usr/report/trust_report";
	}

	@RequestMapping(value = "/trust/years/{year}", method = RequestMethod.GET)
	public String reportTrustDetail(HttpServletRequest request, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "usr/report/trust_report_view_year";
	}

	@RequestMapping(value = "/trust/years/{year}/tab2", method = RequestMethod.GET)
	public String reportTrustDetail2(HttpServletRequest request, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "usr/report/trust_report_view_year2";
	}

	@RequestMapping(value = "/trust/years/{year}/tab3", method = RequestMethod.GET)
	public String reportTrustDetail3(HttpServletRequest request, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		return "usr/report/trust_report_view_year3";
	}

	@RequestMapping(value = "/trust/years/{year}/save", method = RequestMethod.GET)
	public String reportTrustExcel(HttpServletRequest request, @PathVariable String year, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.putAll(getReportSubDetail(memberSeqNo, year));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = "신탁단체_실적보고_";
		filename += DateUtils.getCurrDateNumber() + ".xls";
		filename = URLEncoder.encode(filename, "UTF-8");

		model.putAll(map);
		model.put("filename", filename);
		model.put("date", DateUtils.getCurDate());

		return "adm/report/trust_report_excel2";
	}

	@RequestMapping(value = "/trust/years/{year}/tab2/save", method = RequestMethod.GET)
	public String reportTrustExcel2(HttpServletRequest request, @PathVariable String year, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.putAll(getReportSubDetail(memberSeqNo, year));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = "신탁단체_저작권징수분배내역_";
		filename += DateUtils.getCurrDateNumber() + ".xls";
		filename = URLEncoder.encode(filename, "UTF-8");

		model.putAll(map);
		model.put("filename", filename);
		model.put("date", DateUtils.getCurDate());

		return "adm/report/trust_report_excel3";
	}

	@RequestMapping(value = "/trust/years/{year}/tab3/save", method = RequestMethod.GET)
	public String reportTrustExcel3(HttpServletRequest request, @PathVariable String year, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.putAll(getReportSubDetail(memberSeqNo, year));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = "신탁단체_사업계획_";
		filename += DateUtils.getCurrDateNumber() + ".xls";
		filename = URLEncoder.encode(filename, "UTF-8");

		model.putAll(map);
		model.put("filename", filename);
		model.put("date", DateUtils.getCurDate());

		return "adm/report/trust_report_excel4";
	}

	@RequestMapping(value = { "/trust/years/{year}/edit", "/trust/years/{year}/new" }, method = RequestMethod.GET)
	public String reportTrustDetailModifyForm(HttpServletRequest request, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		boolean regist = request.getRequestURI().indexOf("/new") > -1;

		model.putAll(getReportSubDetail(memberSeqNo, year, regist));
		model.put("data", searchVO);
		model.put("regist", regist);

		return "usr/report/trust_report_view_year_edit";
	}

	@RequestMapping(value = { "/trust/years/{year}/tab2/edit", "/trust/years/{year}/tab2/new" }, method = RequestMethod.GET)
	public String reportTrustDetailModifyForm2(HttpServletRequest request, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		boolean regist = request.getRequestURI().indexOf("/new") > -1;

		model.putAll(getReportSubDetail(memberSeqNo, year, regist));
		model.put("data", searchVO);
		model.put("regist", regist);

		return "usr/report/trust_report_view_year2_edit";
	}

	@RequestMapping(value = { "/trust/years/{year}/tab3/edit", "/trust/years/{year}/tab3/new" }, method = RequestMethod.GET)
	public String reportTrustDetailModifyForm3(HttpServletRequest request, @PathVariable String year,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		boolean regist = request.getRequestURI().indexOf("/new") > -1;

		model.putAll(getReportSubDetail(memberSeqNo, year, regist));
		model.put("data", searchVO);
		model.put("regist", regist);

		return "usr/report/trust_report_view_year3_edit";
	}

	@RequestMapping(value = "/trust/years/{year}", method = RequestMethod.POST)
	public String reportTrustDetailModify(HttpServletRequest request, @PathVariable String year, @ModelAttribute TrustForm form,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		form.setMemberSeqNo(memberSeqNo);
		form.setRptYear(year);

		try {

			Map<String, Object> map = reportService.getTrustReport(memberSeqNo, year);
			if (map.get("report") != null) {

				TrustInfoVO report = (TrustInfoVO) map.get("report");

				//				BeanUtils.copyProperties(form, report);
				form.setBusinessGoal(report.getBusinessGoal());
				form.setProp(report.getProp());
				form.setFilename5(report.getFilename5());
				form.setFilepath5(report.getFilepath5());
				form.setFilename6(report.getFilename6());
				form.setFilepath6(report.getFilepath6());
				form.setFilename7(report.getFilename7());
				form.setFilepath7(report.getFilepath7());
				form.setFilename8(report.getFilename8());
				form.setFilepath8(report.getFilepath8());
				form.setFilename9(report.getFilename9());
				form.setFilepath9(report.getFilepath9());
			}

			reportService.saveTrustReport(form);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		if (StringUtils.isNotEmpty(form.getReturnUrl())) {
			return "redirect:" + form.getReturnUrl();
		}

		return "redirect:/report/trust/years/" + year;
	}

	@RequestMapping(value = "/trust/years/{year}/tab2", method = RequestMethod.POST)
	public String reportTrustDetailModify2(HttpServletRequest request, @PathVariable String year, @ModelAttribute TrustForm form,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		form.setMemberSeqNo(memberSeqNo);
		form.setRptYear(year);

		try {

			List<TrustStat2VO> list = new ArrayList<TrustStat2VO>();
			int idx = 0;
			for (String trustRoyaltySeqNo : form.getTrustRoyaltySeqNo()) {

				TrustStat2VO item = new TrustStat2VO();
				item.setTrustRoyaltySeqNo(form.getTrustRoyaltySeqNo().get(idx));
				item.setTrustGubunSeqNo(form.getTrustGubunSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());
				item.setContract(form.getContract().get(idx));
				item.setInCollected(form.getInCollected().get(idx));
				item.setOutCollected(form.getOutCollected().get(idx));
				item.setCollectedSum(form.getCollectedSum().get(idx));
				item.setInDividend(form.getInDividend().get(idx));
				item.setOutDividend(form.getOutDividend().get(idx));
				item.setDividendSum(form.getDividendSum().get(idx));
				item.setInDividendOff(form.getInDividendOff().get(idx));
				item.setOutDividendOff(form.getOutDividendOff().get(idx));
				item.setDividendOffSum(form.getDividendOffSum().get(idx));
				item.setInChrg(form.getInChrg().get(idx));
				item.setOutChrg(form.getOutChrg().get(idx));
				item.setChrgSum(form.getChrgSum().get(idx));
				item.setInChrgRate(form.getInChrgRate().get(idx));
				item.setOutChrgRate(form.getOutChrgRate().get(idx));
				item.setChrgRateSum(form.getChrgRateSum().get(idx));
				item.setTrustMemo(form.getTrustMemo().get(idx));

				item.setGubunName(form.getGubunName().get(idx));
				item.setGubunKind(form.getGubunKind().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportRoyaltyStatuses(list);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustCopyrightSeqNo : form.getTrustCopyrightSeqNo()) {

				TrustStat6VO item = new TrustStat6VO();
				item.setTrustCopyrightSeqNo(form.getTrustCopyrightSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());

				item.setGubun(form.getGubun().get(idx));
				item.setOrganization(form.getOrganization().get(idx));
				item.setCollected(form.getCollected().get(idx));
				item.setDividend(form.getDividend().get(idx));
				item.setDividendOff(form.getDividendOff().get(idx));
				item.setChrg(form.getChrg().get(idx));
				item.setChrgRate(form.getChrgRate().get(idx));
				item.setMemo(form.getMemo().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportCopyrightStatuses(list);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustExecSeqNo : form.getTrustExecSeqNo()) {

				TrustStat6VO item = new TrustStat6VO();
				item.setTrustExecSeqNo(form.getTrustExecSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());

				item.setGubun(form.getGubun2().get(idx));
				item.setOrganization(form.getOrganization2().get(idx));
				item.setCollected(form.getCollected2().get(idx));
				item.setDividend(form.getDividend2().get(idx));
				item.setDividendOff(form.getDividendOff2().get(idx));
				item.setChrg(form.getChrg2().get(idx));
				item.setChrgRate(form.getChrgRate2().get(idx));
				item.setMemo(form.getMemo2().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportExecuteStatuses(list);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		try {

			List<TrustStat6VO> list = new ArrayList<TrustStat6VO>();
			int idx = 0;
			for (String trustMakerSeqNo : form.getTrustMakerSeqNo()) {

				TrustStat6VO item = new TrustStat6VO();
				item.setTrustMakerSeqNo(form.getTrustMakerSeqNo().get(idx));
				item.setTrustInfoSeqNo(form.getTrustInfoSeqno());

				item.setGubun(form.getGubun3().get(idx));
				item.setOrganization(form.getOrganization3().get(idx));
				item.setCollected(form.getCollected3().get(idx));
				item.setDividend(form.getDividend3().get(idx));
				item.setDividendOff(form.getDividendOff3().get(idx));
				item.setChrg(form.getChrg3().get(idx));
				item.setChrgRate(form.getChrgRate3().get(idx));
				item.setMemo(form.getMemo3().get(idx));

				list.add(item);
				idx++;
			}
			reportService.saveTrustReportMakerStatuses(list);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		if (StringUtils.isNotEmpty(form.getReturnUrl())) {
			return "redirect:" + form.getReturnUrl();
		}

		return "redirect:/reports/trust/years/" + year + "/tab2";
	}

	@RequestMapping(value = "/trust/years/{year}/tab3", method = RequestMethod.POST)
	public String reportTrustDetailModify3(HttpServletRequest request, @PathVariable String year, @ModelAttribute TrustForm form,

			@ModelAttribute ReportInfoVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		String memberSeqNo = CommonUtils.getMemberSeqNo(request);

		form.setMemberSeqNo(memberSeqNo);
		form.setRptYear(year);

		try {

			Map<String, Object> map = reportService.getTrustReport(memberSeqNo, year);
			TrustInfoVO report = (TrustInfoVO) map.get("report");

			//			BeanUtils.copyProperties(form, report);
			report.setBusinessGoal(form.getBusinessGoal());
			report.setProp(form.getProp());
			report.setFilename5(form.getFilename5());
			report.setFilepath5(form.getFilepath5());
			report.setFilename6(form.getFilename6());
			report.setFilepath6(form.getFilepath6());
			report.setFilename7(form.getFilename7());
			report.setFilepath7(form.getFilepath7());
			report.setFilename8(form.getFilename8());
			report.setFilepath8(form.getFilepath8());
			report.setFilename9(form.getFilename9());
			report.setFilepath9(form.getFilepath9());

			reportService.saveTrustReport(report);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.putAll(getReportSubDetail(memberSeqNo, year));
		model.put("data", searchVO);

		if (StringUtils.isNotEmpty(form.getReturnUrl())) {
			return "redirect:" + form.getReturnUrl();
		}

		return "redirect:/report/trust/years/" + year + "/tab3";
	}

	private Map<String, Object> getReportSubDetail(String memberSeqNo, String year) throws Exception {

		return getReportSubDetail(memberSeqNo, year, false);
	}

	private Map<String, Object> getReportSubDetail(String memberSeqNo, String year, boolean init) throws Exception {

		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put("memberSeqNo", memberSeqNo);
		filter.put("rptYear", year);

		//		ReportInfoVO report = new ReportInfoVO();
		Map<String, Object> map = new HashMap<String, Object>();
		//		List<TrustStat2VO> list = new ArrayList<TrustStat2VO>();
		Map<String, Object> map2 = new HashMap<String, Object>();
		try {

			map = reportService.getTrustReport(memberSeqNo, year);

			map2 = reportService.getTrustReportRoyaltyStatuses(filter, init);
			List<TrustStat2VO> list = (List<TrustStat2VO>) map2.get("royaltyList");
			int contractSum = 0;
			int inCollectedSum = 0, outCollectedSum = 0;
			int inDividendSum = 0, outDividendSum = 0;
			int inDividendOffSum = 0, outDividendOffSum = 0;
			int inChrgSum = 0, outChrgSum = 0;
			float inChrgRateSum = 0, outChrgRateSum = 0;
			for (TrustStat2VO item : list) {
				contractSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getContract(), "0"));

				inCollectedSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getInCollected(), "0"));
				outCollectedSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getOutCollected(), "0"));

				inDividendSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getInDividend(), "0"));
				outDividendSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getOutDividend(), "0"));

				inDividendOffSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getInDividendOff(), "0"));
				outDividendOffSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getOutDividendOff(), "0"));

				inChrgSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getInChrg(), "0"));
				outChrgSum += Integer.parseInt(StringUtils.defaultIfEmpty(item.getOutChrg(), "0"));

				inChrgRateSum += Float.valueOf(StringUtils.defaultIfEmpty(item.getInChrgRate(), "0"));
				outChrgRateSum += Float.valueOf(StringUtils.defaultIfEmpty(item.getOutChrgRate(), "0"));
			}
			map2.put("contractSum", contractSum);
			map2.put("inCollectedSum", inCollectedSum);
			map2.put("outCollectedSum", outCollectedSum);
			map2.put("inDividendSum", inDividendSum);
			map2.put("outDividendSum", outDividendSum);
			map2.put("inDividendOffSum", inDividendOffSum);
			map2.put("outDividendOffSum", outDividendOffSum);
			map2.put("inChrgSum", inChrgSum);
			map2.put("outChrgSum", outChrgSum);
			map2.put("inChrgRateSum", inChrgRateSum);
			map2.put("outChrgRateSum", outChrgRateSum);

			map.put("member", userService.getMember(memberSeqNo));

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		Map<String, Object> ret = new HashMap<String, Object>();
		//		ret.put("report", report);
		ret.putAll(map);
		//		ret.put("list", list);
		ret.putAll(map2);
		ret.put("year", year);
		ret.put("lastYear", Integer.valueOf(year) - 1);

		return ret;
	}

}
