package cocoms.copyright.web.controllers;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cocoms.copyright.application.ExcelReportService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.entity.MemberVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
@RequestMapping("/admin/excel")
public class AdminExcelReportController {

	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	@Resource(name = "excelReportService")
	private ExcelReportService excelReportService;

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/statics")
	public String reportExcelStatics(HttpServletRequest request, ModelMap model, @ModelAttribute MemberVO searchVO, @RequestParam(value = "year", defaultValue = "", required = false) String year) throws Exception {

		MemberVO member = CommonUtils.getMemberSession(request);
		
		String smonth=request.getParameter("smonth");
		String emonth=request.getParameter("emonth");
		
		if (year == null || year.equals("")) {

			List<String> yearList = new ArrayList<String>();
			int year1 = Integer.parseInt(DateUtils.getCurrDateNumber().substring(0, 4));
			for (int i = year1; i >= year1 - 10; i--) {

				yearList.add(String.valueOf(i));
			}
			model.put("yearList", yearList);

			

			return "adm/report/excel_statics";
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		if(smonth!=null){
			String years = year+smonth;
			String yeare = year+emonth;

			map.put("year1", years);
			map.put("year2", yeare);
			}
		map.put("year", year);

		map.put("DIV", "10");
		try {

			List list = excelReportService.selectExcelReportList(map);

			model.put("list", list);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			List list_0 = excelReportService.selectExcelReportList_0(map);

			model.put("list_0", list_0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<String> yearList = new ArrayList<String>();
		int year1 = Integer.parseInt(DateUtils.getCurrDateNumber().substring(0, 4));
		for (int i = year1; i >= year1 - 10; i--) {

			yearList.add(String.valueOf(i));
		}
		model.put("yearList", yearList);
		model.put("year", year);

		return "adm/report/excel_statics";
	}

	@RequestMapping(value = "/exceldetails/{year}")
	public String reportExcelDetail(HttpServletRequest request, ModelMap model, @ModelAttribute MemberVO searchVO, @PathVariable String year) throws Exception {
		MemberVO member = CommonUtils.getMemberSession(request);

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("year", year);
		//map.put("type", member.getAppNo2());
		map.put("DIV", "10");
		try {
			List info = excelReportService.getExcelReportsMonthView(map);
			model.put("info", info);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			List info_0 = excelReportService.getExcelReportsMonthCoCnt(map);
			model.put("info_0", info_0);

		} catch (Exception e) {
			e.printStackTrace();
		}

		List<String> yearList = new ArrayList<String>();
		int year1 = Integer.parseInt(DateUtils.getCurrDateNumber().substring(0, 4));
		for (int i = year1; i >= year1 - 10; i--) {

			yearList.add(String.valueOf(i));
		}
		model.put("yearList", yearList);
		model.put("year", year.substring(0, 4));

		return "adm/report/excel_detail";
	}

	@RequestMapping(value = "/excelinfo/{appNo2}/{rept_ymd}/{conditioncd}")
	public String reportExcelDetailInfo(HttpServletRequest request, ModelMap model, @ModelAttribute MemberVO searchVO, @PathVariable String appNo2, @PathVariable String rept_ymd, @PathVariable String conditioncd) throws Exception {
		MemberVO member = CommonUtils.getMemberSession(request);

		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("year", rept_ymd);
		map.put("type", appNo2);
		map.put("DIV", "10");
		map.put("CONDITIONCD", conditioncd);
		try {
			List info = excelReportService.getExcelReportsInfo(map);
			model.put("info", info);

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			List list = excelReportService.getExcelReportsList(map);
			model.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));
			filter.put("appNo2", appNo2);
			filter.put("fromYear", rept_ymd);
			filter.put("toYear", rept_ymd);
			filter.put("conditioncd", conditioncd);

			Map<String, Object> data = excelReportService.getExcelReports(filter);
			uploadList = (List<Map<String, Object>>) data.get("list");
			int total = (int) data.get("total");

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("uploadList", uploadList);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());
		model.put("appno2", appNo2);

		return "adm/report/excel_info";
	}

	@RequestMapping(value = "/staticsdown/{year}")
	public String reportExcelStaticsDown(HttpServletRequest request, ModelMap model, @ModelAttribute MemberVO searchVO, @PathVariable String year) throws Exception {
		MemberVO member = CommonUtils.getMemberSession(request);

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("year", year);

		map.put("DIV", "10");
		try {
			List list = excelReportService.selectExcelReportList(map);

			model.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			List list_0 = excelReportService.selectExcelReportList_0(map);

			model.put("list_0", list_0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.put("filename", getFilename("위탁관리저작물_월별_보고현황"));
		model.put("year", year);
		return "adm/report/excel_statics_down";
	}

	@RequestMapping(value = "/detailsdown/{year}")
	public String reportExcelDetailsDown(HttpServletRequest request, ModelMap model, @ModelAttribute MemberVO searchVO, @PathVariable String year) throws Exception {
		MemberVO member = CommonUtils.getMemberSession(request);

		Map<String, Object> map = new HashMap<String, Object>();

		map.put("year", year);

		map.put("DIV", "10");
		try {
			List info = excelReportService.getExcelReportsMonthView(map);
			model.put("info", info);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			List info_0 = excelReportService.getExcelReportsMonthCoCnt(map);
			model.put("info_0", info_0);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//model.put("filename", getFilename("위탁관리저작물 "+years+"년 "+dates+"월 보고현황"));
		model.put("filename", getFilename("위탁관리저작물_업체보고현황"));
		model.put("year", year);

		return "adm/report/excel_detail_down";
	}

	private String getFilename(String str) {

		String filename = str + "_" + DateUtils.getCurrDateNumber() + ".xls";

		try {
			filename = URLEncoder.encode(filename, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return filename;
	}
}
