package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.board.BoardService;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.entity.*;

@Controller
public class FaqController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** FaqService */
	@Resource(name = "faqService")
	private BoardService faqService;
	
	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = "/faq", method = RequestMethod.GET)
	public String faqList(HttpServletRequest request,
			@ModelAttribute DefaultVO searchVO, 
			ModelMap model) throws Exception {

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<BoardVO> list = new ArrayList<BoardVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = faqService.getNotices(filter);
			list = (List<BoardVO>) data.get("list");
			int total = (int) data.get("total");
			
			for (BoardVO board : list) {
				String content = board.getContent2();
				content = content.replaceAll("\n", "<br/>");
				board.setContent2(content);
			}
			
			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("today", DateUtils.getCurrDateNumber());
		
		model.put("search", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo()-1) * paginationInfo.getRecordCountPerPage());

		return "usr/faq/faq";
	}
	
}
