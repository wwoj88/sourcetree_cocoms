package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.board.BoardService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin")
public class AdminUserController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;
	
	@RequestMapping(value = {"/members1", "/members2"}, method = RequestMethod.GET)
	public String memberList(HttpServletRequest request,
			@ModelAttribute UserForm searchVO,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", searchVO);
		LOGGER.info("uri [{}]", request.getRequestURI());
		
		String coGubun = getCoGubun(request);
		
		searchVO.setCoGubun(coGubun);
		
		if ("2".equals(coGubun)) {
			
			if (StringUtils.isEmpty(searchVO.getDelGubun())) {
				searchVO.setDelGubun("N");
			}
			
			if ("4".equals(searchVO.getDelGubun())) {
				searchVO.setConditionCd("2");
				searchVO.setConDetailCd("");
			}
		}
		
		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<LoginVO> list = new ArrayList<LoginVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = userService.getLogins(filter);
			list = (List<LoginVO>) data.get("list");
			int total = (int) data.get("total");
			
			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		model.put("search", searchVO);
		model.put("list", list);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo()-1) * paginationInfo.getRecordCountPerPage());

		return "1".equals(coGubun) ? "adm/member/member_list" : "adm/member/member2_list";
	}
	
	private String getCoGubun(HttpServletRequest request) {
		return request.getRequestURI().indexOf("members1") > -1 ? "1" : "2";
	}
	
	@RequestMapping(value = {"/members1/excel", "/members2/excel"}, method = RequestMethod.GET)
	public void saveExcel(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute UserForm searchVO,
			Model model) throws Exception {
		LOGGER.info("form [{}]", searchVO);
		LOGGER.info("uri [{}]", request.getRequestURI());
		
		String coGubun = getCoGubun(request);
		
		searchVO.setCoGubun(coGubun);
		
		if ("2".equals(coGubun)) {
			
			if (StringUtils.isEmpty(searchVO.getDelGubun())) {
				searchVO.setDelGubun("N");
			}
			
			if ("4".equals(searchVO.getDelGubun())) {
				searchVO.setConditionCd("2");
				searchVO.setConDetailCd("");
			}
		}
		
		List<LoginVO> list = new ArrayList<LoginVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);
			
			Map<String, Object> data = userService.getLogins(filter);
			list = (List<LoginVO>) data.get("list");
			int total = (int) data.get("total");
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		String filename = DateUtils.getCurDateTime()+".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("회원구분");
			col.add("회사명");
			col.add("이름");
			col.add("아이디");
			col.add("휴대전화");
			col.add("이메일");
			col.add("최종로그인");
			col.add("가입일자");
			col.add("SMS수신가능");
			col.add("Email수신가능");
			col.add("상태");

			list2.add(col);
		}
		int cnt = 0;
		for (LoginVO item : list) {
			String no = String.valueOf(list.size() - cnt);
			String smsAgree = "Y".equals(item.getSmsAgree()) && StringUtils.isNotEmpty(item.getSmsAgree()) ? "Y" : "N";
			String ceoEmail = "Y".equals(item.getSmsAgree()) && StringUtils.isNotEmpty(item.getCeoEmail()) ? "Y" : "N";
			String delGubun = "N".equals(item.getDelGubun()) ? "사용" : "삭제";
		    String cogubun = "1".equals(item.getCoGubun()) && StringUtils.isNotEmpty(item.getCoGubun()) ? "개인회원" :"사업자회원";
		    String coName = (String) item.getCoName(); 
			List<String> col = new ArrayList<String>();
			col.add(no);							// 번호    
			col.add(cogubun);    					// 회원구분 
			col.add(coName.replace("&#40;", "(").replace("&#41;", ")"));			// 회사명
			col.add(item.getCeoName());    			// 이름 
			col.add(item.getLoginId());    			// 아이디 
			col.add(item.getCeoMobile());   		// 휴대전화 
			col.add(item.getCeoEmail());    		// 이메일 
			col.add(item.getLastLoginDateStr());    // 최종로그인 
			col.add(item.getRegDateStr());    		// 가입일자 
			col.add(smsAgree);    					// SMS수신가능 
			col.add(ceoEmail);    					// Email수신가능 
			col.add(delGubun);    					// 상태 
			
			list2.add(col);
			cnt++;
		}
		
		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}
	
	@RequestMapping(value = {"/members1/{loginId}", "/members2/{loginId}"}, method = RequestMethod.GET)
	public String memberDetail(HttpServletRequest request,
			@ModelAttribute UserForm searchVO,
			@PathVariable String loginId,
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		LOGGER.info("login id [{}]", loginId);
		
		String coGubun = getCoGubun(request);
		
		searchVO.setCoGubun(coGubun);

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = getMember(loginId);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
//			throw e;
		}
		
		model.put("search", searchVO);
		model.putAll(map);
		
		return "1".equals(coGubun) ? "adm/member/member_view" : "adm/member/member2_view";
	}
	
	private Map<String, Object> getMember(String loginId) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			Map<String, Object> data = userService.getUserInfo(loginId);
			MemberVO member = (MemberVO) data.get("member");
			
			UserForm user = new UserForm();
			BeanUtils.copyProperties(member, user);

			if (StringUtils.isNotEmpty(member.getCeoMobile())
					&& member.getCeoMobile().indexOf("-") > -1) {
				StringTokenizer st = new StringTokenizer(member.getCeoMobile(), "-");
				if (st.hasMoreTokens()) user.setCeoMobile1(st.nextToken());
				if (st.hasMoreTokens()) user.setCeoMobile2(st.nextToken());
				if (st.hasMoreTokens()) user.setCeoMobile3(st.nextToken());
			}
			
			if (StringUtils.isNotEmpty(member.getCeoEmail())
					&& member.getCeoEmail().indexOf("@") > -1) {
				String email = member.getCeoEmail();
				user.setCeoEmail1(email.substring(0, email.indexOf("@")));	
				user.setCeoEmail2(email.substring(email.indexOf("@")+1));
			}
			
			if (StringUtils.isNotEmpty(member.getCoNo())
					&& member.getCoNo().length() >= 10) {
				user.setCoNo1(member.getCoNo().substring(0, 3));
				user.setCoNo2(member.getCoNo().substring(3, 5));
				user.setCoNo3(member.getCoNo().substring(5, 10));
			}
			
			if (StringUtils.isNotEmpty(member.getBubNo())
					&& member.getBubNo().length() >= 13) {
				user.setBubNo1(member.getBubNo().substring(0, 6));
				user.setBubNo2(member.getBubNo().substring(6, 13));
			}
			
			map.putAll(data);
			map.put("data", user);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return map;
	}
	
	@RequestMapping(value = {"/members1/new", "/members2/new"}, method = RequestMethod.GET)
	public String memberWriteForm(HttpServletRequest request,
			@ModelAttribute UserForm searchVO,
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		String coGubun = getCoGubun(request);
		
		searchVO.setCoGubun(coGubun);
		searchVO.setCoGubunCd(coGubun);
		
		model.put("search", searchVO);

		return "1".equals(coGubun) ? "adm/member/member_write" : "adm/member/member2_write";
	}
	
	@RequestMapping(value = {"/members1", "/members2"}, method = RequestMethod.POST)
	public String memberWrite(HttpServletRequest request,
			@ModelAttribute UserForm form,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		String coGubun = getCoGubun(request);
		
		form.setCoGubun(coGubun);
		
		try {
			
			LoginVO login = new LoginVO();
			BeanUtils.copyProperties(form, login);
			
			MemberVO member = new MemberVO();
			BeanUtils.copyProperties(form, member);
			
			if (StringUtils.isNotEmpty(form.getCoNo1())
					&& StringUtils.isNotEmpty(form.getCoNo2())
					&& StringUtils.isNotEmpty(form.getCoNo3())) {
				String coNo = form.getCoNo1()+form.getCoNo2()+form.getCoNo3();
				member.setCoNo(coNo);
			}
			
			if (StringUtils.isNotEmpty(form.getBubNo1())
					&& StringUtils.isNotEmpty(form.getBubNo2())) {
				String bubNo = form.getBubNo1()+form.getBubNo2();
				member.setBubNo(bubNo);
			}
			
			userService.saveUser(null, login, member, null);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
//			throw e;
			model.addAttribute("message", "아이디가 중복되었습니다.");
			return "redirect:/admin/members"+coGubun+"/new";
		}

		return "redirect:/admin/members"+coGubun;
	}

	@RequestMapping(value = {"/members1/{loginId}/edit", "/members2/{loginId}/edit"}, method = RequestMethod.GET)
	public String memberModifyForm(HttpServletRequest request,
			@ModelAttribute UserForm searchVO,
			@PathVariable String loginId,
			ModelMap model) throws Exception {
		LOGGER.info("vo [{}]", searchVO);
		
		String coGubun = getCoGubun(request);
		
		searchVO.setCoGubun(coGubun);
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = getMember(loginId);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
//			throw e;
		}
		
		model.put("search", searchVO);
		model.putAll(map);
		
		return "1".equals(coGubun) ? "adm/member/member_edit" : "adm/member/member2_edit";
	}
	
	@RequestMapping(value = {"/members1/{loginId}", "/members2/{loginId}"}, method = RequestMethod.POST)
	public String memberModify(HttpServletRequest request,
			@ModelAttribute UserForm form,
			@PathVariable String loginId,
			@RequestParam(value="_method", defaultValue="") String method,
			@RequestParam(value="returnUrl", defaultValue="") String returnUrl,
			
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		String coGubun = getCoGubun(request);
		
		form.setCoGubun(coGubun);
		
		if ("PUT".equals(method.toUpperCase())) {
			return loginModify(coGubun, loginId, form);
		}
		else if ("DELETE".equals(method.toUpperCase())) {
			
			// save log
			actionLogService.saveLog(request, "remove user");
			
			return memberRemove(coGubun, loginId, returnUrl);
		}

		try {
			
			LoginVO login = new LoginVO();
			BeanUtils.copyProperties(form, login);
			
			MemberVO member = new MemberVO();
			BeanUtils.copyProperties(form, member);
			
			if (StringUtils.isNotEmpty(form.getCeoMobile1())
					&& StringUtils.isNotEmpty(form.getCeoMobile2())
					&& StringUtils.isNotEmpty(form.getCeoMobile3())) {
				String ceoMobile = form.getCeoMobile1()+"-"+form.getCeoMobile2()+"-"+form.getCeoMobile3();
				member.setCeoMobile(ceoMobile);
			}
			
			if (StringUtils.isNotEmpty(form.getCeoEmail1())
					&& StringUtils.isNotEmpty(form.getCeoEmail2())) {
				String ceoEmail = form.getCeoEmail1()+"@"+form.getCeoEmail2();
				member.setCeoEmail(ceoEmail);
			}
			
			if (StringUtils.isNotEmpty(form.getCoNo1())
					&& StringUtils.isNotEmpty(form.getCoNo2())
					&& StringUtils.isNotEmpty(form.getCoNo3())) {
				String coNo = form.getCoNo1()+form.getCoNo2()+form.getCoNo3();
				member.setCoNo(coNo);
			}
			
			if (StringUtils.isNotEmpty(form.getBubNo1())
					&& StringUtils.isNotEmpty(form.getBubNo2())) {
				String bubNo = form.getBubNo1()+form.getBubNo2();
				member.setBubNo(bubNo);
			}
			
			String memberSeqNo = userService.getUserByLoginId(loginId).getMemberSeqNo();

			userService.saveUser(memberSeqNo, login, member, null);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return "redirect:/admin/members"+coGubun+"/"+loginId;
	}
	
	private String loginModify(String coGubun, String loginId, UserForm form) throws Exception {
		/*LOGGER.info("form [{}] [{}] [{}]", coGubun, loginId, form);*/
		
		try {
			
			String memberSeqNo = userService.getUserByLoginId(loginId).getMemberSeqNo();
			
			LoginVO login = new LoginVO();
			BeanUtils.copyProperties(form, login);
			
			userService.saveUser(memberSeqNo, login, null, null);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		return "redirect:/admin/members"+coGubun+"/"+loginId;
	}
	
	private String memberRemove(String coGubun, String loginId, String returnUrl) throws Exception {
		LOGGER.info("remove login id [{}]", loginId);
		
		try {
			
			userService.removeUser(loginId);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
//		return StringUtils.isEmpty(returnUrl) ? "redirect:/admin/members"+coGubun+"/"+loginId	// view page
		return StringUtils.isEmpty(returnUrl) ? "redirect:/admin/members"+coGubun				// list page
				: "redirect:"+returnUrl;
	}

	@RequestMapping(value = {"/members1/{loginId}/reset", "/members2/{loginId}/reset"}, method = RequestMethod.POST)
	public String resetPassword(HttpServletRequest request,
			@ModelAttribute UserForm form,
			@PathVariable String loginId,
			@RequestParam(value="sendType", defaultValue="") String sendType,
			
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		
		String coGubun = getCoGubun(request);
		
		form.setCoGubun(coGubun);
		form.setPwd(CommonUtils.getRandomId());
		
		return loginModify(coGubun, loginId, form);
	}
	
}
