package cocoms.copyright.web.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.application.IssueService;
import cocoms.copyright.application.EnrollmentService;
import cocoms.copyright.cert.CertInfoService;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.MemberMapper;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

@Controller
@RequestMapping("/admin")
public class AdminApplController extends CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "memberMapper")
	private MemberMapper memberDAO;

	/** ApplicationService */
	@Resource(name = "applicationService")
	private ApplicationService applicationService;

	/** IssueService */
	@Resource(name = "issueService")
	private IssueService issueService;
	
	/** EnrollmentService */
	@Resource(name = "enrollmentService")
	private EnrollmentService enrollmentService;

	/** CertInfoService */
	@Resource(name = "certInfoService")
	private CertInfoService certInfoService;

	/** CodeService */
	@Resource(name = "codeService")
	private CodeService codeService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	/**
	 * 업체현황
	 */
	@RequestMapping(value = { "/sub/companies", "/trust/companies" }, method = RequestMethod.GET)
	public String companyList(HttpServletRequest request, @ModelAttribute MemberVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);

		model.putAll(super.companyList(searchVO, "type1"));
		//		model.put("title1", getTitle(conditionCd));

		return "1".equals(conditionCd) ? "adm/appl/appl_sub_company" : "adm/appl/appl_trust_company";
	}
	/**
	 * 이력 정보 등록
	 */
	@RequestMapping(value = {"/sub/insert_enrollment"}, method=RequestMethod.POST)
	public String insert_enrollment(HttpServletRequest request, @ModelAttribute EnrollmentVO vo, ModelMap model) throws Exception{
		LOGGER.info("vo [{}]", vo);
		String conditionCd = vo.getGubun();
		String date = vo.getRegdate().replaceAll("-","");
		vo.setRegdate(date);
		enrollmentService.saveEnrollment(vo);
		
		//String memberSeqNo = Integer.toString(vo.getMemberSeqNo());
		
		return "1".equals(conditionCd) ? "redirect:/admin/sub/companies" : "redirect:/admin/trust/companies";
	}
	
	/**
	 * 이력 정보 등록
	 */
	@RequestMapping(value = {"/sub/insert_enrollment2"}, method=RequestMethod.POST)
	public String insert_enrollment2(HttpServletRequest request, @ModelAttribute EnrollmentVO vo, ModelMap model) throws Exception{
		LOGGER.info("vo [{}]", vo);
		String conditionCd = vo.getGubun();

		String date = vo.getRegdate().replaceAll("-","");
		vo.setRegdate(date);
		enrollmentService.saveEnrollment(vo);
		 Map<String, Object> map = new HashMap<String,Object>();
		 map.put("code", request.getParameter("code"));
		 map.put("conditionCd", request.getParameter("conditionCd"));
		 map.put("prestatCd", request.getParameter("prestatCd"));
		 map.put("checkCode", request.getParameter("checkCode"));
		 map.put("memberSeqNo", vo.getMemberSeqNo());
		//String memberSeqNo = Integer.toString(vo.getMemberSeqNo());
		model.put("result", map);
		 
		return "comm/include/result" ;
	}

	@RequestMapping(value = { "/sub/companies/excel", "/trust/companies/excel" }, method = RequestMethod.GET)
	public void saveExcel(HttpServletRequest request, HttpServletResponse response, @ModelAttribute MemberVO searchVO, Model model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);

		Map<String, Object> map = super.companyList(searchVO, "type1", false);

		List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("list");

		String filename = DateUtils.getCurDateTime() + ".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("신고번호");
			col.add("회사명");
			col.add("신청일");
			col.add("등록일");
			col.add("상태");
			col.add("신고서");
			col.add("신고증");
			col.add("변경신고서");
			col.add("변경신고증");
			col.add("발급현황");
			col.add("이력정보");
			col.add("실적");

			list2.add(col);
		}
		int cnt = 0;
		for (Map<String, Object> item : list) {
			String no = String.valueOf(list.size() - cnt);
			String down1 = ((Boolean) item.get("down1")) == true ? "O" : "X";
			String down2 = ((Boolean) item.get("down2")) == true ? "O" : "X";
			String down3 = ((Boolean) item.get("down3")) == true ? "O" : "X";
			String down4 = ((Boolean) item.get("down4")) == true ? "O" : "X";
			String issCnt = Integer.valueOf((String) item.get("issCnt")) > 0 ? "O" : "X";
			String enrollmentCnt = Integer.valueOf((String) item.get("enrollmentCnt")) > 0 ? "O" : "X";
			String reportCnt = Integer.valueOf((String) item.get("reportCnt")) > 0 ? "O" : "X";
			String coName = (String) item.get("coName");

			List<String> col = new ArrayList<String>();
			col.add(no); // 번호    
			col.add((String) item.get("appNoStr")); // 신고번호    
			col.add(coName.replace("&#40;", "(").replace("&#41;", ")")); // 회사명    
			col.add((String) item.get("regDate2Str")); // 신청일    
			col.add((String) item.get("appRegDateStr")); // 등록일    
			col.add((String) item.get("statCdStr")); // 상태    
			col.add(down1); // 신고서    
			col.add(down2); // 신고증    
			col.add(down3); // 변경신고서    
			col.add(down4); // 변경신고증    
			col.add(issCnt); // 발급현황    
			col.add(enrollmentCnt); // 이력정보
			col.add(reportCnt); // 실적    

			list2.add(col);
			cnt++;
		}

		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/charges/save/{certInfoSeqNo}", "/trust/companies/{memberSeqNo}/charges/save/{certInfoSeqNo}", "/sub/statuses/{memberSeqNo}/charges/save/{certInfoSeqNo}",
			"/trust/statuses/{memberSeqNo}/charges/save/{certInfoSeqNo}" }, method = RequestMethod.POST)
	public String save(HttpServletRequest request, ApplicationForm form, @PathVariable String certInfoSeqNo,
			//			BindingResult bindingResult, 
			//			SessionStatus status,
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		boolean gpki_use = propertiesService.getBoolean("gpki.use");
		Properties prop = new Properties();
		prop.setProperty("cms.ldapUrl", propertiesService.getString("gpki.ldapUrl"));
		prop.setProperty("cms.certForSignFile", propertiesService.getString("gpki.certForSignFile"));
		prop.setProperty("cms.keyForSignFile", propertiesService.getString("gpki.keyForSignFile"));
		prop.setProperty("cms.certForEnvFile", propertiesService.getString("gpki.certForEnvFile"));
		prop.setProperty("cms.keyForEnvFile", propertiesService.getString("gpki.keyForEnvFile"));
		prop.setProperty("cms.pinForSign", propertiesService.getString("gpki.pinForSign"));
		prop.setProperty("cms.pinForEnv", propertiesService.getString("gpki.pinForEnv"));
		prop.setProperty("cms.DN", propertiesService.getString("gpki.DN"));
		prop.setProperty("cms.OU", propertiesService.getString("gpki.OU"));
		prop.setProperty("cms.AT", propertiesService.getString("gpki.AT"));
		prop.setProperty("cms.LIC", propertiesService.getString("gpki.LIC"));

		/* return value */
		boolean result = false;
		try {

			String g4cssCK = new GPKICms(prop, gpki_use).cmsDec(form.getG4cssCK());
			LOGGER.info("g4cssCK result [{}]", g4cssCK);

			String[] val = g4cssCK.split("\\^");

			CertInfoVO certInfo = new CertInfoVO();

			// error
			if (g4cssCK.indexOf("\\^") == 1 && val.length > 3) {
				result = false;
				certInfo.setIsSuccess(val[0]);
				certInfo.setReqFindTime(val[1]);
				certInfo.setMallSeq(val[2]);
				certInfo.setIsFail(val[3]);
			}
			// success 
			else if (g4cssCK.indexOf("\\^") != 1 && val.length > 2) {
				result = true;
				certInfo.setIsSuccess(val[0]);
				certInfo.setReqFindTime(val[1]);
				certInfo.setMallSeq(val[2]);
			}

			// update CERT_INFO
			certInfo.setCertInfoSeqNo(certInfoSeqNo);
			certInfoService.saveCertInfo(certInfo);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("result", result);
		return "adm/appl/appl_cancel";
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/charges/cancel", "/trust/companies/{memberSeqNo}/charges/cancel", "/sub/statuses/{memberSeqNo}/charges/cancel",
			"/trust/statuses/{memberSeqNo}/charges/cancel" }, method = RequestMethod.POST)
	public @ResponseBody String cancel(HttpServletRequest request, CertForm form,
			//			BindingResult bindingResult, 
			//			SessionStatus status,
			//, produces = "application/json;charset=UTF-8"
			ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		MemberVO member = userService.getMember(form.getMemberSeqNo());
		LOGGER.info("member [{}]", member);

		String hddReqManName = StringUtils.isNotEmpty(member.getCoName()) ? member.getCoName() : member.getCeoName();
		/*
		 * hddReqManName = new String(hddReqManName.getBytes("UTF-8"),
		 * "ISO8859-1");
		 */
		//hddReqManName = new String(hddReqManName.getBytes("UTF-8"));

		// g4cssCK
		String g4cssCK = "";

		String str = "";
		str += form.getMallSeq().replaceAll("\\^", "<>") + "^";
		str += "2" + "^";
		str += hddReqManName + "^";
		str += form.getReturnUrl().replaceAll("\\^", "<>");
		LOGGER.info("str [{}]", str);
		/*
		 * byte[] byteBuffUTF8=str.getBytes(Charset.forName("UTF-8")); str=new
		 * String(byteBuffUTF8, "UTF-8"); //자바의 유니코드 형식으로
		 * System.out.println("Mid: "+str);
		 * 
		 * byte[] byteBuffEuc=str.getBytes("KSC5601"); str = new
		 * String(byteBuffEuc, "ISO8859-1"); System.out.println("FIN :"+str);
		 */
		str = new String(str.getBytes("KSC5601"), "ISO8859-1");
		boolean gpki_use = propertiesService.getBoolean("gpki.use");
		Properties prop = new Properties();
		prop.setProperty("cms.ldapUrl", propertiesService.getString("gpki.ldapUrl"));
		prop.setProperty("cms.certForSignFile", propertiesService.getString("gpki.certForSignFile"));
		prop.setProperty("cms.keyForSignFile", propertiesService.getString("gpki.keyForSignFile"));
		prop.setProperty("cms.certForEnvFile", propertiesService.getString("gpki.certForEnvFile"));
		prop.setProperty("cms.keyForEnvFile", propertiesService.getString("gpki.keyForEnvFile"));
		prop.setProperty("cms.pinForSign", propertiesService.getString("gpki.pinForSign"));
		prop.setProperty("cms.pinForEnv", propertiesService.getString("gpki.pinForEnv"));
		prop.setProperty("cms.DN", propertiesService.getString("gpki.DN"));
		prop.setProperty("cms.OU", propertiesService.getString("gpki.OU"));
		prop.setProperty("cms.AT", propertiesService.getString("gpki.AT"));
		prop.setProperty("cms.LIC", propertiesService.getString("gpki.LIC"));

		try {
			g4cssCK = new GPKICms(prop, gpki_use).cmsEnc(str);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}
		LOGGER.info("g4cssCK [{}]", g4cssCK);

		// return value
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("g4cssCK", g4cssCK);

		return CommonUtils.makeSuccess(map);
	}

	/**
	 * 발급이력
	 */
	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/issues", "/trust/companies/{memberSeqNo}/issues", "/sub/statuses/{memberSeqNo}/issues", "/trust/statuses/{memberSeqNo}/issues" }, method = RequestMethod.GET)
	public String issueList(HttpServletRequest request, @ModelAttribute MemberVO searchVO, @PathVariable String memberSeqNo,

			ModelMap model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);

		MemberVO member = userService.getMember(memberSeqNo);
		model.put("member", member);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		List<IssueVO> list = new ArrayList<IssueVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = issueService.getIssues(filter);
			list = (List<IssueVO>) data.get("list");
			int total = (int) data.get("total");

			String codeGubun = "1".equals(conditionCd) ? "DSTAT" : "SSTAT";

			for (IssueVO item : list) {
				CommonCodeVO code = codeService.getCode(codeGubun, item.getStatCd());

				Map<String, Object> map = CommonUtils.pojoToMap(item);
				map.put("statCdStr", code.getCodeValueDesc());

				list2.add(map);
			}

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", searchVO);
		//model.put("list", list);
		model.put("list", list2);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		model.put("title1", getTitle(conditionCd));
		model.put("title2", request.getRequestURI().indexOf("/companies/") > -1 ? "업체현황" : "1".equals(conditionCd) ? "신고현황" : "허가신청현황");

		return "adm/appl/appl_issue";
	}
	
	/**
	 * 이력정보
	 */
	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/enrollment", "/trust/companies/{memberSeqNo}/enrollment", "/sub/statuses/{memberSeqNo}/enrollment", "/trust/statuses/{memberSeqNo}/enrollment" }, method = RequestMethod.GET)
	public String enrollmentList(HttpServletRequest request, @ModelAttribute MemberVO searchVO, @PathVariable String memberSeqNo,
			@ModelAttribute EnrollmentVO enrollmentVO,
			ModelMap model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);
		LOGGER.info("member seq no [{}]", memberSeqNo);
		
		String conditionCd = getConditionCd(request);

		String searchAccountManager = enrollmentVO.getSearchAccountManager();
		String searchFromDate = enrollmentVO.getSearchFromDate().replaceAll("-", "");
		String searchToDate = enrollmentVO.getSearchToDate().replaceAll("-", "");
		String searchContents = enrollmentVO.getSearchContents();
		enrollmentVO.setSearchFromDate(searchFromDate);
		enrollmentVO.setSearchToDate(searchToDate);
		searchVO.setConditionCd(conditionCd);

		MemberVO member = userService.getMember(memberSeqNo);
		model.put("member", member);
		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(enrollmentVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		List<EnrollmentVO> list = new ArrayList<EnrollmentVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(enrollmentVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = enrollmentService.getIssues(filter);
			list = (List<EnrollmentVO>) data.get("list");
			int total = (int) data.get("total");

			String codeGubun = "1".equals(conditionCd) ? "DSTAT" : "SSTAT";
			for (EnrollmentVO item : list) {
				CommonCodeVO code = codeService.getCode(codeGubun, item.getStatCd());
				Map<String, Object> map = CommonUtils.pojoToMap(item);
		//		map.put("statCdStr", code.getCodeValueDesc());
				list2.add(map);
			}
			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		  Calendar cal = Calendar.getInstance();
	       cal.setTime(new Date());
	       DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		/*if (StringUtils.isEmpty(searchFromDate)) {
			cal.add(Calendar.YEAR, -1);
			searchFromDate=df.format(cal.getTime());
		}*/
	       
	    if (StringUtils.isEmpty(searchFromDate)) {
	      searchFromDate=DateUtils.getCurDate().replaceAll("-", "");
			
			}
		if (StringUtils.isEmpty(searchToDate)) {
			searchToDate=DateUtils.getCurDate().replaceAll("-", "");
		
		}

		model.put("searchAccountManager", searchAccountManager);
		model.put("searchFromDate", searchFromDate);
		model.put("searchToDate", searchToDate);
		model.put("searchContents", searchContents);
		model.put("data", searchVO);
		//model.put("list", list);
		model.put("list", list2);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		model.put("title1", getTitle(conditionCd));
		model.put("title2", request.getRequestURI().indexOf("/companies/") > -1 ? "업체현황" : "1".equals(conditionCd) ? "신고현황" : "허가신청현황");

		return "adm/appl/appl_enrollment";
	}

	/**
	 * 신청이력
	 */
	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/history", "/trust/companies/{memberSeqNo}/history", "/sub/statuses/{memberSeqNo}/history", "/trust/statuses/{memberSeqNo}/history" }, method = RequestMethod.GET)
	public String applicationList(HttpServletRequest request, @ModelAttribute MemberVO searchVO, @PathVariable String memberSeqNo,

			ModelMap model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);

		MemberVO member = userService.getMember(memberSeqNo);
		model.put("member", member);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		List<RpMgmVO> list = new ArrayList<RpMgmVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = applicationService.getRpMgms(filter);
			list = (List<RpMgmVO>) data.get("list");
			int total = (int) data.get("total");

			String codeGubun = "1".equals(conditionCd) ? "DSTAT" : "SSTAT";

			for (RpMgmVO item : list) {
				CommonCodeVO code = codeService.getCode(codeGubun, item.getStatCd());

				Map<String, Object> map = CommonUtils.pojoToMap(item);
				map.put("statCdStr", code.getCodeValueDesc());

				list2.add(map);
			}

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", searchVO);
		//model.put("list", list);
		model.put("list", list2);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		model.put("title1", getTitle(conditionCd));
		model.put("title2", request.getRequestURI().indexOf("/companies/") > -1 ? "업체현황" : "신고현황");

		model.put("uri1", "1".equals(conditionCd) ? "sub" : "trust");
		model.put("uri2", request.getRequestURI().indexOf("/companies/") > -1 ? "companies" : "statuses");

		return "adm/appl/appl_history";
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/history/excel", "/trust/companies/{memberSeqNo}/history/excel", "/sub/statuses/{memberSeqNo}/history/excel",
			"/trust/statuses/{memberSeqNo}/history/excel" }, method = RequestMethod.GET)
	public void applicationListExcel(HttpServletRequest request, HttpServletResponse response, @ModelAttribute MemberVO searchVO, @PathVariable String memberSeqNo,

			ModelMap model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);

		MemberVO member = userService.getMember(memberSeqNo);
		model.put("member", member);

		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		List<RpMgmVO> list = new ArrayList<RpMgmVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);

			Map<String, Object> data = applicationService.getRpMgms(filter);
			list = (List<RpMgmVO>) data.get("list");
			int total = (int) data.get("total");

			String codeGubun = "1".equals(conditionCd) ? "DSTAT" : "SSTAT";

			for (RpMgmVO item : list) {
				CommonCodeVO code = codeService.getCode(codeGubun, item.getStatCd());

				Map<String, Object> map = CommonUtils.pojoToMap(item);
				map.put("statCdStr", code.getCodeValueDesc());

				list2.add(map);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = DateUtils.getCurDateTime() + ".xls";

		List<List<String>> excelList = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("일자");
			col.add("시간");
			col.add("업무처리 상태");
			col.add("메모");

			excelList.add(col);
		}
		int cnt = 0;
		for (Map<String, Object> item : list2) {
			String no = String.valueOf(list.size() - cnt);

			List<String> col = new ArrayList<String>();
			col.add(no); // 번호    
			col.add((String) item.get("regDateStr")); // 일자
			col.add((String) item.get("regTimeStr")); // 시간
			col.add((String) item.get("statCdStr")); // 업무처리 상태
			col.add((String) item.get("rpMemo")); // 메모

			excelList.add(col);
			cnt++;
		}

		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(excelList);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}

	/**
	 * 결제내역
	 */
	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/charges", "/trust/companies/{memberSeqNo}/charges", "/sub/statuses/{memberSeqNo}/charges", "/trust/statuses/{memberSeqNo}/charges" }, method = RequestMethod.GET)
	public String chargeList(HttpServletRequest request, @ModelAttribute MemberVO searchVO, @PathVariable String memberSeqNo,

			ModelMap model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);
		searchVO.setMemberSeqNo(memberSeqNo);

		MemberVO member = userService.getMember(memberSeqNo);
		model.put("member", member);

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		List<CertInfoVO> list = new ArrayList<CertInfoVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			Map<String, Object> data = certInfoService.getCertInfos(filter);
			list = (List<CertInfoVO>) data.get("list");
			int total = (int) data.get("total");

			//String codeGubun = "1".equals(conditionCd) ? "DSTAT" : "SSTAT";
			//	
			//for (CertInfoVO item : list) {
			//	CommonCodeVO code = codeService.getCode(codeGubun, item.getStatCd());
			//	
			//	Map<String, Object> map = CommonUtils.pojoToMap(item);
			//	map.put("statCdStr", code.getCodeValueDesc());
			//	
			//	list2.add(map);
			//}

			paginationInfo.setTotalRecordCount(total);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		model.put("data", searchVO);
		model.put("list", list);
		//model.put("list", list2);
		model.put("page", paginationInfo.getCurrentPageNo());
		model.put("total", paginationInfo.getTotalRecordCount());
		model.put("paginationInfo", paginationInfo);
		model.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		model.put("title1", getTitle(conditionCd));
		model.put("title2", request.getRequestURI().indexOf("/companies/") > -1 ? "업체현황" : "신고현황");

		model.put("uri1", "1".equals(conditionCd) ? "sub" : "trust");
		model.put("uri2", request.getRequestURI().indexOf("/companies/") > -1 ? "companies" : "statuses");

		return "adm/appl/appl_charge";
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/charges/excel", "/trust/companies/{memberSeqNo}/charges/excel", "/sub/statuses/{memberSeqNo}/charges/excel",
			"/trust/statuses/{memberSeqNo}/charges/excel" }, method = RequestMethod.GET)
	public void chargeListExcel(HttpServletRequest request, HttpServletResponse response, @ModelAttribute MemberVO searchVO, @PathVariable String memberSeqNo,

			ModelMap model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);
		searchVO.setMemberSeqNo(memberSeqNo);

		MemberVO member = userService.getMember(memberSeqNo);
		model.put("member", member);

		List<CertInfoVO> list = new ArrayList<CertInfoVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.put("firstRecordIndex", 0);
			filter.put("lastRecordIndex", 9999);

			Map<String, Object> data = certInfoService.getCertInfos(filter);
			list = (List<CertInfoVO>) data.get("list");
			int total = (int) data.get("total");

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		String filename = DateUtils.getCurDateTime() + ".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("처리일자");
			col.add("처리시간");
			col.add("업무처리 상태");
			col.add("결제방법");
			col.add("결제처리상태");

			list2.add(col);
		}
		int cnt = 0;
		for (CertInfoVO item : list) {
			String no = String.valueOf(list.size() - cnt);
			String conditionCdStr = "1".equals(member.getConditionCd()) ? "저작권대리중개업" : "2".equals(member.getConditionCd()) ? "저작권신탁관리업" : "";
			String statCdStr = "1".equals(member.getStatCd()) ? "신고신청" : "3".equals(member.getStatCd()) ? "변경신고신청" : "";
			String payMthdCd = "0402".equals(item.getPayMthdCd()) ? "카드결제" : "0403".equals(item.getPayMthdCd()) ? "계좌이체" : "0404".equals(item.getPayMthdCd()) ? "휴대폰" : "0407".equals(item.getPayMthdCd()) ? "카드포인트" : "";
			String isSuccess = "1".equals(item.getIsSuccess()) ? "결제완료" : "2".equals(item.getIsSuccess()) ? "취소완료" : "신청처리오류";

			List<String> col = new ArrayList<String>();
			col.add(no); // 번호
			col.add(item.getRegDateStr()); // 처리일자
			col.add(item.getRegTimeStr()); // 처리시간
			col.add(conditionCdStr + " " + statCdStr); // 업무처리 상태
			col.add(payMthdCd); // 결제방법
			col.add(isSuccess); // 결제처리상태

			list2.add(col);
			cnt++;
		}

		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/docs/1", "/trust/companies/{memberSeqNo}/docs/1", "/sub/statuses/{memberSeqNo}/docs/1", "/trust/statuses/{memberSeqNo}/docs/1" }, method = RequestMethod.GET)
	public String companyDoc(HttpServletRequest request, @PathVariable String memberSeqNo, ModelMap model) throws Exception {
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		model.putAll(super.getDoc1(memberSeqNo, conditionCd));
		model.put("title1", getTitle(conditionCd));
		model.put("title2", "업체현황");

		return "1".equals(conditionCd) ? "adm/appl/appl_doc1" : "adm/appl/appl_doc5";
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/docs/2", "/trust/companies/{memberSeqNo}/docs/2", "/sub/statuses/{memberSeqNo}/docs/2", "/trust/statuses/{memberSeqNo}/docs/2" }, method = RequestMethod.GET)
	public String companyDoc2(HttpServletRequest request, @PathVariable String memberSeqNo, ModelMap model) throws Exception {
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		model.putAll(super.getDoc2(memberSeqNo, conditionCd));
		model.put("title1", getTitle(conditionCd));
		model.put("title2", "업체현황");

		return "1".equals(conditionCd) ? "adm/appl/appl_doc2" : "adm/appl/appl_doc6";
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/docs/3", "/trust/companies/{memberSeqNo}/docs/3", "/sub/statuses/{memberSeqNo}/docs/3", "/trust/statuses/{memberSeqNo}/docs/3" }, method = RequestMethod.GET)
	public String companyDoc3(HttpServletRequest request, @PathVariable String memberSeqNo, ModelMap model) throws Exception {
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		model.putAll(super.getDoc3(memberSeqNo, conditionCd));
		model.put("title1", getTitle(conditionCd));
		model.put("title2", "업체현황");

		return "1".equals(conditionCd) ? "adm/appl/appl_doc3" : "adm/appl/appl_doc7";
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}/docs/4", "/trust/companies/{memberSeqNo}/docs/4", "/sub/statuses/{memberSeqNo}/docs/4", "/trust/statuses/{memberSeqNo}/docs/4" }, method = RequestMethod.GET)
	public String companyDoc4(HttpServletRequest request, @PathVariable String memberSeqNo, ModelMap model) throws Exception {
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);

		model.putAll(super.getDoc4(memberSeqNo, conditionCd));
		model.put("title1", getTitle(conditionCd));
		model.put("title2", "업체현황");

		return "1".equals(conditionCd) ? "adm/appl/appl_doc4" : "adm/appl/appl_doc8";
	}

	@RequestMapping(value = "/sub/companies/{memberSeqNo}/status", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String saveCompanyStatCd(HttpServletRequest request, MemberVO form, @PathVariable String memberSeqNo, ModelMap model) throws Exception {
		LOGGER.info("member seq no [{}], stat cd [{}]", memberSeqNo, form.getStatCd());

try{
		String statCd = form.getStatCd();
		String saveCd = form.getStatCd();
		String prestatCd = request.getParameter("prestatCd");
		String checkCode = request.getParameter("checkCode");

		if (statCd.equals("7")) { // 보완요청이오면 

			if (prestatCd.equals("0")) { // 새로운신청이면 

				form.setStatCd(checkCode); // 신고접수를위한 코드 +9
				String str2 = super.saveCompanyStatCd(form, memberSeqNo); // 신고접수상태로 업데이트 

			} else if (prestatCd.equals("19") || prestatCd.equals("39")) { // 신고접수 된 상태에서 보완			
				if (!checkCode.equals("") || checkCode != null) {

					form.setStatCd(checkCode);
					String str3 = super.saveCompanyStatCd(form, memberSeqNo);

				}

			}
		}
		form.setStatCd(saveCd);

		String str = super.saveCompanyStatCd(form, memberSeqNo);

		// noti
		LOGGER.info("condition cd [{}], stat cd [{}]", form.getConditionCd(), statCd);
		String code = "";
		if ("1".equals(form.getConditionCd())) {
			if ("5".equals(statCd))
				code = "51"; // 신고취소
			else if ("5".equals(statCd))
				code = "52"; // 신고정지
			else if ("59".equals(statCd))
				code = "53"; // 신고정지해제
			else if ("19".equals(statCd) || "39".equals(statCd))
				code = "54"; // 접수
			else if ("2".equals(statCd))
				code = "55"; // 신고신청처리
			else if ("4".equals(statCd))
				code = "56"; // 변경신고신청처리
			else if ("7".equals(statCd))
				code = "57"; // 보완요청
			else if ("8".equals(statCd))
				code = "58"; // 보완완료
			else if ("9".equals(statCd))
				code = "59"; // 반려
		} else if ("2".equals(form.getConditionCd())) {
			if ("5".equals(statCd))
				code = "61"; // 신고취소
			else if ("5".equals(statCd))
				code = "62"; // 신고정지
			else if ("59".equals(statCd))
				code = "63"; // 신고정지해제
			else if ("19".equals(statCd) || "39".equals(statCd))
				code = "64"; // 접수
			else if ("2".equals(statCd))
				code = "65"; // 신고신청처리
			else if ("4".equals(statCd))
				code = "66"; // 변경신고신청처리
			else if ("7".equals(statCd))
				code = "67"; // 보완요청
			else if ("8".equals(statCd))
				code = "68"; // 보완완료
			else if ("9".equals(statCd))
				code = "69"; // 반려
		}
		if (StringUtils.isNotEmpty(code)) {

			LoginVO login = new LoginVO();
			login.setMemberSeqNo(memberSeqNo);
			login = userService.getUser(login);

			noti(code, login);
		}
		return str;
}catch (Exception e ){
	e.printStackTrace();
}
return memberSeqNo;
	
	}

	/**
	 * 신고현황
	 */
	@RequestMapping(value = { "/sub/statuses", "/trust/statuses" }, method = RequestMethod.GET)
	public String statusList(HttpServletRequest request, @ModelAttribute MemberVO searchVO, ModelMap model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);

		// default value
		if ("1".equals(conditionCd) & StringUtils.isEmpty(searchVO.getConDetailCd())) {
			searchVO.setConDetailCd("3");
		}

		model.putAll(super.companyList(searchVO, "type2"));

		return "1".equals(conditionCd) ? "adm/appl/appl_sub_status" : "adm/appl/appl_trust_status";
	}

	@RequestMapping(value = { "/sub/statuses/excel", "/trust/statuses/excel" }, method = RequestMethod.GET)
	public void statusListExcel(HttpServletRequest request, HttpServletResponse response, @ModelAttribute MemberVO searchVO, Model model) throws Exception {
		LOGGER.info("search vo [{}]", searchVO);

		String conditionCd = getConditionCd(request);

		searchVO.setConditionCd(conditionCd);

		// default value
		if ("1".equals(conditionCd) && StringUtils.isEmpty(searchVO.getConDetailCd())) {
			searchVO.setConDetailCd("3");
		}

		Map<String, Object> map = super.companyList(searchVO, "type2", false);

		List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("list");

		String filename = DateUtils.getCurDateTime() + ".xls";

		List<List<String>> list2 = new ArrayList<List<String>>();
		{
			List<String> col = new ArrayList<String>();
			col.add("번호");
			col.add("신고번호");
			col.add("회사명");
			col.add("신청일");
			col.add("등록일");
			col.add("상태");
			col.add("신고서");
			col.add("신고증");
			col.add("변경신고서");
			col.add("변경신고증");
			col.add("발급현황");
			col.add("이력정보");
			col.add("실적");

			list2.add(col);
		}
		int cnt = 0;
		for (Map<String, Object> item : list) {
			String no = String.valueOf(list.size() - cnt);
			String down1 = ((Boolean) item.get("down1")) == true ? "O" : "X";
			String down2 = ((Boolean) item.get("down2")) == true ? "O" : "X";
			String down3 = ((Boolean) item.get("down3")) == true ? "O" : "X";
			String down4 = ((Boolean) item.get("down4")) == true ? "O" : "X";
			String issCnt = Integer.valueOf((String) item.get("issCnt")) > 0 ? "O" : "X";
			String enrollmentCnt = Integer.valueOf((String) item.get("enrollmentCnt")) > 0 ? "O" : "X";
			String reportCnt = Integer.valueOf((String) item.get("reportCnt")) > 0 ? "O" : "X";
			String coName = (String) item.get("coName");

			List<String> col = new ArrayList<String>();
			col.add(no); // 번호    
			col.add((String) item.get("appNoStr")); // 신고번호    
			col.add(coName.replace("&#40;", "(").replace("&#41;", ")")); // 회사명    
			col.add((String) item.get("regDate2Str")); // 신청일    
			col.add((String) item.get("appRegDateStr")); // 등록일    
			col.add((String) item.get("statCdStr")); // 상태    
			col.add(down1); // 신고서    
			col.add(down2); // 신고증    
			col.add(down3); // 변경신고서    
			col.add(down4); // 변경신고증    
			col.add(issCnt); // 발급현황    
			col.add(enrollmentCnt); // 이력정보
			col.add(reportCnt); // 실적    

			list2.add(col);
			cnt++;
		}

		/*
		 * start download
		 */
		Workbook workbook = CommonUtils.makeExel(list2);
		CommonUtils.setExcelDownloadHeader(response, workbook, filename);
	}

	@RequestMapping(value = { "/sub/statuses/{memberSeqNo}", "/trust/statuses/{memberSeqNo}" }, method = RequestMethod.GET)
	public String statusDetail(HttpServletRequest request, @PathVariable String memberSeqNo, ModelMap model) throws Exception {
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);
		String statcd = memberDAO.selectStatCd(memberSeqNo);

		model.putAll(super.companyDetail2(memberSeqNo, conditionCd));

		model.put("title1", getTitle(conditionCd));
		model.put("title2", "신고현황");

		model.put("type", "statuses");

		return "adm/appl/appl_view";
	}

	@RequestMapping(value = { "/sub/companies/{memberSeqNo}", "/trust/companies/{memberSeqNo}" }, method = RequestMethod.GET)
	public String companyDetail(HttpServletRequest request, @PathVariable String memberSeqNo, ModelMap model) throws Exception {
		LOGGER.info("member seq no [{}]", memberSeqNo);

		String conditionCd = getConditionCd(request);
		String statcd = memberDAO.selectStatCd(memberSeqNo);

		model.putAll(super.companyDetail2(memberSeqNo, conditionCd));

		model.put("title1", getTitle(conditionCd));
		model.put("title2", "업체현황");

		model.put("type", "companies");

		return "adm/appl/appl_view";
	}

	//	@RequestMapping(value = {"/sub/regist/new", "/trust/regist/new"}, method = RequestMethod.GET)
	//	public String registForm(HttpServletRequest request,
	//			ModelMap model) throws Exception {
	//
	//		String conditionCd = getConditionCd(request);
	//
	//		ApplicationForm appl = new ApplicationForm();
	//		appl.setConditionCd(conditionCd);
	//		appl.setStatCd("1");
	//
	//		model.addAttribute("member", appl);
	//		model.addAttribute("type", "1".equals(conditionCd) ? "sub_regist" : "trust_regist");
	//		model.put("title1", getTitle(conditionCd));
	//		model.put("title2", "1".equals(conditionCd) ? "신고업무" : "허가업무");
	//		
	//		return "adm/appl/appl_regist";
	//	}

	@RequestMapping(value = "/sub/regist", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String regist(HttpServletRequest request, @ModelAttribute ApplicationForm form, ModelMap model) throws Exception {

		String conditionCd = getConditionCd(request);

		form.setStatCd("1");

		new XCrypto(propertiesService.getString("xcry.path"), propertiesService.getBoolean("xcry.use"));

		Map<String, Object> map = CommonUtils.applicationToMember(form);
		MemberVO member = (MemberVO) map.get("member");
		List<WritingKindVO> writingKindList = (List<WritingKindVO>) map.get("writingKindList");
		List<PermKindVO> permKindList = (List<PermKindVO>) map.get("permKindList");

		/* save */
		try {
			applicationService.save(null, conditionCd, member, writingKindList, permKindList);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}

		return CommonUtils.makeSuccess();
	}

	private String getConditionCd(HttpServletRequest request) {
		return request.getRequestURI().indexOf("/sub/") > -1 ? "1" : "2";
	}

	private String getTitle(String conditionCd) {
		return "1".equals(conditionCd) ? "저작권중개업" : "저작권신탁관리업";
	}
}
