package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.DateUtils;
import cocoms.copyright.common.XCrypto;
import cocoms.copyright.entity.*;
import cocoms.copyright.log.ActionLogService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;
import cocoms.copyright.web.interceptor.HttpSessionBindingListenerLogin;

@Controller
public class LoginController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	/** ActionLogService */
	@Resource(name = "actionLogService")
	private ActionLogService actionLogService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	@RequestMapping(value = { "/login", "/login2" }, method = RequestMethod.GET)
	public String loginForm(@ModelAttribute UserForm form, ModelMap model) throws Exception {

		return "usr/login/login";
	}

	@RequestMapping(value = "/login/{key}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String loginCheck(HttpServletRequest request, LoginVO form, @PathVariable String key, ModelMap model) throws Exception {

		LoginVO login = new LoginVO();
		login.setLoginId(key);
		login.setPwd(form.getPwd());
		LOGGER.info("login [{}][{}]", login.getLoginId(), login.getPwd());
		//암호화 
		//String pwd = XCrypto.encHashPw(login.getPwd());
		try {
			login = userService.getUserByLoginId(login.getLoginId());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		LOGGER.info("loginId [{}]", login);
		if (login == null) {
			return CommonUtils.makeError("아이디가 없습니다.");
		} else if (login.getDelGubun().equals("Y")) {
			return CommonUtils.makeError("탈퇴한 회원입니다.");
		} /*
			 * else if (!login.getPwd().equals(pwd)){ return
			 * CommonUtils.makeError("패스워드가 다릅니다."); }
			 */

		return CommonUtils.makeSuccess();
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpServletRequest request, @ModelAttribute UserForm form, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);
		if (form.getLoginId() == null) {
			model.addAttribute("message", "비밀번호가 틀립니다.");
			return "usr/login/login";
		}
		//
		LoginVO login = new LoginVO();
		BeanUtils.copyProperties(form, login);
		try {
			login = userService.getUser(login);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		if (login == null) {
			model.addAttribute("message", "아이디 또는 비밀번호가 틀립니다.");
			return "usr/login/login";
		}

		//
		CertVO cert = new CertVO();
		BeanUtils.copyProperties(form, cert);
		try {
			cert = userService.getCert(cert);
			//			cert = userService.getCert(login.getLoginId());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		if (cert == null) {
			model.addAttribute("message", "등록된 인증서가 없습니다.");
			return "usr/login/login";
		}

		//
		MemberVO member = new MemberVO();
		try {

			member = userService.getMember(login.getMemberSeqNo());

			login.setLastLoginDate(DateUtils.getCurrDateNumber());
			login.setLastLoginTime(DateUtils.getCurrTimeNumber());
			userService.saveLogin(login);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		// session
		HttpSession session = request.getSession();
		session.invalidate();
		session = request.getSession(true);

		session.setAttribute("SESS_USER", login);
		session.setAttribute("SESS_MEMBER", member);
		session.setAttribute("SESS_CERT", cert);
		session.setAttribute("SESS_IP", request.getRemoteAddr());
		session.setMaxInactiveInterval(12000); // sec

		HttpSessionBindingListenerLogin listener = new HttpSessionBindingListenerLogin();
		request.getSession().setAttribute(login.getLoginId(), listener);
		// save log
		actionLogService.saveLog(request, "user login");

		return "redirect:/main";
	}

	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request, ModelMap model) throws Exception {

		// save log
		actionLogService.saveLog(request, "user logout");

		// session
		HttpSession session = request.getSession();
		session.invalidate();

		//		return "forward:/main";
		return "redirect:/main";
	}

	@RequestMapping(value = "/find/id", method = RequestMethod.GET)
	public String findIdForm(HttpServletRequest request, ModelMap model) throws Exception {

		return "usr/login/find_id";
	}

	@RequestMapping(value = "/find/id", method = RequestMethod.POST)
	public String findId(HttpServletRequest request, @ModelAttribute UserForm form, ModelMap model) throws Exception {
		LOGGER.info("form [{}]", form);

		List<LoginVO> list = new ArrayList<LoginVO>();
		try {

			String regNo = "1".equals(form.getCoGubunCd()) ? form.getCeoRegNo1() + form.getCeoRegNo2() : form.getCoNo1() + form.getCoNo2() + form.getCoNo3();
			LOGGER.info("reg no [{}]", regNo);

			list = userService.getUserByRegNo(regNo, form.getCertSn());
			LOGGER.info("user [{}]", list);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		if (list == null || list.size() == 0) {
			model.put("title", "아이디 찾기");
			return "usr/login/find_error";
		}

		model.put("list", list);

		return "usr/login/find_result";
	}

	@RequestMapping(value = "/find/password", method = RequestMethod.GET)
	public String findPasswordForm(HttpServletRequest request, ModelMap model) throws Exception {

		return "usr/login/find_pwd";
	}

	@RequestMapping(value = "/find/password", method = RequestMethod.POST)
	public String findPassword(HttpServletRequest request, @ModelAttribute UserForm form, ModelMap model) throws Exception {

		LoginVO user = new LoginVO();
		try {

			user = userService.getUserByLoginId(form.getLoginId(), form.getCertSn());
			LOGGER.info("user [{}]", user);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		if (user == null) {
			model.put("title", "비밀번호 찾기");
			return "usr/login/find_error";
		}
		model.put("data", form);
		model.put("user", user);
		HttpSession session = request.getSession();
		session.invalidate();
		session = request.getSession(true);

		session.setAttribute("SESS_SEQ", user.getMemberSeqNo());
		session.setAttribute("SESS_CERT", form.getCertSn());
		session.setAttribute("SESS_ID", form.getLoginId());
		session.setAttribute("SESS_IP", request.getRemoteAddr());
		return "usr/login/chg_pwd";
	}

	@RequestMapping(value = "/change/password", method = RequestMethod.POST)
	public String changePassword(HttpServletRequest request, @ModelAttribute UserForm form, ModelMap model) throws Exception {
		HttpSession session = request.getSession();
		try {

			String seq = (String) session.getAttribute("SESS_SEQ");
			String cert = (String) session.getAttribute("SESS_CERT");
			String id = (String) session.getAttribute("SESS_ID");
			String ip = (String) session.getAttribute("SESS_IP");
			if (!form.getMemberSeqNo().equals(seq) || !form.getCertSn().equals(cert) || !form.getLoginId().equals(id) || !request.getRemoteAddr().equals(ip)) {

				session.invalidate();

				return "redirect:/main";
			}
			LoginVO login = new LoginVO();
			login.setLoginId(form.getLoginId());
			login.setPwd(form.getPwd());

			userService.saveUser(form.getMemberSeqNo(), login, null, null);
			actionLogService.saveLog(request, "Change PWD");
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		model.put("chg", "1");
		session.invalidate();
		return "usr/login/login";
	}

}
