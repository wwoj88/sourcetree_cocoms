package cocoms.copyright.web.controllers;

import java.util.*;

import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springmodules.validation.commons.DefaultBeanValidator;


import cocoms.copyright.application.ApplicationService;
import cocoms.copyright.application.ChgHistoryMapper;
import cocoms.copyright.application.PermKindMapper;
import cocoms.copyright.application.RpMgmMapper;
import cocoms.copyright.application.WritingKindMapper;
import cocoms.copyright.code.CodeService;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.MemberMapper;
import cocoms.copyright.sms.SmsService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.web.form.*;

public class CommonController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	/** UserService */
	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "memberMapper")
	private MemberMapper memberDAO;

	@Resource(name = "writingKindMapper")
	private WritingKindMapper writingKindDAO;

	@Resource(name = "permKindMapper")
	private PermKindMapper permKindDAO;

	@Resource(name = "rpMgmMapper")
	private RpMgmMapper rpMgmDAO;

	@Resource(name = "chgHistoryMapper")
	private ChgHistoryMapper chgHistoryDAO;

	/** SmsService */
	@Resource(name = "smsService")
	private SmsService smsService;

	/** ApplicationService */
	@Resource(name = "applicationService")
	private ApplicationService applicationService;

	/** CodeService */
	@Resource(name = "codeService")
	private CodeService codeService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	protected void noti(String code, LoginVO login) throws Exception {

		MemberVO member = userService.getMember(login.getMemberSeqNo());

		noti(code, login, member);
	}

	protected void noti(String code, LoginVO login, MemberVO member) throws Exception {
		LOGGER.info("code [{}]", code);

		try {

			// noti
			smsService.noti(code, login, member);

		} catch (Exception e) {
			LOGGER.error("sms/email send error");
			e.printStackTrace();
		}
	}

	protected Map<String, Object> companyList(MemberVO searchVO, String type) throws Exception {

		return companyList(searchVO, type, true);
	}

	protected Map<String, Object> companyList(MemberVO searchVO, String type, boolean paging) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		/** pageing setting */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(searchVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(propertiesService.getInt("pageUnit"));
		paginationInfo.setPageSize(propertiesService.getInt("pageSize"));

		searchVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		searchVO.setLastIndex(paginationInfo.getLastRecordIndex());
		searchVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		//
		if ("1".equals(searchVO.getConditionCd()) && StringUtils.isEmpty(searchVO.getConDetailCd())) {
			searchVO.setConDetailCd("3");
		}

		//
		String appNo = searchVO.getAppNo();
		if (searchVO.getAppNo().indexOf("-") > -1) {
			String appNo1 = searchVO.getAppNo().split("-")[0];
			String appNo2 = searchVO.getAppNo().split("-")[1];

			searchVO.setAppNo(appNo1);
			searchVO.setAppNo2(appNo2);
		} else {
			searchVO.setAppNo2(searchVO.getAppNo());
			searchVO.setAppNo("");
		}

		//
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = searchVO.getSearchFromDate().replaceAll("-", "");
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = searchVO.getSearchToDate().replaceAll("-", "");
			searchVO.setSearchToDate(searchKeyword);
		}

		/** execute service */
		List<MemberVO> list = new ArrayList<MemberVO>();
		try {
			Map<String, Object> filter = CommonUtils.pojoToMap(searchVO);
			filter.putAll(CommonUtils.pojoToMap(paginationInfo));

			// 업체현황
			if ("type1".equals(type)) {
				filter.put("type", "TYPE1");
			}
			// 신고현황
			else {
				filter.put("type", "TYPE2");
			}

			if (!paging) {
				filter.put("firstRecordIndex", 0);
				filter.put("lastRecordIndex", 9999);
			}

			Map<String, Object> data = userService.getMembers(filter);
			list = (List<MemberVO>) data.get("list");
			int total = (int) data.get("total");
			String minDate = (String) data.get("minDate");

			paginationInfo.setTotalRecordCount(total);

			if (StringUtils.isEmpty(searchVO.getSearchFromDate())) {
				searchVO.setSearchFromDate(minDate);
			}
			if (StringUtils.isEmpty(searchVO.getSearchToDate())) {
				searchVO.setSearchToDate(DateUtils.getCurDate());
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		List<CommonCodeVO> codeList = new ArrayList<CommonCodeVO>();
		try {
			if ("1".equals(searchVO.getConditionCd())) {
				codeList = codeService.getCodes("DSTAT"); // 대리중개업무처리상태			
			} else {
				codeList = codeService.getCodes("SSTAT"); // 신탁업무처리상태
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		for (MemberVO item : list) {
			Map<String, Object> data = new HashMap<String, Object>();
			data = CommonUtils.pojoToMap(item);
			
			for (CommonCodeVO code : codeList) {
				if (code.getCodeValue().equals(item.getStatCd())) {
					data.put("statCdStr", code.getCodeValueDesc());
					break;
				}
			}

			data.putAll(getApplication(item.getMemberSeqNo(), item.getStatCd()));

			list2.add(data);
		}
		
		//
		searchVO.setAppNo(appNo);
		if (StringUtils.isNotEmpty(searchVO.getSearchFromDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchFromDate());
			searchVO.setSearchFromDate(searchKeyword);
		}
		if (StringUtils.isNotEmpty(searchVO.getSearchToDate())) {
			String searchKeyword = DateUtils.toDateFormat(searchVO.getSearchToDate());
			searchVO.setSearchToDate(searchKeyword);
		}

		/** make return value */
		map.put("data", searchVO);
		map.put("list", list2);
		map.put("page", paginationInfo.getCurrentPageNo());
		map.put("total", paginationInfo.getTotalRecordCount());
		map.put("paginationInfo", paginationInfo);
		map.put("start", paginationInfo.getTotalRecordCount() - (paginationInfo.getCurrentPageNo() - 1) * paginationInfo.getRecordCountPerPage());

		map.put("codeList", codeList);

		return map;
	}

	protected Map<String, Object> companyDetail(String key, String conditionCd, String docType) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		/** execute service */
		MemberVO member = new MemberVO();
		List<ChgWritingKindVO> writingKindList = new ArrayList<ChgWritingKindVO>();
		List<ChgPermKindVO> permKindList = new ArrayList<ChgPermKindVO>();
		List<ChgWritingKindVO> writingKindList2 = new ArrayList<ChgWritingKindVO>();
		List<ChgPermKindVO> permKindList2 = new ArrayList<ChgPermKindVO>();

		LoginVO login = new LoginVO();
		ChgHistoryVO ChgHistory = new ChgHistoryVO();
		RpMgmVO rpmgm = new RpMgmVO();
		String statcd = memberDAO.selectStatCd(key);

		try {

			if (!("2").equals(docType) && !("4").equals(docType) && !("6").equals(docType) && !("8").equals(docType)) {

				Map<String, Object> data = applicationService.getChgAppliation(key, conditionCd, docType); //
				//ChgHistory = (ChgHistoryVO) data.get("member");
				member = (MemberVO) data.get("member");
				//System.out.println("MEMEMEMEMEM");
				//LOGGER.info("MEMEMEMEMEM [{}]", data);
				
				writingKindList = (List<ChgWritingKindVO>) data.get("writingKindList");
				writingKindList2 = (List<ChgWritingKindVO>) data.get("writingKindList2");

				permKindList = (List<ChgPermKindVO>) data.get("permKindList");
				permKindList2 = (List<ChgPermKindVO>) data.get("permKindList2");

			} else {

				Map<String, Object> data = applicationService.getAppliation(key, conditionCd);
				member = (MemberVO) data.get("member");
				writingKindList = (List<ChgWritingKindVO>) data.get("writingKindList");
				permKindList = (List<ChgPermKindVO>) data.get("permKindList");

			}

			// TODO 1:N 문제
			login.setMemberSeqNo(member.getMemberSeqNo());
			login = userService.getUser(login);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		LOGGER.info("membe22r [{}]", ChgHistory);

		CommonCodeVO code = new CommonCodeVO();
		CommonCodeVO code2 = new CommonCodeVO();

		/*
		 * if( !("2").equals(docType) && !("4").equals(docType) &&
		 * !("6").equals(docType) && !("8").equals(docType)){
		 * 
		 * try {
		 * 
		 * if ("1".equals(member.getConditionCd())) { code =
		 * codeService.getCode("DSTAT", ChgHistory.getStatCd()); code2 =
		 * codeService.getCode("CONDETAIL", ChgHistory.getConDetailCd());
		 * 
		 * } else { code = codeService.getCode("SSTAT", ChgHistory.getStatCd());
		 * code2 = codeService.getCode("CONDITION",
		 * ChgHistory.getConditioncd()); } } catch (Exception e) {
		 * e.printStackTrace(); LOGGER.error(e.getMessage()); throw e; }
		 * ApplicationForm appl =
		 * CommonUtils.ChgHistoryToApplication(ChgHistory);
		 * appl.setMemberSeqNo(key);
		 * 
		 * appl.setBubNo(appl.getBubNo1()+appl.getBubNo2()); Map<String, Object>
		 * data = CommonUtils.pojoToMap(appl); data.put("statCdStr",
		 * code.getCodeValueDesc()); data.put("conditionCdStr",
		 * code2.getCodeValueDesc()); data.putAll(getApplication(key,
		 * ChgHistory.getStatCd())); data.put("loginId", login.getLoginId());
		 * 
		 * 
		 *//** make return value *//*
									 * map.put("data", data);
									 * map.put("writingKindList",
									 * writingKindList); map.put("permKindList",
									 * permKindList); map.put("permKindList2",
									 * permKindList2);
									 * map.put("writingKindList2",
									 * writingKindList2);
									 * 
									 * return map; }else{
									 */
		try {
			if ("1".equals(member.getConditionCd())) {
				code = codeService.getCode("DSTAT", member.getStatCd());
				code2 = codeService.getCode("CONDETAIL", member.getConDetailCd());
			} else {
				code = codeService.getCode("SSTAT", member.getStatCd());
				code2 = codeService.getCode("CONDITION", member.getConditionCd());
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		ApplicationForm appl = CommonUtils.memberToApplication(member);
		Map<String, Object> data = CommonUtils.pojoToMap(appl);
		data.put("statCdStr", code.getCodeValueDesc());
		data.put("conditionCdStr", code2.getCodeValueDesc());
		data.putAll(getApplication(member.getMemberSeqNo(), member.getStatCd()));
		data.put("loginId", login.getLoginId());
		LOGGER.info("map [{}]", data);

		/** make return value */
		map.put("data", data);
		map.put("writingKindList", writingKindList);
		map.put("writingKindList2", writingKindList2);
		map.put("permKindList", permKindList);
		map.put("permKindList2", permKindList2);

		return map;
	}

	protected Map<String, Object> companyDetail2(String key, String conditionCd) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		/** execute service */
		MemberVO member = new MemberVO();
		List<ChgWritingKindVO> writingKindList = new ArrayList<ChgWritingKindVO>();
		List<ChgPermKindVO> permKindList = new ArrayList<ChgPermKindVO>();

		LoginVO login = new LoginVO();
		ChgHistoryVO ChgHistory = new ChgHistoryVO();
		RpMgmVO rpmgm = new RpMgmVO();

		try {

			Map<String, Object> data = applicationService.getAppliation(key, conditionCd);
			member = (MemberVO) data.get("member");
			writingKindList = (List<ChgWritingKindVO>) data.get("writingKindList");
			permKindList = (List<ChgPermKindVO>) data.get("permKindList");

			// TODO 1:N 문제
			login.setMemberSeqNo(member.getMemberSeqNo());
			login = userService.getUser(login);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		LOGGER.info("member [{}]", ChgHistory);

		CommonCodeVO code = new CommonCodeVO();
		CommonCodeVO code2 = new CommonCodeVO();
		try {
			if ("1".equals(member.getConditionCd())) {
				code = codeService.getCode("DSTAT", member.getStatCd());
				code2 = codeService.getCode("CONDETAIL", member.getConDetailCd());
			} else {
				code = codeService.getCode("SSTAT", member.getStatCd());
				code2 = codeService.getCode("CONDITION", member.getConditionCd());
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		ApplicationForm appl = CommonUtils.memberToApplication(member);
		appl.setMemberSeqNo(key);
		Map<String, Object> data = CommonUtils.pojoToMap(appl);
		data.put("statCdStr", code.getCodeValueDesc());
		data.put("conditionCdStr", code2.getCodeValueDesc());
		data.putAll(getApplication(member.getMemberSeqNo(), ChgHistory.getStatCd()));
		data.put("loginId", login.getLoginId());
		LOGGER.info("map242 [{}]", data);

		/** make return value */
		map.put("data", data);
		map.put("writingKindList", writingKindList);
		map.put("permKindList", permKindList);

		return map;
	}

	private Map<String, Object> getApplication(String memberSeqNo, String statCd) {

		String preStatCd = "";
		try {
			preStatCd = userService.getPreStatCd(memberSeqNo);
		} catch (Exception e) {
		}

		return CommonUtils.getApplication(memberSeqNo, statCd, preStatCd);
	}

	protected String saveCompanyStatCd(MemberVO form, String key) throws Exception {
		LOGGER.info("member [{}][{}]", key, form.getStatCd());

		try {
			userService.saveStatCd(form);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			//			throw e;
			return CommonUtils.makeError(e.getMessage());
		}

		return CommonUtils.makeSuccess();
	}

	protected Map<String, Object> getDoc1(String memberSeqNo, String conditionCd) throws Exception {
		Map<String, Object> ret = new HashMap<String, Object>();

		String statCd = "1"; // 1: 신고
		String docType = "1"; // 신고서
		String preStatCd = "";

		ret.put("type", docType); // TODO check

		try {

			Map<String, Object> data = companyDetail(memberSeqNo, conditionCd, docType);
			List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) data.get("permKindList");
			List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) data.get("writingKindList");
			ret.putAll(data);
			ret.put("permKind", CommonUtils.getChgPermKindStr(permKindList));
			ret.put("writingKind", CommonUtils.getChgWritingKindStr(writingKindList));
			ret.put("permKindList3", permKindList);
			ret.put("writingKindList3", writingKindList);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		try {

			Map<String, Object> map = applicationService.getChgHistory(memberSeqNo, statCd);
			ChgHistoryVO chgHistory = (ChgHistoryVO) map.get("chgHistory");

			List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) map.get("permKindList");

			//암호화
			String ceoRegNo = chgHistory.getPostCeoRegNo();
			String year = "";
			String birthday = "";
			String ceoRegNo1 = "";
			String ceoRegNo2 = "";
			//System.out.println("TEST:REG :::: "+ceoRegNo);
			if (ceoRegNo != null && !ceoRegNo.equals("")) {
				chgHistory.setPostCeoRegNo(XCrypto.decPatternReg(chgHistory.getPostCeoRegNo()));
				ceoRegNo = chgHistory.getPostCeoRegNo();
				//System.out.println("TEST:REG2 :::: "+ceoRegNo);
				ceoRegNo1 = ceoRegNo.substring(0, 6);
				ceoRegNo2 = ceoRegNo.substring(6, 12);
				year = "1,2,5,6,".indexOf(ceoRegNo.substring(6, 7)) > -1 ? "19" : "20";
				birthday = CommonUtils.toFormat(year + ceoRegNo.substring(0, 6), "####.##.##");
			}

			LOGGER.info("birthday [{}]", birthday);
			preStatCd = userService.getPreStatCd(memberSeqNo);
			MemberVO MemStatCd = userService.getMember(memberSeqNo);
			statCd = MemStatCd.getStatCd();

			// rights etc
			String rightsEtc = "";
			List<String> list = new ArrayList<String>();
			for (ChgPermKindVO item : permKindList) {
				String permEtc = item.getPermEtc();
				if (StringUtils.isNotEmpty(permEtc) && !list.contains(permEtc)) {
					list.add(permEtc);
				}
			}
			for (String item : list) {
				rightsEtc += ", " + item;
			}
			if (list.size() > 0)
				rightsEtc = rightsEtc.substring(rightsEtc.length());

			ret.putAll(map);
			ret.put("rightsEtc", rightsEtc);
			ret.put("ceoRegNo1", ceoRegNo1);
			ret.put("ceoRegNo2", ceoRegNo2);
			ret.put("birthday", birthday);
			ret.put("preStatCd", preStatCd);

			ret.put("statCd", statCd);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		return ret;
	}

	protected Map<String, Object> getDoc2(String memberSeqNo, String conditionCd) throws Exception {
		Map<String, Object> ret = new HashMap<String, Object>();

		String statCd = "2";
		String docType = "2"; // 신고증
		String birthday = "";
		Map<String, Object> map = companyDetail(memberSeqNo, conditionCd, docType);
		Map<String, Object> data = (Map<String, Object>) map.get("data");
		List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) map.get("writingKindList");
		List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) map.get("permKindList");

		List<RpMgmVO> rpMgmList = new ArrayList<RpMgmVO>();
		try {

			List<RpMgmVO> rpMgmList2 = applicationService.getRpMgms(memberSeqNo);
			for (RpMgmVO item : rpMgmList2) {
				if (StringUtils.isNotEmpty(item.getInterFilename()) && StringUtils.isNotEmpty(item.getInterFilepath())) {
					rpMgmList.add(item);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		String ceoRegNo = (String) data.get("ceoRegNo");
		if (ceoRegNo != null) {
			if (ceoRegNo.length() > 5) {
				String year = "1,2,5,6,".indexOf(ceoRegNo.substring(6, 7)) > -1 ? "19" : "20";
				birthday = CommonUtils.toFormat(year + ceoRegNo.substring(0, 6), "####.##.##");
			}
		}
		ret.putAll(map);
		ret.put("type", docType);
		ret.put("birthday", birthday);
		ret.put("printId", CommonUtils.getCryptSerial());

		ret.put("writingKind", CommonUtils.getChgWritingKindStr(writingKindList));
		ret.put("permKind", CommonUtils.getChgPermKindStr(permKindList));

		ret.put("appRegDate", CommonUtils.toFormat((String) data.get("appRegDate"), "####년 ##월 ##일"));

		ret.put("rpMgmList", rpMgmList);

		return ret;
	}

	protected Map<String, Object> getDoc3(String memberSeqNo, String conditionCd) throws Exception {
		Map<String, Object> ret = new HashMap<String, Object>();

		String statCd = "3"; // 
		String docType = "3"; // 변경신고서
		String preStatCd = "";
		Map<String, Object> map = companyDetail(memberSeqNo, conditionCd, docType);
		Map<String, Object> data = (Map<String, Object>) map.get("data");
		List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) map.get("writingKindList");
		List<ChgWritingKindVO> writingKindList2 = (List<ChgWritingKindVO>) map.get("writingKindList2");

		List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) map.get("permKindList");
		List<ChgPermKindVO> permKindList2 = (List<ChgPermKindVO>) map.get("permKindList2");

		String regDate = "";
		String birthday = "";
		String before = "";
		String after = "";
		try {

			Map<String, Object> map2 = applicationService.getChgHistory(memberSeqNo, statCd);

			RpMgmVO rpMgm = (RpMgmVO) map2.get("rpMgm");

			ChgHistoryVO chgHistory = (ChgHistoryVO) map2.get("chgHistory");

			List<String> chgCdList = (List<String>) map2.get("chgCdList");

			List<ChgHistoryFileVO> chgHistoryFileList = (List<ChgHistoryFileVO>) map2.get("chgHistoryFileList");

			List<ChgWritingKindVO> prewritingKindList = (List<ChgWritingKindVO>) map2.get("prewritingKindList");
			List<ChgPermKindVO> prepermKindList = (List<ChgPermKindVO>) map2.get("prepermKindList");

			ret.put("rpMgm", rpMgm);
			ret.put("chgHistory", chgHistory);
			ret.put("chgHistoryFileList", chgHistoryFileList);

			String ceoRegNo = (String) data.get("ceoRegNo");
		
			if (ceoRegNo != null) {
				if (ceoRegNo.length() > 5) {
					String year = "1,2,5,6,".indexOf(ceoRegNo.substring(6, 7)) > -1 ? "19" : "20";
					birthday = CommonUtils.toFormat(year + ceoRegNo.substring(0, 6), "####.##.##");
				}
			}
			if (rpMgm.getRegDate() != null) {
				regDate = CommonUtils.toFormat(rpMgm.getRegDate(), "####년 ##월 ##일");
			}
			for (String item : chgCdList) {
				if ("1".equals(item)) {
					//암호화
					String chgHistoryRegno = XCrypto.decPatternReg(chgHistory.getPreCeoRegNo());
					String postchgHistoryRegno = XCrypto.decPatternReg(chgHistory.getPostCeoRegNo());
					/*String regreg = XCrypto.encPatternReg("4102101025519");*/
					
					before += "\n대표자이름: " + chgHistory.getPreCeoName();
					
					before += "\n대표자주민등록번호: " + chgHistoryRegno.substring(0, 6) + "-" + chgHistoryRegno.substring(6, 13);
					before += "\n대표자이메일주소: " + chgHistory.getPreCeoEmail();
					before += "\n대표자주소: " + chgHistory.getPreCeoZipcode() + " " + chgHistory.getPreCeoSido() + " " + chgHistory.getPreCeoGugun() + " " + chgHistory.getPreCeoDong() + " " + chgHistory.getPreCeoBunji() + " "
							+ chgHistory.getPreCeoDetailAddr();
					before += "\n대표자전화번호: " + chgHistory.getPreCeoTel();
					before += "\n대표자팩스번호: " + chgHistory.getPreCeoFax();
					//암호화
					after += "\n대표자이름: " + chgHistory.getPostCeoName();
					after += "\n대표자주민등록번호: " + postchgHistoryRegno.substring(0, 6) + "-" + postchgHistoryRegno.substring(6, 13);
					after += "\n대표자이메일주소: " + chgHistory.getPostCeoEmail();
					after += "\n대표자주소: " + chgHistory.getPostCeoZipcode() + " " + chgHistory.getPostCeoSido() + " " + chgHistory.getPostCeoGugun() + " " + chgHistory.getPostCeoDong() + " " + chgHistory.getPostCeoBunji() + " "
							+ chgHistory.getPostCeoDetailAddr();
					after += "\n대표자전화번호: " + chgHistory.getPostCeoTel();
					after += "\n대표자팩스번호: " + chgHistory.getPostCeoFax();

				} else if ("2".equals(item)) {
					before += "\n업체명: " + chgHistory.getPreCoName();

					after += "\n업체명: " + chgHistory.getPostCoName();
				} else if ("3".equals(item)) {
					before += "\n단체주소: " + chgHistory.getPreTrustZipcode() + " " + chgHistory.getPreTrustSido() + " " + chgHistory.getPreTrustGugun() + " " + chgHistory.getPreTrustDong() + " " + chgHistory.getPreTrustBunji() + " "
							+ chgHistory.getPreTrustDetailAddr();
					before += "\n단체전화번호: " + chgHistory.getPreTrustTel();

					after += "\n단체주소: " + chgHistory.getPostTrustZipcode() + " " + chgHistory.getPostTrustSido() + " " + chgHistory.getPostTrustGugun() + " " + chgHistory.getPostTrustDong() + " " + chgHistory.getPostTrustBunji() + " "
							+ chgHistory.getPostTrustDetailAddr();
					after += "\n단체전화번호: " + chgHistory.getPostTrustTel();

				} else if ("6".equals(item)) {

					String postwritingKind = "", postpermKind = "", prewritingKind = "", prepermKind = "";
					List<String> postpermKindList = new ArrayList<String>();
					List<String> prepermKindList2 = new ArrayList<String>();

					try {

						for (CommonCodeVO item2 : codeService.getCodes("WRITINGKIND")) {
							for (ChgWritingKindVO item3 : writingKindList) {
								if (item3.getWritingKind().equals(item2.getCodeValue())) {
									postwritingKind += ", " + item2.getCodeValueDesc();
								}
							}
						}

						if (StringUtils.isNotBlank(postwritingKind))
							postwritingKind = postwritingKind.substring(2);

						for (CommonCodeVO item2 : codeService.getCodes("PERMKIND")) {
							for (ChgPermKindVO item3 : permKindList) {
								if (item3.getPermKind().equals(item2.getCodeValue())) {

									String str = item2.getCodeValueDesc();

									if (!postpermKindList.contains(str)) {
										postpermKindList.add(str);
										postpermKind += ", " + str;

									}
								}
							}
						}
						if (StringUtils.isNotBlank(postpermKind))
							postpermKind = postpermKind.substring(2);

						for (CommonCodeVO item2 : codeService.getCodes("WRITINGKIND")) {
							for (ChgWritingKindVO item3 : writingKindList2) {
								if (item3.getWritingKind().equals(item2.getCodeValue())) {
									prewritingKind += ", " + item2.getCodeValueDesc();
								}
							}
						}

						if (StringUtils.isNotBlank(prewritingKind))
							prewritingKind = prewritingKind.substring(2);

						for (CommonCodeVO item2 : codeService.getCodes("PERMKIND")) {
							for (ChgPermKindVO item3 : permKindList2) {
								if (item3.getPermKind().equals(item2.getCodeValue())) {

									String str = item2.getCodeValueDesc();

									if (!prepermKindList.contains(str)) {

										prepermKindList2.add(str);
										prepermKind += ", " + str;

									}
								}
							}
						}
						if (StringUtils.isNotBlank(prepermKind))
							prepermKind = prepermKind.substring(2);
						prepermKind = "취급하고자 하는 권리:" + prepermKind;
						prewritingKind = "취급하고자 하는 저작물등의 종류:" + prewritingKind + "\n";
						postwritingKind = "취급하고자 하는 저작물등의 종류:" + postwritingKind + "\n";
						postpermKind = "취급하고자 하는 권리:" + postpermKind;
						/*
						 * ret.put("prePermKind",
						 * prepermKind.replaceAll(" ",""));
						 * ret.put("preWritingKind",
						 * prewritingKind.replaceAll(" ",""));
						 * ret.put("WritingKind",
						 * postwritingKind.replaceAll(" ",""));
						 * ret.put("PermKind", postpermKind.replaceAll(" ",""));
						 */

						ret.put("prePermKind", prepermKind);
						ret.put("preWritingKind", prewritingKind);
						ret.put("WritingKind", postwritingKind);
						ret.put("PermKind", postpermKind);

					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.error(e.getMessage());
						throw e;
					}
				} else if ("7".equals(item)) {

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}
		preStatCd = userService.getPreStatCd(memberSeqNo);
		MemberVO MemStatCd = userService.getMember(memberSeqNo);
		statCd = MemStatCd.getStatCd();
		ret.put("preStatCd", preStatCd);
		ret.put("statCd", statCd);
		ret.putAll(map);
		ret.put("type", docType);

		ret.put("regDate", regDate);
		ret.put("birthday", birthday);
		/*
		 * ret.put("before", before.replaceAll("<BR/>", "")); ret.put("after",
		 * after.replaceAll("<BR/>", ""));
		 */
		ret.put("before", before);
		ret.put("after", after);

		return ret;
	}

	protected Map<String, Object> getDoc4(String memberSeqNo, String conditionCd) throws Exception {
		Map<String, Object> ret = new HashMap<String, Object>();

		String statCd = "3"; // 
		String docType = "4"; // 변경신고증

		Map<String, Object> map = companyDetail(memberSeqNo, conditionCd, docType);

		Map<String, Object> data = (Map<String, Object>) map.get("data");
		List<ChgWritingKindVO> writingKindList = (List<ChgWritingKindVO>) map.get("writingKindList");
		List<ChgPermKindVO> permKindList = (List<ChgPermKindVO>) map.get("permKindList");

		String birthday = "";
		String writingKind = "";
		{
			List<String> list = new ArrayList<String>();
			for (ChgWritingKindVO item : writingKindList) {
				String str = item.getWritingKindStr();

				if (StringUtils.isNotEmpty(str) && !list.contains(str)) {
					list.add(str);
				}
			}
			for (String item : list) {
				writingKind += ", " + item;
			}
			if (list.size() > 0)
				writingKind = writingKind.substring(2);
		}

		String permKind = "";
		{
			List<String> list = new ArrayList<String>();
			for (ChgPermKindVO item : permKindList) {
				String str = "";

				if (item.getPermEtc() == null || ("").equals(item.getPermEtc())) {
					str = item.getPermKindStr();

				} else {
					str = item.getPermKindStr() + "(" + item.getPermEtc() + ")";
				}
				if (StringUtils.isNotEmpty(str) && !list.contains(str)) {
					list.add(str);
				}
			}
			for (String item : list) {
				permKind += ", " + item;
			}
			if (list.size() > 0)
				permKind = permKind.substring(2);
		}

		List<RpMgmVO> rpMgmList = new ArrayList<RpMgmVO>();
		try {

			List<RpMgmVO> rpMgmList2 = applicationService.getRpMgms(memberSeqNo);
			for (RpMgmVO item : rpMgmList2) {
				if (StringUtils.isNotEmpty(item.getInterFilename()) && StringUtils.isNotEmpty(item.getInterFilepath())) {
					rpMgmList.add(item);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		List<String> changeDateList = new ArrayList<String>();
		List<String> contentsList = new ArrayList<String>();
		List<Map<String, Object>> changeList = new ArrayList<Map<String, Object>>();
		List<ChgListVO> chgList = new ArrayList<ChgListVO>();
		try {

			List<Map<String, Object>> list = applicationService.getChgHistories(memberSeqNo, statCd);

			for (Map<String, Object> item : list) {
				RpMgmVO rpMgm = (RpMgmVO) item.get("rpMgm");
				ChgHistoryVO chgHistory = (ChgHistoryVO) item.get("chgHistory");
				List<String> chgCdList = (List<String>) item.get("chgCdList");

				String after = "";
				String before = "";
				String date = CommonUtils.toFormat(rpMgm.getRegDate(), "####.##.##");

				if (chgHistory.getPostCeoRegNo() != null) {
					if (chgHistory.getPostCeoRegNo().length() > 5) {
						String year = "1,2,5,6,".indexOf(chgHistory.getPostCeoRegNo().substring(6, 7)) > -1 ? "19" : "20";
						birthday = CommonUtils.toFormat(year + chgHistory.getPostCeoRegNo().substring(0, 6), "####.##.##");
					}
				} else {
					if (chgHistory.getPreCeoRegNo().length() > 5) {
						String year = "1,2,5,6,".indexOf(chgHistory.getPreCeoRegNo().substring(6, 7)) > -1 ? "19" : "20";
						birthday = CommonUtils.toFormat(year + chgHistory.getPreCeoRegNo().substring(0, 6), "####.##.##");
					}
				}

				String afterstr = "";
				if (date == null || date.isEmpty() || date.equals("")) {
					continue;
				}
				for (String chgCd : chgCdList) {
					ChgListVO chglistrow = new ChgListVO();
					chglistrow.setStrPre2("");
					chglistrow.setStrPre3("");
					chglistrow.setStrPre4("");
					chglistrow.setStrPre5("");
					chglistrow.setStrPre6("");
					chglistrow.setStrPre7("");
					chglistrow.setStrPre8("");
					chglistrow.setStrPre9("");
					chglistrow.setStrPre10("");
					chglistrow.setStrPre11("");
					chglistrow.setStrPre12("");
					chglistrow.setStrPre13("");
					chglistrow.setStrPre14("");
					chglistrow.setStrPre15("");
					chglistrow.setStrPost2("");
					chglistrow.setStrPost3("");
					chglistrow.setStrPost4("");
					chglistrow.setStrPost5("");
					chglistrow.setStrPost6("");
					chglistrow.setStrPost7("");
					chglistrow.setStrPost8("");
					chglistrow.setStrPost9("");
					chglistrow.setStrPost10("");
					chglistrow.setStrPost11("");
					chglistrow.setStrPost12("");
					chglistrow.setStrPost13("");
					chglistrow.setStrPost14("");
					chglistrow.setStrPost15("");
					afterstr = "";
					if ("1".equals(chgCd)) {

						// 암호화
						String regNo = XCrypto.decPatternReg(chgHistory.getPostCeoRegNo());
						chgHistory.setPostCeoRegNo(regNo);

						String regNo2 = XCrypto.decPatternReg(chgHistory.getPreCeoRegNo());
						chgHistory.setPreCeoRegNo(regNo2);

						chglistrow.setStrPre1(date);
						chglistrow.setStrPre2("대표자이름: " + chgHistory.getPreCeoName());
						chglistrow.setStrPre3("대표자주민등록번호: " + chgHistory.getPreCeoRegNo().substring(0, 6) + "-" + chgHistory.getPreCeoRegNo().substring(6, 13));
						chglistrow.setStrPre4("대표자이메일주소: " + chgHistory.getPreCeoEmail());
						chglistrow.setStrPre5("대표자주소: " + chgHistory.getPreCeoZipcode() + " " + chgHistory.getPreCeoSido() + " " + chgHistory.getPreCeoGugun() + " " + chgHistory.getPreCeoDong() + " " + chgHistory.getPreCeoBunji() + " "
								+ chgHistory.getPreCeoDetailAddr());
						chglistrow.setStrPre6("대표자전화번호: " + chgHistory.getPreCeoTel());
						chglistrow.setStrPre7("대표자팩스번호: " + chgHistory.getPreCeoFax());

						chglistrow.setStrPost1(date);
						chglistrow.setStrPost2("대표자이름: " + chgHistory.getPostCeoName());
						chglistrow.setStrPost3("대표자주민등록번호: " + chgHistory.getPostCeoRegNo().substring(0, 6) + "-" + chgHistory.getPostCeoRegNo().substring(6, 13));
						chglistrow.setStrPost4("대표자이메일주소: " + chgHistory.getPostCeoEmail());
						chglistrow.setStrPost5("대표자주소: " + chgHistory.getPostCeoZipcode() + " " + chgHistory.getPostCeoSido() + " " + chgHistory.getPostCeoGugun() + " " + chgHistory.getPostCeoDong() + " " + chgHistory.getPostCeoBunji()
								+ " " + chgHistory.getPostCeoDetailAddr());
						chglistrow.setStrPost6("대표자전화번호: " + chgHistory.getPostCeoTel());
						chglistrow.setStrPost7("대표자팩스번호: " + chgHistory.getPostCeoFax());

						afterstr = "대표자이름: " + chgHistory.getPostCeoName() + "\n대표자주민등록번호: " + chgHistory.getPostCeoRegNo().substring(0, 6) + "-" + chgHistory.getPostCeoRegNo().substring(6, 13) + "\n대표자이메일주소: "
								+ chgHistory.getPostCeoEmail() + "\n대표자주소: " + chgHistory.getPostCeoZipcode() + " " + chgHistory.getPostCeoSido() + " " + chgHistory.getPostCeoGugun() + " " + chgHistory.getPostCeoDong() + " "
								+ chgHistory.getPostCeoBunji() + " " + chgHistory.getPostCeoDetailAddr() + "\n대표자전화번호: " + chgHistory.getPostCeoTel() + "\n대표자팩스번호: " + chgHistory.getPostCeoFax();

					} else if ("2".equals(chgCd)) {

						chglistrow.setStrPre1(date);
						chglistrow.setStrPre2("업체명: " + chgHistory.getPreCoName());

						chglistrow.setStrPost1(date);
						chglistrow.setStrPost2("업체명: " + chgHistory.getPostCoName());

						afterstr = "업체명: " + chgHistory.getPostCoName();
					} else if ("3".equals(chgCd)) {

						chglistrow.setStrPre1(date);
						chglistrow.setStrPre2("\n단체주소: " + chgHistory.getPreTrustZipcode() + " " + chgHistory.getPreTrustSido() + " " + chgHistory.getPreTrustGugun() + " " + chgHistory.getPreTrustDong() + " " + chgHistory.getPreTrustBunji()
								+ " " + chgHistory.getPreTrustDetailAddr());
						chglistrow.setStrPre3("\n단체전화번호: " + chgHistory.getPreTrustTel());
						chglistrow.setStrPre4("\n단체팩스번호: " + chgHistory.getPreTrustFax());

						chglistrow.setStrPost1(date);
						chglistrow.setStrPost2("\n단체주소: " + chgHistory.getPostTrustZipcode() + " " + chgHistory.getPostTrustSido() + " " + chgHistory.getPostTrustGugun() + " " + chgHistory.getPostTrustDong() + " "
								+ chgHistory.getPostTrustBunji() + " " + chgHistory.getPostTrustDetailAddr());
						chglistrow.setStrPost3("\n단체전화번호: " + chgHistory.getPostTrustTel());
						chglistrow.setStrPost4("\n단체팩스번호: " + chgHistory.getPostTrustFax());
						afterstr = "단체주소: " + chgHistory.getPostTrustZipcode() + " " + chgHistory.getPostTrustSido() + " " + chgHistory.getPostTrustGugun() + " " + chgHistory.getPostTrustDong() + " " + chgHistory.getPostTrustBunji() + " "
								+ chgHistory.getPostTrustDetailAddr() + "\n단체전화번호: " + chgHistory.getPostTrustTel() + "\n단체팩스번호: " + chgHistory.getPostTrustFax();
					} else if ("5".equals(chgCd)) {
					} else if ("6".equals(chgCd)) {
						String chgHistorysedno = chgHistory.getChgHistorySeqNo();

						List<ChgPermKindVO> ChgWritingList1 = (List<ChgPermKindVO>) permKindDAO.selectListPreChg(chgHistorysedno);
						List<ChgPermKindVO> ChgWritingList2 = (List<ChgPermKindVO>) permKindDAO.selectListPostChg(chgHistorysedno);

						chglistrow.setStrPre1(date);

						int i = 2;
						for (ChgPermKindVO preList : ChgWritingList1) {

							switch (i) {
							case 2:
								chglistrow.setStrPre2(preList.getPermKind());
								break;
							case 3:
								chglistrow.setStrPre3(preList.getPermKind());
								break;
							case 4:
								chglistrow.setStrPre4(preList.getPermKind());
								break;
							case 5:
								chglistrow.setStrPre5(preList.getPermKind());
								break;
							case 6:
								chglistrow.setStrPre6(preList.getPermKind());
								break;
							case 7:
								chglistrow.setStrPre7(preList.getPermKind());
								break;
							case 8:
								chglistrow.setStrPre8(preList.getPermKind());
								break;
							case 9:
								chglistrow.setStrPre9(preList.getPermKind());
								break;
							case 10:
								chglistrow.setStrPre10(preList.getPermKind());
								break;
							case 11:
								chglistrow.setStrPre11(preList.getPermKind());
								break;
							case 12:
								chglistrow.setStrPre12(preList.getPermKind());
								break;
							case 13:
								chglistrow.setStrPre13(preList.getPermKind());
								break;
							case 14:
								chglistrow.setStrPre14(preList.getPermKind());
								break;
							case 15:
								chglistrow.setStrPre15(preList.getPermKind());
								break;

							}
							i++;

						}

						chglistrow.setStrPost1(date);
						i = 2;
						for (ChgPermKindVO postList : ChgWritingList2) {

							switch (i) {
							case 2:
								chglistrow.setStrPost2(postList.getPermKind());

								break;
							case 3:
								chglistrow.setStrPost3(postList.getPermKind());

								break;
							case 4:
								chglistrow.setStrPost4(postList.getPermKind());

								break;
							case 5:
								chglistrow.setStrPost5(postList.getPermKind());

								break;
							case 6:
								chglistrow.setStrPost6(postList.getPermKind());

								break;
							case 7:
								chglistrow.setStrPost7(postList.getPermKind());

								break;
							case 8:
								chglistrow.setStrPost8(postList.getPermKind());

								break;
							case 9:
								chglistrow.setStrPost9(postList.getPermKind());

								break;
							case 10:
								chglistrow.setStrPost10(postList.getPermKind());

								break;
							case 11:
								chglistrow.setStrPost11(postList.getPermKind());

								break;
							case 12:
								chglistrow.setStrPost12(postList.getPermKind());

								break;
							case 13:
								chglistrow.setStrPost13(postList.getPermKind());

								break;
							case 14:
								chglistrow.setStrPost14(postList.getPermKind());

								break;
							case 15:
								chglistrow.setStrPost15(postList.getPermKind());

								break;

							}
							if (i != 2) {
								afterstr += "\n" + postList.getPermKind();
							} else {
								afterstr += postList.getPermKind();
							}

							i++;
						}

					} else if ("7".equals(chgCd)) {
					}

					if (!"7".equals(chgCd)) {
						chgList.add(chglistrow);
						contentsList.add(afterstr);
						changeDateList.add(date);
					}
				}

				String content = afterstr;

				/* contentsList.add(chglistrow.); */

				Map<String, Object> map2 = new HashMap<String, Object>();
				/*
				 * map2.put("date", date); map2.put("content", content);
				 * changeList.add(map2);
				 */
				//				map2.put("chgList",PrechgList);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			throw e;
		}

		ret.putAll(map);
		ret.put("type", docType);
		ret.put("chgList", chgList);
		ret.put("writingKind", writingKind);
		ret.put("permKind", permKind);
		ret.put("birthday", birthday);
		ret.put("changeDateList", changeDateList);
		ret.put("contentsList", contentsList);
		ret.put("changeList", changeList);

		ret.put("appRegDate", CommonUtils.toFormat((String) data.get("appRegDate"), "####년 ##월 ##일"));

		ret.put("rpMgmList", rpMgmList);

		return ret;
	}
	


}
