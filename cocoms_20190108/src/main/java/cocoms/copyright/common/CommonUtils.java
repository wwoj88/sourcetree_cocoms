package cocoms.copyright.common;

import java.io.*;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpEntity;

import com.cabsoftware.Utils.Crypt;
import com.softforum.xdbe.xCrypto;

import cocoms.copyright.entity.*;
import cocoms.copyright.web.form.*;
import crosscert.Hash;

public class CommonUtils {

	private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

	private static boolean clearReqMap = true;

	private static String encodeVal(String val) {

		if (val == null)
			return "";

		try {
			if (true)
				val = URLDecoder.decode(val, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return val;

	}

	public static Map<String, String> strToMap(String str) {

		HashMap<String, String> params = new HashMap<String, String>();

		if (str.indexOf("&") > -1) {

			String[] arrs = str.split("&");
			for (int i = 0; i < arrs.length; i++) {

				String[] arr = arrs[i].split("=");
				String key = arr[0];
				String val = arr.length >= 2 ? encodeVal(arr[1]) : "";

				if (clearReqMap && val != null && !"".equals(val.trim())) {
					params.put(key, val);
				}

			}

		} else if (str.indexOf("=") > -1) {

			String[] arr = str.split("=");
			String key = arr[0];
			String val = arr.length >= 2 ? encodeVal(arr[1]) : "";

			if (clearReqMap && val != null && !"".equals(val.trim())) {
				params.put(key, val);
			}

		}

		// TODO session => cid, uid

		//System.out.println("params: " + params);

		return params;

	}

	public static String getRandomNumber() {
		return getRandomNumber(4);
	}

	public static String getRandomNumber(int size) {
		// "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
		String chars[] = "0,1,2,3,4,5,6,7,8,9".split(",");

		return getRandom(chars, size);
	}

	public static String getRandom(String chars[], int size) {
		StringBuffer buffer = new StringBuffer();
		Random random = new Random();

		for (int i = 0; i < size; i++) {
			buffer.append(chars[random.nextInt(chars.length)]);
		}
		return buffer.toString();
	}

	public static String getRandomId() {
		return getRandomId(9);
	}

	public static String getRandomId(int size) {

		// limit
		if (999 < size)
			size = 999;

		String id = "";
		while (id.length() < size) {
			UUID uuid = UUID.randomUUID();
			id += uuid.toString().replaceAll("-", "");
		}

		return id.substring(0, size);
	}

	public static String makeSuccess() {

		return makeSuccess(null);
	}

	public static String makeSuccess(Map<String, Object> map) {

		Map<String, Object> retVal = new HashMap<String, Object>();
		retVal.put("success", true);
		retVal.put("errorCode", null);
		retVal.put("errorMessage", null);

		if (map != null) {
			retVal.putAll(map);
		}

		return makeReturnVal(retVal);
	}

	public static String makeError(String errorMessage) {

		return makeError(null, errorMessage, null);
	}

	public static String makeError(String errorCode, String errorMessage, Map<String, Object> map) {

		Map<String, Object> retVal = new HashMap<String, Object>();
		retVal.put("success", false);
		retVal.put("errorCode", errorCode);
		retVal.put("errorMessage", errorMessage);

		if (map != null) {
			retVal.putAll(map);
		}

		return makeReturnVal(retVal);
	}

	public static String makeReturnVal(Map<String, Object> map) {

		String str = "";
		try {
			str = new ObjectMapper().writeValueAsString(map);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return str;
	}

	public static Map<String, Object> pojoToMap(Object obj) {

		ObjectMapper mapper = new ObjectMapper();

		// Convert POJO to Map
		Map<String, Object> map = mapper.convertValue(obj, new TypeReference<Map<String, Object>>() {
		});

		return map;

	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	public static Workbook makeExel(List<List<String>> list) {

		if (list == null)
			return null;

		// Workbook 생성
		Workbook xlsWb = new HSSFWorkbook();

		// Sheet 생성
		Sheet sheet1 = xlsWb.createSheet(); // name

		// 컬럼 너비 설정
		int colsize = 0;
		if (list != null && list.size() > 0) {
			List<String> col = list.get(0);
			if (col != null && col.size() > 0) {
				colsize = col.size();
			}
		}
		for (int i = 0; i < colsize; i++) {
			sheet1.setColumnWidth(i, 7000); // index, width
		}

		// Cell 스타일 생성
		CellStyle cellStyle = xlsWb.createCellStyle();
		cellStyle.setWrapText(true); // 줄 바꿈

		Row row = null;
		Cell cell = null;

		int rownum = 0;
		for (List<String> col : list) {
			row = sheet1.createRow(rownum++);

			int colnum = 0;
			for (String str : col) {
				cell = row.createCell(colnum++);
				cell.setCellValue(str);
				//				cell.setCellStyle(cellStyle);
			}
		}

		return xlsWb;
	}

	public static void setExcelDownloadHeader(HttpServletResponse response, Workbook workbook, String filename) {

		/* response.setContentType("application/excel"); */
		response.setContentType("application/msexcel");
		//		response.setContentType("application/excel;charset=UTF-8");
		response.setHeader("Content-Transper-Encoding", "binary");
		response.setHeader("Content-Disposition", "inline; filename=" + filename);

		ServletOutputStream out;
		try {
			out = response.getOutputStream();
			workbook.write(out);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void setExcelDownloadHeader(HttpServletResponse response, String filepath, String filename) {

		response.setContentType("application/msexcel");
		response.setHeader("Content-Transper-Encoding", "binary");
		response.setHeader("Content-Disposition", "inline; filename=" + filename);

		File file = new File(filepath);

		byte[] buffer = new byte[4096];
		int bytesRead = -1;

		try {
			OutputStream outStream = response.getOutputStream();
			InputStream in = new FileInputStream(file);

			while ((bytesRead = in.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			in.close();
			outStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ApplicationForm memberToApplication(MemberVO member) {
		ApplicationForm appl = new ApplicationForm();

		/* member */
		BeanUtils.copyProperties(member, appl);

		if (StringUtils.isNotEmpty(member.getCoNo()) && member.getCoNo().length() >= 10) {
			appl.setCoNo1(member.getCoNo().substring(0, 3));
			appl.setCoNo2(member.getCoNo().substring(3, 5));
			appl.setCoNo3(member.getCoNo().substring(5, 10));
		}

		if (StringUtils.isNotEmpty(member.getBubNo()) && member.getBubNo().length() >= 13) {
			appl.setBubNo1(member.getBubNo().substring(0, 6));
			appl.setBubNo2(member.getBubNo().substring(6, 13));
		}

		if (StringUtils.isNotEmpty(member.getTrustTel()) && member.getTrustTel().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(member.getTrustTel(), "-");
			if (st.hasMoreTokens())
				appl.setTrustTel1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setTrustTel2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setTrustTel3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(member.getTrustFax()) && member.getTrustFax().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(member.getTrustFax(), "-");
			if (st.hasMoreTokens())
				appl.setTrustFax1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setTrustFax2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setTrustFax3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(member.getCeoRegNo()) && member.getCeoRegNo().length() > 6) {
			//암호화
			//String ceoRegNo = XCrypto.decPatternReg(member.getCeoRegNo());
			String ceoRegNo = member.getCeoRegNo();
			//System.out.println("ceoRegNo  ::::: " + ceoRegNo);

			appl.setCeoRegNo1(ceoRegNo.substring(0, 6));
			appl.setCeoRegNo2(ceoRegNo.substring(6));
		}

		if (StringUtils.isNotEmpty(member.getCeoTel()) && member.getCeoTel().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(member.getCeoTel(), "-");
			if (st.hasMoreTokens())
				appl.setCeoTel1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoTel2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoTel3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(member.getCeoFax()) && member.getCeoFax().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(member.getCeoFax(), "-");
			if (st.hasMoreTokens())
				appl.setCeoFax1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoFax2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoFax3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(member.getCeoMobile()) && member.getCeoMobile().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(member.getCeoMobile(), "-");
			if (st.hasMoreTokens())
				appl.setCeoMobile1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoMobile2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoMobile3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(member.getCeoEmail()) && member.getCeoEmail().indexOf("@") > -1) {
			String email = member.getCeoEmail();
			appl.setCeoEmail1(email.substring(0, email.indexOf("@")));
			appl.setCeoEmail2(email.substring(email.indexOf("@") + 1));
		}

		//		if ("Y".equals(member.getAgreeSms())) {
		//			appl.setSmsAgree("Y");
		//		}
		//		else {
		//			appl.setSmsAgree("N");
		//		}
		appl.setCoGubunCd(member.getCoGubunCd());
		return appl;
	}

	public static ApplicationForm ChgHistoryToApplication(ChgHistoryVO ChgHistoryVO) {
		ApplicationForm appl = new ApplicationForm();

		BeanUtils.copyProperties(ChgHistoryVO, appl);
		appl.setMemberSeqNo(ChgHistoryVO.getMemberseqno());
		appl.setConditionCd(ChgHistoryVO.getConditioncd());
		appl.setConDetailCd(ChgHistoryVO.getConDetailCd());
		appl.setStatCd(ChgHistoryVO.getStatCd());
		appl.setCoGubunCd(ChgHistoryVO.getCoGubunCd());
		appl.setTrustZipcode(ChgHistoryVO.getPostTrustZipcode());
		appl.setTrustBunji(ChgHistoryVO.getPostTrustBunji());
		appl.setTrustDetailAddr(ChgHistoryVO.getPostTrustDetailAddr());
		appl.setTrustDong(ChgHistoryVO.getPostTrustDong());
		appl.setTrustSido(ChgHistoryVO.getPostTrustSido());
		appl.setTrustUrl(ChgHistoryVO.getPostTrustUrl());
		appl.setCeoName(ChgHistoryVO.getPostCeoName());
		appl.setCeoZipcode(ChgHistoryVO.getPostCeoZipcode());
		appl.setCeoBunji(ChgHistoryVO.getPostCeoBunji());
		appl.setCeoDetailAddr(ChgHistoryVO.getPostCeoDetailAddr());
		appl.setCeoDong(ChgHistoryVO.getPostCeoDong());
		appl.setCeoSido(ChgHistoryVO.getPostCeoSido());
		appl.setChgMemo(ChgHistoryVO.getChgMemo());
		appl.setCoName(ChgHistoryVO.getPostCoName());
		appl.setCeoRegNo(ChgHistoryVO.getPostCeoRegNo());
		appl.setConDetailCd(ChgHistoryVO.getConDetailCd());
		appl.setTrustTel(ChgHistoryVO.getPostTrustTel());
		appl.setTrustFax(ChgHistoryVO.getPostTrustFax());
		appl.setCeoTel(ChgHistoryVO.getPostCeoTel());
		appl.setCeoEmail(ChgHistoryVO.getPostCeoEmail());
		appl.setPrecoName(ChgHistoryVO.getPreCoName());
		appl.setPreceoName(ChgHistoryVO.getPreCeoName());
		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostCoNo())

				&& ChgHistoryVO.getPostCoNo().length() >= 10) {
			appl.setCoNo1(ChgHistoryVO.getPostCoNo().substring(0, 3));
			appl.setCoNo2(ChgHistoryVO.getPostCoNo().substring(3, 5));
			appl.setCoNo3(ChgHistoryVO.getPostCoNo().substring(5, 10));
		}

		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostBubNo()) && ChgHistoryVO.getPostBubNo().length() >= 13) {
			appl.setBubNo1(ChgHistoryVO.getPostBubNo().substring(0, 6));
			appl.setBubNo2(ChgHistoryVO.getPostBubNo().substring(6, 13));
		}

		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostTrustTel()) && ChgHistoryVO.getPostTrustTel().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(ChgHistoryVO.getPostTrustTel(), "-");
			if (st.hasMoreTokens())
				appl.setTrustTel1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setTrustTel2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setTrustTel3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostTrustFax()) && ChgHistoryVO.getPostTrustFax().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(ChgHistoryVO.getPostTrustFax(), "-");
			if (st.hasMoreTokens())
				appl.setTrustFax1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setTrustFax2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setTrustFax3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostCeoRegNo()) && ChgHistoryVO.getPostCeoRegNo().length() > 6) {
			//암호화 
			//appl.setCeoRegNo1(XCrypto.decPatternReg(ChgHistoryVO.getPostCeoRegNo()).substring(0, 6));
			//appl.setCeoRegNo2(XCrypto.decPatternReg(ChgHistoryVO.getPostCeoRegNo()).substring(6, 13));
			appl.setCeoRegNo1((ChgHistoryVO.getPostCeoRegNo()).substring(0, 6));
			appl.setCeoRegNo2((ChgHistoryVO.getPostCeoRegNo()).substring(6, 13));
		}

		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostCeoTel()) && ChgHistoryVO.getPostCeoTel().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(ChgHistoryVO.getPostCeoTel(), "-");
			if (st.hasMoreTokens())
				appl.setCeoTel1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoTel2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoTel3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostCeoFax()) && ChgHistoryVO.getPostCeoFax().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(ChgHistoryVO.getPostCeoFax(), "-");
			if (st.hasMoreTokens())
				appl.setCeoFax1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoFax2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoFax3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostCeoMobile()) && ChgHistoryVO.getPostCeoMobile().indexOf("-") > -1) {
			StringTokenizer st = new StringTokenizer(ChgHistoryVO.getPostCeoMobile(), "-");
			if (st.hasMoreTokens())
				appl.setCeoMobile1(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoMobile2(st.nextToken());
			if (st.hasMoreTokens())
				appl.setCeoMobile3(st.nextToken());
		}

		if (StringUtils.isNotEmpty(ChgHistoryVO.getPostCeoEmail()) && ChgHistoryVO.getPostCeoEmail().indexOf("@") > -1) {
			String email = ChgHistoryVO.getPostCeoEmail();
			appl.setCeoEmail1(email.substring(0, email.indexOf("@")));
			appl.setCeoEmail2(email.substring(email.indexOf("@") + 1));
		}

		//		if ("Y".equals(member.getAgreeSms())) {
		//			appl.setSmsAgree("Y");
		//		}
		//		else {
		//			appl.setSmsAgree("N");
		//		}
		return appl;
	}

	public static Map<String, Object> applicationToMember(ApplicationForm form) {

		MemberVO member = new MemberVO();
		BeanUtils.copyProperties(form, member);

		if (StringUtils.isNotEmpty(form.getCoNo1()) && StringUtils.isNotEmpty(form.getCoNo2()) && StringUtils.isNotEmpty(form.getCoNo3()))
			member.setCoNo(form.getCoNo1() + form.getCoNo2() + form.getCoNo3());
		if (StringUtils.isNotEmpty(form.getBubNo1()) && StringUtils.isNotEmpty(form.getBubNo2()))
			member.setBubNo(form.getBubNo1() + form.getBubNo2());
		if (StringUtils.isNotEmpty(form.getTrustTel1()) && StringUtils.isNotEmpty(form.getTrustTel2()) && StringUtils.isNotEmpty(form.getTrustTel3()))
			member.setTrustTel(form.getTrustTel1() + "-" + form.getTrustTel2() + "-" + form.getTrustTel3());
		if (StringUtils.isNotEmpty(form.getTrustFax1()) && StringUtils.isNotEmpty(form.getTrustFax2()) && StringUtils.isNotEmpty(form.getTrustFax3()))
			member.setTrustFax(form.getTrustFax1() + "-" + form.getTrustFax2() + "-" + form.getTrustFax3());
		if (StringUtils.isNotEmpty(form.getCeoRegNo1()) && StringUtils.isNotEmpty(form.getCeoRegNo2()))
			member.setCeoRegNo(XCrypto.encPatternReg(form.getCeoRegNo1() + form.getCeoRegNo2()));
		if (StringUtils.isNotEmpty(form.getCeoTel1()) && StringUtils.isNotEmpty(form.getCeoTel2()) && StringUtils.isNotEmpty(form.getCeoTel3()))
			member.setCeoTel(form.getCeoTel1() + "-" + form.getCeoTel2() + "-" + form.getCeoTel3());
		if (StringUtils.isNotEmpty(form.getCeoFax1()) && StringUtils.isNotEmpty(form.getCeoFax2()) && StringUtils.isNotEmpty(form.getCeoFax3()))
			member.setCeoFax(form.getCeoFax1() + "-" + form.getCeoFax2() + "-" + form.getCeoFax3());
		if (StringUtils.isNotEmpty(form.getCeoMobile1()) && StringUtils.isNotEmpty(form.getCeoMobile2()) && StringUtils.isNotEmpty(form.getCeoMobile3()))
			member.setCeoMobile(form.getCeoMobile1() + "-" + form.getCeoMobile2() + "-" + form.getCeoMobile3());
		if (StringUtils.isNotEmpty(form.getCeoEmail1()) && StringUtils.isNotEmpty(form.getCeoEmail2()))
			member.setCeoEmail(form.getCeoEmail1() + "@" + form.getCeoEmail2());

		if (StringUtils.isNotEmpty(form.getPrecoNo1()) && StringUtils.isNotEmpty(form.getPrecoNo2()) && StringUtils.isNotEmpty(form.getPrecoNo3()))
			member.setPrecoNo(form.getPrecoNo1() + form.getPrecoNo2() + form.getPrecoNo3());
		if (StringUtils.isNotEmpty(form.getPrebubNo1()) && StringUtils.isNotEmpty(form.getPrebubNo2()))
			member.setPrebubNo(form.getPrebubNo1() + form.getPrebubNo2());
		if (StringUtils.isNotEmpty(form.getPretrustTel1()) && StringUtils.isNotEmpty(form.getPretrustTel2()) && StringUtils.isNotEmpty(form.getPretrustTel3()))
			member.setPretrustTel(form.getPretrustTel1() + "-" + form.getPretrustTel2() + "-" + form.getPretrustTel3());
		if (StringUtils.isNotEmpty(form.getPretrustFax1()) && StringUtils.isNotEmpty(form.getPretrustFax2()) && StringUtils.isNotEmpty(form.getPretrustFax3()))
			member.setPretrustFax(form.getPretrustFax1() + "-" + form.getPretrustFax2() + "-" + form.getPretrustFax3());
		if (StringUtils.isNotEmpty(form.getPreceoRegNo1()) && StringUtils.isNotEmpty(form.getPreceoRegNo2()))
			member.setPreceoRegNo(XCrypto.encPatternReg(form.getPreceoRegNo1() + form.getPreceoRegNo2()));
		if (StringUtils.isNotEmpty(form.getPreceoTel1()) && StringUtils.isNotEmpty(form.getPreceoTel2()) && StringUtils.isNotEmpty(form.getPreceoTel3()))
			member.setPreceoTel(form.getPreceoTel1() + "-" + form.getPreceoTel2() + "-" + form.getPreceoTel3());
		if (StringUtils.isNotEmpty(form.getPreceoFax1()) && StringUtils.isNotEmpty(form.getPreceoFax2()) && StringUtils.isNotEmpty(form.getPreceoFax3()))
			member.setPreceoFax(form.getPreceoFax1() + "-" + form.getPreceoFax2() + "-" + form.getPreceoFax3());
		if (StringUtils.isNotEmpty(form.getPreceoMobile1()) && StringUtils.isNotEmpty(form.getPreceoMobile2()) && StringUtils.isNotEmpty(form.getPreceoMobile3()))
			member.setPreceoMobile(form.getPreceoMobile1() + "-" + form.getPreceoMobile2() + "-" + form.getPreceoMobile3());
		if (StringUtils.isNotEmpty(form.getPreceoEmail1()) && StringUtils.isNotEmpty(form.getPreceoEmail2()))
			member.setPreceoEmail(form.getPreceoEmail1() + "@" + form.getPreceoEmail2());

		List<ChgCdVO> chgCdList = new ArrayList<ChgCdVO>();
		for (String chgCd : form.getChgCd()) {
			ChgCdVO ChgCdVO = new ChgCdVO();
			ChgCdVO.setChgCd(chgCd);
			chgCdList.add(ChgCdVO);

		}

		List<WritingKindVO> writingKindList = new ArrayList<WritingKindVO>();
		for (String writingKindCd : form.getWritingKind()) {
			WritingKindVO writingKindVO = new WritingKindVO();
			writingKindVO.setWritingKind(writingKindCd);

			writingKindList.add(writingKindVO);
		}

		List<PermKindVO> permKindList = new ArrayList<PermKindVO>();
		addPermKindList(permKindList, form.getPermkind1(), "1"); // 어문 저작물            
		addPermKindList(permKindList, form.getPermkind2(), "2"); // 음악 저작물            
		addPermKindList(permKindList, form.getPermkind3(), "3"); // 영상 저작물            
		addPermKindList(permKindList, form.getPermkind21(), "21"); // 연극 저작물            
		addPermKindList(permKindList, form.getPermkind4(), "4"); // 미술 저작물            
		addPermKindList(permKindList, form.getPermkind5(), "5"); // 사진 저작물            
		addPermKindList(permKindList, form.getPermkind22(), "22"); // 건축 저작물            
		addPermKindList(permKindList, form.getPermkind23(), "23"); // 도형 저작물            
		addPermKindList(permKindList, form.getPermkind24(), "24"); // 컴퓨터프로그램 저작물       
		addPermKindList(permKindList, form.getPermkind0(), "0"); // 기타 저작물            

		addPermKindList(permKindList, form.getPermkind10(), "10"); // 실연자의 권리        
		addPermKindList(permKindList, form.getPermkind11(), "11"); // 음반제작자의 권리  
		addPermKindList(permKindList, form.getPermkind12(), "12"); // 출판권

		// etc
		addPermEtcList(permKindList, "1", form.getPermEtc1());
		addPermEtcList(permKindList, "2", form.getPermEtc2());
		addPermEtcList(permKindList, "3", form.getPermEtc3());
		addPermEtcList(permKindList, "21", form.getPermEtc21());
		addPermEtcList(permKindList, "4", form.getPermEtc4());
		addPermEtcList(permKindList, "5", form.getPermEtc5());
		addPermEtcList(permKindList, "22", form.getPermEtc22());
		addPermEtcList(permKindList, "23", form.getPermEtc23());
		addPermEtcList(permKindList, "24", form.getPermEtc24());
		addPermEtcList(permKindList, "0", form.getPermEtc0());
		addPermEtcList(permKindList, "10", form.getPermEtc10());
		addPermEtcList(permKindList, "11", form.getPermEtc11());
		//		addPermEtcList(permKindList, "12", form.getPermEtc12());

		//
		List<WritingKindVO> prewritingKindList = new ArrayList<WritingKindVO>();
		for (String writingKindCd : form.getPrewritingKind()) {
			WritingKindVO prewritingKindVO = new WritingKindVO();
			prewritingKindVO.setWritingKind(writingKindCd);

			prewritingKindList.add(prewritingKindVO);
		}

		List<PermKindVO> prepermKindList = new ArrayList<PermKindVO>();
		addPermKindList(prepermKindList, form.getPrepermkind1(), "1"); // 어문 저작물            
		addPermKindList(prepermKindList, form.getPrepermkind2(), "2"); // 음악 저작물            
		addPermKindList(prepermKindList, form.getPrepermkind3(), "3"); // 영상 저작물            
		addPermKindList(prepermKindList, form.getPrepermkind21(), "21"); // 연극 저작물            
		addPermKindList(prepermKindList, form.getPrepermkind4(), "4"); // 미술 저작물            
		addPermKindList(prepermKindList, form.getPrepermkind5(), "5"); // 사진 저작물            
		addPermKindList(prepermKindList, form.getPrepermkind22(), "22"); // 건축 저작물            
		addPermKindList(prepermKindList, form.getPrepermkind23(), "23"); // 도형 저작물            
		addPermKindList(prepermKindList, form.getPrepermkind24(), "24"); // 컴퓨터프로그램 저작물       
		addPermKindList(prepermKindList, form.getPrepermkind0(), "0"); // 기타 저작물            

		addPermKindList(prepermKindList, form.getPrepermkind10(), "10"); // 실연자의 권리        
		addPermKindList(prepermKindList, form.getPrepermkind11(), "11"); // 음반제작자의 권리  
		addPermKindList(prepermKindList, form.getPrepermkind12(), "12"); // 출판권

		// etc
		addPermEtcList(prepermKindList, "1", form.getPrepermEtc1());
		addPermEtcList(prepermKindList, "2", form.getPrepermEtc2());
		addPermEtcList(prepermKindList, "3", form.getPrepermEtc3());
		addPermEtcList(prepermKindList, "21", form.getPrepermEtc21());
		addPermEtcList(prepermKindList, "4", form.getPrepermEtc4());
		addPermEtcList(prepermKindList, "5", form.getPrepermEtc5());
		addPermEtcList(prepermKindList, "22", form.getPrepermEtc22());
		addPermEtcList(prepermKindList, "23", form.getPrepermEtc23());
		addPermEtcList(prepermKindList, "24", form.getPrepermEtc24());
		addPermEtcList(prepermKindList, "0", form.getPrepermEtc0());
		addPermEtcList(prepermKindList, "10", form.getPrepermEtc10());
		addPermEtcList(prepermKindList, "11", form.getPrepermEtc11());
		//		addPermEtcList(permKindList, "12", form.getPermEtc12());

		form.setMember(member);
		form.setWritingKindList(writingKindList);
		form.setPermKindList(permKindList);
		form.setPrepermKindList(prepermKindList);
		form.setPrewritingKindList(prewritingKindList);

		// make return value
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("member", member);
		map.put("writingKindList", writingKindList);
		map.put("prewritingKindList", prewritingKindList);
		map.put("permKindList", permKindList);
		map.put("prepermKindList", prepermKindList);
		map.put("chgCdList", chgCdList);
		map.put("chgMemo", form.getChgMemo());
		return map;
	}

	private static void addPermKindList(List<PermKindVO> permKindList, List<String> permKindCdList, String permKindCd) {
		for (String item : permKindCdList) {
			PermKindVO permKind = new PermKindVO();
			permKind.setWritingKind(permKindCd.trim());
			permKind.setPermKind(item);

			permKindList.add(permKind);
		}
	}

	private static void addPermEtcList(List<PermKindVO> permKindList, String writingKindCd, String permKindEtc) {
		for (PermKindVO item : permKindList) {
			if (writingKindCd.equals(item.getWritingKind()) && "0".equals(item.getPermKind())) {
				item.setPermEtc(permKindEtc);
			}
		}
	}

	public static String toFormat(String inStr, String pInformat) {
		StringBuffer rStr = new StringBuffer();

		try {
			for (int i = 0, j = 0; i < pInformat.length(); i++) {
				if (pInformat.charAt(i) == '#') {
					rStr.append(inStr.charAt(j));
					j++;
				} else {
					rStr.append(pInformat.charAt(i));
				}
			}
		} catch (Exception e) {
		}

		return rStr.toString().trim();
	}

	public static String getCryptSerial() {

		Crypt crypt = new Crypt();

		return crypt.getQuery("Serial"); // 19자리 발급번호
	}

	public static LoginVO getLoginSession(HttpServletRequest request) {

		return (LoginVO) request.getSession().getAttribute("SESS_USER");
	}

	public static LoginVO getAdminLoginSession(HttpServletRequest request) {

		return (LoginVO) request.getSession().getAttribute("SESS_ADMIN");
	}

	public static String getLoginId(HttpServletRequest request) {

		LoginVO login = (LoginVO) request.getSession().getAttribute("SESS_USER");

		return login != null ? login.getLoginId() : null;
	}

	public static String getAdminLoginId(HttpServletRequest request) {

		LoginVO login = (LoginVO) request.getSession().getAttribute("SESS_ADMIN");

		return login != null ? login.getLoginId() : null;
	}

	public static MemberVO getMemberSession(HttpServletRequest request) {

		return (MemberVO) request.getSession().getAttribute("SESS_MEMBER");
	}

	public static String getMemberSeqNo(HttpServletRequest request) {

		MemberVO member = (MemberVO) request.getSession().getAttribute("SESS_MEMBER");

		return member != null ? member.getMemberSeqNo() : null;
	}

	public static CertVO getCertSession(HttpServletRequest request) {

		return (CertVO) request.getSession().getAttribute("SESS_CERT");
	}

	public static String getWritingKindStr(List<WritingKindVO> writingKindList) {

		String writingKind = "";

		List<String> list = new ArrayList<String>();
		for (WritingKindVO item : writingKindList) {
			String str = item.getWritingKindStr();
			if (StringUtils.isNotEmpty(str) && !list.contains(str)) {
				list.add(str);
			}
		}
		for (String item : list) {
			writingKind += ", " + item;
		}
		if (list.size() > 0)
			writingKind = writingKind.substring(writingKind.length());

		return writingKind;
	}

	public static String getChgWritingKindStr(List<ChgWritingKindVO> writingKindList) {

		String writingKind = "";
		//System.out.println(writingKindList);
		List<String> list = new ArrayList<String>();
		for (ChgWritingKindVO item : writingKindList) {
			String str = item.getWritingKindStr();

			if (StringUtils.isNotEmpty(str) && !list.contains(str)) {
				list.add(str);
			}
		}
		for (String item : list) {
			writingKind += ", " + item;
		}
		if (list.size() > 0)
			writingKind = writingKind.substring(2);

		return writingKind;
	}

	public static String getWritingKind(List<WritingKindVO> writingKindList) {

		String writingKind = "";

		List<String> list = new ArrayList<String>();
		for (WritingKindVO item : writingKindList) {
			String str = item.getWritingKind();

			if (StringUtils.isNotEmpty(str) && !list.contains(str)) {
				list.add(str);
			}
		}
		for (String item : list) {
			writingKind += ", " + item;
		}

		if (list.size() > 0)
			writingKind = writingKind.substring(2);

		return writingKind;
	}

	public static String getPermKindStr(List<PermKindVO> permKindList) {

		String permKind = "";

		List<String> list = new ArrayList<String>();
		for (PermKindVO item : permKindList) {
			String str = item.getPermKindStr();
			if (StringUtils.isNotEmpty(str) && !list.contains(str)) {
				list.add(str);
			}
		}
		for (String item : list) {
			permKind += ", " + item;
		}
		if (list.size() > 0)
			permKind = permKind.substring(permKind.length());

		return permKind;
	}

	public static String getChgPermKindStr(List<ChgPermKindVO> permKindList) {

		String permKind = "";

		List<String> list = new ArrayList<String>();
		for (ChgPermKindVO item : permKindList) {
			String str = item.getPermKindStr();
			if (StringUtils.isNotEmpty(str) && !list.contains(str)) {
				list.add(str);
			}
		}
		for (String item : list) {
			permKind += ", " + item;
		}
		if (list.size() > 0) {//permKind = permKind.substring(2);permKind.substring(permKind.length());

			permKind = permKind.substring(2);

		}

		return permKind;
	}

	public static String getPermKind(List<PermKindVO> permKindList) {

		String permKind = "";

		List<String> list = new ArrayList<String>();
		for (PermKindVO item : permKindList) {
			String str = item.getPermKind();
			if (StringUtils.isNotEmpty(str) && !list.contains(str)) {
				list.add(str);
			}
		}
		for (String item : list) {
			permKind += ", " + item;
		}
		if (list.size() > 0)
			permKind = permKind.substring(permKind.length());

		return permKind;
	}

	public static Map<String, Object> getApplication(String memberSeqNo, String statCd2, String preStatCd2) {

		int statCd = 0, preStatCd = 0;
		try {
			statCd = Integer.valueOf(statCd2);
			preStatCd = Integer.valueOf(preStatCd2);

		} catch (Exception e) {
		}

		boolean down1 = false, down2 = false, down3 = false, down4 = false;
		if (statCd != 0) {
			down1 = true;
		}
		if (statCd != 0 && statCd >= 2 && statCd != 7 && statCd != 8 && statCd != 9 && statCd != 19 && statCd != 4 || (statCd == 7 && preStatCd == 39) || (statCd == 8 && preStatCd == 39) || (statCd == 9 && preStatCd == 39)) {
			down2 = true;
		}
		if (statCd != 0 && statCd >= 3 && statCd != 7 && statCd != 8 && statCd != 9 && statCd != 19 || (statCd == 7 && preStatCd == 39) || (statCd == 8 && preStatCd == 39) || (statCd == 9 && preStatCd == 39)
				|| (statCd == 3 && preStatCd == 19)) {
			if (statCd != 5 || (statCd == 8 && preStatCd != 19)) {
				down3 = true;
			}
		}
		if (statCd != 0 && statCd >= 4 && statCd != 7 && statCd != 8 && statCd != 9 && statCd != 19 && statCd != 39) {
			if (statCd != 5 && preStatCd != 19) {
				down4 = true;
			}
		}

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("preStatCd", preStatCd);
		data.put("down1", down1);
		data.put("down2", down2);
		data.put("down3", down3);
		data.put("down4", down4);

		return data;
	}

}
