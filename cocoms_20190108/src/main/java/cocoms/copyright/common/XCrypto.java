package cocoms.copyright.common;

import org.apache.commons.lang3.StringUtils;

import com.softforum.xdbe.xCrypto;

public class XCrypto {

	private static boolean use = true;
	private static String xdspPoolPath = "";
	
	public XCrypto(String xdspPath) {
		init(xdspPath);
	}
	public XCrypto(String xdspPath, boolean use) {
		this.use = use;
		init(xdspPath);
	}
	private static void init(String xdspPath) {
		xdspPoolPath = xdspPath;
	}
	
	/**
	 * 비밀번호 암호화
	 * @param pwd
	 * @return
	 */
	public static String encHashPw(String pwd) {

		// test mode
		if (!use) {
			return pwd.length() > 10 ? pwd : pwd + "XXXX";
		}
		
		String EncPwd = "";
		try {
			if (pwd.length() < 44) {
				EncPwd = xCrypto.Hash(6, pwd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return EncPwd;
	}

	/**
	 * 주민등록번호 암호화
	 * @param regno
	 * @return
	 */
	public static String encPatternReg(String regno) {

		// test mode
		if (!use) {
			return regno != null && regno.length() > 7 ? regno.substring(0, 7) + "XXXXXXXXXXXXXXXXXXXXXXXX" : "";
		}
		
		String EncReg = "";
		try {

			if (regno != null && !regno.equals("")) {

				xCrypto.RegisterEx("pattern7", 2, xdspPoolPath, "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
				EncReg = xCrypto.Encrypt("pattern7", regno);

				if (EncReg == null) {
					EncReg = "";
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return EncReg;
	}

	/**
	 * 주민등록번호 복호화
	 * @param regno
	 * @return
	 */
	public static String decPatternReg(String regno) {

		// test mode
		if (!use) {
			return regno.substring(0, 7) + "******";
		}
		
		String DecReg = "";

		try {
			if (regno != null && !regno.equals("") && regno.length() == 31) {
				
				xCrypto.RegisterEx("pattern7", 2, xdspPoolPath, "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
				DecReg = xCrypto.Decrypt("pattern7", regno);
				
				if (DecReg == null) {
					DecReg = "";
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return DecReg;
	}
}
