package cocoms.copyright.common;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {

	private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

	public static String getCurDate() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}
	public static String getCurDateTime() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}
	
	public static String toDateFormat(String str) {
		
		if (str != null && str.length() == 8) {
			return str.substring(0, 4) + "-" + str.substring(4, 6) + "-" + str.substring(6, 8);
		}
		
		return str;
	}
	
	/**
	 * 8자리의 현재 날짜를 얻어오는 함수
	 * 
	 * @return 8 자리의 현재 날짜 ( 20020301 )
	 */
	public static String getCurrDateNumber() {
		
		java.util.TimeZone zone = java.util.TimeZone.getTimeZone("GMT+9:00");
		java.util.Calendar c = java.util.Calendar.getInstance(zone);

		String returnDate = "" + c.get(java.util.Calendar.YEAR);
		String currentMonth = "" + (c.get(java.util.Calendar.MONTH) + 1);
		String currentDay = "" + c.get(java.util.Calendar.DAY_OF_MONTH);

		returnDate += addZeroFirst(currentMonth, 2);
		returnDate += addZeroFirst(currentDay, 2);

		return (returnDate);
	}

	/**
	 * 6자리의 현재 시간을 얻어오는 함수
	 * 
	 * @return 현재시간 (130904)
	 */
	public static String getCurrTimeNumber() {
		
		java.util.TimeZone zone = java.util.TimeZone.getTimeZone("GMT+9:00");
		java.util.Calendar c = java.util.Calendar.getInstance(zone);

		String currentHour = "" + c.get(java.util.Calendar.HOUR_OF_DAY);
		String currentMinute = "" + c.get(java.util.Calendar.MINUTE);
		String currentSecond = "" + c.get(java.util.Calendar.SECOND);

		String returnTime = "";
		returnTime += addZeroFirst(currentHour, 2);
		returnTime += addZeroFirst(currentMinute, 2);
		returnTime += addZeroFirst(currentSecond, 2);

		return (returnTime);
	}

	/**
	 * 원하는 자릿수만큼 문자열 앞에 0을 채워주는 함수
	 * @param changeIntValue 바꾸고자 하는 숫자
	 * @param 반환되어질 전체 자릿수
	 * @return 0 이 채워진 숫자
	 */
	private static String addZeroFirst(String changeIntValue, int strNum) {
		
		if (strNum < 0)
			return ("Parameter two is wrong");
		
		int intValueLength = changeIntValue.trim().length();

		if (intValueLength >= strNum) {
			return (changeIntValue);
		} 
		else {
			String returnValue = changeIntValue;
			for (int i = 0; i < (strNum - intValueLength); i++) {
				returnValue = "0" + returnValue;
			}
			return (returnValue);
		}
	}

}
