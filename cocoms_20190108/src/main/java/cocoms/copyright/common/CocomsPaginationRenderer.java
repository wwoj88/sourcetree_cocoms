package cocoms.copyright.common;

import egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

public class CocomsPaginationRenderer extends AbstractPaginationRenderer {

	public CocomsPaginationRenderer() {
		firstPageLabel = "";
		previousPageLabel = "<li class=\"direction prev\"><a href=\"\" data-page=\"{1}\"><span class=\"blind\">prev</span></a></li> ";
		currentPageLabel = "<li class=\"active\"><a href=\"\" data-page=\"{0}\">{0}</a></li> ";
		otherPageLabel = "<li><a href=\"\" data-page=\"{1}\">{1}</a></li> ";
		nextPageLabel = "<li class=\"direction next\"><a href=\"\" data-page=\"{1}\"><span class=\"blind\">next</span></a></li> ";
		lastPageLabel = "";
	}

	@Override
	public String renderPagination(PaginationInfo paginationInfo, String jsFunction) {

		return super.renderPagination(paginationInfo, jsFunction);
	}

}