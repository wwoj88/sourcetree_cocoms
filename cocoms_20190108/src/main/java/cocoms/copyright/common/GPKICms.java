package cocoms.copyright.common;

import com.gpki.gpkiapi_jni;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

/**
 * GPKI 암복화 샘플 프로그램
 * 이 샘플코드는 libgpkiapi_jni.jar 라이브러리가 classpath 에 있어야 한다.
 */
public class GPKICms {
	
	private boolean use = true;
	
	/* LDAP에서 G4C공개 키 획듯 URL */
	private String ldapUrl; 
	/* 귀 이용기관에 설치된 서버  인증서 경로 설정 (~ 부분을 귀 이용기관의 인증서값으로 설정) */
	private String certForSignFile;
	private String keyForSignFile;
	private String certForEnvFile;
	private String keyForEnvFile;
	/* 귀 이용기관에 설치된 서버  인증서 비밀번호 설정 */
	private String pinForSign;
	private String pinForEnv;
	private String DN;
	private String OU;
	private String AT;
	private String LIC;
	
	/* 암복호화 툴 */
	private gpkiapi_jni gpkiAPI = null;
	
	public  GPKICms() {
		init();
	}
	public  GPKICms(Properties prop) {
		init(prop);
	}
	public  GPKICms(Properties prop, boolean use) {
		this.use = use;
		init(prop);
	}

	private void init() {

		java.io.InputStream inputstream = getClass().getResourceAsStream("/cms.properties");

		Properties prop = new Properties();

		try {
			prop.load(inputstream);
			
		} catch (Exception exception) {
			//System.out.println(exception);
			return;
		}

		init(prop);
	}
	
	private void init(Properties prop) {

		try {

			ldapUrl = prop.getProperty("cms.ldapUrl");
			certForSignFile = prop.getProperty("cms.certForSignFile");
			keyForSignFile = prop.getProperty("cms.keyForSignFile");
			certForEnvFile = prop.getProperty("cms.certForEnvFile");
			keyForEnvFile = prop.getProperty("cms.keyForEnvFile");
			pinForSign = prop.getProperty("cms.pinForSign");
			pinForEnv = prop.getProperty("cms.pinForEnv");
			DN = prop.getProperty("cms.DN");
			OU = prop.getProperty("cms.OU");
			AT = prop.getProperty("cms.AT");
			LIC = prop.getProperty("cms.LIC");
		} catch (Exception exception) {
			//System.out.println(exception);
			return;
		}
	}
	
	/**
	 * 구분자'^' 추가 및 '^' 변경
	 * @param sParam
	 * @return
	 */
	private String deliStr(Object sParam){
		
		if(sParam == null || sParam.toString().length()==0)
			return "";
		else
			return sParam.toString().replaceAll("\\^", "<>")+"^";
	}

	/**
	 * 암호화
	 * @param GPKI
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@SuppressWarnings("finally")
	public String cmsEnc(String str) throws UnsupportedEncodingException {

		// test mode
		if (!use) {
			// 결제성공 sample
			return "MIIHtQIBADGB/jCB+wIBADBkMFAxCzAJBgNVBAYTAktSMRwwGgYDVQQKExNHb3Zlcm5tZW50IG9mIEtvcmVhMQ0wCwYDVQQLEwRHUEtJMRQwEgYDVQQDEwtDQTEzMTAwMDAwMQIQSwTeogK5LQP5ylIgQZqcgTANBgkqhkiG9w0BAQcwAASBgEQackq7uigKMQ93UQ5FPi1Sy2LBLIjgHJ+sIRtvzxJCypyxg6wF65fHdUJ1bzpbp97kaq0vpPPCusKTlTAw4tBrc8GXWxHWYcFm4BxO09KJ55Ax0hg+18bO0+kcCqXBQcySSDlxwW7rv3NwgTWdHwMVMPhjCbGUFfzF1hgP5f/TMIIGrQYJKoZIhvcNAQcBMBwGCCqDGoyaRAEEBBAqaIEhsxoOQJBVs2zAhWIPgIIGgD42TY7qiSEb1M0+AbEnU8QQ344j/ftqqIjotX48yppQQ1Qq9uM/0hjs73UIHB2l7NVO+deQaGI6NmzDisPiPLon3/NQAlx9KezJOFcMB3j+bC8vUa1jWqICojBStaB6HBw67493UaDli4CapH/JAVqQwUQ6dVycimQsXiLTfDfSgVFwAUbaO479W3byr/ApNgYbXAhdGVGnb7U1wqmUfPbnvFxt8Ne+BkUzfXLTE6SJRHaHUre363/4kPCOlgld52F2GZti+JNp1dDTw9jUXBMVKJulWYMWNyuZnsPXEwQqJH4r7ocABCT0dOj9j3fqR2UOlVm9PKG4xURyo85pziKeFySjix3oeqHgYHGfc1RVROYuLz+UAf8gZeWGvEtP/BZDFQgIt6jrzB2bfa0opyeimfitia1Exqi1r3PV3sDn3tg1cUWb/H68rabfVESeHhJ2Z7Ve9+mXUl+3brdXgpTze7fCtBQfIbkQ4gTDN7q283HDv5D1gT24c1LkcoxJlu/xMo1t5LmCfDYHYnVxOXNPIDGcJ28Jqp1cTZQasMCRHtATjIW+2FfVcMeLmePjZ5W8hPKzADXCC+IVNltW6l2MFuSwiLJlfizIs15A+k7aC5iS541fBFxO+dc0uERVD0xlq8vWaaEeUdYB/5io8y3NeWWXaC+DmnVwM6tJr9IovcNMjYslrekae2SLKQ0JOQIv+0QoInaVfXqYb4JdkCp1JzSD/9aMiwXpOY8AAdx9KwBr8ffs+oHdpni7IZXF8s4GHqfeG3Ax1BcpV9m0y/8UPVAH+vmQKazU+2ei5cw1xv4nSG6b3VfRc/Ti8U/Ki/og2Gep7mF3S6jzBE4nOW8iZFptz359WEO/ExTyIuxqAueovreNkbBlOp2Nr2/M7WIFJn6+BSSVMxeKFizn9A+X6W18PIE4YF3bYsXafTLBwdYTFH159MC4D5Hc5ll34vUnSEQL0oyg6rmPv8GYrHbUrHYV41A4Me9UuippNIFDlEfxE2qwtayUgUQJP/BLeB3umZDH2myeMb3X5uXocV0fSH4nJYif9L+MUjoyMocjFO2jGP46z4KTA3bot3oZLNak5tJ7lYLLdESQ3BPGpykDZygESGroQX8MDgdgNsIquOq8Q0S1mjzdDia91XmCM+MuRhH7VoloSJym6PhpymC2PADxsvp6yY0IlANeq9NqFPSnWw5P/93zjZbqoEbUCqc0vFgAR2nFBFphMyYzuokAphI1Uv6u9THKgm5ofGEfz4myhs3+6epOxwW5KAY1yLWl7DPZfIo3wdgNEVgl75I8OPCBdOUVS5w/hHcVIL5Mh3+3EIQ7IlpFSSsfYQ/zs+Kj5xp8659fChuB/w7HToDnoX0ntxOXRopgMPqItdZ0AYrzxIne6IKH2oSCZXX9SP8Rhhn/2G0DFXncFOjzd+imjuLLijLyROscKNa1/q5NDmE9fHnEydEom738o4PBVwIdwfeJ3nRuNAb15FKqAdhGFKPT455D6d/iWCeE2bBsKyBfexkxCeLvAAKX1UO4a6Z8mr6Y9UX6ug3E6kkqLjOZDxkdKaiMV8wTtWaNVDY2LSIxA7OvAx0OhsBgf2Lrxq1d0Z1jBn41ZPdvjZhzySgssCf258D6+N54pNvrpGNz8L6ZQ6aIMMg0MqmrTMzM0ElwnnmRghNc9NL6zHQta9V+VhgWU1Vjm01UOpGEyC3g+CjEuA3tGtBTJyBL9atuTuB6ZMd+dpKXDXow93LGMO3r8rqbfgI6UQV0mo9g4CX9WfOGA1k+zMeGqAgM44TqBiZohiIFyc8HDx/gQr+W4XPeQvpmAx8x+X8EEwUiBiBXJVCwQimGkBw+3QPYMRgrtEGbxuOPF8XiSU6yfJUdE9kQH1LusVUFjXpU7dwLRLmRc3x5pjWabSa/5cL0F9u38HmiztUrwZ8WZJCcJ17Su53zAog6kmrEztjeoKqyMibvF8YFCYGk+iGliA44Wt3wfjROd2zn8BS5tV/48OnFLKrzht4JuDwantPaCSDfuWY8aeto2COypHUg4eFGhxtj8KaqjVJ8SMdNn9hvVnCJvv0inAI9U0/+F/DSaImxPTxIYIEvXD2/UppAlTtMqLlz0FWa8qsMBmgAl76O8w+bJ802yFpUdK7pLfTANrLLPoAWfSkWnK+gkZ9cZ9qRhPIGFF/taUxw7c772Wtn8uUhITaZ112+Zv1P5t6uhZTEOJpG";
			
			// 결제오류 sample
		}

		String base64Encode = null;
		String orgMsg = null;
		
		orgMsg = deliStr(DN);
		orgMsg += deliStr(OU);
		orgMsg += deliStr(AT);
		orgMsg += str;
		//orgMsg = "SVR1371000009^ou=Group of Server,o=Government of Korea,c=KR^usercertificate;binary^SS00012^13711480912141882636^111^222^1371148^1371148^1371000^문화체육관광부 저작권위탁관리시스템 333 444^555^666^0403";
		System.out.println("strstr["+str+"]");
		System.out.println("orgMsg ["+orgMsg+"]");
		
		byte[] message = orgMsg.getBytes();

/*		String charSet[] = { "UTF-8", "euc-kr", "ksc5601", "iso-8859-1", "ascii", "x-windows-949" };

		for (int i = 0; i < charSet.length; i++) {

			for (int j = 0; j < charSet.length; j++) {

				if (i == j){

					continue;

				}

			
					System.out.println(charSet[i] + " :" + new String(message, charSet[i])+"<br>");
			

			}

		}*/
		apiNew();													//API 생성

		try{

			/**
			 * orgMsg 작성규칙은 G4CSS_GPKI_전자지불공통서비스.hwp 파일을 참조
			 * DN, OU, AT 값은 귀 이용기관의 값으로 설정하셔야 합니다.
			 */
			//System.out.println("1=Org Message| "+orgMsg); 
			
			byte[] gpkiMessage = encrypt(sign(message));				//암호화 과정
			
			base64Encode = encode(gpkiMessage);			//G4C로 전송할 데이터 base64
			//System.out.println("Encrypted Message| "+base64Encode);
 
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			return base64Encode;
		}
	}
	
	/**
	 * 복호화
	 * @param gpkiMsg
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String cmsDec(String gpkiMsg) throws UnsupportedEncodingException {
		
		 
		
		byte[] gpkiMessage = gpkiMsg.getBytes();
		 
		try {
			 
			apiNew();													//API 생성
			System.out.println(gpkiMsg);
			gpkiMessage = BASE64_Decode(gpkiMsg);					//복호화 과정
			gpkiMessage = verifySign(decrypt(gpkiMessage));

			System.out.println("Decrypted Message| "+new String(gpkiMessage));
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			
			/*
			GPKI Gpki = new GPKI();
			
			Gpki.setResMsg(new String(gpkiMessage));
			
			System.out.println("==================================================================");
			System.out.println("=Gpki.getIsSuccess() : "+Gpki.getIsSuccess());
			System.out.println("=Gpki.getReqFinDTime() : "+Gpki.getReqFinDTime());
			System.out.println("=Gpki.getMallSeq() : "+Gpki.getMallSeq());
			System.out.println("=Gpki.getPayMthdCd() : "+Gpki.getPayMthdCd());
			System.out.println("=Gpki.getTotFe() : "+Gpki.getTotFee());
			*/
			
		}
		return new String(gpkiMessage);
	}
	
	public byte[] BASE64_Decode(String sEncData) throws Exception {
		
        try{
			//API 초기화
        		apiInit();
    		int result = gpkiAPI.BASE64_Decode(sEncData.trim());
    		if (result != 0) {
    			throw new Exception(gpkiAPI.sReturnString + "(" + result+ ") : " + gpkiAPI.sDetailErrorString);
    		}
    		return gpkiAPI.baReturnArray;
        }finally{
			apiFinish();
		}
	}
	
	/**
	 * API 생성
	 *
	 */
	private void apiNew(){
		gpkiAPI = new gpkiapi_jni();
	}
	
	/**
	 * API 초기화
	 *
	 */
	private void apiInit(){
		gpkiAPI.API_Init(LIC);
	}
	
	/**
	 * API 종료
	 *
	 */
	private void apiFinish(){
		if (gpkiAPI != null)
		{   
			gpkiAPI.API_Finish();
		}
	}
	
	/**
	 * 평문 전자서명
	 * @param message
	 * @return
	 */
	private byte[] sign(byte[] message) throws Exception{
		
		byte[] signedData = null;
		byte[] certificate = null;
		byte[] key = null;
		int returnCode = 0;
		
		try{
			//API 초기화
			apiInit();
			
			// RSA 개인키와 인증서 읽기
			returnCode = gpkiAPI.STORAGE_ReadCert(gpkiapi_jni.MEDIA_TYPE_FILE_PATH, certForSignFile,gpkiapi_jni.DATA_TYPE_OTHER);
			if (returnCode != 0)
				throw new Exception(gpkiAPI.sReturnString + "(" + returnCode+ ") : " + gpkiAPI.sDetailErrorString);
	
			certificate = gpkiAPI.baReturnArray;
	
			returnCode = gpkiAPI.STORAGE_ReadPriKey(gpkiapi_jni.MEDIA_TYPE_FILE_PATH, keyForSignFile, pinForSign, gpkiapi_jni.DATA_TYPE_OTHER);
	
			if (returnCode != 0)
				throw new Exception(gpkiAPI.sReturnString + "(" + returnCode+ ") : " + gpkiAPI.sDetailErrorString);
	
			key = gpkiAPI.baReturnArray;
	
			// 서명값 생성하기
			returnCode = gpkiAPI.CMS_MakeSignedData(certificate, key, message, "");
	
			if (returnCode != 0)
				throw new Exception(returnCode + ":"+ gpkiAPI.sDetailErrorString);
	
			signedData = gpkiAPI.baReturnArray;
		}catch(Exception e){
			throw e;
		}finally{
			apiFinish();
		}

		return signedData;		
	}
	
	/**
	 * 암호화
	 * @param message
	 * @return
	 */
	private byte[] encrypt(byte[] message) throws Exception{
		
		byte[] encryptedData = null;
		try {
			//API 초기화
			apiInit();
			int returnCode = 0;

			returnCode = gpkiAPI.API_SetOption(gpkiapi_jni.API_OPT_RSA_ENC_V20);
			if (returnCode != 0)
				throw new Exception(gpkiAPI.sReturnString + "(" + returnCode+ ") : " + gpkiAPI.sDetailErrorString);
	   
			if(gpkiAPI.LDAP_GetDataByURL(gpkiapi_jni.LDAP_DATA_KM_CERT, ldapUrl) > 0) {
				throw new Exception(gpkiAPI.sReturnString + "(" + returnCode+ ") : " + gpkiAPI.sDetailErrorString);
			}

			// RSA 공개키를 이용한 평문 암호화
			returnCode = gpkiAPI.CMS_MakeEnvelopedData(gpkiAPI.baReturnArray, message, gpkiapi_jni.SYM_ALG_SEED_CBC);
			if (returnCode != 0)
				throw new Exception(gpkiAPI.sReturnString + "(" + returnCode+ ") : " + gpkiAPI.sDetailErrorString);

			encryptedData = gpkiAPI.baReturnArray;
		}catch(Exception e) {
			throw e;
		}finally{
			apiFinish();
		}
		return encryptedData;
	}
	
	private byte[] decrypt(byte[] originalData) throws Exception {
		
		byte[] plainData = null;
		int returnCode = 0;
		byte[] baPriKey = null;
		byte[] certificate = null;
		
		try{
			//API 초기화
			apiInit();

			// key 파일 읽기
			returnCode = gpkiAPI.STORAGE_ReadPriKey(gpkiapi_jni.MEDIA_TYPE_FILE_PATH, keyForEnvFile, pinForEnv, gpkiapi_jni.DATA_TYPE_OTHER);

			if (returnCode != 0)
				throw new Exception(gpkiAPI.sReturnString + "(" + returnCode+ ") : " + gpkiAPI.sDetailErrorString);

			baPriKey = gpkiAPI.baReturnArray;

			// 인증서 읽기
			returnCode = gpkiAPI.STORAGE_ReadCert(gpkiapi_jni.MEDIA_TYPE_FILE_PATH, certForEnvFile, gpkiapi_jni.DATA_TYPE_OTHER);

			if (returnCode != 0)
				throw new Exception(returnCode + " "+ gpkiAPI.sDetailErrorString);

			certificate = gpkiAPI.baReturnArray;

			returnCode = gpkiAPI.CMS_ProcessEnvelopedData(certificate,
					baPriKey, originalData);
			if (returnCode != 0)
				throw new Exception(gpkiAPI.sReturnString + "(" + returnCode+ ") : " + gpkiAPI.sDetailErrorString);

			plainData = gpkiAPI.baReturnArray;

		} catch(Exception e){
			throw e;
		}finally{
			apiFinish();
		}
		return plainData;
	}
	
	/**
	 * 서명검증
	 * @param signedData
	 * @return
	 * @throws Exception
	 */
	private byte[] verifySign(byte[] signedData) throws Exception {
		
		byte[] plainData = null;
		try {
			//API 초기화
			apiInit();

			int returnCode = 0;

			// 서명 검증
			returnCode = gpkiAPI.CMS_ProcessSignedData(signedData);
			if (returnCode != 0)
				throw new Exception(gpkiAPI.sReturnString + "(" + returnCode+ ") : " + gpkiAPI.sDetailErrorString);
		
			plainData = gpkiAPI.baData;
		}catch(Exception e){
			throw e;
		}finally{
			apiFinish();
		}
		return plainData;

	}
	
	private String encode(byte[] baData) throws Exception {
		
        try{
			//API 초기화
			apiInit();
    		int result = gpkiAPI.BASE64_Encode(baData);
    		if (result != 0) {
    			throw new Exception(gpkiAPI.sReturnString + "(" + result+ ") : " + gpkiAPI.sDetailErrorString);
    		}
            return gpkiAPI.sReturnString;
        }finally{
			apiFinish();
		}
        
	}

	private byte[] decode(String sEncData) throws Exception {
		
        try{
			//API 초기화
		/*	apiInit();*/
    		int result = gpkiAPI.BASE64_Decode(sEncData.trim());
    		if (result != 0) {
    			throw new Exception(gpkiAPI.sReturnString + "(" + result+ ") : " + gpkiAPI.sDetailErrorString);
    		}
    		return gpkiAPI.baReturnArray;
        }finally{
			apiFinish();
		}
	}

}