package cocoms.copyright.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import cocoms.copyright.entity.LoginVO;
import cocoms.copyright.entity.MemberVO;

public class ExcelParser {

	public static ArrayList<Map<String, Object>> parse(List<List<String>> excelDatas, int header2, String gercd, HttpServletRequest request) {
		// 필수조건 Exception 미완성
		LoginVO login2 = CommonUtils.getLoginSession(request);
		MemberVO login = CommonUtils.getMemberSession(request);

		String REPT_CHRR_NAME = request.getParameter("REPT_CHRR_NAME");
		String REPT_CHRR_POSI = request.getParameter("REPT_CHRR_POSI");
		String REPT_CHRR_TELX = request.getParameter("REPT_CHRR_TELX");
		String REPT_CHRR_MAIL = request.getParameter("REPT_CHRR_MAIL");
		String COMM_NAME =null;
		String COMM_TELX =null;
		String COMM_REPS_NAME =null;
		ArrayList<Map<String, Object>> uploadList = new ArrayList<Map<String, Object>>();
		int iCol = 0; //컬럼 구분값
		int iRow = 0; //행 구분값
		GregorianCalendar d = new GregorianCalendar();
		String reptYmdTmp = String.valueOf(d.get(Calendar.YEAR)) + String.valueOf(((d.get(Calendar.MONTH) + 1) < 10) ? "0" + String.valueOf(d.get(Calendar.MONTH) + 1) : String.valueOf(d.get(Calendar.MONTH) + 1));
		String reptYmd = reptYmdTmp.replaceAll(" ", "");
		String rgst_idnt = null;
		String trst_orgn_code = null;
		String conditioncd=login.getConditionCd();
		
		
		if (login == null || login2 == null) {

			login2 = CommonUtils.getAdminLoginSession(request);
			COMM_NAME = "관리자";
			COMM_TELX = "044-203-2468";
			COMM_REPS_NAME ="관리자";
			trst_orgn_code =  login2.getAppno2();
			rgst_idnt = login2.getLoginId();
		} else {
			COMM_NAME = login.getCoName();
			COMM_TELX = login.getTrustTel();
			COMM_REPS_NAME =login.getCeoName();
			trst_orgn_code = login.getAppNo2();
			rgst_idnt = login2.getLoginId();
		}
		if(trst_orgn_code.equals("")|| trst_orgn_code==null){
			trst_orgn_code="0";
		}
		
		if (gercd.equals("1")) { // 음악 
			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {
					
					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						//System.out.println("1 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						//System.out.println("2 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						//System.out.println("3 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 3) {
						//System.out.println("4 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("NATION_CD", str);
					} else if (iCol == 4) {
						//System.out.println("5 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("ALBUM_TITLE", str);
					} else if (iCol == 5) {
						//System.out.println("6 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("ALBUM_LABLE", str);
					} else if (iCol == 6) {
						//System.out.println("7 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("DISK_SIDE", 0);
						} else {
							exMap.put("DISK_SIDE", str);
						}
					} else if (iCol == 7) {
						//System.out.println("8 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("TRACK_NO", 0);
						} else {
							exMap.put("TRACK_NO", str);
						}
					} else if (iCol == 8) {
						//System.out.println("9 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("ALBUM_ISSU_YEAR", 0);
						} else {
							exMap.put("ALBUM_ISSU_YEAR", str);
						}
					} else if (iCol == 9) {
						//System.out.println("10 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("LYRC", str);
					} else if (iCol == 10) {
						//System.out.println("11 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMP", str);
					} else if (iCol == 11) {
						
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("ARRA", str);
					} else if (iCol == 12) {
						//System.out.println("13 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("TRAN", str);
					} else if (iCol == 13) {
						//System.out.println("14 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("SING", str);
					} else if (iCol == 14) {
						//System.out.println("15 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("PERF", str);
					} else if (iCol == 15) {
						//System.out.println("16 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("PROD", str);
					} else if (iCol == 16) {
						//System.out.println("17 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("COMM_MGNT_CD", 0);
						} else {
							exMap.put("COMM_MGNT_CD", str);
						}
					} else if (iCol == 17) {
						//System.out.println("18 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("LICE_MGNT_CD", 0);
						} else {
							exMap.put("LICE_MGNT_CD", str);
						}
					} else if (iCol == 18) {
						//System.out.println("19 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("PERF_MGNT_CD", 0);
						} else {
							exMap.put("PERF_MGNT_CD", str);
						}
					} else if (iCol == 19) {
						//System.out.println("20 :"+str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("PROD_MGNT_CD", 0);
						} else {
							exMap.put("PROD_MGNT_CD", str);
						}
					} else if (iCol == 20) {
						//System.out.println("21 :"+str);
						if (str == "없음" || str.equals("없음")) {
							exMap.put("UCI", 0);
						} else {
							exMap.put("UCI", str);
						}
					} else if (iCol == 21) {
						//System.out.println("22 :"+str);
						if (str == "없음" || str.equals("없음")) {
							exMap.put("RATIO", 0);
						} else {
							exMap.put("RATIO", str);
						}
					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);
					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		} else if (gercd.equals("2")) { //어문

			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {

					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 3) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_SUB_TITLE", str);
					} else if (iCol == 4) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("CRT_YEAR", 0);
						} else {
							exMap.put("CRT_YEAR", str);
						}
					} else if (iCol == 5) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("BOOK_TITLE", str);
					} else if (iCol == 6) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("BOOK_PUBLISHER", str);

					} else if (iCol == 7) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("BOOK_ISSU_YEAR", 0);
						} else {
							exMap.put("BOOK_ISSU_YEAR", str);
						}
					} else if (iCol == 8) {
						if (str.equals("") || str == null) {
							return null;
						}

						exMap.put("COPT_NAME", str);

					} else if (iCol == 9) {
						if (str.equals("") || str == null) {
							return null;
						}

						if (str == "없음" || str.equals("없음")) {

							exMap.put("COMM_MGNT_CD", 0);
						} else {

							exMap.put("COMM_MGNT_CD", str);
						}

					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);
					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		} else if (gercd.equals("3")) { //방송대본

			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {

					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 3) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_ORIG_TITLE", str);
					} else if (iCol == 4) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("SCRT_GENRE_CD", 0);
						} else {
							exMap.put("SCRT_GENRE_CD", str);
						}
					} else if (iCol == 5) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("SCRP_SUBJ_CD", 0);
						} else {
							exMap.put("SCRP_SUBJ_CD", str);
						}
					} else if (iCol == 6) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("ORIG_WRITER", str);
					} else if (iCol == 7) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("PLAYER", 0);
						} else {
							exMap.put("PLAYER", str);
						}
					} else if (iCol == 8) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("BROD_ORD_SEQ", 0);
						} else {
							exMap.put("BROD_ORD_SEQ", str);
						}
					} else if (iCol == 9) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("BORD_DATE", 0);
						} else {
							exMap.put("BORD_DATE", str);
						}
					} else if (iCol == 10) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("MAKE_CPY", str);
					} else if (iCol == 11) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WRITER", str);
					} else if (iCol == 12) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("DIRECTOR", str);
					} else if (iCol == 13) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("COMM_MGNT_CD", 0);
						} else {
							exMap.put("COMM_MGNT_CD", str);
						}

					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);
					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		} else if (gercd.equals("4")) { //영화

			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {

					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 3) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("CRT_YEAR", 0);
						} else {
							exMap.put("CRT_YEAR", str);
						}
					} else if (iCol == 4) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("PLAYER", str);
					} else if (iCol == 5) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("DIRECT", str);
					} else if (iCol == 6) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("WRITER", 0);
						} else {
							exMap.put("WRITER", str);
						}
					} else if (iCol == 7) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("DIRECTOR", 0);
						} else {
							exMap.put("DIRECTOR", str);
						}
					} else if (iCol == 8) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("PRODUCER", 0);
						} else {
							exMap.put("PRODUCER", str);
						}
					} else if (iCol == 9) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("DISTRIBUTOR", str);
					} else if (iCol == 10) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("INVESTOR", str);
					} else if (iCol == 11) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("COMM_MGNT_CD", 0);
						} else {
							exMap.put("COMM_MGNT_CD", str);
						}
					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);
					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		} else if (gercd.equals("5")) { //방송

			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {

					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 3) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("BORD_GENRE_CD", 0);
						} else {
							exMap.put("BORD_GENRE_CD", str);
						}
					} else if (iCol == 4) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("MAKE_YEAR", 0);
						} else {
							exMap.put("MAKE_YEAR", str);
						}
					} else if (iCol == 5) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("BROD_ORD_SEQ", 0);
						} else {
							exMap.put("BROD_ORD_SEQ", str);
						}
					} else if (iCol == 6) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("DIRECTOR", 0);
						} else {
							exMap.put("DIRECTOR", str);
						}
					} else if (iCol == 7) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("MAKE_CPY", 0);
						} else {
							exMap.put("MAKE_CPY", str);
						}
					} else if (iCol == 8) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("PLAYER", 0);
						} else {
							exMap.put("PLAYER", str);
						}
					} else if (iCol == 9) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WRITER", str);
					} else if (iCol == 10) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("COMM_MGNT_CD", 0);
						} else {
							exMap.put("COMM_MGNT_CD", str);
						}
					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);
					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		} else if (gercd.equals("6")) { //뉴스

			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {

					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 3) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("PAPE_NO", str);
					} else if (iCol == 4) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("PAPE_KIND", str);
					} else if (iCol == 5) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("CONTRIBUTOR", str);
					} else if (iCol == 6) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("REPOTER", str);
					} else if (iCol == 7) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("REVISION_ID", str);
					} else if (iCol == 8) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("ARTICL_DATE", 0);
						} else {
							exMap.put("ARTICL_DATE", str);
						}
					} else if (iCol == 9) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("ARTICL_ISSU_DTTM", str);
					} else if (iCol == 10) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("PROVIDER", str);
					} else if (iCol == 11) {

						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("COMM_MGNT_CD", 0);
						} else {
							exMap.put("COMM_MGNT_CD", str);
						}
					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);
					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		} else if (gercd.equals("7")) { //미술
			System.out.println(excelDatas.size());
			System.out.println(excelDatas.size());
			System.out.println(excelDatas.size());
			System.out.println(excelDatas.size());
			System.out.println(excelDatas.size());
			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {

					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						System.out.println("0  :"+ str);
						if (str.equals("") || str == null) {
							
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						System.out.println("1  :"+ str);
						if (str.equals("") || str == null) {
							
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						System.out.println("12  :"+ str);
						if (str.equals("") || str == null) {
							
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 3) {
						System.out.println("13  :"+ str);
						exMap.put("WORKS_SUB_TITLE", str);
					} else if (iCol == 4) {
						System.out.println("4  :"+ str);
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("KIND", str);
						
					} else if (iCol == 5) {
						System.out.println("5  :"+ str);
						if (str.equals("") || str == null) {
							
							return null;
						}
						exMap.put("MAKE_DATE", str);
					} else if (iCol == 6) {
						System.out.println("66  :"+ str);
						if (str == "없음" || str.equals("없음")) {
							exMap.put("SOURCE_INFO", 0);
						} else {
							exMap.put("SOURCE_INFO", str);
						}
					} else if (iCol == 7) {
						System.out.println("7  :"+ str);
						if (str == "없음" || str.equals("없음")) {
							exMap.put("SIZE_INFO", 0);
						} else {
							exMap.put("SIZE_INFO", str);
						}
					} else if (iCol == 8) {
						System.out.println("8  :"+ str);
						if (str == "없음" || str.equals("없음")) {
							exMap.put("RESOLUTION", 0);
						} else {
							exMap.put("RESOLUTION", str);
						}
					} else if (iCol == 9) {
						System.out.println("9  :"+ str);
						exMap.put("MAIN_MTRL", str);
					} else if (iCol == 10) {
						System.out.println("10  :"+ str);
						exMap.put("TXTR", str);
					} else if (iCol == 11) {
						System.out.println("11  :"+ str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("IMG_URL", 0);
						} else {
							exMap.put("IMG_URL", str);
						}
					} else if (iCol == 12) {
						System.out.println("12  :"+ str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("DETL_URL", 0);
						} else {
							exMap.put("DETL_URL", str);
						}

					} else if (iCol == 13) {
						System.out.println("13  :"+ str);
						exMap.put("WRITER", str);
					} else if (iCol == 14) {
						System.out.println("14  :"+ str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("POSS_DATE", 0);
						} else {
							exMap.put("POSS_DATE", str);
						}

					} else if (iCol == 15) {
						System.out.println("15  :"+ str);
						exMap.put("POSS_ORGN_NAME", str);
					} else if (iCol == 16) {
						System.out.println("16  :"+ str);
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("COMM_MGNT_CD", 0);
						} else {
							exMap.put("COMM_MGNT_CD", str);
						}
					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);
					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		} else if (gercd.equals("8")) { //이미지

			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {

					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 3) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("IMAGE_DESC", 0);
						} else {
							exMap.put("IMAGE_DESC", str);
						}
					} else if (iCol == 4) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("KEYWORD", 0);
						} else {
							exMap.put("KEYWORD", str);
						}
					} else if (iCol == 5) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("CRT_YEAR", 0);
						} else {
							exMap.put("CRT_YEAR", str);
						}
					} else if (iCol == 6) {
						if (str.equals("") || str == null) {
							return null;
						}

						exMap.put("WRITER", str);

					} else if (iCol == 7) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("PRODUCER", str);

					} else if (iCol == 8) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("IMAGE_URL", 0);
						} else {
							exMap.put("IMAGE_URL", str);
						}
					} else if (iCol == 9) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("IMAGE_OPEN_YN", str);
					} else if (iCol == 10) {

						exMap.put("ICNX_NUMB", str);
					} else if (iCol == 11) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("COMM_MGNT_CD", 0);
						} else {
							exMap.put("COMM_MGNT_CD", str);
						}
					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);
					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		} else if (gercd.equals("99")) { //기타 

			for (List<String> dataRow : excelDatas) {
				Map<String, Object> exMap = new HashMap<String, Object>();
				for (String str : dataRow) {

					if (iCol == header2) {
						break;
					}
					if (iCol == 0) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("RGST_ORGN_NAME", str);
					} else if (iCol == 1) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COMM_WORKS_ID", str);
					} else if (iCol == 2) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("SIDE_GENRE_CD", str);
					} else if (iCol == 3) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("WORKS_TITLE", str);
					} else if (iCol == 4) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("COPT_NAME", str);
					} else if (iCol == 5) {
						if (str.equals("") || str == null) {
							return null;
						}
						exMap.put("CRT_YEAR", str);
					} else if (iCol == 6) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("PUBL_MEDI", 0);
						} else {
							exMap.put("PUBL_MEDI", str);
						}
					} else if (iCol == 7) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("PUBL_DATE", 0);
						} else {
							exMap.put("PUBL_DATE", str);
						}
					} else if (iCol == 8) {
						if (str.equals("") || str == null) {
							return null;
						}
						if (str == "없음" || str.equals("없음")) {
							exMap.put("COMM_MGNT_CD", 0);
						} else {
							exMap.put("COMM_MGNT_CD", str);
						}
					}
					exMap.put("TOT_CNT", excelDatas.size());
					exMap.put("REPT_YMD", reptYmd);
					exMap.put("GENRE_CD", gercd);
					//임시
					exMap.put("TRST_ORGN_CODE", Integer.parseInt(trst_orgn_code)+300);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("REPT_CHRR_NAME", REPT_CHRR_NAME);
					exMap.put("REPT_CHRR_POSI", REPT_CHRR_POSI);
					exMap.put("REPT_CHRR_TELX", REPT_CHRR_TELX);
					exMap.put("REPT_CHRR_MAIL", REPT_CHRR_MAIL);
					exMap.put("RGST_IDNT", rgst_idnt);
					exMap.put("COMM_NAME", COMM_NAME);
					exMap.put("COMM_TELX", COMM_TELX);
					exMap.put("COMM_REPS_NAME", COMM_REPS_NAME);
					exMap.put("CONDITIONCD", conditioncd);

					iCol++;

				}
				uploadList.add(exMap);
				//exMap.clear();
				iCol = 0;
				//iRow = 0;

			}

		}

		return uploadList;
	}

}
