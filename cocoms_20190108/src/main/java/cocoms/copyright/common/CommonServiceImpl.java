package cocoms.copyright.common;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service
abstract public class CommonServiceImpl extends EgovAbstractServiceImpl {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private int rowPerPage = 10;
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}

	private int pagePerBlock = 10;
	public void setPagePerBlock(int pagePerBlock) {
		this.pagePerBlock = pagePerBlock;
	}

	protected Map<String, Object> getPageInfo(int page, int total) {
		logger.info("page[{}] total[{}]", page, total);

		int totalPage = total % rowPerPage == 0 ? (int) Math.floor(total/rowPerPage) : (int) Math.floor(total/rowPerPage) +1;
		int end = page%pagePerBlock == 0 ? (int) Math.floor(page/pagePerBlock) * pagePerBlock : ((int) Math.floor(page/pagePerBlock) +1) * pagePerBlock;
		int start = end - pagePerBlock +1;
		if (totalPage == 0) totalPage = 1;
		if (totalPage < end) end = totalPage;
		/*logger.info("totalPage[{}] start[{}] end[{}]", totalPage, start, end);*/
		
		boolean first = start == 1 ? true : false;
		boolean last = totalPage - pagePerBlock < start ? true : false;
		logger.info("first[{}] last[{}]", first, last);
		
		List<Integer> pages = new ArrayList<Integer>();
		for (int i=start; i<=end; i++) {
			pages.add(i);
		}
		
		boolean next = end < totalPage;	// next block
		boolean prev = 1 < start;		// prev block
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("first", first);
		map.put("last", last);
		map.put("start", start);
		map.put("end", end);
		map.put("pages", pages);
		map.put("next", next);
		map.put("prev", prev);
		return map;
	}
	
}
