package cocoms.copyright.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Collections;
import javax.annotation.Resource;
import javax.enterprise.inject.Model;
import javax.persistence.RollbackException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JPopupMenu.Separator;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.xmlbeans.impl.xb.xsdschema.Wildcard;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonNull;

import cocoms.copyright.application.ExcelMapper;
import cocoms.copyright.common.CommonUtils;
import cocoms.copyright.common.GPKICms;
import cocoms.copyright.entity.CertInfoVO;
import cocoms.copyright.entity.ChgWritingKindVO;
import cocoms.copyright.entity.LoginVO;
import cocoms.copyright.entity.MemberVO;
import cocoms.copyright.entity.ReportInfoVO;
import cocoms.copyright.entity.ReportStatVO;
import cocoms.copyright.report.ReportService;
import cocoms.copyright.user.UserService;
import cocoms.copyright.entity.ApiVO;
import cocoms.copyright.entity.ApiExcelVO;
import cocoms.copyright.entity.ApiReportVO;
import cocoms.copyright.web.form.CertForm;
import cocoms.copyright.web.form.ReportForm;
import cocoms.copyright.web.form.UserForm;
import oracle.net.aso.e;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

@Controller
public class CsUserApiController {
	/** apiService */
	@Resource(name = "apiService")
	private ApiService apiService;

	@Resource(name = "ExcelMapper")
	private ExcelMapper excelDao;
	/** ReportService */
	@Resource(name = "reportService")
	protected ReportService reportService;
	@Autowired
	@Qualifier("sqlSession")
	private SqlSessionFactory sqlSessionFactory;

	@RequestMapping(value = "/getuser", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> userInfo(@RequestBody ApiVO user) {
		String writekind = "";
		String memberseqno = "";
		ApiVO login = new ApiVO();
		try {
			login = apiService.checkUser(user);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

		}
		if (login == null) {
			return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
		}
		memberseqno = login.getMemberSeqNo();

		try {
			List<String> desc = new ArrayList<>();
			List<String> kind = new ArrayList<>();
			List<ApiVO> writingKindList = (List<ApiVO>) apiService.writingKind(memberseqno);
			Collections.sort(writingKindList);
			
			for (ApiVO writingKind : writingKindList) {
				
				desc.add(writingKind.getWritingKindDesc());
				kind.add(writingKind.getWritingKind());
			}
			StringBuilder sb = new StringBuilder();
			for (String str : desc) {

				sb.append(str);
				sb.append(",");
			}
			StringBuilder sb2 = new StringBuilder();
			for (String str : kind) {

				sb2.append(str);
				sb2.append(",");
			}
			String desccomm = sb.toString();
			String kind2  = sb2.toString();
			kind2 = kind2.substring(0, kind2.length() - 1);
			desccomm = desccomm.substring(0, desccomm.length() - 1);
			login.setWritingKind(kind2);
			login.setWritingKindDesc(desccomm);
			login.setPwd("");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(login, HttpStatus.OK);

	}



	@RequestMapping(value = "/exceldata", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> test2(HttpServletRequest request, @RequestBody List<ApiExcelVO> list) throws Exception {
		//System.out.println(list.toString());
		SqlSession session = null;
		int checkReport = 0;
		int rept_work_cont = list.size();
		ApiExcelVO checkmap = new ApiExcelVO();
		int listSize = list.size();
		checkmap = (ApiExcelVO) list.get(0);
		//System.out.println(checkmap.toString());
		//checkmap2= (ApiExcelVO) list.get(1);
		//System.out.println("EXCEL START");
		int code = Integer.parseInt(checkmap.getTrst_orgn_code()) + 300;
		String trstcode = Integer.toString(code);
		String conditioncd = checkmap.getConditioncd();
		checkmap.setTrst_orgn_code(trstcode);
		//System.out.println(code);
		try {
			checkReport = apiService.checkReport(checkmap);
			//System.out.println(checkReport);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.BAD_GATEWAY);
		}
		//System.out.println("??");
		//System.out.println(checkReport);
		if (checkReport == 0) {
			System.out.println("No Report");
			// INSERT First Data or SOME Data
			checkmap.setRept_works_cont(Integer.toString(list.size()));
			try {
				apiService.commWorksReportInsert(checkmap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			List<ApiExcelVO> result = new ArrayList();

			for (int i = 0; i < listSize; i++) {

				list.get(i).setTrst_orgn_code(trstcode);

				if (apiService.getExistCommWorks(list.get(i)) == 0) {

					int works_id = excelDao.getCommWorksId();
					list.get(i).setWorks_id(works_id);
					
					result.add(list.get(i));
				} else {
					//중복제거

					//System.out.println("중복발생");
					//list.remove(i);
				}

			}
			if ((list.size() - result.size()) != 0) {
				checkmap.setRept_works_cont(Integer.toString(result.size()));
				checkmap.setConditioncd(conditioncd);
				apiService.commWorksReportUpdateCnt(checkmap);
			}
			//result.size(); -> 추가로 들어갈것들 or 신규
			try {
				int idx = 0;
				session = sqlSessionFactory.openSession(ExecutorType.BATCH);
				ApiMapper mapper = session.getMapper(ApiMapper.class);

				for (int i = 0; i < result.size(); i++) {
					
					mapper.commWorksInsertBacth(result.get(i));
					if (idx % 100 == 0) {
						session.commit();
					}

					idx++;
				}

			} catch (Exception e) {

				session.rollback();
				e.printStackTrace();
				return new ResponseEntity<>(result, HttpStatus.BAD_GATEWAY);

			} finally {
				session.commit();
				session.close();
			}

			try {
				int idx = 0;
				session = sqlSessionFactory.openSession(ExecutorType.BATCH);
				ApiMapper mapper = session.getMapper(ApiMapper.class);

				for (int i = 0; i < result.size(); i++) {

					mapper.commMusicInsertBacth(result.get(i));
					if (idx % 100 == 0) {
						session.commit();
					}
					idx++;

				}
			} catch (Exception e) {

				session.rollback();
				e.printStackTrace();
				return new ResponseEntity<>(result, HttpStatus.BAD_GATEWAY);

			} finally {
				session.commit();
				session.close();
			}

			return new ResponseEntity<>(result.size(), HttpStatus.OK);

		} else {
			// 추가 보고 
			
			List<ApiExcelVO> result = new ArrayList();
			try{
			for (int i = 0; i < listSize; i++) {

				list.get(i).setTrst_orgn_code(trstcode);
				list.get(i).setConditioncd(conditioncd);
				if (apiService.getExistCommWorks(list.get(i)) == 0) {

					int works_id = excelDao.getCommWorksId();
					list.get(i).setWorks_id(works_id);
					result.add(list.get(i));
				} else {

				}

			}
			
				//System.out.println((list.size() - result.size()));
			
				checkmap.setRept_works_cont(Integer.toString(result.size()));
				apiService.commWorksReportAddUpdateCnt(checkmap);
			} catch (Exception e) {
					e.printStackTrace();
			}
			try {
				int idx = 0;
				session = sqlSessionFactory.openSession(ExecutorType.BATCH);
				ApiMapper mapper = session.getMapper(ApiMapper.class);

				for (int i = 0; i < result.size(); i++) {
					//System.out.println(result.get(i).toString());
					mapper.commWorksInsertBacth(result.get(i));
					if (idx % 100 == 0) {
						session.commit();
					}
					//System.out.println(idx);
					idx++;
				}

			} catch (Exception e) {

				session.rollback();
				e.printStackTrace();
				return new ResponseEntity<>(result, HttpStatus.BAD_GATEWAY);

			} finally {
				session.commit();
				session.close();
			}

			try {
				int idx = 0;
				session = sqlSessionFactory.openSession(ExecutorType.BATCH);
				ApiMapper mapper = session.getMapper(ApiMapper.class);

				for (int i = 0; i < result.size(); i++) {

					mapper.commMusicInsertBacth(result.get(i));
					if (idx % 100 == 0) {
						session.commit();
					}
					idx++;

				}
			} catch (Exception e) {

				session.rollback();
				e.printStackTrace();
				return new ResponseEntity<>(result, HttpStatus.BAD_GATEWAY);

			} finally {
				session.commit();
				session.close();
			}

			return new ResponseEntity<>(result.size(), HttpStatus.OK);

		}

	}
	
	
	

	@RequestMapping(value = "/csreport", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> csreportYear(HttpServletRequest request,	@RequestBody  List<ApiReportVO> form) throws Exception {
		
		//System.out.println(form.toString());
		List<ReportStatVO> reportStatusList = new ArrayList<ReportStatVO>(); 
		for(int i=0; i<form.size();i++){

			ReportStatVO reportStat = new ReportStatVO();
			reportStat.setMemberSeqNo(form.get(i).getMemberSeqNo());
			reportStat.setRptYear(form.get(i).getRptYear());
			reportStat.setWritingKind(form.get(i).getWritingKind());
			reportStat.setPermKind("0");

			reportStat.setInWorkNum(form.get(i).getInWorkNum());
			reportStat.setOutWorkNum(form.get(i).getOutWorkNum());
			reportStat.setWorkSum(form.get(i).getWorkSum());

			reportStat.setInRental(form.get(i).getInRental());
			reportStat.setOutRental(form.get(i).getOutRental());
			reportStat.setRentalSum(form.get(i).getRentalSum());
			reportStat.setInChrg(form.get(i).getInChrg());
			reportStat.setOutChrg(form.get(i).getOutChrg());
			reportStat.setChrgSum(form.get(i).getChrgSum());
			reportStat.setInChrgRate(form.get(i).getInChrgRate());
			reportStat.setOutChrgRate(form.get(i).getOutChrgRate());
			reportStat.setAvgChrgRate(form.get(i).getAvgChrgRate());
			
			//System.out.println(reportStat.toString());
			reportStatusList.add(reportStat);
		}

		try {
			ReportInfoVO member = new ReportInfoVO();
			BeanUtils.copyProperties(form.get(0), member);
			
			reportService.saveSubReport(member);
			reportService.saveSubReportStatuses(reportStatusList);

		} catch (Exception e) {
			e.printStackTrace();
			
			return new ResponseEntity<>(null, HttpStatus.BAD_GATEWAY);

		}
		
	

		return new ResponseEntity<>(null, HttpStatus.OK);

	}
	
	

}
