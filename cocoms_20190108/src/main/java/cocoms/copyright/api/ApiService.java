package cocoms.copyright.api;

import java.util.List;

import cocoms.copyright.entity.ApiExcelVO;
import cocoms.copyright.entity.ApiVO;

public interface ApiService {

	
	ApiVO checkUser(ApiVO user) throws Exception;
	ApiVO getuser(ApiVO user) throws Exception;
	List<?> writingKind(String memberseqno) throws Exception;
	int checkReport(ApiExcelVO checkmap) throws Exception;
	void commWorksReportInsert(ApiExcelVO checkmap)  throws Exception;
	int getExistCommWorks(ApiExcelVO apiExcelVO) throws Exception;
	int getCommWorksId() throws Exception;
	void commWorksReportUpdateCnt(ApiExcelVO checkmap)throws Exception;
	void commWorksReportAddUpdateCnt(ApiExcelVO checkmap)throws Exception;
	

	

}
