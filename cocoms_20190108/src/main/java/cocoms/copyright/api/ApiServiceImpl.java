package cocoms.copyright.api;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import cocoms.copyright.api.ApiService;
import cocoms.copyright.application.PermKindMapper;
import cocoms.copyright.common.XCrypto;
import cocoms.copyright.entity.ApiExcelVO;
import cocoms.copyright.entity.ApiVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;

@Service("apiService")
public class ApiServiceImpl extends EgovAbstractServiceImpl implements ApiService {
	
	//API MAPPER
	@Resource(name = "apiMapper")
	private ApiMapper apiDAO;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Override
	public ApiVO getuser(ApiVO user) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApiVO checkUser(ApiVO user) throws Exception {
		

		new XCrypto(propertiesService.getString("xcry.path"), propertiesService.getBoolean("xcry.use"));

		// password enc
		String pwd = user.getPwd();
		
		if (StringUtils.isNotEmpty(pwd)) {
			pwd = XCrypto.encHashPw(pwd);
			user.setPwd(pwd);
		}
		user = apiDAO.checkUser(user);
		
		
		if(user.getStatCd().equals("5") || user.getStatCd().equals("6")|| user.getAppno2().equals("")|| user.getAppno2()==null){
			return null;
		}

		//user.setPwd("dnjsdn11!!XXXX");
			return apiDAO.checkUser(user);
	}

	@Override
	public List<?> writingKind(String memberseqno) throws Exception {
	   	// TODO Auto-generated method stub
		
		return apiDAO.writingKind(memberseqno);
	}

	@Override
	public int checkReport(ApiExcelVO checkmap) throws Exception {
		// TODO Auto-generated method stub
		return apiDAO.checkReport(checkmap);
	}

	@Override
	public void commWorksReportInsert(ApiExcelVO checkmap) throws Exception {
		// TODO Auto-generated method stub
		apiDAO.commWorksReportInsert(checkmap);
	}

	@Override
	public int getExistCommWorks(ApiExcelVO list) throws Exception {
		// TODO Auto-generated method stub
		return apiDAO.getExistCommWorks(list);
	}

	@Override
	public int getCommWorksId() throws Exception {
		// TODO Auto-generated method stub
		return apiDAO.getCommWorksId();
	}

	@Override
	public void commWorksReportUpdateCnt(ApiExcelVO checkmap) throws Exception {
		// TODO Auto-generated method stub
		apiDAO.commWorksReportUpdateCnt(checkmap);
	}

	@Override
	public void commWorksReportAddUpdateCnt(ApiExcelVO checkmap) throws Exception {
		// TODO Auto-generated method stub
		apiDAO.commWorksReportAddUpdateCnt(checkmap);
	}

}
