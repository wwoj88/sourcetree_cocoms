/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.api;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("apiMapper")
public interface ApiMapper {

	ApiVO checkUser(ApiVO user)throws Exception;

	List<?> writingKind(String memberseqno)throws Exception;

	int checkReport(ApiExcelVO checkmap)throws Exception;

	void commWorksReportInsert(ApiExcelVO checkmap)throws Exception;

	int getCommWorksId()throws Exception;

	int getExistCommWorks(ApiExcelVO list)throws Exception;

	void commWorksInsertBacth(ApiExcelVO apiExcelVO)throws Exception;

	void commMusicInsertBacth(ApiExcelVO apiExcelVO)throws Exception;

	void commWorksReportUpdateCnt(ApiExcelVO checkmap)throws Exception;

	void commWorksReportAddUpdateCnt(ApiExcelVO checkmap)throws Exception;


	
}
