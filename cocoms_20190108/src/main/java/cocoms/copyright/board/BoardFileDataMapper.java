/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.board;

import java.util.*;

import cocoms.copyright.entity.*;

import egovframework.rte.psl.dataaccess.mapper.Mapper;

@Mapper("boardFileDataMapper")
public interface BoardFileDataMapper {

	List<?> selectList(Map<String, Object> filter) throws Exception;
	
	void insert(BoardFileDataVO vo) throws Exception;
	
	void delete(BoardFileDataVO vo) throws Exception;

	String selectName(Map<String, Object> filter)throws Exception;
	
}
