/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.board;

import java.util.*;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import cocoms.copyright.application.PermKindMapper;
import cocoms.copyright.application.RpMgmMapper;
import cocoms.copyright.application.WritingKindMapper;
import cocoms.copyright.common.*;
import cocoms.copyright.entity.*;
import cocoms.copyright.member.*;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Service("noticeService")
public class NoticeServiceImpl extends EgovAbstractServiceImpl implements BoardService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	private final String boardId = "3"; 	// 3: 공지사항

	@Resource(name="boardMapper")
	private BoardMapper boardDAO;

	@Resource(name="boardFileDataMapper")
	private BoardFileDataMapper boardFileDataDAO;

	@Override
	public Map<String, Object> getNotices(Map<String, Object> filter) throws Exception {
		//LOGGER.info("filter [{}]", filter);

		filter.put("boardId", boardId);
		
		List<BoardVO> list = (List<BoardVO>) boardDAO.selectList(filter);
		int total = boardDAO.selectTotalCount(filter);
		
		for (BoardVO board : list) {
			
			List<BoardFileDataVO> fileList = (List<BoardFileDataVO>) boardFileDataDAO.selectList(CommonUtils.pojoToMap(board));
			board.setBoardFileDataList(fileList);
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("total", total);
		
		return map;
	}

	@Override
	public void hitCount(BoardVO vo) throws Exception {
		LOGGER.info(vo.toString());

		boardDAO.countHit(vo);
	}
	
	@Override
	public BoardVO getNotice(BoardVO vo) throws Exception {
		LOGGER.info(vo.toString());
		
		vo.setBoardId(boardId);

		BoardVO data = boardDAO.select(vo);
		
		List<BoardFileDataVO> fileList = (List<BoardFileDataVO>) boardFileDataDAO.selectList(CommonUtils.pojoToMap(data));
		data.setBoardFileDataList(fileList);
		
		return data;
	}
	
	@Override
	public BoardVO getNotice(String boardSeqNo) throws Exception {
		LOGGER.info("boardSeqNo [{}]", boardSeqNo);

		return getNotice(new BoardVO(boardSeqNo));
	}

	@Override
	public String saveNotice(BoardVO vo) throws Exception {
		LOGGER.info("board [{}]", vo);

		vo.setBoardId(boardId);
		
		// insert
		if (StringUtils.isEmpty(vo.getBoardSeqNo())) {
			boardDAO.insert(vo);
			
			String boardSeqNo = vo.getBoardSeqNo();
			
			for (BoardFileDataVO file : vo.getBoardFileDataList()) {
				file.setBoardSeqNo(boardSeqNo);
				LOGGER.info("file [{}]", file);

				boardFileDataDAO.insert(file);
			}
		}
		// update
		else {
			boardDAO.update(vo);

			String boardSeqNo = vo.getBoardSeqNo();

			boardFileDataDAO.delete(new BoardFileDataVO(boardSeqNo));
			
			for (BoardFileDataVO file : vo.getBoardFileDataList()) {
				file.setBoardSeqNo(boardSeqNo);
				LOGGER.info("file #2 [{}]", file);

				boardFileDataDAO.insert(file);
			}
		}
		
		return vo.getBoardSeqNo();
	}
	
	@Override
	public void removeNotice(BoardVO vo) throws Exception {
		LOGGER.info(vo.toString());

		vo.setBoardId(boardId);
		
		boardDAO.delete(vo);
	}

	@Override
	public void removeNotice(String boardSeqNo) throws Exception {
		LOGGER.info("boardSeqNo [{}]", boardSeqNo);

		removeNotice(new BoardVO(boardSeqNo));
	}
	
}
