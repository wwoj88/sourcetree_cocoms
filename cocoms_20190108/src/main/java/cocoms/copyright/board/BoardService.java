/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cocoms.copyright.board;

import java.util.*;

import cocoms.copyright.entity.*;

public interface BoardService {

	Map<String, Object> getNotices(Map<String, Object> filter) throws Exception;

	void hitCount(BoardVO vo) throws Exception;
	
	BoardVO getNotice(BoardVO vo) throws Exception;
	BoardVO getNotice(String boardSeqNo) throws Exception;
	
	String saveNotice(BoardVO vo) throws Exception;

	void removeNotice(BoardVO vo) throws Exception;
	void removeNotice(String boardSeqNo) throws Exception;


}
