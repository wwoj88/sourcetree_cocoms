package cocoms.copyright.entity;

import java.util.Date;

public class CommWorksVO  extends DefaultVO{

	private static final long serialVersionUID = 1L;
	private String 	works_id="";
	private String	rept_ymd="";
	private String 	comm_works_id="";
	private String 	genre_cd="";
	private String 	nation_cd="";
	private String 	works_title="";
	private String 	works_sub_title="";
	private String 	crt_year="";
	private String 	comm_mgnt_cd="";
	private String 	rgst_orgn_name="";
	private String 	icnx_numb="";
	private String 	rgst_idnt="";
	private String 	rgst_dttm="";
	private String 	modi_idnt="";
	private String 	modi_dttm="";
	private String 	trst_orgn_code="";
	private String 	rept_cnt="";
	public String getWorks_id() {
		return works_id;
	}
	public void setWorks_id(String works_id) {
		this.works_id = works_id;
	}
	public String getRept_ymd() {
		return rept_ymd;
	}
	public void setRept_ymd(String rept_ymd) {
		this.rept_ymd = rept_ymd;
	}
	public String getComm_works_id() {
		return comm_works_id;
	}
	public void setComm_works_id(String comm_works_id) {
		this.comm_works_id = comm_works_id;
	}
	public String getGenre_cd() {
		return genre_cd;
	}
	public void setGenre_cd(String genre_cd) {
		this.genre_cd = genre_cd;
	}
	public String getNation_cd() {
		return nation_cd;
	}
	public void setNation_cd(String nation_cd) {
		this.nation_cd = nation_cd;
	}
	public String getWorks_title() {
		return works_title;
	}
	public void setWorks_title(String works_title) {
		this.works_title = works_title;
	}
	public String getWorks_sub_title() {
		return works_sub_title;
	}
	public void setWorks_sub_title(String works_sub_title) {
		this.works_sub_title = works_sub_title;
	}
	public String getCrt_year() {
		return crt_year;
	}
	public void setCrt_year(String crt_year) {
		this.crt_year = crt_year;
	}
	public String getComm_mgnt_cd() {
		return comm_mgnt_cd;
	}
	public void setComm_mgnt_cd(String comm_mgnt_cd) {
		this.comm_mgnt_cd = comm_mgnt_cd;
	}
	public String getRgst_orgn_name() {
		return rgst_orgn_name;
	}
	public void setRgst_orgn_name(String rgst_orgn_name) {
		this.rgst_orgn_name = rgst_orgn_name;
	}
	public String getIcnx_numb() {
		return icnx_numb;
	}
	public void setIcnx_numb(String icnx_numb) {
		this.icnx_numb = icnx_numb;
	}
	public String getRgst_idnt() {
		return rgst_idnt;
	}
	public void setRgst_idnt(String rgst_idnt) {
		this.rgst_idnt = rgst_idnt;
	}
	public String getRgst_dttm() {
		return rgst_dttm;
	}
	public void setRgst_dttm(String rgst_dttm) {
		this.rgst_dttm = rgst_dttm;
	}
	public String getModi_idnt() {
		return modi_idnt;
	}
	public void setModi_idnt(String modi_idnt) {
		this.modi_idnt = modi_idnt;
	}
	public String getModi_dttm() {
		return modi_dttm;
	}
	public void setModi_dttm(String modi_dttm) {
		this.modi_dttm = modi_dttm;
	}
	public String getTrst_orgn_code() {
		return trst_orgn_code;
	}
	public void setTrst_orgn_code(String trst_orgn_code) {
		this.trst_orgn_code = trst_orgn_code;
	}
	public String getRept_cnt() {
		return rept_cnt;
	}
	public void setRept_cnt(String rept_cnt) {
		this.rept_cnt = rept_cnt;
	}
	@Override
	public String toString() {
		return "CommWorksVO [works_id=" + works_id + ", rept_ymd=" + rept_ymd + ", comm_works_id=" + comm_works_id + ", genre_cd=" + genre_cd + ", nation_cd=" + nation_cd + ", works_title=" + works_title + ", works_sub_title="
				+ works_sub_title + ", crt_year=" + crt_year + ", comm_mgnt_cd=" + comm_mgnt_cd + ", rgst_orgn_name=" + rgst_orgn_name + ", icnx_numb=" + icnx_numb + ", rgst_idnt=" + rgst_idnt + ", rgst_dttm=" + rgst_dttm + ", modi_idnt="
				+ modi_idnt + ", modi_dttm=" + modi_dttm + ", trst_orgn_code=" + trst_orgn_code + ", rept_cnt=" + rept_cnt + "]";
	}

}
