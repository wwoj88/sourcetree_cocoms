package cocoms.copyright.entity;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;


public class MemberVO extends DefaultVO {
	private static final long serialVersionUID = 1L;

	private String memberSeqNo     = "";	//
	private String conditionCd     = "";	//
	private String conDetailCd     = "";	//
	private String coGubunCd       = "";	// 업체구분코드 (1:개인사업자, 2:법인사업자)
	private String appNo           = "";	// 신고번호, temp와 상이
	private String appRegDate      = "";	// 등록일, temp와 상이
	private String statCd          = "";	//
	private String coName          = "";	//
	private String coNo            = "";	// 사업자번호
	private String bubNo           = "";	//
	private String trustZipcode    = "";	//
	private String trustSido       = "";	//
	private String trustGugun      = "";	//
	private String trustDong       = "";	//
	private String trustBunji      = "";	//
	private String trustDetailAddr = "";	//
	private String trustTel        = "";	//
	private String trustFax        = "";	//
	private String trustRegDay     = "";	//
	private String ceoName         = "";	//
	private String ceoRegNo        = "";	//
	private String ceoZipcode      = "";	//
	private String ceoSido         = "";	//
	private String ceoGugun        = "";	//
	private String ceoDong         = "";	//
	private String ceoBunji        = "";	//
	private String ceoDetailAddr   = "";	//
	private String ceoTel          = "";	//
	private String ceoFax          = "";	//
	private String delGubun        = "";	//
	private String ceoEmail        = "";	//
	private String trustUrl        = "";	//
	private String ceoMobile       = "";	//
	private String sms             = "";	//
	private String appNo2          = "";	// 신고번호, temp와 상이
	private String regDate2        = "";	// 신청일, temp와 상이
	private String smsAgree        = "";	// SMS/메일수신동의여부
	private String agreeDate       = "";	// SMS/메일수신동의일
	private String terms1Agree     = "";	// 서비스 이용약관 동의여부
	private String terms1AgreeDate = "";	// 서비스 이용약관 동의일
	private String terms2Agree     = "";	// 권리자찾기정보시스템 이용약관 동의여부
	private String terms2AgreeDate = "";	// 권리자찾기정보시스템 이용약관 동의일
	private String terms3Agree     = "";	// 개인정보 수집 및 이용동의여부
	private String terms3AgreeDate = "";	// 개인정보 수집 및 이용동의일

	private String issCnt          = "";	//
	private String reportCnt       = "";	//
	private String enrollmentCnt   = "";	// 현엽수정

	private String file1Path       = "";    //
	private String file2Path       = "";    //
	private String file3Path       = "";    //
	private String file4Path       = "";    //
	private String file5Path  	   = "";
	
	
	private String precoName          = "";	//            preCeoSido         
	private String precoNo            = "";	// 사업자번호 preCeoGugun        
	private String prebubNo           = "";	//            preCeoDong         
	private String pretrustZipcode    = "";	//            preCeoBunji        
	private String pretrustSido       = "";	//            preCeoDetailAddr   
	private String pretrustGugun      = "";	//            preCeoTel          
	private String pretrustDong       = "";	//            preCeoFax          
	private String pretrustBunji      = "";	//            postCeoName        
	private String pretrustDetailAddr = "";	//            postCeoRegNo       
	private String pretrustTel        = "";	//            postCeoEmail       
	private String pretrustFax        = "";	//            postCeoZipcode     
	private String pretrustRegDay     = "";	//            postCeoSido        
	private String preceoName         = "";	//            postCeoGugun       
	private String preceoRegNo        = "";	//            postCeoDong        
	private String preceoZipcode      = "";	//            postCeoBunji       
	private String preceoSido         = "";	//            postCeoDetailAddr  
	private String preceoGugun        = "";	//            postCeoTel         
	private String preceoDong         = "";	//            postCeoFax         
	private String preceoBunji        = "";	//            preCoName          
	private String preceoDetailAddr   = "";	//            preTrustZipcode    
	private String preceoTel          = "";	//            preTrustSido       
	private String preceoFax          = "";	//            preTrustGugun      
	private String predelGubun        = "";	//            preTrustDong       
	private String preceoEmail        = "";	//            preTrustBunji      
	private String pretrustUrl        = "";	//            preTrustDetailAddr 
	private String preceoMobile       = "";	//            preTrustTel        
	private String preSms			  =	"";
	
	
	
	private List<String> chgCd = new ArrayList<String>();
	
	
	
	
	public String getPreSms() {
		return preSms;
	}
	public void setPreSms(String preSms) {
		this.preSms = preSms;
	}
	public List<String> getChgCd() {
		return chgCd;
	}
	public void setChgCd(List<String> chgCd) {
		this.chgCd = chgCd;
	}
	public String getPrecoName() {
		return precoName;
	}
	public void setPrecoName(String precoName) {
		this.precoName = precoName;
	}
	public String getPrecoNo() {
		return precoNo;
	}
	public void setPrecoNo(String precoNo) {
		this.precoNo = precoNo;
	}
	public String getPrebubNo() {
		return prebubNo;
	}
	public void setPrebubNo(String prebubNo) {
		this.prebubNo = prebubNo;
	}
	public String getPretrustZipcode() {
		return pretrustZipcode;
	}
	public void setPretrustZipcode(String pretrustZipcode) {
		this.pretrustZipcode = pretrustZipcode;
	}
	public String getPretrustSido() {
		return pretrustSido;
	}
	public void setPretrustSido(String pretrustSido) {
		this.pretrustSido = pretrustSido;
	}
	public String getPretrustGugun() {
		return pretrustGugun;
	}
	public void setPretrustGugun(String pretrustGugun) {
		this.pretrustGugun = pretrustGugun;
	}
	public String getPretrustDong() {
		return pretrustDong;
	}
	public void setPretrustDong(String pretrustDong) {
		this.pretrustDong = pretrustDong;
	}
	public String getPretrustBunji() {
		return pretrustBunji;
	}
	public void setPretrustBunji(String pretrustBunji) {
		this.pretrustBunji = pretrustBunji;
	}
	public String getPretrustDetailAddr() {
		return pretrustDetailAddr;
	}
	public void setPretrustDetailAddr(String pretrustDetailAddr) {
		this.pretrustDetailAddr = pretrustDetailAddr;
	}
	public String getPretrustTel() {
		return pretrustTel;
	}
	public void setPretrustTel(String pretrustTel) {
		this.pretrustTel = pretrustTel;
	}
	public String getPretrustFax() {
		return pretrustFax;
	}
	public void setPretrustFax(String pretrustFax) {
		this.pretrustFax = pretrustFax;
	}
	public String getPretrustRegDay() {
		return pretrustRegDay;
	}
	public void setPretrustRegDay(String pretrustRegDay) {
		this.pretrustRegDay = pretrustRegDay;
	}
	public String getPreceoName() {
		return preceoName;
	}
	public void setPreceoName(String preceoName) {
		this.preceoName = preceoName;
	}
	public String getPreceoRegNo() {
		return preceoRegNo;
	}
	public void setPreceoRegNo(String preceoRegNo) {
		this.preceoRegNo = preceoRegNo;
	}
	public String getPreceoZipcode() {
		return preceoZipcode;
	}
	public void setPreceoZipcode(String preceoZipcode) {
		this.preceoZipcode = preceoZipcode;
	}
	public String getPreceoSido() {
		return preceoSido;
	}
	public void setPreceoSido(String preceoSido) {
		this.preceoSido = preceoSido;
	}
	public String getPreceoGugun() {
		return preceoGugun;
	}
	public void setPreceoGugun(String preceoGugun) {
		this.preceoGugun = preceoGugun;
	}
	public String getPreceoDong() {
		return preceoDong;
	}
	public void setPreceoDong(String preceoDong) {
		this.preceoDong = preceoDong;
	}
	public String getPreceoBunji() {
		return preceoBunji;
	}
	public void setPreceoBunji(String preceoBunji) {
		this.preceoBunji = preceoBunji;
	}
	public String getPreceoDetailAddr() {
		return preceoDetailAddr;
	}
	public void setPreceoDetailAddr(String preceoDetailAddr) {
		this.preceoDetailAddr = preceoDetailAddr;
	}
	public String getPreceoTel() {
		return preceoTel;
	}
	public void setPreceoTel(String preceoTel) {
		this.preceoTel = preceoTel;
	}
	public String getPreceoFax() {
		return preceoFax;
	}
	public void setPreceoFax(String preceoFax) {
		this.preceoFax = preceoFax;
	}
	public String getPredelGubun() {
		return predelGubun;
	}
	public void setPredelGubun(String predelGubun) {
		this.predelGubun = predelGubun;
	}
	public String getPreceoEmail() {
		return preceoEmail;
	}
	public void setPreceoEmail(String preceoEmail) {
		this.preceoEmail = preceoEmail;
	}
	public String getPretrustUrl() {
		return pretrustUrl;
	}
	public void setPretrustUrl(String pretrustUrl) {
		this.pretrustUrl = pretrustUrl;
	}
	public String getPreceoMobile() {
		return preceoMobile;
	}
	public void setPreceoMobile(String preceoMobile) {
		this.preceoMobile = preceoMobile;
	}
	public MemberVO() {
	}
	public MemberVO(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	
	/*
	 * Special Getters
	 */
	public String getAppNoStr() {
		
		String str = "";
		if (StringUtils.isNotEmpty(appNo2)) {
			if (StringUtils.isNotEmpty(conditionCd) 
					&& "1".equals(conditionCd)) {
				str += "신고";
			}
			else {
				str += "허가";
			}
			str += " 제 ";
			if (StringUtils.isNotEmpty(appNo)) {
				str += appNo + "-";
			}
			str += appNo2 + "호";
		}
		
		return str;
	}
	public String getTrustRegDayStr() {
		
		if (trustRegDay != null && trustRegDay.length() == 8) {
			return trustRegDay.substring(0, 4)+"-"+trustRegDay.substring(4, 6)+"-"+trustRegDay.substring(6, 8);
		}
		
		return appRegDate;
	}
	public String getAppRegDateStr() {
		
		if (appRegDate != null && appRegDate.length() == 8) {
			return appRegDate.substring(0, 4)+"-"+appRegDate.substring(4, 6)+"-"+appRegDate.substring(6, 8);
		}
		
		return appRegDate;
	}
	public String getRegDate2Str() {
		
		if (regDate2 != null && regDate2.length() == 8) {
			return regDate2.substring(0, 4)+"-"+regDate2.substring(4, 6)+"-"+regDate2.substring(6, 8);
		}
		
		return regDate2;
	}
	public String getBubNoStr() {
		
		if (StringUtils.isNotEmpty(bubNo)) {
			return CommonUtils.toFormat(bubNo, "######-#######");
		}
		
		return bubNo;
	}
	public String getCoNoStr() {
		
		if (StringUtils.isNotEmpty(coNo)) {
			return CommonUtils.toFormat(coNo, "###-##-#####");
		}
		
		return coNo;
	}
	public String getAgreeDateStr() {
		
		if (agreeDate != null && agreeDate.length() == 8) {
			return agreeDate.substring(0, 4)+"-"+agreeDate.substring(4, 6)+"-"+agreeDate.substring(6, 8);
		}
		
		return agreeDate;
	}
	public String getTerms1AgreeDateStr() {
		
		if (terms1AgreeDate != null && terms1AgreeDate.length() == 8) {
			return terms1AgreeDate.substring(0, 4)+"-"+terms1AgreeDate.substring(4, 6)+"-"+terms1AgreeDate.substring(6, 8);
		}
		
		return terms1AgreeDate;
	}

	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getTerms1Agree() {
		return terms1Agree;
	}
	public void setTerms1Agree(String terms1Agree) {
		this.terms1Agree = terms1Agree;
	}
	public String getTerms1AgreeDate() {
		return terms1AgreeDate;
	}
	public void setTerms1AgreeDate(String terms1AgreeDate) {
		this.terms1AgreeDate = terms1AgreeDate;
	}
	public String getTerms2Agree() {
		return terms2Agree;
	}
	public void setTerms2Agree(String terms2Agree) {
		this.terms2Agree = terms2Agree;
	}
	public String getTerms2AgreeDate() {
		return terms2AgreeDate;
	}
	public void setTerms2AgreeDate(String terms2AgreeDate) {
		this.terms2AgreeDate = terms2AgreeDate;
	}
	public String getTerms3Agree() {
		return terms3Agree;
	}
	public void setTerms3Agree(String terms3Agree) {
		this.terms3Agree = terms3Agree;
	}
	public String getTerms3AgreeDate() {
		return terms3AgreeDate;
	}
	public void setTerms3AgreeDate(String terms3AgreeDate) {
		this.terms3AgreeDate = terms3AgreeDate;
	}
	public String getAgreeDate() {
		return agreeDate;
	}
	public void setAgreeDate(String agreeDate) {
		this.agreeDate = agreeDate;
	}
	public String getSmsAgree() {
		return smsAgree;
	}
	public void setSmsAgree(String smsAgree) {
		this.smsAgree = smsAgree;
	}
	public String getReportCnt() {
		return reportCnt;
	}
	public void setReportCnt(String reportCnt) {
		this.reportCnt = reportCnt;
	}
	public String getIssCnt() {
		return issCnt;
	}
	public void setIssCnt(String issCnt) {
		this.issCnt = issCnt;
	}
	public String getEnrollmentCnt() {	//현엽수정
		return enrollmentCnt;
	}
	public void setenrollmentCnt(String enrollmentCnt) {	//현엽수정
		this.enrollmentCnt = enrollmentCnt;
	}
	public String getConDetailCd() {
		return conDetailCd;
	}
	public void setConDetailCd(String conDetailCd) {
		this.conDetailCd = conDetailCd;
	}
	public String getFile1Path() {
		return file1Path;
	}
	public void setFile1Path(String file1Path) {
		this.file1Path = file1Path;
	}
	public String getFile2Path() {
		return file2Path;
	}
	public void setFile2Path(String file2Path) {
		this.file2Path = file2Path;
	}
	public String getFile3Path() {
		return file3Path;
	}
	public void setFile3Path(String file3Path) {
		this.file3Path = file3Path;
	}
	public String getFile4Path() {
		return file4Path;
	}
	public void setFile4Path(String file4Path) {
		this.file4Path = file4Path;
	}
	public String getConditionCd() {
		return conditionCd;
	}
	public void setConditionCd(String conditionCd) {
		this.conditionCd = conditionCd;
	}
	public String getCoGubunCd() {
		return coGubunCd;
	}
	public void setCoGubunCd(String coGubunCd) {
		this.coGubunCd = coGubunCd;
	}
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getAppRegDate() {
		return appRegDate;
	}
	public void setAppRegDate(String appRegDate) {
		this.appRegDate = appRegDate;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getCoName() {
		return coName;
	}
	public void setCoName(String coName) {
		this.coName = coName;
	}
	public String getCoNo() {
		return coNo;
	}
	public void setCoNo(String coNo) {
		this.coNo = coNo;
	}
	public String getBubNo() {
		return bubNo;
	}
	public void setBubNo(String bubNo) {
		this.bubNo = bubNo;
	}
	public String getTrustZipcode() {
		return trustZipcode;
	}
	public void setTrustZipcode(String trustZipcode) {
		this.trustZipcode = trustZipcode;
	}
	public String getTrustSido() {
		return trustSido;
	}
	public void setTrustSido(String trustSido) {
		this.trustSido = trustSido;
	}
	public String getTrustGugun() {
		return trustGugun;
	}
	public void setTrustGugun(String trustGugun) {
		this.trustGugun = trustGugun;
	}
	public String getTrustDong() {
		return trustDong;
	}
	public void setTrustDong(String trustDong) {
		this.trustDong = trustDong;
	}
	public String getTrustBunji() {
		return trustBunji;
	}
	public void setTrustBunji(String trustBunji) {
		this.trustBunji = trustBunji;
	}
	public String getTrustDetailAddr() {
		return trustDetailAddr;
	}
	public void setTrustDetailAddr(String trustDetailAddr) {
		this.trustDetailAddr = trustDetailAddr;
	}
	public String getTrustTel() {
		return trustTel;
	}
	public void setTrustTel(String trustTel) {
		this.trustTel = trustTel;
	}
	public String getTrustFax() {
		return trustFax;
	}
	public void setTrustFax(String trustFax) {
		this.trustFax = trustFax;
	}
	public String getTrustRegDay() {
		return trustRegDay;
	}
	public void setTrustRegDay(String trustRegDay) {
		this.trustRegDay = trustRegDay;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getCeoRegNo() {
		return ceoRegNo;
	}
	public void setCeoRegNo(String ceoRegNo) {
		this.ceoRegNo = ceoRegNo;
	}
	public String getCeoZipcode() {
		return ceoZipcode;
	}
	public void setCeoZipcode(String ceoZipcode) {
		this.ceoZipcode = ceoZipcode;
	}
	public String getCeoSido() {
		return ceoSido;
	}
	public void setCeoSido(String ceoSido) {
		this.ceoSido = ceoSido;
	}
	public String getCeoGugun() {
		return ceoGugun;
	}
	public void setCeoGugun(String ceoGugun) {
		this.ceoGugun = ceoGugun;
	}
	public String getCeoDong() {
		return ceoDong;
	}
	public void setCeoDong(String ceoDong) {
		this.ceoDong = ceoDong;
	}
	public String getCeoBunji() {
		return ceoBunji;
	}
	public void setCeoBunji(String ceoBunji) {
		this.ceoBunji = ceoBunji;
	}
	public String getCeoDetailAddr() {
		return ceoDetailAddr;
	}
	public void setCeoDetailAddr(String ceoDetailAddr) {
		this.ceoDetailAddr = ceoDetailAddr;
	}
	public String getCeoTel() {
		return ceoTel;
	}
	public void setCeoTel(String ceoTel) {
		this.ceoTel = ceoTel;
	}
	public String getCeoFax() {
		return ceoFax;
	}
	public void setCeoFax(String ceoFax) {
		this.ceoFax = ceoFax;
	}
	public String getDelGubun() {
		return delGubun;
	}
	public void setDelGubun(String delGubun) {
		this.delGubun = delGubun;
	}
	public String getCeoEmail() {
		return ceoEmail;
	}
	public void setCeoEmail(String ceoEmail) {
		this.ceoEmail = ceoEmail;
	}
	public String getTrustUrl() {
		return trustUrl;
	}
	public void setTrustUrl(String trustUrl) {
		this.trustUrl = trustUrl;
	}
	public String getCeoMobile() {
		return ceoMobile;
	}
	public void setCeoMobile(String ceoMobile) {
		this.ceoMobile = ceoMobile;
	}
	public String getSms() {
		return sms;
	}
	public void setSms(String sms) {
		this.sms = sms;
	}
	public String getAppNo2() {
		return appNo2;
	}
	public void setAppNo2(String appNo2) {
		this.appNo2 = appNo2;
	}
	public String getRegDate2() {
		return regDate2;
	}
	public void setRegDate2(String regDate2) {
		this.regDate2 = regDate2;
	}
	
	public String getFile5Path() {
		return file5Path;
	}
	public void setFile5Path(String file5Path) {
		this.file5Path = file5Path;
	}
	
}