package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

public class ReportVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String memberSeqNo     = "";	//
	private String appNo           = "";	//
	private String coName          = "";	//
	private String year1           = "";	//
	private String year2           = "";	//
	private String year3           = "";	//
	private String year4           = "";	//
	private String year5           = "";	//
	private String appNo1          = "";	//
	private String appNo2          = "";	//
	private String regDate         = "";	//
	private String regTime         = "";	//
	
	/*
	 * Special Getters
	 */
	public String getAppNoStr() {
		
		String str = "";
		if (StringUtils.isNotEmpty(appNo2)) {
			str += "제 ";
			if (StringUtils.isNotEmpty(appNo)) {
				str += appNo + "-";
			}
			str += appNo2 + "호";
		}
		
		return str;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	public String getCoName() {
		return coName;
	}
	public void setCoName(String coName) {
		this.coName = coName;
	}
	public String getYear1() {
		return year1;
	}
	public void setYear1(String year1) {
		this.year1 = year1;
	}
	public String getYear2() {
		return year2;
	}
	public void setYear2(String year2) {
		this.year2 = year2;
	}
	public String getYear3() {
		return year3;
	}
	public void setYear3(String year3) {
		this.year3 = year3;
	}
	public String getYear4() {
		return year4;
	}
	public void setYear4(String year4) {
		this.year4 = year4;
	}
	public String getYear5() {
		return year5;
	}
	public void setYear5(String year5) {
		this.year5 = year5;
	}
	public String getAppNo1() {
		return appNo1;
	}
	public void setAppNo1(String appNo1) {
		this.appNo1 = appNo1;
	}
	public String getAppNo2() {
		return appNo2;
	}
	public void setAppNo2(String appNo2) {
		this.appNo2 = appNo2;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
		
}