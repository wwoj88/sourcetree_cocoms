package cocoms.copyright.entity;

public class ReportStat2VO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String memberSeqNo      = "";	//
	private String name             = "";	//
	private String kind             = "";	//
	private String cnt              = "";	//
	private String tit              = "";	//
	private String sum              = "";	//
	
	private String val11            = "";	//
	private String val12            = "";	//
	private String val13            = "";	//
	
	private String val21            = "";	//
	private String val22            = "";	//
	private String val23            = "";	//
	
	private String val31            = "";	//
	private String val32            = "";	//
	private String val33            = "";	//
	
	private String val41            = "";	//
	private String val42            = "";	//
	private String val43            = "";	//
	
	private String val51            = "";	//
	private String val52            = "";	//
	private String val53            = "";	//
	
	private String organization     = "";	//
	private String collected        = "";	//
	private String dividend         = "";	//
	private String dividendOff      = "";	//
	private String chrg             = "";	//
	private String chrgRate         = "";	//
	
	private String val1             = "";	//
	private String val2             = "";	//
	private String val3             = "";	//
	private String val4             = "";	//
	private String val5             = "";	//
	private String val6             = "";	//
	private String val7             = "";	//
	private String val8             = "";	//
	private String val9             = "";	//
	private String val10            = "";	//

	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public String getCnt() {
		return cnt;
	}
	public void setCnt(String cnt) {
		this.cnt = cnt;
	}
	public String getTit() {
		return tit;
	}
	public void setTit(String tit) {
		this.tit = tit;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}
	public String getVal11() {
		return val11;
	}
	public void setVal11(String val11) {
		this.val11 = val11;
	}
	public String getVal12() {
		return val12;
	}
	public void setVal12(String val12) {
		this.val12 = val12;
	}
	public String getVal13() {
		return val13;
	}
	public void setVal13(String val13) {
		this.val13 = val13;
	}
	public String getVal21() {
		return val21;
	}
	public void setVal21(String val21) {
		this.val21 = val21;
	}
	public String getVal22() {
		return val22;
	}
	public void setVal22(String val22) {
		this.val22 = val22;
	}
	public String getVal23() {
		return val23;
	}
	public void setVal23(String val23) {
		this.val23 = val23;
	}
	public String getVal31() {
		return val31;
	}
	public void setVal31(String val31) {
		this.val31 = val31;
	}
	public String getVal32() {
		return val32;
	}
	public void setVal32(String val32) {
		this.val32 = val32;
	}
	public String getVal33() {
		return val33;
	}
	public void setVal33(String val33) {
		this.val33 = val33;
	}
	public String getVal41() {
		return val41;
	}
	public void setVal41(String val41) {
		this.val41 = val41;
	}
	public String getVal42() {
		return val42;
	}
	public void setVal42(String val42) {
		this.val42 = val42;
	}
	public String getVal43() {
		return val43;
	}
	public void setVal43(String val43) {
		this.val43 = val43;
	}
	public String getVal51() {
		return val51;
	}
	public void setVal51(String val51) {
		this.val51 = val51;
	}
	public String getVal52() {
		return val52;
	}
	public void setVal52(String val52) {
		this.val52 = val52;
	}
	public String getVal53() {
		return val53;
	}
	public void setVal53(String val53) {
		this.val53 = val53;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getCollected() {
		return collected;
	}
	public void setCollected(String collected) {
		this.collected = collected;
	}
	public String getDividend() {
		return dividend;
	}
	public void setDividend(String dividend) {
		this.dividend = dividend;
	}
	public String getDividendOff() {
		return dividendOff;
	}
	public void setDividendOff(String dividendOff) {
		this.dividendOff = dividendOff;
	}
	public String getChrg() {
		return chrg;
	}
	public void setChrg(String chrg) {
		this.chrg = chrg;
	}
	public String getChrgRate() {
		return chrgRate;
	}
	public void setChrgRate(String chrgRate) {
		this.chrgRate = chrgRate;
	}
	public String getVal1() {
		return val1;
	}
	public void setVal1(String val1) {
		this.val1 = val1;
	}
	public String getVal2() {
		return val2;
	}
	public void setVal2(String val2) {
		this.val2 = val2;
	}
	public String getVal3() {
		return val3;
	}
	public void setVal3(String val3) {
		this.val3 = val3;
	}
	public String getVal4() {
		return val4;
	}
	public void setVal4(String val4) {
		this.val4 = val4;
	}
	public String getVal5() {
		return val5;
	}
	public void setVal5(String val5) {
		this.val5 = val5;
	}
	public String getVal6() {
		return val6;
	}
	public void setVal6(String val6) {
		this.val6 = val6;
	}
	public String getVal7() {
		return val7;
	}
	public void setVal7(String val7) {
		this.val7 = val7;
	}
	public String getVal8() {
		return val8;
	}
	public void setVal8(String val8) {
		this.val8 = val8;
	}
	public String getVal9() {
		return val9;
	}
	public void setVal9(String val9) {
		this.val9 = val9;
	}
	public String getVal10() {
		return val10;
	}
	public void setVal10(String val10) {
		this.val10 = val10;
	}
	
}