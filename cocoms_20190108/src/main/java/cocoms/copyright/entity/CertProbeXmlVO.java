package cocoms.copyright.entity;

public class CertProbeXmlVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String printId        = "";	//
	private String no             = "";	//
	private String xmlData        = "";	//
	private String layout         = "";	//

	public CertProbeXmlVO() {
	}
	public CertProbeXmlVO(String printId) {
		this.printId = printId;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getPrintId() {
		return printId;
	}
	public void setPrintId(String printId) {
		this.printId = printId;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getXmlData() {
		return xmlData;
	}
	public void setXmlData(String xmlData) {
		this.xmlData = xmlData;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
}