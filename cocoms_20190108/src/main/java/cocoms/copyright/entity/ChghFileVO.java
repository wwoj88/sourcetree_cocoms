package cocoms.copyright.entity;

public class ChghFileVO extends DefaultVO {
	
	private static final long serialVersionUID = 1L;

	private String chghfileseqno    	= "";	//
	private String filename     	    = "";	//
	private String filepath     		= "";	//
	private String regdate              = "";
	private String regtime				= "";
	private String fsize				= "";
	private String delgubun				= "";
	private String chghistoryseqno	    = "";
	private String memberseqno          = "";
	
	
	public String getMemberseqno() {
		return memberseqno;
	}
	public void setMemberseqno(String memberseqno) {
		this.memberseqno = memberseqno;
	}
	public String getChghfileseqno() {
		return chghfileseqno;
	}
	public void setChghfileseqno(String chghfileseqno) {
		this.chghfileseqno = chghfileseqno;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getRegtime() {
		return regtime;
	}
	public void setRegtime(String regtime) {
		this.regtime = regtime;
	}
	public String getFsize() {
		return fsize;
	}
	public void setFsize(String fsize) {
		this.fsize = fsize;
	}
	public String getDelgubun() {
		return delgubun;
	}
	public void setDelgubun(String delgubun) {
		this.delgubun = delgubun;
	}
	public String getChghistoryseqno() {
		return chghistoryseqno;
	}
	public void setChghistoryseqno(String chghistoryseqno) {
		this.chghistoryseqno = chghistoryseqno;
	}
	

}
