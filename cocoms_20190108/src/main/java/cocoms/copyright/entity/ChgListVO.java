package cocoms.copyright.entity;

public class ChgListVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String strPre1 = "";
	private String strPre2 = "";
	private String strPre3 = "";
	private String strPre4 = "";
	private String strPre5 = "";
	private String strPre6 = "";
	private String strPre7 = "";
	private String strPre8 = "";
	private String strPre9 = "";
	private String strPre10 = "";
	private String strPre11= "";
	private String strPre12 = "";
	private String strPre13 = "";
	private String strPre14 = "";
	private String strPre15= "";
	private String strPost1 = "";
	private String strPost2 = "";
	private String strPost3 = "";
	private String strPost4 = "";
	private String strPost5 = "";
	private String strPost6=  "";
	private String strPost7 = "";
	private String strPost8 = "";
	private String strPost9 = "";
	private String strPost10 = "";
	private String strPost11 = "";
	private String strPost12 = "";
	private String strPost13 = "";
	private String strPost14 = "";
	private String strPost15 = "";
	public String getStrPre1() {
		return strPre1;
	}
	public void setStrPre1(String strPre1) {
		this.strPre1 = strPre1;
	}
	public String getStrPre2() {
		return strPre2;
	}
	public void setStrPre2(String strPre2) {
		this.strPre2 = strPre2;
	}
	public String getStrPre3() {
		return strPre3;
	}
	public void setStrPre3(String strPre3) {
		this.strPre3 = strPre3;
	}
	public String getStrPre4() {
		return strPre4;
	}
	public void setStrPre4(String strPre4) {
		this.strPre4 = strPre4;
	}
	public String getStrPre5() {
		return strPre5;
	}
	public void setStrPre5(String strPre5) {
		this.strPre5 = strPre5;
	}
	public String getStrPre6() {
		return strPre6;
	}
	public void setStrPre6(String strPre6) {
		this.strPre6 = strPre6;
	}
	public String getStrPre7() {
		return strPre7;
	}
	public void setStrPre7(String strPre7) {
		this.strPre7 = strPre7;
	}
	public String getStrPre8() {
		return strPre8;
	}
	public void setStrPre8(String strPre8) {
		this.strPre8 = strPre8;
	}
	public String getStrPre9() {
		return strPre9;
	}
	public void setStrPre9(String strPre9) {
		this.strPre9 = strPre9;
	}
	public String getStrPre10() {
		return strPre10;
	}
	public void setStrPre10(String strPre10) {
		this.strPre10 = strPre10;
	}
	public String getStrPre11() {
		return strPre11;
	}
	public void setStrPre11(String strPre11) {
		this.strPre11 = strPre11;
	}
	public String getStrPre12() {
		return strPre12;
	}
	public void setStrPre12(String strPre12) {
		this.strPre12 = strPre12;
	}
	public String getStrPre13() {
		return strPre13;
	}
	public void setStrPre13(String strPre13) {
		this.strPre13 = strPre13;
	}
	public String getStrPre14() {
		return strPre14;
	}
	public void setStrPre14(String strPre14) {
		this.strPre14 = strPre14;
	}
	public String getStrPre15() {
		return strPre15;
	}
	public void setStrPre15(String strPre15) {
		this.strPre15 = strPre15;
	}
	public String getStrPost1() {
		return strPost1;
	}
	public void setStrPost1(String strPost1) {
		this.strPost1 = strPost1;
	}
	public String getStrPost2() {
		return strPost2;
	}
	public void setStrPost2(String strPost2) {
		this.strPost2 = strPost2;
	}
	public String getStrPost3() {
		return strPost3;
	}
	public void setStrPost3(String strPost3) {
		this.strPost3 = strPost3;
	}
	public String getStrPost4() {
		return strPost4;
	}
	public void setStrPost4(String strPost4) {
		this.strPost4 = strPost4;
	}
	public String getStrPost5() {
		return strPost5;
	}
	public void setStrPost5(String strPost5) {
		this.strPost5 = strPost5;
	}
	public String getStrPost6() {
		return strPost6;
	}
	public void setStrPost6(String strPost6) {
		this.strPost6 = strPost6;
	}
	public String getStrPost7() {
		return strPost7;
	}
	public void setStrPost7(String strPost7) {
		this.strPost7 = strPost7;
	}
	public String getStrPost8() {
		return strPost8;
	}
	public void setStrPost8(String strPost8) {
		this.strPost8 = strPost8;
	}
	public String getStrPost9() {
		return strPost9;
	}
	public void setStrPost9(String strPost9) {
		this.strPost9 = strPost9;
	}
	public String getStrPost10() {
		return strPost10;
	}
	public void setStrPost10(String strPost10) {
		this.strPost10 = strPost10;
	}
	public String getStrPost11() {
		return strPost11;
	}
	public void setStrPost11(String strPost11) {
		this.strPost11 = strPost11;
	}
	public String getStrPost12() {
		return strPost12;
	}
	public void setStrPost12(String strPost12) {
		this.strPost12 = strPost12;
	}
	public String getStrPost13() {
		return strPost13;
	}
	public void setStrPost13(String strPost13) {
		this.strPost13 = strPost13;
	}
	public String getStrPost14() {
		return strPost14;
	}
	public void setStrPost14(String strPost14) {
		this.strPost14 = strPost14;
	}
	public String getStrPost15() {
		return strPost15;
	}
	public void setStrPost15(String strPost15) {
		this.strPost15 = strPost15;
	}
	 
	
	
	
	
	
	
}
