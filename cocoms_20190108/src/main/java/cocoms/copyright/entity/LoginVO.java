package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class LoginVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String loginId     = "";	//
	private String memberSeqNo = "";	//
	private String pwd         = "";	//
	private String regDate     = "";	//
	private String delGubun    = "";	//
	private String regNo       = "";	//
	private String coGubun     = "";	//
	private String lastLoginDate	= "";	//
	private String lastLoginTime	= "";	//

	private String certType      = "";	// 1: 개인, 2: 법인
	
	private String statCd          = "";	//
	private String ceoName         = "";	//
	private String ceoMobile       = "";	//
	private String ceoEmail        = "";	//
	private String conditionCd     = "";	//
	private String conDetailCd     = "";	//
	private String coName          = "";	//
	private String coNo            = "";	// 사업자번호
	private String bubNo           = "";	//
	private String sms             = "";	//
	private String smsAgree        = "";
	private String appno2 		   = "";
	public LoginVO() {
	}
	public LoginVO(String loginId) {
		this.loginId = loginId;
	}
	
	public String getAppno2() {
		return appno2;
	}
	public void setAppno2(String appno2) {
		this.appno2 = appno2;
	}
	/*
	 * Special Getters
	 */
	public String getRegDateStr() {
		
		if (regDate != null && regDate.length() == 8) {
			return regDate.substring(0,  4)+"-"+regDate.substring(4, 6)+"-"+regDate.substring(6, 8);
		}
		
		return regDate;
	}
	public String getLastLoginDateStr() {
		
		if (lastLoginDate != null && lastLoginDate.length() == 8) {
			return lastLoginDate.substring(0,  4)+"-"+lastLoginDate.substring(4, 6)+"-"+lastLoginDate.substring(6, 8);
		}
		
		return lastLoginDate;
	}
	public String getLastLoginTimeStr() {
		
		if (lastLoginTime != null && lastLoginTime.length() == 6) {
			return lastLoginTime.substring(0, 2)+":"+lastLoginTime.substring(2, 4)+":"+lastLoginTime.substring(4, 6);
		}
		
		return lastLoginTime;
	}
	public String getBubNoStr() {
		
		if (StringUtils.isNotEmpty(bubNo)) {
			return CommonUtils.toFormat(bubNo, "######-#######");
		}
		
		return bubNo;
	}
	public String getCoNoStr() {
		
		if (StringUtils.isNotEmpty(coNo)) {
			return CommonUtils.toFormat(coNo, "###-##-#####");
		}
		
		return coNo;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getSmsAgree() {
		return smsAgree;
	}
	public void setSmsAgree(String smsAgree) {
		this.smsAgree = smsAgree;
	}
	public String getSms() {
		return sms;
	}
	public void setSms(String sms) {
		this.sms = sms;
	}
	public String getCoName() {
		return coName;
	}
	public void setCoName(String coName) {
		this.coName = coName;
	}
	public String getConditionCd() {
		return conditionCd;
	}
	public void setConditionCd(String conditionCd) {
		this.conditionCd = conditionCd;
	}
	public String getConDetailCd() {
		return conDetailCd;
	}
	public void setConDetailCd(String conDetailCd) {
		this.conDetailCd = conDetailCd;
	}
	public String getCoNo() {
		return coNo;
	}
	public void setCoNo(String coNo) {
		this.coNo = coNo;
	}
	public String getBubNo() {
		return bubNo;
	}
	public void setBubNo(String bubNo) {
		this.bubNo = bubNo;
	}
	public String getCeoMobile() {
		return ceoMobile;
	}
	public void setCeoMobile(String ceoMobile) {
		this.ceoMobile = ceoMobile;
	}
	public String getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getCeoEmail() {
		return ceoEmail;
	}
	public void setCeoEmail(String ceoEmail) {
		this.ceoEmail = ceoEmail;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getDelGubun() {
		return delGubun;
	}
	public void setDelGubun(String delGubun) {
		this.delGubun = delGubun;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getCoGubun() {
		return coGubun;
	}
	public void setCoGubun(String coGubun) {
		this.coGubun = coGubun;
	}
}