package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class SmsTemplateVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String mainCode    = "";	//
	private String detailCode  = "";	//
	
	private String smsType     = "";	//
	private String subject     = "";	//
	private String content     = "";	//
	
	private String regId       = "";	//
	private String regDate     = "";	//
	private String regTime     = "";	//
	private String modId       = "";	//
	private String modDate     = "";	//
	private String modTime     = "";	//
	private String useYn       = "";	//
	
	/*
	 * Getters and Setters
	 */
	public String getMainCode() {
		return mainCode;
	}
	public void setMainCode(String mainCode) {
		this.mainCode = mainCode;
	}
	public String getDetailCode() {
		return detailCode;
	}
	public void setDetailCode(String detailCode) {
		this.detailCode = detailCode;
	}
	public String getSmsType() {
		return smsType;
	}
	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getModTime() {
		return modTime;
	}
	public void setModTime(String modTime) {
		this.modTime = modTime;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}		
}