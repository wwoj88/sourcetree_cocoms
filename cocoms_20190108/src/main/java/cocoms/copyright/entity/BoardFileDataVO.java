package cocoms.copyright.entity;

public class BoardFileDataVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String boardFileDataSeqNo = ""; 	// 
	private String boardSeqNo         = ""; 	// 
	private String regId              = ""; 	// 
	private String filename           = ""; 	// 
	private String maskName           = ""; 	// 
	private String regDate            = ""; 	// 
	private String regTime            = ""; 	// 
	private String filesize           = ""; 	// 
	private String delGubun           = ""; 	// 
	private String guid               = ""; 	//
	
	public BoardFileDataVO() {
	}
	public BoardFileDataVO(String boardSeqNo) {
		this.boardSeqNo = boardSeqNo;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getBoardSeqNo() {
		return boardSeqNo;
	}
	public void setBoardSeqNo(String boardSeqNo) {
		this.boardSeqNo = boardSeqNo;
	}
	public String getBoardFileDataSeqNo() {
		return boardFileDataSeqNo;
	}
	public void setBoardFileDataSeqNo(String boardFileDataSeqNo) {
		this.boardFileDataSeqNo = boardFileDataSeqNo;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getMaskName() {
		return maskName;
	}
	public void setMaskName(String maskName) {
		this.maskName = maskName;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getFilesize() {
		return filesize;
	}
	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}
	public String getDelGubun() {
		return delGubun;
	}
	public void setDelGubun(String delGubun) {
		this.delGubun = delGubun;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}

}
