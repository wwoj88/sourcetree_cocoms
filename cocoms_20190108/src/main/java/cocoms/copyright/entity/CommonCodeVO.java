package cocoms.copyright.entity;

public class CommonCodeVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String commonCodeSeqNo = "";	//
	private String codeGubun       = "";	//
	private String codeDesc        = "";	//
	private String codeValue       = "";	//
	private String codeValueDesc   = "";	//
	private String codeOrder       = "";	//
	private String orderYn         = "";	//
	
	/*
	 * Getters and Setters
	 */
	public String getCommonCodeSeqNo() {
		return commonCodeSeqNo;
	}
	public void setCommonCodeSeqNo(String commonCodeSeqNo) {
		this.commonCodeSeqNo = commonCodeSeqNo;
	}
	public String getCodeGubun() {
		return codeGubun;
	}
	public void setCodeGubun(String codeGubun) {
		this.codeGubun = codeGubun;
	}
	public String getCodeDesc() {
		return codeDesc;
	}
	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}
	public String getCodeValue() {
		return codeValue;
	}
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}
	public String getCodeValueDesc() {
		return codeValueDesc;
	}
	public void setCodeValueDesc(String codeValueDesc) {
		this.codeValueDesc = codeValueDesc;
	}
	public String getCodeOrder() {
		return codeOrder;
	}
	public void setCodeOrder(String codeOrder) {
		this.codeOrder = codeOrder;
	}
	public String getOrderYn() {
		return orderYn;
	}
	public void setOrderYn(String orderYn) {
		this.orderYn = orderYn;
	}
}