package cocoms.copyright.entity;

public class TrustStat6VO extends TrustInfoVO {

	private static final long serialVersionUID = 1L;

	private String trustCopyrightSeqNo	= "";	//
	private String trustExecSeqNo	    = "";	//
	private String trustMakerSeqNo	    = "";	//
	
	private String trustInfoSeqNo     	= "";	//
	private String gubun            	= "";	//
	private String organization     	= "";	//
	private String collected        	= "";	//
	private String dividend         	= "";	//
	private String dividendOff      	= "";	//
	private String chrg             	= "";	//
	private String chrgRate         	= "";	//
	private String memo             	= "";	//

	private String gubunName           	= "";	//

	/*
	 * Getters and Setters
	 */
	public String getTrustInfoSeqNo() {
		return trustInfoSeqNo;
	}
	public void setTrustInfoSeqNo(String trustInfoSeqNo) {
		this.trustInfoSeqNo = trustInfoSeqNo;
	}
	public String getGubunName() {
		return gubunName;
	}
	public void setGubunName(String gubunName) {
		this.gubunName = gubunName;
	}
	public String getTrustCopyrightSeqNo() {
		return trustCopyrightSeqNo;
	}
	public void setTrustCopyrightSeqNo(String trustCopyrightSeqNo) {
		this.trustCopyrightSeqNo = trustCopyrightSeqNo;
	}
	public String getTrustExecSeqNo() {
		return trustExecSeqNo;
	}
	public void setTrustExecSeqNo(String trustExecSeqNo) {
		this.trustExecSeqNo = trustExecSeqNo;
	}
	public String getTrustMakerSeqNo() {
		return trustMakerSeqNo;
	}
	public void setTrustMakerSeqNo(String trustMakerSeqNo) {
		this.trustMakerSeqNo = trustMakerSeqNo;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getCollected() {
		return collected;
	}
	public void setCollected(String collected) {
		this.collected = collected;
	}
	public String getDividend() {
		return dividend;
	}
	public void setDividend(String dividend) {
		this.dividend = dividend;
	}
	public String getDividendOff() {
		return dividendOff;
	}
	public void setDividendOff(String dividendOff) {
		this.dividendOff = dividendOff;
	}
	public String getChrg() {
		return chrg;
	}
	public void setChrg(String chrg) {
		this.chrg = chrg;
	}
	public String getChrgRate() {
		return chrgRate;
	}
	public void setChrgRate(String chrgRate) {
		this.chrgRate = chrgRate;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}