package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class SmsVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String num = "";			//
	private String sendDate = "";		//
	private String sendStatus = "";		// 0: 발송대기, 1: 전송완료, 2: 결과수신완료
	private String messageType = "";	// 0: 일반메시지, 1: 콜백URL메시지
	private String phone = "";			//
	private String callback = "";		//
	private String message = "";		//
	
	public SmsVO() {
	}
	public SmsVO(String to, String message, String from) {
		this.phone = to;
		this.message = message;
		this.callback = from;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getSendDate() {
		return sendDate;
	}
	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}
	public String getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCallback() {
		return callback;
	}
	public void setCallback(String callback) {
		this.callback = callback;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}