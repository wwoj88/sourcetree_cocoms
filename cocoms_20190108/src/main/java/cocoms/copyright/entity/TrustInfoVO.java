package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

public class TrustInfoVO extends ReportVO {

	private static final long serialVersionUID = 1L;

	private String trustInfoSeqno         = "";	//
	private String memberSeqNo            = "";	//
	private String rptYear                = "";	//
	private String thisMember             = "";	//
	private String thisGroup              = "";	//
	private String thisWorknum            = "";	//
	private String thisTrustAccnt         = "";	//
	private String thisIndemnityAccnt     = "";	//
	private String thisGeneralAccnt       = "";	//
	private String lastTrustAccnt         = "";	//
	private String lastIndemnityAccnt     = "";	//
	private String lastGeneralAccnt       = "";	//
	private String lastMember             = "";	//
	private String lastGroup              = "";	//
	private String lastWorknum            = "";	//
	private String businessGoal           = "";	//
	private String prop                   = "";	//
	private String regDate                = "";	//
	private String regTime                = "";	//

	private String filename1              = "";	//
	private String filepath1              = "";	//
	private String filename2              = "";	//
	private String filepath2              = "";	//
	private String filename3              = "";	//
	private String filepath3              = "";	//
	private String filename4              = "";	//
	private String filepath4              = "";	//
	
	private String filename5              = "";	//
	private String filepath5              = "";	//
	private String filename6              = "";	//
	private String filepath6              = "";	//
	private String filename7              = "";	//
	private String filepath7              = "";	//
	private String filename8              = "";	//
	private String filepath8              = "";	//
	private String filename9              = "";	//
	private String filepath9              = "";	//
	
	private String codeValue              = "";	//
	private String filesize               = "";	//

	public TrustInfoVO() {
	}
	public TrustInfoVO(String memberSeqNo, String rptYear) {
		this.memberSeqNo = memberSeqNo;
		this.rptYear = rptYear;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getFilename5() {
		return filename5;
	}
	public void setFilename5(String filename5) {
		this.filename5 = filename5;
	}
	public String getFilepath5() {
		return filepath5;
	}
	public void setFilepath5(String filepath5) {
		this.filepath5 = filepath5;
	}
	public String getFilename6() {
		return filename6;
	}
	public void setFilename6(String filename6) {
		this.filename6 = filename6;
	}
	public String getFilepath6() {
		return filepath6;
	}
	public void setFilepath6(String filepath6) {
		this.filepath6 = filepath6;
	}
	public String getFilename7() {
		return filename7;
	}
	public void setFilename7(String filename7) {
		this.filename7 = filename7;
	}
	public String getFilepath7() {
		return filepath7;
	}
	public void setFilepath7(String filepath7) {
		this.filepath7 = filepath7;
	}
	public String getFilename8() {
		return filename8;
	}
	public void setFilename8(String filename8) {
		this.filename8 = filename8;
	}
	public String getFilepath8() {
		return filepath8;
	}
	public void setFilepath8(String filepath8) {
		this.filepath8 = filepath8;
	}
	public String getFilename9() {
		return filename9;
	}
	public void setFilename9(String filename9) {
		this.filename9 = filename9;
	}
	public String getFilepath9() {
		return filepath9;
	}
	public void setFilepath9(String filepath9) {
		this.filepath9 = filepath9;
	}
	public String getFilesize() {
		return filesize;
	}
	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}
	public String getFilename1() {
		return filename1;
	}
	public String getCodeValue() {
		return codeValue;
	}
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}
	public void setFilename1(String filename1) {
		this.filename1 = filename1;
	}
	public String getFilepath1() {
		return filepath1;
	}
	public void setFilepath1(String filepath1) {
		this.filepath1 = filepath1;
	}
	public String getFilename2() {
		return filename2;
	}
	public void setFilename2(String filename2) {
		this.filename2 = filename2;
	}
	public String getFilepath2() {
		return filepath2;
	}
	public void setFilepath2(String filepath2) {
		this.filepath2 = filepath2;
	}
	public String getFilename3() {
		return filename3;
	}
	public void setFilename3(String filename3) {
		this.filename3 = filename3;
	}
	public String getFilepath3() {
		return filepath3;
	}
	public void setFilepath3(String filepath3) {
		this.filepath3 = filepath3;
	}
	public String getFilename4() {
		return filename4;
	}
	public void setFilename4(String filename4) {
		this.filename4 = filename4;
	}
	public String getFilepath4() {
		return filepath4;
	}
	public void setFilepath4(String filepath4) {
		this.filepath4 = filepath4;
	}
	public String getTrustInfoSeqno() {
		return trustInfoSeqno;
	}
	public void setTrustInfoSeqno(String trustInfoSeqno) {
		this.trustInfoSeqno = trustInfoSeqno;
	}
	public String getRptYear() {
		return rptYear;
	}
	public void setRptYear(String rptYear) {
		this.rptYear = rptYear;
	}
	public String getThisMember() {
		return thisMember;
	}
	public void setThisMember(String thisMember) {
		this.thisMember = thisMember;
	}
	public String getThisGroup() {
		return thisGroup;
	}
	public void setThisGroup(String thisGroup) {
		this.thisGroup = thisGroup;
	}
	public String getThisWorknum() {
		return thisWorknum;
	}
	public void setThisWorknum(String thisWorknum) {
		this.thisWorknum = thisWorknum;
	}
	public String getThisTrustAccnt() {
		return thisTrustAccnt;
	}
	public void setThisTrustAccnt(String thisTrustAccnt) {
		this.thisTrustAccnt = thisTrustAccnt;
	}
	public String getThisIndemnityAccnt() {
		return thisIndemnityAccnt;
	}
	public void setThisIndemnityAccnt(String thisIndemnityAccnt) {
		this.thisIndemnityAccnt = thisIndemnityAccnt;
	}
	public String getThisGeneralAccnt() {
		return thisGeneralAccnt;
	}
	public void setThisGeneralAccnt(String thisGeneralAccnt) {
		this.thisGeneralAccnt = thisGeneralAccnt;
	}
	public String getLastTrustAccnt() {
		return lastTrustAccnt;
	}
	public void setLastTrustAccnt(String lastTrustAccnt) {
		this.lastTrustAccnt = lastTrustAccnt;
	}
	public String getLastIndemnityAccnt() {
		return lastIndemnityAccnt;
	}
	public void setLastIndemnityAccnt(String lastIndemnityAccnt) {
		this.lastIndemnityAccnt = lastIndemnityAccnt;
	}
	public String getLastGeneralAccnt() {
		return lastGeneralAccnt;
	}
	public void setLastGeneralAccnt(String lastGeneralAccnt) {
		this.lastGeneralAccnt = lastGeneralAccnt;
	}
	public String getLastMember() {
		return lastMember;
	}
	public void setLastMember(String lastMember) {
		this.lastMember = lastMember;
	}
	public String getLastGroup() {
		return lastGroup;
	}
	public void setLastGroup(String lastGroup) {
		this.lastGroup = lastGroup;
	}
	public String getLastWorknum() {
		return lastWorknum;
	}
	public void setLastWorknum(String lastWorknum) {
		this.lastWorknum = lastWorknum;
	}
	public String getBusinessGoal() {
		return businessGoal;
	}
	public void setBusinessGoal(String businessGoal) {
		this.businessGoal = businessGoal;
	}
	public String getProp() {
		return prop;
	}
	public void setProp(String prop) {
		this.prop = prop;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
		
}