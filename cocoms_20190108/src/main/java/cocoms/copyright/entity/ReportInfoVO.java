package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

public class ReportInfoVO extends ReportVO {

	private static final long serialVersionUID = 1L;

	private String memberSeqNo     = "";	//
	private String rptYear         = "";	//
	private String regName         = "";	//
	private String regPosi         = "";	//
	private String regTel          = "";	//
	private String regEmail        = "";	//
	private String staffNum        = "";	//
	private String listingCd       = "";	//
	private String actrstFileName  = "";	//
	private String actrstMaskName  = "";	//
	private String planFileName    = "";	//
	private String planMaskName    = "";	//
	private String memo            = "";	//
	private String regDate         = "";	//
	private String regTime         = "";	//
	
	private String startDate = "";	// 대리중계실적보고 시작일
	private String endDate   = "";	// 대리중계실적보고 종료일

	public ReportInfoVO() {
	}
	public ReportInfoVO(String memberSeqNo, String rptYear) {
		this.memberSeqNo = memberSeqNo;
		this.rptYear = rptYear;
	}
	
	/*
	 * 
	 */
	public String getListingCdStr() {
		
		if (StringUtils.isNotEmpty(listingCd)) {
			if ("1".equals(listingCd)) {
				return "코스닥";
			}
			else if ("2".equals(listingCd)) {
				return "코스피";
			}
			else if ("3".equals(listingCd)) {
				return "비상장";
			}
			else if ("4".equals(listingCd)) {
				return "기타";
			}
		}
		
		return listingCd;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getActrstFileName() {
		return actrstFileName;
	}
	public void setActrstFileName(String actrstFileName) {
		this.actrstFileName = actrstFileName;
	}
	public String getPlanFileName() {
		return planFileName;
	}
	public void setPlanFileName(String planFileName) {
		this.planFileName = planFileName;
	}
	public String getRptYear() {
		return rptYear;
	}
	public void setRptYear(String rptYear) {
		this.rptYear = rptYear;
	}
	public String getRegName() {
		return regName;
	}
	public void setRegName(String regName) {
		this.regName = regName;
	}
	public String getRegPosi() {
		return regPosi;
	}
	public void setRegPosi(String regPosi) {
		this.regPosi = regPosi;
	}
	public String getRegTel() {
		return regTel;
	}
	public void setRegTel(String regTel) {
		this.regTel = regTel;
	}
	public String getRegEmail() {
		return regEmail;
	}
	public void setRegEmail(String regEmail) {
		this.regEmail = regEmail;
	}
	public String getStaffNum() {
		return staffNum;
	}
	public void setStaffNum(String staffNum) {
		this.staffNum = staffNum;
	}
	public String getListingCd() {
		return listingCd;
	}
	public void setListingCd(String listingCd) {
		this.listingCd = listingCd;
	}
	public String getActrstMaskName() {
		return actrstMaskName;
	}
	public void setActrstMaskName(String actrstMaskName) {
		this.actrstMaskName = actrstMaskName;
	}
	public String getPlanMaskName() {
		return planMaskName;
	}
	public void setPlanMaskName(String planMaskName) {
		this.planMaskName = planMaskName;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	
}