package cocoms.copyright.entity;

public class ActionLogVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String loginId		= "";	//
	private String userIp     	= "";	//
	private String action     	= "";	//
	private String url     		= "";	//
	
	private String regId           = "";	//
	private String regDate         = "";	//
	private String regTime         = "";	//
	private String modId           = "";	//
	private String modDate         = "";	//
	private String modTime         = "";	//
	private String useYn           = "";	//

	private String ceoName         = "";	//
	private String coName          = "";	//

	public String getRegDateStr() {
		
		if (regDate != null && regDate.length() == 8) {
			return regDate.substring(0, 4)+"-"+regDate.substring(4, 6)+"-"+regDate.substring(6, 8);
		}
		
		return regDate;
	}
	public String getRegTimeStr() {
		
		if (regTime != null && regTime.length() == 6) {
			return regTime.substring(0, 2)+":"+regTime.substring(2, 4)+":"+regTime.substring(4, 6);
		}
		
		return regTime;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getModTime() {
		return modTime;
	}
	public void setModTime(String modTime) {
		this.modTime = modTime;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getCoName() {
		return coName;
	}
	public void setCoName(String coName) {
		this.coName = coName;
	}
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}