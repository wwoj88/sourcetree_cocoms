package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class ApplHistoryVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String applSeqNo       = "";	//
	private String docType         = "";	//
	private String memberSeqNo     = "";	//
	private String coName          = "";	//
	private String coNo            = "";	//
	private String applContent     = "";	//
	private String pdfContent      = "";	//
	private String regId           = "";	//
	private String regDate         = "";	//
	private String regTime         = "";	//
	private String modId           = "";	//
	private String modDate         = "";	//
	private String modTime         = "";	//
	private String useYn           = "";	//
	
	public ApplHistoryVO() {
	}
	public ApplHistoryVO(String applSeqNo) {
		this.applSeqNo = applSeqNo;
	}
	
	/*
	 * Special Getters
	 */
	public String getRegDateStr() {
		
		if (regDate != null && regDate.length() == 8) {
			return regDate.substring(0,  4)+"-"+regDate.substring(4, 6)+"-"+regDate.substring(6, 8);
		}
		
		return regDate;
	}
	public String getCoNoStr() {
		
		if (StringUtils.isNotEmpty(coNo)) {
			return CommonUtils.toFormat(coNo, "###-##-#####");
		}
		
		return coNo;
	}

	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getApplSeqNo() {
		return applSeqNo;
	}
	public void setApplSeqNo(String applSeqNo) {
		this.applSeqNo = applSeqNo;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getCoName() {
		return coName;
	}
	public void setCoName(String coName) {
		this.coName = coName;
	}
	public String getCoNo() {
		return coNo;
	}
	public void setCoNo(String coNo) {
		this.coNo = coNo;
	}
	public String getApplContent() {
		return applContent;
	}
	public void setApplContent(String applContent) {
		this.applContent = applContent;
	}
	public String getPdfContent() {
		return pdfContent;
	}
	public void setPdfContent(String pdfContent) {
		this.pdfContent = pdfContent;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getModTime() {
		return modTime;
	}
	public void setModTime(String modTime) {
		this.modTime = modTime;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
		
}