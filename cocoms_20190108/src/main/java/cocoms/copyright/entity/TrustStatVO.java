package cocoms.copyright.entity;

public class TrustStatVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String trustInfoSeqNo          = "";	//
	private String rptYear                 = "";	//
	private String businessGoal            = "";	//
	private String prop                    = "";	//
	private String trustCopyrightSeqNo     = "";	//
	private String trustExecSeqNo          = "";	//
	private String trustMakerSeqNo         = "";	//
	private String trustRoyaltySeqNo       = "";	//

	private String memberSeqNo             = "";	//

	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getTrustMakerSeqNo() {
		return trustMakerSeqNo;
	}
	public void setTrustMakerSeqNo(String trustMakerSeqNo) {
		this.trustMakerSeqNo = trustMakerSeqNo;
	}
	public String getRptYear() {
		return rptYear;
	}
	public void setRptYear(String rptYear) {
		this.rptYear = rptYear;
	}
	public String getTrustInfoSeqNo() {
		return trustInfoSeqNo;
	}
	public void setTrustInfoSeqNo(String trustInfoSeqNo) {
		this.trustInfoSeqNo = trustInfoSeqNo;
	}
	public String getBusinessGoal() {
		return businessGoal;
	}
	public void setBusinessGoal(String businessGoal) {
		this.businessGoal = businessGoal;
	}
	public String getProp() {
		return prop;
	}
	public void setProp(String prop) {
		this.prop = prop;
	}
	public String getTrustCopyrightSeqNo() {
		return trustCopyrightSeqNo;
	}
	public void setTrustCopyrightSeqNo(String trustCopyrightSeqNo) {
		this.trustCopyrightSeqNo = trustCopyrightSeqNo;
	}
	public String getTrustExecSeqNo() {
		return trustExecSeqNo;
	}
	public void setTrustExecSeqNo(String trustExecSeqNo) {
		this.trustExecSeqNo = trustExecSeqNo;
	}
	public String getTrustRoyaltySeqNo() {
		return trustRoyaltySeqNo;
	}
	public void setTrustRoyaltySeqNo(String trustRoyaltySeqNo) {
		this.trustRoyaltySeqNo = trustRoyaltySeqNo;
	}
	
}