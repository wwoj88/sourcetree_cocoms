package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class IssueVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String issSeqNo           = "";	//
	private String memberSeqNo        = "";	//
	private String reportKindCd       = "";	//
	private String issDate            = "";	//
	private String issTime            = "";	//
	private String issFilePath        = "";	//
	private String issXmlPath         = "";	//
	private String statCd             = "";	//
	private String viewId             = "";	//
	
	private String reportKindDesc     = "";	//
	
	/*
	 * Special Getters
	 */
	public String getIssDateStr() {
		
		if (issDate != null && issDate.length() == 8) {
			return issDate.substring(0, 4)+"-"+issDate.substring(4, 6)+"-"+issDate.substring(6, 8);
		}
		
		return issDate;
	}
	public String getIssTimeStr() {
		
		if (issTime != null && issTime.length() == 6) {
			return issTime.substring(0, 2)+":"+issTime.substring(2, 4)+":"+issTime.substring(4, 6);
		}
		
		return issTime;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getIssSeqNo() {
		return issSeqNo;
	}
	public void setIssSeqNo(String issSeqNo) {
		this.issSeqNo = issSeqNo;
	}
	public String getReportKindCd() {
		return reportKindCd;
	}
	public void setReportKindCd(String reportKindCd) {
		this.reportKindCd = reportKindCd;
	}
	public String getIssDate() {
		return issDate;
	}
	public void setIssDate(String issDate) {
		this.issDate = issDate;
	}
	public String getIssTime() {
		return issTime;
	}
	public void setIssTime(String issTime) {
		this.issTime = issTime;
	}
	public String getIssFilePath() {
		return issFilePath;
	}
	public void setIssFilePath(String issFilePath) {
		this.issFilePath = issFilePath;
	}
	public String getIssXmlPath() {
		return issXmlPath;
	}
	public void setIssXmlPath(String issXmlPath) {
		this.issXmlPath = issXmlPath;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getViewId() {
		return viewId;
	}
	public void setViewId(String viewId) {
		this.viewId = viewId;
	}
	public String getReportKindDesc() {
		return reportKindDesc;
	}
	public void setReportKindDesc(String reportKindDesc) {
		this.reportKindDesc = reportKindDesc;
	}
}