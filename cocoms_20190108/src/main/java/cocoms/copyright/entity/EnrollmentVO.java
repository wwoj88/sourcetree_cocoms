package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class EnrollmentVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String enrollmentSeqNo   		= "";	//업체이력일련번호
	private String memberSeqNo       		= "";	//저작권위탁기관일련번호
	private String accountManager    		= "";	//업체담당자
	private String regdate    					= "";	//등록일
	private String type        		 		= "";	//이력유형
	private String contents          		= "";	//이력상세내용
	private String statCd			 		= ""; //업무처리상태코드
	private String gubun             		= "";	//1:저작권중개업, 2:저작권신탁관리업
	private String searchAccountManager     = "";	// 검색 업체담당자
	private String searchFromDate     		= "";	// 검색 등록일자 시작
	private String searchToDate       		= "";	// 검색 등록일자 끝업체구분코드
	private String searchType           	= "";	// 검색 이력유형
	private String searchContents      		= "";	// 검색 내용
	private String searchGubun				= "";	// 검색 업체구분코드(1:저작권중개업, 2:저작권신탁관리업)
	
	
	/*
	 * Special Getters
	 */
	/*public String getEnrollmentDateStr() {
		
		if (enrollmentDate != null && enrollmentDate.length() == 8) {
			return enrollmentDate.substring(0, 4)+"-"+enrollmentDate.substring(4, 6)+"-"+enrollmentDate.substring(6, 8);
		}
		
		return enrollmentDate;
	}*/
	
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getEnrollmentSeqNo() {
		return enrollmentSeqNo;
	}
	public void setEnrollmentSeqNo(String enrollmentSeqNo) {
		this.enrollmentSeqNo = enrollmentSeqNo;
	}
	public String getAccountManager() {
		return accountManager;
	}
	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}


	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getSearchAccountManager() {
		return searchAccountManager;
	}
	public void setSearchAccountManager(String searchAccountManager) {
		this.searchAccountManager = searchAccountManager;
	}
	public String getSearchFromDate() {
		return searchFromDate;
	}
	public void setSearchFromDate(String searchFromDate) {
		this.searchFromDate = searchFromDate;
	}
	public String getSearchToDate() {
		return searchToDate;
	}
	public void setSearchToDate(String searchToDate) {
		this.searchToDate = searchToDate;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getSearchContents() {
		return searchContents;
	}
	public void setSearchContents(String searchContents) {
		this.searchContents = searchContents;
	}
	public String getSearchGubun() {
		return searchGubun;
	}
	public void setSearchGubun(String searchGubun) {
		this.searchGubun = searchGubun;
	}
}