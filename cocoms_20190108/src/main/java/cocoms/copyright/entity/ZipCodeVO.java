package cocoms.copyright.entity;

public class ZipCodeVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String zipCodeSeq = "";	//
	private String zipCode    = "";	//
	private String sido       = "";	//
	private String gugun      = "";	//
	private String dong       = "";	//
	private String bunji      = "";	//
	
	/*
	 * Getters and Setters
	 */
	public String getZipCodeSeq() {
		return zipCodeSeq;
	}
	public void setZipCodeSeq(String zipCodeSeq) {
		this.zipCodeSeq = zipCodeSeq;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getSido() {
		return sido;
	}
	public void setSido(String sido) {
		this.sido = sido;
	}
	public String getGugun() {
		return gugun;
	}
	public void setGugun(String gugun) {
		this.gugun = gugun;
	}
	public String getDong() {
		return dong;
	}
	public void setDong(String dong) {
		this.dong = dong;
	}
	public String getBunji() {
		return bunji;
	}
	public void setBunji(String bunji) {
		this.bunji = bunji;
	}
	
}