package cocoms.copyright.entity;

public class CertInfoVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String certInfoSeqNo   = "";	//
	private String gubun           = "";	//
	private String source          = "";	//
	private String signature       = "";	//
	private String userDn          = "";	//
	private String certSn          = "";	//
	private String regDate         = "";	//
	private String regTime         = "";	//
	private String useSysCd        = "";	//
	private String mallSeq         = "";	//
	private String mallSeqKikwanCd = "";	//
	private String mallSeqRegdate  = "";	//
	private String mallSeqSeqno    = "";	//
	private String cappTot         = "";	//
	private String pgFee           = "";	//
	private String useIncCd        = "";	//
	private String setlIncCd       = "";	//
	private String recvIncCd       = "";	//
	private String remark          = "";	//
	private String payMthdCd       = "";	//
	private String delGubun        = "";	//
	private String isSuccess       = "";	//
	private String reqFindTime     = "";	//
	private String isFail          = "";	//
	private String reqManRrn       = "";	//
	private String hddReqManName   = "";	//
	private String conditionCd     = "";	//
	private String statCd          = "";	//
	private String memberSeqNo     = "";	//
	
	/*
	 * Special Getters
	 */
	public String getRegDateStr() {
		
		if (regDate != null && regDate.length() == 8) {
			return regDate.substring(0, 4)+"-"+regDate.substring(4, 6)+"-"+regDate.substring(6, 8);
		}
		
		return regDate;
	}
	public String getRegTimeStr() {
		
		if (regTime != null && regTime.length() == 6) {
			return regTime.substring(0, 2)+":"+regTime.substring(2, 4)+":"+regTime.substring(4, 6);
		}
		
		return regTime;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getUserDn() {
		return userDn;
	}
	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getCertInfoSeqNo() {
		return certInfoSeqNo;
	}
	public void setCertInfoSeqNo(String certInfoSeqNo) {
		this.certInfoSeqNo = certInfoSeqNo;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getUseSysCd() {
		return useSysCd;
	}
	public void setUseSysCd(String useSysCd) {
		this.useSysCd = useSysCd;
	}
	public String getMallSeq() {
		return mallSeq;
	}
	public void setMallSeq(String mallSeq) {
		this.mallSeq = mallSeq;
	}
	public String getMallSeqKikwanCd() {
		return mallSeqKikwanCd;
	}
	public void setMallSeqKikwanCd(String mallSeqKikwanCd) {
		this.mallSeqKikwanCd = mallSeqKikwanCd;
	}
	public String getMallSeqRegdate() {
		return mallSeqRegdate;
	}
	public void setMallSeqRegdate(String mallSeqRegdate) {
		this.mallSeqRegdate = mallSeqRegdate;
	}
	public String getMallSeqSeqno() {
		return mallSeqSeqno;
	}
	public void setMallSeqSeqno(String mallSeqSeqno) {
		this.mallSeqSeqno = mallSeqSeqno;
	}
	public String getCappTot() {
		return cappTot;
	}
	public void setCappTot(String cappTot) {
		this.cappTot = cappTot;
	}
	public String getPgFee() {
		return pgFee;
	}
	public void setPgFee(String pgFee) {
		this.pgFee = pgFee;
	}
	public String getUseIncCd() {
		return useIncCd;
	}
	public void setUseIncCd(String useIncCd) {
		this.useIncCd = useIncCd;
	}
	public String getSetlIncCd() {
		return setlIncCd;
	}
	public void setSetlIncCd(String setlIncCd) {
		this.setlIncCd = setlIncCd;
	}
	public String getRecvIncCd() {
		return recvIncCd;
	}
	public void setRecvIncCd(String recvIncCd) {
		this.recvIncCd = recvIncCd;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getPayMthdCd() {
		return payMthdCd;
	}
	public void setPayMthdCd(String payMthdCd) {
		this.payMthdCd = payMthdCd;
	}
	public String getDelGubun() {
		return delGubun;
	}
	public void setDelGubun(String delGubun) {
		this.delGubun = delGubun;
	}
	public String getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getReqFindTime() {
		return reqFindTime;
	}
	public void setReqFindTime(String reqFindTime) {
		this.reqFindTime = reqFindTime;
	}
	public String getIsFail() {
		return isFail;
	}
	public void setIsFail(String isFail) {
		this.isFail = isFail;
	}
	public String getReqManRrn() {
		return reqManRrn;
	}
	public void setReqManRrn(String reqManRrn) {
		this.reqManRrn = reqManRrn;
	}
	public String getHddReqManName() {
		return hddReqManName;
	}
	public void setHddReqManName(String hddReqManName) {
		this.hddReqManName = hddReqManName;
	}
	public String getConditionCd() {
		return conditionCd;
	}
	public void setConditionCd(String conditionCd) {
		this.conditionCd = conditionCd;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getCertSn() {
		return certSn;
	}
	public void setCertSn(String certSn) {
		this.certSn = certSn;
	}
}