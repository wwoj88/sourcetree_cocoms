package cocoms.copyright.entity;

public class PermKindVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String chgwritingkindseqno = "";		//
	private String chghistoryseqno = "";		//
	
	private String memberSeqNo = "";		//
	private String writingKind = "";		//
	private String permKind = ""; 			// 
	private String permEtc = ""; 			// 
	private String conditionCd = ""; 		// 

	private String permKindStr = ""; 			// 

	


	public String getChgwritingkindseqno() {
		return chgwritingkindseqno;
	}
	public void setChgwritingkindseqno(String chgwritingkindseqno) {
		this.chgwritingkindseqno = chgwritingkindseqno;
	}
	public String getChghistoryseqno() {
		return chghistoryseqno;
	}
	public void setChghistoryseqno(String chghistoryseqno) {
		this.chghistoryseqno = chghistoryseqno;
	}
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getPermKindStr() {
		return permKindStr;
	}
	public void setPermKindStr(String permKindStr) {
		this.permKindStr = permKindStr;
	}
	public String getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(String writingKind) {
		this.writingKind = writingKind;
	}
	public String getPermKind() {
		return permKind;
	}
	public void setPermKind(String permKind) {
		this.permKind = permKind;
	}
	public String getPermEtc() {
		return permEtc;
	}
	public void setPermEtc(String permEtc) {
		this.permEtc = permEtc;
	}
	public String getConditionCd() {
		return conditionCd;
	}
	public void setConditionCd(String conditionCd) {
		this.conditionCd = conditionCd;
	}
	
}