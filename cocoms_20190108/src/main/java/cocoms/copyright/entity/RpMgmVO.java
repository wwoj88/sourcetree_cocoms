package cocoms.copyright.entity;

public class RpMgmVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String rpMgmSeqNo    = "";	//
	private String memberSeqNo   = "";	//
	private String regDate       = "";	//
	private String regTime       = "";	//
	private String statCd        = "";	//
	private String rpMemo        = "";	//
	private String file1Path     = "";	//
	private String file2Path     = "";	//
	private String file3Path     = "";	//
	private String file4Path     = "";	//
	private String file5Path     = "";	//
	private String interFilename = "";	//
	private String interFilepath = "";	//
	private String certInfoSeqNo = "";	//
	private String retYn         = "";	//
	
	public RpMgmVO() {
	}
	public RpMgmVO(String rpMgmSeqNo) {
		this.rpMgmSeqNo = rpMgmSeqNo;
	}
	
	/*
	 * Special Getters
	 */
	public String getRegDateStr() {
		
		if (regDate != null && regDate.length() == 8) {
			return regDate.substring(0, 4)+"-"+regDate.substring(4, 6)+"-"+regDate.substring(6, 8);
		}
		
		return regDate;
	}
	public String getRegTimeStr() {
		
		if (regTime != null && regTime.length() == 6) {
			return regTime.substring(0, 2)+":"+regTime.substring(2, 4)+":"+regTime.substring(4, 6);
		}
		
		return regTime;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getFile1Path() {
		return file1Path;
	}
	public void setFile1Path(String file1Path) {
		this.file1Path = file1Path;
	}
	public String getFile2Path() {
		return file2Path;
	}
	public void setFile2Path(String file2Path) {
		this.file2Path = file2Path;
	}
	public String getFile3Path() {
		return file3Path;
	}
	public void setFile3Path(String file3Path) {
		this.file3Path = file3Path;
	}
	public String getFile4Path() {
		return file4Path;
	}
	public void setFile4Path(String file4Path) {
		this.file4Path = file4Path;
	}
	public String getRpMgmSeqNo() {
		return rpMgmSeqNo;
	}
	public void setRpMgmSeqNo(String rpMgmSeqNo) {
		this.rpMgmSeqNo = rpMgmSeqNo;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getRpMemo() {
		return rpMemo;
	}
	public void setRpMemo(String rpMemo) {
		this.rpMemo = rpMemo;
	}
	public String getInterFilename() {
		return interFilename;
	}
	public void setInterFilename(String interFilename) {
		this.interFilename = interFilename;
	}
	public String getInterFilepath() {
		return interFilepath;
	}
	public void setInterFilepath(String interFilepath) {
		this.interFilepath = interFilepath;
	}
	public String getCertInfoSeqNo() {
		return certInfoSeqNo;
	}
	public void setCertInfoSeqNo(String certInfoSeqNo) {
		this.certInfoSeqNo = certInfoSeqNo;
	}
	public String getRetYn() {
		return retYn;
	}
	public void setRetYn(String retYn) {
		this.retYn = retYn;
	}
	public String getFile5Path() {
		return file5Path;
	}
	public void setFile5Path(String file5Path) {
		this.file5Path = file5Path;
	}
	
}