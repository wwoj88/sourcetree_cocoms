package cocoms.copyright.entity;

public class CertVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String userDn        = "";	//
	private String certSn        = "";	// serial number
	private String loginId       = "";	//
	private String certType      = "";	// 1: 개인, 2: 법인
	private String certificate   = "";	//
	private String certStatus    = "";	// 1: 유효, 2: 폐기, 3: 정지
	private String regNo         = "";	//
	private String certStartDate = "";	//
	private String certEndDate   = "";	//
	private String regDate       = "";	//
	
	public CertVO() {
	}
	public CertVO(String loginId, String certStatus) {
		this.loginId = loginId;
		this.certStatus = certStatus;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getUserDn() {
		return userDn;
	}
	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}
	public String getCertSn() {
		return certSn;
	}
	public void setCertSn(String certSn) {
		this.certSn = certSn;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public String getCertStatus() {
		return certStatus;
	}
	public void setCertStatus(String certStatus) {
		this.certStatus = certStatus;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getCertStartDate() {
		return certStartDate;
	}
	public void setCertStartDate(String certStartDate) {
		this.certStartDate = certStartDate;
	}
	public String getCertEndDate() {
		return certEndDate;
	}
	public void setCertEndDate(String certEndDate) {
		this.certEndDate = certEndDate;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
}