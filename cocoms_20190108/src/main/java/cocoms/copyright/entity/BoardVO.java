package cocoms.copyright.entity;

import java.util.*;

public class BoardVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String boardSeqNo = ""; 	// 
	private String boardId    = ""; 	// 
	private String regDate    = ""; 	// 
	private String regTime    = ""; 	// 
	private String regId      = ""; 	// 
	private String title      = ""; 	// 
	private String content    = ""; 	// 
	private String content2   = ""; 	// 
	private String ref        = ""; 	// 
	private String depth      = ""; 	// 
	private String position   = ""; 	// 
	private String hit        = ""; 	// 
	private String dref       = ""; 	// 
	private String delGubun   = ""; 	// 
	private String download_hit        = ""; 	// 
	private String regName    = ""; 	// 
	private String replyYn    = ""; 	//

	private List<BoardFileDataVO> boardFileDataList = new ArrayList<BoardFileDataVO>(); 	// 

	public BoardVO() {
	}
	public BoardVO(String boardSeqNo) {
		this.boardSeqNo = boardSeqNo;
	}
	
	/*
	 * Special Getters
	 */
	public String getRegDateStr() {
		
		if (regDate != null && regDate.length() == 8) {
			return regDate.substring(0,  4)+"-"+regDate.substring(4, 6)+"-"+regDate.substring(6, 8);
		}
		
		return regDate;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getReplyYn() {
		return replyYn;
	}
	public void setReplyYn(String replyYn) {
		this.replyYn = replyYn;
	}
	public String getRegName() {
		return regName;
	}
	public void setRegName(String regName) {
		this.regName = regName;
	}
	public List<BoardFileDataVO> getBoardFileDataList() {
		return boardFileDataList;
	}
	public void setBoardFileDataList(List<BoardFileDataVO> boardFileDataList) {
		this.boardFileDataList = boardFileDataList;
	}
	public String getBoardSeqNo() {
		return boardSeqNo;
	}
	public void setBoardSeqNo(String boardSeqNo) {
		this.boardSeqNo = boardSeqNo;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getContent2() {
		return content2;
	}
	public void setContent2(String content2) {
		this.content2 = content2;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getDepth() {
		return depth;
	}
	public void setDepth(String depth) {
		this.depth = depth;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getHit() {
		return hit;
	}
	public void setHit(String hit) {
		this.hit = hit;
	}
	public String getDref() {
		return dref;
	}
	public void setDref(String dref) {
		this.dref = dref;
	}
	public String getDelGubun() {
		return delGubun;
	}
	public void setDelGubun(String delGubun) {
		this.delGubun = delGubun;
	}
	public String getDownload_hit() {
		return download_hit;
	}
	public void setDownload_hit(String download_hit) {
		this.download_hit = download_hit;
	}

}
