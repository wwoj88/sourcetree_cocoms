package cocoms.copyright.entity;

public class ApiReportVO {
	private String writingKind;
	private String permKind;
	private String inWorkNum;
	private String outWorkNum;
	private String workSum;
	private String inRental;
	private String outRental;
	private String rentalSum;
	private String inChrg;
	private String outChrg;
	private String chrgSum;
	private String inChrgRate;
	private String outChrgRate;
	private String avgChrgRate;
	private String memberSeqNo;
	private String rptYear;
	private String regName;
	private String regPosi;
	private String regTel;
	private String regEmail;
	private String staffNum;
	private String memo;
	
	public String getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(String writingKind) {
		this.writingKind = writingKind;
	}
	public String getPermKind() {
		return permKind;
	}
	public void setPermKind(String permKind) {
		this.permKind = permKind;
	}
	public String getInWorkNum() {
		return inWorkNum;
	}
	public void setInWorkNum(String inWorkNum) {
		this.inWorkNum = inWorkNum;
	}
	public String getOutWorkNum() {
		return outWorkNum;
	}
	public void setOutWorkNum(String outWorkNum) {
		this.outWorkNum = outWorkNum;
	}
	public String getWorkSum() {
		return workSum;
	}
	public void setWorkSum(String workSum) {
		this.workSum = workSum;
	}
	public String getInRental() {
		return inRental;
	}
	public void setInRental(String inRental) {
		this.inRental = inRental;
	}
	public String getOutRental() {
		return outRental;
	}
	public void setOutRental(String outRental) {
		this.outRental = outRental;
	}
	public String getRentalSum() {
		return rentalSum;
	}
	public void setRentalSum(String rentalSum) {
		this.rentalSum = rentalSum;
	}
	public String getInChrg() {
		return inChrg;
	}
	public void setInChrg(String inChrg) {
		this.inChrg = inChrg;
	}
	public String getOutChrg() {
		return outChrg;
	}
	public void setOutChrg(String outChrg) {
		this.outChrg = outChrg;
	}
	public String getChrgSum() {
		return chrgSum;
	}
	public void setChrgSum(String chrgSum) {
		this.chrgSum = chrgSum;
	}
	public String getInChrgRate() {
		return inChrgRate;
	}
	public void setInChrgRate(String inChrgRate) {
		this.inChrgRate = inChrgRate;
	}
	public String getOutChrgRate() {
		return outChrgRate;
	}
	public void setOutChrgRate(String outChrgRate) {
		this.outChrgRate = outChrgRate;
	}
	public String getAvgChrgRate() {
		return avgChrgRate;
	}
	public void setAvgChrgRate(String avgChrgRate) {
		this.avgChrgRate = avgChrgRate;
	}
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getRptYear() {
		return rptYear;
	}
	public void setRptYear(String rptYear) {
		this.rptYear = rptYear;
	}
	public String getRegName() {
		return regName;
	}
	public void setRegName(String regName) {
		this.regName = regName;
	}
	public String getRegPosi() {
		return regPosi;
	}
	public void setRegPosi(String regPosi) {
		this.regPosi = regPosi;
	}
	public String getRegTel() {
		return regTel;
	}
	public void setRegTel(String regTel) {
		this.regTel = regTel;
	}
	public String getRegEmail() {
		return regEmail;
	}
	public void setRegEmail(String regEmail) {
		this.regEmail = regEmail;
	}
	public String getStaffNum() {
		return staffNum;
	}
	public void setStaffNum(String staffNum) {
		this.staffNum = staffNum;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}

	
	
	
}
