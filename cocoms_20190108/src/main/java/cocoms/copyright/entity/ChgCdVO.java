package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class ChgCdVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String chgCdSeqNo    		= "";	//
	private String chgHistorySeqNo     	= "";	//
	private String chgCd     			= "";	//
	private String memberseqno          = "";
	private String chgseqno				= "";
	
	public String getChgseqno() {
		return chgseqno;
	}
	public void setChgseqno(String chgseqno) {
		this.chgseqno = chgseqno;
	}
	public String getMemberseqno() {
		return memberseqno;
	}
	public void setMemberseqno(String memberseqno) {
		this.memberseqno = memberseqno;
	}
	public ChgCdVO() {
	}
	public ChgCdVO(String chgHistorySeqNo, String chgCd) {
		this.chgHistorySeqNo = chgHistorySeqNo;
		this.chgCd = chgCd;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getChgHistorySeqNo() {
		return chgHistorySeqNo;
	}
	public void setChgHistorySeqNo(String chgHistorySeqNo) {
		this.chgHistorySeqNo = chgHistorySeqNo;
	}
	public String getChgCdSeqNo() {
		return chgCdSeqNo;
	}
	public void setChgCdSeqNo(String chgCdSeqNo) {
		this.chgCdSeqNo = chgCdSeqNo;
	}
	public String getChgCd() {
		return chgCd;
	}
	public void setChgCd(String chgCd) {
		this.chgCd = chgCd;
	}	
}