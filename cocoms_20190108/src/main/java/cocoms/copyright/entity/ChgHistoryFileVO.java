package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class ChgHistoryFileVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String chgHistoryFileSeqNo     = "";	//
	private String chgHistorySeqNo         = "";	//
	private String filename                = "";	//
	private String filepath                = "";	//
	private String filesize                = "";	//
	private String regDate                 = "";	//
	private String regTime                 = "";	//
	private String delGubun                = "";	//
	
	/*
	 * Special Getters
	 */
	public String getRegDateStr() {
		
		if (StringUtils.isNotEmpty(regDate)
				&& regDate.length() >= 8) {
			return CommonUtils.toFormat(regDate, "####.##.##");
		}
		
		return regDate;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getChgHistorySeqNo() {
		return chgHistorySeqNo;
	}
	public void setChgHistorySeqNo(String chgHistorySeqNo) {
		this.chgHistorySeqNo = chgHistorySeqNo;
	}
	public String getChgHistoryFileSeqNo() {
		return chgHistoryFileSeqNo;
	}
	public void setChgHistoryFileSeqNo(String chgHistoryFileSeqNo) {
		this.chgHistoryFileSeqNo = chgHistoryFileSeqNo;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getFilesize() {
		return filesize;
	}
	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getDelGubun() {
		return delGubun;
	}
	public void setDelGubun(String delGubun) {
		this.delGubun = delGubun;
	}
}