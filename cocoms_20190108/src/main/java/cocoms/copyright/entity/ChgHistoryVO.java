package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class ChgHistoryVO extends DefaultVO {

	private static final long serialVersionUID = 1L;
	private String conDetailCd     = "";	//
	private String coGubunCd       = "";	// 업체구분코드 (1:개인사업자, 2:법인사업자)
    private String conditioncd         = "";
	private String chgHistorySeqNo     = "";	//
	private String rpMgmSeqNo          = "";	//
	private String statCd              = "";	//
	private String preCeoName          = "";	//
	private String preCeoRegNo         = "";	//
	private String preCeoEmail         = "";	//
	private String preCeoZipcode       = "";	//
	private String preCeoSido          = "";	//
	private String preCeoGugun         = "";	//
	private String preCeoDong          = "";	//
	private String preCeoBunji         = "";	//
	private String preCeoDetailAddr    = "";	//
	private String preCeoTel           = "";	//
	private String preCeoFax           = "";	//
	private String postCeoName         = "";	//
	private String postCeoRegNo        = "";	//
	private String postCeoEmail        = "";	//
	private String postCeoZipcode      = "";	//
	private String postCeoSido         = "";	//
	private String postCeoGugun        = "";	//
	private String postCeoDong         = "";	//
	private String postCeoBunji        = "";	//
	private String postCeoDetailAddr   = "";	//
	private String postCeoTel          = "";	//
	private String postCeoFax          = "";	//
	private String preCoName           = "";	//
	private String preTrustZipcode     = "";	//
	private String preTrustSido        = "";	//
	private String preTrustGugun       = "";	//
	private String preTrustDong        = "";	//
	private String preTrustBunji       = "";	//
	private String preTrustDetailAddr  = "";	//
	private String preTrustTel         = "";	//
	private String preTrustFax         = "";	//
	private String postCoName          = "";	//
	private String postTrustZipcode    = "";	//
	private String postTrustSido       = "";	//
	private String postTrustGugun      = "";	//
	private String postTrustDong       = "";	//
	private String postTrustBunji      = "";	//
	private String postTrustDetailAddr = "";	//
	private String postTrustTel        = "";	//
	private String postTrustFax        = "";	//
	private String chgMemo             = "";	//
	private String signature           = "";	//
	private String userDn              = "";	//
	private String certSn              = "";	//
	private String preCeoMobile        = "";	//
	private String postCeoMobile       = "";	//
	private String preTrustUrl         = "";	//
	private String postTrustUrl        = "";	//
	private String preBubNo            = "";	//
	private String postBubNo           = "";	//
	private String preCoNo             = "";	//
	private String postCoNo            = "";	//
	private String preSms              = "";	//
	private String postSms             = "";	//
	private String memberseqno 		   = ""; 
	private String regdate 		       = ""; 
	
	
	
	/*
	 * 
	 */	
	
	public String getRegdate() {
		return regdate;
	}
	public String getConDetailCd() {
		return conDetailCd;
	}
	public void setConDetailCd(String conDetailCd) {
		this.conDetailCd = conDetailCd;
	}
	public String getCoGubunCd() {
		return coGubunCd;
	}
	public void setCoGubunCd(String coGubunCd) {
		this.coGubunCd = coGubunCd;
	}
	public String getConditioncd() {
		return conditioncd;
	}
	public void setConditioncd(String conditioncd) {
		this.conditioncd = conditioncd;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getMemberseqno() {
		return memberseqno;
	}
	public void setMemberseqno(String memberseqno) {
		this.memberseqno = memberseqno;
	}
	public String getPostBubNoStr() {
		
		if (StringUtils.isNotEmpty(postBubNo)) {
			return CommonUtils.toFormat(postBubNo, "######-#######");
		}
		
		return postBubNo;
	}
	public String getPostCoNoStr() {
		
		if (StringUtils.isNotEmpty(postCoNo)) {
			return CommonUtils.toFormat(postCoNo, "###-##-#####");
		}
		
		return postCoNo;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getChgHistorySeqNo() {
		return chgHistorySeqNo;
	}
	public void setChgHistorySeqNo(String chgHistorySeqNo) {
		this.chgHistorySeqNo = chgHistorySeqNo;
	}
	public String getPreCeoEmail() {
		return preCeoEmail;
	}
	public void setPreCeoEmail(String preCeoEmail) {
		this.preCeoEmail = preCeoEmail;
	}
	public String getPreTrustDetailAddr() {
		return preTrustDetailAddr;
	}
	public void setPreTrustDetailAddr(String preTrustDetailAddr) {
		this.preTrustDetailAddr = preTrustDetailAddr;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getRpMgmSeqNo() {
		return rpMgmSeqNo;
	}
	public void setRpMgmSeqNo(String rpMgmSeqNo) {
		this.rpMgmSeqNo = rpMgmSeqNo;
	}
	public String getPreCeoName() {
		return preCeoName;
	}
	public void setPreCeoName(String preCeoName) {
		this.preCeoName = preCeoName;
	}
	public String getPreCeoRegNo() {
		return preCeoRegNo;
	}
	public void setPreCeoRegNo(String preCeoRegNo) {
		this.preCeoRegNo = preCeoRegNo;
	}
	public String getPreCeoZipcode() {
		return preCeoZipcode;
	}
	public void setPreCeoZipcode(String preCeoZipcode) {
		this.preCeoZipcode = preCeoZipcode;
	}
	public String getPreCeoSido() {
		return preCeoSido;
	}
	public void setPreCeoSido(String preCeoSido) {
		this.preCeoSido = preCeoSido;
	}
	public String getPreCeoGugun() {
		return preCeoGugun;
	}
	public void setPreCeoGugun(String preCeoGugun) {
		this.preCeoGugun = preCeoGugun;
	}
	public String getPreCeoDong() {
		return preCeoDong;
	}
	public void setPreCeoDong(String preCeoDong) {
		this.preCeoDong = preCeoDong;
	}
	public String getPreCeoBunji() {
		return preCeoBunji;
	}
	public void setPreCeoBunji(String preCeoBunji) {
		this.preCeoBunji = preCeoBunji;
	}
	public String getPreCeoDetailAddr() {
		return preCeoDetailAddr;
	}
	public void setPreCeoDetailAddr(String preCeoDetailAddr) {
		this.preCeoDetailAddr = preCeoDetailAddr;
	}
	public String getPreCeoTel() {
		return preCeoTel;
	}
	public void setPreCeoTel(String preCeoTel) {
		this.preCeoTel = preCeoTel;
	}
	public String getPreCeoFax() {
		return preCeoFax;
	}
	public void setPreCeoFax(String preCeoFax) {
		this.preCeoFax = preCeoFax;
	}
	public String getPostCeoName() {
		return postCeoName;
	}
	public void setPostCeoName(String postCeoName) {
		this.postCeoName = postCeoName;
	}
	public String getPostCeoRegNo() {
		return postCeoRegNo;
	}
	public void setPostCeoRegNo(String postCeoRegNo) {
		this.postCeoRegNo = postCeoRegNo;
	}
	public String getPostCeoEmail() {
		return postCeoEmail;
	}
	public void setPostCeoEmail(String postCeoEmail) {
		this.postCeoEmail = postCeoEmail;
	}
	public String getPostCeoZipcode() {
		return postCeoZipcode;
	}
	public void setPostCeoZipcode(String postCeoZipcode) {
		this.postCeoZipcode = postCeoZipcode;
	}
	public String getPostCeoSido() {
		return postCeoSido;
	}
	public void setPostCeoSido(String postCeoSido) {
		this.postCeoSido = postCeoSido;
	}
	public String getPostCeoGugun() {
		return postCeoGugun;
	}
	public void setPostCeoGugun(String postCeoGugun) {
		this.postCeoGugun = postCeoGugun;
	}
	public String getPostCeoDong() {
		return postCeoDong;
	}
	public void setPostCeoDong(String postCeoDong) {
		this.postCeoDong = postCeoDong;
	}
	public String getPostCeoBunji() {
		return postCeoBunji;
	}
	public void setPostCeoBunji(String postCeoBunji) {
		this.postCeoBunji = postCeoBunji;
	}
	public String getPostCeoDetailAddr() {
		return postCeoDetailAddr;
	}
	public void setPostCeoDetailAddr(String postCeoDetailAddr) {
		this.postCeoDetailAddr = postCeoDetailAddr;
	}
	public String getPostCeoTel() {
		return postCeoTel;
	}
	public void setPostCeoTel(String postCeoTel) {
		this.postCeoTel = postCeoTel;
	}
	public String getPostCeoFax() {
		return postCeoFax;
	}
	public void setPostCeoFax(String postCeoFax) {
		this.postCeoFax = postCeoFax;
	}
	public String getPreCoName() {
		return preCoName;
	}
	public void setPreCoName(String preCoName) {
		this.preCoName = preCoName;
	}
	public String getPreTrustZipcode() {
		return preTrustZipcode;
	}
	public void setPreTrustZipcode(String preTrustZipcode) {
		this.preTrustZipcode = preTrustZipcode;
	}
	public String getPreTrustSido() {
		return preTrustSido;
	}
	public void setPreTrustSido(String preTrustSido) {
		this.preTrustSido = preTrustSido;
	}
	public String getPreTrustGugun() {
		return preTrustGugun;
	}
	public void setPreTrustGugun(String preTrustGugun) {
		this.preTrustGugun = preTrustGugun;
	}
	public String getPreTrustDong() {
		return preTrustDong;
	}
	public void setPreTrustDong(String preTrustDong) {
		this.preTrustDong = preTrustDong;
	}
	public String getPreTrustBunji() {
		return preTrustBunji;
	}
	public void setPreTrustBunji(String preTrustBunji) {
		this.preTrustBunji = preTrustBunji;
	}
	public String getPreTrustTel() {
		return preTrustTel;
	}
	public void setPreTrustTel(String preTrustTel) {
		this.preTrustTel = preTrustTel;
	}
	public String getPreTrustFax() {
		return preTrustFax;
	}
	public void setPreTrustFax(String preTrustFax) {
		this.preTrustFax = preTrustFax;
	}
	public String getPostCoName() {
		return postCoName;
	}
	public void setPostCoName(String postCoName) {
		this.postCoName = postCoName;
	}
	public String getPostTrustZipcode() {
		return postTrustZipcode;
	}
	public void setPostTrustZipcode(String postTrustZipcode) {
		this.postTrustZipcode = postTrustZipcode;
	}
	public String getPostTrustSido() {
		return postTrustSido;
	}
	public void setPostTrustSido(String postTrustSido) {
		this.postTrustSido = postTrustSido;
	}
	public String getPostTrustGugun() {
		return postTrustGugun;
	}
	public void setPostTrustGugun(String postTrustGugun) {
		this.postTrustGugun = postTrustGugun;
	}
	public String getPostTrustDong() {
		return postTrustDong;
	}
	public void setPostTrustDong(String postTrustDong) {
		this.postTrustDong = postTrustDong;
	}
	public String getPostTrustBunji() {
		return postTrustBunji;
	}
	public void setPostTrustBunji(String postTrustBunji) {
		this.postTrustBunji = postTrustBunji;
	}
	public String getPostTrustDetailAddr() {
		return postTrustDetailAddr;
	}
	public void setPostTrustDetailAddr(String postTrustDetailAddr) {
		this.postTrustDetailAddr = postTrustDetailAddr;
	}
	public String getPostTrustTel() {
		return postTrustTel;
	}
	public void setPostTrustTel(String postTrustTel) {
		this.postTrustTel = postTrustTel;
	}
	public String getPostTrustFax() {
		return postTrustFax;
	}
	public void setPostTrustFax(String postTrustFax) {
		this.postTrustFax = postTrustFax;
	}
	public String getChgMemo() {
		return chgMemo;
	}
	public void setChgMemo(String chgMemo) {
		this.chgMemo = chgMemo;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getUserDn() {
		return userDn;
	}
	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}
	public String getCertSn() {
		return certSn;
	}
	public void setCertSn(String certSn) {
		this.certSn = certSn;
	}
	public String getPreCeoMobile() {
		return preCeoMobile;
	}
	public void setPreCeoMobile(String preCeoMobile) {
		this.preCeoMobile = preCeoMobile;
	}
	public String getPostCeoMobile() {
		return postCeoMobile;
	}
	public void setPostCeoMobile(String postCeoMobile) {
		this.postCeoMobile = postCeoMobile;
	}
	public String getPreTrustUrl() {
		return preTrustUrl;
	}
	public void setPreTrustUrl(String preTrustUrl) {
		this.preTrustUrl = preTrustUrl;
	}
	public String getPostTrustUrl() {
		return postTrustUrl;
	}
	public void setPostTrustUrl(String postTrustUrl) {
		this.postTrustUrl = postTrustUrl;
	}
	public String getPreBubNo() {
		return preBubNo;
	}
	public void setPreBubNo(String preBubNo) {
		this.preBubNo = preBubNo;
	}
	public String getPostBubNo() {
		return postBubNo;
	}
	public void setPostBubNo(String postBubNo) {
		this.postBubNo = postBubNo;
	}
	public String getPreCoNo() {
		return preCoNo;
	}
	public void setPreCoNo(String preCoNo) {
		this.preCoNo = preCoNo;
	}
	public String getPostCoNo() {
		return postCoNo;
	}
	public void setPostCoNo(String postCoNo) {
		this.postCoNo = postCoNo;
	}
	public String getPreSms() {
		return preSms;
	}
	public void setPreSms(String preSms) {
		this.preSms = preSms;
	}
	public String getPostSms() {
		return postSms;
	}
	public void setPostSms(String postSms) {
		this.postSms = postSms;
	}
	
}