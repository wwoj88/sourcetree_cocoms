package cocoms.copyright.entity;

public class WritingKindVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String writingKind = "";		//
	private String memberSeqNo = "";		//
	private String writingEtc = ""; 		// 
	private String conditionCd = ""; 		// 
	private String writingKindStr = "";		//
	private String chghistoryseqno = "";		//
	private String writingchgcd = ""; 			// 
	private String writingStr = "";
	private String prewritingKind = "";
	private String prewritingEtc = "";
	private String chgwritingkindseqno = "";		//


	
	public String getChgwritingkindseqno() {
		return chgwritingkindseqno;
	}
	public void setChgwritingkindseqno(String chgwritingkindseqno) {
		this.chgwritingkindseqno = chgwritingkindseqno;
	}
	public String getChghistoryseqno() {
		return chghistoryseqno;
	}
	public void setChghistoryseqno(String chghistoryseqno) {
		this.chghistoryseqno = chghistoryseqno;
	}
	public String getWritingchgcd() {
		return writingchgcd;
	}
	public void setWritingchgcd(String writingchgcd) {
		this.writingchgcd = writingchgcd;
	}
	public String getWritingStr() {
		return writingStr;
	}
	public void setWritingStr(String writingStr) {
		this.writingStr = writingStr;
	}
	public String getPrewritingKind() {
		return prewritingKind;
	}
	public void setPrewritingKind(String prewritingKind) {
		this.prewritingKind = prewritingKind;
	}
	public String getPrewritingEtc() {
		return prewritingEtc;
	}
	public void setPrewritingEtc(String prewritingEtc) {
		this.prewritingEtc = prewritingEtc;
	}
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getWritingKindStr() {
		return writingKindStr;
	}
	public void setWritingKindStr(String writingKindStr) {
		this.writingKindStr = writingKindStr;
	}
	public String getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(String writingKind) {
		this.writingKind = writingKind;
	}
	public String getWritingEtc() {
		return writingEtc;
	}
	public void setWritingEtc(String writingEtc) {
		this.writingEtc = writingEtc;
	}
	public String getConditionCd() {
		return conditionCd;
	}
	public void setConditionCd(String conditionCd) {
		this.conditionCd = conditionCd;
	}
	
}