package cocoms.copyright.entity;

public class TrustStat2VO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String trustRoyaltySeqNo	= "";	//
	private String trustGubunSeqNo      = "";	//
	private String trustInfoSeqNo       = "";	//
	private String contract             = "";	//
	private String inCollected          = "";	//
	private String outCollected         = "";	//
	private String collectedSum         = "";	//
	private String inDividend           = "";	//
	private String outDividend          = "";	//
	private String dividendSum          = "";	//
	private String inDividendOff        = "";	//
	private String outDividendOff       = "";	//
	private String dividendOffSum       = "";	//
	private String inChrg               = "";	//
	private String outChrg              = "";	//
	private String chrgSum              = "";	//
	private String inChrgRate           = "";	//
	private String outChrgRate          = "";	//
	private String chrgRateSum          = "";	//
	private String trustMemo            = "";	//

	private String gubunName            = "";	//
	private String gubunKind            = "";	//

	/*
	 * Getters and Setters
	 */
	public String getTrustRoyaltySeqNo() {
		return trustRoyaltySeqNo;
	}
	public void setTrustRoyaltySeqNo(String trustRoyaltySeqNo) {
		this.trustRoyaltySeqNo = trustRoyaltySeqNo;
	}
	public String getGubunName() {
		return gubunName;
	}
	public void setGubunName(String gubunName) {
		this.gubunName = gubunName;
	}
	public String getGubunKind() {
		return gubunKind;
	}
	public void setGubunKind(String gubunKind) {
		this.gubunKind = gubunKind;
	}
	public String getTrustGubunSeqNo() {
		return trustGubunSeqNo;
	}
	public void setTrustGubunSeqNo(String trustGubunSeqNo) {
		this.trustGubunSeqNo = trustGubunSeqNo;
	}
	public String getTrustInfoSeqNo() {
		return trustInfoSeqNo;
	}
	public void setTrustInfoSeqNo(String trustInfoSeqNo) {
		this.trustInfoSeqNo = trustInfoSeqNo;
	}
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public String getInCollected() {
		return inCollected;
	}
	public void setInCollected(String inCollected) {
		this.inCollected = inCollected;
	}
	public String getOutCollected() {
		return outCollected;
	}
	public void setOutCollected(String outCollected) {
		this.outCollected = outCollected;
	}
	public String getCollectedSum() {
		return collectedSum;
	}
	public void setCollectedSum(String collectedSum) {
		this.collectedSum = collectedSum;
	}
	public String getInDividend() {
		return inDividend;
	}
	public void setInDividend(String inDividend) {
		this.inDividend = inDividend;
	}
	public String getOutDividend() {
		return outDividend;
	}
	public void setOutDividend(String outDividend) {
		this.outDividend = outDividend;
	}
	public String getDividendSum() {
		return dividendSum;
	}
	public void setDividendSum(String dividendSum) {
		this.dividendSum = dividendSum;
	}
	public String getInDividendOff() {
		return inDividendOff;
	}
	public void setInDividendOff(String inDividendOff) {
		this.inDividendOff = inDividendOff;
	}
	public String getOutDividendOff() {
		return outDividendOff;
	}
	public void setOutDividendOff(String outDividendOff) {
		this.outDividendOff = outDividendOff;
	}
	public String getDividendOffSum() {
		return dividendOffSum;
	}
	public void setDividendOffSum(String dividendOffSum) {
		this.dividendOffSum = dividendOffSum;
	}
	public String getInChrg() {
		return inChrg;
	}
	public void setInChrg(String inChrg) {
		this.inChrg = inChrg;
	}
	public String getOutChrg() {
		return outChrg;
	}
	public void setOutChrg(String outChrg) {
		this.outChrg = outChrg;
	}
	public String getChrgSum() {
		return chrgSum;
	}
	public void setChrgSum(String chrgSum) {
		this.chrgSum = chrgSum;
	}
	public String getInChrgRate() {
		return inChrgRate;
	}
	public void setInChrgRate(String inChrgRate) {
		this.inChrgRate = inChrgRate;
	}
	public String getOutChrgRate() {
		return outChrgRate;
	}
	public void setOutChrgRate(String outChrgRate) {
		this.outChrgRate = outChrgRate;
	}
	public String getChrgRateSum() {
		return chrgRateSum;
	}
	public void setChrgRateSum(String chrgRateSum) {
		this.chrgRateSum = chrgRateSum;
	}
	public String getTrustMemo() {
		return trustMemo;
	}
	public void setTrustMemo(String trustMemo) {
		this.trustMemo = trustMemo;
	}
}