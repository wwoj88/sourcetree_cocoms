package cocoms.copyright.entity;

public class ReportStatVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String memberSeqNo     = "";	//
	private String rptYear         = "";	//
	private String writingKind     = "";	//
	private String permKind        = "";	//
	private String inWorkNum       = "";	//
	private String outWorkNum      = "";	//
	private String workSum         = "";	//
	private String inRental        = "";	//
	private String outRental       = "";	//
	private String rentalSum       = "";	//
	private String inChrg          = "";	//
	private String outChrg         = "";	//
	private String chrgSum         = "";	//
	private String inChrgRate      = "";	//
	private String outChrgRate     = "";	//
	private String avgChrgRate     = "";	//
	
	private String writingKindDesc     = "";	//
	private String permKindDesc     = "";	//

	public ReportStatVO() {
	}
	public ReportStatVO(String memberSeqNo, String rptYear) {
		this.memberSeqNo = memberSeqNo;
		this.rptYear = rptYear;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getWritingKindDesc() {
		return writingKindDesc;
	}
	public void setWritingKindDesc(String writingKindDesc) {
		this.writingKindDesc = writingKindDesc;
	}
	public String getPermKindDesc() {
		return permKindDesc;
	}
	public void setPermKindDesc(String permKindDesc) {
		this.permKindDesc = permKindDesc;
	}
	public String getRptYear() {
		return rptYear;
	}
	public void setRptYear(String rptYear) {
		this.rptYear = rptYear;
	}
	public String getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(String writingKind) {
		this.writingKind = writingKind;
	}
	public String getPermKind() {
		return permKind;
	}
	public void setPermKind(String permKind) {
		this.permKind = permKind;
	}
	public String getInWorkNum() {
		return inWorkNum;
	}
	public void setInWorkNum(String inWorkNum) {
		this.inWorkNum = inWorkNum;
	}
	public String getOutWorkNum() {
		return outWorkNum;
	}
	public void setOutWorkNum(String outWorkNum) {
		this.outWorkNum = outWorkNum;
	}
	public String getWorkSum() {
		return workSum;
	}
	public void setWorkSum(String workSum) {
		this.workSum = workSum;
	}
	public String getInRental() {
		return inRental;
	}
	public void setInRental(String inRental) {
		this.inRental = inRental;
	}
	public String getOutRental() {
		return outRental;
	}
	public void setOutRental(String outRental) {
		this.outRental = outRental;
	}
	public String getRentalSum() {
		return rentalSum;
	}
	public void setRentalSum(String rentalSum) {
		this.rentalSum = rentalSum;
	}
	public String getInChrg() {
		return inChrg;
	}
	public void setInChrg(String inChrg) {
		this.inChrg = inChrg;
	}
	public String getOutChrg() {
		return outChrg;
	}
	public void setOutChrg(String outChrg) {
		this.outChrg = outChrg;
	}
	public String getChrgSum() {
		return chrgSum;
	}
	public void setChrgSum(String chrgSum) {
		this.chrgSum = chrgSum;
	}
	public String getInChrgRate() {
		return inChrgRate;
	}
	public void setInChrgRate(String inChrgRate) {
		this.inChrgRate = inChrgRate;
	}
	public String getOutChrgRate() {
		return outChrgRate;
	}
	public void setOutChrgRate(String outChrgRate) {
		this.outChrgRate = outChrgRate;
	}
	public String getAvgChrgRate() {
		return avgChrgRate;
	}
	public void setAvgChrgRate(String avgChrgRate) {
		this.avgChrgRate = avgChrgRate;
	}
	
}