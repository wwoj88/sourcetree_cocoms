package cocoms.copyright.entity;

public class CertProbeVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String printId        = "";	//
	private String userPosition   = "";	//
	private String userStatus     = "";	//
	private String certId         = "";	//
	private String certType       = "";	//
	private String printDate      = "";	//
	private String printUser      = "";	//
	private String userId         = "";	//
	private String userSn         = "";	//
	private String regDate        = "";	//
	private String regCount       = "";	//
	private String regIp          = "";	//
	private String layout         = "";	//
	private String userIp         = "";	//

	public CertProbeVO() {
	}
	public CertProbeVO(String printId) {
		this.printId = printId;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getPrintId() {
		return printId;
	}
	public void setPrintId(String printId) {
		this.printId = printId;
	}
	public String getUserPosition() {
		return userPosition;
	}
	public void setUserPosition(String userPosition) {
		this.userPosition = userPosition;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public String getCertId() {
		return certId;
	}
	public void setCertId(String certId) {
		this.certId = certId;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	public String getPrintUser() {
		return printUser;
	}
	public void setPrintUser(String printUser) {
		this.printUser = printUser;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserSn() {
		return userSn;
	}
	public void setUserSn(String userSn) {
		this.userSn = userSn;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegCount() {
		return regCount;
	}
	public void setRegCount(String regCount) {
		this.regCount = regCount;
	}
	public String getRegIp() {
		return regIp;
	}
	public void setRegIp(String regIp) {
		this.regIp = regIp;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
}