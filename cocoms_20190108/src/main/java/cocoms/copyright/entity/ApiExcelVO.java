package cocoms.copyright.entity;

import java.util.ArrayList;
import java.util.List;

public class ApiExcelVO {
	
	private String comm_name;
	private String comm_telx;
	private String comm_reps_name;
	private String rgst_idnt;
	private String rept_chrr_name;
	private String rept_chrr_posi;
	private String rept_chrr_telx;
	private String rept_chrr_mail;
	private String trst_orgn_code;
	private String works_sub_title;
	private String rgst_orgn_name;
	private int works_id;
	private String comm_works_id;
	private String works_title;
	private String album_title;
	private String album_lable;
	private String disk_side;
	private String nation_cd;
	private String track_no;
	private String album_issu_year;
	private String lyrc;
	private String comp;
	private String arra;
	private String tran;
	private String sing;
	private String conditioncd;
	private String perf;
	private String comm_mgnt_cd;
	private String lice_mgnt_cd;
	private String perf_mgnt_cd;
	private String prod_mgnt_cd;
	private String uci;
	private String rept_ymd;
	private String rept_works_cont;
    private int reptSeqn;
    private String prod;
    
	public String getConditioncd() {
		return conditioncd;
	}
	public void setConditioncd(String conditioncd) {
		this.conditioncd = conditioncd;
	}
	private ArrayList<ApiExcelVO> ApiExcelVOList = new ArrayList<ApiExcelVO>(); 	// 

	public String getComm_name() {
		return comm_name;
	}
	public void setComm_name(String comm_name) {
		this.comm_name = comm_name;
	}
	public String getComm_telx() {
		return comm_telx;
	}
	public void setComm_telx(String comm_telx) {
		this.comm_telx = comm_telx;
	}
	public String getComm_reps_name() {
		return comm_reps_name;
	}
	public void setComm_reps_name(String comm_reps_name) {
		this.comm_reps_name = comm_reps_name;
	}
	public String getRgst_idnt() {
		return rgst_idnt;
	}
	public void setRgst_idnt(String rgst_idnt) {
		this.rgst_idnt = rgst_idnt;
	}
	public String getRept_chrr_name() {
		return rept_chrr_name;
	}
	public void setRept_chrr_name(String rept_chrr_name) {
		this.rept_chrr_name = rept_chrr_name;
	}
	public String getRept_chrr_posi() {
		return rept_chrr_posi;
	}
	public void setRept_chrr_posi(String rept_chrr_posi) {
		this.rept_chrr_posi = rept_chrr_posi;
	}
	public String getRept_chrr_telx() {
		return rept_chrr_telx;
	}
	public void setRept_chrr_telx(String rept_chrr_telx) {
		this.rept_chrr_telx = rept_chrr_telx;
	}
	public String getRept_chrr_mail() {
		return rept_chrr_mail;
	}
	public void setRept_chrr_mail(String rept_chrr_mail) {
		this.rept_chrr_mail = rept_chrr_mail;
	}
	public String getTrst_orgn_code() {
		return trst_orgn_code;
	}
	public void setTrst_orgn_code(String trst_orgn_code) {
		this.trst_orgn_code = trst_orgn_code;
	}
	public String getRgst_orgn_name() {
		return rgst_orgn_name;
	}
	public void setRgst_orgn_name(String rgst_orgn_name) {
		this.rgst_orgn_name = rgst_orgn_name;
	}
	public String getComm_works_id() {
		return comm_works_id;
	}
	public void setComm_works_id(String comm_works_id) {
		this.comm_works_id = comm_works_id;
	}
	public String getWorks_title() {
		return works_title;
	}
	public void setWorks_title(String works_title) {
		this.works_title = works_title;
	}
	public String getAlbum_title() {
		return album_title;
	}
	public void setAlbum_title(String album_title) {
		this.album_title = album_title;
	}
	public String getAlbum_lable() {
		return album_lable;
	}
	public void setAlbum_lable(String album_lable) {
		this.album_lable = album_lable;
	}
	public String getDisk_side() {
		return disk_side;
	}
	public void setDisk_side(String disk_side) {
		this.disk_side = disk_side;
	}
	public String getNation_cd() {
		return nation_cd;
	}
	public void setNation_cd(String nation_cd) {
		this.nation_cd = nation_cd;
	}
	public String getTrack_no() {
		return track_no;
	}
	public void setTrack_no(String track_no) {
		this.track_no = track_no;
	}
	public String getAlbum_issu_year() {
		return album_issu_year;
	}
	public void setAlbum_issu_year(String album_issu_year) {
		this.album_issu_year = album_issu_year;
	}
	public String getLyrc() {
		return lyrc;
	}
	public void setLyrc(String lyrc) {
		this.lyrc = lyrc;
	}
	public String getComp() {
		return comp;
	}
	public void setComp(String comp) {
		this.comp = comp;
	}
	public String getArra() {
		return arra;
	}
	public void setArra(String arra) {
		this.arra = arra;
	}
	public String getTran() {
		return tran;
	}
	public void setTran(String tran) {
		this.tran = tran;
	}
	public String getSing() {
		return sing;
	}
	public void setSing(String sing) {
		this.sing = sing;
	}
	public String getPerf() {
		return perf;
	}
	public void setPerf(String perf) {
		this.perf = perf;
	}
	public String getComm_mgnt_cd() {
		return comm_mgnt_cd;
	}
	public void setComm_mgnt_cd(String comm_mgnt_cd) {
		this.comm_mgnt_cd = comm_mgnt_cd;
	}
	public String getLice_mgnt_cd() {
		return lice_mgnt_cd;
	}
	public void setLice_mgnt_cd(String lice_mgnt_cd) {
		this.lice_mgnt_cd = lice_mgnt_cd;
	}
	public String getPerf_mgnt_cd() {
		return perf_mgnt_cd;
	}
	public void setPerf_mgnt_cd(String perf_mgnt_cd) {
		this.perf_mgnt_cd = perf_mgnt_cd;
	}
	public String getUci() {
		return uci;
	}
	public void setUci(String uci) {
		this.uci = uci;
	}
	public ArrayList<ApiExcelVO> getApiExcelVOList() {
		return ApiExcelVOList;
	}
	public void setApiExcelVOList(ArrayList<ApiExcelVO> apiExcelVOList) {
		ApiExcelVOList = apiExcelVOList;
	}
	public String getRept_ymd() {
		return rept_ymd;
	}
	public void setRept_ymd(String rept_ymd) {
		this.rept_ymd = rept_ymd;
	}
	public String getRept_works_cont() {
		return rept_works_cont;
	}
	public void setRept_works_cont(String rept_work_cont) {
		this.rept_works_cont = rept_work_cont;
	}
	public int getReptSeqn() {
		return reptSeqn;
	}
	public void setReptSeqn(int reptSeqn) {
		this.reptSeqn = reptSeqn;
	}
	public int getWorks_id() {
		return works_id;
	}
	public void setWorks_id(int works_id) {
		this.works_id = works_id;
	}
	public String getWorks_sub_title() {
		return works_sub_title;
	}
	public void setWorks_sub_title(String works_sub_title) {
		this.works_sub_title = works_sub_title;
	}
	public String getProd() {
		return prod;
	}
	public void setProd(String prod) {
		this.prod = prod;
	}
	public String getProd_mgnt_cd() {
		return prod_mgnt_cd;
	}
	public void setProd_mgnt_cd(String prod_mgnt_cd) {
		this.prod_mgnt_cd = prod_mgnt_cd;
	}
	
	

}
