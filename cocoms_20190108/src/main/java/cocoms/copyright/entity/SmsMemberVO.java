package cocoms.copyright.entity;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class SmsMemberVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String smsSeqNo    = "";	//
	private String memberSeqNo = "";	//
	private String receiver = "";		//

	private String ceoName = "";		//
	private String coName = "";			//
	private String loginId = "";		//

	/*
	 * Getters and Setters
	 */
	public String getSmsSeqNo() {
		return smsSeqNo;
	}
	public void setSmsSeqNo(String smsSeqNo) {
		this.smsSeqNo = smsSeqNo;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getCoName() {
		return coName;
	}
	public void setCoName(String coName) {
		this.coName = coName;
	}
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
			
}