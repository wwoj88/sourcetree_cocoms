package cocoms.copyright.entity;

public class ChgPermKindVO extends DefaultVO{
	
	private static final long serialVersionUID = 1L;

	private String chgwritingkindseqno = "";		//
	private String chghistoryseqno = "";		//
	private String permKind = ""; 			// 
	private String permKindStr = ""; 			// 
    private String memberSeqNo = "";
    private String conditionCd = "";
	private String writingKind = "";		//
 	private String permEtc = ""; 			// 
	public String getChgwritingkindseqno() {
		return chgwritingkindseqno;
	}
	public void setChgwritingkindseqno(String chgwritingkindseqno) {
		this.chgwritingkindseqno = chgwritingkindseqno;
	}
	public String getChghistoryseqno() {
		return chghistoryseqno;
	}
	public void setChghistoryseqno(String chghistoryseqno) {
		this.chghistoryseqno = chghistoryseqno;
	}
	public String getPermKind() {
		return permKind;
	}
	public void setPermKind(String permKind) {
		this.permKind = permKind;
	}
	public String getPermKindStr() {
		return permKindStr;
	}
	public void setPermKindStr(String permKindStr) {
		this.permKindStr = permKindStr;
	}
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getConditionCd() {
		return conditionCd;
	}
	public void setConditionCd(String conditionCd) {
		this.conditionCd = conditionCd;
	}
	public String getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(String writingKind) {
		this.writingKind = writingKind;
	}
	public String getPermEtc() {
		return permEtc;
	}
	public void setPermEtc(String permEtc) {
		this.permEtc = permEtc;
	}
 

 

}
