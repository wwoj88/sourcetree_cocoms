package cocoms.copyright.entity;

public class ApiVO implements Comparable<ApiVO> {
	
	private String loginId;//ID
	private String pwd;//PWD
	private String statCd;//상태값
	private String appno2;//TRST_ORGN_CODE
	private String conditionCd;// 1 대리중개 / 2 신탁
	//실적보고 
	/*private String ceoName;	//
	private String ceoMobile;	//
	private String ceoEmail;	//
*//*	private String regName;	//
	private String regPosi;	//
	private String regTel;	//
	private String regEmail;	//
	private String staffNum;	//
*/	
	private String writingKind;
	private String writingKindDesc;
	private String memberSeqNo;
	private String appRegDate; //등록일 ( 허가등록일 ) 
	
	
	
	public String getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(String writingKind) {
		this.writingKind = writingKind;
	}
	public String getAppRegDate() {
		return appRegDate;
	}
	public void setAppRegDate(String appRegDate) {
		this.appRegDate = appRegDate;
	}
	public String getMemberSeqNo() {
		return memberSeqNo;
	}
	public void setMemberSeqNo(String memberSeqNo) {
		this.memberSeqNo = memberSeqNo;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getAppno2() {
		return appno2;
	}
	public void setAppno2(String appno2) {
		this.appno2 = appno2;
	}
	public String getConditionCd() {
		return conditionCd;
	}
	public void setConditionCd(String conditionCd) {
		this.conditionCd = conditionCd;
	}/*
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getCeoMobile() {
		return ceoMobile;
	}
	public void setCeoMobile(String ceoMobile) {
		this.ceoMobile = ceoMobile;
	}
	public String getCeoEmail() {
		return ceoEmail;
	}
	public void setCeoEmail(String ceoEmail) {
		this.ceoEmail = ceoEmail;
	}*/
	
	/*public String getWritingKind() {
		return writingKind;
	}
	public void setWritingKind(String writingKind) {
		this.writingKind = writingKind;
	}*/
	public String getWritingKindDesc() {
		return writingKindDesc;
	}
	public void setWritingKindDesc(String writingKindDesc) {
		this.writingKindDesc = writingKindDesc;
	}
	@Override
	public int compareTo(ApiVO o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
