package cocoms.copyright.entity;

import java.util.*;

import org.apache.commons.lang3.StringUtils;

import cocoms.copyright.common.CommonUtils;

public class SmsHistoryVO extends DefaultVO {

	private static final long serialVersionUID = 1L;

	private String smsSeqNo    = "";	//
	
	private String sendType    = "";	// 발송구분 (0: 자동, 1: 즉시, 2: 예약)
	private String sendDate    = "";	//
	private String sendTime    = "";	//
	private String sendYn      = "";	//
	
	private String smsType     = "";	//
	private String subject     = "";	//
	private String content     = "";	//
	
	private String regId       = "";	//
	private String regDate     = "";	//
	private String regTime     = "";	//
	private String modId       = "";	//
	private String modDate     = "";	//
	private String modTime     = "";	//
	private String useYn       = "";	//
	
	private List<SmsMemberVO> smsMemberList = new ArrayList<SmsMemberVO>();	//

	public SmsHistoryVO() {
	}
	public SmsHistoryVO(String smsSeqNo) {
		this.smsSeqNo = smsSeqNo;
	}
	
	/*
	 * Special Getters
	 */
	public String getSendDateStr() {
		
		if (sendDate != null && sendDate.length() == 8) {
			return sendDate.substring(0, 4)+"-"+sendDate.substring(4, 6)+"-"+sendDate.substring(6, 8);
		}
		
		return sendDate;
	}
	public String getSendTimeStr() {
		
		if (sendTime != null && sendTime.length() == 6) {
//			return sendTime.substring(0, 2)+":"+sendTime.substring(2, 4)+":"+sendTime.substring(4, 6);
			return sendTime.substring(0, 2)+":"+sendTime.substring(2, 4);
		}
		
		return sendTime;
	}
	
	/*
	 * Getters and Setters
	 */
	public String getSmsSeqNo() {
		return smsSeqNo;
	}
	public void setSmsSeqNo(String smsSeqNo) {
		this.smsSeqNo = smsSeqNo;
	}
	public List<SmsMemberVO> getSmsMemberList() {
		return smsMemberList;
	}
	public void setSmsMemberList(List<SmsMemberVO> smsMemberList) {
		this.smsMemberList = smsMemberList;
	}
	public String getSendType() {
		return sendType;
	}
	public void setSendType(String sendType) {
		this.sendType = sendType;
	}
	public String getSendDate() {
		return sendDate;
	}
	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}
	public String getSendTime() {
		return sendTime;
	}
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	public String getSendYn() {
		return sendYn;
	}
	public void setSendYn(String sendYn) {
		this.sendYn = sendYn;
	}
	public String getSmsType() {
		return smsType;
	}
	public void setSmsType(String smsType) {
		this.smsType = smsType;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public String getModId() {
		return modId;
	}
	public void setModId(String modId) {
		this.modId = modId;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	public String getModTime() {
		return modTime;
	}
	public void setModTime(String modTime) {
		this.modTime = modTime;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
		
}