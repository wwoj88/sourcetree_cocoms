jQuery(document).ready(function(){
    
    var select = $("select.color");
    
    select.change(function(){
        var select_name = $(this).children("option:selected").text();
        $(this).siblings("label").text(select_name);
    });
});


 jQuery(document).ready(function(){
 function setupLabel() {
		if ($('.label_check input').length) {
			$('.label_check').each(function(){ 
				$(this).removeClass('c_on');
			});
			$('.label_check input:checked').each(function(){ 
				$(this).parent('label').addClass('c_on');
			});                
		};
		if ($('.label_radio input').length) {
			$('.label_radio').each(function(){ 
				$(this).removeClass('r_on');
			});
			$('.label_radio input:checked').each(function(){ 
				$(this).parent('label').addClass('r_on');
			});
		};
	};
	$(document).ready(function(){
		$('body').addClass('has-js');
		$('.label_check, .label_radio').click(function(){
			setupLabel();
		});
		setupLabel(); 
	});
});

$(document).ready(function() {
	$(".gnb li").hover(function() { 
		$(this).addClass('on');
		$(this).find("ul.depth2").show();
	} , function() { 
		$(this).removeClass('on');
		$(this).find("ul.depth2").hide(); 
	});
	
	$(".gnb li ul.depth2 .mm").hover(function() {
		$(this).css({ 'background' : 'none'}); 
		$(this).find("ul.depth3").show();
	} , function() { 
		$(this).css({ 'background' : 'none'}); 
		$(this).find("ul.depth3").hide(); 
	});

});

function fileUploadActive(){

	$('.insert_file').click(function(){
		$(this).prev('.insert_file_target').click();
	});
	$('.insert_file_target').each(function(i){
		$(this).change(function(){
			$(this).prev().val($(this).val());
		})
	});
};


$(function(){
	fileUploadActive();
})

$(document).ready(function(){ 

	var menu_tab = $('.tab_property .tab'), menu_item = $('.tab_property .item');  

	menu_tab.click(function(e){ 
		  e.preventDefault();  
		  menu_tab.removeClass('active');  
		  menu_item.slideUp('normal');  

		  if($(this).next().is(':hidden') == true) {  
				 $(this).addClass('active');  
				 $(this).next().slideDown('normal');  
		  }  
	});  
});  