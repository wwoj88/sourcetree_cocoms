$(function () {

	/* register form - submit */
	//$("#form").click(function (event){
	$('#form').on('submit', function (event) {
	
		var	chgmemo= $('#ChgMemo').val();

		$('.select_style').prop('disabled',false);
		$('.membinfo input[type=text], .membinfo input[type=checkbox], .membinfo button[type=button]').prop('disabled',false);
		$('.ceoinfo input[type=text],  .ceoinfo input[type=checkbox], .ceoinfo input[type=password],.ceoinfo button[type=button]').prop('disabled',false);
		$('.list_copy input[type=text],  .list_copy input[type=checkbox], .list_copy input[type=password]').prop('disabled',false);
 
		var $form = $(this);
		var params = formToJson($form);
       
		event.preventDefault();
		
		// validation form
		if (!validForm(params)) {
			if(params['statCd']=='1'){
				return false;
			}

			
			$('.select_style').prop('disabled',true);
			$('.membinfo input[type=text], .membinfo input[type=checkbox], .membinfo button[type=button]').prop('disabled',true);
			$('.ceoinfo input[type=text],  .ceoinfo input[type=checkbox], .ceoinfo input[type=password], .ceoinfo button[type=button]').prop('disabled',true);
			$('.list_copy input[type=text],  .list_copy input[type=checkbox], .list_copy input[type=password]').prop('disabled',true);
			
		   $('input:checkbox[name=chgCd]:checked').each(function () {
				     
		          if($(this).val()=='1'){
		 			   $('.ceoinfo input[type=text],  .ceoinfo input[type=checkbox], .ceoinfo input[type=password]').prop('disabled',false);
		 			   $('#modiceoTel').prop('disabled',false);
		 			   $('#modiceoFax').prop('disabled',false);
		 			   $('#findCeoAddr').prop('disabled',false);
		 		   }
		 		   if($(this).val()=='2' ){
		 				$('input:text[name="coName"]').prop('disabled',false);   
		 		   }
		 		   if($(this).val()=='3' ){
		 				$('input:text[name="trustDetailAddr"], .membinfo button[type=button]' ).prop('disabled',false);   
		 				$('input:text[name="trustZipcode"], .membinfo button[type=button]' ).prop('disabled',false);   
		 				$('input:text[name="trustAddr"], .membinfo button[type=button]' ).prop('disabled',false);   
		 		  	    $('#moditrustTel').prop('disabled',false);
		 		  	    $('#moditrustFax').prop('disabled',false);
		 		  	    $('input:text[name="trustTel2"]' ).prop('disabled',false);   
		 		  	    $('input:text[name="trustTel1"]' ).prop('disabled',false);   
		 		  	    $('input:text[name="trustTel3"]' ).prop('disabled',false);   
		 		  	    $('input:text[name="trustFax2"]' ).prop('disabled',false);   
		 		  	    $('input:text[name="trustFax3"]' ).prop('disabled',false);   
		 		  	    $('input:text[name="trustFax1"]' ).prop('disabled',false);   
		 		   }
		 		   if($(this).val()=='6'){
		 			   $('.list_copy input[type=text],  .list_copy input[type=checkbox], .list_copy input[type=password]').prop('disabled',false);
		 		   }
		 		   
		 		})
		 		
		        
		
			return false;
		}
		
		// form submit
		if ($('#alreadyConfirm').val() == 'N') {
			
			if (!confirm('저장하시겠습니까?')) {
				return false;
			}
			
			$('#alreadyConfirm').val('Y');
		}
		
		var ww_filePath
			,ww_fileSize
			,ww_fileName;
		
		for(var i=0; i<filecnt; i++) {
			 if(!$("input[name=filePath2]:eq("+i+")").val()  ||  $("input[name=filePath2]:eq("+i+")").val() ==""){
				 continue;
			 }
		
			ww_filePath = document.createElement("input");
			ww_filePath.setAttribute("type", "hidden");
			ww_filePath.setAttribute("name", "filePath");
			ww_filePath.setAttribute("value", $("input[name=filePath2]:eq("+i+")").val());
			document.getElementById("form").appendChild(ww_filePath);

			ww_fileSize = document.createElement("input");
			ww_fileSize.setAttribute("type", "hidden");
			ww_fileSize.setAttribute("name", "fileSize");
			ww_fileSize.setAttribute("value", $("input[name=fileSize2]:eq("+i+")").val());
			document.getElementById("form").appendChild(ww_fileSize);
			
			ww_fileName = document.createElement("input");
			ww_fileName.setAttribute("type", "hidden");
			ww_fileName.setAttribute("name", "fileName");
			ww_fileName.setAttribute("value", $("input[name=fileName2]:eq("+i+")").val());
			document.getElementById("form").appendChild(ww_fileName);

		}
	
		$form.unbind().submit();
	});
	
	/* preview - print */
	$('#print').click(function () {
		window.open('about:blank', 'chargeinfo', 'toolbar=no, width=920, height=850, status=no, scrollbars=yes, resizeable = yes');

		var proc_type = $('#procType').val();
		var url = proc_type == 'sub_regist' ? '/appl/sub/reg_print' : 
			proc_type == 'trust_regist' ? '/appl/trust/reg_print' :
			proc_type == 'sub_modify' ? '/appl/sub/mod_print' :
			proc_type == 'trust_modify' ? '/appl/trust/mod_print' : '';

		var $form = $('<form />');
		$('body').append($form);
		
		
		$form.attr('action', CTX_ROOT + url);
		$form.attr('method', 'POST');
		$form.attr('target', 'chargeinfo');

		$form.submit();
	});
	
	/* preview - charge */
	$('#charge').click(function () {
		
		// 인증서 제출
		var openCert = function (callback) {
			
//			uniSignModule.open({'callback': function (signObj) {
//				
//				if (!!callback) callback(signObj);
//			}});
			
			var dummy = {};
			dummy.signedText = 'NONE';
			dummy.dn = 'NONE';
			dummy.sn = $('[name="certSn"]').val();
			if (!!callback) callback(dummy);
		}
		
		// 인증서 제출
		openCert(function (signObj) {
			
			if ($('[name="certSn"]').val() != signObj.sn) {
				alert('로그인시 사용했던 인증서를 사용하시기 바랍니다.');
				return;
			}
			
			$('[name="source"]').val(signObj.signedText);
			$('[name="userDn"]').val(signObj.dn);
			$('[name="certSn"]').val(signObj.sn);
			
			$('#chargeForm').show();
		});
	});
	
	/*$('#chargeForm #submit').click(function () {
		var $this = $(this);

		var appl = {};
		appl.cappTot = $('#cappTot').val();
		appl.pgFee = $('#pgFee').val();
		appl.remark = $('#remark').val();
		appl.conditionCd = $('#conditionCd').val();	// 1: 대리중개업, 2: 신탁업
		appl.statCd = $('#statCd').val();			// 1: 신고, 3: 변경

		// save cert info
		var saveCertInfo = function (appl, callback) {
			
			var date = toDateFormat(new Date()).replace(/[- :]/g, '');

			var params = {};
			params.useSysCd = 'SS00012';
			params.mallSeq = '1371148' + date.substring(2, 10) + date.substring(9)
			params.cappTot = appl.cappTot;
			params.pgFee = appl.pgFee;
			params.useIncCd = '1371148';
			params.setlIncCd = '1371148';
			params.recvIncCd = '1371000';
			params.remark = appl.remark;
			params.remark2 = '';
			params.reqManRrn = '';
			params.hddReqManName = '';
			params.payMthdCd = $('[name="payMthdCd"]:checked').val();

			params.returnUrl = $(location).attr('origin')+CTX_ROOT+'/appl/charge';	// callback url
			//console.log(params.returnUrl);
			
			params.mallSeqKikwanCd = '1371148';
			params.mallSeqRegdate = date.substring(2, 10);
			params.mallSeqSeqno = date.substring(9);
			params.conditionCd = appl.conditionCd;
			params.statCd = appl.statCd;

			params.source = $('[name="source"]').val();
			params.userDn = $('[name="userDn"]').val();
			params.certSn = $('[name="certSn"]').val();
			
			executeTransaction('POST', '/cert', params, function (data) {

				if (!!callback) callback(data); 
			});
		}
		
		// insert cert info
		saveCertInfo(appl, function (data) {

			window.open('about:blank', 'chargeinfo', 'toolbar=no, width=640, height=598, status=no');

			var $form = $('<form />');
			$('body').append($form);
			
			$form.attr('action', 'http://www.minwon.go.kr/main');
			$form.attr('method', 'POST');
			$form.attr('target', 'chargeinfo');

			var $input1 = $('<input type="hidden" />').attr('name', 'a').val('CS040GPKIPaySetlPreApp'); 
			var $input2 = $('<input type="hidden" />').attr('name', 'g4cssCK').val(data.g4cssCK); 
			var $input3 = $('<input type="hidden" />').attr('name', 'CertInfoSeqNo').val(data.certInfoSeqNo); 
			
			$form.append($input1).append($input2).append($input3);
			$form.submit();
			
			// test
			if (false) {
				
				setTimeout(function () {
					var $form = $('<form />');
					$form.attr('action', CTX_ROOT+'/appl/'+data.certInfoSeqNo+'/charge');
					$form.attr('method', 'POST');
					$form.attr('target', 'chargeinfo');
					
					$('body').append($form);
					$form.submit();
				}, 2000);
			}
		});
		
	});*/

	/*$('#chargeForm #submit').click(function () {
		var $this = $(this);
		var appl = {};
		appl.cappTot = $('#cappTot').val();
		appl.pgFee = $('#pgFee').val();
		appl.remark = $('#remark').val();
		appl.conditionCd = $('#conditionCd').val();	// 1: 대리중개업, 2: 신탁업
		appl.statCd = $('#statCd').val();			// 1: 신고, 3: 변경
		appl.hddReqManName = $('#hddReqManName').val();
		// save cert info
		var saveCertInfo = function (appl, callback) {
			
			var date = toDateFormat(new Date()).replace(/[- :]/g, '');

			var params = {};
			params.useSysCd = 'SS00012';
			params.mallSeq = '1371148' + date.substring(2, 10) + date.substring(9)
			params.cappTot = appl.cappTot;
			params.pgFee = appl.pgFee;
			params.useIncCd = '1371148';
			params.setlIncCd = '1371148';
			params.recvIncCd = '1371000';
			params.remark = appl.remark;
			params.remark2 = '';
			params.reqManRrn = '';
			params.hddReqManName = '';
			params.payMthdCd = $('[name="payMthdCd"]:checked').val();

	
			params.mallSeqKikwanCd = '1371148';
			params.mallSeqRegdate = date.substring(2, 10);
			params.mallSeqSeqno = date.substring(9);
			params.conditionCd = appl.conditionCd;
			params.statCd = appl.statCd;

			params.source = $('[name="source"]').val();
			params.userDn = $('[name="userDn"]').val();
			params.certSn = $('[name="certSn"]').val();
			params.returnUrl = $(location).attr('origin')+CTX_ROOT+'/appl/charge';	// callback url
			//console.log(params.returnUrl);
			
			executeTransaction('POST', '/cert', params, function (data) {

				if (!!callback) callback(data); 
			});
		}
		
		// insert cert info
		saveCertInfo(appl, function (data) {

			window.open('about:blank', 'chargeinfo', 'toolbar=no, width=640, height=598, status=no');

			var $form = $('<form />');
			$('body').append($form);
			
			$form.attr('action', 'http://www.minwon.go.kr/main');
			$form.attr('method', 'POST');
			$form.attr('target', 'chargeinfo');

			var $input1 = $('<input type="hidden" />').attr('name', 'a').val('CS040GPKIPaySetlPreApp'); 
			var $input2 = $('<input type="hidden" />').attr('name', 'g4cssCK').val(data.g4cssCK); 
			var $input3 = $('<input type="hidden" />').attr('name', 'CertInfoSeqNo').val(data.certInfoSeqNo); 
			
			$form.append($input1).append($input2).append($input3);
			$form.submit();
			
			// test
			if (false) {
				
				setTimeout(function () {
					var $form = $('<form />');
					$form.attr('action', CTX_ROOT+'/appl/'+data.certInfoSeqNo+'/charge');
					$form.attr('method', 'POST');
					$form.attr('target', 'chargeinfo');
					
					$('body').append($form);
					$form.submit();
				}, 2000);
			}
		});
		
	});*/
	
	$('#chargeForm #submit').click(function () {
		var $this = $(this);

		var appl = {};
		appl.cappTot = $('#cappTot').val();
		appl.pgFee = $('#pgFee').val();
		appl.remark = $('#remark').val();
		appl.conditionCd = $('#conditionCd').val();	// 1: 대리중개업, 2: 신탁업
		appl.statCd = $('#statCd').val();			// 1: 신고, 3: 변경
		appl.hddReqManName = $('#hddReqManName').val();
		// save cert info
		var saveCertInfo = function (appl, callback) {
			
			var date = toDateFormat(new Date()).replace(/[- :]/g, '');

			var params = {};
			params.useSysCd = 'SS00012';
			params.mallSeq = '1371148' + date.substring(2, 10) + date.substring(9)
			params.cappTot = appl.cappTot;
			params.pgFee = appl.pgFee;
			params.useIncCd = '1371148';
			params.setlIncCd = '1371148';
			params.recvIncCd = '1371000';
			params.remark = appl.remark;
			params.remark2 = '';
			params.reqManRrn = '';
			params.hddReqManName = '';
			params.payMthdCd = $('[name="payMthdCd"]:checked').val();

	
			params.mallSeqKikwanCd = '1371148';
			params.mallSeqRegdate = date.substring(2, 10);
			params.mallSeqSeqno = date.substring(9);
			params.conditionCd = appl.conditionCd;
			params.statCd = appl.statCd;

			params.source = $('[name="source"]').val();
			params.userDn = $('[name="userDn"]').val();
			params.certSn = $('[name="certSn"]').val();
			if(!$(location).attr('origin')){
				var ori=  window.location.protocol + "//" + window.location.hostname + ( window.location.port ? ':'+window.location.port : '' );
				
				
			}else{
				var ori=$(location).attr('origin');
			}
			params.returnUrl = ori+CTX_ROOT+'/appl/charge';	// callback url
			//console.log(params.returnUrl);
			
			executeTransaction('POST', '/cert', params, function (data) {

				if (!!callback) callback(data); 
			});
		}
		
		// insert cert info
		saveCertInfo(appl, function (data) {

			window.open('about:blank', 'chargeinfo', 'toolbar=no, width=640, height=598, status=no');

			var $form = $('<form />');
			$('body').append($form);
			
			//$form.attr('action', 'http://www.minwon.go.kr/main');
			$form.attr('action', 'https://www.gov.kr/main');
			$form.attr('method', 'POST');
			$form.attr('target', 'chargeinfo');
			$form.attr('accept-charset', 'euc-kr');
			
			var $input1 = $('<input type="hidden" />').attr('name', 'a').val('CS040GPKIPaySetlPreApp'); 
			var $input2 = $('<input type="hidden" />').attr('name', 'g4cssCK').val(data.g4cssCK); 
			var $input3 = $('<input type="hidden" />').attr('name', 'CertInfoSeqNo').val(data.certInfoSeqNo); 
			
			$form.append($input1).append($input2).append($input3);
			$form.submit();
			
			// test시 true 결제된것처럼 
			if (true) {
				
				setTimeout(function () {
					var $form = $('<form />');
					$form.attr('action', CTX_ROOT+'/appl/'+data.certInfoSeqNo+'/charge');
					$form.attr('method', 'POST');
					$form.attr('target', 'chargeinfo');
					
					$('body').append($form);
					$form.submit();
				}, 2000);
			}
		});
		
	});
	
	
	$('#resubmit').click(function () {
	
			if (!confirm('보완신청 하시겠습니까?')) {
				return false;
			}
		var $this = $(this);
		var $form = $('<form />');
		$form.attr('action', CTX_ROOT+'/appl/recharge');
		$form.attr('method', 'POST');
		$form.attr('target', 'chargeinfo');
		
		$('body').append($form);
		$form.submit();
		
	});
	
	$('#saved').click(function () {
		var $form = $('#form');

		event.preventDefault();
		
			$form.submit();
	
	});
	
});
