$(function () {

	// writing kind checkbox
	$('input:radio[name="coGubunCd"]').change(function () {
		var $this = $(this);
		var val = $this.val();
		
		if (val == '1') {
			$('[id^="coGubunCd2_layer"]').hide();
		}
		else {
			$('[id^="coGubunCd2_layer"]').show();
		}
	});
	
	// writing kind checkbox
	$('input:checkbox[id^="writingKind"]').click(function () {
		var $this = $(this);
		var checked = $this.prop('checked');
		var writing_kind = $this.attr('id').replace('writingKind', '');
		
		if (!checked) {
			$('input:checkbox[name="permkind'+writing_kind+'"]').prop('checked', false);
			$('input:text[name="permEtc'+writing_kind+'"]').prop('disabled', true).val('');
		}
	});
	
	// perm kind checkbox
	$('input:checkbox[id^="permkind"]').click(function () {
		var $this = $(this);
		var name = $this.attr('name');
		var checked = $this.prop('checked');
		var writing_kind = $this.attr('name').replace('permkind', '');
		
		if (checked) {
			$('input:checkbox[id="writingKind'+writing_kind+'"]').prop('checked', true);
		}
		
		var cnt = $('input:checkbox[name="'+name+'"]:checked').length;
		if (cnt == 0) {
			$('input:checkbox[id="writingKind'+writing_kind+'"]').prop('checked', false);
		}
	});

	// 기타 checkbox
	$('input:checkbox[id^="permkind"][id$="_0"]').click(function () {
		var $this = $(this);
		var checked = $this.prop('checked');
		var writing_kind = $this.attr('name').replace('permkind', '');
		
		if (checked) {
			$('input:text[name="permEtc'+writing_kind+'"]').prop('disabled', false);	
		}
		else {
			$('input:text[name="permEtc'+writing_kind+'"]').prop('disabled', true).val('');
		}
	});
	
	// daum address searcher 
	var popupDaumAddr = function (postcodeId, addrId, sidoId, sigunguId, bnameId) {
		daum.postcode.load(function(){
		new daum.Postcode({
			oncomplete: function(data) {
				
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById(postcodeId).value = data.zonecode; //5자리 새우편번호 사용
//                document.getElementById('sample4_roadAddress').value = fullRoadAddr;
//                document.getElementById('sample4_jibunAddress').value = data.jibunAddress;
                if (!!fullRoadAddr) {
                	document.getElementById(addrId).value = fullRoadAddr;
                }
                else {
                	document.getElementById(addrId).value = data.jibunAddress;
                }

                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    //예상되는 도로명 주소에 조합형 주소를 추가한다.
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
//                    document.getElementById('guide').innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
//                    document.getElementById('guide').innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

                } else {
//                    document.getElementById('guide').innerHTML = '';
                }
                
                if (!!sidoId && !!data.sido) {
                	document.getElementById(sidoId).value = data.sido;
                }
                if (!!sigunguId && !!data.sigungu) {
                	document.getElementById(sigunguId).value = data.sigungu;
                }
                if (!!bnameId && !!data.roadname) {
                    var val = document.getElementById(addrId).value;
                    val = val.substring(val.lastIndexOf(data.roadname));
                	document.getElementById(bnameId).value = val;
                }
			}
		}).open();
	})
	};
	/* 회원정보 주소찾기 */
	$('#findTrustAddr').click(function () {
		
		popupDaumAddr('trustZipcode', 'trustAddr', 'trustSido', 'trustGugun', 'trustDong');
	});
	
	/* 대표자정보 주소찾기 */
	$('#findCeoAddr').click(function () {
		
		popupDaumAddr('ceoZipcode', 'ceoAddr', 'ceoSido', 'ceoGugun', 'ceoDong');
	});

	/* register form - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();

		// validation form
		var validForm = function (params) {
			
			if (!params['coNo1'] 
					|| !params['coNo2'] 
					|| !params['coNo3']) {
				alert('사업자등록번호를 입력해 주세요.');
				return false;
			}
			if (!params['coNo1'].match(/^[0-9]{3}$/)
					|| !params['coNo2'].match(/^[0-9]{2}$/)
					|| !params['coNo3'].match(/^[0-9]{5}$/)) {
				alert('사업자번호 형식에 맞지 않습니다.');
				return false;
			}
			if (!check_busino(params['coNo1'] + params['coNo2'] + params['coNo3'] + '')) {
				alert('사업자번호가 유효하지않습니다.');
				return false;
			}
			
			if (!params['bubNo']) {
				alert('법인 등록번호를 입력해 주세요.');
				return false;
			}
			if (!params['bubNo'].match(/^[0-9]+$/)) {
				alert('법인 등록번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
				return false;
			}
			
			if (!params['trustZipcode']
//					|| !params['trustAddr']
					|| !params['trustDetailAddr']) {
				alert('주소를 입력해 주세요.');
				return false;
			}
			
			if (!params['trustTel1']
					|| !params['trustTel2']
					|| !params['trustTel3']) {
				alert('전화번호를 입력해 주세요.');
				return false;
			}
			if (!params['trustTel1'].match(/^[0-9]+$/)
					|| !params['trustTel2'].match(/^[0-9]+$/)
					|| !params['trustTel3'].match(/^[0-9]+$/)) {
				alert('전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
				return false;
			}
			
			if (!params['ceoName']) {
				alert('대표자명을 입력해 주세요.');
				return false;
			}
			
			if (!params['ceoRegNo1']
					|| !params['ceoRegNo2']) {
				alert('주민등록번호를 입력해 주세요.');
				return false;
			}
			if (!params['ceoRegNo1'].match(/^[0-9]{6}$/)
					|| !params['ceoRegNo2'].match(/^[0-9]{7}$/)) {
				alert('주민등록번호 형식에 맞지 않습니다.)');
				return false;
			}
			
			if (!params['ceoZipcode']
//					|| !params['ceoAddr']
					|| !params['ceoDetailAddr']) {
				alert('주소를 입력해 주세요.');
				return false;
			}
			
			if (!params['ceoMobile1']
					|| !params['ceoMobile2']
					|| !params['ceoMobile3']) {
				alert('휴대전화번호를 입력해 주세요.');
				return false;
			}			
			if (!params['ceoMobile1'].match(/^[0-9]+$/)
					|| !params['ceoMobile2'].match(/^[0-9]+$/)
					|| !params['ceoMobile3'].match(/^[0-9]+$/)) {
				alert('휴대전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
				return false;
			}
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
		
		// form submit
		$form.unbind().submit();
	});
	
	/* preview - charge */
	$('#charge').click(function () {
		
		// 인증서 제출
		var openCert = function (callback) {
			
			uniSignModule.open({'callback': function (signObj) {
				
				if (!!callback) callback(signObj);
			}});
		}
		
		// 인증서 제출
		openCert(function (signObj) {
			
			$('[name="source"]').val(signObj.signedText);
			$('[name="userDn"]').val(signObj.dn);
			$('[name="certSn"]').val(signObj.sn);
			
			$('#chargeForm').show();
		});
	});
	
	$('#chargeForm #submit').click(function () {
		var $this = $(this);

		// TODO
		var appl = {};
		appl.cappTot = '4000';
		appl.pgFee = '160';
		appl.remark = '대리중개업 신고'; 
		appl.conditionCd = '1';	// 1: 대리중개업, 2: 신탁업
		appl.statCd = '1';		// 1: 신고, 3: 변경

		// make common data
//		var getData = function (callback) {
//
//			executeTransaction('GET', '/sub/data', {}, function (sess) {
//
//				var date = toDateFormat(new Date()).replace(/[- :]/g, '');
//				var mall_seq = '1371148' + date.substring(2, 10) + date.substring(9);
//
//				// TODO
//				var appl = {};
//				appl.capp_tot = '4000';
//				appl.pg_fee = '160';
//				appl.remark = '대리중개업 신고'; 
//
//				var data = {};
//				data.useSysCd = 'SS00012';
//				data.mallSeq = mall_seq;
//				data.cappTot = appl.capp_tot;
//				data.pgFee = appl.pg_fee;
//				data.useIncCd = '1371148';
//				data.setlIncCd = '1371148';
//				data.recvIncCd = '1371000';
//				data.remark = appl.remark;
//				data.remark2 = sess.remark2;
//				data.reqManRrn = sess.req_man_rrn;
//				data.hddReqManName = sess.hddReqManName;
//				data.payMthdCd = $('[name="payMthdCd"]').val();
//				
//				if (!!callback) callback(data); 
//			});
//		}
		
		// make g4cssCK data
//		var makeG4cssCK = function (data, callback) {
//			
//			var str = '';
//			str += data.useSysCd.replace(/\^/g, '<>')+'^';
//			str += data.mallSeq.replace(/\^/g, '<>')+'^';
//			str += data.cappTot.replace(/\^/g, '<>')+'^';
//			str += data.pgFee.replace(/\^/g, '<>')+'^';
//			str += data.useIncCd.replace(/\^/g, '<>')+'^';
//			str += data.setlIncCd.replace(/\^/g, '<>')+'^';
//			str += data.recvIncCd.replace(/\^/g, '<>')+'^';
//			str += data.remark.replace(/\^/g, '<>')+'^';
//			str += data.remark2.replace(/\^/g, '<>')+'^';
//			str += data.reqManRrn.replace(/\^/g, '<>')+'^';
//			str += data.hddReqManName.replace(/\^/g, '<>')+'^';
//			str += data.payMthdCd.replace(/\^/g, '<>')+'^';
//			str += $(location).attr('origin')+'/'+CTX_ROOT+'/sub/charge';	// callback url
//
//			executeTransaction('POST', '/sub/g4cssCK', str, function (g4cssCK) {
//
//				if (!!callback) callback(g4cssCK); 
//			});
//		}
		
		// insert cert info
//		var saveCertInfo = function (data, callback) {
//			
//			var date = toDateFormat(new Date()).replace(/[- :]/g, '');
//
//			// data 11 (remark2 제외)
//			data.mallSeqKikwanCd = '1371148';
//			data.mallSeqRegdate = date.substring(2, 10);
//			data.mallSeqSeqno = date.substring(9);
//			
//			executeTransaction('POST', '/sub/cert', data, function (certInfoSeqNo) {
//
//				if (!!callback) callback(certInfoSeqNo); 
//			});
//		}
		
		// save cert info
		var saveCertInfo2 = function (appl, callback) {
			
			var date = toDateFormat(new Date()).replace(/[- :]/g, '');

			var params = {};
			params.useSysCd = 'SS00012';
			params.mallSeq = '1371148' + date.substring(2, 10) + date.substring(9)
			params.cappTot = appl.cappTot;
			params.pgFee = appl.pgFee;
			params.useIncCd = '1371148';
			params.setlIncCd = '1371148';
			params.recvIncCd = '1371000';
			params.remark = appl.remark;
			params.remark2 = '';
			params.reqManRrn = '';
			params.hddReqManName = '';
			params.payMthdCd = $('[name="payMthdCd"]').val();

			params.returnUrl = $(location).attr('origin')+'/'+CTX_ROOT+'/sub/charge';	// callback url

			params.mallSeqKikwanCd = '1371148';
			params.mallSeqRegdate = date.substring(2, 10);
			params.mallSeqSeqno = date.substring(9);
			params.conditionCd = appl.conditionCd;
			params.statCd = appl.statCd;

			params.source = $('[name="source"]').val();
			params.userDn = $('[name="userDn"]').val();
			params.certSn = $('[name="certSn"]').val();
			
			executeTransaction('POST', '/cert', params, function (data) {

				if (!!callback) callback(data); 
			});
		}
		
		// make common data
//		getData(function (data) {
//			
//			// make g4cssCK data
//			makeG4cssCK(data, function (g4cssCK) {
//				
//				// insert cert info
//				saveCertInfo(data, function (certInfoSeqNo) {
//
//					window.open('about:blank', 'chargeinfo', 'toolbar=no, width=640, height=598, status=no');
//
//					var $form = $('<form />');
//					$('body').append($form);
//					
//					$form.attr('action', 'http://www.minwon.go.kr/main');
//					$form.attr('method', 'POST');
//					$form.attr('target', 'chargeinfo');
//
//					var $input1 = $('<input type="hidden" />').attr('name', 'a').val('CS040GPKIPaySetlPreApp'); 
//					var $input2 = $('<input type="hidden" />').attr('name', 'g4cssCK').val(g4cssCK); 
//					var $input3 = $('<input type="hidden" />').attr('name', 'CertInfoSeqNo').val(certInfoSeqNo); 
//					
//					$form.append($input1).append($input2).append($input3);
//					$form.submit();
//				});
//			});
//		});

		// insert cert info
		saveCertInfo2(appl, function (data) {

			window.open('about:blank', 'chargeinfo', 'toolbar=no, width=640, height=598, status=no');

			var $form = $('<form />');
			$('body').append($form);
			
			$form.attr('action', 'http://www.minwon.go.kr/main');
			$form.attr('method', 'POST');
			$form.attr('target', 'chargeinfo');

			var $input1 = $('<input type="hidden" />').attr('name', 'a').val('CS040GPKIPaySetlPreApp'); 
			var $input2 = $('<input type="hidden" />').attr('name', 'g4cssCK').val(data.g4cssCK); 
			var $input3 = $('<input type="hidden" />').attr('name', 'CertInfoSeqNo').val(data.certInfoSeqNo); 
			
			$form.append($input1).append($input2).append($input3);
			$form.submit();
		});
		
	});
	
//	$('#submit').click(function () {
//		submit();
//	});
	
});

//function submit() {
//
//	var $form = $('<form />');
//	$form.attr('action', CTX_ROOT+'/sub/charge');
//	$form.attr('method', 'POST');
//	
//	$('body').append($form);
//	$form.submit();
//}
