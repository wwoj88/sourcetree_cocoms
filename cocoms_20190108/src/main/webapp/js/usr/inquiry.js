$(function () {

	/* register form - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['title']) {
				alert('제목을 입력해 주세요');
				return false;
			}

			if (!params['content']) {
				alert('내용을 입력해 주세요');
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
    $('#submit').click(function (event) {
		var $form = $('#form');

    	event.preventDefault();
    	
    	$form.submit();
    });
    
});
