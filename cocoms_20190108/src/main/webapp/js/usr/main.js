$(function () {

	$('.main_slider_inner').bxSlider({
		auto: true,
		autoStart: true,
		speed: 400,
		pager: true,
		pause: 5000,
		autoHover: true,
		controls: true
	});

	$('[name="cert_probe"]').click(function (event) {
		
		event.preventDefault();
		
		window.open(CTX_ROOT+'/cert/probe', 'icert', 'width=650,height=600,toolbar=no,location=no,directories=no,status=yes,menubar=np,scrollbars=no,copyhistory=no,resizable=no,left=0,top=0');
		
	});
	$('#cert_probe1,#cert_probe2').click(function (event) {
		
		event.preventDefault();
		
		window.open(CTX_ROOT+'/cert/probe', 'icert', 'width=650,height=600,toolbar=no,location=no,directories=no,status=yes,menubar=np,scrollbars=no,copyhistory=no,resizable=no,left=0,top=0');
		
	});
	$('#agree4', '#form').click(function () {
		var $this = $(this);
		var $form = $('#form');
		
		if ($this.prop('checked')) {
			$form.find('#agree1,#agree1b,#agree2,#agree3').prop('checked', true);
		}
	});
	
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['agree1'] || params['agree1'] != 'Y') {
				alert('서비스 약관을 동의해 주세요.');
				return false;
			}
			if (!params['agree1b'] || params['agree1b'] != 'Y') {
				alert('권리자찾기정보시스템 이용약관을 동의해 주세요.');
				return false;
			}
			if (!params['agree2'] || params['agree2'] != 'Y') {
				alert('개인정보 수집 및 이용에 동의해 주세요.”');
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
		
		// form submit
		$form.unbind().submit();
	});
	
});
