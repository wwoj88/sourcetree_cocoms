$(function() {

	/* step 01 - 일반회원/사업자회원을 선택 */
	$('input:radio[name="coGubunCd"]', '#form1').change(function() {
		var $this = $(this);
		var val = $this.val();

		if (val == '1') {
			$('#layer1').show();
			$('#layer2').hide();
		} else {
			$('#layer1').hide();
			$('#layer2').show();
		}
	});

	$('#form0_12').click(function() {
		var $form = $('#form0');
		var page = $('<input type="hidden" value="1" name="coGubunCd">');
		$form.append(page);
		// console.log( $form.append(page));
		$form.unbind().submit();
	});
	$('#form0_13').click(function() {
		// alert('5');
		var $form = $('#form0');
		var page = $('<input type="hidden" value="2" name="coGubunCd">');
		//var page1 = $('<input type="hidden" value="1" name="bubNo">');
		//$form.append(page).append(page1);
		$form.append(page);
		// console.log( $form.append(page));
		$form.unbind().submit();
	});
	$('#form0_14').click(function() {

		var $form = $('#form0');
		var page = $('<input type="hidden" value="2" name="coGubunCd">');
		//var page1 = $('<input type="hidden" value="2" name="bubNo">');
//		$form.append(page).append(page1);
		$form.append(page);
		// console.log( $form.append(page));
		$form.unbind().submit();
	});
	/* step 01 - submit */
	$('#form1')
			.on(
					'submit',
					function(event) {

						var $form = $(this);
						var params = formToJson($form);

						event.preventDefault();

						// validation form
						var validForm = function(params) {
							
							//alert(params['bubNo1'] + params['bubNo2']);
							if (!params['coGubunCd']) {
								alert('일반회원/사업자회원을 선택해 주세요.');
								return false;
							}

							if (params['coGubunCd'] == '1') {

								if (!params['ceoName']) {
									alert('성명을 입력해 주세요.');
									return false;
								}

								if (!params['ceoRegNo1']
										|| !params['ceoRegNo2']) {
									alert('주민번호을 입력해 주세요.');
									return false;
								}
								/*
								 * if (!params['ceoRegNo1'].match(/^[0-9]{6}$/) ||
								 * !params['ceoRegNo2'].match(/^[0-9]{7}$/)) {
								 * alert('주민번호 형식에 맞지 않습니다.'); return false; }
								 */
								var jumins3 = params['ceoRegNo1']
										+ params['ceoRegNo2'];
								// 주민등록번호 생년월일 전달

								// var fmt = RegExp(/^\d{6}[1234]\d{6}$/) //포멧
								// 설정
								var buf = new Array(13);
								var regExp = /^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))[1-6][0-9]{6}$/;
								// '-'제외한 주민번호 검증 (외국인등록번호도 포함)
								// 주민번호 유효성 검사
								if (!regExp.test(jumins3)) {

									alert("주민등록번호 형식에 맞게 입력해주세요");

									return false;
								}

								
								var ssnCheck = {
									/**
									 * 유효성검사. allCheck("8201011234567");
									 */
									allCheck : function(rrn) {
										if (this.fnrrnCheck(rrn)
												|| this.fnfgnCheck(rrn)) {

											return true;
										}

										return false;
									},

									/**
									 * 주민등록번호 유효성검사
									 */
									fnrrnCheck : function(rrn) {
										var sum = 0;

										if (rrn.length != 13) {
											return false;
										} else if (rrn.substr(6, 1) != 1
												&& rrn.substr(6, 1) != 2
												&& rrn.substr(6, 1) != 3
												&& rrn.substr(6, 1) != 4) {
											return false;
										}

										for (var i = 0; i < 12; i++) {
											sum += Number(rrn.substr(i, 1))
													* ((i % 8) + 2);
										}

										if (((11 - (sum % 11)) % 10) == Number(rrn
												.substr(12, 1))) {
											return true;
										}

										return false;
									},

									/**
									 * 외국인등록번호 유효성검사
									 * 
									 * @param rrn
									 * @returns
									 */
									fnfgnCheck : function(rrn) {
										var sum = 0;

										if (rrn.length != 13) {
											return false;
										} else if (rrn.substr(6, 1) != 5
												&& rrn.substr(6, 1) != 6
												&& rrn.substr(6, 1) != 7
												&& rrn.substr(6, 1) != 8) {
											return false;
										}

										if (Number(rrn.substr(7, 2)) % 2 != 0) {
											return false;
										}

										for (var i = 0; i < 12; i++) {
											sum += Number(rrn.substr(i, 1))
													* ((i % 8) + 2);
										}

										if ((((11 - (sum % 11)) % 10 + 2) % 10) == Number(rrn
												.substr(12, 1))) {
											return true;
										}

										return false;
									}

								}
								if (ssnCheck.allCheck(jumins3) == false) {
									alert('주민등록번호 또는 외국인등록번호를 정확하게 입력해주세요.');
									return false;
								}

							}
							if (params['coGubunCd'] == '2') {

								if (!params['coName']) {
									alert('회사명을 입력해 주세요.');
									return false;
								}
								//alert(params['bubNo']);
								if (params['bubNo']=='2'){
									if (!params['bubNo1'] || !params['bubNo2'] ) {
										alert('법인 등록번호를 입력해 주세요.');
										return false;
									}
								}else{
										if (!params['coNo1'] || !params['coNo2'] || !params['coNo3']) {
											alert('사업자등록번호를 입력해 주세요.');
											return false;
										}
										if (!params['coNo1'].match(/^[0-9]{3}$/)|| !params['coNo2'].match(/^[0-9]{2}$/)	|| !params['coNo3'].match(/^[0-9]{5}$/)) {
											alert('사업자등록번호 형식에 맞지 않습니다.');
											return false;
										}
										if (!check_busino(params['coNo1']+ params['coNo2'] + params['coNo3']+ '')) {
											alert('사업자번호가 유효하지않습니다.');
											return false;
										}
								}
							}

							return true;
						}

						// 기가입체크
						var check = function(params, callback) {

							params.ceoRegNo = params.ceoRegNo1+ params.ceoRegNo2;
							params.coNo = params.coNo1 + params.coNo2+ params.coNo3;
							//alert(isNaN(params.coNo));
							
							if(isNaN(params.coNo)==true){
							params.coNo = params.bubNo1 + params.bubNo2;
								alert(params.coNo);
							}
							
							params.regNo = params.coGubunCd == '1' ? params.ceoRegNo1+ params.ceoRegNo2: params.coNo;
							
							executeTransaction(
									'GET',
									'/userid',
									params,
									function(data) {

										if (!!data.total && Number(data.total) >= 2) {
											var msg = '회원가입은 동일한 주민(사업자)번호로 최대 2개 계정을 생성할 수 있습니다.';
												msg += '아이디찾기를 통해 가입되어있는 계정정보를 확인해주세요.';
											if (confirm(msg)) {
												$(location).attr('href',
														CTX_ROOT + '/find/id');
											}
											return false;
										}

										if (!!callback)
											callback();
									});
						}

						// validation form
						if (!validForm(params)) {
							return false;
						}

						// 기가입체크
						check(	params,	function() {

									var regNo = params['coGubunCd'] == '1' ? params['ceoRegNo']: params['coNo']; // verify cert
									//alert("4444444 "+regNo);
									verifyCert(regNo,function(certAttrs,signedText) {

										$form.find('[name="certificate"]').val(signedText);
										$form.find('[name="userDn"]').val(certAttrs.subjectName);
										$form.find('[name="certSn"]').val(certAttrs.serialNumber);
										$form.find('[name="certStartDate"]').val(certAttrs.validateFrom2);
										$form.find('[name="certEndDate"]').val(certAttrs.validateTo2);

										// form submit
										$form.unbind().submit();
									});
								});
					});

	/* step 02 - all check */
	$('#agree4', '#form2').click(
			function() {
				var $this = $(this);
				var $form = $('#form2');

				if ($this.prop('checked')) {
					$form.find('#agree1,#agree1b,#agree2,#agree3').prop(
							'checked', true);
				}
			});

	/* step 02 - submit */
	$('#form2').on('submit', function(event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();

		// validation form
		var validForm = function(params) {

			if (!params['agree1'] || params['agree1'] != 'Y') {
				alert('서비스 약관을 동의해 주세요.');
				return false;
			}
			if (!params['agree1b'] || params['agree1b'] != 'Y') {
				alert('권리자찾기정보시스템 이용약관을 동의해 주세요.');
				return false;
			}
			if (!params['agree2'] || params['agree2'] != 'Y') {
				alert('개인정보 수집 및 이용에 동의해 주세요.”');
				return false;
			}

			return true;
		}

		// validation form
		if (!validForm(params)) {
			return false;
		}

		// form submit
		$form.unbind().submit();
	});

	/* step 04 - login id */
	$('[name="loginId"]', '#form4').change(function() {
		var $form = $('#form4');

		$form.find('#checkId').data('check', false);
	});

	/* step 04 - check id */
	var checking = false;
	$('#checkId', '#form4').click(function() {
		var $this = $(this);
		var $form = $('#form4');

		var loginId = $form.find('[name="loginId"]').val();

		if (!loginId || checking) {
			return false;
		}
		if (!loginId.match(/^[a-zA-Z][0-9a-zA-Z-_]{3,50}$/)) {
			alert('아이디 형식에 맞지 않습니다.\n(3글자이상의 영문자,숫자로 구성해주시기 바랍니다.)');
			return false;
		}
		var params = {};
		checking = true;
		executeTransaction('GET', '/user/' + loginId, params, function(data) {
			if (!data.exists) {
				alert('해당아이디는 사용가능합니다.');
				$this.data('check', true);
			} else {
				alert('아이디가 이미 사용중 이거나  탈퇴한 아이디입니다.\n다른 아이디를 입력해주세요.');
				$this.data('check', false);
			}
			checking = false;
		});
	});

	/* step 04 - submit */
	$('#form4')
			.on(
					'submit',
					function(event) {
						var $form = $(this);
						var params = formToJson($form);

						event.preventDefault();

						// validation form
						var validForm = function($form, params) {
							var check = /^(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?=.*[0-9]).{8,20}$/;
							// if (!$.trim(params['ceoName'])) {
							// alert('이름을 입력해 주세요.');
							// return false;
							// }

							if (!$.trim(params['loginId'])) {
								alert('아이디를 입력해 주세요.');
								return false;
							}
							if (!params['loginId']
									.match(/^[a-zA-Z][0-9a-zA-Z-_]{3,50}$/)) {
								alert('아이디 형식에 맞지 않습니다.\n(영문자,숫자로 구성해주시기 바랍니다.)');
								return false;
							}

							if (!$.trim(params['pwd'])
									|| !$.trim(params['pwd2'])) {
								alert('비밀번호를 입력해 주세요.');
								return false;
							}
							if (params['pwd'] != params['pwd2']) {
								alert('비밀번호가 일치하지 않습니다.');
								return false;
							}
							if (params['pwd'].length < 8) {
								alert('비밀번호가 너무 짧습니다.\n(8자이상 입력해주세요.)');
								return false;
							}
							if (params['pwd'].length > 20) {
								alert('비밀번호가 너무 깁니다.\n(20자이하 입력해주세요.)');
								return false;
							}
							if (!check.test(params['pwd'])) {
								alert("영문, 숫자, 특수문자의 조합으로 입력해주세요.");
								return false;
							}
							if (!$.trim(params['ceoMobile2'])
									|| !$.trim(params['ceoMobile3'])) {
								alert('휴대전화번호를 입력해 주세요.');
								return false;
							}
							if (!params['ceoMobile2'].match(/^[0-9]+$/)
									|| !params['ceoMobile3'].match(/^[0-9]+$/)) {
								alert('휴대전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
								return false;
							}

							if (!$.trim(params['ceoEmail1'])
									|| !$.trim(params['ceoEmail2'])) {
								alert('이메일을 입력해 주세요.');
								return false;
							}
							if (!(params['ceoEmail1'] + '@' + params['ceoEmail2'])
									.match(/[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,3}$/)) {
								alert('이메일 형식에 맞지 않습니다.');
								return false;
							}

							$form.find('[name="ceoMobile"]').val(
									params['ceoMobile1'] + '-'
											+ params['ceoMobile2'] + '-'
											+ params['ceoMobile3']);
							$form.find('[name="ceoEmail"]').val(
									params['ceoEmail1'] + '@'
											+ params['ceoEmail2']);

							return true;
						}

						// validation form
						if (!validForm($form, params)) {
							return false;
						}

						if (!$form.find('#checkId').data('check')) {
							alert('아이디 중복확인을 해주십시요.');
							return false;
						}

						// form submit
						$form.unbind().submit();
					});

	// move to prev page
	$('[id^="move_step"]').click(function() {
		var $this = $(this);

		var $form = $('<form />');
		$('body').append($form);

		var step = $this.attr('id').replace('move_step', '');
		$form.attr('method', 'POST').attr('action', '?step=' + step);
		$form.submit();
	});

});
