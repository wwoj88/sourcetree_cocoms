$(function () {

	var checkPrintId = function (printId) {
		
		var params = {};
		executeTransaction('POST', '/cert/probe/'+printId, params, function (data) {
			
			if (!!data && data.result) {
				alert('입력하신 발급번호는 원본확인 신고증입니다.');
			}
			else {
				alert('입력하신 발급번호는 미확인 신고증입니다.');
			}
		});
	}
	
	$('#check_old').click(function (event) {
		var $this = $(this);
		var $form = $('#form_old');
		
		event.preventDefault();
		
		var printId = $form.find('[name="key1"]').val()
				+ '-' + $form.find('[name="key2"]').val()
				+ '-' + $form.find('[name="key3"]').val()
				+ '-' + $form.find('[name="key4"]').val();
		printId = printId.toUpperCase();

		checkPrintId(printId);
	});
	
	$('#check_new').click(function (event) {
		var $this = $(this);
		var $form = $('#form_new');
		
		event.preventDefault();
		
		var printId = $form.find('[name="key1"]').val()
				+ '-' + $form.find('[name="key2"]').val()
				+ '-' + $form.find('[name="key3"]').val()
				+ '-' + $form.find('[name="key4"]').val()
				+ '-' + $form.find('[name="key5"]').val();
		
		checkPrintId(printId);
	});
	
	$('#info1').click(function (event) {
		
		event.preventDefault();
		
		window.open(CTX_ROOT+'/cert/info1', 'certinfo', 'width=650,height=595,location=no,toolbar=no,status=no,menubar=no,scrollbars=yes,resizable=no');
	});
	
});
