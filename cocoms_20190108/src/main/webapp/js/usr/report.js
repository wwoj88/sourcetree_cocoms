$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	$form.submit();
    }

	
	$('#checkAll').click(function () {
		var $this = $(this);
		
		if ($this.prop('checked')) {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', true);
		}
		else {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', false);
		}
	});
	
    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});
	
	$('#report_all').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();

		var year = $('#year').val();
		var uri = CTX_ROOT + '/admin/reports/sub/all/'+year;
		
		var $form = $('<form method="GET" target="_blank" />').attr('action', uri);
		$('body').append($form);
		
		$form.submit();
	});

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// YEARLY DETAIL
	
	$('[name="rptYear"]').change(function () {
		var $this = $(this);
		
		var url = $(location).attr('pathname').replace(CTX_ROOT, '');
		url = url.substring(0, url.lastIndexOf('/')+1) + $this.val();
		
		movePage(url);
	});
	
//	$('#edit').click(function () {
//
//		event.preventDefault();
//
//		var url = $(location).attr('pathname') + '/edit';
//		movePage(url);
//	});

	$('#deleteList').click(function(){
	
		var checkArr  = new Array();
		var chk       = $('#data :checkbox[id^="checkbox_"]:checked').length;
		if(chk == 0){
			alert('삭제할 저작물을 선택해주세요.');
			return false;
		}
		
		if (!confirm('해당 저작물을 삭제하시겠습니까?')) {
			return false;
		}
		$('#data :checkbox[id^="checkbox_"]:checked').each(function(){
			checkArr.push($(this).val());
			
		});
				
		
		$.ajax({
			    type: 'POST',
	            url: CTX_ROOT+'/report/deleteData',
	            data :{chbox:checkArr},
	            async: false,
	            dataType :'json',
	            success: function (data) {
	            	
	            	if(data.success == true){
	            		 alert("저작물 삭제하였습니다" );
		            	 location.reload();
	            	}else{
	            		alert("삭제에 실패하였습니다.");
	            		location.reload();
	            	}
	            }
		})
		//alert(checkArr);
		//console.log(checkArr);
	});
	

	$('#checkButton').click(function () {

		
			
			var $this= $(this);
			var $form = $this.closest('form');
			var data = $this.data();
			var dataform = $this.gercd;
			var form = $('#formUpload')[0];
			

			var data = new FormData(form);
			
			
			//alert($('#gercd').val());
			var gercd = 0;
			var REPT_CHRR_NAME = $('#REPT_CHRR_NAME').val();
			var REPT_CHRR_POSI = $('#REPT_CHRR_POSI').val();
			var REPT_CHRR_TELX = $('#REPT_CHRR_TELX').val();
			var REPT_CHRR_MAIL = $('#REPT_CHRR_MAIL').val();
		
			if(REPT_CHRR_NAME ==null || REPT_CHRR_NAME==undefined || REPT_CHRR_NAME ==""){
				alert("담당자를 작성해주세요.");
				 $('#REPT_CHRR_NAME').focus();
				 return;
			}
			if(REPT_CHRR_POSI ==null || REPT_CHRR_POSI==undefined || REPT_CHRR_POSI ==""){
				alert("담당자 직위를 작성해주세요.");
				 $('#REPT_CHRR_POSI').focus();
				 return;
			}
			if(REPT_CHRR_TELX ==null || REPT_CHRR_TELX==undefined || REPT_CHRR_TELX ==""){
				alert("담당자 전화번호를 작성해주세요.");
				 $('#REPT_CHRR_TELX').focus();
				 return;
			}
			if(REPT_CHRR_MAIL ==null || REPT_CHRR_MAIL==undefined || REPT_CHRR_MAIL ==""){
				alert("담당자 이메일을 작성해주세요.");
				 $('#REPT_CHRR_MAIL').focus();
				 return;
			}

			if(!isEmail(REPT_CHRR_MAIL)){
				alert("이메일 형식에 맞게 적어주세요.");
				 return;
			}


			if(doubleSubmitCheck()) return;
			if (!confirm('보고 저작물 없음으로 보고합니다. \n확인해주세요.')) {
				return false;
			}
//			$form.find('#status').text('업로드 중 입니다.').show();
			//$('#modalBack').show(); 
			//$('#Progress_Loading').show(); 
			$('#modalBack').attr('style', "display:show;");
			$('#Progress_Loading').attr('style', "display:show; position: absolute; left: 40%;	top: 38%;");
	        $.ajax({
	            type: 'POST',
	            enctype: 'multipart/form-data',
	            url: CTX_ROOT+'/report/excel/noData',
	            data: data,
	            dataType: 'json',
	            processData: false,
	            contentType: false,
	            cache: false,
	            timeout: 7200000,
	            success: function (data) {
	            	
	            if(data.success == true){
	            	$('#Progress_Loading').attr('style', "display:none;");
	        		$('#modalBack').attr('style', "display:none;");
	            	 alert(data.errorMessage);
	            	 location.reload();
	            }else if (data.success==false){
	            	$('#Progress_Loading').attr('style', "display:none;");
	        		$('#modalBack').attr('style', "display:none;");
	            	//$('#Progress_Loading').hide();
	            	//$('#modalBack').hide(); 
	            	 alert(data.errorMessage);
	            	 location.reload();
	            }
	            	if (data.files.length > 0) {
	            		
	                	
	            	}
	            	else {
	            		alert('첨부할 수 없는 파일형식입니다.');
	            		
	            		$this.siblings(':text').val('');
	            	}
	            	
//	            	$form.find('#status').text('업로드되었습니다.');
	            },
	            error: function (e) {
	            
	                alert(e.responseText);
	            }
	          
	        });
		});
	$('#print').click(function (event) {
		
	    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

	    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
	    mywindow.document.write('</head><body >');
	    mywindow.document.write('<h1>' + document.title  + '</h1>');
	    mywindow.document.write(document.getElementById('print_area').innerHTML);
	    mywindow.document.write('</body></html>');

	    mywindow.document.close(); // necessary for IE >= 10
	    mywindow.focus(); // necessary for IE >= 10*/

	    mywindow.print();
	    mywindow.close();

	    return true;
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// YEARLY DETAIL EDIT FORM
	
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		var validForm = function (params) {
		if(!params['trust']){
			if(!params['regName'] ){
				alert('작성자를 입력해주세요.');
				return false;
			}
		}
			if(!params['trust']){
			if(!params['regTel']){
				alert('전화번호를 입력해주세요.');
				return false;
			}
			}
			if(!params['trust']){
			if (!params['regTel'].match(/^[0-9 -]+$/)) {
				alert('전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
				return false;
			}
			}
			if(!params['trust']){
			if(!params['regEmail']){
				alert('이메일를 입력해주세요.');
				return false;
			}
		}
			
		
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
		
		// form submit
		if (confirm('사용료,수수료 단위는 천원입니다. 저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
	var setSum = function (name) {
		var sum = 0;
		$('[name="'+name+'"]').each(function (key, val) {
			sum += Number($(val).val());
		});
		$('#'+name).val(sum);
	}
	
	/* 파일첨부 */
	$(':file[name^="file"]').change(function () {
		var $this= $(this);
		var $form = $this.closest('form');
		var data = $this.data();
		var ref_name = $this.data().refName;
		var ref_name2 = $this.data().refName2;

//		$form.find('#status').text('업로드 중 입니다.').show();
		
        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: CTX_ROOT+'/upload2?name='+$this.attr('name'),
            data: new FormData($form[0]),
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
            	
            	if (data.files.length > 0) {
                	var filename = decodeURIComponent(data.files[0].name);
                	var filepath = data.files[0].url;
                	var filesize = data.files[0].size;
                	
                	$('[name="'+ref_name+'"]').val(filename);
                	$('[name="'+ref_name2+'"]').val(filepath);
            	}
            	else {
            		alert('첨부할 수 없는 파일형식입니다.');
            		$this.siblings(':text').val('');
            	}
            	
//            	$form.find('#status').text('업로드되었습니다.');
            },
            error: function (e) {
                alert(e.responseText);
            }
        });
	});

	$('#remove_file1').click(function () {
		var $this = $(this);
		var ref_name = $this.data().refName;
		var data = $this.data();

		if (confirm('삭제하시겠습니까?')) {
			$($('input[name=file1]')).val('');
			
			$('[name="'+$this.data('refName')+'"]').val('');
			$('[name="'+$this.data('refName2')+'"]').val('');
		}
	});
	
	$('#remove_file2').click(function () {
		var $this = $(this);
			if (confirm('삭제하시겠습니까?')) {
			$($('input[name=file2]')).val('');
			$($('input[name=planFileName]')).val('');
			$('[name="'+$this.data('refName')+'"]').val('');
			$('[name="'+$this.data('refName2')+'"]').val('');
		}
	});
/*	$('#count input:text').keyup(function () {
		var $this = $(this);
		var id =  $this.attr('id').replace(/[^0-9]/g,"");
	
		
		if (!!Number($this.val())) {
			$this.val(Number($this.val()));	
		}
		else {
			$this.val('0');
		}
		
		setSum('inWorkNum');
		setSum('outWorkNum');
		setSum2('workSum'+id);
	});
	
	$('#amt input:text').keyup(function () {
		var $this = $(this);
		var id =  $this.attr('id').replace(/[^0-9]/g,"");
	
		if (!!Number($this.val())) {
			$this.val(Number($this.val()));	
		}
		else {
			$this.val('0');
		}
		
		setSum('inRental');
		setSum('outRental');
		
		setSum2('rentalSum'+id);
		
		setSum('inChrg');
		setSum('outChrg');
		
		setSum2('chrgSum'+id);
		
		setSum2('inChrgRate');
		setSum2('outChrgRate');
		
		setSum2('avgChrgRate'+id);
	});
	
	$('#royalty input:text').keyup(function () {
		var $this = $(this);
		var id2 =  $this.attr('id');
		var id = id2.replace(/[^0-9]/g,"");
		
	
		if (!!Number($this.val())) {
			$this.val(Number($this.val()));	
		}
		else {
			$this.val('0');
		}

		setSum('contract');
		setSum('inCollected');
		setSum('outCollected');
    
		setSum2('collectedSum'+id);
		
		setSum('inDividend');
		setSum('outDividend');
		
		setSum2('dividendSum'+id);
		
		setSum('inDividendOff');
		setSum('outDividendOff');
		
		setSum2('dividendOffSum'+id);
		
		setSum('inChrg');
		setSum('outChrg');
		
		setSum2('chrgSum'+id);
		
		setSum2('inChrgRate');
		setSum2('outChrgRate');
		
		setSum2('chrgRateSum'+id); 
	});
	*/
/*
var setSum2 = function (name) {
		
		var id =  name.replace(/[^0-9]/g,"");
		
		var sum = 0;
		if(name.indexOf("collectedSum")!=-1){
			
			sum =Number($('#inCollected'+id).val())+Number($('#outCollected'+id).val()) 
			sum2 =Number($('#inCollected').val())+Number($('#outCollected').val())
			$('#'+name).val(sum);
			$('#collectedSum').val(sum2);
		}
		if(name.indexOf("dividendSum")!=-1){

			sum =Number($('#outDividend'+id).val())+Number($('#inDividend'+id).val()) 
			sum2 =Number($('#outDividend').val())+Number($('#inDividend').val())
			$('#'+name).val(sum);
			$('#dividendSum').val(sum2);
		}
		if(name.indexOf("dividendOffSum")!=-1){

			sum =Number($('#outDividendOff'+id).val())+Number($('#inDividendOff'+id).val()) 
			sum2 =Number($('#outDividendOff').val())+Number($('#inDividendOff').val())
			$('#'+name).val(sum);
			$('#dividendOffSum').val(sum2);
		}
		if(name.indexOf("chrgSum")!=-1){

			sum =Number($('#inChrg'+id).val())+Number($('#outChrg'+id).val()) 
			sum2 =Number($('#inChrg').val())+Number($('#outChrg').val())
			$('#'+name).val(sum);
			$('#chrgSum').val(sum2);
		}
		if(name.indexOf("chrgRateSum")!=-1){

			sum =Number($('#inChrgRate'+id).val())+Number($('#outChrgRate'+id).val()) 
			sum2 =Number($('#inChrgRate').val())+Number($('#outChrgRate').val())
			$('#'+name).val(sum);
			$('#chrgRateSum').val(sum2);
			
		}	
		if(name.indexOf("workSum")!=-1){
			
			sum =Number($('#inWorkNum'+id).val())+Number($('#outWorkNum'+id).val()) 
			sum2 =Number($('#inWorkNum').val())+Number($('#outWorkNum').val())
			$('#'+name).val(sum);
			$('#workSum').val(sum2);
			
		}
		if(name.indexOf("rentalSum")!=-1){
		
			sum =Number($('#inRental'+id).val())+Number($('#outRental'+id).val()) 
			sum2 =Number($('#inRental').val())+Number($('#outRental').val())
			
			$('#'+name).val(sum);
			$('#rentalSum').val(sum2);
			
		}
		if(name.indexOf("chrgSum")!=-1){

			sum =Number($('#inChrg'+id).val())+Number($('#outChrg'+id).val()) 
			sum2 =Number($('#inChrg').val())+Number($('#outChrg').val())
			$('#'+name).val(sum);
			$('#chrgSum').val(sum2);
			
		}
		if(name.indexOf("avgChrgRate")!=-1){

			sum =Number($('#inChrgRate'+id).val())+Number($('#outChrgRate'+id).val()) 
			sum2 =Number($('#inChrgRate').val())+Number($('#outChrgRate').val())
			$('#'+name).val(sum);
			$('#avgChrgRate').val(sum2);
			
		}
		

	}*/
    var doubleSubmitFlag = false;
    function doubleSubmitCheck(){
        if(doubleSubmitFlag){
            return doubleSubmitFlag;
        }else{
            doubleSubmitFlag = true;
            return false;
        }
    }
	
$('#downFile').click(function () {
		
		var $this= $(this);
		var $form = $this.closest('form');
		var data = $this.data();
		var dataform = $this.gercd;
		//alert($('#gercd').val());
		var gercd = $('#gercdDown').val();
		//alert(gercd);
		if(gercd ==null || gercd==undefined || gercd ==""){
			alert("저작물 종류를 선택해주세요.");
			 $('#gercdDown').focus();
			 return;
		}

        location.href = CTX_ROOT+'/download3?gercd='+gercd;
	});
function isEmail(asValue) {

	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	return regExp.test(asValue); // 형식에 맞는 경우 true 리턴	

}
	
/* 파일첨부 */
$('#upLoadFile').click(function () {

	var $this= $(this);
	var $form = $this.closest('form');
	var data = $this.data();
	var dataform = $this.gercd;

	var form = $('#formUpload')[0];

	var data = new FormData(form);
	var file = $('input[name=addFile2]').val();
	
	//alert($('#gercd').val());
	var gercd = $('#gercd').val();
	var REPT_CHRR_NAME = $('#REPT_CHRR_NAME').val();
	var REPT_CHRR_POSI = $('#REPT_CHRR_POSI').val();
	var REPT_CHRR_TELX = $('#REPT_CHRR_TELX').val();
	var REPT_CHRR_MAIL = $('#REPT_CHRR_MAIL').val();

	if(REPT_CHRR_NAME ==null || REPT_CHRR_NAME==undefined || REPT_CHRR_NAME ==""){
		alert("담당자를 작성해주세요.");
		 $('#REPT_CHRR_NAME').focus();
		 return;
	}
	if(REPT_CHRR_POSI ==null || REPT_CHRR_POSI==undefined || REPT_CHRR_POSI ==""){
		alert("담당자 직위를 작성해주세요.");
		 $('#REPT_CHRR_POSI').focus();
		 return;
	}
	if(REPT_CHRR_TELX ==null || REPT_CHRR_TELX==undefined || REPT_CHRR_TELX ==""){
		alert("담당자 전화번호를 작성해주세요.");
		 $('#REPT_CHRR_TELX').focus();
		 return;
	}
	if(REPT_CHRR_MAIL ==null || REPT_CHRR_MAIL==undefined || REPT_CHRR_MAIL ==""){
		alert("담당자 이메일을 작성해주세요.");
		 $('#REPT_CHRR_MAIL').focus();
		 return;
	}
	if(gercd ==null || gercd==undefined || gercd ==""){
		alert("저작물 종류를 선택해주세요.");
		 $('#gercd').focus();
		 return;
	}
	if(!isEmail(REPT_CHRR_MAIL)){
		alert("이메일 형식에 맞게 적어주세요.");
		 return;
	}

	if(file ==null || file==undefined || file ==""){
		alert("파일을 선택해주세요.");
		 $('#file').focus();
		 return;
	}
	if(doubleSubmitCheck()) return;
	if (!confirm('대용량 업로드시 10~15분 정도 시간이 소요됩니다. \n완료시까지 페이지를 유지해주세요.')) {
		return false;
	}
//	$form.find('#status').text('업로드 중 입니다.').show();

	$('#Progress_Loading').attr('style', "display:show; position: absolute; left: 40%;	top: 38%;");
	$('#modalBack').attr('style', "display:show;");
    $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: CTX_ROOT+'/upload4',
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 7200000,
        success: function (data) {
        	
        if(data.success == true){
        	$('#Progress_Loading').attr('style', "display:none;");
        	$('#modalBack').attr('style', "display:none;");
        	 alert(data.errorMessage);
        	 location.reload();
        }else if (data.success==false){
          	$('#Progress_Loading').attr('style', "display:none;");
        	$('#modalBack').attr('style', "display:none;"); 
        	/*alert("이미 업로드한 데이터 또는 장르가 다른 데이터입니다.  \n수정시에는 일괄수정을 사용해주세요.\n( 금월내 동일종류 보고이력 존재 )");*/
        	 alert(data.errorMessage);
        	 location.reload();
        }

        	
//        	$form.find('#status').text('업로드되었습니다.');
        },
        error: function (e) {
        
            alert(e.responseText);
        }
      
    });
});




/* 파일첨부 */
$('#upLoadFile2').click(function () {

	var $this= $(this);
	var $form = $this.closest('form');
	var data = $this.data();
	var dataform = $this.gercd;

	var form = $('#formUpload')[0];

	var data = new FormData(form);
	var file = $('input[name=addFile2]').val();
	
	//alert($('#gercd').val());
	var gercd = $('#gercd').val();
	var REPT_CHRR_NAME = $('#REPT_CHRR_NAME').val();
	var REPT_CHRR_POSI = $('#REPT_CHRR_POSI').val();
	var REPT_CHRR_TELX = $('#REPT_CHRR_TELX').val();
	var REPT_CHRR_MAIL = $('#REPT_CHRR_MAIL').val();

	if(REPT_CHRR_NAME ==null || REPT_CHRR_NAME==undefined || REPT_CHRR_NAME ==""){
		alert("담당자를 작성해주세요.");
		 $('#REPT_CHRR_NAME').focus();
		 return;
	}
	if(REPT_CHRR_POSI ==null || REPT_CHRR_POSI==undefined || REPT_CHRR_POSI ==""){
		alert("담당자 직위를 작성해주세요.");
		 $('#REPT_CHRR_POSI').focus();
		 return;
	}
	if(REPT_CHRR_TELX ==null || REPT_CHRR_TELX==undefined || REPT_CHRR_TELX ==""){
		alert("담당자 전화번호를 작성해주세요.");
		 $('#REPT_CHRR_TELX').focus();
		 return;
	}
	if(REPT_CHRR_MAIL ==null || REPT_CHRR_MAIL==undefined || REPT_CHRR_MAIL ==""){
		alert("담당자 이메일을 작성해주세요.");
		 $('#REPT_CHRR_MAIL').focus();
		 return;
	}
	if(gercd ==null || gercd==undefined || gercd ==""){
		alert("저작물 종류를 선택해주세요.");
		 $('#gercd').focus();
		 return;
	}
	if(!isEmail(REPT_CHRR_MAIL)){
		alert("이메일 형식에 맞게 적어주세요.");
		 return;
	}

	if(file ==null || file==undefined || file ==""){
		alert("파일을 선택해주세요.");
		 $('#file').focus();
		 return;
	}
	if(doubleSubmitCheck()) return;
	if (!confirm('수정시 해당월의 데이터가 초기화되고 다시 업로드됩니다. \n 대량업로드시 10~15분 시간이 소요됩니다 페이지를 유지해주세요.')) {
		return false;
	}
//	$form.find('#status').text('업로드 중 입니다.').show();
	$('#Progress_Loading').attr('style', "display:show; position: absolute; left: 40%;	top: 38%;");
	$('#modalBack').attr('style', "display:show;");
	
    $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: CTX_ROOT+'/upload5',
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 7200000,
        success: function (data) {
        	
        if(data.success == true){
          	$('#Progress_Loading').attr('style', "display:none;");
        	$('#modalBack').attr('style', "display:none;");
        	 alert(data.errorMessage);
        	 location.reload();
        }else if (data.success==false){
          	$('#Progress_Loading').attr('style', "display:none;");
        	$('#modalBack').attr('style', "display:none;"); 
        	/*alert("이미 업로드한 데이터 또는 장르가 다른 데이터입니다.  \n수정시에는 일괄수정을 사용해주세요.\n( 금월내 동일종류 보고이력 존재 )");*/
        	 alert(data.errorMessage);
        	 location.reload();
        }

        	
//        	$form.find('#status').text('업로드되었습니다.');
        },
        error: function (e) {
        
            alert(e.responseText);
        }
      
    });
});



	$('#count input:text').keyup(function () {
		var $this = $(this);
		var id =  $this.attr('id').replace(/[^0-9]/g,"");
	
		
		if (!!Number($this.val())) {
			$this.val(Number($this.val()));	
		}
		else {
			$this.val('0');
		}
		
		setSum('inWorkNum');
		setSum('outWorkNum');
		setSum2('workSum'+id);
	});
	
	$('#amt input:text').keyup(function () {
		var $this = $(this);
		var id =  $this.attr('id').replace(/[^0-9]/g,"");
	
		if (!!Number($this.val())) {
			$this.val(Number($this.val()));	
		}
		else {
			$this.val('0');
		}
		setSum('inRental');
		setSum('outRental');
		
		setSum2('rentalSum'+id);
		
		setSum('inChrg');
		setSum('outChrg');
		
		setSum2('chrgSum'+id);
		
		setSum2('inChrgRate'+id);
		setSum2('outChrgRate'+id);
		
		setSum2('avgChrgRate'+id);
	});
	
	$('#royalty input:text').keyup(function () {
		var $this = $(this);
		var id2 =  $this.attr('id');
		var id = id2.replace(/[^0-9]/g,"");
		
	
		if (!!Number($this.val())) {
			$this.val(Number($this.val()));	
		}
		else {
			$this.val('0');
		}

		setSum('contract');
		setSum('inCollected');
		setSum('outCollected');
    
		setSum2('collectedSum'+id);
		
		setSum('inDividend');
		setSum('outDividend');
		
		setSum2('dividendSum'+id);
		
		setSum('inDividendOff');
		setSum('outDividendOff');
		
		setSum2('dividendOffSum'+id);
		
		setSum('inChrg');
		setSum('outChrg');
		
		setSum2('chrgSum'+id);
		
		setSum2('inChrgRate');
		setSum2('outChrgRate');
		
		setSum2('chrgRateSum'+id); 
	});
	
	var setSum3 = function (name) {
		var id =  name.replace(/[^0-9]/g,"");
		var sum = 0;
		
		$('[name="'+name+'"]').each(function (key, val) {
			sum += Number($(val).val());
			
		});
		
		$('#'+name).val(sum);
	}
	
	var setSum2 = function (name) {
			
			var id =  name.replace(/[^0-9]/g,"");
			
			var sum = 0;
			if(name.indexOf("inChrgRate")!=-1){
				if($('#inChrg'+id).val()!=0 && $('#inRental'+id).val()!=0){
				var test = (Number($('#inChrg'+id).val()));
				var test2 = (Number($('#inRental'+id).val()));
				//alert(test);
				//alert(test2);
				/*sum =(Number($('#inChrg'+id).val())/Number($('#inRental'+id).val()))*100;*/ 
				sum =(test/test2)*100;
				//alert(sum);
				sum2 =Math.round(sum*1000)/1000;
				//alert(sum2);
				$('#'+name).val(sum2);
				}
				if($('#inChrg'+id).val()==0 || $('#inRental'+id).val()==0){
	     			$('#'+name).val('0');
				}
				
			}
			if(name.indexOf("outChrgRate")!=-1){
				if($('#outChrg'+id).val()!=0 && $('#outRental'+id).val()!=0){
				sum =(Number($('#outChrg'+id).val())/Number($('#outRental'+id).val()))*100; 
				sum2 =Math.round(sum*1000)/1000;
				$('#'+name).val(sum2);
				}
				if($('#outChrg'+id).val()==0 || $('#outRental'+id).val()==0){
	     			$('#'+name).val('0');
				}
				
			}
			if(name.indexOf("collectedSum")!=-1){

				sum =Number($('#inCollected'+id).val())+Number($('#outCollected'+id).val()) 
				sum2 =Number($('#inCollected').val())+Number($('#outCollected').val())
				$('#'+name).val(sum);
				$('#collectedSum').val(sum2);
			}
			if(name.indexOf("dividendSum")!=-1){

				sum =Number($('#outDividend'+id).val())+Number($('#inDividend'+id).val()) 
				sum2 =Number($('#outDividend').val())+Number($('#inDividend').val())
				$('#'+name).val(sum);
				$('#dividendSum').val(sum2);
			}
			if(name.indexOf("dividendOffSum")!=-1){

				sum =Number($('#outDividendOff'+id).val())+Number($('#inDividendOff'+id).val()) 
				sum2 =Number($('#outDividendOff').val())+Number($('#inDividendOff').val())
				$('#'+name).val(sum);
				$('#dividendOffSum').val(sum2);
			}
			if(name.indexOf("chrgSum")!=-1){

				sum =Number($('#inChrg'+id).val())+Number($('#outChrg'+id).val()) 
				sum2 =Number($('#inChrg').val())+Number($('#outChrg').val())
				$('#'+name).val(sum);
				$('#chrgSum').val(sum2);
			}
			if(name.indexOf("chrgRateSum")!=-1){

				sum =Number($('#inChrgRate'+id).val())+Number($('#outChrgRate'+id).val()) 
				sum2 =Number($('#inChrgRate').val())+Number($('#outChrgRate').val())
				$('#'+name).val(sum);
				$('#chrgRateSum').val(sum2);
				
			}	
			if(name.indexOf("workSum")!=-1){
				
				sum =Number($('#inWorkNum'+id).val())+Number($('#outWorkNum'+id).val()) 
				sum2 =Number($('#inWorkNum').val())+Number($('#outWorkNum').val())
				$('#'+name).val(sum);
				$('#workSum').val(sum2);
				
			}
			if(name.indexOf("rentalSum")!=-1){
			
				sum =Number($('#inRental'+id).val())+Number($('#outRental'+id).val()) 
				sum2 =Number($('#inRental').val())+Number($('#outRental').val())
				
				$('#'+name).val(sum);
				$('#rentalSum').val(sum2);
				
			}
			if(name.indexOf("chrgSum")!=-1){

				sum =Number($('#inChrg'+id).val())+Number($('#outChrg'+id).val()) 
				sum2 =Number($('#inChrg').val())+Number($('#outChrg').val())
				$('#'+name).val(sum);
				$('#chrgSum').val(sum2);
				
			}
			if(name.indexOf("avgChrgRate")!=-1){
				if($('#chrgSum'+id).val()!=0 && $('#rentalSum'+id).val()!=0){
				sum =(Number($('#chrgSum'+id).val())/Number($('#rentalSum'+id).val()))*100
				sum2 =Math.round(sum*1000)/1000;
				
				$('#'+name).val(sum2);
				}
				if($('#chrgSum'+id).val()==0  || $('#rentalSum'+id).val()==0){
		
					$('#'+name).val('0');
					}
				
				
			}
			

		}

	
	$('#save').click(function () {
		var $form = $('#form');

		event.preventDefault();
		
			$form.submit();
	
	});

});
