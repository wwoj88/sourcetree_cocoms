$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// EDIT FORM
	  $('#mypage_user').click(function (event) {
	
	    var $form = $("#form_0");
	    var params = formToJson($form);
	    if (!$.trim(params['pwd'])) {
			alert('비밀번호를 입력해 주세요.');
			return false;
		}
	    event.preventDefault();
    	$form.submit();
	    });
	/* write form - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			var check = /^(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?=.*[0-9]).{8,20}$/;
			
			if (!$.trim(params['pwd'])) {
				alert('현재 비밀번호를 입력해 주세요.');
				return false;
			}
			
			if (!$.trim(params['newPwd'])
					|| !$.trim(params['newPwd2'])) {
				alert('새 비밀번호를 입력해 주세요.');
				return false;
			}
			if (params['newPwd'] != params['newPwd2']) {
				alert('새 비밀번호가 일치하지 않습니다.');
				return false;
			}
			if (params['newPwd'].length < 8) {
				alert('새 비밀번호가 너무 짧습니다.\n(8자이상 입력해주세요.)');
				return false;
			}
			if (!check.test(params['newPwd'])) {
				alert("영문, 숫자, 특수문자의 조합으로 입력해주세요.");
				return false;
			}
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
    $('#submit').click(function (event) {
    	var $this = $(this);
		var $form = $this.closest('form');

    	event.preventDefault();
    	
    	$form.submit();
    });
	
	
    $('#remove').click(function (event) {
    	var $this = $(this);

    	event.preventDefault();
    	
    	//var loginId = $('form [name="loginId"]').val();
    	//var memberSeqNo = $('form [name="memberSeqNo"]').val();
    	
    	var $input = $('<input type="hidden" name="_method" value="DELETE" />');
    	//var $input2 = $('<input type="hidden" name="loginId" />').val(loginId);
    	//var $input3 = $('<input type="hidden" name="memberSeqNo" />').val(memberSeqNo);

    	var $form = $('<form method="POST" />').attr('action', CTX_ROOT+'/mypage');
    	//$form.append($input).append($input2).append($input3);
    	$form.append($input);
    	if (confirm('탈퇴하시겠습니까?')) {

        	$('body').append($form);
    		$form.submit();
    	}
    });

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// DOC
	
	$('#print').click(function (event) {
		
		event.preventDefault();
		
		var rpx = $('#rpx').val();
		var xmldata = $('#xmldata').text();

		printReport(rpx, xmldata);
		
		// save history
		var appl_content = $('#appl_content').html();
		var doc_type = $('#appl_content').data('docType');
		
		var $form = $('<form id="form" style="display: none;">');
		var $input1 = $('<input type="hidden" name="docType" />').val(doc_type);
		var $input2 = $('<input type="hidden" name="applContent" />').val(appl_content);
		var $input3 = $('<input type="hidden" name="pdfContent" />').val(xmldata);
		
		$('body').append($form);
		$form.append($input1).append($input2).append($input3);
		
		executeCreate('/appl_histories', $form, function (data) {
		});
	});
	
	$('#print_cert').click(function (event) {
		
		event.preventDefault();
		
		var rpx = $('#rpx').val();
		var xmldata = $('#xmldata').text();

		printReport(rpx, xmldata, 'cert');
		
		// save history
		var appl_content = $('#appl_content').html();
		var doc_type = $('#appl_content').data('docType');
		
		var $form = $('<form id="form" style="display: none;">');
		var $input1 = $('<input type="hidden" name="docType" />').val(doc_type);
		var $input2 = $('<input type="hidden" name="applContent" />').val(appl_content);
		var $input3 = $('<input type="hidden" name="pdfContent" />').val(xmldata);
		
		$('body').append($form);
		$form.append($input1).append($input2).append($input3);
		
		executeCreate('/appl_histories', $form, function (data) {
		});
	});
    
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// QNA

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

	$('#checkAll').click(function () {
		var $this = $(this);
		
		if ($this.prop('checked')) {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', true);
		}
		else {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', false);
		}
	});
	
    $('#remove_checked').click(function (event) {
    	var $this = $(this);
    	var $form = $this.closest('form');

    	event.preventDefault();
    	
    	if ($form.find('[name="boardSeqNoList"]:checked').length == 0) {
    		alert('삭제 대상을 선택해주세요.');
    		return;
    	}
    	
    	var $input = $('<input type="hidden" name="_method" value="DELETE" />');
    	
    	if (confirm('삭제하시겠습니까?')) {
    		
        	$form.attr('action', CTX_ROOT+'/qna/checked').attr('method', 'POST');
        	$form.append($input);
        	
    		$form.submit();
    	}
    });
	
    $('#remove2').click(function (event) {
    	var $this = $(this);

    	event.preventDefault();
    	
    	var $form = $('<form method="POST" />').attr('action', '');
    	
    	var $input = $('<input type="hidden" name="_method" value="DELETE" />');
    	
    	$('body').append($form);
    	
    	$form.append($input);
    	
    	if (confirm('삭제하시겠습니까?')) {
    		
    		$form.submit();
    	}
    });

    
    
//    var init = function () {
//		$('.event-date').datepicker({
//			dateFormat: 'yy-mm-dd',
//			numberOfMonths: 1,
//			showOn: 'button',
//			buttonImage: CTX_ROOT+'img/date-icon.png' 
//		});  
//		
//		$('.event-date2').datepicker({
//			dateFormat: 'yy-mm-dd',
//			numberOfMonths: 1,
//			showOn: 'button',
//			buttonImage: CTX_ROOT+'img/date-icon.png' 
//		});  
//		
//		$('.ui-datepicker-trigger').attr('style', 'margin-left: 5px; vertical-align: middle; cursor: pointer; border: 0');
//    }();

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// APPL HISTORY

    $('[name="showPopup"]').click(function (event) {
    	var $this = $(this);
    	var data = $this.data();
    	
		event.preventDefault();

		var url = CTX_ROOT+'/history/'+data.applSeqNo;
		window.open(url, 'appl_popup', 'toolbar=no, width=900, height=700, status=no');
    });
    
});
