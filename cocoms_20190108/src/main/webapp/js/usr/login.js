$(function () {

	/*$('#save_id').click(function () {
		
		// save cookie
    	if (!$('#save_id').prop('checked')) {
    		
    		setCookie('checked', false);
    		setCookie('loginId', '');
    	}else{
    		setCookie('loginId')
    	}
	});*/
	
	/* login - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();

		// validation form
		var validForm = function (params) {
			
			
			if (!params['loginId']) {
				alert('아이디를 입력해 주세요.');
				return false;
			}
			
			if (!params['pwd']) {
				alert('비밀번호를 입력해 주세요.');
				return false;
			}
			
			
			return true;
		}
		
		// 인증서 제출
		var openCert = function (callback) {
			
			uniSignModule.open({'callback': function (signObj) {
				
				if (!!callback) callback(signObj);
			}});
		}
		
		// 아이디 패스워드 검증
		var checkLogin = function (params, callback, errCallback) {
			
			executeTransaction('POST', '/login/'+params.loginId, params, function (data) {
				
				if (!!callback) callback();	
				
			}, function (data) {
				
				if (!!errCallback) errCallback(data);
			});
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}

		// 아이디 패스워드 검증
		checkLogin(params, function () {

			// save cookie
	    	if ($('#save_id').prop('checked')) {
	    		
	    		setCookie('checked', true, 7);
	    		
	    		var loginId = $form.find('[name="loginId"]').val();
	    		setCookie('loginId', loginId, 7);
	    	}
	    	
			// 인증서 제출
			openCert(function (signObj) {
				
				$form.find('[name="userDn"]').val(signObj.dn);
				$form.find('[name="certSn"]').val(signObj.sn);
				
				// form submit
				$form.unbind().submit();
			});
			
		}, function (data) {
			
			$('#message').text(data.errorMessage);
			$("input[name='pwd']").val('');
			$("input[name='loginId']").val('');
		})
	});

	/* step 02 - all check */
	$('#agree4', '#form2').click(function () {
		var $this = $(this);
		var $form = $('#form2');
		
		if ($this.prop('checked')) {
			$form.find('#agree1,#agree2,#agree3').prop('checked', true);
		}
	});
	
	/* step 02 - submit */
	$('#form2').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['agree1'] || params['agree1'] != 'Y') {
				alert('서비스 약관을 동의해 주세요.');
				return false;
			}
			if (!params['agree2'] || params['agree2'] != 'Y') {
				alert('개인정보 수집 및 이용에 동의해 주세요.”');
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
		
		// form submit
		$form.unbind().submit();
	});
	
	/* step 04 - login id */
	$('[name="loginId"]', '#form4').change(function () {
		var $form = $('#form4');

		$form.find('#checkId').data('check', false);
	});
	
	/* step 04 - check id */
	var checking = false;
	$('#checkId', '#form4').click(function () {
		var $this = $(this);
		var $form = $('#form4');
		
		var loginId = $form.find('[name="loginId"]').val();
		
		if (!loginId 
				|| checking) {
			return false;
		}
		
		var params =  {};
		checking = true;
		executeTransaction('GET', '/user/'+loginId, params, function (data) {
			if (!data.exists) {
				alert('해당아이디는 사용가능합니다.');
				$this.data('check', true);
			}
			else {
				alert('아이디가 이미 사용중 이거나 탈퇴한 아이디입니다.\n다른 아이디를 입력해주세요.');
				$this.data('check', false);
			}
			checking = false;
		});
	});
	
	/* step 04 - submit */
	$('#form4').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function ($form, params) {
			var check = /^(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?=.*[0-9]).{8,20}$/;
//			if (!$.trim(params['ceoName'])) {
//				alert('이름을 입력해 주세요.');
//				return false;
//			}
			
			if (!$.trim(params['loginId'])) {
				alert('아이디를 입력해 주세요.');
				return false;
			}
			if (!params['loginId'].match(/^[a-zA-Z][0-9a-zA-Z-_]{3,50}$/)) {
				alert('아이디 형식에 맞지 않습니다.\n(영문자로 시작하는 4-50자의 영문자, 숫자, -, _)');
				return false;
			}	
			
			if (!$.trim(params['pwd'])
					|| !$.trim(params['pwd2'])) {
				alert('비밀번호를 입력해 주세요.');
				return false;
			}
			if (params['pwd'] != params['pwd2']) {
				alert('비밀번호가 일치하지 않습니다.');
				return false;
			}
			if (params['pwd'].length < 8) {
				alert('비밀번호가 너무 짧습니다.\n(8자이상 입력해주세요.)');
				return false;
			}
			if (!check.test(params['pwd'])) {
				alert("비밀번호는 영문, 숫자, 특수문자의 조합으로 입력해주세요.");
				return false;
			}

			if (!$.trim(params['ceoMobile2'])
					|| !$.trim(params['ceoMobile3'])) {
				alert('휴대전화번호를 입력해 주세요.');
				return false;
			}
			if (!params['ceoMobile2'].match(/^[0-9]+$/)
					|| !params['ceoMobile3'].match(/^[0-9]+$/)) {
				alert('휴대전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
				return false;
			}

			if (!$.trim(params['ceoEmail1'])
					|| !$.trim(params['ceoEmail2'])) {
				alert('이메일을 입력해 주세요.');
				return false;
			}
			if (!(params['ceoEmail1']+'@'+params['ceoEmail2']).match(/[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,3}$/)) {
				alert('이메일 형식에 맞지 않습니다.');
				return false;
			}	

			$form.find('[name="ceoMobile"]').val(params['ceoMobile1']+'-'+params['ceoMobile2']+'-'+params['ceoMobile3']);
			$form.find('[name="ceoEmail"]').val(params['ceoEmail1']+'@'+params['ceoEmail2']);

			return true;
		}
		
		// validation form
		if (!validForm($form, params)) {
			return false;
		}
		
		if (!$form.find('#checkId').data('check')) {
			alert('아이디 중복확인을 해주십시요.');
			return false;
		}
		
		// form submit
		$form.unbind().submit();
	});

	
	
	$('[name="coGubunCd"]').click(function () {
		var $this = $(this);
		
		if ($this.val() == '1') {
			$('#ceoRegNo').show();
			$('#coNo').hide();
		}
		else {
			$('#ceoRegNo').hide();
			$('#coNo').show();
		}
	});
	
	
	/* find id */
	$('#formFindId').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (params['coGubunCd'] == '1') {
				
				if (!params['ceoRegNo1']
						|| !params['ceoRegNo2']) {
					alert('주민등록번호를 입력해 주세요.');
					return false;
				}
				if (!params['ceoRegNo1'].match(/^[0-9]{6}$/)
						|| !params['ceoRegNo2'].match(/^[0-9]{7}$/)) {
					alert('주민등록번호 형식에 맞지 않습니다.)');
					return false;
				}				
			}
			
			else {
				
				if (!params['coNo1'] 
						|| !params['coNo2'] 
						|| !params['coNo3']) {
					alert('사업자등록번호를 입력해 주세요.');
					return false;
				}
				if (!params['coNo1'].match(/^[0-9]{3}$/)
						|| !params['coNo2'].match(/^[0-9]{2}$/)
						|| !params['coNo3'].match(/^[0-9]{5}$/)) {
					alert('사업자번호 형식에 맞지 않습니다.');
					return false;
				}
				if (!check_busino(params['coNo1'] + params['coNo2'] + params['coNo3'] + '')) {
					alert('사업자번호가 유효하지않습니다.');
					return false;
				}				
			}
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
		
		var regNo = (params['coGubunCd'] == '1') ? params['ceoRegNo1']+params['ceoRegNo2'] :
				params['coNo1']+params['coNo2']+params['coNo3'];
		
		// open cert
		verifyCert(regNo, function (certAttrs) {
			
			$form.find('[name="certSn"]').val(certAttrs.serialNumber);
			
			// form submit
			$form.unbind().submit();
		});
	});

	/* find password */
	$('#formFindPassword').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['loginId'] ) {
				alert('아이디를 입력해 주세요.');
				return false;
			}
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}

		// 인증서 제출
		var openCert = function (callback) {
			
			uniSignModule.open({'callback': function (signObj) {
				
				if (!!callback) callback(signObj);
			}});
		}

		// 인증서 제출
		openCert(function (signObj) {
			
			$form.find('[name="certSn"]').val(signObj.sn);
			
			// form submit
			$form.unbind().submit();
		});
	});
	
	/* change password */
	$('#formChangePassword').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			var check = /^(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?=.*[0-9]).{8,20}$/;
			if (!params['pwd']
					|| !params['pwd2']) {
				alert('비밀번호를 입력해 주세요.');
				return false;
			}
			if (params['pwd'] != params['pwd2']) {
				alert('비밀번호가 일치하지 않습니다.');
				return false;
			}
			if (params['pwd'].length < 8) {
				alert('비밀번호가 너무 짧습니다.\n(8자이상 입력해주세요.)');
				return false;
			}
			if (!check.test(params['pwd'])) {
				alert("영문, 숫자, 특수문자의 조합으로 입력해주세요.");
				return false;
			}

			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}

		// form submit
		$form.unbind().submit();
	});
	
	/* enroll #1 */
	$('#formEnroll').on('submit', function (event) {
		
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			if(!params['loginId']){
				alert('회원 아이디를 입력해 주세요.')
				return false;
			}
			if (!params['ceoRegNo1']
					|| !params['ceoRegNo2']) {
				alert('주민등록번호를 입력해 주세요.');
				return false;
			}
			if (!params['ceoRegNo1'].match(/^[0-9]{6}$/)
					|| !params['ceoRegNo2'].match(/^[0-9]{7}$/)) {
				alert('주민등록번호 형식에 맞지 않습니다.)');
				return false;
			}				
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}

		var regNo = params['ceoRegNo1']+params['ceoRegNo2'];

		// open cert
		verifyCert(regNo, function (certAttrs, signedText) {
			
			$form.find('[name="userDn"]').val(certAttrs.subjectName);
			$form.find('[name="certSn"]').val(certAttrs.serialNumber);
			$form.find('[name="certificate"]').val(signedText);
			$form.find('[name="certStartDate"]').val(certAttrs.validateFrom2);
			$form.find('[name="certEndDate"]').val(certAttrs.validateTo2);

			// form submit
			$form.unbind().submit();
		});
	});
	
	$('#formEnroll2').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			if(!params['loginId']){
				alert('회원 아이디를 입력해 주세요.')
				return false;
			}
			if (!params['coNo1'] 
					|| !params['coNo2'] 
					|| !params['coNo3']) {
				alert('사업자등록번호를 입력해 주세요.');
				return false;
			}
			if (!params['coNo1'].match(/^[0-9]{3}$/)
					|| !params['coNo2'].match(/^[0-9]{2}$/)
					|| !params['coNo3'].match(/^[0-9]{5}$/)) {
				alert('사업자번호 형식에 맞지 않습니다.');
				return false;
			}
			if (!check_busino(params['coNo1'] + params['coNo2'] + params['coNo3'] + '')) {
				alert('사업자번호가 유효하지않습니다.');
				return false;
			}				
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}

		var regNo = params['coNo1']+params['coNo2']+params['coNo3'];

		// verify cert
		verifyCert(regNo, function (certAttrs, signedText) {
			
			$form.find('[name="userDn"]').val(certAttrs.subjectName);
			$form.find('[name="certSn"]').val(certAttrs.serialNumber);
			$form.find('[name="certificate"]').val(signedText);
			$form.find('[name="certStartDate"]').val(certAttrs.validateFrom2);
			$form.find('[name="certEndDate"]').val(certAttrs.validateTo2);
			
			// form submit
			$form.unbind().submit();
		});
	});
	
    $('#submit,#submit2').click(function (event) {
    	var $this = $(this);
		var $form = $this.closest('form');

    	event.preventDefault();
    	
    	$form.submit();
    });
    
    var init = function () {
    	
    	if (getCookie('checked') == 'true') {
    		$('#form [id="save_id"]').prop('checked', true);
    		
        	var loginId = getCookie('loginId');
        	$('#form [name="loginId"]').val(loginId);
    	}
    }();
    
});
