/*$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var validForm = function ($form) {
		
		if ($form.find('[name="appNo"]').val() != '' 
				&& !$form.find('[name="appNo"]').val().match(/^[0-9\-]+$/)) {
			alert('신고번호는 숫자 및 "-"만 입력가능합니다.');
			return false;
		}
		
		return true;
	}

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
		// validation form
		if (!validForm($form)) {
			return false;
		}
    	
//    	var url = formToUrl($form);
//    	movePage('/admin/sub/companies?'+url);
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination li a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

	$('#conDetailCd1,#conDetailCd2').click(function (event) {
		
		var conDetailCd1 = Number($('#conDetailCd1:checked').val()) || 0;
		var conDetailCd2 = Number($('#conDetailCd2:checked').val()) || 0; 
		
		var conDetailCd = conDetailCd1 + conDetailCd2;
		if (conDetailCd == 3) conDetailCd = '3';
		
		$('[name="conDetailCd"]').val(conDetailCd);
	});
	
	$('#checkAll').click(function () {
		var $this = $(this);
		
		if ($this.prop('checked')) {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', true);
		}
		else {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', false);
		}
	});
	
	$('.list_btn [id^="changeStatCd_"]').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		if ($('#data :checked').length == 0) {
			alert('처리할 데이터를 선택해 주세요');
			return;
		}
		if ($('#data :checked').length > 1) {
			alert('처리할 데이터를 한개만 선택해 주세요');
			return;
		}
		
		var memberSeqNo = $('#data :checked').val();
		var code = $this.attr('id').split('_')[1];
		var statCd = $('#data :checked').data('statCd');
		var preStatCd = $('#data :checked').data('preStatCd');
		
		// 신청 (접수)
		if (!code) {
			if (statCd == '1' || statCd == '3') {
				code = $('#data :checked').data('statCd') + '9';	
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고신청처리
		else if (code == '2') {
			if (statCd == '19' || (statCd == '8' && preStatCd == '19')) {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고취소
		else if (code == '6') {
			if (statCd == '2' || statCd == '4' || statCd == '5') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 변경신고신청처리
		else if (code == '4') {
			if (statCd == '39' || (statCd == '8' && preStatCd == '39')) {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 보완요청
		else if (code == '7') {
			if (statCd == '19' || statCd == '39' || statCd == '8') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 반려
		else if (code == '9') {
			if (statCd == '7') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고정지
		else if (code == '5') {
			if (statCd == '2' || statCd == '4') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고정지해제
		else if (code == '59') {
			if (statCd == '5') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		
		var params = {};
		params.memberSeqNo = memberSeqNo;
		params.conditionCd = $('[name="conditionCd"]').val();
		params.statCd = code;
		
		if (confirm($this.text()+' 하시겠습니까?')) {
			
			executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/status', params, function (data) {
				alert('처리되었습니다.');
				location.reload();
			});
		}
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// VIEW
	
	$('#tab1,#tab2').click(function (event) {
		var $this = $(this);
		var id = $this.attr('id');
		
		event.preventDefault();

		if (id == 'tab1') {
			$('#area1').show();
			$('#area2').hide();
		}
		else {
			$('#area1').hide();
			$('#area2').show();
		}
		
		$this.closest('li').addClass('active');
		$this.closest('li').siblings().removeClass('active');
	});
	
	$('.btn_area [id^="changeStatCd_"]').click(function (event) {
		var $this = $(this);
		var memberSeqNo = $('#memberSeqNo').val();
		var code = $this.attr('id').split('_')[1];
		
		event.preventDefault();
		
		var params = {};
		params.memberSeqNo = memberSeqNo;
		params.conditionCd = $('#conditionCd').val();
		params.statCd = code;
		
		if (confirm($this.text()+' 하시겠습니까?')) {
			
			executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/status', params, function (data) {
				location.reload();
			});
		}
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// HISTORY
	
    $('[name="openHistory"]').click(function (event) {
    	var $this = $(this);
    	var data = $this.data();
    	
		event.preventDefault();
		
		$('#rpMemo').text(data.rpMemo);
		
		$('#popupHistory').show();
    });
    
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// DOC
	
	$('#print').click(function (event) {
		
		event.preventDefault();
		
		var rpx = $('#rpx').val();
		var xmldata = $('#xmldata').text();

		printReport(rpx, xmldata);
	});

	$('#print_cert').click(function (event) {
		
		event.preventDefault();
		
		var rpx = $('#rpx').val();
		var xmldata = $('#xmldata').text();

		printReport(rpx, xmldata, 'cert');
	});
	
	$('#prev').click(function (event) {
		
		event.preventDefault();

		// move view page
//		var idx = $(location).attr('pathname').indexOf('/docs');
//		var uri = $(location).attr('pathname').substring(0, idx);
		
		// move list page
		var memberSeqNo = $('#memberSeqNo').val();
		var idx = $(location).attr('pathname').indexOf(memberSeqNo);
		var uri = $(location).attr('pathname').substring(0, idx);
		$(location).attr('href', uri);
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// REGIST
	
	$('#save').click(function (event) {
		var $form = $('#form');
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
		
		// form submit
		if (!confirm('저장하시겠습니까?')) {
			return false;
		}
		
		executeTransaction('POST', '/admin/sub/regist', params, function (data) {
			
			alert('처리되었습니다.');
			location.reload();
		});
	});
	
});
*/
$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var validForm = function ($form) {
		
		if ($form.find('[name="appNo"]').val() != '' 
				&& !$form.find('[name="appNo"]').val().match(/^[0-9\-]+$/)) {
			alert('신고번호는 숫자 및 "-"만 입력가능합니다.');
			return false;
		}
		
		return true;
	}

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
		// validation form
		if (!validForm($form)) {
			return false;
		}
    	
//    	var url = formToUrl($form);
//    	movePage('/admin/sub/companies?'+url);
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination li a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		console.log($this);
		search($this.data('page'));
	});

	$('#conDetailCd1,#conDetailCd2').click(function (event) {
		
		var conDetailCd1 = Number($('#conDetailCd1:checked').val()) || 0;
		var conDetailCd2 = Number($('#conDetailCd2:checked').val()) || 0; 
		
		var conDetailCd = conDetailCd1 + conDetailCd2;
		/*if (conDetailCd == 3) conDetailCd = '3';*/
		
		$('[name="conDetailCd"]').val(conDetailCd);
	});
	
	$('#checkAll').click(function () {
		var $this = $(this);
		
		if ($this.prop('checked')) {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', true);
		}
		else {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', false);
		}
	});
	
	$('.list_btn [id^="changeStatCd_"]').click(function (event) {
		var $this = $(this);

		event.preventDefault();
		
		if ($('#data :checked').length == 0) {
			alert('처리할 데이터를 선택해 주세요');
			return;
		}
		if ($('#data :checked').length > 1) {
			alert('처리할 데이터를 한개만 선택해 주세요');
			return;
		}
		var checkCode="";
		var memberSeqNo = $('#data :checked').val();
		var code = $this.attr('id').split('_')[1];
		var statCd = $('#data :checked').data('statCd');
		var preStatCd = $('#data :checked').data('preStatCd');
	
		// 신청 (접수)
		if (!code) {
			if (statCd == '1' || statCd == '3') {
				code = $('#data :checked').data('statCd') + '9';	
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고신청처리
		else if (code == '2') {
			if (statCd == '19' || (statCd == '8' && preStatCd == '19') || (statCd == '8' && preStatCd == '7')) {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고취소
		else if (code == '6') {
			if (statCd == '2' || statCd == '4' || statCd == '5') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 변경신고신청처리
		else if (code == '4') {
			if (statCd == '39' || (statCd == '8' && preStatCd == '39')  || (statCd == '8' && preStatCd == '7')) {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 보완요청
		else if (code == '7') {
			if (statCd == '19' || statCd == '39' || statCd == '8' || statCd == '1' || statCd=='3') {
				if( statCd == '1' || statCd=='3'){
					checkCode= statCd  + '9';	
				}else if(preStatCd =='19'|| preStatCd =='39'){
					checkCode= preStatCd.substr(0, 1) +'9';
				}							
			}
			/*
			if (statCd == '19' || statCd == '39' || statCd == '8' ) {
			}
			*/
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 반려
		else if (code == '9') {
			if (statCd == '7') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고정지
		else if (code == '5') {
			
			if (statCd == '2' || statCd == '4') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고정지해제
		else if (code == '59') {
			if (statCd == '5') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		if(code=='7' || code=='9' || code=='5' || code =='6'){
			if (!confirm($this.text()+' 하시겠습니까?')) {
				return false;
			}
			
			  /*$('#popupEnrollment').show();
			  $('#code').val(code);
			  $('#conditionCd').val($('input[name=conditionCd]').val());
			  $('#prestatCd').val(preStatCd);
			  $('#checkCode').val(checkCode);*/
				var params = {};
				params.memberSeqNo = memberSeqNo;
				params.conditionCd = $('[name="conditionCd"]').val();
				params.statCd = code;
				
			executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/status', params, function (data) {
				alert('처리되었습니다.');
				location.history(-1);
				window.history.back();
				location.href = document.referrer; 
			});
		     
	}else{
			if (confirm($this.text()+' 하시겠습니까?')) {
				var params = {};
				params.memberSeqNo = memberSeqNo;
				params.conditionCd = $('[name="conditionCd"]').val();
				params.statCd = code;
				
			executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/status', params, function (data) {
				alert('처리되었습니다.');
				location.history(-1);
				window.history.back();
				location.href = document.referrer; 
				
			});
			}
		}
		
		
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// VIEW
	
	$('#tab1,#tab2').click(function (event) {
		var $this = $(this);
		var id = $this.attr('id');
		
		event.preventDefault();

		if (id == 'tab1') {
			$('#area1').show();
			$('#area2').hide();
		}
		else {
			$('#area1').hide();
			$('#area2').show();
		}
		
		$this.closest('li').addClass('active');
		$this.closest('li').siblings().removeClass('active');
	});
	
	$('.btn_area [id^="changeStatCd_"]').click(function (event) {
		var $this = $(this);
		var memberSeqNo = $('#memberSeqNo').val();
		var code = $this.attr('id').split('_')[1];
		var checkCode="";
		
		event.preventDefault();
		
		
		var statCd = $('input[name=statCd]').val();
		var preStatCd = $('input[name=preStatCd]').val();
	
		// 신청 (접수)
		if (!code) {
			if (statCd == '1' || statCd == '3') {
				code = statCd  + '9';	
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고신청처리
		else if (code == '2') {
			
			if (statCd == '19' || (statCd == '8' && preStatCd == '19') || (statCd == '8' && preStatCd == '7')) {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고취소
		else if (code == '6') {
			if (statCd == '2' || statCd == '4' || statCd == '5') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 변경신고신청처리
		else if (code == '4') {
			if (statCd == '39' || (statCd == '8' && preStatCd == '39') || (statCd == '8' && preStatCd == '7') ) {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 보완요청
		else if (code == '7') {
		
			if (statCd == '19' || statCd == '39' || statCd == '8' || statCd == '1' || statCd=='3') {
				if( statCd == '1' || statCd=='3'){
					checkCode= statCd  + '9';	
				}else if(preStatCd =='19'|| preStatCd =='39'){
					checkCode= preStatCd.substr(0, 1) +'9';
				}					
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 반려
		else if (code == '9') {
			if (statCd == '7' || statCd=='8') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고정지
		else if (code == '5') {
			if (statCd == '2' || statCd == '4') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고정지해제
		else if (code == '59') {
			if (statCd == '5') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		var params = {};
		params.memberSeqNo = memberSeqNo;
		params.conditionCd = $('input[name=conditionCd]').val();
		params.statCd = code;
		params.prestatCd =preStatCd;
		params.checkCode=checkCode;
	
		if(code=='7' || code=='9' || code=='5' || code =='6'){
			if (!confirm($this.text()+' 하시겠습니까?')) {
				return false;
			}
			
			  $('#popupEnrollment').show();
			  $('#code').val(code);
			  $('#conditionCd').val($('input[name=conditionCd]').val());
			  $('#prestatCd').val(preStatCd);
			  $('#checkCode').val(checkCode);
			
			  
		     
		}else{
			if (confirm($this.text()+' 하시겠습니까?')) {
			
			executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/status', params, function (data) {
				alert('처리되었습니다.');
				location.history(-1);
				window.history.back();
				location.href = document.referrer; 
				
			});
			}
		}
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// HISTORY
	
    $('[name="openHistory"]').click(function (event) {
    	var $this = $(this);
    	var data = $this.data();
    	
		event.preventDefault();
		
		$('#rpMemo').text(data.rpMemo);
		
		$('#popupHistory').show();
    });
    
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// DOC
	
	$('#print').click(function (event) {
		
		event.preventDefault();
		
		var rpx = $('#rpx').val();
		var xmldata = $('#xmldata').text();

		printReport(rpx, xmldata);
	});

	$('#print_cert').click(function (event) {
		
		event.preventDefault();
		
		var rpx = $('#rpx').val();
		var xmldata = $('#xmldata').text();

		printReport(rpx, xmldata, 'cert');
	});
	
	$('#prev').click(function (event) {
		
		event.preventDefault();

		// move view page
//		var idx = $(location).attr('pathname').indexOf('/docs');
//		var uri = $(location).attr('pathname').substring(0, idx);
		
		// move list page
		var memberSeqNo = $('#memberSeqNo').val();
		var idx = $(location).attr('pathname').indexOf(memberSeqNo);
		var uri = $(location).attr('pathname').substring(0, idx);
		$(location).attr('href', uri);
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// REGIST
	
	$('#save').click(function (event) {
		var $form = $('#form');
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
		
		// form submit
		if (!confirm('저장하시겠습니까?')) {
			return false;
		}
		
		executeTransaction('POST', '/admin/sub/regist', params, function (data) {
			
			alert('처리되었습니다.');
			location.reload();
		});
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// CHARGE
	
	$('[name="cancelCharge"]').click(function (event) {
		var $this = $(this);
		
		var certInfoSeqNo = $this.data('certInfoSeqNo');
		var memberSeqNo = $this.data('memberSeqNo');
		
		// 
		if (!confirm('결제취소 하시겠습니까?')) {
			return false;
		}
		
		// get gpki data
		var getGpkiData = function (callback) {
			
			var params = {};
			params.memberSeqNo = $this.data('memberSeqNo');
			params.mallSeq = $this.data('mallSeq');
			
			params.returnUrl = $(location).attr('origin')+CTX_ROOT+'/admin/sub/companies/'+memberSeqNo+'/charges/save/'+certInfoSeqNo;	// callback url

			executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/charges/cancel', params, function (data) {
				
				if (!!callback) callback(data); 
			});
		}

		//
		getGpkiData(function (data) {
			
			window.open('about:blank', 'chargeinfo', 'toolbar=no, width=640, height=598, status=no');

			var $form = $('<form />');
			$('body').append($form);
			
			//$form.attr('action', 'http://www.minwon.go.kr/main');
			$form.attr('action', 'https://www.gov.kr/main');
			$form.attr('method', 'POST');
			$form.attr('target', 'chargeinfo');
			$form.attr('accept-charset', 'euc-kr');
			
			var $input1 = $('<input type="hidden" />').attr('name', 'a').val('CS040GPKIComCancelApp'); 
			var $input2 = $('<input type="hidden" />').attr('name', 'g4cssCK').val(data.g4cssCK); 
			
			$form.append($input1).append($input2);
			$form.submit();
		});
	});
	
	
	$('#sbpop').click(function (event) {
		var $form = $('#form');
		var params = formToJson($form);
		var date = params.regdate;
		var manager = params.accountManager;
		var contents =params.contents;
		event.preventDefault();
		
		if (date =="" || date==null) {
			alert('날짜를 선택해주세요')
			return false;
		}if (manager =="" || manager==null) {
			alert('담당자를 적어주세요.')
			return false;
		}if (contents =="" || contents==null) {
			alert('이력 내용을 기입해주세요.')
			return false;
		}
		
		if (!confirm('저장하시겠습니까?')) {
			return false;
		}
		$('#form').submit();
		
	});
	
	$('#enrollsrch').click(function (event) {
		var $form = $('#enrollmentSearchForm');
		var params = formToJson($form);
		var from = params.searchFromDate.replaceAll("-","");
		var to = params.searchToDate.replaceAll("-","");

		event.preventDefault();
		if(from>to){
			alert('시작날짜가 더 큽니다.확인해주세요.')
			return false;
		}
		$('#enrollmentSearchForm').submit();
		
		//$('#form').submit();
		
	});
	
});
