$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

	$('#checkAll').click(function () {
		var $this = $(this);
		
		if ($this.prop('checked')) {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', true);
		}
		else {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', false);
		}
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// WRITE FORM

    $('#remove').click(function (event) {
//		var $form = $('#form');

    	event.preventDefault();
    	
    	if (!confirm('삭제하시겠습니까?')) {
    		return;
    	}
    	
    	var $form = $('<form method="POST" />');
    	$('body').append($form);
    	
    	$form.append('<input type="hidden" name="_method" value="DELETE" />');
    	
    	$form.submit();
    });
    
	/* write form - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['title']) {
				alert('제목을 입력해 주세요.');
				return false;
			}

			if (!params['content']) {
				alert('내용을 입력해 주세요.');
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
    $('#submit').click(function (event) {
		var $form = $('#form');

    	event.preventDefault();
    	
    	$form.submit();
    });
    
});
