$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// WRITE FORM

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });
    
    $('#members').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
		$('#popupMembers').show();
    });

    $('#members2').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
		$('#popupMembers2').show();
    });

	$('#popupMembers #pagination2,#popupMembers2 #pagination2').on('click', 'a', function (event) {
		var $this = $(this);
		var page = $this.data('page');
		var popupId = $this.closest('.pop_common').attr('id');

		event.preventDefault();
		
		$('#pagination2').find('li').removeClass('active');
		$('#pagination2').find('a[data-page="'+page+'"]').closest('li').addClass('active');

		var $form = $('#searchForm');
		var params = formToJson($form);
		params.pageIndex = page;

		executeTransaction('GET', '/admin/sms/members/', params, function (data) {

			var $target = $('#data tbody'); 
			
			$target.html('');
			for (var i=0; i<data.list.length; i++) {
				var $td1 = $('<td />').text(data.list[i].memberSeqNo);
				var $td2 = $('<td />').text(data.list[i].coName);
				var $td3 = $('<td />').text(data.list[i].ceoName);
				var $td4 = popupId == 'popupMembers' ? $('<td />').text(data.list[i].sms) 
						: $('<td />').text(data.list[i].ceoEmail);
				
				var $tr = $('<tr />').append($td1).append($td2).append($td3).append($td4);
				$target.append($tr);
			}

			var html = '';
			var page = data.page;
			var start = data.paginationInfo.firstPageNoOnPageList;
			var end = data.paginationInfo.lastPageNoOnPageList;
			var total = data.paginationInfo.totalPageCount;
			if (start > 5) html += '<li class="direction prev"><a href="" data-page="'+(start-1)+'"><span class="blind">prev</span></a></li> ';
			else html += '<li class="direction prev"><a href="" data-page="'+(1)+'"><span class="blind">prev</span></a></li> ';
			for (var i=start; i<=end; i++) {
				if (page == i) html += '<li class="active"><a href="" data-page="'+i+'">'+i+'</a></li> ';	
				else html += '<li><a href="" data-page="'+i+'">'+i+'</a></li> ';
			}
			if (end < total) html += '<li class="direction next"><a href="" data-page="'+(end+1)+'"><span class="blind">prev</span></a></li> ';
			else html += '<li class="direction next"><a href="" data-page="'+(end)+'"><span class="blind">prev</span></a></li> ';
			$('#pagination2').html(html);
		});
	});
	
    $('[name="popup_close"]').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();

    	$this.closest('.pop_common').hide();
    });
    
	/* write form - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['content']) {
				alert('메세지를 입력해 주세요.');
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('발송하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
	/* write form - submit */
	$('#form2').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['subject']) {
				alert('제목을 입력해 주세요.');
				return false;
			}

			if (!params['content']) {
				alert('내용을 입력해 주세요.');
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('발송하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
    $('#submit').click(function (event) {
    	var $this = $(this);
    	var $form = $this.closest('form');
    	
    	event.preventDefault();
    	
    	$form.submit();
    });
    
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// SMS SETUP

	/* write form - submit */
	$('#formSms').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
    $('[name="save"]').click(function (event) {
    	var $this = $(this);
    	var $form = $this.closest('form');
    	
    	event.preventDefault();
    	
    	$form.submit();
    });
    
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// EMAIL SETUP

    $('[name="preview"]').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
		var detailCode = $this.data('detailCode');
		var subject = $('#subject_'+detailCode).val();
		var content = $('#content_'+detailCode).val();
		$('#popupPreview [class="subject"]').text(subject);
		$('#popupPreview [class="content"] pre').html(content);
		
		$('#popupForm').hide();
		$('#popupPreview').show();
    });

    $('[name="modify"]').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
		var detailCode = $this.data('detailCode');
		var subject = $('#subject_'+detailCode).val();
		var content = $('#content_'+detailCode).val();
		var useYn = $('#useYn_'+detailCode).val();
		$('#popupForm [name="detailCode"]').val(detailCode);
		$('#popupForm [name="subject"]').val(subject);
		$('#popupForm [name="content"]').text(content);
		$('#popupForm [name="useYn"][value="'+useYn+'"]').prop('checked', true);
		
		$('#popupForm').show();
    });
    
	/* write form - submit */
	$('#formEmail').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['subject']) {
				alert('제목을 입력해 주세요.');
				return false;
			}

			if (!params['content']) {
				alert('내용을 입력해 주세요.');
				return false;
			}
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		var action = $form.attr('action');
		$form.attr('action', action + '/' + params['detailCode']);
		
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// HISTORY LIST 

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

	$('#checkAll').click(function () {
		var $this = $(this);
		
		if ($this.prop('checked')) {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', true);
		}
		else {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', false);
		}
	});

    $('#remove').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
		if ($('#data :checked').length == 0) {
			alert('처리할 데이터를 선택해 주세요');
			return;
		}
		if ($('#data :checked').length > 1) {
			alert('처리할 데이터를 한개만 선택해 주세요');
			return;
		}
		
    	if (!confirm('삭제하시겠습니까?')) {
    		return;
    	}

    	var $form = $('<form method="POST" />');
    	$('body').append($form);
    	
    	$form.append('<input type="hidden" name="_method" value="DELETE" />');
    	
    	var type = $('[name="type"]').val();
    	$form.append('<input type="hidden" name="returnUrl" value="/admin/sms/histories?type='+type+'" />');
    	
    	var val = $('#data :checked').val();    	
    	$form.attr('action', CTX_ROOT+'/admin/sms/histories/'+val+'?type='+type);

    	$form.submit();
    });	
    
    $('#remove2').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
    	if (!confirm('삭제하시겠습니까?')) {
    		return;
    	}
    	
    	var $form = $('<form method="POST" />');
    	$('body').append($form);
    	
    	$form.append('<input type="hidden" name="_method" value="DELETE" />');
    	
    	$form.submit();
    });
    
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	var init = function () {
		
		$('.event-date').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: 'button',
			buttonImage: CTX_ROOT+'/img/date-icon.png' 
		});  
	
		$('.event-date').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: "button",
			buttonImage: CTX_ROOT+'/img/date-icon.png' 
		});  
		
		$('.event-date2').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: "button",
			buttonImage: CTX_ROOT+'/img/date-icon.png' 
		});   
	
		$('.ui-datepicker-trigger').attr('style', 'margin-left:5px; vertical-align:middle; cursor:pointer; border:0');
	}();
	
});
