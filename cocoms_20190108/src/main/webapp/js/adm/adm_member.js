$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

	$('#checkAll').click(function () {
		var $this = $(this);
		
		if ($this.prop('checked')) {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', true);
		}
		else {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', false);
		}
	});
	
    $('#remove').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
		if ($('#data :checked').length == 0) {
			alert('처리할 데이터를 선택해 주세요');
			return;
		}
		if ($('#data :checked').length > 1) {
			alert('처리할 데이터를 한개만 선택해 주세요');
			return;
		}
		
    	if (!confirm('삭제하시겠습니까?')) {
    		return;
    	}

    	var $form = $('<form method="POST" />');
    	$('body').append($form);
    	
    	$form.append('<input type="hidden" name="_method" value="DELETE" />');
    	
    	var coGubun = $('[name="coGubun"]').val();
    	$form.append('<input type="hidden" name="returnUrl" value="/admin/members'+coGubun+'" />');
    	
    	var loginId = $('#data :checked').val();    	
    	$form.attr('action', CTX_ROOT+'/admin/members1/'+loginId);
    	
    	$form.submit();
    });	
	
    var init = function () {
    	
		$('.event-date').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: "button",
			buttonImage: "/cocoms/img/date-icon.png" 
		});  
		
		$('.event-date2').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: "button",
			buttonImage: "/cocoms/img/date-icon.png" 
		});  
		
		$(".ui-datepicker-trigger").attr("style","margin-left:5px; vertical-align:middle; cursor:pointer; border:0");
    }();
    
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// VIEW
   
    $('#changePassword').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
		$('#popupChangePassword').show();
    });
    
    $('#sendPassword').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
		$('#popupSendPassword').show();
    });

	/* write form - submit */
	$('#formChangePassword').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);
		var check = /^(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?=.*[0-9]).{8,20}$/;
		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['pwd']
					|| !params['pwd2']) {
				alert('비밀번호를 입력해 주세요.');
				return false;
			}
			if (params['pwd'] != params['pwd2']) {
				alert('비밀번호가 일치하지 않습니다.');
				return false;
			}
			if (params['pwd'].length < 8) {
				alert('비밀번호가 너무 짧습니다.\n(8자이상 입력해주세요.)');
				return false;
			}
			if (!check.test(params['pwd'])) {
				alert("영문, 숫자, 특수문자의 조합으로 입력해주세요.");
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('변경하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
	$('#form_send').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['sendType']) {
				alert('전송방식을 선택해 주세요.');
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('발송하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
    $('#remove2').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();
		
    	if (!confirm('삭제하시겠습니까?')) {
    		return;
    	}
    	
    	var $form = $('<form method="POST" />');
    	$('body').append($form);
    	
    	$form.append('<input type="hidden" name="_method" value="DELETE" />');
    	
    	$form.submit();
    });
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// WRITE FORM

	/* write form - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['coGubunCd']) {
				alert('회원구분을 선택해 주세요.');
				return false;
			}

			if (!params['loginId']) {
				alert('아이디를 입력해 주세요.');
				return false;
			}

			if (!params['pwd']
					|| !params['pwd2']) {
				alert('비밀번호를 입력해 주세요.');
				return false;
			}
			if (params['pwd'] != params['pwd2']) {
				alert('비밀번호가 일치하지 않습니다.');
				return false;
			}
			if (params['pwd'].length < 8) {
				alert('비밀번호가 너무 짧습니다.\n(8자이상 입력해주세요.)');
				return false;
			}

			if (!params['ceoName']) {
				alert('성명을 입력해 주세요.');
				return false;
			}
			
			if (!params['delGubun']) {
				alert('상태를 선택해 주세요.');
				return false;
			}
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// EDIT FORM

	/* write form - submit */
	$('#form_edit').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
	$('#submit').click(function (event) {
		var $this = $(this);
		var $form = $this.closest('form');

    	event.preventDefault();
    	
    	$form.submit();
    });
    
});
