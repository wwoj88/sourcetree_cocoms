$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	
    	event.preventDefault();
    	var smonth =$("#smonth").val();
    	var emonth =$("#emonth").val();
    	var num1 =  parseInt(smonth);
    	var num2 = parseInt(emonth);
    	
    	if(smonth==null || smonth==""){
    		alert("시작월을 선택해주세요.");
    		return false;
    	}
    	if(emonth==null || emonth==""){
    		alert("종료월을 선택해주세요.");
    		return false;
    	}
    	if(num1>num2){
    		alert("시작월이 더 큽니다 다시확인해주세요.");
    		return false;
    	}
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

	
    var init = function () {
    	
		$('.event-date').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: "button",
			buttonImage: "/cocoms/img/date-icon.png" 
		});  
		
		$('.event-date2').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: "button",
			buttonImage: "/cocoms/img/date-icon.png" 
		});  
		
		$(".ui-datepicker-trigger").attr("style","margin-left:5px; vertical-align:middle; cursor:pointer; border:0");
    }();
    
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// VIEW
   
	
	$('#submit').click(function (event) {
		var $this = $(this);
		var $form = $this.closest('form');

    	event.preventDefault();
    	
    	$form.submit();
    });
    
});
