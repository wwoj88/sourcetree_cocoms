$(function () {

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
    	$("#searchForm").attr("action", "/cocoms/admin/status/stats")

//    	var url = formToUrl($form);
//    	movePage('/admin/sub/companies?'+url);
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});
	
	var searchZipCode = function (sido, callback) {
		
		var params = {};
		params.sido = sido;
		executeTransaction('GET', '/admin/status/zip_code', params, function (data) {
			
			if (!!callback) callback(data);
		});
	};
	
	$('[name="trustSido"]').change(function (event) {
		var $this = $(this);
		var val = $this.val();
		
		var $select = $('[name="trustGugun"]');
		$select.find('option[value!=""]').remove();

		if (!val) {
			return;
		}
		
		searchZipCode(val, function (data) {
			
			for (var i=0; i<data.list.length; i++) {
				var zipCode = data.list[i];
				var sido = decodeURIComponent(zipCode['sido']);
				var gugun = decodeURIComponent(zipCode['gugun']);
				
				var selected = gugun == $('#trustGugun').val();

				var $option = $('<option />').attr('value', gugun).text(gugun).prop('selected', selected);
				$select.append($option);
			}
		});
	});
	
	var init = function () {
		
		searchZipCode('', function (data) {
			
			var $select = $('[name="trustSido"]');
			for (var i=0; i<data.list.length; i++) {
				var zipCode = data.list[i];
				var sido = decodeURIComponent(zipCode['sido']);
				var gugun = decodeURIComponent(zipCode['gugun']);
				
				var selected = sido == $('#trustSido').val();
				
				var $option = $('<option />').attr('value', sido).text(sido).prop('selected', selected);
				$select.append($option);
			}
		});
		
		var sido = $('#trustSido').val();
		if (!!sido) {

			var $select = $('[name="trustGugun"]');
			$select.find('option[value!=""]').remove();

			searchZipCode(sido, function (data) {
				
				for (var i=0; i<data.list.length; i++) {
					var zipCode = data.list[i];
					var sido = decodeURIComponent(zipCode['sido']);
					var gugun = decodeURIComponent(zipCode['gugun']);
					
					var selected = gugun == $('#trustGugun').val();

					var $option = $('<option />').attr('value', gugun).text(gugun).prop('selected', selected);
					$select.append($option);
				}
			});
		}
		
		var setTable = function ($table, arr) {
			
			var tit = arr.pop();
			$table.find('tbody tr').each(function (key, val) {
				var $val = $(val);
				var type = $val.data('type')
				
				$val.find('td:eq(0)').text(tit);
				
				if (type == '2') {
					tit = arr.pop();
				}
			});
		}
		if ($('table#data tbody tr').length > 0) {
			var $table = $('table#data');
			var arr = ['도서관보상금', '수업목적의 복제등 보상금', '교과용 도서 보상금'];
			setTable($table, arr);
		}
		if ($('table#data2 tbody tr').length > 0) {
			var $table = $('table#data2');
			var arr = ['판매용 음반의 공연 보상금', '음반의 디음송 보상금', '판매용 음반방송 보상금'];
			setTable($table, arr);
		}
		
	}();

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// APPL HISTORY

    $('[name="showPopup"]').click(function (event) {
    	var $this = $(this);
    	var data = $this.data();
    	
		event.preventDefault();

		var url = CTX_ROOT+'/admin/status/applications/'+data.applSeqNo;
		window.open(url, 'appl_popup', 'toolbar=no, width=900, height=700, status=no');
    });
    
});
