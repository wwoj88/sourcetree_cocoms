$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

    var init = function () {
    	
		$('.event-date').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: "button",
			buttonImage: "/cocoms/img/date-icon.png" 
		});  
		
		$('.event-date2').datepicker({
			dateFormat:'yy-mm-dd',
			numberOfMonths:1,
			showOn: "button",
			buttonImage: "/cocoms/img/date-icon.png" 
		});  
		
		$(".ui-datepicker-trigger").attr("style","margin-left:5px; vertical-align:middle; cursor:pointer; border:0");
    }();
    
});
