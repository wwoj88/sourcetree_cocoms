$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

	$('#checkAll').click(function () {
		var $this = $(this);
		
		if ($this.prop('checked')) {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', true);
		}
		else {
			$('#data :checkbox[id^="checkbox_"]').prop('checked', false);
		}
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// WRITE FORM

    $('#remove').click(function (event) {
//		var $form = $('#form');

    	event.preventDefault();
    	
    	if (!confirm('삭제하시겠습니까?')) {
    		return;
    	}
    	
    	var $form = $('<form method="POST" />');
    	$('body').append($form);
    	
    	$form.append('<input type="hidden" name="_method" value="DELETE" />');
    	
    	$form.submit();
    });
    
	/* 파일첨부 */
	$(':file[name="addFile"]').change(function () {
		var $this= $(this);
		var $form = $this.closest('form');
		var data = $this.data();

//		$form.find('#status').text('업로드 중 입니다.').show();
		
        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: CTX_ROOT+'/upload2?name='+$this.attr('name'),
            data: new FormData($form[0]),
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
            
            	if (data.files.length > 0) {
                	var filename = decodeURIComponent(data.files[0].name);
                	var filepath = data.files[0].url;
                	var filesize = data.files[0].size;
                	
                	$('#addFile').data('filename', filename);
                	$('#addFile').data('filepath', filepath);
                	$('#addFile').data('filesize', filesize);
            	}
            	else {
            		alert('첨부할 수 없는 파일형식입니다.');
            		
            		$this.siblings(':text').val('');
            	}
            	
//            	$form.find('#status').text('업로드되었습니다.');
            },
            error: function (e) {
            
                alert(e.responseText);
            }
          
        });
	});
	
    var doubleSubmitFlag = false;
    function doubleSubmitCheck(){
        if(doubleSubmitFlag){
            return doubleSubmitFlag;
        }else{
            doubleSubmitFlag = true;
            return false;
        }
    }
    $('#downFile').click(function () {
		
		var $this= $(this);
		var $form = $this.closest('form');
		var data = $this.data();
		var dataform = $this.gercd;
		var gercd = $('#gercdDown').val();
	
		if(gercd ==null || gercd==undefined || gercd ==""){
			alert("저작물 종류를 선택해주세요.");
			 $('#gercdDown').focus();
			 return;
		}

        location.href = CTX_ROOT+'/download3?gercd='+gercd;
	});

    function isEmail(asValue) {

    	var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

    	return regExp.test(asValue); // 형식에 맞는 경우 true 리턴	

    }

    
$('#checkButton').click(function () {

	
		
		var $this= $(this);
		var $form = $this.closest('form');
		var data = $this.data();
		var dataform = $this.gercd;
		var form = $('#formUpload')[0];
		

		var data = new FormData(form);
		
		
		//alert($('#gercd').val());
		var gercd = 0;
		var REPT_CHRR_NAME = $('#REPT_CHRR_NAME').val();
		var REPT_CHRR_POSI = $('#REPT_CHRR_POSI').val();
		var REPT_CHRR_TELX = $('#REPT_CHRR_TELX').val();
		var REPT_CHRR_MAIL = $('#REPT_CHRR_MAIL').val();
	
		if(REPT_CHRR_NAME ==null || REPT_CHRR_NAME==undefined || REPT_CHRR_NAME ==""){
			alert("담당자를 작성해주세요.");
			 $('#REPT_CHRR_NAME').focus();
			 return;
		}
		if(REPT_CHRR_POSI ==null || REPT_CHRR_POSI==undefined || REPT_CHRR_POSI ==""){
			alert("담당자 직위를 작성해주세요.");
			 $('#REPT_CHRR_POSI').focus();
			 return;
		}
		if(REPT_CHRR_TELX ==null || REPT_CHRR_TELX==undefined || REPT_CHRR_TELX ==""){
			alert("담당자 전화번호를 작성해주세요.");
			 $('#REPT_CHRR_TELX').focus();
			 return;
		}
		if(REPT_CHRR_MAIL ==null || REPT_CHRR_MAIL==undefined || REPT_CHRR_MAIL ==""){
			alert("담당자 이메일을 작성해주세요.");
			 $('#REPT_CHRR_MAIL').focus();
			 return;
		}

		if(!isEmail(REPT_CHRR_MAIL)){
			alert("이메일 형식에 맞게 적어주세요.");
			 return;
		}


		if(doubleSubmitCheck()) return;
		if (!confirm('보고 저작물 없음을 선택하셨습니다. \n확인해주세요.')) {
			return false;
		}
//		$form.find('#status').text('업로드 중 입니다.').show();
		$('#Progress_Loading').show(); 
        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: CTX_ROOT+'/admin/excel/noData',
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 7200000,
            success: function (data) {
            	
            if(data.success == true){
            	$('#Progress_Loading').hide();
            	 alert(data.errorMessage);
            	 location.reload();
            }else if (data.success==false){
            	$('#Progress_Loading').hide();
       
            	 alert(data.errorMessage);
            	 location.reload();
            }
            	if (data.files.length > 0) {
            		
                	
            	}
            	else {
            		alert('첨부할 수 없는 파일형식입니다.');
            		
            		$this.siblings(':text').val('');
            	}
            	
//            	$form.find('#status').text('업로드되었습니다.');
            },
            error: function (e) {
            
                alert(e.responseText);
            }
          
        });
	});
	/* 파일첨부 */
	$('#upLoadFile').click(function () {

	
		
		var $this= $(this);
		var $form = $this.closest('form');
		var data = $this.data();
		var dataform = $this.gercd;
	
		var form = $('#formUpload')[0];

		var data = new FormData(form);
		var file = $('input[name=addFile2]').val();
		
		//alert($('#gercd').val());
		var gercd = $('#gercd').val();
		var REPT_CHRR_NAME = $('#REPT_CHRR_NAME').val();
		var REPT_CHRR_POSI = $('#REPT_CHRR_POSI').val();
		var REPT_CHRR_TELX = $('#REPT_CHRR_TELX').val();
		var REPT_CHRR_MAIL = $('#REPT_CHRR_MAIL').val();
	
		if(REPT_CHRR_NAME ==null || REPT_CHRR_NAME==undefined || REPT_CHRR_NAME ==""){
			alert("담당자를 작성해주세요.");
			 $('#REPT_CHRR_NAME').focus();
			 return;
		}
		if(REPT_CHRR_POSI ==null || REPT_CHRR_POSI==undefined || REPT_CHRR_POSI ==""){
			alert("담당자 직위를 작성해주세요.");
			 $('#REPT_CHRR_POSI').focus();
			 return;
		}
		if(REPT_CHRR_TELX ==null || REPT_CHRR_TELX==undefined || REPT_CHRR_TELX ==""){
			alert("담당자 전화번호를 작성해주세요.");
			 $('#REPT_CHRR_TELX').focus();
			 return;
		}
		if(REPT_CHRR_MAIL ==null || REPT_CHRR_MAIL==undefined || REPT_CHRR_MAIL ==""){
			alert("담당자 이메일을 작성해주세요.");
			 $('#REPT_CHRR_MAIL').focus();
			 return;
		}
		if(gercd ==null || gercd==undefined || gercd ==""){
			alert("저작물 종류를 선택해주세요.");
			 $('#gercd').focus();
			 return;
		}
		if(!isEmail(REPT_CHRR_MAIL)){
			alert("이메일 형식에 맞게 적어주세요.");
			 return;
		}

		if(file ==null || file==undefined || file ==""){
			alert("파일을 선택해주세요.");
			 $('#file').focus();
			 return;
		}
		if(doubleSubmitCheck()) return;
		if (!confirm('대용량 업로드시 10~15분 정도 시간이 소요됩니다. \n완료시까지 페이지를 유지해주세요.')) {
			return false;
		}
//		$form.find('#status').text('업로드 중 입니다.').show();
		$('#Progress_Loading').show(); 
        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: CTX_ROOT+'/upload4',
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 7200000,
            success: function (data) {
            	
            if(data.success == true){
            	$('#Progress_Loading').hide();
            	 alert(data.errorMessage);
            	 location.reload();
            }else if (data.success==false){
            	$('#Progress_Loading').hide();
            	/*alert("이미 업로드한 데이터 또는 장르가 다른 데이터입니다.  \n수정시에는 일괄수정을 사용해주세요.\n( 금월내 동일종류 보고이력 존재 )");*/
            	 alert(data.errorMessage);
            	 location.reload();
            }
            	if (data.files.length > 0) {
            		
                	var filename = decodeURIComponent(data.files[0].name);
                	var filepath = data.files[0].url;
                	var filesize = data.files[0].size;
                	
                	$('#addFile').data('filename', filename);
                	$('#addFile').data('filepath', filepath);
                	$('#addFile').data('filesize', filesize);
                	
            	}
            	else {
            		alert('첨부할 수 없는 파일형식입니다.');
            		
            		$this.siblings(':text').val('');
            	}
            	
//            	$form.find('#status').text('업로드되었습니다.');
            },
            error: function (e) {
            
                alert(e.responseText);
            }
          
        });
	});
	$('#addFile').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();

		var filename = $this.data('filename');
		var filepath = $this.data('filepath');
		var filesize = $this.data('filesize');
		
		if (!filename) {
			return;
		}
		
		var $a = $('<a href="" name="file" class="link_board"><i class="fileDown board">파일 다운로드</i> </a>').append(filename);
		
		var $a2 = $('<a href="" name="removeFile" class="btn_del" />');
		var $img = $('<img />').attr('src', CTX_ROOT+'/img/btn_del.png');
		$a2.append($img)
		
		var $input1 = $('<input type="hidden" name="filename" />').val(filename);
		var $input2 = $('<input type="hidden" name="maskName" />').val(filepath);
		var $input3 = $('<input type="hidden" name="filesize" />').val(filesize);
		
		$('.wrap_link_board').append($a).append($a2).append($input1).append($input2).append($input3);
		
		$this.data('filename', '');
		$this.data('filepath', '');
		$this.siblings('input:text').val('');
	});
	
    $('.wrap_link_board').on('click', '[name="removeFile"]', function (event) {  
		var $this = $(this);

    	event.preventDefault();
    	
    	var index = $('[name="removeFile"]').index($this);
    	
    	$('[name="file"]').eq(index).remove();
    	$this.remove();
    	$('[name="filename"]').eq(index).remove();
    	$('[name="maskName"]').eq(index).remove();
    	$('[name="filesize"]').eq(index).remove();
    });
    
	/* register form - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		// validation form
		var validForm = function (params) {
			
			if (!params['title']) {
				alert('제목을 입력해 주세요');
				return false;
			}

			if (!params['content']) {
				alert('내용을 입력해 주세요');
				return false;
			}

			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
    	
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
    $('#submit').click(function (event) {
		var $form = $('#form');

    	event.preventDefault();
    	
    	$form.submit();
    });
    
});
