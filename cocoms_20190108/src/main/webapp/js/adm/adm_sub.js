$(function () {

    var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	
//    	var url = formToUrl($form);
//    	movePage('/admin/sub/companies?'+url);
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});

	$('#conDetailCd1,#conDetailCd2').click(function () {
		var $this = $(this);
		
		var val1 = Number($('#conDetailCd1:checked').val()) || 0;
		var val2 = Number($('#conDetailCd2:checked').val()) || 0;
		$('[name="conDetailCd"]').val(val1 + val2);
	});
	
	$('.list_btn [id^="changeStatCd_"]').click(function (event) {
		var $this = $(this);
    
		event.preventDefault();
		
		if ($('#data :checked').length == 0) {
			alert('처리할 데이터를 선택해 주세요');
			return;
		}
		if ($('#data :checked').length > 1) {
			alert('처리할 데이터를 한개만 선택해 주세요');
			return;
		}

		var memberSeqNo = $('#data :checked').val();
		var code = $this.attr('id').split('_')[1];
		var statCd = $('#data :checked').data('statCd');
		var preStatCd = $('#data :checked').data('preStatCd');
		
		// 신청
		if (!code) {
			if (statCd == '1' || statCd == '3') {
				code = $('#data :checked').data('statCd') + '9';	
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고신청처리
		else if (code == '2') {
			if (statCd == '19' || (statCd == '8' && preStatCd == '19')) {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고취소
		else if (code == '6') {
			if (statCd == '2' || statCd == '4' || statCd == '5') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 변경신고신청처리
		else if (code == '4') {
			if (statCd == '39' || (statCd == '8' && preStatCd == '39')) {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 보완요청
		else if (code == '7') {
			if (statCd == '19' || statCd == '39' || statCd == '8') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 반려
		else if (code == '9') {
			if (statCd == '7') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고정지
		else if (code == '5') {
			if (statCd == '2' || statCd == '4') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		// 신고정지해제
		else if (code == '59') {
			if (statCd == '5') {
			}
			else {
				alert('처리가능한 상태가아닙니다.');
				return;
			}
		}
		
		var params = {};
		params.memberSeqNo = memberSeqNo;
		params.conditionCd = $('#conditionCd').val();
		params.statCd = code;
		
		if (confirm($this.text()+' 하시겠습니까?')) {
			
			executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/status', params, function (data) {
				alert('처리되었습니다.');
			/*	location.reload();*/
				location.href = document.referrer; 
			});
		}
	});
	
	$('#tab1,#tab2').click(function (event) {
		var $this = $(this);
		var id = $this.attr('id');
		
		event.preventDefault();

		if (id == 'tab1') {
			$('#area1').show();
			$('#area2').hide();
		}
		else {
			$('#area1').hide();
			$('#area2').show();
		}
		
		$this.closest('li').addClass('active');
		$this.closest('li').siblings().removeClass('active');
	});
	
	$('.btn_area [id^="changeStatCd_"]').click(function (event) {
		var $this = $(this);
		var memberSeqNo = $('#memberSeqNo').val();
		var code = $this.attr('id').split('_')[1];
		
		event.preventDefault();
		
		var params = {};
		params.memberSeqNo = memberSeqNo;
		params.conditionCd = $('#conditionCd').val();
		params.statCd = code;
		
		if (confirm($this.text()+' 하시겠습니까?')) {
			
			//var uri = $(location).attr('pathname').substring(CTX_ROOT.length);
			executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/status', params, function (data) {
				location.reload();
			});
		}
	});
	
});
