$(function () {

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// LIST

	var search = function (page) {
    	page = page || 1;
    	var $form = $('#searchForm');
    	
    	$form.find('[name="pageIndex"]').val(page);
    	$form.submit();
    }

    $('#searchForm #search').click(function (event) {
    	
    	event.preventDefault();
    	
    	search();
    });

	$('#pagination a').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();
		
		search($this.data('page'));
	});
	
	$('#report_all').click(function (event) {
		var $this = $(this);
		
		event.preventDefault();

		var year = $('#year').val();
		var uri = CTX_ROOT + '/admin/reports/sub/all/'+year;
		
		var $form = $('<form method="GET" target="_blank" />').attr('action', uri);
		$('body').append($form);
		
		$form.submit();
	});
	
	$('#save_excel_1,#save_excel_0').click(function (event) {
		var $this = $(this);
		var type = $this.attr('id').replace('save_excel_', '');
		
		event.preventDefault();

		var year = $('#year').val();
		var uri = CTX_ROOT + '/admin/reports/sub/excel/'+year+'/'+type;
		
		var $form = $('<form method="GET" />').attr('action', uri);
		$('body').append($form);
		
		$form.submit();
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// YEARLY DETAIL
	
	$('[name="rptYear"]').change(function () {
		var $this = $(this);
		
		var url = $(location).attr('pathname').replace(CTX_ROOT, '');
		url = url.substring(0, url.lastIndexOf('/')+1) + $this.val();
		
		movePage(url);
	});
	
//	$('#edit').click(function () {
//
//		event.preventDefault();
//
//		var url = $(location).attr('pathname') + '/edit';
//		movePage(url);
//	});

//	$('#save_excel').click(function (event) {
//		var $this = $(this);
//		
//		event.preventDefault();
//
//		var memberSeqNo = $('#memberSeqNo').val();
//		var year = $('[name="rptYear"]').val();
//		var uri = CTX_ROOT + '/admin/reports/sub/'+memberSeqNo+'/years'+year+'/save';
//		
//		var $form = $('<form method="GET" />').attr('action', uri);
//		$('body').append($form);
//		
//		$form.submit();
//	});
	
	$('#print').click(function (event) {
		
	    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

	    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
	    mywindow.document.write('</head><body >');
	    mywindow.document.write('<h1>' + document.title  + '</h1>');
	    mywindow.document.write(document.getElementById('print_area').innerHTML);
	    mywindow.document.write('</body></html>');

	    mywindow.document.close(); // necessary for IE >= 10
	    mywindow.focus(); // necessary for IE >= 10*/

	    mywindow.print();
	    mywindow.close();

	    return true;
	});
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	// YEARLY DETAIL EDIT FORM
	
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();
		
		var validForm = function (params) {
			if(!params['trust']){
				if(!params['regName'] ){
					alert('작성자를 입력해주세요.');
					return false;
				}
			}
			if(!params['trust']){
			if(!params['regTel']){
				alert('전화번호를 입력해주세요.');
				return false;
			}
			}
			if(!params['trust']){
			if (!params['regTel'].match(/^[0-9 -]+$/)) {
				alert('전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
				return false;
			}
			}
			if(!params['trust']){
			if(!params['regEmail']){
				alert('이메일를 입력해주세요.');
				return false;
			}
			}
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}
		
		// form submit
		if (confirm('저장하시겠습니까?')) {
			$form.unbind().submit();
		}
	});
	
	var setSum = function (name) {
		
		var sum = 0;
		
		$('[name="'+name+'"]').each(function (key, val) {
			sum += Number($(val).val());
			
		});
		
		$('#'+name).val(sum);
	}

	
	
	/* 파일첨부 */
	$(':file[name^="file"]').change(function () {
		var $this= $(this);
		var $form = $this.closest('form');
		var data = $this.data();
		var ref_name = $this.data().refName;
		var ref_name2 = $this.data().refName2;

//		$form.find('#status').text('업로드 중 입니다.').show();
		
        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: CTX_ROOT+'/upload2?name='+$this.attr('name'),
            data: new FormData($form[0]),
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
            	
            	if (data.files.length > 0) {
            		var filename = data.files[0].name;
//                	var filename = decodeURIComponent(data.files[0].name);
                	var filepath = data.files[0].url;
                	var filesize = data.files[0].size;
                	
                	$('[name="'+ref_name+'"]').val(filename);
                	$('[name="'+ref_name2+'"]').val(filepath);
            	}
            	else {
            		alert('첨부할 수 없는 파일형식입니다.');
            		$this.siblings(':text').val('');
            	}
            	
//            	$form.find('#status').text('업로드되었습니다.');
            },
            error: function (e) {
                alert(e.responseText);
            }
        });
	});

/*	$('[id^="remove_file"]').click(function () {
		var $this = $(this);
		
		if (confirm('삭제하시겠습니까?')) {
			$('[name="'+$this.data('refName')+'"]').val('');
			$('[name="'+$this.data('refName2')+'"]').val('');
		}
	});*/
	$('#remove_file1').click(function () {
		var $this = $(this);
		var ref_name = $this.data().refName;
		var data = $this.data();

		if (confirm('삭제하시겠습니까?')) {
			$($('input[name=file1]')).val('');
			
			$('[name="'+$this.data('refName')+'"]').val('');
			$('[name="'+$this.data('refName2')+'"]').val('');
		}
	});
	
	$('#remove_file2').click(function () {
		var $this = $(this);
		
	
		if (confirm('삭제하시겠습니까?')) {
			$($('input[name=file2]')).val('');
			$('[name="'+$this.data('refName')+'"]').val('');
			$('[name="'+$this.data('refName2')+'"]').val('');
		}
	});
	$('#count input:text').keyup(function () {
		var $this = $(this);
		var id =  $this.attr('id').replace(/[^0-9]/g,"");
	
		
		if (!!Number($this.val())) {
			$this.val(Number($this.val()));	
		}
		else {
			$this.val('0');
		}
		
		setSum('inWorkNum');
		setSum('outWorkNum');
		setSum2('workSum'+id);
	});
	
	$('#amt input:text').keyup(function () {
		var $this = $(this);
		var id =  $this.attr('id').replace(/[^0-9]/g,"");
	
		if (!!Number($this.val())) {
			$this.val(Number($this.val()));	
		}
		else {
			$this.val('0');
		}
		setSum('inRental');
		setSum('outRental');
		
		setSum2('rentalSum'+id);
		
		setSum('inChrg');
		setSum('outChrg');
		
		setSum2('chrgSum'+id);
		
		setSum2('inChrgRate'+id);
		setSum2('outChrgRate'+id);
		
		setSum2('avgChrgRate'+id);
	});
	
	$('#royalty input:text').keyup(function () {
		var $this = $(this);
		var id2 =  $this.attr('id');
		var id = id2.replace(/[^0-9]/g,"");
		
	
		/*if (!!Number($this.val())) {
		
			$this.val(Number($this.val()));
			
		}
		else {
		
			$this.val('0');
		}*/

		setSum('contract');
		setSum('inCollected');
		setSum('outCollected');
    
		setSum2('collectedSum'+id);
		
		setSum('inDividend');
		setSum('outDividend');
		
		setSum2('dividendSum'+id);
		
		setSum('inDividendOff');
		setSum('outDividendOff');
		
		setSum2('dividendOffSum'+id);
		
		setSum('inChrg');
		setSum('outChrg');
		
		setSum2('chrgSum'+id);
		
	
		
		setSum2('chrgRateSum'+id); 
	});
	
	var setSum3 = function (name) {
		var id =  name.replace(/[^0-9]/g,"");
		var sum = 0;
		
		$('[name="'+name+'"]').each(function (key, val) {
			sum += Number($(val).val());
			
		});
		
		$('#'+name).val(sum);
	}
	
var setSum2 = function (name) {
		
		var id =  name.replace(/[^0-9]/g,"");
		
		var sum = 0;
		if(name.indexOf("inChrgRate")!=-1){
			if($('#inChrg'+id).val()!=0 && $('#inRental'+id).val()!=0){
			
			sum =(Number($('#inChrg'+id).val())/Number($('#inRental'+id).val()))*100; 
			sum2 =Math.round(sum*1000)/1000;
			$('#'+name).val(sum2);
			}
			if($('#inChrg'+id).val()==0 || $('#inRental'+id).val()==0){
     			$('#'+name).val('0');
			}
			
		}
		if(name.indexOf("outChrgRate")!=-1){
			
			if($('#outChrg'+id).val()!=0 && $('#outRental'+id).val()!=0){
			sum =(Number($('#outChrg'+id).val())/Number($('#outRental'+id).val()))*100; 
			sum2 =Math.round(sum*1000)/1000;
			$('#'+name).val(sum2);
			}
			if($('#outChrg'+id).val()==0 || $('#outRental'+id).val()==0){
     			$('#'+name).val('0');
			}
			
		}
		if(name.indexOf("collectedSum")!=-1){

			sum =Number($('#inCollected'+id).val())+Number($('#outCollected'+id).val()) 
			sum2 =Number($('#inCollected').val())+Number($('#outCollected').val())
			$('#'+name).val(sum);
			$('#collectedSum').val(sum2);
		}
		if(name.indexOf("dividendSum")!=-1){

			sum =Number($('#outDividend'+id).val())+Number($('#inDividend'+id).val()) 
			sum2 =Number($('#outDividend').val())+Number($('#inDividend').val())
			$('#'+name).val(sum);
			$('#dividendSum').val(sum2);
		}
		if(name.indexOf("dividendOffSum")!=-1){

			sum =Number($('#outDividendOff'+id).val())+Number($('#inDividendOff'+id).val()) 
			sum2 =Number($('#outDividendOff').val())+Number($('#inDividendOff').val())
			$('#'+name).val(sum);
			$('#dividendOffSum').val(sum2);
		}
		if(name.indexOf("chrgSum")!=-1){

			sum =Number($('#inChrg'+id).val())+Number($('#outChrg'+id).val()) 
			sum2 =Number($('#inChrg').val())+Number($('#outChrg').val())
			$('#'+name).val(sum);
			$('#chrgSum').val(sum2);
		}
		if(name.indexOf("chrgRateSum")!=-1){

			sum =Number($('#inChrgRate'+id).val())+Number($('#outChrgRate'+id).val()) 
			sum2 =Number($('#inChrgRate').val())+Number($('#outChrgRate').val())
			$('#'+name).val(sum);
			$('#chrgRateSum').val(sum2);
			
		}	
		if(name.indexOf("workSum")!=-1){
			
			sum =Number($('#inWorkNum'+id).val())+Number($('#outWorkNum'+id).val()) 
			sum2 =Number($('#inWorkNum').val())+Number($('#outWorkNum').val())
			$('#'+name).val(sum);
			$('#workSum').val(sum2);
			
		}
		if(name.indexOf("rentalSum")!=-1){
		
			sum =Number($('#inRental'+id).val())+Number($('#outRental'+id).val()) 
			sum2 =Number($('#inRental').val())+Number($('#outRental').val())
			
			$('#'+name).val(sum);
			$('#rentalSum').val(sum2);
			
		}
		if(name.indexOf("chrgSum")!=-1){

			sum =Number($('#inChrg'+id).val())+Number($('#outChrg'+id).val()) 
			sum2 =Number($('#inChrg').val())+Number($('#outChrg').val())
			$('#'+name).val(sum);
			$('#chrgSum').val(sum2);
			
		}
		if(name.indexOf("avgChrgRate")!=-1){
			if($('#chrgSum'+id).val()!=0 && $('#rentalSum'+id).val()!=0){
			sum =(Number($('#chrgSum'+id).val())/Number($('#rentalSum'+id).val()))*100
			sum2 =Math.round(sum*1000)/1000;
			
			$('#'+name).val(sum2);
			}
			if($('#chrgSum'+id).val()==0  || $('#rentalSum'+id).val()==0){
	
				$('#'+name).val('0');
				}
			
			
		}
		

	}

	
	$('#save').click(function () {
		var $form = $('#form');

		event.preventDefault();
		if (confirm('사용료,수수료 단위는 천원입니다. 저장하시겠습니까?')) {
			$form.submit();
		}
	});

});
