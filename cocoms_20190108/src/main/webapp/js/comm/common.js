var executeTransaction_sub = function (option, successCallback, errorCallback) {

//	if (!!initData.startTransaction) initData.startTransaction();
	
	if (option.method == 'GET') {
		option.dataType = 'json';
	
	}
	else if (option.method == 'POST') {
		option.contentType = 'application/x-www-form-urlencoded';
		option.dataType = 'json';
	}
	else if (option.method == 'PUT' 			|| option.method == 'PATCH') {
		option.url += '?' + jsonToUrl(option.data);
		option.contentType = 'application/json; charset=UTF-8';
		option.dataType = 'json';
//		option.data = JSON.stringify(option.data);
	}
	
	var defaultError = function (errorCallback, data) {
		
		if (!!errorCallback) errorCallback(data);
		else {
			//data.errorCode
			var msg = data.errorMessage || ' Ajax통신중 오류가 발생하였습니다.';
			alert(msg);
		}
	}
	
	$.ajax(option)
			.done(function (data) {
				//console.log('success');
//				if (!!checkLogin && !checkLogin(data)) return;
				//console.log('data : '+JSON.stringify(data));
				try {
					// if dataType is not 'json'
					data = $.parseJSON(data) || {};
					//console.log('data2 : '+data);
				} catch (e) {}

				// success
				if (data.success) {
					if (!!successCallback) successCallback(data);	
				}
				// error
				else {
					//console.log('data : '+JSON.stringify(data));
					defaultError(errorCallback, data);
				}
			})
			.fail(function (data) {
				//console.log('error');
				//console.log(data.responseText);
				//console.log(JSON.parse(data.responseText));
				defaultError(errorCallback, data);
				
			})
			.always(function() {
				//console.log('complete');
				//console.log(data);
//				if (!!initData.endTransaction) initData.endTransaction();
			});
	
}

var executeTransaction = function (method, url, data, callback, errorCallback) {
	/*alert('444');*/
	var contextRoot = CTX_ROOT || '';

	var option = {
		async: false,
		/*ContentType : 'text/html; charset=utf-8',*/
		method: method,
		url: contextRoot + url,
		data: data
	}
	
	executeTransaction_sub(option, callback, errorCallback);
}

var executeList = function (url, $form, callback, errorCallback) {
	var params = $form.serializeArray();
	executeTransaction('GET', url, params, function (data) {
		if (!!callback) callback(data);
	}, errorCallback);
}

var executeCreate = function (url, $form, callback, errorCallback) {
//	var params = $form.serializeArray();
	var params = formToJson($form);
	executeTransaction('POST', url, params, function (data) {
		if (!!callback) callback(data);
	}, errorCallback);
}

var executeModify = function (url, $form, callback, errorCallback) {
	var params = formToJson($form);
	executeTransaction('PUT', url, params, function (data) {
		if (!!callback) callback(data);
	}, errorCallback);
}

var executePatch = function (url, $form, callback, errorCallback) {
	var params = formToJson($form);
	executeTransaction('PATCH', url, params, function (data) {
		if (!!callback) callback(data);
	}, errorCallback);
}

var executeRemove = function (url, callback, errorCallback) {
	var params = {};
	executeTransaction('DELETE', url, params, function (data) {
		if (!!callback) callback(data);
	}, errorCallback);
}

var reloadPage = function () {
	location.reload();
}

var movePage = function (uri) {
//	if (uri.startsWith('/')) uri = uri.substring(1);	// ie error
	if (uri.indexOf(CTX_ROOT) == 0) uri = uri.replace(CTX_ROOT, '');
	if (uri.indexOf('/') == 0) uri = uri.substring(1);
	$(location).attr('href', CTX_ROOT+'/'+uri);
}


var verifyCert = function (ssn, callback) {

	unisign.SignDataNVerifyVID(' ', null, ssn, function(rv, signedText, certAttrs) { 
		//signedTextBox.value = signedText;
		
		if (rv != 0) {
			unisign.GetLastError(function(errCode, errMsg) { 
				alert('Error code : ' + errCode + '\n\nError Msg : ' + errMsg); 
			});
		} 
		else {
			
			if (certAttrs) {
				
				var from = new Date(certAttrs.validateFrom);
				from = toDateFormat(from).replace(/[- :]/g, '');
				var to = new Date(certAttrs.validateTo);
				to = toDateFormat(to).replace(/[- :]/g, '');
				
				certAttrs.validateFrom2 = from;
				certAttrs.validateTo2 = to;
				
				if (!!callback) callback(certAttrs, signedText);	
			}
		}
	});
}

/* cross cert */
var uniSignModule = (function() {

	var signObj = {
		signedText: null,
		dn: null,
		sn: null,
		certAttrs: null
	};
	
	var open = function(args) {
		
		if(typeof args.textBox === 'undefined') {
			var input = document.createElement('input');
		    input.type = 'hidden';
		    input.name = 'plainText';
		    input.value = ' ';
		    
			args.textBox = input;
		}
		
		// 인증서 모듈 실행
        unisign.SignData(args.textBox, null, function(signedText, curdevice, certAttrs) {
			signObj.signedText = signedText; 
			
			if (null == signObj.signedText || '' == signObj.signedText) {
				unisign.GetLastError( function(errCode, errMsg) { 
					if (errCode == 999) {
						 alert("사용자가 인증서 선택창을 취소 하였습니다.");
						 
						 _check = false;
						 window.self.close();
					} else {
						_check = false;
						 alert('Error code : ' + errCode + '\n\nError Msg : ' + errMsg);
					}
				});
				//history.back();
			} else {
				signObj.dn = certAttrs.subjectName;
				signObj.sn = certAttrs.serialNumber;
				signObj.validateFrom = certAttrs.validateFrom;
				signObj.validateTo = certAttrs.validateTo;
				//signObj.certAttrs = certAttrs;
				
				var from = new Date(signObj.validateFrom);
				from = toDateFormat(from).replace(/[- :]/g, '');
				var to = new Date(signObj.validateTo);
				to = toDateFormat(to).replace(/[- :]/g, '');
				
				signObj.validateFrom2 = from;
				signObj.validateTo2 = to;
				
				if (args.callback == null || typeof(args.callback) === 'undefined') {
					_return();
				} else {
				    args.callback(signObj);
				}
				  
			};
		});
    };
	
    var _return  = function() {
    	return signObj;
    };
    
	return {
		open: open,
		_return : _return
	};
}());

var printReport_sub = function (rpx, xmldata, type) {
	type = type || '';

	var url;
	 $.ajax({   
	   type: "POST"  
	  , url: CTX_ROOT+"/getUrl"
	  ,data: "data=" + type
	  ,async:false
	  ,success:function(data){
	   url=data;
	  }
	  ,error:function(data){
	    alert("error");
	    return false;
	  }
	  });
	 
	xmldata = $('#xmldata').text();
	xmldata = xmldata.replace(/&amp;/g, '&').replace(/&#35;/g, '#').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&#39;/g, '\\').replace(/&#37;/g , '%' ).replace(/&#40;/g , '(').replace(/&#41;/g , ')').replace(/&#43;/g , '+').replace(/&#47;/g , '/').replace(/&#46;/g , '.');
	xmldata = window.btoa(unescape(encodeURIComponent(xmldata)));
	
	var $input1 = $('<input type="hidden" />').attr('name', 'Servlet').val('html_service.jsp');
	var $input2 = $('<input type="hidden" />').attr('name', 'ReportFile').val(rpx);
	var $input3 = $('<input type="hidden" />').attr('name', 'Process').val('PDF');
	var $input4 = $('<input type="hidden" />').attr('name', 'rptTitle').val(rpx);
	var $input5 = $('<input type="hidden" />').attr('name', 'data').val(xmldata);
	var $input6 = $('<input type="hidden" />').attr('name', 'type').val('xml');
	
	if (type == 'cert') {
	
		
		$input1 = $('<input type="hidden" />').attr('name', 'Servlet').val('smartcert_service.jsp');
		$input2 = $('<input type="hidden" />').attr('name', 'ReportFile').val(rpx+'_cert');
	}

	// save log
	var params = {};
	params.action = 'application print';
	executeTransaction('POST', '/log/save', params, function (data) {
	});
	
	var $form = $('<form method="POST" target="_blank" />').attr('action', url);
	$form.append($input1).append($input2).append($input3).append($input4).append($input5).append($input6);
	
	return $form;
}

var printReport = function (rpx, xmldata, type) {
	
	var $form = printReport_sub(rpx, xmldata, type);
	
	$('body').append($form);
	$form.submit();
}

var printReport2 = function (rpx, xmldata, type) {
	
	var $form = printReport_sub(rpx, xmldata, type);
	$form.attr('target', '');
	
	$('body').append($form);
	$form.submit();
}
