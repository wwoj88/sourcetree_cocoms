function fileUploadActive_ww(num){
	seletno = num;
	
	$('input[name=file'+num+']').click();
	$('.insert_file_target').each(function(i){
		$(this).change(function(){
			$(this).prev().val($(this).val());
		})
	});
	/*$('input[name=filename'+num+']').val($('input[name=file'+num+']').val());*/
};

var filecnt = 1;
var seletno = 1;

function numberOnly(){
	   if((event.keyCode<48)||(event.keyCode>57))
		   event.returnValue=false;
}


$(function () {

	
	   $('[name="popup_close"]').click(function (event) {
	    	var $this = $(this);
			event.preventDefault();

	    	$this.closest('.pop_common').hide();
	    });
	   


	

	$('input:radio[name="coGubunCd"]').change(function () {
		var $this = $(this);
		var val = $this.val();

		if (val == '1') {
			$('[id^="coGubunCd2_layer"]').hide();
		    $('#checkedstar').hide();
		    $('[id^="addfile"]').show();
			$('#addfile').hide();
		}
		else {
			$('[id^="coGubunCd2_layer"]').show();
			//$('#checkedstar').show();
			$('[id^="addfile"]').show();
			$('#addfile').show();
		}
		
	});
	
	
    $('#addfiles').click(function(){
    	if(filecnt ==6 ){
    		alert('파일추가는 6개까지만 됩니다.');
    		return false;
    	}
    	filecnt++;
    	var htm = '<div class="list_file">' 
    	+'<form id="addForm'+filecnt+'" >'
    	+'<p class="txt">첨부파일 <strong class="txt_point">*</strong></p>'
    	+'<input type="text" class="input_style1 prj_file" id="realfilename'+filecnt+'" placeholder="PDF, JPG  파일만 첨부" title="파일첨부" readonly />'
    	+'<input type="file" name="file'+filecnt+'" class="insert_file_target" data-ref-name="file'+filecnt+'Path" title="파일첨부" />\n'
    	+'<button type="button" class="form_btn insert_file btn_style1" onclick="fileUploadActive_ww('+filecnt+')">파일찾기</button>\n'
    	+'<button type="button" class="btn_style1" onclick="removefile('+filecnt+')">파일삭제</button>'
    	+'</form>'
    	+'<input type="hidden" name="fileName2"/>'
	    +'<input type="hidden" name="fileSize2"/>'
		+'<input type="hidden" name="filePath2"/>'
    	+'</div>';

    	
    	//htm = $( '#add' ).html()+htm;
    	//$( '#add' ).html(htm);
    	$( '#add' ).append(htm).trigger("create");
    });
	$('input:checkbox[name="chgCd"]').change(function () {
		var $this= $(this);
		//var $form = $this.closest('form');
		var data = $this.data();
		var name =  $this.attr('id').substr(5,1);
		if(($this).prop('checked')==true){
		   if(name=='1'){
			   $('.ceoinfo input[type=text],  .ceoinfo input[type=checkbox], .ceoinfo input[type=password]').prop('disabled',false);
			   $('#modiceoTel').prop('disabled',false);
			   $('#modiceoFax').prop('disabled',false);
			   $('#findCeoAddr').prop('disabled',false);
		   }
		   if(name=='2' ){
				$('input:text[name="coName"]').prop('disabled',false);   
				
		   }
		   if(name=='3' ){
				$('input:text[name="trustDetailAddr"], .membinfo button[type=button]' ).prop('disabled',false);   
				$('input:text[name="trustZipcode"], .membinfo button[type=button]' ).prop('disabled',false);   
				$('input:text[name="trustAddr"], .membinfo button[type=button]' ).prop('disabled',false);   
		  	    $('#moditrustTel').prop('disabled',false);
		  	    $('#moditrustFax').prop('disabled',false);
		  	    $('input:text[name="trustTel2"]' ).prop('disabled',false);   
		  	    $('input:text[name="trustTel1"]' ).prop('disabled',false);   
		  	    $('input:text[name="trustTel3"]' ).prop('disabled',false);   
		  	    $('input:text[name="trustFax2"]' ).prop('disabled',false);   
		  	    $('input:text[name="trustFax3"]' ).prop('disabled',false);   
		  	    $('input:text[name="trustFax1"]' ).prop('disabled',false);   
		   }
		   if(name=='6'){
			   $('.list_copy input[type=text],  .list_copy input[type=checkbox], .list_copy input[type=password]').prop('disabled',false);
		   }
	/*	   if(name=='7'){
			   	$('input:text[name="coNo1"]').prop('disabled',false);   
				$('input:text[name="coNo2"]').prop('disabled',false);   
				$('input:text[name="coNo3"]').prop('disabled',false);   
				$('input:text[name="bubNo1"]').prop('disabled',false);   
				$('input:text[name="bubNo2"]').prop('disabled',false);   
		   }*/
		   
		}else{
		   if(name=='1'){
			   $('.ceoinfo input[type=text],  .ceoinfo input[type=checkbox], .ceoinfo input[type=password]').prop('disabled',true);
			   $('#modiceoTel').prop('disabled',true);
			   $('#modiceoFax').prop('disabled',true);
			   $('#findCeoAddr').prop('disabled',true);
		   }
		   if(name=='2' ){
			   $('.membinfo input[type=text], .membinfo input[type=checkbox], .membinfo button[type=button] ').prop('disabled',true);   
		   }
		   if(name=='3' ){
				$('input:text[name="trustDetailAddr"], .membinfo button[type=button]').prop('disabled',true);
				$('input:text[name="trustZipcode"], .membinfo button[type=button]' ).prop('disabled',true);   
				$('input:text[name="trustAddr"], .membinfo button[type=button]' ).prop('disabled',true);   
				  $('#moditrustTel').prop('disabled',true);
			      $('#moditrustFax').prop('disabled',true);
		    	  $('input:text[name="trustTel2"]' ).prop('disabled',true);   
		    	  $('input:text[name="trustTel1"]' ).prop('disabled',true);   
		   	      $('input:text[name="trustTel3"]' ).prop('disabled',true);   
			  	  $('input:text[name="trustFax2"]' ).prop('disabled',true);   
			  	  $('input:text[name="trustFax3"]' ).prop('disabled',true);   
			  	  $('input:text[name="trustFax1"]' ).prop('disabled',true);   
		   }
		   if(name=='6'){
			   $('.list_copy input[type=text],  .list_copy input[type=checkbox], .list_copy input[type=password]').prop('disabled',true);
		   }
		}
		

	})
	$('input#conDetailCd1,input#conDetailCd2').click(function () {
		var $this = $(this);
		
		var val1 = Number($('input#conDetailCd1:checked').val()) || 0;
		var val2 = Number($('input#conDetailCd2:checked').val()) || 0;
		$('[name="conDetailCd"]').val(val1 + val2);
	});
	
	// writing kind checkbox
	$('input:checkbox[id^="writingKind"]').click(function () {
		var $this = $(this);
		var checked = $this.prop('checked');
		var writing_kind = $this.attr('id').replace('writingKind', '');
		
		if (!checked) {
			$('input:checkbox[name="permkind'+writing_kind+'"]').prop('checked', false);
			$('input:text[name="permEtc'+writing_kind+'"]').prop('disabled', true).val('');
		}
	});
	
	// perm kind checkbox
	$('input:checkbox[id^="permkind"]').click(function () {
		var $this = $(this);
		var name = $this.attr('name');
		var checked = $this.prop('checked');
		var writing_kind = $this.attr('name').replace('permkind', '');
		
		if (checked) {
			$('input:checkbox[id="writingKind'+writing_kind+'"]').prop('checked', true);
		}
		
		var cnt = $('input:checkbox[name="'+name+'"]:checked').length;
		if (cnt == 0) {
			$('input:checkbox[id="writingKind'+writing_kind+'"]').prop('checked', false);
		}
	});

	// 기타 checkbox
	$('input:checkbox[id^="permkind"][id$="_0"]').click(function () {
		var $this = $(this);
		var checked = $this.prop('checked');
		var writing_kind = $this.attr('name').replace('permkind', '');
		
		if (checked) {
			$('input:text[name="permEtc'+writing_kind+'"]').prop('disabled', false);	
		}
		else {
			$('input:text[name="permEtc'+writing_kind+'"]').prop('disabled', true).val('');
		}
	});
	
	// 기타 checkbox
	$('input:text[name^="permEtc"]').keyup(function () {
		var $this = $(this);
		var name = $this.attr('name');
		
		var target_id = name.replace('permEtc', 'permkind')+'_0';
		$('[id="'+target_id+'"]').prop('checked', true);
		
		var target_id2 = name.replace('permEtc', 'writingKind');
		$('[id="'+target_id2+'"]').prop('checked', true);
	});
	
	// daum address searcher 
	var popupDaumAddr = function (postcodeId, addrId, sidoId, sigunguId, bnameId) {
		daum.postcode.load(function(){
		new daum.Postcode({
			oncomplete: function(data) {
				
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                   extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById(postcodeId).value = data.zonecode; //5자리 새우편번호 사용
//                document.getElementById('sample4_roadAddress').value = fullRoadAddr;
//                document.getElementById('sample4_jibunAddress').value = data.jibunAddress;
                if (!!fullRoadAddr) {
                	document.getElementById(addrId).value = fullRoadAddr;
                }
                else {
                	document.getElementById(addrId).value = data.jibunAddress;
                }

                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    //예상되는 도로명 주소에 조합형 주소를 추가한다.
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
//                    document.getElementById('guide').innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
//                    document.getElementById('guide').innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

                } else {
//                    document.getElementById('guide').innerHTML = '';
                }
                
                if (!!sidoId && !!data.sido) {
                	document.getElementById(sidoId).value = data.sido;
                }
                if (!!sigunguId && !!data.sigungu) {
                	document.getElementById(sigunguId).value = data.sigungu;
                }
                if (!!bnameId && !!data.roadname) {
                    var val = document.getElementById(addrId).value;
                    val = val.substring(val.lastIndexOf(data.roadname));
                	document.getElementById(bnameId).value = val;
                }
			}
		}).open();
	})
	};
	/* 회원정보 주소찾기 */
	$('#findTrustAddr').click(function () {
		
		popupDaumAddr('trustZipcode', 'trustAddr', 'trustSido', 'trustGugun', 'trustDong');
	});
	
	/* 대표자정보 주소찾기 */
	$('#findCeoAddr').click(function () {
		
		popupDaumAddr('ceoZipcode', 'ceoAddr', 'ceoSido', 'ceoGugun', 'ceoDong');
	});
	
	
	/* 파일첨부 */
	$(':file[name^="file"]').change(function () {
		
		var $this= $(this);
		//var $form = $this.closest('form');
		var data = $this.data();
		var ref_name = $this.data().refName;
		var name =  $this.attr('name');
		 var ext = $("input[name='file"+name.substr(4,1)+"']").val().split('.').pop().toLowerCase();
		 if(!ext){
			 return false;
		 }
         $('#tmpForm'+name.substr(4,1)).ajaxForm({
            type: 'POST',
            enctype: 'multipart/form-data',
//            url: '/test.do',
            url: CTX_ROOT+'/upload?name='+$this.attr('name'),
            //data: new FormData($form[0]),
            dataType: 'json',
            processData: false,
            contentType: false,
            async: false,
            cache: false,
            timeout: 600000,
            beforeSubmit: function (data,form2,option){
            	
                var ext = $("input[name='file"+name.substr(4,1)+"']").val().split('.').pop().toLowerCase();
                
                if(jQuery.inArray(ext, ['jpg','pdf','zip']) == -1) {
                 	if(!ext){
                 		alert(ext);
                		return false;
                	}
                    alert("JPG,PDF 파일만 업로드 할수 있습니다.");
                	$this.siblings(':text').val('');
                    return false; 
                }       
           },
            success: function (data) {
            	//var data = JSON.parse(data);
            	if (data.files.length > 0) {

            		var filename = decodeURIComponent(data.files[0].name);
                	var filepath = data.files[0].url;
                	var filesize = data.files[0].size;
                	
                	$('[name="'+ref_name+'"]').val(filepath);
            	}
            	else {
            		alert('PDF, JPG 파일만 첨부 가능합니다.');
            		$this.siblings(':text').val('');
            	}
            	
//            	$form.find('#status').text('업로드되었습니다.');
            },
            error: function (e) {
            	alert("첨부파일업로드 오류 : "+JSON.stringify(e));
             	$this.siblings(':text').val('');
            	$this.val('');
            }
        });
   
         $('#tmpForm'+name.substr(4,1)).submit();
	});
   
});

	 
function removefile(num){
	
	
	 if(confirm("정말 삭제하시겠습니까?")==false){
		 return false;
	 }else{
		var $this= $($('input[name=file'+num+']'));
		
		
		$this.siblings(':text').val('');
		$this.val('');
		$('input[name=file'+num+'Path]').val('');
		/*$('input[name=file'+num+']').val();*/
		
		$('input[name=fileName2]').eq(num-1).val('');
		$('input[name=fileSize2]').eq(num-1).val('');
		$('input[name=filePath2]').eq(num-1).val('');
		
	 }

	/*	$('.insert_file_target').each(function(i){
			$(this).change(function(){
				$(this).prev().val($(this).val());
			})
		});
*/
		/*$('input[name=filename'+num+']').val($('input[name=file'+num+']').val());*/
	};
   
/*	


*/
/* 파일첨부 */
//$(':file[name^="file"]').change(function () {
$(document).on("change",':file[name^="file"]',function(){

	var $this= $(this);
	//var $form = $this.closest('form');
	var data = $this.data();
	var ref_name = $this.data().refName;
	
	var name =  $this.attr('name');
	
	var num  = name.substr(4,1);
	 var ext = $("input[name='file"+name.substr(4,1)+"']").val().split('.').pop().toLowerCase();
	 if(!ext){
		
		 return false;
	 }
	    $('#addForm'+name.substr(4,1)).ajaxForm({
        type: 'POST',
        enctype: 'multipart/form-data',
//        url: '/test.do',
        url: CTX_ROOT+'/upload?name='+$this.attr('name'),
        //data: new FormData($form[0]),
        dataType: 'json',
        processData: false,
        contentType: false,
        async: false,
        cache: false,
        timeout: 600000,
        beforeSubmit: function (data,form2,option){
       
            var ext = $("input[name='file"+name.substr(4,1)+"']").val().split('.').pop().toLowerCase()
             if(jQuery.inArray(ext, ['pdf','jpg','zip']) == -1) {
            	if(!ext){
            		return false;
            	}
            	  alert("PDF,JPG 파일만 업로드 할수 있습니다.");
            	$this.siblings(':text').val('');
                return false; 
            }       
       },
        success: function (data) {
        
        	//var data = JSON.parse(data);
        	
        	if (data.files.length > 0) {

        		var filename = decodeURIComponent(data.files[0].name);
            	var filepath = data.files[0].url;
            	var filesize = data.files[0].size;
            	
/*            	$('[name="file'+seletno+'Path"]').val(filepath);
            	$('[name="file'+seletno+'Size"]').val(filesize);
            	$('[name="file'+seletno+'Name"]').val(filename);
            	
            	    */        	

         
            	$("input[name=filePath2]:eq("+(seletno-1)+")").val(filepath);
            	$("input[name=fileSize2]:eq("+(seletno-1)+")").val(filesize);
            	$("input[name=fileName2]:eq("+(seletno-1)+")").val(filename);
            	$this.siblings(':text').val(filename);
            	
        	}
        	else {
        		alert('PDF, JPG 파일만 첨부 가능합니다.');
        		$this.siblings(':text').val('');
        	}
        	
//        	$form.find('#status').text('업로드되었습니다.');
        },
        error: function (e) {
        	alert("첨부파일업로드 오류 : "+JSON.stringify(e));
            $this.siblings(':text').val('');
        	$this.val('');
        }
    });
	    
     $('#addForm'+name.substr(4,1)).submit();
});

//$(document).on("click","#addForm2",function(){
//	alert('2');
//});
//validation form
var validForm = function (params) {
	
	var yy = params['ceoRegNo1'].substring(0,2);
	var mm = params['ceoRegNo1'].substring(2,4);
	var dd = params['ceoRegNo1'].substring(4,6);
	var gender =params['ceoRegNo2'].substring(0,1);
	var ck=0;
    var url =$(location).attr('pathname');
 
	var jumins3 = params['ceoRegNo1'] + params['ceoRegNo2'];
    //주민등록번호 생년월일 전달
     
    var fmt = RegExp(/^\d{6}[1234]\d{6}$/) 
    //포멧 설정
    var buf = new Array(13);
    var regExp = /^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))[1-6][0-9]{6}$/;
    //주민번호 유효성 검사\
   if(url == "/cocoms/appl/sub/new" || url =="/cocoms/appl/trust/new" ){
	   
	    if (!regExp.test(jumins3)) {
	        alert("대표자 주민등록번호를 입력해주세요.");
	        $("input[name=ceoRegNo1]").val("");
	        $("input[name=ceoRegNo2]").val("");
	        $("input[name=ceoRegNo1]").focus();

	        return false;
	      }
   }
   
   if(params['ceoRegNo2'] != '*******' || !params['ceoRegNo2'] ){
	   
    if (!regExp.test(jumins3)) {
      alert("주민등록번호 형식에 맞게 입력해주세요");
      $("input[name=ceoRegNo1]").val("");
      $("input[name=ceoRegNo2]").val("");
      $("input[name=ceoRegNo1]").focus();

      return false;
    }
   }
    var ssnCheck = {
    	    /**
    	     * 유효성검사. allCheck("8201011234567");
    	     */
    	    allCheck : function(rrn) { 
    	        if (this.fnrrnCheck(rrn) || this.fnfgnCheck(rrn) ) {
    	             
    	             return true;
    	        }
	    	   
    	        return false;
    	    },
    	 
    	    /**
    	     * 주민등록번호 유효성검사
    	     */
    	    fnrrnCheck : function(rrn) {
    	        var sum = 0;
    	         
    	        if (rrn.length != 13) {
    	            return false;
    	        } else if (rrn.substr(6, 1) != 1 && rrn.substr(6, 1) != 2 && rrn.substr(6, 1) != 3 && rrn.substr(6, 1) != 4) {
    	            return false;
    	        }
    	         
    	        for (var i = 0; i < 12; i++) {
    	            sum += Number(rrn.substr(i, 1)) * ((i % 8) + 2);
    	        }
    	         
    	        if (((11 - (sum % 11)) % 10) == Number(rrn.substr(12, 1))) {
    	            return true;
    	        }
    	         
    	        return false;
    	    },
    	 
    	    /**
    	     * 외국인등록번호 유효성검사
    	     * @param rrn
    	     * @returns
    	     */
    	    fnfgnCheck : function(rrn) {
    	        var sum = 0;
    	         
    	        if (rrn.length != 13) {
    	            return false;
    	        } else if (rrn.substr(6, 1) != 5 && rrn.substr(6, 1) != 6 && rrn.substr(6, 1) != 7 && rrn.substr(6, 1) != 8) {
    	            return false;
    	        }
    	         
    	        if (Number(rrn.substr(7, 2)) % 2 != 0) {
    	            return false;
    	        }
    	         
    	        for (var i = 0; i < 12; i++) {
    	            sum += Number(rrn.substr(i, 1)) * ((i % 8) + 2);
    	        }
    	         
    	        if ((((11 - (sum % 11)) % 10 + 2) % 10) == Number(rrn.substr(12, 1))) {
    	            return true;
    	        }
    	         
    	        return false;
    	    }    	 
    	
    	}
    if(params['ceoRegNo2'] != '*******' || !params['ceoRegNo2'] ){
 	   
    if(ssnCheck.allCheck(jumins3)==false){
    	 alert('주민등록번호 또는 외국인등록번호를 정확하게 입력해주세요.');
        $("input[name=ceoRegNo1]").val("");
        $("input[name=ceoRegNo2]").val("");
        $("input[name=ceoRegNo1]").focus();
    	return false;
    }
    }
/*  if ((11 - (sum % 11)) % 10 != buf[12]) {
    alert("잘못된 주민등록번호 입니다.");
    $("input[name=ceoRegNo1]").val("");
    $("input[name=ceoRegNo2]").val("");
    $("input[name=ceoRegNo1]").focus();

    return false;
  }*/
	if(!params['coName']){
		alert('업체명을 입력해 주세요.');
		 $("input[name=coName]").focus();
		 return false;
		
	}
	if (!params['coGubunCd']) {
		alert('회원구분을 선택해 주세요.');
		return false;
	}
	/*if (params['coGubunCd'] == '2') {
		if (!params['coNo1'] 	|| !params['coNo2'] 	|| !params['coNo3']) {
		    $("input[name=coNo1]").val("");
		    $("input[name=coNo2]").val("");
		    $("input[name=coNo3]").val("");
		    $("input[name=coNo1]").focus();
		alert('사업자등록번호를 입력해 주세요.');
		return false;
	}
	if (!params['coNo1'].match(/^[0-9]{3}$/)
			|| !params['coNo2'].match(/^[0-9]{2}$/)
			|| !params['coNo3'].match(/^[0-9]{5}$/)) {
		alert('사업자번호 형식에 맞지 않습니다.');
		 $("input[name=coNo1]").val("");
		 $("input[name=coNo2]").val("");
		 $("input[name=coNo3]").val("");
		 $("input[name=coNo1]").focus();
		return false;
	}
	if (!check_busino(params['coNo1'] + params['coNo2'] + params['coNo3'] + '')) {
		alert('사업자번호가 유효하지않습니다.');
		 $("input[name=coNo1]").val("");
		 $("input[name=coNo2]").val("");
		 $("input[name=coNo3]").val("");
		 $("input[name=coNo1]").focus();
		return false;
	}
	}*/

	// 법일일 경우
	if (params['coGubunCd'] == '2') {
		
		if (!params['bubNo1'] || !params['bubNo2']) {
			alert('법인 등록번호를 입력해 주세요.');
			 $("input[name=bubNo1]").val("");
			 $("input[name=bubNo2]").val("");
			 $("input[name=bubNo1]").focus();
			return false;
		}
		if (!params['bubNo1'].match(/^[0-9]+$/)	|| !params['bubNo2'].match(/^[0-9]+$/)) {
			alert('법인 등록번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
			 $("input[name=bubNo1]").val("");
			 $("input[name=bubNo2]").val("");
			 $("input[name=bubNo1]").focus();
			return false;
		}
	}
	
	if (!params['trustZipcode']
//			|| !params['trustAddr']
			|| !params['trustDetailAddr']) {
		alert('주소를 입력해 주세요.');
		 $("#trustDetailAddr").focus();
		return false;
	}
	
	if (!params['trustTel1']
			|| !params['trustTel2']
			|| !params['trustTel3']) {
		alert('전화번호를 입력해 주세요.');
		 $("input[name=trustTel1]").val("");
		 $("input[name=trustTel2]").val("");
		 $("input[name=trustTel3]").val("");
		 $("input[name=trustTel1]").focus();
		return false;
	}
	if (!params['trustTel1'].match(/^[0-9]+$/)
			|| !params['trustTel2'].match(/^[0-9]+$/)
			|| !params['trustTel3'].match(/^[0-9]+$/)) {
		alert('전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
		 $("input[name=trustTel1]").val("");
		 $("input[name=trustTel2]").val("");
		 $("input[name=trustTel3]").val("");
		 $("input[name=trustTel1]").focus();
		return false;
	}
	
	if (!params['ceoName']) {
		alert('대표자명을 입력해 주세요.');
		 $("input[name=ceoName]").val("");
		 $("input[name=ceoName]").focus();
		return false;
	}
	
	if (!params['ceoRegNo1']
			|| !params['ceoRegNo2']) {
		alert('주민등록번호를 입력해 주세요.');
	      $("input[name=ceoRegNo1]").val("");
	      $("input[name=ceoRegNo2]").val("");
	      $("input[name=ceoRegNo1]").focus();
		return false;
	}
	 if(params['ceoRegNo2'] != '*******' || !params['ceoRegNo2'] ){
	
	if (!params['ceoRegNo1'].match(/^[0-9]{6}$/)
			|| !params['ceoRegNo2'].match(/^[0-9]{7}$/)) {
		alert('주민등록번호 형식에 맞지 않습니다.');
	      $("input[name=ceoRegNo1]").val("");
	      $("input[name=ceoRegNo2]").val("");
	      $("input[name=ceoRegNo1]").focus();
		return false;
	}
	 }
	if (!params['ceoZipcode']
//			|| !params['ceoAddr']
			|| !params['ceoDetailAddr']) {
		alert(' 대표자 주소를 입력해 주세요.');
	     $("#ceoDetailAddr]").focus();
		return false;
	}

	if (!params['ceoTel1']	|| !params['ceoTel2']	|| !params['ceoTel3']) {
	     alert('전화번호를 입력해 주세요.');
	      $("input[name=ceoTel1]").val("");
	      $("input[name=ceoTel2]").val("");
	      $("input[name=ceoTel3]").val("");
	      $("input[name=ceoTel1]").focus();
		return false;
	}
	if (!params['ceoTel1'].match(/^[0-9]+$/)
			|| !params['ceoTel2'].match(/^[0-9]+$/)
			|| !params['ceoTel3'].match(/^[0-9]+$/)) {
		alert('전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
		  $("input[name=ceoTel1]").val("");
	      $("input[name=ceoTel2]").val("");
	      $("input[name=ceoTel3]").val("");
	      $("input[name=ceoTel1]").focus();
		return false;
	}

	if (!params['ceoMobile1']
			|| !params['ceoMobile2']
			|| !params['ceoMobile3']) {
		alert('휴대전화번호를 입력해 주세요.');
		  $("input[name=ceoMobile1]").val("");
	      $("input[name=ceoMobile2]").val("");
	      $("input[name=ceoMobile3]").val("");
	      $("input[name=ceoMobile1]").focus();
		return false;
	}			
	if (!params['ceoMobile1'].match(/^[0-9]+$/)
			|| !params['ceoMobile2'].match(/^[0-9]+$/)
			|| !params['ceoMobile3'].match(/^[0-9]+$/)) {
		alert('휴대전화번호 형식에 맞지 않습니다.\n(숫자만 입력가능합니다.)');
		  $("input[name=ceoMobile1]").val("");
	      $("input[name=ceoMobile2]").val("");
	      $("input[name=ceoMobile3]").val("");
	      $("input[name=ceoMobile1]").focus();
		return false;
	}

	if (params['statCd'] == '3'
			&& !params['chgCd']) {
		alert('변경사항을 선택해 주세요.');
		return false;
	}
	
	if (params['conditionCd'] == '1'
			&& (!params['conDetailCd'] || params['conDetailCd'] == 0)) {
		alert('취급 하고자 하는 업무의 내용을 선택해 주세요.');
		return false;
	}
	
	if ($('input:checkbox[id^="writingKind"]:checked').length == 0) {
		alert('취급 저작물의 종류 및 취급 권리의 종류을 선택해 주세요.');
		return false;
	}
	if( params['statCd'] == '3'){
		var fileName = $("input[name=fileName2]").val();
		if(!fileName){
			alert('첨부파일을 업로드 해주세요.');
			return false;
	}
	}
	
	if( params['statCd'] == '3'){
	      if(!params['chgMemo']){
			alert('변경 사유와 내용을 입력해주세요.');
			  $("input[name=chgMemo]").val("");
		      $("input[name=chgMemo]").focus();
			return false;
	      }
	}
		
	if( params['statCd'] != '3'){
	if (params['coGubunCd'] == '1'){
		
		if (!params['file1Path']|| !params['file2Path'] || !params['file5Path']){
			alert('첨부파일을 모두 업로드 해주세요.');
			return false;
		}
	}
	if (params['coGubunCd'] == '2') {
		if (!params['file1Path']
			|| !params['file2Path']
			|| !params['file3Path']
			|| !params['file4Path']
		|| !params['file5Path']) {
		alert('첨부파일을 모두 업로드 해주세요.');
		return false;
		}
	  }
	}
	
	return true;
}
