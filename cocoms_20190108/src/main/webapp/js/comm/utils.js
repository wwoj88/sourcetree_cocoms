var formToJson = function ($form) {
	var params = {};
	
	$form.serializeArray().map(function (m) {
		var key = m.name;
		var val = m.value;
		
		params[key] = val;
	});
	
	return params;
}

var jsonToUrl = function (data) {
	
	var url = Object.keys(data).map(function(k) {
	    return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
	}).join('&');
	
	return url;
}

var formToUrl = function ($form) {
	var data = formToJson($form);
	var url = jsonToUrl(data);
	return url;
}

var toDateFormat = function (date) {
	var month = (date.getMonth()+1) < 10 ? '0'+(date.getMonth()+1) : (date.getMonth()+1);
	var day = date.getDate() < 10 ? '0'+date.getDate() : date.getDate();
	var hour = date.getHours() < 10 ? '0'+date.getHours() : date.getHours();
	var min = date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes();
	var sec = date.getSeconds() < 10 ? '0'+date.getSeconds() : date.getSeconds();
	
	return date.getFullYear()+'-'+month+'-'+day+' '+hour+':'+min+':'+sec;
}

/**
 * 사업자번호체크
 */
function check_busino(vencod) {
	var sum = 0;
	var getlist =new Array(10);
	var chkvalue =new Array("1","3","7","1","3","7","1","3","5");
	
	for(var i=0; i<10; i++) { getlist[i] = vencod.substring(i, i+1); }
	for(var i=0; i<9; i++) { sum += getlist[i]*chkvalue[i]; }
	
	sum = sum + parseInt((getlist[8]*5)/10);
	sidliy = sum % 10;
	sidchk = 0;
	
	if(sidliy != 0) { sidchk = 10 - sidliy; }
	else { sidchk = 0; }
	
	if(sidchk != getlist[9]) { return false; }
	
	return true;
}

function setCookie(name, value, expiredays) {
	var todayDate = new Date();
	todayDate.setDate(todayDate.getDate() + expiredays);
	document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
}

function getCookie(name) {
	var nameOfCookie = name + "=";
	var x = 0;
	while (x <= document.cookie.length) {
		var y = (x + nameOfCookie.length);
		if (document.cookie.substring(x, y) == nameOfCookie) {
			if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
				endOfCookie = document.cookie.length;
			return unescape(document.cookie.substring(y, endOfCookie));
		}
		x = document.cookie.indexOf(" ", x) + 1;
		if (x == 0)
			break;
	}
	return "";
}
