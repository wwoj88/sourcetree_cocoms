<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>DB Connection Test</title>
</head>
<body>
<%
	String DB_URL = "jdbc:oracle:thin:@222.231.43.26:1521:ora10g";	
	String DB_USER = "smsmanager";
	String DB_PASSWORD= "eksanswk2014";
	
	Connection conn;
	Statement stmt;
	
	try {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		stmt = conn.createStatement();
		//conn.close();
		
		out.println("Oracle jdbc test: connect ok!!");
	} catch(Exception e) {
		out.println(e.getMessage());
	}
	
    try {
		String sql = "INSERT INTO SC_TRAN (TR_NUM ,TR_SENDDATE , TR_SENDSTAT ,TR_MSGTYPE ,TR_PHONE ,TR_CALLBACK , TR_MSG) VALUES (SC_TRAN_SEQ.NEXTVAL, SYSDATE, '0', '0', '01068652031', '0233334444',  'Test')";
		conn.prepareStatement(sql).executeUpdate();
    } catch(Exception e) {
		out.println(e.getMessage());
	} finally {
    	if(conn != null) try { conn.close(); } catch(SQLException sqle) {}
    }
%>
</body>
</html>
