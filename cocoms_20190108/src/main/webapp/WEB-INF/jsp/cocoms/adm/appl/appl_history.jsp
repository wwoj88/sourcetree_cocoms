<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="${title1}" />
	<jsp:param name="nav2" value="${title2}" />
	<jsp:param name="nav3" value="신청이력" />
	<jsp:param name="title" value="NONE" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />
	
		<h2 class="tit_c">신청이력</h2>
		<h2 class="tit_c_s pt0">${member.coName}</h2> 
		<div class="total_top_common pt0">
			<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
		</div>
		<div class="table_area list list_type1">
		
			<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
			
			<table>
				<caption></caption>
				<colgroup>
					<col style="width:69px" />
					<col style="width:285px" />
					<col style="width:285px" />
					<col style="width:285px" />
					<col style="width:auto" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>번호</th>
						<th>일자</th>
						<th>시간</th>
						<th>업무처리 상태</th>
						<th>메모</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><c:out value="${start - status.index}" /></td>
							<td><c:out value="${item.regDateStr}" /></td>
							<td><c:out value="${item.regTimeStr}" /></td>
							<td><c:out value="${item.statCdStr}" /></td>
							<td>
								<c:if test="${!empty item.rpMemo}">
									<a href="javascript:;" name="openHistory" class="btn_style2 list_view view_pop"
											data-rp-memo="<c:out value="${item.rpMemo}" />">보기</a>
								</c:if>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(list) eq '0'}">
						<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
					</c:if>
				</tbody>
			</table>
		
			</form>
			
		</div>
		<!-- //table_area -->
		<div id="pagination" class="pagination button_area mt22">
			<ul>
	       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
			</ul>
			<div class="right">
				<c:if test="${member.conditionCd eq '1' }">
					<a href="<c:url value="/admin/sub/${uri2}" />" class="btn btn_style1">이전</a>
					<a href="<c:url value="/admin/sub/companies/${memberSeqNo}/history/excel" />" class="btn_style1">엑셀저장</a>
				</c:if>
				<c:if test="${member.conditionCd ne '1' }">
					<a href="<c:url value="/admin/trust/${uri2}" />" class="btn btn_style1">이전</a>
					<a href="<c:url value="/admin/trust/companies/${memberSeqNo}/history/excel" />" class="btn_style1">엑셀저장</a>
				</c:if>
			</div>
		</div>

	<div id="popupHistory" class="pop_common hide">
		<div class="inner">
			<h2 class="tit">메모</h2>
			<a href="javascript:;" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont">
				<form>
				<table class="last">
					<caption>메모</caption>
					<colgroup>
						<col style="width:95px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>회사명</th>
							<td><input type="text" class="input_style1" value="<c:out value="${member.coName}" />" title="회사명" disabled /></td>
						</tr>
						<tr>
							<th>내용</th>
							<td>
								<textarea id="rpMemo" class="input_style1 height" title="내용" disabled></textarea>
							</td>
						</tr>
					</tbody>
				</table>
				</form>
				<div class="btn_area">
					<a href="javascript:;" name="popup_cancel" class="btn_style1">닫기</a>
				</div>
			</div>
		</div>
	</div>
	<!-- //pop_common -->
			
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_appl" />
	<jsp:param name="includes" value="company_tab" />
</jsp:include>
