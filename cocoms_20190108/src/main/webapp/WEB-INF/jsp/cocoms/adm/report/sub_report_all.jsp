<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<input type="hidden" id="rpx" value="contract_stat" />
<textarea id="xmldata" style="width: 100%; height: 500px; display: none;">
	<root>
		<c:forEach var="item" items="${list}" varStatus="status">
			<content>
				<!-- 기본정보-->			
				<신고번호><![CDATA[${item.appNo}]]></신고번호>
				<업체명><![CDATA[${item.coName}]]></업체명>
				<보고년도><![CDATA[${item.rptYear}]]></보고년도>
				
				<!-- 실적보고 요약-->
				<저작물수><![CDATA[${item.workSum}]]></저작물수>
				<사용료><![CDATA[${item.rentalSum}]]></사용료>
				<수수료><![CDATA[${item.chrgSum}]]></수수료>
				
				<!-- 실적보고 상세-->
				<국내저작물수> <![CDATA[${item.inWorkNum}]]></국내저작물수>
				<해외저작물수> <![CDATA[${item.outWorkNum}]]></해외저작물수>
				<저작물계><![CDATA[${item.workSum}]]></저작물계>
				<국내사용료><![CDATA[${item.inRental}]]></국내사용료>
				<해외사용료><![CDATA[${item.outRental}]]></해외사용료>
				<사용료계><![CDATA[${item.rentalSum}]]></사용료계>
				<국내수수료><![CDATA[${item.inChrg}]]></국내수수료>
				<해외수수료><![CDATA[${item.outChrg}]]></해외수수료>
				<수수료계><![CDATA[${item.chrgSum}]]></수수료계>
				<국내수수료율><![CDATA[${item.inChrgRate}]]></국내수수료율>
				<해외수수료율><![CDATA[${item.outChrgRate}]]></해외수수료율>
				<수수료율계><![CDATA[${item.avgChrgRate}]]></수수료율계>
			</content>				
		</c:forEach>
	</root>
</textarea>


<script src="<c:url value="/js/jquery-1.10.2.min.js" />"></script>

<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>

<script>
$(function () {
	
	var rpx = $('#rpx').val();
	var xmldata = $('#xmldata').text();

	printReport2(rpx, xmldata);
});
</script>