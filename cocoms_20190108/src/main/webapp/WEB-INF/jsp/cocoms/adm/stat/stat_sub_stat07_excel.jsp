<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<title>사업실적 </title>
</head>
<body>

	<table width="670" cellpadding="0" cellspacing="0" border="1">
		<tr>
			<td colspan="7"><c:out value="${fromYear}" />년 사업실적 입니다.</td>
		</tr>
		<tr align="center">
			<td ></td>
			<td  colspan="3">국내</td>
			<td colspan="3">해외</td>
		</tr>
		<tr align="center">
			<td></td>
			<td >평균사용료(천원)</td>
			<td>평균수수료(천원)</td>
			<td>수수료율(%)</td>
			<td>평균사용료(천원)</td>
			<td>평균수수료(천원)</td>
			<td>수수료율(%)</td>
		</tr>
		<c:forEach var="item" items="${list}" varStatus="status">
			<tr align="center">
				<td><c:out value="${item.tit}" /></td>
				<td><fmt:formatNumber value="${item.val1}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val2}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val3}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val4}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val5}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val6}" type="number" /></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(list) eq '0'}">
			<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
		</c:if>
	</table>

</body>
</html>