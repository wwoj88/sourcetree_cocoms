<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="SMS/이메일 관리" />
	<jsp:param name="nav2" value="이메일 발송내역" />
</jsp:include>
	
		<div class="tab_result">
			<ul>
				<c:if test="${empty param.type}">
					<c:set var="cls1" value="active" />
				</c:if>
				<c:if test="${param.type eq 'bulk'}">
					<c:set var="cls2" value="active" />
				</c:if>
				<li class="${cls1}"><a href="<c:url value="/admin/email/histories" />">개별발송</a></li>
				<li class="${cls2}"><a href="<c:url value="/admin/email/histories?type=bulk" />">단체발송</a></li>
			</ul>
		</div>
		<!-- //step_sub -->
		<div class="search_top mt35">
			<div class="table_area list_top">
				<form id="searchForm" method="GET">
				<input type="hidden" name="pageIndex" value="${page}" />
				<input type="hidden" name="type" value="${param.type}" />

					<table>
						<caption></caption>
						<colgroup>
							<col style="width:125px" />
							<col style="width:395px" />
							<col style="width:125px" />
							<col style="width:395px" />
						</colgroup>
						<tbody>
							<tr>
								<th>발송일</th>
								<td colspan="3">
									<input type="text" name="searchFromDate" class="event-date input_style1 w172" value="${search.searchFromDate}" />
									<input type="text" name="searchToDate" class="event-date2 input_style1 w172" value="${search.searchToDate}" />
								</td>
							</tr>
							<tr>
								<th>이름/회사명</th>
								<td><input type="text" name="ceoName" class="input_style1" value="${search.ceoName}" /></td>
								<th>아이디</th>
								<td><input type="text" name="loginId" class="input_style1" value="${search.loginId}" /></td>
							</tr>
						</tbody>
					</table>
					<a href="" id="search" class="btn_style2 btn_search" style="height: 84px;line-height: 84px;">검색</a>
				</form>
			</div>
			<!-- //table_area -->
		</div>
		<div class="total_top_common">
			<p class="total">검색결과 : <c:out value="${total}" />건</p>
			<div class="list_btn">
				<a href="" id="remove" class="btn_style3">선택삭제</a>
			</div>
		</div>
		<div class="table_area list list_type1">
			<table id="data">
				<caption></caption>
				<colgroup>
					<col style="width:73px" />
					<col style="width:58px" />
					<col style="width:116px" />
					<col style="width:97px" />
					<col style="width:186px" />
					<col style="width:auto" />
					<col style="width:187px" />
					<col style="width:146px" />
					<col style="width:96px" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>
							<div class="btm_form_box">
								<input type="checkbox" id="checkAll">
								<label for="checkAll">선택</label> 
							</div>
						</th>
						<th>번호</th>
						<th>발송구분</th>
						<th>이름</th>
						<th>아이디</th>
						<th>회사명</th>
						<th>발송일시 <span>(예약일시)</span></th>
						<th>전송결과</th>
						<th>상태</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td>
								<div class="btm_form_box">
									<input type="checkbox" id="checkbox_${item.smsSeqNo}" value="${item.smsSeqNo}" />
									<label for="checkbox_${item.smsSeqNo}">체크</label> 
								</div>
							</td>
							<td><c:out value="${start - status.index}" /></td>
							<td>
								<c:if test="${item.sendType eq '0'}">
									자동
								</c:if>
								<c:if test="${item.sendType ne '0'}">
									수동
								</c:if>
							</td>
							<td>
								<c:forEach var="smsMember" items="${item.smsMemberList}" varStatus="status2">
									<c:if test="${status2.index eq 0}">
										<c:out value="${smsMember.ceoName}" />
									</c:if>
								</c:forEach>
								<c:if test="${fn:length(item.smsMemberList) gt 1}">
									외 ${fn:length(item.smsMemberList)} 건
								</c:if>
							</td>
							<td>
								<a href="<c:url value="/admin/email/histories/${item.smsSeqNo}?type=${param.type}&pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />">
									<c:forEach var="smsMember" items="${item.smsMemberList}" varStatus="status2">
										<c:if test="${status2.index eq 0}">
											<c:out value="${smsMember.loginId}" />
										</c:if>
									</c:forEach>
									<c:if test="${fn:length(item.smsMemberList) gt 1}">
										외 ${fn:length(item.smsMemberList)} 건
									</c:if>
								</a>
							</td>
							<td>
								<c:forEach var="smsMember" items="${item.smsMemberList}" varStatus="status2">
									<c:if test="${status2.index eq 0}">
										<c:out value="${smsMember.coName}" />
									</c:if>
								</c:forEach>
								<c:if test="${fn:length(item.smsMemberList) gt 1}">
									외 ${fn:length(item.smsMemberList)} 건
								</c:if>
							</td>
							<td>
								<c:out value="${item.sendDateStr}" />
								<c:out value="${item.sendTimeStr}" />
							 </td>
							<td>
								<c:if test="${item.sendYn eq 'Y'}">
									<span class="txt_blue">발송완료</span>
								</c:if>
								<c:if test="${item.sendYn eq 'N'}">
									<span class="txt_blue">발송대기중</span>
								</c:if>
							</td>
							<td>
								<c:if test="${item.sendType eq '1'}">
									즉시
								</c:if>
								<c:if test="${item.sendType eq '2'}">
									예약
								</c:if>							
							</td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(list) eq '0'}">
						<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
					</c:if>					
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<div id="pagination" class="pagination">
       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_sms" />
</jsp:include>
