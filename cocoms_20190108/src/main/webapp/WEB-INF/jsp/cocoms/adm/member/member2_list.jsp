<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="회원관리" />
	<jsp:param name="nav2" value="사업자회원" />
</jsp:include>

		<div class="search_top">
			<div class="table_area list_top">
				<form id="searchForm" method="GET">
				<input type="hidden" name="pageIndex" value="${page}" />
				<input type="hidden" name="coGubun" value="${search.coGubun}" />
				
					<table>
						<caption> 사업자회원</caption>
						<colgroup>
							<col style="width:125px" />
							<col style="width:395px" />
							<col style="width:125px" />
							<col style="width:395px" />
						</colgroup>
						<tbody>
							<tr>
								<th>회원가입일</th>
								<td colspan="3">
									<input type="text" name="searchFromDate" class="event-date input_style1 w172" value="${search.searchFromDate}" /><input 
											type="text" name="searchToDate" class="event-date2 input_style1 w172" value="${search.searchToDate}" />
								</td>
							</tr>
							<tr>
								<th>업체구분</th>
								<td>
									<c:if test="${empty search.conDetailCd}"><c:set var="conDetailCd" value="checked" /></c:if>
									<c:if test="${search.conDetailCd eq '1'}"><c:set var="conDetailCd1" value="checked" /></c:if>
									<c:if test="${search.conDetailCd eq '2'}"><c:set var="conDetailCd2" value="checked" /></c:if>
									<c:if test="${search.conDetailCd eq '3'}"><c:set var="conDetailCd3" value="checked" /></c:if>
									<c:if test="${search.conDetailCd eq '4'}"><c:set var="conDetailCd4" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd" name="conDetailCd" value="" ${conDetailCd} />
										<label for="conDetailCd">전체</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd4" name="conDetailCd" value="4" ${conDetailCd4} />
										<label for="conDetailCd4">신탁</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd3" name="conDetailCd" value="3" ${conDetailCd3} />
										<label for="conDetailCd3">대리중개</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd1" name="conDetailCd" value="1" ${conDetailCd1} />
										<label for="conDetailCd1">대리</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd2" name="conDetailCd" value="2" ${conDetailCd2} />
										<label for="conDetailCd2">중개</label> 
									</div>
								</td>
								<th>상태</th>
								<td>
									<c:if test="${search.delGubun eq 'N'}"><c:set var="delGubunN" value="checked" /></c:if>
									<c:if test="${search.delGubun eq 'Y'}"><c:set var="delGubunY" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="delGubunN" name="delGubun" value="N" ${delGubunN} />
										<label for="delGubunN">사용</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="delGubunY" name="delGubun" value="Y" ${delGubunY} />
										<label for="delGubunY">탈퇴/정지</label> 
									</div>
								</td>
							</tr>							
							<tr>
								<th>검색옵션</th>
								<td colspan="3">
									<c:if test="${search.searchCondition eq 'ceoName'}">
										<c:set var="selectedCeoName" value="selected" />
									</c:if>
									<c:if test="${search.searchCondition eq 'loginId'}">
										<c:set var="selectedLoginId" value="selected" />
									</c:if>
									<c:if test="${search.searchCondition eq 'ceoTel'}">
										<c:set var="selectedCeoTel" value="selected" />
									</c:if>
									<c:if test="${search.searchCondition eq 'ceoEmail'}">
										<c:set var="selectedCeoEmail" value="selected" />
									</c:if>
									<c:if test="${search.searchCondition eq 'coName'}">
										<c:set var="selectedCoName" value="selected" />
									</c:if>
									<select name="searchCondition" class="select_style w121">
										<option value="all">전체</option>
										<option value="coName" ${selectedCoName}>업체명</option>
										<option value="ceoName" ${selectedCeoName}>이름</option>
										<option value="loginId" ${selectedLoginId}>회원ID</option>
										<option value="ceoTel" ${selectedCeoTel}>휴대전화</option>
										<option value="ceoEmail" ${selectedCeoEmail}>이메일</option>
									</select>
									<input type="text" name="searchKeyword" class="input_style1" style="width: 300px;" value="${search.searchKeyword}" />
								</td>
							</tr>
						</tbody>
					</table>
					<a href="" id="search" class="btn_style2 btn_search" style="height: 84px;line-height: 84px;">검색</a>
				</form>
			</div>
			<!-- //table_area -->
		</div>
		<div class="total_top_common">
			<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
			<div class="list_btn">
				<a href="" id="sendSms" class="btn_style2">SMS발송</a>
				<a href="" id="sendEmail" class="btn_style2">이메일발송</a>
				
				<a href="" id="remove" class="btn_style3">탈퇴처리</a>
				
				<a href="<c:url value="/admin/members${search.coGubun}/new" />" class="btn_style1">회원등록</a>

				<a href="<c:url value="/admin/members${search.coGubun}/excel" />" class="btn_style1">엑셀저장</a>
			</div>
		</div>
		<div class="table_area list list_type1 ">
			<div>
				<table id="data" summary="사업자회원">
					<caption>사업자회원</caption>
					<colgroup>
						<col style="width:64px" />
						<col style="width:41px" />
						<col style="width:120px" />
						<col style="width:90px" />
						<col style="width:207px" />
						<col style="width:116px" />
						<col style="width:90px" />
						<col style="width:107px" />
						<col style="width:100px" />
						<%-- <col style="width:70px" /> --%>
						<col style="width:129px" />
						<col style="width:129px" />
						<col style="width:97px" />
						<col style="width:146px" />
						<col style="width:100px" />
						<col style="width:100px" />
						<col style="width:58px" />
					</colgroup>
					<thead>
						<tr>
							<th>
								<div class="btm_form_box">
									<input type="checkbox" id="checkAll">
									<label for="checkAll">선택</label> 
								</div>
							</th>
							<th>번호</th>
							<th>회원구분</th>
							<th>업체구분</th>
							<th>회사명</th>
							<th>아이디</th>
							<th>대표자명</th>
							<th>사업자등록번호</th>
							<th>법인등록번호</th>
							<!-- <th>담당자명</th> -->
							<th>휴대전화</th>
							<th>이메일</th>
							<th>가입일자</th>
							<th>최종로그인</th>
							<th>SMS수신</th>
							<th>Email수신</th>
							<th>상태</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr>
								<td>
									<div class="btm_form_box">
										<input type="checkbox" id="checkbox_${item.loginId}" value="${item.loginId}" 
												data-member-seq-no="${item.memberSeqNo}" data-tel="${item.sms}" data-email="${item.ceoEmail}" data-agree="${item.smsAgree}" />
										<label for="checkbox_${item.loginId}">체크</label> 
									</div>
								</td>
								<td><c:out value="${start - status.index}" /></td>
								<td>사업자회원</td>
								<td>
									<c:if test="${item.conditionCd eq '1'}">
										<c:if test="${item.conDetailCd eq '1'}">
											대리
										</c:if>
										<c:if test="${item.conDetailCd eq '2'}">
											중계
										</c:if>
										<c:if test="${item.conDetailCd eq '3'}">
											대리중계
										</c:if>
									</c:if>
									<c:if test="${item.conditionCd eq '2'}">
										신탁
									</c:if>
								</td>
								<td><c:out value="${item.coName}" /></td>
								<td><a href="<c:url value="/admin/members${search.coGubun}/${item.loginId}?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}&searchFromDate=${search.searchFromDate}&searchToDate=${search.searchToDate}" />"><c:out value="${item.loginId}" /></a></td>
								<td><c:out value="${item.ceoName}" /></td>
								<td><c:out value="${item.coNoStr}" /></td>
								<td><c:out value="${item.bubNoStr}" /></td>
								<!-- <td></td> -->
								<td><c:out value="${item.ceoMobile}" /></td>
								<td><c:out value="${item.ceoEmail}" /></td>
								<td><c:out value="${item.regDateStr}" /></td>
								<td><c:out value="${item.lastLoginDateStr}" /></td>
								<td>
									<c:choose>
										<c:when test="${item.smsAgree eq 'Y' and !empty item.sms}">Y</c:when>
										<c:otherwise>N</c:otherwise>
									</c:choose>
								</td>
								<td>
									<c:choose>
										<c:when test="${item.smsAgree eq 'Y' and !empty item.ceoEmail}">Y</c:when>
										<c:otherwise>N</c:otherwise>
									</c:choose>
								</td>
								<td>
									<c:if test="${item.delGubun eq 'N'}">
										사용
									</c:if>
									<c:if test="${item.delGubun ne 'N'}">
										삭제
									</c:if>
								</td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(list) eq '0'}">
							<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //table_area -->
		<div id="pagination" class="pagination">
       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
		</div>
		
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/sms_popup.jsp" flush="true" />

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_member" />
	<jsp:param name="includes" value="sms_popup" />
</jsp:include>
