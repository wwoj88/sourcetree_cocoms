<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="정보센터" />
	<jsp:param name="nav2" value="관련법제도" />
</jsp:include>

		<div class="total_top_common pt0">
			<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
			<div class="list_btn">
				<a href="<c:url value="/admin/law_new" />" class="btn_style2">관련법제도 등록</a>
				<!-- <a href="" class="btn_style1">선택삭제</a> -->
			</div>
		</div>
		<div class="table_area list board list_type1">
			<table id="data" summary="law">
				<caption>관련법제도</caption>
				<colgroup>
					<%-- <col style="width:70px" /> --%>
					<col style="width:70px" />
					<col style="width:auto" />
					<col style="width:93px" />
					<col style="width:137px" />
					<col style="width:137px" />
					<col style="width:88px" />
					<col style="width:88px" />
				</colgroup>
				<thead>
					<tr> 	 	 	
						<!-- <th>
							<div class="btm_form_box">
								<input type="checkbox" id="checkAll">
								<label for="checkAll">선택</label> 
							</div>
						</th> -->
						<th>번호</th>
						<th>제목</th>
						<th>첨부파일</th>
						<th>작성자</th>
						<th>작성일</th>
						<th>조회수</th>
						<th>다운로드수</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<%-- <td>
								<div class="btm_form_box">
									<input type="checkbox" id="checkbox_${item.boardSeqNo}">
									<label for="checkbox_${item.boardSeqNo}">체크</label> 
								</div>
							</td> --%>
							<td><c:out value="${start - status.index}" /></td>
							<td class="subject">
								<a href="<c:url value="/admin/law/${item.boardSeqNo}" />"><c:out value="${item.title}" /></a>
								<!--   
								<strong class="new">NEW</strong>
								 -->
							</td>
							<td>
								<c:if test="${fn:length(item.boardFileDataList) gt '0'}">
									<span title="fileDown" class="fileDown board">파일 다운로드</span>
								</c:if>
							</td>
							<td><c:out value="${item.regName}" /></td>
							<td><c:out value="${item.regDateStr}" /></td>
							<td><c:out value="${item.hit}" /></td>
							<td><c:out value="${item.download_hit}" /></td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(list) eq '0'}">
						<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
					</c:if>
				</tbody>
			</table>
		</div>
		
		<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
		
			<!-- //table_area -->
			<div id="pagination" class="pagination">
	       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
			</div>
			<div class="button_area">
				<div class="search_form_common">
					<input type="hidden" name="searchCondition" value="all" />
					<input type="text" name="searchKeyword" class="input_style1" value="${data.searchKeyword}" placeholder="검색어를 입력해 주세요" />
					<input type="submit" id="search" class="btn btn_style2" value="검색" />
				</div>
				<!-- //search_form_common -->
			</div>
	
		
		</form>
	

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_notice" />
</jsp:include>
