<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="신탁단체" />
	<jsp:param name="title" value="${member.coName}" /> 
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${memberSeqNo}" />

		<form id="form" method="post" action="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab3" />">

		<input type="hidden" name="trustInfoSeqno" value="${report.trustInfoSeqno}" />
<input type="hidden" name="trust" value="1"/>
		<input type="hidden" name="filepath5" value="${report.filepath5}" />
		<input type="hidden" name="filepath6" value="${report.filepath6}" />
		<input type="hidden" name="filepath7" value="${report.filepath7}" />
		<input type="hidden" name="filepath8" value="${report.filepath8}" />
		<input type="hidden" name="filepath9" value="${report.filepath9}" />

		<c:if test="${regist eq true}">
			<input type="hidden" name="returnUrl" value="" />
		</c:if>
			
		<div class="tab_result">
			<ul>
				<c:if test="${regist ne true}">
					<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}" />">실적정보</a></li>
					<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab2" />">저작권료 징수분배 내역</a></li>
					<li class="active"><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab3" />">사업계획</a></li>
				</c:if>
				<c:if test="${regist eq true}">
					<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/new" />">실적정보</a></li>
					<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab2/new" />">저작권료 징수분배 내역</a></li>
					<li class="active"><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab3/new" />">사업계획</a></li>
				</c:if>
			</ul>
		</div>
		<!-- //step_sub -->
		<h2 class="tit_c_s">1. 사업목표<span>(<c:out value="${year+1}" />년)</span></h2>
		<textarea name="businessGoal" class="input_style1 height_bottom_n mb0">${report.businessGoal}</textarea>
		<h2 class="tit_c_s">2. 추친방향<span>(<c:out value="${year+1}" />년)</span></h2>
		<textarea name="prop" class="input_style1 height_bottom_n mb0">${report.prop}</textarea>
		<h2 class="tit_c_s">3. 첨부자료<span>(<c:out value="${year+1}" />년)</span></h2>
		<div class="list_finl_link list_finl_link2 bg">
			<ul>
				<li><p>당해연도 사업계획서</p>
					<div class="list_file">
						<input type="text" name="filename5" class="input_style1 prj_file" value="${report.filename5}" placeholdere="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file5" class="insert_file_target" data-ref-name="filename5" data-ref-name2="filepath5" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file5" class="form_btn insert_file btn_style1" data-ref-name="filename5" data-ref-name2="filepath5">파일삭제</button>
					</div>
				</li>
				<li><p>사용료 징수.분배계획</p>
					<div class="list_file">
						<input type="text" name="filename6" class="input_style1 prj_file" value="${report.filename6}" placeholdere="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file6" class="insert_file_target" data-ref-name="filename6" data-ref-name2="filepath6" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file6" class="form_btn insert_file btn_style1" data-ref-name="filename6" data-ref-name2="filepath6">파일삭제</button>
					</div>
				</li>
				<li><p>보상금 징수.분배계획<span>(첨부자료없음)</span></p>
					<div class="list_file">
						<input type="text" name="filename7" class="input_style1 prj_file" value="${report.filename7}" placeholdere="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file7" class="insert_file_target" data-ref-name="filename7" data-ref-name2="filepath7" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file7" class="form_btn insert_file btn_style1" data-ref-name="filename7" data-ref-name2="filepath7">파일삭제</button>
					</div>
				</li>
				<li><p>예산편성안</p>
					<div class="list_file">
						<input type="text" name="filename8" class="input_style1 prj_file" value="${report.filename8}" placeholdere="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file8" class="insert_file_target" data-ref-name="filename8" data-ref-name2="filepath8" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file8" class="form_btn insert_file btn_style1" data-ref-name="filename8" data-ref-name2="filepath8">파일삭제</button>
					</div>
				</li>
				<li><p>정기총회 자료집 </p>
					<div class="list_file">
						<input type="text" name="filename9" class="input_style1 prj_file" value="${report.filename9}" placeholdere="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file9" class="insert_file_target" data-ref-name="filename9" data-ref-name2="filepath9" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file9" class="form_btn insert_file btn_style1" data-ref-name="filename9" data-ref-name2="filepath9">파일삭제</button>
					</div>
				</li>
			</ul>
		</div>
		<!-- //list_finl_link -->
		<div class="button_area">
			<a href="" id="save" class="btn btn_style2">저장</a>
			<div class="right">
				<a href="<c:url value="/admin/reports/trust/${member.memberSeqNo}" />" class="btn btn_style1">목록</a>
			</div>
		</div>
		
		</form>
				
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_report" />
</jsp:include>
