<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="업체현황" />
	<jsp:param name="nav2" value="통계조회" />
</jsp:include>
	
		<div class="table_area list_top list_type3">
			<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/stats_search.jsp" flush="true" />
		</div>
		<!-- //table_area -->
		<h2 class="tit_c_s"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 
		<strong class="txt_blue">판매용 음반 보상금 - 음반제작자</strong></h2>
		<div class="table_area list list_type2 scroll">
			<div class="inner">
				<table id="">
					<caption></caption>
					<colgroup>
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
					</colgroup>
					<thead>
						<tr> 	 
							<th>구분</th>
							<th>이용단체</th>
							<th>징수액 <span>(단위 : 천원)</span></th>
							<th>분배액 <span>(단위 : 천원)</span></th>
							<th>미분배액 <span>(단위 : 천원)</span></th>
							<th>수수료 <span>(단위 : 천원)</span></th>
							<th>수수료율 <span>(단위 : %)</span></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr>
								<td>
									<c:if test="${item.tit eq '1'}">
										판매용음반의 방송보상금
									</c:if>
									<c:if test="${item.tit eq '2'}">
										음반의 디음송보상금
									</c:if>
									<c:if test="${item.tit eq '3'}">
										판매용 음반의 공연보상금
									</c:if>
								</td>
								<td><fmt:formatNumber value="${item.organization}" type="number" /></td>
								<td><fmt:formatNumber value="${item.collected}" type="number" /></td>
								<td><fmt:formatNumber value="${item.dividend}" type="number" /></td>
								<td><fmt:formatNumber value="${item.dividendOff}" type="number" /></td>
								<td><fmt:formatNumber value="${item.chrg}" type="number" /></td>
								<td><fmt:formatNumber value="${item.chrgRate}" type="number" /></td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(list) eq '0'}">
							<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //table_area -->
		<div class="button_area mt25">
			<div class="right">
				<a href="<c:url value="/admin/status/stats/save?conditionCd=${conditionCd}&fromYear=${fromYear}&toYear=${toYear}&type=${type}" />" id="excel" class="btn btn_style2">엑셀출력</a>
				<%-- 
				<a href="" id="list" class="btn btn_style1">목록</a>
				--%>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_stat" />
	<jsp:param name="includes" value="stats_search" />
</jsp:include>
