<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="대리중개업체" />
</jsp:include>

		<form id="searchForm" method="GET">
		<input type="hidden" name="pageIndex" value="${page}" />
	
		<div class="search_top">
			<div class="table_area list_top">
				
				<table>
					<caption> 대리중개업체</caption>
					<colgroup>
						<%-- <col style="width:125px" /> --%>
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<%-- <tr>
							<th>실적보고 등록기간</th>
							<td>
								<select class="select_style">
									<c:forEach var="item" begin="1" end="12" varStatus="status">
										<c:set var="str" value="${item}" />
										<c:if test="${item lt 10}">
											<c:set var="str" value="0${item}" />
										</c:if>
										<option value="">${str}</option>
									</c:forEach>
								</select>
								월
								<select class="select_style">
									<c:forEach var="item" begin="1" end="31" varStatus="status">
										<c:set var="str" value="${item}" />
										<c:if test="${item lt 10}">
											<c:set var="str" value="0${item}" />
										</c:if>
										<option value="">${str}</option>
									</c:forEach>
								</select>
								일
								~
								<select class="select_style">
									<c:forEach var="item" begin="1" end="12" varStatus="status">
										<c:set var="str" value="${item}" />
										<c:if test="${item lt 10}">
											<c:set var="str" value="0${item}" />
										</c:if>
										<option value="">${str}</option>
									</c:forEach>
								</select>
								월
								<select class="select_style">
									<c:forEach var="item" begin="1" end="31" varStatus="status">
										<c:set var="str" value="${item}" />
										<c:if test="${item lt 10}">
											<c:set var="str" value="0${item}" />
										</c:if>
										<option value="">${str}</option>
									</c:forEach>
								</select>
								일									
							</td>
						</tr> --%>
						
						
						<tr>
							<th>업체명</th>
							<td><input type="text" name="coName" class="input_style1" style="width:362px" value="${data.coName}" /></td>
						</tr>
					</tbody>
				</table>
				<a href="" id="search" class="btn_style2 btn_search" style="height: 34px;line-height: 34px;">검색</a>
			</div>
			<!-- //table_area -->
		</div>
		<div class="total_top_common">
			<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
			<c:if test="${sort ne 'appNo'}"><c:set var="orderNone" value="selected" /></c:if>
			<c:if test="${sort eq 'appNo'}"><c:set var="orderAppNo" value="selected" /></c:if>
			<select name="sort" class="select_style">
				<option value=""      ${orderNone} >실적보고</option>
				<option value="appNo" ${orderAppNo}>신고번호</option>
			</select>
		</div>

		</form>
		
		<div class="table_area list list_type1">
			<table>
				<caption> 실적보고</caption>
				<colgroup>
					<col style="width:183px" />
					<col style="width:auto" />
					<col style="width:128px" />
					<col style="width:128px" />
					<col style="width:128px" />
					<col style="width:128px" />
					<col style="width:128px" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>신고번호</th>
						<th>업체명</th>
						<c:forEach var="item" items="${yearList2}" varStatus="status">
							<th><c:out value="${item}" />년</th>
						</c:forEach>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td>
								<c:if test="${!empty item.appNo2}">
									신고 ${item.appNoStr}
								</c:if>
							</td>
							<td><a href="<c:url value="/admin/reports/sub/${item.memberSeqNo}" />"><c:out value="${item.coName}"  escapeXml="false"/></a></td>
							<td>
								<c:if test="${item.year5 eq '1'}">O</c:if>
								<c:if test="${item.year5 ne '1'}">X</c:if>
							</td>
							<td>
								<c:if test="${item.year4 eq '1'}">O</c:if>
								<c:if test="${item.year4 ne '1'}">X</c:if>
							</td>
							<td>
								<c:if test="${item.year3 eq '1'}">O</c:if>
								<c:if test="${item.year3 ne '1'}">X</c:if>
							</td>
							<td>
								<c:if test="${item.year2 eq '1'}">O</c:if>
								<c:if test="${item.year2 ne '1'}">X</c:if>
							</td>
							<td>
								<c:if test="${item.year1 eq '1'}">O</c:if>
								<c:if test="${item.year1 ne '1'}">X</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<div id="pagination" class="pagination">
       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
		</div>
		<div class="button_area">
			<select id="year" class="select_style pr">
				<c:forEach var="item" items="${yearList2}" varStatus="status">
					<c:set var="selected" value="" />
					<c:if test="${status.last}">
						<c:set var="selected" value="selected" />
					</c:if>
					<option value="${item}" ${selected}><c:out value="${item}" /></option>
				</c:forEach>				
			</select>
			<a href="" id="report_all" class="btn btn_style2">년도별 전체 실적보고</a>
			<hr class="line"></hr>
			<a href="" id="save_excel_1" class="btn btn_style1">실적보고 등록업체 엑셀출력</a>
			<a href="" id="save_excel_0" class="btn btn_style1">실적보고 미등록업체 엑셀출력</a>
			
			<a href="<c:url value="/admin/reports/sub/excel" />" class="btn_style1">엑셀저장</a>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_report" />
</jsp:include>
