<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="업체현황" />
	<jsp:param name="nav2" value="통계조회" />
</jsp:include>
	
		<div class="table_area list_top list_type3">
			<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/stats_search.jsp" flush="true" />
		</div>
		
		<h2 class="tit_c_s"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 
		<strong class="txt_blue">직원수별 대리중개업체수</strong></h2>
		<div class="table_area list list_type2 scroll">
			<div class="inner">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
					</colgroup>
					<thead>
						<tr> 	 
							<th></th>
							<th>0~5</th>
							<th>6~10</th>
							<th>11~30</th>
							<th>31~50</th>
							<th>51~100</th>
							<th>101~200</th>
							<th>201~</th>
							<th>계</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr>
								<td>
									
     								<c:if test="${!status.last }"><a href="<c:url value="/admin/status/stats/detail?conditionCd=${conditionCd}&fromYear=${item.tit}&toYear=${item.tit}&type=1" />" >${item.tit}</a></c:if>
     								<c:if test="${status.last }"><c:out value="${item.tit}" /></c:if>

								
								
								</td>
								<%-- <c:out value="${item.tit}" /> --%>
								<td><fmt:formatNumber value="${item.val1}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val2}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val3}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val4}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val5}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val6}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val7}" type="number" /></td>
								<td><fmt:formatNumber value="${item.sum}" type="number" /></td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(list) eq '0'}">
							<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //table_area -->
		<div class="button_area mt25">
			<div class="right">
				<a href="<c:url value="/admin/status/stats/save?conditionCd=${conditionCd}&fromYear=${fromYear}&toYear=${toYear}&type=${type}" />" id="excel" class="btn btn_style2">엑셀출력</a>
				<%-- 
				<a href="" id="list" class="btn btn_style1">목록</a>
				--%>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_stat" />
	<jsp:param name="includes" value="stats_search" />
</jsp:include>
