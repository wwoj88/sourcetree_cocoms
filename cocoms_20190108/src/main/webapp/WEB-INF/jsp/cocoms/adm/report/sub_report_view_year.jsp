<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="대리중개업체" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${memberSeqNo}" />
	
	<div id="print_area">
	
		<h2 class="tit_c_s" style="padding-top:0"><c:out value="${year}" />년 <c:out value="${member.coName}" escapeXml="false"/> 업무보고 정보</h2>
		<div class="table_area list_top">
			<form>
				<table>
					<caption> 대리중개업체</caption>
					<colgroup>
						<col style="width:156px" />
						<col style="width:427px" />
						<col style="width:156px" />
						<col style="width:427px" />
					</colgroup>
					<tbody>
						<tr>
							<th>작성자</th>
							<td>
								<c:out value="${report.regName}" />
							</td>
							<th>직위</th>
							<td>
								<c:out value="${report.regPosi}" />
							</td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td>
								<c:out value="${report.regTel}" />
							</td>
							<th>이메일</th>
							<td>
								<c:out value="${report.regEmail}" />
							</td>
						</tr>
						<tr>
							<th>직원수</th>
							<td>
								 <c:out value="${report.staffNum}" />명
							</td>
							<th>상장여부</th>
							<td>
								<c:out value="${report.listingCdStr}" />
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<!-- //table_area -->
		<h2 class="tit_c_s" style="padding-top:41px"><c:out value="${year}" />년 실적보고 상세</h2>
		<div class="box_top_common date">
			<p class="txt_dot">년도 선택</p>
			<select name="rptYear" class="select_style">
				<c:forEach var="item" items="${yearList}" varStatus="status">
					<c:set var="selected" value="" />
					<c:if test="${item eq year}">
						<c:set var="selected" value="selected" />
					</c:if>
					<option value="${item}" ${selected}><c:out value="${item}" /></option>
				</c:forEach>
			</select>
			<p class="txt_red">※ 연극, 건축, 도형, 컴퓨터프로그램은 2009년 7월 24일부로 추가된 저작물종류입니다.</p>
		</div>
		<!-- //box_top_common -->
		<h2 class="tit_box tit_box_n">저작물수</h2>
		<div class="table_area list list_type2">
			<table>
				<caption>저작물수</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>종류 / 권리종류</th>
						<th>국내</th>
						<th>해외</th>
						<th>소계</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><c:out value="${item.writingKindDesc}" /><c:if test="${item.permKindDesc ne '기타' and  !empty item.permKindDesc}">/ <c:out value="${item.permKindDesc}" /></c:if></td>
							<td><c:out value="${item.inWorkNum}" /></td>
							<td><c:out value="${item.outWorkNum}" /></td>
							<td><c:out value="${item.workSum}" /></td>
						</tr>
					</c:forEach>
					<tr class="bg_total">
						<td>합계</td>
						<td><c:out value="${sum.inWorkNum}" /></td>
						<td><c:out value="${sum.outWorkNum}" /></td>
						<td><c:out value="${sum.workSum}" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_box tit_box_n">첨부파일</h2>
		<div class="list_finl_link">
			<ul>
				<c:if test="${!empty report.actrstMaskName}">
					<li>국내 관리저작물 목록<a href="<c:url value="/download?filename=${report.actrstMaskName}" />" target="_blank"><c:out value="${report.actrstMaskName}" /></a></li>
				</c:if>
				<c:if test="${!empty report.planMaskName}">
					<li>해외 관리저작물 목록<a href="<c:url value="/download?filename=${report.planMaskName}" />" target="_blank"><c:out value="${report.planMaskName}" /></a></li>
				</c:if>
			</ul>
		</div>
		<!-- //list_finl_link -->
		<h2 class="tit_box tit_box_n">사용료 및 수수료</h2>
		<div class="table_area list list_type2">
			<table>
				<caption> 사용료 및 수수료</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>종류 / 권리종류</th>
						<th colspan="3">사용료 <span style="color:red">(단위 : 천원)</span></th>
						<th colspan="3">수수료 <span style="color:red">(단위 : 천원)</span></th>
						<th colspan="3">수수료율 <span>(단위 : %)</span></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>-</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
					</tr>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><c:out value="${item.writingKindDesc}" /> <c:if test="${item.permKindDesc ne '기타' and  !empty item.permKindDesc}">/ <c:out value="${item.permKindDesc}" /></c:if></td>
							<td><fmt:formatNumber value="${item.inRental}" type="number" /></td>
							<td><fmt:formatNumber value="${item.outRental}" type="number" /></td>
							<td><fmt:formatNumber value="${item.rentalSum}" type="number" /></td>
							<td><fmt:formatNumber value="${item.inChrg}" type="number" /></td>
							<td><fmt:formatNumber value="${item.outChrg}" type="number" /></td>
							<td><fmt:formatNumber value="${item.chrgSum}" type="number" /></td>
							<td><fmt:formatNumber value="${item.inChrgRate}" type="number" /></td>
							<td><fmt:formatNumber value="${item.outChrgRate}" type="number" /></td>
							<td><fmt:formatNumber value="${item.avgChrgRate}" type="number" /></td>
						</tr>
					</c:forEach>
					<tr class="bg_total">
						<td>합계</td>
						<td><fmt:formatNumber value="${sum.inRental}" type="number" /></td>
						<td><fmt:formatNumber value="${sum.outRental}" type="number" /></td>
						<td><fmt:formatNumber value="${sum.rentalSum}" type="number" /></td>
						<td><fmt:formatNumber value="${sum.inChrg}" type="number" /></td>
						<td><fmt:formatNumber value="${sum.outChrg}" type="number" /></td>
						<td><fmt:formatNumber value="${sum.chrgSum}" type="number" /></td>
<%-- 						<td><fmt:formatNumber value="${sum.inChrgRate}" type="number" /></td>
						<td><fmt:formatNumber value="${sum.outChrgRate}" type="number" /></td>
						<td><fmt:formatNumber value="${sum.avgChrgRate}" type="number" /></td> --%>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->

		<h2 class="tit_box tit_box_n">특이사항</h2>
		<textarea class="input_style1 height_bottom_n" readonly><c:out value="${report.memo}" /></textarea>
		
	</div>
		
		<div class="button_area center">
			<a href="<c:url value="/admin/reports/sub/${memberSeqNo}/years/${year}/edit" />" class="btn btn_style2">수정</a>
			<div class="right">
				<a href="<c:url value="/admin/reports/sub/${memberSeqNo}/years/${year}/save" />" class="btn btn_style2" target="_blank">엑셀출력</a>
				<a href="" id="print" class="btn btn_style2">인쇄</a>
				<a href="<c:url value="/admin/reports/sub/${memberSeqNo}" />" class="btn btn_style1">목록</a>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_report" />
</jsp:include>
