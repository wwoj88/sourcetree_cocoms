<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="${title1}" />
	<jsp:param name="nav2" value="${title2}" />
	<jsp:param name="title" value="NONE" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />
<input type="hidden" id="conditionCd" value="${data.conditionCd}" />
	
		<!-- //step_sub -->
		<h2 class="tit_c">${data.coName}<a href="<c:url value="/admin/members${data.coGubunCd}/${data.loginId}/edit" />" class="btn_style2 right">회원정보 변경</a></h2>
		<div class="form_apply form_apply_style1">
			<div class="aside">
				<div class="tab_type">
					<ul>
						<li class="active"><a href="" id="tab1">회원정보</a></li>
						<li><a href="" id="tab2">대표자정보</a></li>
					</ul>
				</div>
				<div id="area1" class="table_area">
					<table>
						<caption> 업체현황</caption>
						<tbody>
							<tr>
								<th>
									 회원구분
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${data.coGubunCd eq '1'}"><c:set var="coGubunCd1Checked" value="checked" /></c:if>
									<c:if test="${data.coGubunCd eq '2'}"><c:set var="coGubunCd2Checked" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd1" name="coGubunCd" value="1" ${coGubunCd1Checked} disabled />
										<label for="coGubunCd1">개인사업자</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd2" name="coGubunCd" value="2" ${coGubunCd2Checked} disabled />
										<label for="coGubunCd2">법인사업자</label> 
									</div>
								</td>
							</tr>
							<tr>
								<th>
									 사업자 등록번호
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${data.coNo1}" title="사업자번호 첫번째자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num1 disabled" value="${data.coNo2}" title="사업자번호 두째자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num2 disabled" value="${data.coNo3}" title="사업자번호 세째자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									법인 등록번호
								</th>
							</tr>
							<tr>
								<td>
									<%-- <input type="text" class="input_style1 num3 disabled" value="${data.bubNoStr}" disabled />
									<br>
									 --%>
									<input type="text" class="input_style1 num2 disabled" value="${fn:substring(data.bubNoStr,0,6)}" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num2 disabled" value="${fn:substring(data.bubNoStr,7,15)}" disabled />
								</td>
							</tr>
							<tr>
								<th>
									주소 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 address1" value="${data.trustZipcode}" placeholder="" disabled />
									<input type="text" class="input_style1 margin" value="${data.trustSido} ${data.trustGugun} ${data.trustDong} ${data.trustBunji}" placeholder="" disabled />
									<input type="text" class="input_style1" value="${data.trustDetailAddr}" placeholder="" disabled />
								</td>
							</tr>
							<tr>
								<th>
									전화번호 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${data.trustTel1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustTel2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustTel3}" placeholder="" title="전화번호 앞자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									팩스번호 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${data.trustFax1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustFax2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustFax3}" placeholder="" title="전화번호 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									홈페이지 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 disabled" value="${data.trustUrl}" placeholder="" disabled />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="area2" class="table_area" style="display: none;">
					<table>
						<caption>대표자정보</caption>
						<tbody>
							<tr>
								<th>
									  대표자명
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 disabled" value="${data.ceoName}" placeholder="" disabled />
								</td>
							</tr>
							<tr>
								<th>
									주민등록번호
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 resident disabled" 
											value="${data.ceoRegNo1}" placeholder="" disabled /><span>-</span><input type="text" class="input_style1 resident disabled" value="${data.ceoRegNo2}" placeholder="" disabled />
								</td>
							</tr>
							<tr>
								<th>
									주소 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 address1 disabled" value="${data.ceoZipcode}" placeholder="" disabled />
									<input type="text" class="input_style1 margin disabled" value="${data.ceoSido} ${data.ceoGugun} ${data.ceoDong} ${data.ceoBunji}" placeholder="" disabled />
									<input type="text" class="input_style1 disabled" value="${data.ceoDetailAddr}" placeholder="" disabled />
								</td>
							</tr>
							<tr>
								<th>
									휴대전화번호 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${data.ceoMobile1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.ceoMobile2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.ceoMobile3}" placeholder="" title="전화번호 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									이메일
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 resident disabled" 
											value="${data.ceoEmail1}" placeholder="" disabled /><span>@</span><input type="text" class="input_style1 resident disabled" 
											value="${data.ceoEmail2}"placeholder="" disabled />
								</td>
							</tr>
							<tr>
								<th>
									 SMS수신동의 
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${data.smsAgree eq 'Y'}"><c:set var="smsAgreeYChecked" value="checked" /></c:if>
									<c:if test="${data.smsAgree ne 'Y'}"><c:set var="smsAgreeNChecked" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" ${smsAgreeYChecked} disabled />
										<label for="">동의</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" ${smsAgreeNChecked} disabled />
										<label for="">동의안함</label> 
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
			</div>
			<!-- //aside -->
			<div class="cont">
				<div class="table_area">
					<table>
						<caption>허가/신고정보</caption>
						<thead>
							<tr> 	 	 	
								<th>허가/신고정보</th>
							</tr>
						</thead>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="cell cell_type1">
					<p class="tit">허가/신고번호</p>
					<p class="txt">
						<c:out value="${data.appNoStr}" />
						
						<c:if test="${data.conditionCd eq '1'}">
							<c:if test="${data.down3 == false and data.down1}">
								<a href="<c:url value="/admin/sub/companies/${data.memberSeqNo}/docs/1" />" class="btn_style2 file_s ml">신고서</a>
							</c:if>
							<c:if test="${data.down4 == false and data.down2}">
								<a href="<c:url value="/admin/sub/companies/${data.memberSeqNo}/docs/2" />" class="btn_style2 file_s">신고증</a>
							</c:if>
							<c:if test="${data.down3}">
								<a href="<c:url value="/admin/sub/companies/${data.memberSeqNo}/docs/3" />" class="btn_style2 file_s ml">신고서</a>
							</c:if>
							<c:if test="${data.down4}">
								<a href="<c:url value="/admin/sub/companies/${data.memberSeqNo}/docs/4" />" class="btn_style2 file_s">신고증</a>
							</c:if>
						</c:if>
						<c:if test="${data.conditionCd eq '2'}">
							<c:if test="${data.down3 == false and data.down1}">
								<a href="<c:url value="/admin/trust/companies/${data.memberSeqNo}/docs/1" />" class="btn_style2 file_s ml">신고서</a>
							</c:if>
							<c:if test="${data.down4 == false and data.down2}">
								<a href="<c:url value="/admin/trust/companies/${data.memberSeqNo}/docs/2" />" class="btn_style2 file_s">신고증</a>
							</c:if>
							<c:if test="${data.down3}">
								<a href="<c:url value="/admin/trust/companies/${data.memberSeqNo}/docs/3" />" class="btn_style2 file_s ml">신고서</a>
							</c:if>
							<c:if test="${data.down4}">
								<a href="<c:url value="/admin/trust/companies/${data.memberSeqNo}/docs/4" />" class="btn_style2 file_s">신고증</a>
							</c:if>
						</c:if>
					</p>
				</div>
				<!-- //cell -->
				<div class="cell cell_type1">
					<p class="tit">허가/신고일</p>
					<p class="txt">${data.appRegDateStr}</p>
				</div>
				<!-- //cell -->
				<div class="cell cell_type1">
					<p class="tit">허가/신고상태</p>
					<p class="txt">${data.statCdStr}</p>
				</div>
				<!-- //cell -->
			<c:if test= "${!empty data.conDetailCd}">
					<div class="cell cell_type1">
					<p class="tit">취급하고자 하는 업무의 내용</p>
					<c:if test="${data.conDetailCd eq '1'}"><p class="txt">대리</p></c:if><c:if test="${data.conDetailCd eq '2'}"><p class="txt">중개</p></c:if><c:if test="${data.conDetailCd eq '3'}"><p class="txt">대리중개</p></c:if>
				</div>
			</c:if>
				<!-- //cell -->
				<div class="cell cell_type1">
					<p class="tit">취급하고자 하는 저작물 종류/권리</p>
					<ul class="list_dot">
						<c:forEach var="item" items="${writingKindList}" varStatus="status">
							<c:if test="${!empty item.writingKindStr}">
							   	<c:set var="virgin" value="false" />
								<li>
									<strong>${item.writingKindStr} : </strong>
									<c:forEach var="item2" items="${permKindList}" varStatus="status">
										<c:if test="${item2.writingKind eq item.writingKind}">
											<c:if test="${virgin eq true}">,${item2.permKindStr}</c:if>
											<c:if test="${virgin eq false}">${item2.permKindStr}<c:set var="virgin" value="true" /></c:if>
										</c:if>
									</c:forEach>
								</li>
							</c:if>
						</c:forEach>
					</ul>
				</div>
				<!-- //cell -->
			</div>
			<!-- //cont -->
		</div>
		<!-- //form_apply -->
		<div class="btn_area">
			<div class="left">

				<c:if test="${data.statCd eq '1' or data.statCd eq '3'}">
					<a href="" id="changeStatCd_${data.statCd}9" class="btn_style3">접수</a>
				</c:if>

				<c:if test="${data.statCd eq '19' or (data.statCd eq '8' and data.preStatCd eq '19')}">
					<a href="" id="changeStatCd_2" class="btn_style3">신청처리</a>
				</c:if>

				<c:if test="${data.statCd eq '39' or (data.statCd eq '8' and data.preStatCd eq '39')}">
					<a href="" id="changeStatCd_4" class="btn_style3">변경처리</a>
				</c:if>

				<c:if test="${data.statCd eq '19' or data.statCd eq '39' or data.statCd eq '8'}">
					<a href="" id="changeStatCd_7" class="btn_style3">보완요청</a>
				</c:if>
			
				<c:if test="${data.statCd eq '7' and (data.preStatCd eq '19' or data.preStatCd eq '39')}">
					<a href="" id="" class="btn_style3">보완</a>
					<!-- TODO 19: 신고, 39: 변경신청 -->
				</c:if>
			
				<c:if test="${data.conditionCd eq '1'}">
					<c:if test="${data.statCd eq '5'}">
						<a href="" id="changeStatCd_59" class="btn_style3">신고정지해제</a>
					</c:if>
					<c:if test="${data.statCd eq '2' or data.statCd eq '4'}">
						<a href="" id="changeStatCd_5" class="btn_style3">신고정지</a>
					</c:if>
				</c:if>
				<c:if test="${data.conditionCd eq '2'}">
					<c:if test="${data.statCd eq '5'}">
						<a href="" id="changeStatCd_59" class="btn_style3">허가정지해제</a>
					</c:if>
					<c:if test="${data.statCd eq '2' or data.statCd eq '4'}">
						<a href="" id="changeStatCd_5" class="btn_style3">허가정지</a>
					</c:if>
				</c:if>
				
				<c:if test="${data.statCd eq '7'}">
					<a href="" id="changeStatCd_9" class="btn_style3">반려</a>
				</c:if>
			
			</div>
			<div class="right">
			
				<c:if test="${data.conditionCd eq '1'}">
					<c:if test="${type eq 'companies'}">
						<a href="<c:url value="/admin/sub/companies/${data.memberSeqNo}/charges" />" class="btn_style2">결제내역</a>
						<a href="<c:url value="/admin/sub/companies/${data.memberSeqNo}/history" />" class="btn_style2">신청이력보기</a>
						<!-- <a href="<c:url value="/admin/sub/companies/${data.memberSeqNo}/Register" />" class="btn_style2">업체이력등록</a> -->
						<a href="" id="enrollment" class="btn_style2">업체이력등록</a>
						<!-- <a href="<c:url value="/admin/sub/companies?${pageContext.request.queryString}" />" class="btn_style1">목록</a> -->
					</c:if>
					<c:if test="${type eq 'statuses'}">
						<a href="<c:url value="/admin/sub/statuses/${data.memberSeqNo}/charges" />" class="btn_style2">결제내역</a>
						<a href="<c:url value="/admin/sub/statuses/${data.memberSeqNo}/history" />" class="btn_style2">신청이력보기</a>
						<!-- <a href="<c:url value="/admin/sub/statuses?${pageContext.request.queryString}" />" class="btn_style1">목록</a> -->
						<a href="" id="enrollment" class="btn_style2">업체이력등록</a>
					</c:if>
				</c:if>
				<c:if test="${data.conditionCd eq '2'}">
					<c:if test="${type eq 'companies'}">
						<a href="<c:url value="/admin/trust/companies/${data.memberSeqNo}/charges" />" class="btn_style2">결제내역</a>
						<a href="<c:url value="/admin/trust/companies/${data.memberSeqNo}/history" />" class="btn_style2">신청이력보기</a>
						<!-- <a href="<c:url value="/admin/trust/companies?${pageContext.request.queryString}" />" class="btn_style1">목록</a> -->
						<a href="" id="enrollment" class="btn_style2">업체이력등록</a>
					</c:if>
					<c:if test="${type eq 'statuses'}">
						<a href="<c:url value="/admin/trust/statuses/${data.memberSeqNo}/charges" />" class="btn_style2">결제내역</a>
						<a href="<c:url value="/admin/trust/statuses/${data.memberSeqNo}/history" />" class="btn_style2">신청이력보기</a>
						<!-- <a href="<c:url value="/admin/trust/statuses?${pageContext.request.queryString}" />" class="btn_style1">목록</a> -->
						<a href="" id="enrollment" class="btn_style2">업체이력등록</a>
					</c:if>
				</c:if>
				<a href="javascript:history.back();" class="btn_style1">목록</a>
			</div>
		</div>
		<!-- //btn_area -->
		
<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/enrollment_popup.jsp" flush="true" />
		
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_appl" />
	<jsp:param name="includes" value="enrollment_popup" />
</jsp:include>
