<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="${title1}" />
	<jsp:param name="nav2" value="${title2}" />
	<jsp:param name="title" value="NONE" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />
	
		<h2 class="tit_c">${data.coName}</h2>
		<div class="form_apply form_apply_style1">
			<div class="aside">
				<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/company_tab.jsp" flush="true" />
			</div>
			<!-- //aside -->
			<div class="cont">
				<div class="table_area">
					<table>
						<caption>저작권신탁관리업</caption>
						<thead>
							<tr> 	 	 	
								<th>저작권신탁관리업 허가증</th>
							</tr>
						</thead>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="declaration_preview">
					<div class="inner">
						<div class="preview">
							<p class="num_top">허가 제 5호</p>
							<h1 class="tit">저작권신탁관리업 허가증</h1>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:185px" />
										<col style="width:236px" />
										<col style="width:173px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr class="height">
											<th>법인<span>(또는 단체)</span>명</th>
											<td><c:out value="${data.coName}" /></td>
											<th>법인/사업자등록번호<br>(생년월일)</th>
											<td>
												<c:if test="${!empty data.bubNo}"><c:out value="${data.bubNoStr}" /></c:if>
												<c:if test="${empty data.bubNo}"><c:out value="${data.coNoStr}" /></c:if>		
																							
											</td>
										</tr>
										<tr>
											<th>대표자</th>
											<td><c:out value="${data.ceoName}" /></td>
											<th>전화번호</th>
											<td><c:out value="${data.trustTel}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td class="txt_l">
												<c:if test="${!empty data.trustZipcode}">(<c:out value="${data.trustZipcode}" />)</c:if>
												<c:out value="${data.trustSido}" />
												<c:out value="${data.trustGugun}" />
												<c:out value="${data.trustDong}" />
												<c:out value="${data.trustBunji}" />
												<c:out value="${data.trustDetailAddr}" />
											</td>
											<th>팩스번호</th>
											<td><c:out value="${data.trustFax}" /></td>
										</tr>
										<tr>
											<th>취급하고자 하는 <br>저작물 등의 종류</th>
											<td colspan="3" class="txt_l">
												<c:out value="${writingKind}" />
											</td>
										</tr>
										<tr>
											<th>취급하고자 하는 권리</th>
											<td colspan="3" class="txt_l">
												<c:out value="${permKind}" />
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<p class="txt1"> 본인은 이 건 업무처리와 관련하여 <strong>[전자정부법]</strong> 제21조 제1항에 따른 행정정보의 공동이용을 통하여<br> 담당 공무원이 위의 담당 공무원 확인사항을 확인하는 것에 동의합니다</p>
							<div class="sign">
								<p><c:out value="${appRegDate}" /></p>
							</div>
							<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
							<div class="table_preview">
								<table>
									<caption></caption>
									<tbody>
										<tr>
											<th>내부결제 첨부파일</th>
										</tr>
										<c:forEach var="item" items="${rpMgmList}" varStatus="status">
											<tr>
												<td>
													<p><a href="<c:url value="/download?filename=${item.interFilepath}" />"><c:out value="${item.interFilename}" /></a></p>
												</td>
											</tr>
										</c:forEach>
										<c:if test="${fn:length(rpMgmList) eq '0'}">
											<tr>
												<td>
													<p class="txt_nofile">내부결제 첨부파일이 없습니다.</p>
												</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<h2 class="tit">신고증 기재사항 변경 내역</h2>
						<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:191px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th>변경일자</th>
											<th>변경 전</th>
											<th>변경 후</th>
										</tr>
										<c:forEach var ="item" items="${chgList}" varStatus="status">
										<tr>
									   		<td>
									   		${item.strPre1}
											</td>
											<td>
												<c:if test="${!empty item.strPre2}">${item.strPre2}<br></c:if>
												<c:if test="${!empty item.strPre3}">${item.strPre3}<br></c:if>
												<c:if test="${!empty item.strPre4}">${item.strPre4}<br></c:if>
												<c:if test="${!empty item.strPre5}">${item.strPre5}<br></c:if>
												<c:if test="${!empty item.strPre6}">${item.strPre6}<br></c:if>
												<c:if test="${!empty item.strPre7}">${item.strPre7}<br></c:if>
												<c:if test="${!empty item.strPre8}">${item.strPre8}<br></c:if>
												<c:if test="${!empty item.strPre9}">${item.strPre9}<br></c:if>
												<c:if test="${!empty item.strPre10}">${item.strPre10}<br></c:if>
												<c:if test="${!empty item.strPre11}">${item.strPre11}<br></c:if>
												<c:if test="${!empty item.strPre12}">${item.strPre12}<br></c:if>
												<c:if test="${!empty item.strPre13}">${item.strPre13}<br></c:if>
												<c:if test="${!empty item.strPre14}">${item.strPre14}<br></c:if>
												<c:if test="${!empty item.strPre15}">${item.strPre15}<br></c:if>
											</td>
											<td>
												<c:if test="${!empty item.strPost2}">${item.strPost2}<br></c:if>
												<c:if test="${!empty item.strPost3}">${item.strPost3}<br></c:if>
												<c:if test="${!empty item.strPost4}">${item.strPost4}<br></c:if>
												<c:if test="${!empty item.strPost5}">${item.strPost5}<br></c:if>
												<c:if test="${!empty item.strPost6}">${item.strPost6}<br></c:if>
												<c:if test="${!empty item.strPost7}">${item.strPost7}<br></c:if>
												<c:if test="${!empty item.strPost8}">${item.strPost8}<br></c:if>
												<c:if test="${!empty item.strPost9}">${item.strPost9}<br></c:if>
												<c:if test="${!empty item.strPost10}">${item.strPost10}<br></c:if>
												<c:if test="${!empty item.strPost11}">${item.strPost11}<br></c:if>
												<c:if test="${!empty item.strPost12}">${item.strPost12}<br></c:if>
												<c:if test="${!empty item.strPost13}">${item.strPost13}<br></c:if>
												<c:if test="${!empty item.strPost14}">${item.strPost14}<br></c:if>
												<c:if test="${!empty item.strPost15}">${item.strPost15}<br></c:if>
											</td>
										</tr>
										</c:forEach>
										<%--  <c:forEach var="item" items="${changeList}" varStatus="status">
											<tr>
												<th class="bg"><c:out value="${item.date}" /></th>
												<td class="txt_l">
													<div class="list_change">
														<c:out value="${item.content}" />
													</div>
												</td>
											</tr>	
										</c:forEach>	 		 --%>							
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
						</div>
					</div>
					<!-- //preview -->
				</div>
				<!-- //declaration_preview -->
			</div>
			<!-- //cont -->
		</div>
		<!-- //form_apply -->
		<div class="btn_area">
			<div class="right">
				<!-- 
				<a href="" class="btn_style2">PDF저장</a>
				 -->
				<a href="" id="print_cert" class="btn_style2">인쇄</a>
				<!-- <a href="" id="prev" class="btn_style1">이전</a> -->
				<a href="javascript:history.go(-1);" id="" class="btn_style1">이전</a>
			</div>
		</div>
		<!-- //btn_area -->
		

<input type="hidden" id="rpx" value="contract_declaration_union_m_new" />	
<textarea id="xmldata" style="width: 100%; height: 500px; display: none;">
	<root>
		<title>저작권신탁관리업허가증</title>    
		<certno>memberseqno</certno>                                         
		<report_no><c:out value="${data.appNoStr}" /></report_no>   
		<company_info>                                                     
			<company><![CDATA[${data.coName}]]></company>                 
			<regno><c:if test="${!empty data.bubNo}"><c:out value="${data.bubNoStr}" /></c:if> 
			<c:if test="${empty data.bubNo}">
			<c:out value="${data.coNoStr}" />
			</c:if></regno>                                          
			<boss_name><![CDATA[${data.ceoName}]]></boss_name>            
			<tel><![CDATA[${data.trustTel}]]></tel>                      
			<fax><![CDATA[${data.trustFax}]]></fax>                      
			<address><![CDATA[${data.trustZipcode} ${data.trustSido} ${data.trustGugun} ${data.trustDong} ${data.trustBunji} ${data.trustDetailAddr}]]></address>
		</company_info>                         
		<works><![CDATA[${writingKind}]]></works>                          
		<rights><![CDATA[${permKind}]]></rights>
		
		<c:forEach var="item" items="${changeDateList}" varStatus="status">
			<change_date_${status.index}><c:out value="${item}" /></change_date_${status.index}> 
		</c:forEach>
		
		<c:forEach var="item" items="${contentsList}" varStatus="status">
			<contents_${status.index}><c:out value="${item}" /></contents_${status.index}>
		</c:forEach>		
		
		<prtdate><c:out value="${appRegDate}" /></prtdate>
	</root>
</textarea>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/enrollment_popup_2.jsp" flush="true" />				
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_appl" />
	<jsp:param name="includes" value="company_tab" />
</jsp:include>
