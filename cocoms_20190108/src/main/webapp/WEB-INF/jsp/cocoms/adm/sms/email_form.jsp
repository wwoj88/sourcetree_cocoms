<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="SMS/이메일 관리" />
	<jsp:param name="nav2" value="SMS/이메일 단체발송" />
</jsp:include>
	
		<div class="tab_result">
			<ul>
				<li><a href="<c:url value="/admin/sms/send" />">SMS 발송</a></li>
				<li class="active"><a href="<c:url value="/admin/sms/send?type=email" />">이메일 발송</a></li>
			</ul>
		</div>
		<!-- //step_sub -->
		<h2 class="tit_c_s">발송대상선택</h2>
		
		<div class="search_top">
			<div class="table_area list_top btm_form_box_r40">
				<form id="searchForm" method="GET">
				<input type="hidden" name="type" value="${param.type}" />
				
					<table>
						<caption>발송대상선택</caption>
						<colgroup>
							<col style="width:125px" />
							<col style="width:auto" />
						</colgroup>
						<tbody>
							<tr>
								<th>대상선택</th>
								<td>
									<c:if test="${empty search.conDetailCd}"><c:set var="conDetailCd" value="checked" /></c:if>
									<c:if test="${search.conDetailCd eq '1'}"><c:set var="conDetailCd1" value="checked" /></c:if>
									<c:if test="${search.conDetailCd eq '2'}"><c:set var="conDetailCd2" value="checked" /></c:if>
									<c:if test="${search.conDetailCd eq '3'}"><c:set var="conDetailCd3" value="checked" /></c:if>
									<c:if test="${search.conDetailCd eq '4'}"><c:set var="conDetailCd4" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd" name="conDetailCd" value="" ${conDetailCd} />
										<label for="conDetailCd">전체</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd4" name="conDetailCd" value="4" ${conDetailCd4} />
										<label for="conDetailCd4">신탁</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd3" name="conDetailCd" value="3" ${conDetailCd3} />
										<label for="conDetailCd3">대리중개</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd1" name="conDetailCd" value="1" ${conDetailCd1} />
										<label for="conDetailCd1">대리</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="conDetailCd2" name="conDetailCd" value="2" ${conDetailCd2} />
										<label for="conDetailCd2">중개</label> 
									</div>
								</td>
							</tr>
							<tr>
								<th>수신동의 선택</th>
								<td>
									<c:if test="${empty search.smsAgree or search.smsAgree eq 'Y'}"><c:set var="smsAgreeY" value="checked" /></c:if>
									<c:if test="${search.smsAgree eq 'N'}"><c:set var="smsAgreeN" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="smsAgreeY" name="smsAgree" value="Y" ${smsAgreeY} disabled />
										<label for="smsAgreeY">수신동의</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="smsAgreeN" name="smsAgree" value="N" ${smsAgreeN} disabled />
										<label for="smsAgreeN">수신미동의</label> 
									</div>
								</td>
							</tr>
							<tr>
								<th>사업자구분</th>
								<td>
									<c:if test="${empty search.coGubunCd}"><c:set var="coGubunCd" value="checked" /></c:if>
									<c:if test="${search.coGubunCd eq '1'}"><c:set var="coGubunCd1" value="checked" /></c:if>
									<c:if test="${search.coGubunCd eq '2'}"><c:set var="coGubunCd2" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd" name="coGubunCd" value="" ${coGubunCd} />
										<label for="coGubunCd">전체</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd1" name="coGubunCd" value="1" ${coGubunCd1} />
										<label for="coGubunCd1">개인사업자</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd2" name="coGubunCd" value="2" ${coGubunCd2} />
										<label for="coGubunCd2">법인사업자</label> 
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<a href="" id="search" class="btn_style2 btn_search">조건체크</a>
				</form>
			</div>
			<!-- //table_area -->
		</div>
		
		<h2 class="tit_c_s">발송내용</h2>
		<form id="form2" action="<c:url value="/admin/email" />" method="POST">
		<c:forEach var="item" items="${allList}" varStatus="status">
			<c:if test="${!empty item.ceoEmail}">
				<input type="hidden" name="memberSeqNo" value="${item.memberSeqNo}" />
				<input type="hidden" name="receiver" value="${item.ceoEmail}" />
			</c:if>
		</c:forEach>
		
		<div class="table_area list_top sms_step_1">
			<table>
				<caption>발송내용</caption>
				<colgroup>
					<col style="width:127px" />
					<col style="width:auto" />
				</colgroup>
				<tbody>
					<tr>
						<th>발송건수</th>
						<td><strong class="txt_blue"><c:out value="${total}" />건</strong>
							<a href="" id="members2" class="btn btn_style1">목록보기</a></td>
					</tr>
					<tr>
						<th>제목</th>
						<td><input type="text" name="subject" class="input_style1" value="{user_name}님 회원가입을 진심으로 축하드립니다!"/></td>
					</tr>
					<tr>
						<th>내용</th>
						<td><textarea name="content" class="input_style1 height">{user_name}님 회원가입을 진심으로 축하드립니다!</textarea></td>
					</tr>
					<tr>
						<th>발송설정</th>
						<td>
							<div class="btm_form_box">
								<input type="radio" id="sendType1" name="sendType" value="1" checked />
								<label for="sendType1">즉시발송</label> 
							</div>
							<div class="btm_form_box">
								<input type="radio" id="sendType2" name="sendType" value="2" />
								<label for="sendType2">예약발송</label> 
							</div>
							<input type="text" name="sendDate" class="event-date input_style1 w172" value="2018-09-00">
							<select name="sendTime1" class="select_style w69 ml20">
								<c:forEach var="item" begin="0" end="23" varStatus="status">
									<c:set var="str" value="${item}" />
									<c:if test="${item lt 10}">
										<c:set var="str" value="0${item}" />
									</c:if>
									<option value="">${str}</option>
								</c:forEach>
							</select> 시
							<select name="sendTime2" class="select_style w69 ml20">
								<c:forEach var="item" begin="0" end="59" step="5" varStatus="status">
									<c:set var="str" value="${item}" />
									<c:if test="${item lt 10}">
										<c:set var="str" value="0${item}" />
									</c:if>
									<option value="">${str}</option>
								</c:forEach>								
							</select> 분
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
	
		<div class="button_area mt22">
			<div class="right">
				<a href="" id="submit" class="btn btn_style2">이메일 발송</a>
			</div>
		</div>
		</form>

	<div id="popupMembers2" class="pop_common" style="display: none;">
		<div class="inner">
			<h2 class="tit">이메일 발송목록</h2>
			<a href="" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont_n">
				<form>
					<div class="total_top_common">
						<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
					</div>
					<div class="table_area list list_type1">
						<table id="data">
							<caption></caption>
							<colgroup>
								<col style="width:auto" />
								<col style="width:136px" />
								<col style="width:136px" />
								<col style="width:136px" />
							</colgroup>
							<thead>
								<tr> 	 
									<th>번호</th>
									<th>회사명</th>
									<th>이름</th>
									<th>수신메일</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="item" items="${list}" varStatus="status">
									<tr>
										<td><c:out value="${item.memberSeqNo}" /></td>
										<td><c:out value="${item.coName}" /></td>
										<td><c:out value="${item.ceoName}" /></td>
										<td><c:out value="${item.ceoEmail}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
					<div id="pagination2" class="pagination">
			       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
					</div>
				</form>
				<div class="btn_area">
					<a href="" name="popup_close" class="btn_style1">닫기</a>
				</div>
			</div>
		</div>
	</div>
	<!-- //pop_common -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_sms" />
</jsp:include>
