<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="공지사항" />
</jsp:include>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#Progress_Loading').hide(); //첫 시작시 로딩바를 숨겨준다.
	});

	function chk01_change(chkVal) {
		var $obj = $("input[name='chk01']");
		var checkCount = $obj.size();
		for (var i = 0; i < checkCount; i++) {
			if ($obj.eq(i).is(":checked") == true) {
				$('#checkButton').attr('style', "display:show;");
				$('#upLoadFile').attr('style', "display:none;");
			} else {

				$('#checkButton').attr('style', "display:none;");
				$('#upLoadFile').attr('style', "display:show;");
			}
		}
	}
</script>
<div id="Progress_Loading">
	<!-- 로딩바 -->
	<img src="<c:url value="/img/loading.gif" />" alt="저작권위탁관리업시스템" />
</div>
<style type="text/css">
/* body
{
 text-align: center;
 margin: 0 auto;
}
 */
#Progress_Loading {
	position: absolute;
	left: 50%;
	top: 38%;
	/* background: #ffffff; */
}
</style>


<div class="table_area view write">


	<table>
		<caption>업체현황</caption>
		<colgroup>
			<col style="width: 188px">
			<col style="width: 395px">
			<col style="width: 188px">
			<col style="width: 395px">
		</colgroup>
		<tbody>
			<form name="formUpload" id="formUpload" method="post" enctype="multipart/form-data">
			<tr>
				<th>
					담당자<strong class="txt_point">*</strong>
				</th>
				<td>
					<input type="text" id="REPT_CHRR_NAME" name="REPT_CHRR_NAME" class="input_style1" value="">
				</td>
				<th>
					담당자 직위<strong class="txt_point">*</strong>
				</th>
				<td>
					<input type="text" id="REPT_CHRR_POSI" name="REPT_CHRR_POSI" class="input_style1" value="">
				</td>
			</tr>
			<tr>
				<th>
					담당자 전화번호<strong class="txt_point">*</strong>
				</th>
				<td>
					<input type="text" id="REPT_CHRR_TELX" name="REPT_CHRR_TELX" class="input_style1" value="">
				</td>
				<th>
					담당자 이메일<strong class="txt_point">*</strong>
				</th>
				<td>
					<input type="text" id="REPT_CHRR_MAIL" name="REPT_CHRR_MAIL" class="input_style1" value="">
				</td>
			</tr>
			<tr>
				<th>보고저작물 없음</th>
				<td>
					<input type="checkbox" id="chk01" name="chk01" style="height: 26px;" onchange="javascript:chk01_change(this.value);">

				</td>
				<td>
					<a href="#" id="checkButton" style="float: right; display: none; height: 26px;" class="btn_style2">위탁관리저작물 보고</a>
					<!-- <input type="button" id="checkButton" name="checkButton" value="위탁관리저작물 보고 " style="float: right; display: none; height:26px;" class="btn_style2"> -->
				</td>
			</tr>
		</tbody>
	</table>

</div>
<div class="table_area view write">

	<table>
		<caption>공지사항</caption>
		<colgroup>
			<col style="width: 193px" />
			<col style="width: auto" />
		</colgroup>
		<thead>

		</thead>
		<tbody>
			<tr>
				<th>양식</th>
				<td>
					<div class="list_file">

						<select class="select_style w172" id="gercdDown" name="gercdDown">
							<option selected="selected" value="">-선택-</option>
							<option value="1">음악</option>
							<option value="2">어문</option>
							<option value="3">방송대본</option>
							<option value="4">영화</option>
							<option value="5">방송</option>
							<option value="6">뉴스</option>
							<option value="7">미술</option>
							<option value="8">이미지</option>
							<option value="99">기타</option>
						</select>

						<a href="#" id="downFile" class="form_btn insert_file btn_style1">파일 다운로드</a>
					</div>
				</td>
			</tr>
			<tr>

				<th>첨부파일</th>
				<td>
					<div class="list_file">
						<select class="select_style w172" id="gercd" name="gercd">
							<option selected="selected" value="">-선택-</option>
							<option value="1">음악</option>
							<option value="2">어문</option>
							<option value="3">방송대본</option>
							<option value="4">영화</option>
							<option value="5">방송</option>
							<option value="6">뉴스</option>
							<option value="7">미술</option>
							<option value="8">이미지</option>
							<option value="99">기타</option>
						</select>
						<div id="DivIdFile1" style="display: inline-block;">
							<input type="text" class="input_style1 prj_file" placeholder="파일 첨부" readonly />
							<input type="file" name="addFile2" class="insert_file_target">
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
							&nbsp;&nbsp; <a href="#" id="upLoadFile" class="btn_style2">위탁관리저작물 보고</a>
							<!-- onclick="javascript:file_upload(this.value);" -->
						</div>
						</form>
					</div>

				</td>
			</tr>

		</tbody>

	</table>



</div>
<!-- //table_area -->

<div class="button_area">
	<div class="right">
		<!-- <a href="#" id="upLoadFile" class="btn_style2">위탁관리저작물 보고</a> -->
	</div>

</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_notice" />
</jsp:include>
