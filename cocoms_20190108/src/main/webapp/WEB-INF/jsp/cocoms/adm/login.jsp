<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="robots" content="noindex,nofollow"> 
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value="/css/html5_reset.css" />">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/adm_layout.css" />">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/utill.css" />">
<link rel="shortcut icon" href="<c:url value='/img/favicon.ico' />">
</head>
<body>
<div class="wrap">
	<div class="info_top">
		<div class="inner">
			<ul>
				<li><a href="<c:url value="/main" />" target="_blank">홈페이지 가기</a></li>
			</ul>
		</div>
	</div>
	<!-- //info_top -->
	<div class="container login">
		<div class="login_content login_content_n">
			<div class="inner">
				<div class="cell cell2">
				<!-- 	<h2><img src="img/logo_login.png" alt="" /></h2> -->
					<h3 class="tit">로그인</h3>
					<c:if test="${!empty message}">
						<p style="color: red; margin-bottom: 20px;">${message}</p>
					</c:if>
					<form id="form" action="<c:url value="/admin/login" />" method="POST">
						<div class="input_login">
							<input type="text" name="loginId" class="input_style1" maxlength="50" placeholder="아이디" />
							<input type="password" autocomplete="off" name="pwd" class="input_style1" maxlength="20" placeholder="비밀번호" />
							<input type="submit" class="btn_style2" value="로그인" />
						</div>
						<div class="check">
							<div class="btm_form_box">
								<input type="checkbox" id="save_id" />
								<label for="save_id">아이디저장</label> 
							</div>
						</div>
					</form>
				</div>
				<!-- //cell -->	
			</div>
			<!-- //inner -->	
		</div>
		<!-- //login_content -->
	</div>
	<!-- //container -->
	<div class="footer">
		Copyright Ⓒ  Ministry of Culture, Sports and Tourism
	</div>
	<!-- //footer -->
</div>
<!-- //wrap -->

<script src="<c:url value="/js/jquery-1.10.2.min.js" />"></script>
<script src="<c:url value="/js/jquery.bxslider.js" />"></script>
<script src="<c:url value="/js/adm_script.js" />"></script>
<script src="<c:url value="/js/jquery-ui.js" />"></script>

<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>

<script>
$(function () {

	/* login - submit */
	$('#form').on('submit', function (event) {
		var $form = $(this);
		var params = formToJson($form);

		event.preventDefault();

		// validation form
		var validForm = function (params) {
			
			if (!params['loginId']) {
				alert('아이디를 입력해 주세요.');
				return false;
			}
			
			if (!params['pwd']) {
				alert('비밀번호를 입력해 주세요.');
				return false;
			}
			
			return true;
		}
		
		// validation form
		if (!validForm(params)) {
			return false;
		}

		// save cookie
    	if ($('#save_id').prop('checked')) {
    		
    		setCookie('checked2', true, 7);
    		
    		var loginId = $form.find('[name="loginId"]').val();
    		setCookie('loginId2', loginId, 7);
    	}
		
		// form submit
		$form.unbind().submit();
	});
	
	$('#save_id').click(function () {
		
		// save cookie
    	if (!$('#save_id').prop('checked2')) {
    		
    		setCookie('checked2', false);
    		setCookie('loginId2', '');
    	}
	});
	
    var init = function () {
    	
    	if (getCookie('checked2') == 'true') {
    		$('#form [id="save_id"]').prop('checked', true);
    		
        	var loginId = getCookie('loginId2');
        	$('#form [name="loginId"]').val(loginId);
    	}
    }();
	
});
</script>

</body>
</html>
