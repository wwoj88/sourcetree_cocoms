<%@ page language="java" contentType="application/vnd.ms-excel; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<meta http-equiv="Content-Type" content="application/vnd.ms-excel">
<html >
<head>
<title>문화체육관광부</title>
<style>
br{mso-data-placement:same-cell;}
</style>
</head>
<body> 
	<table border="1">
		<tr>
			<td align="center"><b>번호</b></td>
			<td height="40" align="center"><b>신고번호</b></td>
			<td align="center"><b>대표명</b></td>
			<td align="center"><b>업체명</b></td>
			<td align="center"><b>전화번호</b></td>
			<td align="center"><b>전자우편</b></td>
			<td align="center"><b>주 소</b></td>
			
		</tr>
		<c:forEach var="item" items="${list}" varStatus="status">
			<tr>
				<td height="40" style='mso-number-format:"\@";'><font color="black"><c:out value="${status.count}" /></font></td>
				<td><font color="black"><c:out value="${item.appNo2}" /></font></td>
				<td><font color="black"><c:out value="${item.ceoName}" /></font></td>
				<td><font color="black"><c:out value="${item.coName}" /></font></td>
				<td style='mso-number-format:"\@";'><font color="black"</font><c:out value="${item.trustTel}" /></td>
				<td><font color="black"><c:out value="${item.ceoEmail}" /></font></td>
				<td><font color="black"><c:out value="${item.trustZipcode}" /><font color="black"><c:out value="${item.trustSido}" /> <c:out value="${item.trustGugun}" /> <c:out value="${item.trustDong}" /> <c:out value="${item.trustBunji}" /> <c:out value="${item.trustDetailAddr}" /></font></td>
				
			</tr>
		</c:forEach>
	</table>
</body>
</html>