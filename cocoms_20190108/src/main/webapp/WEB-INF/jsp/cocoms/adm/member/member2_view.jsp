<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="회원관리" />
	<jsp:param name="nav2" value="사업자회원" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />
<input type="hidden" id="agree" value="${data.smsAgree}" />
<input type="hidden" id="tel" value="${data.sms}" />
<input type="hidden" id="email" value="${data.ceoEmail}" /> 

		<div class="table_area list_top member">
			<table>
				<caption>사업자회원</caption>
				<colgroup>
					<col style="width:203px" />
					<col style="width:auto" />
				</colgroup>
				<tbody>
					<tr>
						<th>가입일자</th>
						<td>
							<c:out value="${login.regDateStr}" />
						</td>
					</tr>
					<tr>
						<th>최종 로그인</th>
						<td>
							<c:out value="${login.lastLoginDateStr}" /> <c:out value="${login.lastLoginTimeStr}" />
						</td>
					</tr>
					<tr>
						<th>약관동의 일자</th>
						<td>
							<c:out value="${data.terms1AgreeDateStr}" />
						</td>
					</tr>
					<tr>
						<th>업체구분</th>
						<td>
							<c:if test="${data.conditionCd eq '1'}">
								<c:if test="${data.conDetailCd eq '1'}">
									대리
								</c:if>
								<c:if test="${data.conDetailCd eq '2'}">
									중개
								</c:if>
								<c:if test="${data.conDetailCd eq '3'}">
									대리중개
								</c:if>
							</c:if>
							<c:if test="${data.conditionCd eq '2'}">
								신탁
							</c:if>
						</td>
					</tr>
					<tr>
						<th>아이디</th>
						<td>
							<c:out value="${login.loginId}" />
						</td>
					</tr>
					<%-- 
					<tr>
						<th>성명</th>
						<td>
							<input type="text" class="input_style1 w323" value="${data.ceoName}" disabled />
						</td>
					</tr>
					--%>
					<tr>
						<th>휴대전화번호</th>
						<td>
							<select class="select_style" disabled>
								<option value="" selected="selected"><c:out value="${data.ceoMobile1}" /></option>
							</select>
							<span>-</span>
							<input class="input_style1 w111" type="text" value="${data.ceoMobile2}" disabled />
							<span>-</span>
							<input class="input_style1 w111" type="text" value="${data.ceoMobile3}" disabled />
						</td>
					</tr>
					<tr>
						<th>이메일 주소</th>
						<td>
							<input type="text" class="input_style1 w149" value="${data.ceoEmail1}" disabled /> @ <input type="text" class="input_style1 w149" value="${data.ceoEmail2}" disabled />
						</td>
					</tr>
					<tr>
						<th>인증수단</th>
						<td>
							<c:if test="${!empty cert.certType}">
								<strong class="fs14">공인인증서</strong>
							</c:if>
						</td>
					</tr>
					<tr>
						<th>비밀번호 변경</th>
						<td>
							<a href="" id="changePassword" class="btn btn_style2">비밀번호 변경</a>
							<!-- <a href="" id="sendPassword" class="btn btn_style1">임시비밀번호 발송</a> -->
						</td>
					</tr>
					<tr>
						<th>상태</th>
						<td>
							<c:if test="${login.delGubun eq 'N'}">
								<c:set var="delGubunN" value="checked" />
							</c:if>
							<c:if test="${login.delGubun eq 'Y'}">
								<c:set var="delGubunY" value="checked" />
							</c:if>
							<div class="btm_form_box">
								<input type="radio" id="delGubunN" name="delGubun" ${delGubunN} disabled />
								<label for="delGubunN">사용</label> 
							</div>
							<div class="btm_form_box">
								<input type="radio" id="delGubunY" name="delGubun" ${delGubunY} disabled />
								<label for="delGubunY">탈퇴/정지</label> 
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
					
		<div class="table_area list_top member">
			<table>
				<caption>사업자회원</caption>
				<colgroup>
					<col style="width:203px" />
					<col style="width:auto" />
				</colgroup>
				<tbody>
					<%-- 
					<tr>
						<th>사업자구분</th>
						<td>
							<select class="select_style w143" disabled>
								<c:if test="${login.coGubun eq '1'}">
									<option value="" selected="selected">개인사업자</option>
								</c:if>
								<c:if test="${login.coGubun eq '2'}">
									<option value="" selected="selected">법인사업자</option>
								</c:if>
							</select>
						</td>
					</tr>
					--%>
					<tr>
						<th>사업자명</th>
						<td>
							<c:out value="${member.coName}" />
						</td>
					</tr>
					<tr>
						<th>대표자명</th>
						<td>
							<c:out value="${member.ceoName}" />
						</td>
					</tr>
					<tr>
						<th>사업자등록번호</th>
						<td>
							<c:out value="${member.coNoStr}" /> 
						</td>
					</tr>
					<tr>
						<th>법인등록번호</th>
						<td>
							<c:out value="${member.bubNoStr}" />
						</td>
					</tr>					
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<div class="button_area mt22">
			<div class="left">
				<a href="" id="sendSms2" class="btn btn_style2">SMS발송</a>
				<a href="" id="sendEmail2" class="btn btn_style2">이메일발송</a>
				<a href="" id="remove2" class="btn btn_style3">탈퇴처리</a>
			</div>
			<div class="right">
				<a href="<c:url value="/admin/members${search.coGubun}?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}&searchFromDate=${search.searchFromDate}&searchToDate=${search.searchToDate}" />" class="btn btn_style1">목록</a>
				<a href="<c:url value="/admin/members${search.coGubun}/${loginId}/edit?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}&searchFromDate=${search.searchFromDate}&searchToDate=${search.searchToDate}" />" class="btn btn_style2">수정</a>
			</div>
		</div>

	<div id="popupChangePassword" class="pop_common" style="display: none;">
		<div class="inner">
			<h2 class="tit">비밀번호 변경</h2>
			<a href="" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont">
				<form id="formChangePassword" action="<c:url value="/admin/members${search.coGubun}/${login.loginId}" />" method="POST">
				<input type="hidden" name="_method" value="PUT" />
				<input type="hidden" name="loginId" value="${login.loginId}" />
				
				<table class="last">
					<caption>비밀번호 변경</caption>
					<colgroup>
						<col style="width:111px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>아이디</th>
							<td><c:out value="${login.loginId}" /></td>
						</tr>
						<tr>
							<th>새 비밀번호</th>
							<td><input type="password" name="pwd" class="input_style1" maxlength="20" placeholder="영문/숫자/특수문자 8~20자" /></td>
						</tr>
						<tr>
							<th>새 비밀번호 확인</th>
							<td><input type="password" name="pwd2" class="input_style1" maxlength="20" placeholder="영문/숫자/특수문자 8~20자" /></td>
						</tr>
					</tbody>
				</table>
				<div class="btn_area">
					<a href="" name="popup_submit" class="btn_style2">확인</a>
					<a href="" name="popup_cancel" class="btn_style1">취소</a>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- //pop_common -->
	
	<div id="popupSendPassword" class="pop_common" style="display: none;">
		<div class="inner">
			<h2 class="tit">임시비밀번호 발송</h2>
			<a href="" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont">
				<div class="txt_top">
					<p>임시 비밀번호 전송 방식 설정</p>
					<ul class="list_dot">
						<li>선택한 방식으로 회원에게 임시비밀번호를 발송합니다.</li>
						<li>전송 성공여부와 상관없이 비밀번호는 변경됩니다.</li>
					</ul>
				</div>
				<form id="form_send" action="<c:url value="/admin/members${search.coGubun}/${login.loginId}" />/reset" method="POST">
				<table class="last">
					<caption>임시비밀번호 발송</caption>
					<colgroup>
						<col style="width:111px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>전송방식</th>
							<td>
								<div class="btm_form_box">
									<input type="radio" id="sendType1" name="sendType" value="1" />
									<label for="sendType1">SMS발송</label> 
								</div>
								<div class="btm_form_box">
									<input type="radio" id="sendType2" name="sendType" value="2" />
									<label for="sendType2">이메일발송</label> 
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="btn_area">
					<a href="" name="popup_submit" class="btn_style2">확인</a>
					<a href="" name="popup_cancel" class="btn_style1">취소</a>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- //pop_common -->	

	<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/sms_popup.jsp" flush="true" />
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_member" />
	<jsp:param name="includes" value="sms_popup" />
</jsp:include>
