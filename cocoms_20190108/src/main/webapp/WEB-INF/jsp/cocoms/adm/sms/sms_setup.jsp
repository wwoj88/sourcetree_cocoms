<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="SMS/이메일 관리" />
	<jsp:param name="nav2" value="SMS 자동발송 설정" />
</jsp:include>

		<form id="formSms" action="<c:url value="/admin/sms/all" />" method="POST">
	
		<h2 class="tit_c_s txt_blue">사용자 - 로그인</h2>
		<div class="wrap_cell_sms">
			<c:forEach var="item" items="${list}" varStatus="status">
				<input type="hidden" name="mainCodes" value="${item.mainCode}" />
				<input type="hidden" name="detailCodes" value="${item.detailCode}" />
			
				<div class="cell">
					<h2 class="tit_box tit_box_n"><c:out value="${item.subject}" /></h2>
					<div class="table_area list_top sms_step_1">
						<table>
							<caption></caption>
							<colgroup>
								<col style="width:90px" />
								<col style="width:auto" />
							</colgroup>
							<tbody>
								<tr>
									<th>사용여부</th>
									<td>
										<c:if test="${item.useYn eq 'Y'}"><c:set var="useY" value="checked" /></c:if>
										<c:if test="${item.useYn eq 'N'}"><c:set var="useN" value="checked" /></c:if>
										<div class="btm_form_box">
											<input type="radio" id="useY_${item.detailCode}" name="useYn_${item.detailCode}" value="Y" ${useY} />
											<label for="useY_${item.detailCode}">사용함</label> 
										</div>
										<div class="btm_form_box">
											<input type="radio" id="useN_${item.detailCode}" name="useYn_${item.detailCode}" value="N" ${useN} />
											<label for="useN_${item.detailCode}">사용안함</label> 
										</div>
									</td>
								</tr>
								<tr>
									<th>메세지<p class="txt_byte">0/90byte</p></th>
									<td>
										<textarea name="contents" class="input_style1 height">${item.content}</textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
				
					<div class="button_area mt13">
						<a href="" name="save" class="btn btn_style2">저장</a>
					</div>
				</div>
				<!-- //cell -->
			</c:forEach>
		</div>
		<!-- //wrap_cell_sms -->
		
		<h2 class="tit_c_s txt_blue">사용자 - 마이페이지</h2>
		<div class="wrap_cell_sms">
			<c:forEach var="item" items="${list2}" varStatus="status">
				<input type="hidden" name="mainCodes" value="${item.mainCode}" />
				<input type="hidden" name="detailCodes" value="${item.detailCode}" />
				
				<div class="cell">
					<h2 class="tit_box tit_box_n"><c:out value="${item.subject}" /></h2>
					<div class="table_area list_top sms_step_1">
						<table>
							<caption></caption>
							<colgroup>
								<col style="width:90px" />
								<col style="width:auto" />
							</colgroup>
							<tbody>
								<tr>
									<th>사용여부</th>
									<td>
										<c:if test="${item.useYn eq 'Y'}"><c:set var="useY" value="checked" /></c:if>
										<c:if test="${item.useYn eq 'N'}"><c:set var="useN" value="checked" /></c:if>
										<div class="btm_form_box">
											<input type="radio" id="useY_${item.detailCode}" name="useYn_${item.detailCode}" value="Y" ${useY} />
											<label for="useY_${item.detailCode}">사용함</label> 
										</div>
										<div class="btm_form_box">
											<input type="radio" id="useN_${item.detailCode}" name="useYn_${item.detailCode}" value="N" ${useN} />
											<label for="useN_${item.detailCode}">사용안함</label> 
										</div>
									</td>
								</tr>
								<tr>
									<th>메세지<p class="txt_byte">0/90byte</p></th>
									<td>
										<textarea name="contents" class="input_style1 height">${item.content}</textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
				
					<div class="button_area mt13">
						<a href="" name="save" class="btn btn_style2">저장</a>
					</div>
				</div>
				<!-- //cell -->
			</c:forEach>
		</div>
		<!-- //wrap_cell_sms -->
		
		<h2 class="tit_c_s txt_blue">사용자 - 저작권대리중개업</h2>
		<div class="wrap_cell_sms">
			<c:forEach var="item" items="${list3}" varStatus="status">
				<input type="hidden" name="mainCodes" value="${item.mainCode}" />
				<input type="hidden" name="detailCodes" value="${item.detailCode}" />
				
				<div class="cell">
					<h2 class="tit_box tit_box_n"><c:out value="${item.subject}" /></h2>
					<div class="table_area list_top sms_step_1">
						<table>
							<caption></caption>
							<colgroup>
								<col style="width:90px" />
								<col style="width:auto" />
							</colgroup>
							<tbody>
								<tr>
									<th>사용여부</th>
									<td>
										<c:if test="${item.useYn eq 'Y'}"><c:set var="useY" value="checked" /></c:if>
										<c:if test="${item.useYn eq 'N'}"><c:set var="useN" value="checked" /></c:if>
										<div class="btm_form_box">
											<input type="radio" id="useY_${item.detailCode}" name="useYn_${item.detailCode}" value="Y" ${useY} />
											<label for="useY_${item.detailCode}">사용함</label> 
										</div>
										<div class="btm_form_box">
											<input type="radio" id="useN_${item.detailCode}" name="useYn_${item.detailCode}" value="N" ${useN} />
											<label for="useN_${item.detailCode}">사용안함</label> 
										</div>
									</td>
								</tr>
								<tr>
									<th>메세지<p class="txt_byte">0/90byte</p></th>
									<td>
										<textarea name="contents" class="input_style1 height">${item.content}</textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
				
					<div class="button_area mt13">
						<a href="" name="save" class="btn btn_style2">저장</a>
					</div>
				</div>
				<!-- //cell -->
			</c:forEach>
		</div>
		<!-- //wrap_cell_sms -->

		<h2 class="tit_c_s txt_blue">사용자 - 저작권신탁관리업</h2>
		<div class="wrap_cell_sms">
			<c:forEach var="item" items="${list4}" varStatus="status">
				<input type="hidden" name="mainCodes" value="${item.mainCode}" />
				<input type="hidden" name="detailCodes" value="${item.detailCode}" />
				
				<div class="cell">
					<h2 class="tit_box tit_box_n"><c:out value="${item.subject}" /></h2>
					<div class="table_area list_top sms_step_1">
						<table>
							<caption></caption>
							<colgroup>
								<col style="width:90px" />
								<col style="width:auto" />
							</colgroup>
							<tbody>
								<tr>
									<th>사용여부</th>
									<td>
										<c:if test="${item.useYn eq 'Y'}"><c:set var="useY" value="checked" /></c:if>
										<c:if test="${item.useYn eq 'N'}"><c:set var="useN" value="checked" /></c:if>
										<div class="btm_form_box">
											<input type="radio" id="useY_${item.detailCode}" name="useYn_${item.detailCode}" value="Y" ${useY} />
											<label for="useY_${item.detailCode}">사용함</label> 
										</div>
										<div class="btm_form_box">
											<input type="radio" id="useN_${item.detailCode}" name="useYn_${item.detailCode}" value="N" ${useN} />
											<label for="useN_${item.detailCode}">사용안함</label> 
										</div>
									</td>
								</tr>
								<tr>
									<th>메세지<p class="txt_byte">0/90byte</p></th>
									<td>
										<textarea name="contents" class="input_style1 height">${item.content}</textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
				
					<div class="button_area mt13">
						<a href="" name="save" class="btn btn_style2">저장</a>
					</div>
				</div>
				<!-- //cell -->
			</c:forEach>
		</div>
		<!-- //wrap_cell_sms -->

		<h2 class="tit_c_s txt_blue">관리자 - 저작권중개업</h2>
		<div class="wrap_cell_sms">
			<c:forEach var="item" items="${list5}" varStatus="status">
				<input type="hidden" name="mainCodes" value="${item.mainCode}" />
				<input type="hidden" name="detailCodes" value="${item.detailCode}" />
				
				<div class="cell">
					<h2 class="tit_box tit_box_n"><c:out value="${item.subject}" /></h2>
					<div class="table_area list_top sms_step_1">
						<table>
							<caption></caption>
							<colgroup>
								<col style="width:90px" />
								<col style="width:auto" />
							</colgroup>
							<tbody>
								<tr>
									<th>사용여부</th>
									<td>
										<c:if test="${item.useYn eq 'Y'}"><c:set var="useY" value="checked" /></c:if>
										<c:if test="${item.useYn eq 'N'}"><c:set var="useN" value="checked" /></c:if>
										<div class="btm_form_box">
											<input type="radio" id="useY_${item.detailCode}" name="useYn_${item.detailCode}" value="Y" ${useY} />
											<label for="useY_${item.detailCode}">사용함</label> 
										</div>
										<div class="btm_form_box">
											<input type="radio" id="useN_${item.detailCode}" name="useYn_${item.detailCode}" value="N" ${useN} />
											<label for="useN_${item.detailCode}">사용안함</label> 
										</div>
									</td>
								</tr>
								<tr>
									<th>메세지<p class="txt_byte">0/90byte</p></th>
									<td>
										<textarea name="contents" class="input_style1 height">${item.content}</textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
				
					<div class="button_area mt13">
						<a href="" name="save" class="btn btn_style2">저장</a>
					</div>
				</div>
				<!-- //cell -->
			</c:forEach>
		</div>
		<!-- //wrap_cell_sms -->

		<h2 class="tit_c_s txt_blue">관리자 - 저작권신탁관리업</h2>
		<div class="wrap_cell_sms">
			<c:forEach var="item" items="${list6}" varStatus="status">
				<input type="hidden" name="mainCodes" value="${item.mainCode}" />
				<input type="hidden" name="detailCodes" value="${item.detailCode}" />
				
				<div class="cell">
					<h2 class="tit_box tit_box_n"><c:out value="${item.subject}" /></h2>
					<div class="table_area list_top sms_step_1">
						<table>
							<caption></caption>
							<colgroup>
								<col style="width:90px" />
								<col style="width:auto" />
							</colgroup>
							<tbody>
								<tr>
									<th>사용여부</th>
									<td>
										<c:if test="${item.useYn eq 'Y'}"><c:set var="useY" value="checked" /></c:if>
										<c:if test="${item.useYn eq 'N'}"><c:set var="useN" value="checked" /></c:if>
										<div class="btm_form_box">
											<input type="radio" id="useY_${item.detailCode}" name="useYn_${item.detailCode}" value="Y" ${useY} />
											<label for="useY_${item.detailCode}">사용함</label> 
										</div>
										<div class="btm_form_box">
											<input type="radio" id="useN_${item.detailCode}" name="useYn_${item.detailCode}" value="N" ${useN} />
											<label for="useN_${item.detailCode}">사용안함</label> 
										</div>
									</td>
								</tr>
								<tr>
									<th>메세지<p class="txt_byte">0/90byte</p></th>
									<td>
										<textarea name="contents" class="input_style1 height">${item.content}</textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
				
					<div class="button_area mt13">
						<a href="" name="save" class="btn btn_style2">저장</a>
					</div>
				</div>
				<!-- //cell -->
			</c:forEach>
		</div>
		<!-- //wrap_cell_sms -->

		<h2 class="tit_c_s txt_blue">관리자 - 실적보고</h2>
		<div class="wrap_cell_sms">
			<c:forEach var="item" items="${list7}" varStatus="status">
				<input type="hidden" name="mainCodes" value="${item.mainCode}" />
				<input type="hidden" name="detailCodes" value="${item.detailCode}" />
				
				<div class="cell">
					<h2 class="tit_box tit_box_n"><c:out value="${item.subject}" /></h2>
					<div class="table_area list_top sms_step_1">
						<table>
							<caption></caption>
							<colgroup>
								<col style="width:90px" />
								<col style="width:auto" />
							</colgroup>
							<tbody>
								<tr>
									<th>사용여부</th>
									<td>
										<c:if test="${item.useYn eq 'Y'}"><c:set var="useY" value="checked" /></c:if>
										<c:if test="${item.useYn eq 'N'}"><c:set var="useN" value="checked" /></c:if>
										<div class="btm_form_box">
											<input type="radio" id="useY_${item.detailCode}" name="useYn_${item.detailCode}" value="Y" ${useY} />
											<label for="useY_${item.detailCode}">사용함</label> 
										</div>
										<div class="btm_form_box">
											<input type="radio" id="useN_${item.detailCode}" name="useYn_${item.detailCode}" value="N" ${useN} />
											<label for="useN_${item.detailCode}">사용안함</label> 
										</div>
									</td>
								</tr>
								<tr>
									<th>메세지<p class="txt_byte">0/90byte</p></th>
									<td>
										<textarea name="contents" class="input_style1 height">${item.content}</textarea>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
				
					<div class="button_area mt13">
						<a href="" name="save" class="btn btn_style2">저장</a>
					</div>
				</div>
				<!-- //cell -->
			</c:forEach>
		</div>
		<!-- //wrap_cell_sms -->

		<div class="button_area">
			<a href="" id="submit" class="btn btn_style1">전체저장</a>
		</div>
		
		</form>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_sms" />
</jsp:include>
