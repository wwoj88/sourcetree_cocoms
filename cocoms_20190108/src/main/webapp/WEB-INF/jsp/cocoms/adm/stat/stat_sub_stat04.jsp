<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="업체현황" />
	<jsp:param name="nav2" value="통계조회" />
</jsp:include>
	
		<div class="table_area list_top list_type3">
			<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/stats_search.jsp" flush="true" />
		</div>
		<!-- //table_area -->
		<h2 class="tit_c_s"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 
		<strong class="txt_blue">저작권 유형별 저작물수</strong></h2> 
		<div class="table_area list list_type2 scroll">
			<div class="inner">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
						<col style="width:57px" />
					</colgroup>
					<thead>
						<tr> 	 
							<th></th>
							<th>복제권</th>
							<th>배포권</th>
							<th>전송권</th>
							<th>공연권</th>
							<th>전시권</th>
							<th>대여권</th>
							<th>방송권</th>
							<th>2차적저작물 작성권</th>
							<th>출판권</th>
							<th>기타</th>
							<th>계</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr>
								<td>				
								<c:if test="${!status.last }"><a href="<c:url value="/admin/status/stats/detail?conditionCd=${conditionCd}&fromYear=${item.tit}&toYear=${item.tit}&type=4" />" >${item.tit}</a></c:if>
     							<c:if test="${status.last }"><c:out value="${item.tit}"  escapeXml="false" /></c:if>
								</td>
								<td><fmt:formatNumber value="${item.val1}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val2}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val3}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val4}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val5}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val6}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val7}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val8}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val9}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val10}" type="number" /></td>
								<td><fmt:formatNumber value="${item.sum}" type="number" /></td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(list) eq '0'}">
							<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //table_area -->
		<div class="button_area mt25">
			<div class="right">
				<a href="<c:url value="/admin/status/stats/save?conditionCd=${conditionCd}&fromYear=${fromYear}&toYear=${toYear}&type=${type}" />" id="excel" class="btn btn_style2">엑셀출력</a>
				<%-- 
				<a href="" id="list" class="btn btn_style1">목록</a>
				--%>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_stat" />
	<jsp:param name="includes" value="stats_search" />
</jsp:include>
