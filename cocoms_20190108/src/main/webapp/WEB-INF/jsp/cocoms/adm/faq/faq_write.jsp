<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="자주묻는질문 (FAQ)" />
</jsp:include>

		<div class="table_area view write">
			<form id="form" method="POST" action="<c:url value="/admin/faq/${boardSeqNo}" />">
			<table>
				<caption>공지사항</caption>
				<colgroup>
					<col style="width:140px" />
					<col style="width:auto" />
				</colgroup>
				<thead>
					
				</thead>
				<tbody>
					<tr> 	 	 	
						<th>
							질문
						</th>
						<td>	
							<input type="text" name="title" class="input_style1" value="${data.title}" maxlength="100" />
						</td>
					</tr>
					<tr>
						<th>
							답변
						</th>
						<td class="cont">
							<textarea name="content" class="input_style1 height"><c:out value="${data.content2}" /></textarea>
						</td>
					</tr>
				</tbody>
			</table>
			</form>
		</div>
		<!-- //table_area -->

		<div class="button_area">
			<div class="right">
				<a href="" id="submit" class="btn btn_style2">저장</a>
				<a href="<c:url value="/admin/faq?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />" class="btn btn_style1">목록</a>
			</div>
			
		</div>
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_notice" />
</jsp:include>
