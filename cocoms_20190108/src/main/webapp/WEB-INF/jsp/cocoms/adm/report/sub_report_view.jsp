<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="대리중개업체" />
</jsp:include>
	
		<div class="box_top_common">
			<ul>
				<li>아래 사항은 저작권법 제 108조 제 1항에 따라 요청 드리는 실적보고 내용입니다. </li>
				<li>변경된 내용은 회원정보에서 수정하여 주십시오</li>
			</ul>
		</div>
		<h2 class="tit_c_s">일반사항</h2>
		<div class="table_area list_top">
			<form>
				<table>
					<caption> 대리중개업체</caption>
					<colgroup>
						<col style="width:156px" />
						<col style="width:427px" />
						<col style="width:156px" />
						<col style="width:427px" />
					</colgroup>
					<tbody>
						<tr>
							<th>대리중개업체명</th>
							<td>
								<c:out value="${member.coName}" escapeXml="false"/>
							</td>
							<th>대표자</th>
							<td>
								<c:out value="${member.ceoName}" />
							</td>
						</tr>
						<tr>
							<th>주소</th>
							<td>
								<c:out value="${member.trustZipcode}" />
								<c:out value="${member.trustSido}" />
								<c:out value="${member.trustGugun}" />
								<c:out value="${member.trustDong}" />
								<c:out value="${member.trustBunji}" />
								<c:out value="${member.trustDetailAddr}" />
								
							</td>
							<th>전화번호 / 팩스번호</th>
							<td>
								<c:out value="${member.trustTel}" />
								/
								<c:out value="${member.trustFax}" />
							</td>
						</tr>
						<tr>
							<th>경영형태 / 설립일</th>
							<td>
								<c:if test="${member.coGubunCd eq '1'}">
									개인
								</c:if>
								<c:if test="${member.coGubunCd eq '2'}">
									법인
								</c:if>
								<c:if test="${!empty member.trustRegDay}">
									/ <c:out value="${member.trustRegDayStr}" /> 
								</c:if>
								 
							</td>
							<th>대리중개업 신고일</th>
							<td>
								<c:out value="${member.appRegDateStr}" />
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<!-- //table_area -->
		<h2 class="tit_c_s">매출액 목록</h2>
		<div class="total_top_common" style="padding-top:0">
			<p class="total">(단위 : 건, 천원)</p>
		</div>
	
		
		<div class="table_area list list_type1">
			<table>
				<caption> 매출액 목록</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>년도</th>
						<th>저작물수</th>
						<th>사용료</th>
						<th>수수료</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><a href="<c:url value="/admin/reports/sub/${member.memberSeqNo}/years/${item.rptYear}" />"><c:out value="${item.rptYear}" /></a></td>
							<input type ="hidden"  id="checkyear" value="${item.rptYear}"/>
							<td><fmt:formatNumber value="${item.workSum}" type="number" /></td>
							<td><fmt:formatNumber value="${item.rentalSum}" type="number" /></td>
							<td><fmt:formatNumber value="${item.chrgSum}" type="number" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<div class="pagination">
		</div>		
		<div class="button_area">
			<%-- <c:if test="${reg_auth}"> --%>
				<%-- <a href="<c:url value="/admin/reports/sub/${member.memberSeqNo}/years/${reg_year}/new" />" class="btn btn_style2">등록</a> --%>
				<select name="selectedyear"  id="selectedyear" class="select_style" title="연도" >
					<option value="2020"  <c:if test="${reg_year eq '2019' }">selected</c:if>>2020</option> 
					<option value="2019"  <c:if test="${reg_year eq '2019' }">selected</c:if>>2019</option> 
					<option value="2018"  <c:if test="${reg_year eq '2018' }">selected</c:if>>2018</option> 
					<option value="2017"  <c:if test="${reg_year eq '2017' }">selected</c:if>>2017</option> 
					<option value="2016"  <c:if test="${reg_year eq '2016' }">selected</c:if>>2016</option> 
					<option value="2015"  <c:if test="${reg_year eq '2015' }">selected</c:if>>2015</option> 
					<option value="2014"  <c:if test="${reg_year eq '2014' }">selected</c:if>>2014</option> 
					<option value="2013"  <c:if test="${reg_year eq '2013' }">selected</c:if>>2013</option> 
					<option value="2012"  <c:if test="${reg_year eq '2012' }">selected</c:if>>2012</option> 
					<option value="2011"  <c:if test="${reg_year eq '2011' }">selected</c:if>>2011</option> 
					<option value="2010"  <c:if test="${reg_year eq '2010' }">selected</c:if>>2010</option> 
				</select>
				<a onclick="regchk();"  <%-- href="<c:url value="/admin/reports/trust/${member.memberSeqNo}/years/${reg_year}/new" />" --%>  id="regist" class="btn btn_style2">등록</a>
			<%-- </c:if> --%>
			<a href="<c:url value="/admin/reports/sub" />" class="btn btn_style2">목록</a>
			
			<a href="<c:url value="/admin/reports/sub/${memberSeqNo}/excel" />" class="btn_style1">엑셀저장</a>			
		</div>
<input type ="hidden"  id="memberSeqNo" value="${member.memberSeqNo}"/>
<input type ="hidden"  id="reg_year" value="${reg_year}"/>
<script  src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="/cocoms/js/comm/jquery.form.min.js"></script>
<script type="text/javascript">

var now = new Date()
console.log("Current Time is: " + now);

// getFullYear function will give current year 
var lastYear = now.getFullYear()-1;
console.log("Current year is: " + lastYear);

var checkyear = $('#checkyear').val();
var memberSeqNo = $('#memberSeqNo').val();

console.log("checkyear year is: " + checkyear);
function regchk(){
	var reg_year = $('#selectedyear').val();
	
/* 	if(lastYear==checkyear){
		alert('이미 등록이 완료되었습니다. 수정버튼을 사용해주십시오.');	
		return false;
	}
	 */
	location.href="/cocoms/admin/reports/sub/"+memberSeqNo+"/years/"+reg_year+"/new";
   return false;
}
</script>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_report" />
</jsp:include>
