<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="교육정보" />
	<jsp:param name="nav2" value="오프라인" />
</jsp:include>

			<div class="table_area view">
			<table>
				<caption>오프라인</caption>
				<colgroup>
					<col style="width:140px" />
					<col style="width:auto" />
				</colgroup>
				<thead>
					
				</thead>
				<tbody>
					<tr> 	 	 	
						<th>
							제목
						</th>
						<td>	
							<c:out value="${data.title}" />
						</td>
					</tr>
					<tr>
						<th>
							내용
						</th>
						<td class="cont">
							<pre style="word-wrap: break-word;white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-break:break-all;"><c:out value="${data.content2}" escapeXml="false"/></pre>
						</td>
					</tr>
					<tr>
						<th>
							첨부파일
						</th>
						<td>
							<c:forEach var="item" items="${data.boardFileDataList}" varStatus="status">
								<a href="<c:url value="/download?filename=${item.maskName}" />" class="link_board" target="_blank"><i class="fileDown board">파일 다운로드</i> <c:out value="${item.filename}" /></a>
							</c:forEach>
							<c:if test="${fn:length(data.boardFileDataList) eq '0'}">
								-
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->

		<div class="button_area">
			<div class="left">
				<a href="" id="remove" class="btn btn_style3">삭제</a>
			</div>
			<div class="right">
				<a href="<c:url value="/admin/offline/${boardSeqNo}/edit?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />" class="btn btn_style2">수정</a>
				<a href="<c:url value="/admin/offline?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />" class="btn btn_style1">목록</a>
			</div>
		</div>
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_notice" />
</jsp:include>
