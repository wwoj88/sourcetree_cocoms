<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<title>저작권 유형별 저작물수 </title>
</head>
<body>

	<table width="670" cellpadding="0" cellspacing="0" border="1">
		<tr>
			<td colspan="12"><span class="tit_02"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 저작권 유형별 저작물수 입니다.</td>
		</tr>
		<tr>
			<td colspan="12" align="right">(단위) 가로:연도, 세로:저작권 유형</td>
		</tr>
		<tr align="center">
			<td ></td>
			<td >복제권</td>
			<td >배포권</td>
			<td >전송권</td>
			<td >공연권</td>
			<td >전시권</td>
			<td >대여권</td>
			<td >방송권</td>
			<td >2차적저작물작성권</td>
			<td >출판권</td>
			<td >기타</td>
			<td>계</td>
		</tr>
		<c:forEach var="item" items="${list}" varStatus="status">
			<tr align="center">
				<td><c:out value="${item.tit}" /></td>
				<td><fmt:formatNumber value="${item.val1}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val2}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val3}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val4}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val5}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val6}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val7}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val8}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val9}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val10}" type="number" /></td>
				<td><fmt:formatNumber value="${item.sum}" type="number" /></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(list) eq '0'}">
			<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
		</c:if>
	</table>

</body>
</html>