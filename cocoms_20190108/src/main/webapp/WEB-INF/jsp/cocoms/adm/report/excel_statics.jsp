<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="위탁관리저작물 관리" />
	<jsp:param name="nav2" value="통계" />
</jsp:include>

		<div class="search_top">
			<div class="table_area list_top">
				<form id="searchForm" method="GET">
				<input type="hidden" name="pageIndex" value="${page}" />
				
				
					<table>
						<caption> 통계</caption>
						<colgroup>
							<%-- <col style="width:125px" />
							<col style="width:395px" /> --%>
							<col style="width:10%" />
							<col style="width:10%" />
							<col style="width:10%" />
							<col style="width:10%" />
							<col style="width:10%" />
							<col style="width:10%" />
							<col style="width:10%" />
							<col style="width:10%" />
						</colgroup>
						<tbody>
							<tr>
							<th>년도</th>
							<td >
						
								<select name="year" class="select_style w172 ml20">
									<option selected="selected"  value="" >-선택해주세요-</option>
									<c:forEach var="item" items="${yearList}" varStatus="status">
										<c:set var="selected" value="" />
										<c:if test="${item eq year}">
											<c:set var="selected" value="selected" />
										</c:if>
										<option value="${item}" ${selected}><c:out value="${item}" /></option>
									</c:forEach>
								</select>
							</td>
							<th>시작월</th>
							<td>
						
								<select id="smonth"  name="smonth" class="select_style w172 ml20">
									<option selected="selected"  value="" >-선택해주세요-</option>
									<option  value="01" >01월</option>
									<option  value="02" >02월</option>
									<option  value="03" >03월</option>
									<option  value="04" >04월</option>
									<option  value="05" >05월</option>
									<option  value="06" >06월</option>
									<option  value="07" >07월</option>
									<option  value="08" >08월</option>
									<option  value="09" >09월</option>
									<option  value="10" >10월</option>
									<option  value="11" >11월</option>
									<option  value="12" >12월</option>

								</select>
							</td>
							<th>종료월</th>
							<td >
						
								<select id="emonth" name="emonth" class="select_style w172 ml20">
									<option selected="selected"  value="" >-선택해주세요-</option>
									<option  value="01" >01월</option>
									<option  value="02" >02월</option>
									<option  value="03" >03월</option>
									<option  value="04" >04월</option>
									<option  value="05" >05월</option>
									<option  value="06" >06월</option>
									<option  value="07" >07월</option>
									<option  value="08" >08월</option>
									<option  value="09" >09월</option>
									<option  value="10" >10월</option>
									<option  value="11" >11월</option>
									<option  value="12" >12월</option>
									
								</select>
							</td>
						</tr>	
							
						</tbody>
					</table>
					<a href="" id="search" class="btn_style2 btn_search" style="height: 40px;line-height: 40px;">검색</a>
				</form>
			</div>
			<!-- //table_area -->
		</div>
		<div class="total_top_common">
			<c:if test="${!empty list }">
			<div class="list_btn">

				<a href="<c:url value="/admin/excel/staticsdown/${year}" />" class="btn_style1" style="background-color:#1e4787">엑셀저장</a>
			</div>
			</c:if>
			

		</div>
		<div class="table_area list list_type1">
		<table id="data">
				<caption>통계</caption>
				<colgroup>
					
					<col style="width:7%" />
					
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:12%" />

				</colgroup>
				<thead>
					<tr> 	 
						
						<th>보고년월</th>
						
						<th>음악</th>
						<th>어문</th>
						<th>방송대본</th>
						<th>영화</th>
						<th>방송</th>
						<th>뉴스</th>
						<th>미술</th>
						<th>이미지</th>
						<th>사진</th>
						<th>기타</th>
						<th>저작물없음</th>
						<th>계</th>
						
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty list}">
							<tr>
								<td colspan="13" class="text-center" style="height:400px">게시물이 없습니다.(년도 검색을 해주세요.)</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:set var="sum1" value="0" />
							<c:set var="sum2" value="0" />
							<c:set var="sum3" value="0" />
							<c:set var="sum4" value="0" />
							<c:set var="sum5" value="0" />
							<c:set var="sum6" value="0" />
							<c:set var="sum7" value="0" />
							<c:set var="sum8" value="0" />
							<c:set var="sum12" value="0" />
							<c:set var="sum9" value="0" />
							<c:set var="sum10" value="0" />
							<c:forEach var="info" items="${list}" varStatus="listStatus">
								<c:set var="reptYmd2" value="${info.REPT_YMD }"></c:set>
								<tr>
									<td class="text-center">
										<a style="color: blue;" href="/cocoms/admin/excel/exceldetails/${reptYmd2}" ><b><u><c:out value="${info.REPT_YMD }" /></u></b></a>
									</td>
									
									<td class="text-center">
										<c:out value="${info.CNT_1 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_2 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_3 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_4 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_5 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_6 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_7 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_8 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_12 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_9 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_10 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_TOT }" />
									</td>
									<c:set var="sum1" value="${ sum1 + info.CNT1_ }" />
									<c:set var="sum2" value="${ sum2 + info.CNT2_ }" />
									<c:set var="sum3" value="${ sum3 + info.CNT3_ }" />
									<c:set var="sum4" value="${ sum4 + info.CNT4_ }" />
									<c:set var="sum5" value="${ sum5 + info.CNT5_ }" />
									<c:set var="sum6" value="${ sum6 + info.CNT6_ }" />
									<c:set var="sum7" value="${ sum7 + info.CNT7_ }" />
									<c:set var="sum8" value="${ sum8 + info.CNT8_ }" />
									<c:set var="sum12" value="${ sum12 + info.CNT12_ }" />
									<c:set var="sum9" value="${ sum9 + info.CNT9_ }" />
									<c:set var="sum10" value="${ sum10 + info.CNT10_ }" />
								</tr>
							</c:forEach>
							<tr>
								<td class="text-center">
									<b>합계</b>
								</td>
								
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum1}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum2}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum3}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum4}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum5}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum6}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum7}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum8}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum12}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum9}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum10}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9+sum10}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
					<c:choose>
					<c:when test="${empty list}">
						
						</c:when>
				<c:otherwise>
					<c:forEach var="info_0" items="${list_0}" varStatus="listStatus">
					
					<tr>
						<td class="text-center"><b>보고기관수</b></td>
						
						<td class="text-center"><c:out value="${info_0.CNT1 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT2 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT3 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT4 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT5 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT6 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT7 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT8 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT12 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT9 }"/></td>
						<td class="text-center"><c:out value="${info_0.CNT10 }"/></td>
						<td class="text-center"></td>
					</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
				</tbody>
				
				
				
			</table>
	</div>
		<!-- //table_area -->
		<div id="pagination" class="pagination" style="height:30px">
       		
		</div>
		

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_excel" />
	
</jsp:include>
