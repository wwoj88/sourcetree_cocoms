<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="회원관리" />
	<jsp:param name="nav2" value="개인회원" />
</jsp:include>

		<c:if test="${empty loginId}">
			<form id="form" method="POST" action="<c:url value="/admin/members${search.coGubun}" />">
		</c:if>
		<c:if test="${!empty loginId}">
			<form id="form_edit" method="POST" action="<c:url value="/admin/members${search.coGubun}/${loginId}" />">
		</c:if>
		<input type="hidden" name="coGubun" value="${search.coGubun}" />
		<input type="hidden" name="pageIndex" value="${search.pageIndex}" />

		<div class="table_area list_top member">
			<table>
				<caption>회원등록</caption>
				<colgroup>
					<col style="width:203px" />
					<col style="width:auto" />
				</colgroup>
				<tbody>
					<tr>
						<th>회원구분</th>
						<td>
							<c:if test="${search.coGubunCd eq '1'}">
								<c:set var="coGubunCd1" value="checked" />
							</c:if>
							<c:if test="${search.coGubunCd eq '2'}">
								<c:set var="coGubunCd2" value="checked" />
							</c:if>
							<div class="btm_form_box">
								<input type="radio" id="coGubunCd1" name="coGubunCd" value="1" ${coGubunCd1} readonly />
								<label for="">개인회원</label> 
							</div>
							<div class="btm_form_box">
								<input type="radio" id="coGubunCd2" name="coGubunCd" value="2" ${coGubunCd2} readonly />
								<label for="">사업자회원</label> 
							</div>
						</td>
					</tr>
					<c:if test="${empty loginId}">
						<tr>
							<th>아이디</th>
							<td>
								<input type="text" name="loginId" class="input_style1 w323" value="" placeholder="" />
							</td>
						</tr>
						<tr>
							<th>비밀번호</th>
							<td>
								<input type="password" name="pwd" class="input_style1 w323" value="" maxlength="20" placeholder="영문/숫자/특수문자 8~20자" />
							</td>
						</tr>
						<tr>
							<th>비밀번호 확인</th>
							<td>
								<input type="password" name="pwd2" class="input_style1 w323" value="" maxlength="20" placeholder="영문/숫자/특수문자 8~20자" />
							</td>
						</tr>
					</c:if>
					<c:if test="${!empty loginId}">
						<tr>
							<th>아이디</th>
							<td>
								<input type="text" name="loginId" class="input_style1 w323 disabled" value="${login.loginId}" placeholder="" readonly />
							</td>
						</tr>
					</c:if>
					<tr>
						<th>성명</th>
						<td>
							<input type="text" name="ceoName" class="input_style1 w323" value="${data.ceoName}" />
						</td>
					</tr>
					<tr>
						<th>휴대전화번호</th>
						<td>
									<select name="ceoMobile1" class="select_style">
										<option value="010" <c:if test="${data.ceoMobile1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${data.ceoMobile1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${data.ceoMobile1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${data.ceoMobile1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${data.ceoMobile1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${data.ceoMobile1 eq '019'}">selected</c:if>>019</option> 
									</select>
							<span>-</span>
							<input type="text" name="ceoMobile2" class="input_style1 w111" value="${data.ceoMobile2}" maxlength="4" />
							<span>-</span>
							<input type="text" name="ceoMobile3" class="input_style1 w111" value="${data.ceoMobile3}" maxlength="4" />
						</td>
					</tr>
					<tr>
						<th>이메일 주소</th>
						<td>
							<input type="text" name="ceoEmail1" class="input_style1 w149" value="${data.ceoEmail1}" /> @ <input 
									type="text" name="ceoEmail2" class="input_style1 w149" value="${data.ceoEmail2}" />
						</td>
					</tr>
					<tr>
						<th>상태</th>
						<td>
							<c:if test="${data.delGubun eq 'N'}">
								<c:set var="delGubunN" value="checked" />
							</c:if>
							<c:if test="${data.delGubun eq 'Y'}">
								<c:set var="delGubunY" value="checked" />
							</c:if>
							<div class="btm_form_box">
								<input type="radio" id="delGubunN" name="delGubun" value="N" ${delGubunN} />
								<label for="delGubunN">사용</label> 
							</div>
							<div class="btm_form_box">
								<input type="radio" id="delGubunY" name="delGubun" value="Y" ${delGubunY} />
								<label for="delGubunY">탈퇴/정지</label> 
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->

		<div class="button_area mt22">
			<a href="" id="submit" class="btn btn_style2">저장</a>
			<a href="<c:url value="/admin/members${search.coGubun}?coGubun=${search.coGubun}&pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}&searchFromDate=${search.searchFromDate}&searchToDate=${search.searchToDate}" />" class="btn btn_style1">취소</a>
		</div>

		</form>
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_member" />
</jsp:include>
