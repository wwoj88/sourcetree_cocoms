<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="회원관리" />
	<jsp:param name="nav2" value="사업자회원" />
</jsp:include>

		<form id="form_edit" method="POST" action="<c:url value="/admin/members${search.coGubun}/${loginId}" />">
		<input type="hidden" name="conditionCd" value="${member.conditionCd}" />
		<input type="hidden" name="conDetailCd" value="${member.conDetailCd}" />
		<input type="hidden" name="coGubun" value="${search.coGubun}" />
		<input type="hidden" name="pageIndex" value="${search.pageIndex}" />

		<div class="table_area list_top member">
			<table>
				<caption>회원등록</caption>
				<colgroup>
					<col style="width:203px" />
					<col style="width:auto" />
				</colgroup>
				<tbody>
					<tr>
						<th>회원구분</th>
						<td>
							<c:if test="${data.coGubunCd eq '1'}">
								<c:set var="coGubunCd1" value="checked" />
							</c:if>
							<c:if test="${data.coGubunCd eq '2'}">
								<c:set var="coGubunCd2" value="checked" />
							</c:if>
							<div class="btm_form_box">
								<input type="radio" id="coGubunCd1" name="" value="1" ${coGubunCd1} readonly />
								<label for="">개인회원</label> 
							</div>
							<div class="btm_form_box">
								<input type="radio" id="coGubunCd2" name="" value="2" ${coGubunCd2} readonly />
								<label for="">사업자회원</label> 
							</div>
						</td>
					</tr>
					<tr>
						<th>약관동의 일자</th>
						<td>
							<input type="text" id="terms1AgreeDate" name="terms1AgreeDate" value="${data.terms1AgreeDate}" maxlength="8" />
						</td>
					</tr>
					<tr>
						<th>아이디</th>
						<td>
							<input type="text" name="loginId" class="input_style1 w323 disabled" value="${login.loginId}" placeholder="" readonly />
						</td>
					</tr>
					<%-- 
					<tr>
						<th>성명</th>
						<td>
							<input type="text" name="ceoName" class="input_style1 w323" value="${data.ceoName}" />
						</td>
					</tr>
					--%>
					<tr>
						<th>휴대전화번호</th>
						<td>
									<select name="ceoMobile1" class="select_style">
										<option value="010" <c:if test="${data.ceoMobile1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${data.ceoMobile1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${data.ceoMobile1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${data.ceoMobile1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${data.ceoMobile1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${data.ceoMobile1 eq '019'}">selected</c:if>>019</option> 
									</select>
							<span>-</span>
							<input type="text" name="ceoMobile2" class="input_style1 w111" value="${data.ceoMobile2}" maxlength="4" />
							<span>-</span>
							<input type="text" name="ceoMobile3" class="input_style1 w111" value="${data.ceoMobile3}" maxlength="4" />
						</td>
					</tr>
					<tr>
						<th>이메일 주소</th>
						<td>
							<input type="text" name="ceoEmail1" class="input_style1 w149" value="${data.ceoEmail1}" /> @ <input 
									type="text" name="ceoEmail2" class="input_style1 w149" value="${data.ceoEmail2}" />
						</td>
					</tr>
					<tr>
						<th>상태</th>
						<td>
							<c:if test="${login.delGubun eq 'N'}">
								<c:set var="delGubunN" value="checked" />
							</c:if>
							<c:if test="${login.delGubun eq 'Y'}">
								<c:set var="delGubunY" value="checked" />
							</c:if>
							<div class="btm_form_box">
								<input type="radio" id="delGubunN" name="delGubun" value="N" ${delGubunN} />
								<label for="delGubunN">사용</label> 
							</div>
							<div class="btm_form_box">
								<input type="radio" id="delGubunY" name="delGubun" value="Y" ${delGubunY} />
								<label for="delGubunY">탈퇴/정지</label> 
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="table_area list_top member">
			<table>
				<caption>회원등록</caption>
				<colgroup>
					<col style="width:203px" />
					<col style="width:auto" />
				</colgroup>
				<tbody>
					<%-- 
					<tr>
						<th>사업자구분</th>
						<td>
							<select name="coGubunCd" class="select_style w143">
								<option value="2">법인사업자</option>
								<option value="1">개인사업자</option>
							</select>
						</td>
					</tr>
					--%>
					<tr>
						<th>사업자명</th>
						<td>
							<input type="text" name="coName" class="input_style1 w323" value="${member.coName}" />
						</td>
					</tr>
					<tr>
						<th>대표자명</th>
						<td>
							<input type="text" name="ceoName" class="input_style1 w323" value="${member.ceoName}" />
						</td>
					</tr>
					<tr>
						<th>사업자등록번호</th>
						<td>
							<input type="text" name="coNo1" class="input_style1 w111" value="${data.coNo1}" maxlength="3" /> - 
							<input type="text" name="coNo2" class="input_style1 w111" value="${data.coNo2}" maxlength="2" /> - 
							<input type="text" name="coNo3" class="input_style1 w111" value="${data.coNo3}" maxlength="5" /> 
						</td>
					</tr>
					<tr>
						<th>법인등록번호</th>
						<td>
							<input  type="text" name="bubNo1" class="input_style1 w173" value="${data.bubNo1}" maxlength="6" /> - 
							<input  type="text" name="bubNo2" class="input_style1 w173" value="${data.bubNo2}" maxlength="7" />
						</td>                   
					</tr>
				</tbody>
			</table>
		</div>		
		<!-- //table_area -->

		<div class="button_area mt22">
			<a href="" id="submit" class="btn btn_style2">저장</a>
			<a href="<c:url value="/admin/members${search.coGubun}?coGubun=${search.coGubun}&pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}&searchFromDate=${search.searchFromDate}&searchToDate=${search.searchToDate}" />" class="btn btn_style1">취소</a>
		</div>

		</form>
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_member" />
</jsp:include>
