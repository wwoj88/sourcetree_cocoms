<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="신탁단체" />
	<jsp:param name="title" value="${member.coName}" /> 
</jsp:include>

<style>
#amt input {width: 50px;} 
</style>

		<form id="form" method="post" action="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab2" />">
		<input type="hidden" name="trustInfoSeqno" value="${report.trustInfoSeqno}" />
	<input type="hidden" name="trust" value="1"/>
		<c:if test="${regist eq true}">
			<input type="hidden" name="returnUrl" value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab3/new" />
		</c:if>
		
		<div class="tab_result">
			<ul>
				<c:if test="${regist ne true}">
					<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}" />">실적정보</a></li>
					<li class="active"><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab2" />">저작권료 징수분배 내역</a></li>
					<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab3" />">사업계획</a></li>
				</c:if>
				<c:if test="${regist eq true}">
					<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/new" />">실적정보</a></li>
					<li class="active"><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab2/new" />">저작권료 징수분배 내역</a></li>
					<li><a>사업계획</a></li>
				</c:if>
			</ul>
		</div>
		<!-- //step_sub -->
		<h2 class="tit_c_s">1. <c:out value="${year}" />년 신탁사용료 <!-- <a href="" class="btn btn_style3 right">기타항목 추가</a> --></h2>
		<div class="table_area list list_type2 scroll">
			<div class="inner">
				<table id="royalty">
					<caption></caption>
					<colgroup>
						<col style="width:168px" />
						<col style="width:138px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:500px" />
					</colgroup>
					<thead>
						<tr> 	 
							<th>구분</th>
							<th>계약 (이용건수)</th>
							<th colspan="3">징수액 <span>(단위 : 천원)</span></th>
							<th colspan="3">분배액 <span>(단위 : 천원)</span></th>
							<th colspan="3">미분배액 <span>(단위 : 천원)</span></th>
							<th colspan="3">수수료 <span>(단위 : 천원)</span></th>
							<th colspan="4">수수료율 <span>(단위 : %)</span></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>-</td>
							<td>-</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>비고</td>
						</tr>
						<c:forEach var="item" items="${royaltyList}" varStatus="status">
							<input type="hidden" name="trustRoyaltySeqNo" value="${item.trustRoyaltySeqNo}" />
							<input type="hidden" name="trustGubunSeqNo" value="${item.trustGubunSeqNo}" />
							
							<input type="hidden" name="gubunName" value="${item.gubunName}" />
							<input type="hidden" name="gubunKind" value="${item.gubunKind}" />
							<tr>
								<td>
									<%-- <c:if test="${item.gubunKind ne '1'}">
										기타<br>
									</c:if> --%>
									<c:out value="${item.gubunName}" />
								</td>
								<td><input type="text" id="contract${status.count}" name="contract" class="input_style1" value="<c:out value="${item.contract}" />" /></td>
								<td><input type="text" id="inCollected${status.count}" name="inCollected" class="input_style1" value="<c:out value="${item.inCollected}" />" /></td>
								<td><input type="text" id="outCollected${status.count}" name="outCollected" class="input_style1" value="<c:out value="${item.outCollected}" />" /></td>
								<td><input type="text" id="collectedSum${status.count}"  name="collectedSum" class="input_style1" value="<c:out value="${item.collectedSum}" />" readonly/></td>
								<td><input type="text" id="inDividend${status.count}"  name="inDividend" class="input_style1" value="<c:out value="${item.inDividend}" />" /></td>
								<td><input type="text" id="outDividend${status.count}" name="outDividend" class="input_style1" value="<c:out value="${item.outDividend}" />" /></td>
								<td><input type="text" id="dividendSum${status.count}" name="dividendSum" class="input_style1" value="<c:out value="${item.dividendSum}" />" readonly/></td>
								<td><input type="text" id="inDividendOff${status.count}" name="inDividendOff" class="input_style1" value="<c:out value="${item.inDividendOff}" />" /></td>
								<td><input type="text" id="outDividendOff${status.count}"  name="outDividendOff" class="input_style1" value="<c:out value="${item.outDividendOff}" />" /></td>
								<td><input type="text" id="dividendOffSum${status.count}"  name="dividendOffSum" class="input_style1" value="<c:out value="${item.dividendOffSum}" />" readonly/></td>
								<td><input type="text" id="inChrg${status.count}"  name="inChrg" class="input_style1" value="<c:out value="${item.inChrg}" />" /></td>
								<td><input type="text" id="outChrg${status.count}"  name="outChrg" class="input_style1" value="<c:out value="${item.outChrg}" />" /></td>
								<td><input type="text" id="chrgSum${status.count}" name="chrgSum" class="input_style1" value="<c:out value="${item.chrgSum}" />" readonly/></td>
								<td><input type="text" id="inChrgRate${status.count}"  name="inChrgRate" class="input_style1" value="<c:out value="${item.inChrgRate}" />" /></td>
								<td><input type="text" id="outChrgRate${status.count}"  name="outChrgRate" class="input_style1" value="<c:out value="${item.outChrgRate}" />" /></td>
								<td><input type="text" id="chrgRateSum${status.count}" name="chrgRateSum" class="input_style1" value="<c:out value="${item.chrgRateSum}" />" /></td>
								<td ><textarea id="trustMemo${status.count}" name="trustMemo" class="input_style1"  ><c:out value="${item.trustMemo}"/></textarea></td>
							</tr>						
						</c:forEach>
						<c:if test="${fn:length(royaltyList) gt 0}">
							<tr class="bg_total">
								<td>총계</td>
								<td><input type="text" id="contract" class="input_style1" value="<fmt:formatNumber value="${contractSum}" type="number" />" readonly /></td>
								<td><input type="text" id="inCollected" class="input_style1" value="<fmt:formatNumber value="${inCollectedSum}" type="number" />" readonly /></td>
								<td><input type="text" id="outCollected" class="input_style1" value="<fmt:formatNumber value="${outCollectedSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="collectedSum" class="input_style1" value="<fmt:formatNumber value="${collectedSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="inDividend" class="input_style1" value="<fmt:formatNumber value="${inDividendSum}" type="number" />" readonly /></td>
								<td><input type="text" id="outDividend" class="input_style1" value="<fmt:formatNumber value="${outDividendSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="dividendSum" class="input_style1" value="<fmt:formatNumber value="${dividendSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="inDividendOff" class="input_style1" value="<fmt:formatNumber value="${inDividendOffSum}" type="number" />" readonly /></td>
								<td><input type="text" id="outDividendOff" class="input_style1" value="<fmt:formatNumber value="${outDividendOffSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="dividendOffSum" class="input_style1" value="<fmt:formatNumber value="${dividendOffSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="inChrg" class="input_style1" value="<fmt:formatNumber value="${inChrgSum}" type="number" />" readonly /></td>
								<td><input type="text" id="outChrg" class="input_style1" value="<fmt:formatNumber value="${outChrgSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="chrgSum" class="input_style1" value="<fmt:formatNumber value="${chrgSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="inChrgRate" class="input_style1" value="<fmt:formatNumber value="${inChrgRateSum}" type="number" />" readonly /></td>
								<td><input type="text" id="outChrgRate" class="input_style1" value="<fmt:formatNumber value="${outChrgRateSum}" type="number" />" readonly /></td>
								
								<td><input type="text" id="chrgRateSum" class="input_style1" value="<fmt:formatNumber value="${chrgRateSum}" type="number" />" readonly /></td>
								
							</tr>
						</c:if>
						<c:if test="${fn:length(royaltyList) eq '0'}">
							<tr><td colspan="17">등록된 신탁사용료 내용이 없습니다.</td></tr>
						</c:if>						
					</tbody>
				</table>
			</div>
		</div>

		<h2 class="tit_c_s">2. <c:out value="${year}" />년 학교교육목적, 도서관 보상금</h2>
		<h2 class="tit_box yellow" style="margin-top:0">저작권자 <p class="txt_unit">(단위 건, 천원, %) </p></h2>
		<div class="table_area list list_type1 list_type1_n">
			<table>
				<caption></caption>
				<colgroup>
					<col style="width:84px" />
					<col style="width:84px" />
					<col style="width:68px" />
					<col style="width:151px" />
					<col style="width:151px" />
					<col style="width:151px" />
					<col style="width:151px" />
					<col style="width:151px" />
					<col style="width:151px" />
				</colgroup>
				<thead>
					<tr> 	 
						<th colspan="2">구분</th>
						<th>이용단체수</th>
						<th>정수액</th>
						<th>분배액</th>
						<th>미분배액</th>
						<th>수수료</th>
						<th>수수료율</th>
						<th>비고</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${copyrightList}" varStatus="status">
						<input type="hidden" name="trustCopyrightSeqNo" value="${item.trustCopyrightSeqNo}" />
						<input type="hidden" name="gubun" value="${item.gubun}" />
						<tr>
									<c:if test="${status.index eq '0'}">
								<td rowspan="5"> 교과용<br>도서<br>보상금</td>
							</c:if>					
							<c:if test="${status.index eq '5'}">
								<td rowspan="5"> 수업목적의<br>복제등<br>보상금</td>
							</c:if>		
							<c:if test="${status.index eq '10'}">
								<td colspan="2"> 도서관보상금</td>
							</c:if>				
							<c:if test="${status.index eq '11'}">
								<td rowspan="5">수업지원 목적의<br>복제등<br>보상금</td>
							</c:if>					
							<c:if test="${status.index ne '10'}">
								<td><c:out value="${item.gubunName}" /></td>
							</c:if>					
							<td><input type="text" name="organization" class="input_style1" value="<c:out value="${item.organization}" />" /></td>
							<td><input type="text" name="collected" class="input_style1" value="<c:out value="${item.collected}" />" /></td>
							<td><input type="text" name="dividend" class="input_style1" value="<c:out value="${item.dividend}" />" /></td>
							<td><input type="text" name="dividendOff" class="input_style1" value="<c:out value="${item.dividendOff}" />" /></td>
							<td><input type="text" name="chrg" class="input_style1" value="<c:out value="${item.chrg}" />" /></td>
							<td><input type="text" name="chrgRate" class="input_style1" value="<c:out value="${item.chrgRate}" />" /></td>
							<td><input type="text" name="memo" class="input_style1" value="<c:out value="${item.memo}" />" /></td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(copyrightList) eq '0'}">
						<tr><td colspan="8">등록된 보상금(저작권자) 내용이 없습니다.</td></tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->		

<h2 class="tit_box tit_box_n mt25">
			실연자
			<p class="txt_unit">(단위 건, 천원, %)</p>
		</h2>
		<div class="table_area list list_type1">
			<table>
				<caption></caption>
				<colgroup>
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
				</colgroup>
				<thead>
					<tr>
						<th colspan="2">구분</th>
						<th>이용업체</th>
						<th>정수액</th>
						<th>분배액</th>
						<th>미분배액</th>
						<th>수수료</th>
						<th>수수료율</th>
						<th>비고</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${executeList}" varStatus="status">
						<input type="hidden" name="trustExecSeqNo" value="${item.trustExecSeqNo}" />
						<input type="hidden" name="gubun2" value="${item.gubun}" />
						<tr>
							<c:if test="${status.index eq '0'}">
								<td rowspan="5">판매용<br>음반방송<br>보상금
								</td>
							</c:if>
							<c:if test="${status.index eq '5'}">
								<td rowspan="5">음반의 디음송<br>보상금
								</td>
							</c:if>
							<c:if test="${status.index eq '10'}">
								<td rowspan="5">판매용<br>음반의 공연<br>보상금
								</td>
							</c:if>
							<td>${item.gubunName}</td>
							<c:if test="${status.index eq '0' or status.index eq '5' or status.index eq '10'}">
								<td rowspan="4"></td>
							</c:if>
							<c:choose>
								<c:when test="${status.index eq '4' or status.index eq '9' or status.index eq '14'}">
									<td><input type="text" name="organization2" class="input_style1" value="<c:out value="${item.organization}" />" /></td>
								</c:when>
								<c:otherwise>
									<input type="hidden" name="organization2" value="" />
								</c:otherwise>
							</c:choose>
							<c:if test="${status.index eq '0' or status.index eq '5' or status.index eq '10'}">
								<td rowspan="4"></td>
							</c:if>
							<c:choose>
								<c:when test="${status.index eq '4' or status.index eq '9' or status.index eq '14'}">
									<td><input type="text" name="collected2" class="input_style1" value="<c:out value="${item.collected}" />" /></td>
								</c:when>
								<c:otherwise>
									<input type="hidden" name="collected2" value="" />
								</c:otherwise>
							</c:choose>
							<td><input type="text" name="dividend2" class="input_style1" value="<c:out value="${item.dividend}" />" /></td>
							<td><input type="text" name="dividendOff2" class="input_style1" value="<c:out value="${item.dividendOff}" />" /></td>
							<td><input type="text" name="chrg2" class="input_style1" value="<c:out value="${item.chrg}" />" /></td>
							<td><input type="text" name="chrgRate2" class="input_style1" value="<c:out value="${item.chrgRate}" />" /></td>
							<td><input type="text" name="memo2" class="input_style1" value="<c:out value="${item.memo}" />" /></td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(executeList) eq '0'}">
						<tr>
							<td colspan="8">등록된 보상금(실연자) 내용이 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<h2 class="tit_box tit_box_n mt25">
			음반제작자
			<p class="txt_unit">(단위 건, 천원, %)</p>
		</h2>
		<div class="table_area list list_type1">
			<table>
				<caption></caption>
				<colgroup>
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
					<col style="width: 151px" />
				</colgroup>
				<thead>
					<tr>
						<th>구분</th>
						<th>이용업체</th>
						<th>정수액</th>
						<th>분배액</th>
						<th>미분배액</th>
						<th>수수료</th>
						<th>수수료율</th>
						<th>비고</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${makerList}" varStatus="status">
						<input type="hidden" name="trustMakerSeqNo" value="${item.trustMakerSeqNo}" />
						<input type="hidden" name="gubun3" value="${item.gubun}" />
						<tr>
							<c:if test="${status.index eq '0'}">
								<td>판매용음반의<br>방송보상금
								</td>
							</c:if>
							<c:if test="${status.index eq '1'}">
								<td>음반의<br>디음송보상금
								</td>
							</c:if>
							<c:if test="${status.index gt '1'}">
								<td>판매용음반의<br>공연보상금
								</td>
							</c:if>
							<td><input type="text" name="organization3" class="input_style1" value="<c:out value="${item.organization}" />" /></td>
							<td><input type="text" name="collected3" class="input_style1" value="<c:out value="${item.collected}" />" /></td>
							<td><input type="text" name="dividend3" class="input_style1" value="<c:out value="${item.dividend}" />" /></td>
							<td><input type="text" name="dividendOff3" class="input_style1" value="<c:out value="${item.dividendOff}" />" /></td>
							<td><input type="text" name="chrg3" class="input_style1" value="<c:out value="${item.chrg}" />" /></td>
							<td><input type="text" name="chrgRate3" class="input_style1" value="<c:out value="${item.chrgRate}" />" /></td>
							<td><input type="text" name="memo3" class="input_style1" value="<c:out value="${item.memo}" />" /></td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(makerList) eq '0'}">
						<tr>
							<td colspan="8">등록된 보상금(음반제작자) 내용이 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>




		<!-- //table_area -->
		<div class="button_area mt25">
			<a href="" id="save" class="btn btn_style2">완료</a>
			<div class="right">
				<a href="<c:url value="/admin/reports/trust/${member.memberSeqNo}" />" class="btn btn_style1">목록</a>
			</div>
		</div>
		
		</form>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_report2" />
</jsp:include>
