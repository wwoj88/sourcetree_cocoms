<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="신탁단체" /> 
	<jsp:param name="title" value="${member.coName}" /> 
</jsp:include>
	
		<div class="table_area list list_type1">
			<table>
				<caption> 매출액 목록</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>년도</th>
						<th>실적정보</th>
						<th>저작권료 징수분배 내역</th>
						<th>사업계획</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><span><a href="<c:url value="/admin/reports/trust/${member.memberSeqNo}/years/${item.rptYear}" />"><c:out value="${item.rptYear}" /></a></span></td>
							<input type ="hidden"  id="checkyear" value="${item.rptYear}"/>
							<td>
								<c:if test="${item.trustInfoSeqNo gt 0}">
									<span title="fileDown" class="fileDown">파일 다운로드</span>
								</c:if>
							</td>
							<td>
								<c:if test="${item.trustCopyrightSeqNo gt 0 or item.trustExecSeqNo gt 0 or item.trustMakerSeqNo gt 0 or item.trustRoyaltySeqNo gt 0}">
									<span title="fileDown" class="fileDown">파일 다운로드</span>
								</c:if>
							</td>
							<td>
								<c:if test="${!empty item.businessGoal or !empty item.prop}">
									<span title="fileDown" class="fileDown">파일 다운로드</span>
									(<c:out value="${item.rptYear}" />)
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<div class="pagination">
		</div>		
		<div class="button_area">
			<%-- <c:if test="${reg_auth}"> --%>
				<select name="selectedyear"  id="selectedyear" class="select_style" title="연도" >
					<option value="2019"  <c:if test="${reg_year eq '2019' }">selected</c:if>>2019</option> 
					<option value="2018"  <c:if test="${reg_year eq '2018' }">selected</c:if>>2018</option> 
					<option value="2017"  <c:if test="${reg_year eq '2017' }">selected</c:if>>2017</option> 
					<option value="2016"  <c:if test="${reg_year eq '2016' }">selected</c:if>>2016</option> 
					<option value="2015"  <c:if test="${reg_year eq '2015' }">selected</c:if>>2015</option> 
					<option value="2014"  <c:if test="${reg_year eq '2014' }">selected</c:if>>2014</option> 
					<option value="2013"  <c:if test="${reg_year eq '2013' }">selected</c:if>>2013</option> 
					<option value="2012"  <c:if test="${reg_year eq '2012' }">selected</c:if>>2012</option> 
					<option value="2011"  <c:if test="${reg_year eq '2011' }">selected</c:if>>2011</option> 
					<option value="2010"  <c:if test="${reg_year eq '2010' }">selected</c:if>>2010</option> 
				</select>
				<a onclick="regchk();"  <%-- href="<c:url value="/admin/reports/trust/${member.memberSeqNo}/years/${reg_year}/new" />" --%>  id="regist" class="btn btn_style2">등록</a>
			<%-- </c:if> --%>
			<a href="<c:url value="/admin/reports/trust" />" class="btn btn_style2">목록</a>

			<a href="<c:url value="/admin/reports/trust/${memberSeqNo}/excel" />" class="btn_style1">엑셀저장</a>			
		</div>
<input type ="hidden"  id="memberSeqNo" value="${member.memberSeqNo}"/>
<input type ="hidden"  id="reg_year" value="${reg_year}"/>
<script  src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="/cocoms/js/comm/jquery.form.min.js"></script>
<script type="text/javascript">

var now = new Date()
console.log("Current Time is: " + now);

// getFullYear function will give current year 
var lastYear = now.getFullYear()-1;
console.log("Current year is: " + lastYear);

var checkyear = $('#checkyear').val();
var memberSeqNo = $('#memberSeqNo').val();


console.log("checkyear year is: " + checkyear);
function regchk(){
	var reg_year = $('#selectedyear').val();
	
	location.href="/cocoms/admin/reports/trust/"+memberSeqNo+"/years/"+reg_year+"/new";
   return false;
}
</script>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_report" />
</jsp:include>
