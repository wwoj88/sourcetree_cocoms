<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="msg" value="결제취소중 오류가 발생하였습니다." />
<c:if test="${result eq true}">
	<c:set var="msg" value="결제취소가 완료 되었습니다." />
</c:if>

<script>
	alert('<c:out value="${msg}" />');
	
	opener.location.reload();
	window.close();
</script>
