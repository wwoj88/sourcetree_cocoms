<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename"));
	response.setHeader("Content-Description", "JSP Generated Data");


%>
<html>
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 </head>

<body>

	<table width="670" cellpadding="0" cellspacing="0" border="1">
		<tr>
			<td colspan="2" align="center"><c:out value="${fromYear}" />년 대리중개 업체별 직원수</td>
		</tr>
		<tr>
			<td colspan="2" align="right">(단위) 가로:직원수, 세로:업체명</td>
		</tr>
		<tr align="center">
			
			<td style="width:536">업체명</td>
			<td style="width:134">직원수</td>
			
		</tr>
		<c:forEach var="item" items="${list}" varStatus="status">
			<tr align="center">
				<td><c:out value="${item.name}" escapeXml="false"  /></td>
				<td><fmt:formatNumber value="${item.sum}" type="number" /></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(list) eq '0'}">
		<br>
			<tr><td  align="center" colspan="2">조회된 데이터가 없습니다.</td></tr>
		</c:if>					
	</table>

</body>
</html>