<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="공지사항" />
</jsp:include>

		<div class="table_area view write">
			<form id="form" method="POST" action="<c:url value="/admin/notices/${boardSeqNo}" />">
			<table>
				<caption>공지사항</caption>
				<colgroup>
					<col style="width:140px" />
					<col style="width:auto" />
				</colgroup>
				<thead>
					
				</thead>
				<tbody>
					<tr> 	 	 	
						<th>
							제목
						</th>
						<td>	
							<input type="text" name="title" class="input_style1" value="${data.title}" maxlength="100" />
						</td>
					</tr>
					<tr>
						<th>
							내용
						</th>
						<td class="cont">
							<textarea name="content" class="input_style1 height">${data.content2}</textarea>
						</td>
					</tr>
					<tr>
						<th>
							첨부파일
						</th>
						<td>
							<div class="list_file">
								<input type="text" class="input_style1 prj_file" placeholder="파일 첨부" readonly />
								<input type="file" name="addFile" class="insert_file_target">
								<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
								<a href="" id="addFile" class="btn_style2">추가</a>
							</div>
							<div class="wrap_link_board">
								<c:forEach var="item" items="${data.boardFileDataList}" varStatus="status">
									<a href="/download/=${item.maskName}" name="file" class="link_board"><i class="fileDown board">파일 다운로드</i> <c:out value="${item.filename}" /></a>
									<a href="" name="removeFile" class="btn_del"><img src="<c:url value="/img/btn_del.png" />" alt="파일삭제" /></a>
									<input type="hidden" name="filename" value="${item.filename}" />
									<input type="hidden" name="maskName" value="${item.maskName}" />
									<input type="hidden" name="filesize" value="${item.filesize}" />
								</c:forEach>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			</form>
		</div>
		<!-- //table_area -->

		<div class="button_area">
			<div class="right">
				<a href="" id="submit" class="btn btn_style2">저장</a>
				<a href="<c:url value="/admin/notices?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />" class="btn btn_style1">목록</a>
			</div>
			
		</div>
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_notice" />
</jsp:include>
