<%@ page language="java" contentType="application/vnd.ms-excel; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>문화체육관광부</title>
<style>
	br{mso-data-placement:same-cell;}
	.HtdBg {
		background:#CCCCFF;
		text-align:center;
	}	
</style>
</head>
<body>

<table width="757" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center"><span style="font-size:15pt">▣ 실적보고-저작권 징수분배 내역</span></td>
	</tr>
	<tr>
		<td align="right">인쇄일 : <c:out value="${date}" /></td>
	</tr>
	<tr>
		<td align="right"><c:out value="${report.coName}" /></td>
	</tr>
	<tr>
		<td>1. <c:out value="${year}" />년 신탁사용료</td>
	</tr>
	<tr>
		<td align="right">(단위 건, 천원, %)</td>
	</tr>
	<tr>
		<td>
			<table width="737" cellpadding="0" cellspacing="0" border="1">
				<tr align="center">
					<td width="95" class="HtdBg">구분</td>
					<td width="70"  class="HtdBg">계약<br>(이용건수)</td>
					<td width="190" class="HtdBg" colspan="3" >징수액</td>
					<td width="189" class="HtdBg" colspan="3">분배액</td>
					<td width="189" class="HtdBg" colspan="3">미분배액</td>
				</tr>
				
				<tr align="center">
					<td class="HtdBg"></td>
					<td class="HtdBg"></td>
					<td width="63" height="25" class="HtdBg">국내</td>
					<td width="63" class="HtdBg">해외</td>
					<td width="64" class="HtdBg">소계</td>
					<td width="63" class="HtdBg">국내</td>
					<td width="63" class="HtdBg">해외</td>
					<td width="63" class="HtdBg">소계</td>
					<td width="63" class="HtdBg">국내</td>
					<td width="63" class="HtdBg">해외</td>
					<td width="63" class="HtdBg">소계</td>
				</tr>
				<c:forEach var="item" items="${royaltyList}" varStatus="status">
					<tr align="center">
						<td class="HtdBg" >
							<c:if test="${item.gubunKind ne '1'}">
								기타<br>
							</c:if>
							<c:out value="${item.gubunName}" />						
						</td>
						<td><fmt:formatNumber value="${item.contract}" type="number" /></td>
						<td><fmt:formatNumber value="${item.inCollected}" type="number" /></td>
						<td><fmt:formatNumber value="${item.outCollected}" type="number" /></td>
						<td><fmt:formatNumber value="${item.collectedSum}" type="number" /></td>
						<td><fmt:formatNumber value="${item.inDividend}" type="number" /></td>
						<td><fmt:formatNumber value="${item.outDividend}" type="number" /></td>
						<td><fmt:formatNumber value="${item.dividendSum}" type="number" /></td>
						<td><fmt:formatNumber value="${item.inDividendOff}" type="number" /></td>
						<td><fmt:formatNumber value="${item.outDividendOff}" type="number" /></td>
						<td><fmt:formatNumber value="${item.dividendOffSum}" type="number" /></td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(royaltyList) gt 0}">
					<tr align="center">
						<td class="HtdBg">총계</td>
						<td><fmt:formatNumber value="${contractSum}" type="number" /></td>
						<td><fmt:formatNumber value="${inCollectedSum}" type="number" /></td>
						<td><fmt:formatNumber value="${outCollectedSum}" type="number" /></td>
						<td></td>
						<td><fmt:formatNumber value="${inDividendSum}" type="number" /></td>
						<td><fmt:formatNumber value="${outDividendSum}" type="number" /></td>
						<td></td>
						<td><fmt:formatNumber value="${inDividendOffSum}" type="number" /></td>
						<td><fmt:formatNumber value="${outDividendOffSum}" type="number" /></td>
						<td></td>
					</tr>
				</c:if>
				<c:if test="${fn:length(royaltyList) eq '0'}">
					<tr><td align="center" colspan="17"height="25">등록된 신탁사용료 내용이 없습니다.</td></tr>
				</c:if>						
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>							
		<!-- 수수료PART START -->
	<tr>
		<td>	
			<table width="405" cellpadding="0" cellspacing="0" border="1">
			
				<tr align="center">
					<td width="95" height="33" class="HtdBg" rowspan="2">구분</td>
					<td width="190" class="HtdBg" colspan="3">수수료</td>
					<td width="120" class="HtdBg" colspan="3">수수료율</td>
					<td class="HtdBg" colspan="4" rowspan="2">비고</td>
				</tr>
								
				<tr align="center">
					<td width="63" height="25" class="HtdBg">국내</td>
					<td width="63" class="HtdBg">해외</td>
					<td width="64" class="HtdBg">소계</td>
					<td width="40" class="HtdBg">국내</td>
					<td width="40" class="HtdBg">해외</td>
					<td width="40" class="HtdBg">소계</td>
				</tr>
				<c:forEach var="item" items="${royaltyList}" varStatus="status">
					<tr align="center">
						<td class="HtdBg">
							<c:if test="${item.gubunKind ne '1'}">
								기타<br>
							</c:if>
							<c:out value="${item.gubunName}" />
						</td>
						<td><fmt:formatNumber value="${item.inChrg}" type="number" /></td>
						<td><fmt:formatNumber value="${item.outChrg}" type="number" /></td>
						<td><fmt:formatNumber value="${item.chrgSum}" type="number" /></td>
						<td><fmt:formatNumber value="${item.inChrgRate}" type="number" /></td>
						<td><fmt:formatNumber value="${item.outChrgRate}" type="number" /></td>
						<td><fmt:formatNumber value="${item.chrgRateSum}" type="number" /></td>
						<td colspan="4"><c:out value="${item.trustMemo}" /></td>
					</tr>						
				</c:forEach>
				<c:if test="${fn:length(royaltyList) gt 0}">
					<tr align="center">
						<td class="HtdBg">총계</td>
						<td><fmt:formatNumber value="${inChrgSum}" type="number" /></td>
						<td><fmt:formatNumber value="${outChrgSum}" type="number" /></td>
						<td></td>
						<td><fmt:formatNumber value="${inChrgRateSum}" type="number" /></td>
						<td><fmt:formatNumber value="${outChrgRateSum}" type="number" /></td>
						<td></td>
						<td colspan="4"></td>
					</tr>
				</c:if>
				<c:if test="${fn:length(royaltyList) eq '0'}">
					<tr><td align="center" colspan="21"height="25">등록된 신탁사용료 내용이 없습니다.</td></tr>
				</c:if>	
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;* 기타 항목 추가 가능</td>
	</tr>
	<tr>
		<td>&nbsp;* 국내.해외 구분은 저작물을 기준으로 함</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>2. <c:out value="${year}" />년 학교교육목적, 도서관 보상금</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;▷저작권자</td>
	</tr>
	<tr>
		<td align="right">
			<table>
				<tr><td colspan="11" align="right">(단위 건, 천원, %)</td></tr>
			</table>
		</td>
	</tr>
					
	<tr>
		<td>
			<table height="31" cellpadding="0" cellspacing="0" border="1">
				<tr>
					<td width="154" class="HtdBg" colspan="2">구분</td>
					<td width="75" class="HtdBg">이용단체</td>
					<td width="99" class="HtdBg">징수액</td>
					<td width="99" class="HtdBg">분배액</td>
					<td width="99" class="HtdBg">미분배액</td>
					<td width="99" class="HtdBg">수수료</td>
					<td width="94" class="HtdBg">수수료율</td>
					<td class="HtdBg" colspan="3">비고</td>
				</tr>
				<c:forEach var="item" items="${copyrightList}" varStatus="status">
					<tr align="center">
						<c:if test="${status.index eq '0'}">
							<td width="90" rowspan="5" class="HtdBg"> 교과용<br>도서<br>보상금</td>
						</c:if>					
						<c:if test="${status.index eq '5'}">
							<td rowspan="5" class="HtdBg"> 수업목적의<br>복제등<br>보상금</td>
						</c:if>					
						<c:if test="${status.index eq '10'}">
							<td colspan="2" height="50" class="HtdBg"> 도서관보상금</td>
						</c:if>					
						<c:if test="${status.index ne '10'}">
							<td width="70" height="25" class="HtdBg"><c:out value="${item.gubunName}" /></td>
						</c:if>
						<td width="76"><fmt:formatNumber value="${item.organization}" type="number" /></td>
						<td width="100"><fmt:formatNumber value="${item.collected}" type="number" /></td>
						<td width="100"><fmt:formatNumber value="${item.dividend}" type="number" /></td>
						<td width="100"><fmt:formatNumber value="${item.dividendOff}" type="number" /></td>
						<td width="100"><fmt:formatNumber value="${item.chrg}" type="number" /></td>
						<td width="100"><fmt:formatNumber value="${item.chrgRate}" type="number" /></td>
						<td colspan="3"><c:out value="${item.memo}" /></td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(copyrightList) eq '0'}">
					<tr><td align="center" colspan="11" height="25">등록된 보상금(저작권자) 내용이 없습니다.</td></tr>
				</c:if>	
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;▷실연자</td>
	</tr>
	<tr>
		<td align="right">
			<table>
				<tr><td colspan="11" align="right">(단위 건, 천원, %)</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table height="31" cellpadding="0" cellspacing="0" border="1">
				<tr>
					<td width="154" class="HtdBg" colspan="2">구분</td>
					<td width="75" class="HtdBg">이용단체</td>
					<td width="99" class="HtdBg">징수액</td>
					<td width="99" class="HtdBg">분배액</td>
					<td width="99" class="HtdBg">미분배액</td>
					<td width="99" class="HtdBg">수수료</td>
					<td width="94" class="HtdBg">수수료율</td>
					<td class="HtdBg" colspan="3">비고</td>
				</tr>
				<c:forEach var="item" items="${executeList}" varStatus="status">
					<tr align="center">
						<c:if test="${status.index eq '0'}">
							<td width="90" rowspan="5" class="HtdBg">판매용<br>음반방송<br>보상금</td>
						</c:if>					
						<c:if test="${status.index eq '5'}">
							<td rowspan="5" class="HtdBg">음반의 디음송<br>보상금</td>
						</c:if>					
						<c:if test="${status.index eq '10'}">
							<td rowspan="5" class="HtdBg">판매용<br>음반의 공연<br>보상금</td>
						</c:if>			
						<td width="70" class="HtdBg"><c:out value="${item.gubunName}" /></td>
						<c:if test="${status.index eq '0' or status.index eq '5' or status.index eq '10'}">
							<td rowspan="4" width="76"></td>
						</c:if>					
						<c:if test="${status.index eq '4' or status.index eq '9' or status.index eq '14'}">
							<td width="76" ><fmt:formatNumber value="${item.organization}" type="number" /></td>
						</c:if>					
						<c:if test="${status.index eq '0' or status.index eq '5' or status.index eq '10'}">
							<td width="100" rowspan="4"></td>
						</c:if>					
						<c:if test="${status.index eq '4' or status.index eq '9' or status.index eq '14'}">
							<td width="100"><fmt:formatNumber value="${item.collected}" type="number" /></td>
						</c:if>					
						<td width="100"><fmt:formatNumber value="${item.dividend}" type="number" /></td>
						<td width="100"><fmt:formatNumber value="${item.dividendOff}" type="number" /></td>
						<td width="100"><fmt:formatNumber value="${item.chrg}" type="number" /></td>
						<td width="100"><fmt:formatNumber value="${item.chrgRate}" type="number" /></td>
						<td colspan="3"><c:out value="${item.memo}" /></td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(executeList) eq '0'}">
					<tr><td align="center" colspan="11" height="25">등록된 보상금(실연자) 내용이 없습니다.</td></tr>
				</c:if>	
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;▷음반제작자</td>
	</tr>
		<tr>
		<td align="right">
			<table>
				<tr><td colspan="11" align="right">(단위 건, 천원, %)</td></tr>
			</table>
		</td>
		</tr>
		<tr>
			<td>
				<table  height="31" cellpadding="0" cellspacing="0" border="1">
					<tr>
						<td width="154" class="HtdBg" colspan="2">구분</td>
						<td width="75" class="HtdBg">이용업체</td>
						<td width="99" class="HtdBg">징수액</td>
						<td width="99" class="HtdBg">분배액</td>
						<td width="99" class="HtdBg">미분배액</td>
						<td width="99" class="HtdBg">수수료</td>
						<td width="94" class="HtdBg">수수료율</td>
						<td class="HtdBg" colspan="3">비고</td>
					</tr>
					<c:forEach var="item" items="${makerList}" varStatus="status">
						<tr align="center">
							<c:if test="${status.index eq '0'}">
								<td width="161" class="HtdBg" colspan="2">판매용음반의<br>방송보상금</td>
							</c:if>			
							<c:if test="${status.index eq '1'}">
								<td class="HtdBg" colspan="2">음반의<br>디음송보상금</td>
							</c:if>			
							<c:if test="${status.index gt '1'}">
								<td class="HtdBg" colspan="2">판매용음반의<br>공연보상금</td>
							</c:if>			
							<td width="760"><fmt:formatNumber value="${item.organization}" type="number" /></td>
							<td width="100"><fmt:formatNumber value="${item.collected}" type="number" /></td>
							<td width="100"><fmt:formatNumber value="${item.dividend}" type="number" /></td>
							<td width="100"><fmt:formatNumber value="${item.dividendOff}" type="number" /></td>
							<td width="100"><fmt:formatNumber value="${item.chrg}" type="number" /></td>
							<td width="100"><fmt:formatNumber value="${item.chrgRate}" type="number" /></td>
							<td colspan="3"><c:out value="${item.memo}" /></td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(makerList) eq '0'}">
						<tr><td align="center" colspan="11" height="25">등록된 보상금(음반제작자) 내용이 없습니다.</td></tr>
					</c:if>					
				</table>
			</td>
		</tr>
	</table>
	
</body>
</html>