<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="${title1}" />
	<jsp:param name="nav2" value="${title2}" />
	<jsp:param name="nav3" value="결제내역" />
	<jsp:param name="title" value="NONE" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />
	
		<h2 class="tit_c">결제내역</h2>
		<h2 class="tit_c_s pt0">${member.coName}</h2> 
		<div class="total_top_common pt0">
			<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
		</div>
		<div class="table_area list list_type1">
		
			<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
			
			<table>
				<caption></caption>
				<colgroup>
					<col style="width:69px" />
					<col style="width:141px" />
					<col style="width:144px" />
					<col style="width:256px" />
					<col style="width:145px" />
					<col style="width:150px" />
					<col style="width:150px" />
					<col style="width:auto" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>번호</th>
						<th>처리일자</th>
						<th>처리시간</th>
						<th>업무처리 상태</th>
						<%-- 
						<th>결제금액</th>
						--%>
						<th>결제방법</th>
						<th>결제처리상태</th>
						<th>결제취소</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><c:out value="${start - status.index}" /></td>
							<td><c:out value="${item.regDateStr}" /></td>
							<td><c:out value="${item.regTimeStr}" /></td>
							<td>
								<c:if test="${member.conditionCd eq '1' }">
									대리중개
								</c:if>
								<c:if test="${member.conditionCd eq '2' }">
									신탁
								</c:if>
								
								<c:if test="${member.statCd eq '1' }">
									신고신청
								</c:if>
								<c:if test="${member.statCd eq '3' }">
									변경신고신청
								</c:if>
							</td>
							<%-- 
							<td>결제금액</td>
							--%>
							<td>
								<c:if test="${item.payMthdCd eq '0402' }">
									카드결제
								</c:if>
								<c:if test="${item.payMthdCd eq '0403' }">
									계좌이체
								</c:if>
								<c:if test="${item.payMthdCd eq '0404' }">
									휴대폰
								</c:if>
								<c:if test="${item.payMthdCd eq '0407' }">
									카드포인트
								</c:if>
							</td>
							<td>
								<c:choose>
									<c:when test="${item.isSuccess eq '1' }">
										결제완료
									</c:when>
									<c:when test="${item.isSuccess eq '2' }">
										취소완료
									</c:when>
									<c:otherwise>
										<font style="color: red;">신청처리오류</font>
									</c:otherwise>
								</c:choose>
							</td>
							
							<td>
							<c:if test="${item.isSuccess eq '1' }">
									<c:if test="${item.payMthdCd ne '2' }">
									<a href="javascript:;" name="cancelCharge" class="btn_style2 list_view view_pop"
											data-cert-info-seq-no="<c:out value="${item.certInfoSeqNo}" />"
											data-member-seq-no="<c:out value="${data.memberSeqNo}" />"
											data-mall-seq="<c:out value="${item.mallSeq}" />">결제취소</a>
								</c:if>
								</c:if>
							</td>
							
						</tr>
					</c:forEach>
					<c:if test="${fn:length(list) eq '0'}">
						<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
					</c:if>
				</tbody>
			</table>
		
			</form>
			
		</div>
		<!-- //table_area -->
		<div id="pagination" class="pagination button_area mt22">
			<ul>
	       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
			</ul>
			<div class="right">
				<c:if test="${member.conditionCd eq '1' }">
					<a href="<c:url value="/admin/sub/${uri2}" />" class="btn btn_style1">이전</a>
					<a href="<c:url value="/admin/sub/companies/${memberSeqNo}/charges/excel" />" class="btn_style1">엑셀저장</a>
				</c:if>
				<c:if test="${member.conditionCd ne '1' }">
					<a href="<c:url value="/admin/trust/${uri2}" />" class="btn btn_style1">이전</a>
					<a href="<c:url value="/admin/trust/companies/${memberSeqNo}/charges/excel" />" class="btn_style1">엑셀저장</a>
				</c:if>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_appl" />
	<jsp:param name="includes" value="company_tab" />
</jsp:include>
