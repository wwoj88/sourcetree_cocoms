<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<title>직원수별 대리중개업체수 </title>
</head>
<body>

	<table width="670" cellpadding="0" cellspacing="0" border="1">
		<tr>
			<td colspan="9"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 직원수별 대리중개업체수 입니다.</td>
		</tr>
		<tr>
			<td colspan="9" align="right">(단위) 가로:연도, 세로:명</td>
		</tr>
		<tr align="center">
			<td></td>
			<td>0~5</td>
			<td>6~10</td>
			<td>11~30</td>
			<td>31~50</td>
			<td>51~100</td>
			<td>101~200</td>
			<td>201~</td>
			<td>계</td>
		</tr>
		<c:forEach var="item" items="${list}" varStatus="status">
			<tr align="center">
				<td><c:out value="${item.tit}" /></td>
				<td><fmt:formatNumber value="${item.val1}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val2}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val3}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val4}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val5}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val6}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val7}" type="number" /></td>
				<td><fmt:formatNumber value="${item.sum}" type="number" /></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(list) eq '0'}">
			<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
		</c:if>
	</table>

</body>
</html>