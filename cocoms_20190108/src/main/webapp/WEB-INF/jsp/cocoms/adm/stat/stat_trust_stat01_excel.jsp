<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<title>저작권 종류별 신탁사용료 </title>
</head>
<body>
	<table cellpadding="0" cellspacing="1" border="1">
		<tr>
			<td colspan="17" height="30"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 저작권 종류별 신탁사용료</td>
		</tr>
		<tr>
			<td colspan="17" align="right">(단위 건, 천원, %)</td>
		</tr>
		<tr>
			<td align="center">구분</td>
			<td align="center">계약(이용건수)</td>
			<td colspan="3" align="center">징수액(천원)</td>
			<td colspan="3" align="center">분배액(천원)</td>
			<td colspan="3" align="center">미분배액(천원)</td>
			<td colspan="3" align="center">수수료(천원)</td>
			<td colspan="3" align="center">수수료율(%)</td>
		</tr>
		<tr align="center">
			<td></td>
			<td>&nbsp;</td>
			<td height="25">국내</td>
			<td>해외</td>
			<td>소계</td>
			<td>국내</td>
			<td>해외</td>
			<td>소계</td>
			<td>국내</td>
			<td>해외</td>
			<td>소계</td>
			<td>국내</td>
			<td>해외</td>
			<td>소계</td>
			<td>국내</td>
			<td>해외</td>
			<td>소계</td>
		</tr>
		<c:forEach var="item" items="${list}" varStatus="status">
			<tr align="center">
				<td>
					<c:if test="${item.kind eq '1'}">
						<c:out value="${item.name}" />
					</c:if>
					<c:if test="${item.kind ne '1'}">
						기타(<c:out value="${item.name}" />)
					</c:if>
				</td>
				<td><fmt:formatNumber value="${item.cnt}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val11}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val12}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val13}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val21}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val22}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val23}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val31}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val32}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val33}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val41}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val42}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val43}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val51}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val52}" type="number" /></td>
				<td><fmt:formatNumber value="${item.val53}" type="number" /></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(list) gt '1'}">
			<tr align="center">
				<td>합계</td>
				<td><fmt:formatNumber value="${sum.cnt}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val11}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val12}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val13}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val21}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val22}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val23}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val31}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val32}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val33}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val41}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val42}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val43}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val51}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val52}" type="number" /></td>
				<td><fmt:formatNumber value="${sum.val53}" type="number" /></td>
			</tr>		
		</c:if>
		<c:if test="${fn:length(list) eq '0'}">
			<tr><td align="center" colspan="17" height="25">등록된 신탁사용료 내용이 없습니다.</td></tr>
		</c:if>
	</table>
</body>
</html>