<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="저작권중개업" />
	<jsp:param name="nav2" value="업체현황" />
</jsp:include>

<div class="search_top">
	<div class="table_area list_top">
		<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
			<input type="hidden" name="conditionCd" value="${data.conditionCd}" />
			<input type="hidden" name="conDetailCd" value="${data.conDetailCd}" />

			<table>
				<caption>업체현황</caption>
				<colgroup>
					<col style="width: 125px" />
					<col style="width: 395px" />
					<col style="width: 125px" />
					<col style="width: 395px" />
				</colgroup>
				<tbody>
					<tr>
						<th>신청일/등록일</th>
						<td colspan="3">
							<select name="searchCondition" class="select_style">
								<option value="regDate2" selected="selected">신청일</option>
								<option value="appRegDate">등록일</option>
							</select>
							<input type="text" name="searchFromDate" class="event-date input_style1" value="${data.searchFromDate}" />
							<input type="text" name="searchToDate" class="event-date2 input_style1" value="${data.searchToDate}" />
						</td>
					</tr>
					<tr>
						<th>회원구분</th>
						<td>
							<c:if test="${data.coGubunCd eq '1'}">
								<c:set var="coGubunCd1" value="selected" />
							</c:if>
							<c:if test="${data.coGubunCd eq '2'}">
								<c:set var="coGubunCd2" value="selected" />
							</c:if>
							<select name="coGubunCd" class="select_style">
								<option value="">전체</option>
								<option value="1" ${coGubunCd1}>개인</option>
								<option value="2" ${coGubunCd2}>단체/법인</option>
							</select>

							<c:if test="${data.conDetailCd eq '1'}">
								<c:set var="conDetailCd1" value="checked" />
								<c:set var="conDetailCd2" value="" />
							</c:if>
							<c:if test="${data.conDetailCd eq '2'}">
								<c:set var="conDetailCd1" value="" />
								<c:set var="conDetailCd2" value="checked" />
							</c:if>
							<c:if test="${data.conDetailCd eq '3'}">
								<c:set var="conDetailCd1" value="checked" />
								<c:set var="conDetailCd2" value="checked" />
							</c:if>

							<div class="btm_form_box">
								<input type="checkbox" id="conDetailCd1" value="1" ${conDetailCd1} />
								<label for="conDetailCd1">대리</label>
							</div>
							<div class="btm_form_box">
								<input type="checkbox" id="conDetailCd2" value="2" ${conDetailCd2} />
								<label for="conDetailCd2">중개</label>
							</div>
						</td>
						<th>신고번호</th>
						<td>
							<input type="text" name="appNo" class="input_style1" value="${data.appNo}" />
						</td>
					</tr>
					<tr>
						<th>사업자명</th>
						<td>
							<input type="text" name="coName" class="input_style1" value="${data.coName}" />
						</td>
						<th>대표자명</th>
						<td>
							<input type="text" name="ceoName" class="input_style1" value="${data.ceoName}" />
						</td>
					</tr>
				</tbody>
			</table>
			<a href="" id="search" class="btn_style2 btn_search">검색</a>
		</form>
	</div>
	<!-- //table_area -->
</div>
<div class="total_top_common">
	<p class="total">
		전체 : <strong><c:out value="${total}" />건</strong>
	</p>
	<%-- <div class="list_btn">
		<a href="" id="sendSms" class="btn_style2">SMS발송</a> <a href="" id="sendEmail" class="btn_style2">이메일발송</a> <a href="" id="changeStatCd_6" class="btn_style3">신고취소</a> <a href="" id="changeStatCd_5" class="btn_style3">신고정지</a> <a href="" id="changeStatCd_59" class="btn_style3">신고정지해제</a> <a href="<c:url value="/admin/sub/companies/excel" />" class="btn_style1">엑셀저장</a>
	</div> --%>
</div>
<div class="table_area list list_type1">
	<table id="data">
		<caption>실적보고</caption>
		<colgroup>
			<col style="width: auto" />
			<col style="width: auto" />
			<col style="width: auto" />
			<col style="width: auto" />
			<col style="width: auto" />
			<col style="width: auto" />
			<col style="width: 84px" />
			<col style="width: 84px" />
			<col style="width: 84px" />
			<col style="width: 84px" />
			<col style="width: 84px" />
			<col style="width: 68px" />
			<col style="width: 68px" />
			<col style="width: 86px" />
		</colgroup>
		<thead>
			<tr>
				<th>
					<div class="btm_form_box">
						<input type="checkbox" id="checkAll">
						<label for="checkAll">선택</label>
					</div>
				</th>
				<th>번호</th>
				<th>신고번호</th>
				<th>회사명</th>
				<th>신청일</th>
				<th>등록일</th>
				<th>상태</th>
				<th>신고서</th>
				<th>신고증</th>
				<th>변경신고서</th>
				<th>변경신고증</th>
				<th>발급현황</th>
				<th>이력정보</th>
				<th>실적</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${list}" varStatus="status">
				<tr>
					<td>
						<div class="btm_form_box">
							<input type="checkbox" id="checkbox_${item.memberSeqNo}" value="${item.memberSeqNo}" data-member-seq-no="${item.memberSeqNo}" data-tel="${item.sms}" data-email="${item.ceoEmail}" data-agree="${item.smsAgree}" data-stat-cd="${item.statCd}" />
							<label for="checkbox_${item.memberSeqNo}">체크</label>
						</div>
					</td>
					<td>
						<c:out value="${start - status.index}" />
					</td>
					<td>
						<c:out value="${item.appNoStr}" />
					</td>
					<td>
						<a href="<c:url value="/admin/sub/companies/${item.memberSeqNo}" />"><c:out value="${item.coName}" escapeXml="false" /></a>
					</td>
					<td>
						<c:out value="${item.regDate2Str}" />
					</td>
					<td>
						<c:out value="${item.appRegDateStr}" />
					</td>
					<td>
						<c:out value="${item.statCdStr}" />
					</td>
					<td>
						<c:if test="${item.down1}">
							<a href="<c:url value="/admin/sub/companies/${item.memberSeqNo}/docs/1" />" title="" class="fileDown">파일 다운로드</a>
						</c:if>
					</td>
					<td>
						<c:if test="${item.down2}">
							<a href="<c:url value="/admin/sub/companies/${item.memberSeqNo}/docs/2" />" title="" class="fileDown">파일 다운로드</a>
						</c:if>
					</td>
					<td>
						<c:if test="${item.down3}">
							<a href="<c:url value="/admin/sub/companies/${item.memberSeqNo}/docs/3" />" title="" class="fileDown">파일 다운로드</a>
						</c:if>
					</td>
					<td>
						<c:if test="${item.down4}">
							<a href="<c:url value="/admin/sub/companies/${item.memberSeqNo}/docs/4" />" title="" class="fileDown">파일 다운로드</a>
						</c:if>
					</td>
					<td>
						<c:if test="${item.issCnt gt 0}">
							<a href="<c:url value="/admin/sub/companies/${item.memberSeqNo}" />/issues" title="iconSearch" class="iconSearch">검색</a>
						</c:if>
					</td>
					<td>
						<c:if test="${item.enrollmentCnt gt 0}">
							<a href="<c:url value="/admin/sub/companies/${item.memberSeqNo}" />/enrollment" title="iconSearch" class="iconSearch">검색</a>
						</c:if>
					</td>
					<td>
						<c:if test="${item.reportCnt gt 0}">
							<a href="<c:url value="/admin/reports/sub/${item.memberSeqNo}" />" class="btn_style2 list_view">보기</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${fn:length(list) eq '0'}">
				<tr>
					<td colspan="15">조회된 데이터가 없습니다.</td>
				</tr>
			</c:if>
		</tbody>
	</table>
</div>
<!-- //table_area -->
<div id="pagination" class="pagination">
	<ui:pagination paginationInfo="${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/sms_popup.jsp" flush="true" />

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_appl" />
	<jsp:param name="includes" value="sms_popup" />
</jsp:include>
