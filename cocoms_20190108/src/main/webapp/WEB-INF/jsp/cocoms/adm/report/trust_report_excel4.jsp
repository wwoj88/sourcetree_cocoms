<%@ page language="java" contentType="application/vnd.ms-excel; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>문화체육관광부</title>
<style>
	br{mso-data-placement:same-cell;}
	.HtdBg {
		background:#CCCCFF;
		text-align:center;
	}	
</style>
</head>
<body>

	<table width="757" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="center"><span style="font-size:15pt">▣ 실적보고-사업계획</span></td>
		</tr>
		<tr>
			<td align="right">인쇄일 : <c:out value="${date}" /></td>
		</tr>
		<tr>
			<td align="right"><c:out value="${report.coName}" /></td>
		</tr>
		<tr>
			<td align="left">1. 사업목표(<c:out value="${year}" />년)</td>
		</tr>
		<tr>
			<td><table border=1 width="100%"><tr><td><c:out value="${report.businessGoal}" /></td></tr></table></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="left">2. 추진방향(<c:out value="${year}" />년)</td>
		</tr>
		<tr>
			<td ><table border=1 width="100%"><tr><td><c:out value="${report.prop}" /></td></tr></table></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="left">3. 첨부자료(<c:out value="${year}" />년)</td>
		</tr>
		<tr>
			<td>
				<c:if test="${!empty report.filename5}">
					당해연도 사업계획서: <c:out value="${report.filename5}" /><br />
				</c:if>
				<c:if test="${!empty report.filename6}">
					사용료 징수.분배계획: <c:out value="${report.filename6}" /><br />
				</c:if>
				<c:if test="${!empty report.filename7}">
					보상금 징수.분배계획: <c:out value="${report.filename7}" /><br />
				</c:if>
				<c:if test="${!empty report.filename8}">
					예산편성안: <c:out value="${report.filename8}" /><br />
				</c:if>
				<c:if test="${!empty report.filename9}">
					정기총회 자료집: <c:out value="${report.filename9}" /><br />
				</c:if>
			</td>
		</tr>
	</table>
	
</body>
</html>