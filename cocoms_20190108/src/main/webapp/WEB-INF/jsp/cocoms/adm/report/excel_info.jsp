<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="위탁관리저작물 관리" />
	<jsp:param name="nav2" value="통계/업체 상세내역" />
</jsp:include>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>

	<c:forEach var="info" items="${info}" varStatus="listStatus">
		<c:set var="reptYmd" value="${info.RGST_DTTM }"/>
		<c:set var="yyyymm" value="${fn:substring(info.RGST_DTTM,0,4) }${fn:substring(info.RGST_DTTM ,5,7) }" />
	</c:forEach>
		<div class="total_top_common" style="padding-top:0">
			
			
			<h4><b>▶${fn:substring(reptYmd,0,4) }년 ${fn:substring(reptYmd,5,7) }월 월별 위탁관리저작물 보고현황</b></h4>
		</div>
	<!-- //table_area -->
	<form id="searchForm" method="GET" >
			<input type="hidden" name="pageIndex" value="${page}" />
			</form>
	<div class="table_area list list_type1">
		<table id="data">
					<caption> 통계/업체 상세내역</caption>

					<colgroup>
						<col style="width:156px" />
						<col style="width:427px" />
						<col style="width:156px" />
						<col style="width:427px" />
						<col style="width:156px" />
						<col style="width:427px" />
					</colgroup>
						
					<tbody>
			
				<c:forEach var="info" items="${info}" varStatus="listStatus">
						<tr>
							<th>보고기관</th>
							<td >
								<c:out value="${info.COMM_NAME}" escapeXml="false"/>
							</td>
							<th>최종 보고일</th>
							<td >
								<c:out value="${info.RGST_DTTM}" escapeXml="false"/>
							</td>
							
						</tr>
						<tr>
							<th>담당자 직위</th>
							<td>
								<c:out value="${info.REPT_CHRR_POSI}"  escapeXml="false"/>
								
							</td>
							<th>담당자 전화번호</th>
							<td>
								<c:out value="${info.REPT_CHRR_TELX}"  escapeXml="false"/>
						
							</td>
						</tr>
						<tr>
							<th>담당자</th>
							<td>
								<c:out value="${info.REPT_CHRR_NAME}"  escapeXml="false"/>
							</td>
							<th>담당자 이메일</th>
							<td>
								<c:out value="${info.REPT_CHRR_MAIL}"  escapeXml="false"/>
							 
							</td>
							
						</tr>
							</c:forEach>
					
							
							
							
							
					
					</tbody>
				</table>
				</div>
		<div class="total_top_common" style="padding-top:0">
			
		</div>
	
		
		<div class="table_area list list_type1">
			<table>
				<caption> 상세조회</caption>
				<colgroup>
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
				</colgroup>
				<thead>
					<tr> 	 
						
						<th>보고건수</th>						
						<th>음악</th>
						<th>어문</th>
						<th>방송대본</th>
						<th>영화</th>
						<th>방송</th>
						<th>뉴스</th>
						<th>미술</th>
						<th>이미지</th>
						<th>사진</th>
						<th>기타</th>
					
						
					</tr>
				</thead>
				<tbody>
				
				<c:choose>
				<c:when test="${empty list}">
				<td colspan="13" class="text-center">게시물이 없습니다.(년도 검색을 해주세요.)</td>
				</c:when>
				<c:otherwise>
					<c:forEach var="count" items="${list}" varStatus="listStatus">
					<tr>
						<th><c:out value="${count.CNT_TOT }"/></th>
						<td class="text-center"><c:out value="${count.CNT_1 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_2 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_3 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_4 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_5 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_6 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_7 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_8 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_12 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_9 }"/></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				
				
				</tbody>
				
			</table>
		</div>
			<div class="total_top_common" style="padding-top:0">
			
		</div>
			<!-- //table_area -->
	<div class="table_area list list_type1">
		<table id="data">
				<caption>등록현황</caption>
				<colgroup>
					
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />

				</colgroup>
				<thead>
					<tr> 	 
						
						<th>보고년월</th>
						<th>저작물 관리번호</th>
						<th>국내외구분</th>
						<th>저작물분류</th>
						<th>저작물명</th>
						<th>저작물부제</th>
						<th>창작년도(발행연월일)</th>
						<th>위탁관리구분</th>
						<th>작성기관명</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${uploadList}" varStatus="status">
						<tr>
							
							<td>${item.REPT_YMD}</td>
							<td>${item.COMM_WORKS_ID}</td>
							<td>
								<c:if test="${item.NATION_CD == '1' }">국내</c:if>
								<c:if test="${item.NATION_CD == '2' }">국외</c:if>
							</td>
							<td>
								<c:if test="${item.GENRE_CD == '1' }">음악</c:if>
								<c:if test="${item.GENRE_CD == '2' }">어문</c:if>
								<c:if test="${item.GENRE_CD == '3' }">방송대본</c:if>
								<c:if test="${item.GENRE_CD == '4' }">영화</c:if>
								<c:if test="${item.GENRE_CD == '5' }">방송</c:if>
								<c:if test="${item.GENRE_CD == '6' }">뉴스</c:if>
								<c:if test="${item.GENRE_CD == '7' }">미술</c:if>
								<c:if test="${item.GENRE_CD == '8' }">이미지</c:if>
								<c:if test="${item.GENRE_CD == '99' }">기타</c:if>
							</td>
							<td>${item.WORKS_TITLE}</td>
							<td>${item.WORKS_SUB_TITLE}</td>
							<td>${item.CRT_YEAR}</td>
							<td>		
								<c:if test="${item.COMM_MGNT_CD == '0' }">없음</c:if>
								<c:if test="${item.COMM_MGNT_CD != '0' }"><c:out value="${item.COMM_MGNT_CD}"/></c:if>
								</td>
							<td>${item.RGST_ORGN_NAME}</td>
						
							
						</tr>
					</c:forEach>
					<c:if test="${empty uploadList}">
						<tr><td colspan="10">조회된 데이터가 없습니다.</td></tr>
					</c:if>
				</tbody>
			</table>
	</div>	
				
	<!-- //table_area -->
	<div id="pagination" class="pagination" style="height:30px">
	<ul><ui:pagination paginationInfo="${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" /></ul>
	</div>
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_excel" />
</jsp:include>

