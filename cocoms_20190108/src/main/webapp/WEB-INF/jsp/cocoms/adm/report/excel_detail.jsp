<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="위탁관리저작물 관리" />
	<jsp:param name="nav2" value="통계 / 업체 보고현황" />
</jsp:include>
<c:forEach var="info" items="${info}" varStatus="listStatus">
	<c:set var="reptYmd2" value="${info.REPT_YMD }"></c:set>
	<c:set var="yyyymm" value="${fn:substring(info.RGST_DTTM,0,4) }${fn:substring(info.RGST_DTTM ,5,7) }" />
</c:forEach>
<div class="total_top_common">


	<div class="list_btn">

		<a href="<c:url value="/admin/excel/detailsdown/${yyyymm}" />" class="btn_style1" style="background-color: #1e4787">엑셀저장</a>
	</div>
</div>
<div class="table_area list list_type1">
	<table id="data">
		<caption>통계</caption>
		<colgroup>

			<col style="width: 10%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 7%" />
			<col style="width: 12%" />

		</colgroup>
		<thead>
			<tr>

				<th>작성기관명</th>
				<th>보고일자</th>
				<th>음악</th>
				<th>어문</th>
				<th>방송대본</th>
				<th>영화</th>
				<th>방송</th>
				<th>뉴스</th>
				<th>미술</th>
				<th>이미지</th>
				<th>사진</th>
				<th>기타</th>
				<th>저작물없음</th>
				<th>계</th>

			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${empty info}">
					<tr>
						<td colspan="13" class="text-center" style="height: 400px">게시물이 없습니다.(년도 검색을 해주세요.)</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:set var="sum1" value="0" />
					<c:set var="sum2" value="0" />
					<c:set var="sum3" value="0" />
					<c:set var="sum4" value="0" />
					<c:set var="sum5" value="0" />
					<c:set var="sum6" value="0" />
					<c:set var="sum7" value="0" />
					<c:set var="sum8" value="0" />
					<c:set var="sum12" value="0" />
					<c:set var="sum9" value="0" />
					<c:set var="sum10" value="0" />

					<c:forEach var="info" items="${info}" varStatus="listStatus">

						<tr>
							<td class="text-center">
								<a style="color: blue;" href="/cocoms/admin/excel/excelinfo/${info.CUR_TRST_ORGN_CODE}/${yyyymm}/${info.CONDITIONCD}"><b><u><c:out value="${info.CONAME }" /> </u></b></a>


							</td>
							<td class="text-center">
								<c:out value="${info.RGST_DTTM }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_1 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_2 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_3 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_4 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_5 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_6 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_7 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_8 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_12 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_9 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_10 }" />
							</td>
							<td class="text-center">
								<c:out value="${info.CNT_TOT }" />
							</td>
							<c:set var="sum1" value="${ sum1 + info.CNT1_ }" />
							<c:set var="sum2" value="${ sum2 + info.CNT2_ }" />
							<c:set var="sum3" value="${ sum3 + info.CNT3_ }" />
							<c:set var="sum4" value="${ sum4 + info.CNT4_ }" />
							<c:set var="sum5" value="${ sum5 + info.CNT5_ }" />
							<c:set var="sum6" value="${ sum6 + info.CNT6_ }" />
							<c:set var="sum7" value="${ sum7 + info.CNT7_ }" />
							<c:set var="sum8" value="${ sum8 + info.CNT8_ }" />
							<c:set var="sum12" value="${ sum12 + info.CNT12_ }" />
							<c:set var="sum9" value="${ sum9 + info.CNT9_ }" />
							<c:set var="sum10" value="${ sum10 + info.CNT10_ }" />
						</tr>
					</c:forEach>
					<tr>
						<td id="sum1Total" class="text-center" colspan="2">
							<b>합계</b>
						</td>
						<td id="sum2Total" class="text-center">
							<fmt:formatNumber value="${sum1}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum3Total" class="text-center">
							<fmt:formatNumber value="${sum2}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum4Total" class="text-center">
							<fmt:formatNumber value="${sum3}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum5Total" class="text-center">
							<fmt:formatNumber value="${sum4}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum6Total" class="text-center">
							<fmt:formatNumber value="${sum5}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum7Total" class="text-center">
							<fmt:formatNumber value="${sum6}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum8Total" class="text-center">
							<fmt:formatNumber value="${sum7}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum9Total" class="text-center">
							<fmt:formatNumber value="${sum8}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum9Total" class="text-center">
							<fmt:formatNumber value="${sum12}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum10Total" class="text-center">
							<fmt:formatNumber value="${sum9}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
						<td id="sum11Total" class="text-center">
							<b>-</b>
						</td>
						<td id="sum12Total" class="text-center">
							<fmt:formatNumber value="${sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9+sum12}" pattern="###,###,###,##0" groupingUsed="true" />
						</td>
					</tr>
					<tr>
						<c:forEach var="info_0" items="${info_0}" varStatus="listStatus">
							<c:set var="info_1" value="${info_0.CNT1 }" />
							<c:set var="info_2" value="${info_0.CNT2 }" />
							<c:set var="info_3" value="${info_0.CNT3 }" />
							<c:set var="info_4" value="${info_0.CNT4 }" />
							<c:set var="info_5" value="${info_0.CNT5 }" />
							<c:set var="info_6" value="${info_0.CNT6 }" />
							<c:set var="info_7" value="${info_0.CNT7 }" />
							<c:set var="info_8" value="${info_0.CNT8 }" />
							<c:set var="info_12" value="${info_0.CNT12 }" />
							<c:set var="info_9" value="${info_0.CNT9 }" />
							<c:set var="info_10" value="${info_0.CNT10 }" />
						</c:forEach>
						<td class="text-center" colspan="2">
							<b>보고기관수</b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_1 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_2 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_3 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_4 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_5 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_6 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_7 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_8 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_12 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_9 }" /></b>
						</td>
						<td class="text-center">
							<b><c:out value="${info_10 }" /></b>
						</td>
						<td class="text-center">
							</b>
						</td>
					</tr>
				</c:otherwise>
			</c:choose>

		</tbody>



	</table>
</div>
<!-- //table_area -->
<div id="pagination" class="pagination" style="height: 30px">

	<div style="float: left; display: inline-block; margin-top: 10px;">
		<font size="4pt">최초보고업체 *구분<br />보고일자: 보고업체 월별 최종 보고일자<br />보고기관수: 기관수(신규기관수/기존기관수)
		</font>
	</div>
</div>



<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_excel" />
</jsp:include>
