<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="${title1}" />
	<jsp:param name="nav2" value="${title2}" />
	<jsp:param name="nav3" value="발급현황" />
	<jsp:param name="title" value="NONE" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />

<h2 class="tit_c">이력 정보 상세 조회</h2>
<h2 class="tit_c_s pt0">${member.coName}</h2>
<div class="search_top">
	<div class="table_area list_top">
		<form id="enrollmentSearchForm" method="GET" action="<c:url value="/admin/sub/companies/${data.memberSeqNo}" />/enrollment">
			<input type="hidden" name="pageIndex" value="${page}" />
			<input type="hidden" name="conditionCd" value="${data.conditionCd}" />
			<%-- <input type="hidden" name="conDetailCd" value="${data.conDetailCd}" /> --%>
			<input type="hidden" name="searchGubun" value="1" />

			<table>
				<caption>업체현황</caption>
				<colgroup>
					<col style="width: 116px" />
					<col style="width: 405px" />
					<col style="width: 115px" />
					<col style="width: 535px" />
				</colgroup>
				<tbody>
					<tr>
						<th>업체담당자</th>
						<td>
				
								<input type="text" name="searchAccountManager" class="input_style1" value="${searchAccountManager}" />
						
						</td>
						<th>등록일자</th>
						<td>
							<input type="text" name="searchFromDate" class="event-date input_style1" value="${fn:substring(searchFromDate,0,4)}-${fn:substring(searchFromDate,4,6)}-${fn:substring(searchFromDate,6,8)}" readonly="readonly" />
							
							<input type="text" name="searchToDate" class="event-date input_style1" value="${fn:substring(searchToDate,0,4)}-${fn:substring(searchToDate,4,6)}-${fn:substring(searchToDate,6,8)}" readonly="readonly" />
						
						</td>
					</tr>
					<tr>
						<th>이력유형</th>
						<td>
							<select name="searchType" class="select_style" style="width: 100%;">
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<option value="D">D</option>
								<option value="E">E</option>
							</select>



						</td>
						<th>내용</th>
						<td>
							<c:if test="${searchContents == ''}">
								<input type="text" name="searchContents" class="input_style1" value="" />
							</c:if>
							<c:if test="${searchContents ne ''}">
								<input type="text" name="searchContents" class="input_style1" value="${searchContents}" />
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
			<a href=""><button class="btn_style2 btn_search" id="enrollsrch" style="width: 100px; height: 90px; line-height: 90px;">검색</button></a>
		</form>
	</div>
	<!-- //table_area -->
</div>
<br />
<br />
<div class="total_top_common pt0">
	<p class="total">
		전체 : <strong><c:out value="${total}" />건</strong>
	</p>
</div>
<div class="table_area list list_type1">

	<%-- <form id="searchForm" method="GET">
		<input type="hidden" name="pageIndex" value="${page}" /> --%>

		<table>
			<caption></caption>
			<colgroup>
				<col style="width: 110px" />
				<col style="width: 110px" />
				<col style="width: 110px" />
				<col style="width: 110px" />
				<col style="width: auto" />
			</colgroup>
			<thead>
				<tr>
					<th>등록일자</th>
					<th>담당자</th>
					<th>업체담당자</th>
					<th>이력유형</th>
					<th>내용(통화,우편물)</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${list}" varStatus="status">
					<tr>
						<td>
							<c:out value="${item.regdate}" />
						</td>
						<td>관리자</td>
						<td>
							<c:out value="${item.accountManager}" />
						</td>
						<td>
							<c:out value="${item.type}" />
						</td>
						<td>
							<c:out value="${item.contents}" />
						</td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(list) eq '0'}">
					<tr>
						<td colspan="15">조회된 데이터가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
<%-- 
	</form> --%>

</div>
<!-- //table_area -->
<div id="pagination" class="pagination button_area mt22">
	<ul>
		<ui:pagination paginationInfo="${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
	</ul>
	<div class="right">
		<!--  
				<c:if test="${member.conditionCd eq '1' }">
					<a href="<c:url value="/admin/sub/companies" />" class="btn btn_style1">이전</a>
				</c:if>
				<c:if test="${member.conditionCd ne '1' }">
					<a href="<c:url value="/admin/trust/companies" />" class="btn btn_style1">이전</a>
				</c:if>
				-->
		<a href="javascript:history.back()" class="btn btn_style1">이전</a>
	</div>
</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_appl" />
	<jsp:param name="includes" value="company_tab" />
</jsp:include>
