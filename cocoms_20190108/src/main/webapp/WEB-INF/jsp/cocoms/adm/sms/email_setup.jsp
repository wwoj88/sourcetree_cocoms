<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="SMS/이메일 관리" />
	<jsp:param name="nav2" value="이메일 자동발송 설정" />
</jsp:include>
	
		<!--  
		<select class="select_style w193 mb20">
			<option value="" selected="selected">전체</option>
			<option value=""></option>
		</select>
		-->
		<div class="table_area list sms_step_1">
			<table>
				<caption></caption>
				<colgroup>
					<col style="width:223px" />
					<col style="width:auto" />
					<col style="width:210px" />
					<col style="width:183px" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>구분</th>
						<th>제목</th>
						<th>자동메일 전송사용</th>
						<th>관리</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<input type="hidden" id="subject_${item.detailCode}" value="<c:out value="${item.subject}" />" />
						<input type="hidden" id="content_${item.detailCode}" value="<c:out value="${item.content}" />" />
						<input type="hidden" id="useYn_${item.detailCode}" value="<c:out value="${item.useYn}" />" />
						<tr>
							<td>
								<c:if test="${item.mainCode eq '1'}">
									사용자 - 로그인
								</c:if>
								<c:if test="${item.mainCode eq '2'}">
									사용자 - 마이페이지
								</c:if>
								<c:if test="${item.mainCode eq '3'}">
									사용자 - 저작권대리중개업
								</c:if>
								<c:if test="${item.mainCode eq '4'}">
									사용자 - 저작권신탁관리업
								</c:if>
								<c:if test="${item.mainCode eq '5'}">
									관리자 - 저작권중개업
								</c:if>
								<c:if test="${item.mainCode eq '6'}">
									관리자 - 저작권신탁관리업
								</c:if>
								<c:if test="${item.mainCode eq '7'}">
									관리자 - 실적보고
								</c:if>
							</td>
							<td class="subject"><c:out value="${item.subject}" /></td>
							<td>
								<c:set var="useY" value="" />
								<c:set var="useN" value="" />
								<c:if test="${item.useYn eq 'Y'}"><c:set var="useY" value="checked" /></c:if>
								<c:if test="${item.useYn eq 'N'}"><c:set var="useN" value="checked" /></c:if>
								<div class="btm_form_box">
									<input type="radio" id="useY_${item.detailCode}" name="useYn_${item.detailCode}" ${useY} disabled />
									<label for="useY_${item.detailCode}">전송함</label> 
								</div>
								<div class="btm_form_box">
									<input type="radio" id="useN_${item.detailCode}" name="useYn_${item.detailCode}" ${useN} disabled />
									<label for="useN_${item.detailCode}">전송안함</label> 
								</div>
							</td>
							<td>
								<a href="" name="preview" class="btn_style1" data-detail-code="${item.detailCode}">미리보기</a>
								<a href="" name="modify" class="btn_style2" data-detail-code="${item.detailCode}">수정하기</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<div id="pagination" class="pagination">
		</div>

	<div id="popupForm" class="pop_common" style="display: none;">
		<div class="inner w637">
			<h2 class="tit">회원가입완료 후 알림 받기</h2>
			<a href="" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont">
				<div class="txt_top_n">
					<p>자동완성 라이브러리</p>
					<div class="box">
						<ul>
							<li>받는사람 이름 : {user_name} </li>
							<li>기본Header : {email_header} </li>
							<li>가입일 : {enter_data} </li>
							<li>받는사람 이메일 : {user_email}</li>
							<li>기본Footer : {email_footer}</li>
							<li>탈퇴예정일 : {Withdrawal_data} </li>
							<li>받는사람 아이디 : {user_id외 2건} </li>
							<li>받는사람 사업자명 : {user_company} </li>
						</ul>
					</div>
				</div>
				<form id="formEmail" method="POST" action="<c:url value="/admin/email" />">
				<input type="hidden" name="detailCode" value="" />
				
				<table class="last">
					<caption>회원가입완료 후 알림 받기</caption>
					<colgroup>
						<col style="width:39px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>제목</th>
							<td><input type="text" name="subject" class="input_style1" value="" /></td>
						</tr>
						<tr>
							<th>내용</th>
							<td>
								<textarea name="content" class="input_style1 h282"></textarea>
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<div class="btm_form_box">
									<input type="radio" id="useY" name="useYn" value="Y" />
									<label for="useY">전송함</label> 
								</div>
								<div class="btm_form_box">
									<input type="radio" id="useN" name="useYn" value="N" />
									<label for="useN">전송안함</label> 
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="btn_area">
					<a href="" name="preview" class="btn_style1">미리보기</a>
					<a href="" id="submit" class="btn_style2">저장</a>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- //pop_common -->
	
	<div id="popupPreview" class="pop_common bg2" style="display: none;">
		<div class="inner">
			<h2 class="tit">회원가입완료 후 알림 받기 - 미리보기</h2>
			<a href="" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont_n">
				<form>
					<div class="table_area list list_type1">
						<table>
							<caption></caption>
							<colgroup>
								<col style="width:111px" />
								<col style="width:auto" />
							</colgroup>
							<tbody>
								<tr>
									<th>제목</th>
									<td class="subject"></td>
								</tr>
								<tr>
									<th>내용</th>
									<td class="content"><pre></pre></tr>
							</tbody>
						</table>
					</div>
					<!-- //table_area -->
				</div>
			</div>
		</div>
	</div>
	<!-- //pop_common -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_sms" />
</jsp:include>
