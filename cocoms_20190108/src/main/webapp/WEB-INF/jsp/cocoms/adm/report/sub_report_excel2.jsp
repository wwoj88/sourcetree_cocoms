<%@ page language="java" contentType="application/vnd.ms-excel; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>문화체육관광부</title>
<style>
	br{mso-data-placement:same-cell;}
	.HtdBg {
		background:#CCCCFF;
		text-align:center;
	}	
</style>
</head>
<body>

<table width="757" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center"><span style="font-size:15pt">▣ 실적보고</span></td>
	</tr>
	<tr>
		<td align="right">인쇄일 : <c:out value="${date}" /></td>
	</tr>
	<tr>
		<td align="left">▣<c:out value="${report.rptYear}" />년 업무보고 정보</td>
	</tr>
	<tr>
		<td align="left">1. 작성자</td>
	</tr>
	<tr>
		<td>
				<table width="480" cellpadding="0" cellspacing="0" border="1">
					<tr>
						<td width="80" class="HtdBg" colspan="2">성명</td>
						<td align="left" 	 width="160" align="left"  colspan="3" ><c:out value="${report.regName}" /></td>
						<td  width="80" class="HtdBg" colspan="2"> 직위</td>
						<td align="left" 	 width="160" align="left"   colspan="4" ><c:out value="${report.regPosi}" /></td>
					</tr>
					<tr>
						<td class="HtdBg"width="80" colspan="2">전화번호</td>
						<td style='mso-number-format:"\@";'  colspan="3" ><c:out value="${report.regTel}" /></td>
						<td class="HtdBg"width="80" colspan="2">이메일</td>
						<td style='mso-number-format:"\@";'  colspan="4" ><c:out value="${report.regEmail}" /></td>
					</tr>
				</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="left">2. 업체현황 </td>
	</tr>	
	<tr>
		<td>
				<table width="550" cellpadding="0" cellspacing="0" border="1">
					<tr>
						<td class="HtdBg" width="80" colspan="2">직원수</td>
						<td colspan="3" ><c:out value="${report.staffNum}" />명</td>
						<td class="HtdBg" width="80" colspan="2">상장 여부</td>
						<td align="left" colspan="4"><c:out value="${report.listingCdStr}" /></td>
					</tr>
				</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="left">▣<c:out value="${report.rptYear}" />년 실적보고 상세 (<c:out value="${writingKindList}" />)</td>
	</tr>	
	<tr>
		<td align="left">1. 저작물수</td>
	</tr>	
	<tr>
		<td align="right"> (단위: 건)</td>
	</tr>	
	<tr>
		<td align="left">
			<table width="747" height="31" cellpadding="0" cellspacing="0" border="1">
				<tr>
					<td width="114" class="HtdBg">종류</td>
				<%-- 	<td width="137" class="HtdBg">권리종류</td>--%>
					<td width="160" class="HtdBg" colspan="3">국내</td>
					<td width="160" class="HtdBg" colspan="3">해외</td>
					<td width="160" class="HtdBg" colspan="3">소계</td>
				</tr>
				<c:forEach var="item" items="${list}" varStatus="status">
					<tr align="center">
						<td width="120" class="HtdBg"><c:out value="${item.writingKindDesc}" /></td>
						<%-- <td width="138" class="HtdBg"><c:out value="${item.permKindDesc}" /></td> --%>
						<td width="161" colspan="3"><c:out value="${item.inWorkNum}" /></td>
						<td width="161" colspan="3"><c:out value="${item.outWorkNum}" /></td>
						<td width="167" colspan="3"><c:out value="${item.workSum}" /></td>
					</tr>
				</c:forEach>
				<tr align="center">
					<td height="25" class="HtdBg">합계</td>
					<%-- <td class="HtdBg"></td>--%>
					<td colspan="3"><c:out value="${sum.inWorkNum}" /></td>
					<td colspan="3"><c:out value="${sum.outWorkNum}" /></td>
					<td colspan="3"><c:out value="${sum.workSum}" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>		
	<tr>
		<td align="left">2. 사용료 및 수수료</td>
	</tr>	
	<tr>
		<td align="left">
			<table width="747" height="31" cellpadding="0" cellspacing="0" border="1">
				<tr>
					<td width="114" class="HtdBg">종류</td>
					<%--<td width="137" class="HtdBg">권리종류</td>--%>
					<td width="160" class="HtdBg" colspan=3>사용료(단위:천원)</td>
					<td width="160" class="HtdBg" colspan=3>수수료(단위:천원)</td>
					<td width="160" class="HtdBg" colspan=3>수수료율(단위:%)</td>
				</tr>
				<tr align="center">
					<td width="119" class="HtdBg" height="25"></td>
					<%--<td width="136" class="HtdBg"></td>--%>
					<td width="53" class="HtdBg">국내</td>
					<td width="53" class="HtdBg">해외</td>
					<td width="53" class="HtdBg">소계</td>
					<td width="53" class="HtdBg">국내</td>
					<td width="53" class="HtdBg">해외</td>
					<td width="53" class="HtdBg">소계</td>
					<td width="54" class="HtdBg">국내</td>
					<td width="54" class="HtdBg">해외</td>
					<td width="54" class="HtdBg">평균</td>
				</tr>
				<c:forEach var="item" items="${list}" varStatus="status">
					<tr align="center">
						<td class="HtdBg" height="25"><c:out value="${item.writingKindDesc}" /></td>
						<%-- <td class="HtdBg"><c:out value="${item.permKindDesc}" /></td> --%>
						<td><c:out value="${item.inRental}" /></td>
						<td><c:out value="${item.outRental}" /></td>
						<td><c:out value="${item.rentalSum}" /></td>
						<td><c:out value="${item.inChrg}" /></td>
						<td><c:out value="${item.outChrg}" /></td>
						<td><c:out value="${item.chrgSum}" /></td>
						<td><c:out value="${item.inChrgRate}" /></td>
						<td><c:out value="${item.outChrgRate}" /></td>
						<td><c:out value="${item.avgChrgRate}" /></td>
					</tr>
				</c:forEach>
				<tr align="center">
					<td class="HtdBg" height="25">합계</td>
					<%--<td class="HtdBg"></td> --%>
					<td><c:out value="${sum.inRental}" /></td>
					<td><c:out value="${sum.outRental}" /></td>
					<td><c:out value="${sum.rentalSum}" /></td>
					<td><c:out value="${sum.inChrg}" /></td>
					<td><c:out value="${sum.outChrg}" /></td>
					<td><c:out value="${sum.chrgSum}" /></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;</td>
	</tr>
	<tr>
		<td align="left">3. 첨부자료</td>
	</tr>
	<tr>
		<td align="left">&nbsp;</td>
	</tr>
	<tr>
		<td>4. 특기사항</td>
	</tr>
	<tr>
		<td><c:out value="${report.memo}" /></td>
	</tr>
</table>

</body>

</html>
