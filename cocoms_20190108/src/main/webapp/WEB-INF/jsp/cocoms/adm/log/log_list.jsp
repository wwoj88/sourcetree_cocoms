<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="로그 검색" />
	<jsp:param name="nav2" value="회원 로그 검색" />
</jsp:include>

<div class="search_top">
	<div class="table_area list_top">
		<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
			<input type="hidden" name="coGubun" value="${search.coGubun}" />

			<table>
				<caption>회원 로그 검색</caption>
				<colgroup>
					<col style="width: 125px" />
					<col style="width: 395px" />
					<col style="width: 125px" />
					<col style="width: 395px" />
				</colgroup>
				<tbody>
					<tr>
						<th>날짜</th>
						<td colspan="3">
							<input type="text" name="searchFromDate" class="event-date input_style1 w172" value="${search.searchFromDate}" />
							<input type="text" name="searchToDate" class="event-date2 input_style1 w172" value="${search.searchToDate}" />
						</td>
					</tr>
					<tr>
						<th>검색옵션</th>
						<td colspan="3">
							<c:if test="${search.searchCondition eq 'loginId'}">
								<c:set var="selectedLoginId" value="selected" />
							</c:if>
							<c:if test="${search.searchCondition eq 'ceoName'}">
								<c:set var="selectedCeoName" value="selected" />
							</c:if>
							<c:if test="${search.searchCondition eq 'coName'}">
								<c:set var="selectedCoName" value="selected" />
							</c:if>
							<select name="searchCondition" class="select_style w121">
								<option value="all">전체</option>
								<option value="loginId" ${selectedLoginId}>아이디</option>
								<option value="ceoName" ${selectedCeoName}>이름</option>
								<option value="coName" ${selectedCoName} escapeXml="false">업체명</option>
							</select>
							<input type="text" name="searchKeyword" class="input_style1" style="width: 300px;" value="${search.searchKeyword}" />
						</td>
					</tr>
				</tbody>
			</table>
			<a href="" id="search" class="btn_style2 btn_search" style="height: 84px; line-height: 84px;">검색</a>
		</form>
	</div>
	<!-- //table_area -->
</div>
<div class="total_top_common">
	<p class="total">
		전체 : <strong><c:out value="${total}" />건</strong>
	</p>
	<div class="list_btn">
		<a href="<c:url value="/admin/logs/excel" />" class="btn_style1">엑셀저장</a>
	</div>
</div>
<div class="table_area list list_type1 ">
	<div class="inner1">
		<table id="data" summary="개인회원">
			<caption>개인회원</caption>
			<colgroup>
				<col style="width: 73px" />
				<col style="width: 57px" />
				<col style="width: 250px" />
				<col style="width: 100px" />
				<col style="width: 145px" />
				<col style="width: 144px" />
				<col style="width: auto" />
			</colgroup>
			<thead>
				<tr>
					<th>번호</th>
					<th>구분</th>
					<th>날짜 및 시간</th>
					<th>아이디</th>
					<th>이름</th>
					<th>업체명</th>
					<th>이벤트</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${list}" varStatus="status">
					<tr>
						<td>
							<c:out value="${start - status.index}" />
						</td>
						<td>
							<c:out value="${item.type}" />
						</td>
						<td>
							<c:out value="${item.regDateStr}" />
							<c:out value="${item.regTimeStr}" />
						</td>
						<td>
							<c:out value="${item.loginId}" />
						</td>
						<td>
							<c:out value="${item.ceoName}" />
						</td>
						<td>
							<c:out value="${item.coName}" escapeXml="false" />
						</td>
						<td>
							<c:out value="${item.action}" />
						</td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(list) eq '0'}">
					<tr>
						<td colspan="15">조회된 데이터가 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
</div>
<!-- //table_area -->
<div id="pagination" class="pagination">
	<ul>
		<ui:pagination paginationInfo="${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
	</ul>
</div>


<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_log" />
</jsp:include>
