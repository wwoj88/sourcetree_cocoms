<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="업체현황" />
	<jsp:param name="nav2" value="통계조회" />
</jsp:include>
	
		<div class="table_area list_top list_type3">
			<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/stats_search.jsp" flush="true" />
		</div>
		<!-- //table_area -->
		<h2 class="tit_c_s"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 
		<strong class="txt_blue"> 업체별 매출액 </strong></h2>
		<div class="table_area list list_type2 ">
			<div class="inner">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:80%" />
						<col style="width:20%" />
			
					</colgroup>
					<thead>
						<tr> 	 
						
							<th>업체명</th>
							<th>매출액</th>
	
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr>
								<td>${item.name}</td>
								<td><fmt:formatNumber value="${item.sum}" type="number" /></td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(list) eq '0'}">
							<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //table_area -->
		<div class="button_area mt25">
			<div class="right">
				<a href="<c:url value="/admin/status/statsdetail/save?conditionCd=${conditionCd}&fromYear=${fromYear}&toYear=${toYear}&type=${type}" />" id="excel" class="btn btn_style2">엑셀출력</a>
				<%-- 
				<a href="" id="list" class="btn btn_style1">목록</a>
				--%>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_stat" />
	<jsp:param name="includes" value="stats_search" />
</jsp:include>
