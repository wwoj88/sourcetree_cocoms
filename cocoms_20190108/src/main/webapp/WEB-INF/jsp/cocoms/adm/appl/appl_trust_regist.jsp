<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="저작권신탁관리업" />
	<jsp:param name="nav2" value="신고업무" />
</jsp:include>
	
		<form id="form" method="post" action="<c:url value="/admin/sub/regist" />">
		<input type="hidden" id="conDetailCd" name="conDetailCd" value="${member.conDetailCd}" />
		
		<input type="hidden" id="trustSido" name="trustSido" value="${member.trustSido}" />
		<input type="hidden" id="trustGugun" name="trustGugun" value="${member.trustGugun}" />
		<input type="hidden" id="trustDong" name="trustDong" value="${member.trustDong}" />
		<input type="hidden" id="trustBunji" name="trustBunji" value="${member.trustBunji}" />
		<input type="hidden" id="ceoSido" name="ceoSido" value="${member.ceoSido}" />
		<input type="hidden" id="ceoGugun" name="ceoGugun" value="${member.ceoGugun}" />
		<input type="hidden" id="ceoDong" name="ceoDong" value="${member.ceoDong}" />
		<input type="hidden" id="ceoBunji" name="ceoBunji" value="${member.ceoBunji}" />
		
		<input type="hidden" name="file1Path" />
		<input type="hidden" name="file2Path" />
		<input type="hidden" name="file3Path" />
		<input type="hidden" name="file4Path" />
	
		<div class="form_apply">
			<div class="aside">
				<div class="table_area">
					<table>
						<caption> 회원정보</caption>
						<thead>
							<tr> 	 	 	
								<th> 회원정보</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>
									 회원구분 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${member.coGubunCd eq '1'}"><c:set var="coGubunCd1Checked" value="checked" /></c:if>
									<c:if test="${member.coGubunCd eq '2'}"><c:set var="coGubunCd2Checked" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd1" name="coGubunCd" value="1" ${coGubunCd1Checked} />
										<label for="coGubunCd1">개인사업자</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd2" name="coGubunCd" value="2" ${coGubunCd2Checked} />
										<label for="coGubunCd2">법인사업자</label> 
									</div>
								</td>
							</tr>
							<tr>
								<th>
									 사업자 등록번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="coNo1" class="input_style1 num1" value="${member.coNo1}" maxlength="3" title="사업자번호 첫번째자리" />
									<span>-</span>
									<input type="text" name="coNo2" class="input_style1 num1" value="${member.coNo2}" maxlength="2" title="사업자번호 두째자리" />
									<span>-</span>
									<input type="text" name="coNo3" class="input_style1 num2" value="${member.coNo3}" maxlength="5" title="사업자번호 세째자리" />
								</td>
							</tr>
							<c:if test="${member.coGubunCd eq '2'}">
								<tr id="coGubunCd2_layer1">
									<th>
										 법인 등록번호 <strong class="txt_point">*</strong>
									</th>
								</tr>
								<tr id="coGubunCd2_layer2">
									<td>
										<input type="text" name="bubNo" class="input_style1" value="${member.bubNo}" maxlength="13" />
									</td>
								</tr>
							</c:if>
							<tr>
								<th>
									주소 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" id="trustZipcode" name="trustZipcode" class="input_style1 address1 disabled" 
											value="${member.trustZipcode}" readonly placeholder="" /><button type="button" id="findTrustAddr" class="btn_style1">주소찾기</button>
									<input type="text" id="trustAddr" class="input_style1 margin disabled" 
											value="${member.trustSido} ${member.trustGugun} ${member.trustDong} ${member.trustBunji}" readonly placeholder="" />
									<input type="text" id="trustDetailAddr" name="trustDetailAddr" class="input_style1" 
											value="${member.trustDetailAddr}" maxlength="100" placeholder="" />
								</td>
							</tr>
							<tr>
								<th>
									전화번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<select name="trustTel1" class="select_style">
										<option value="02"   <c:if test="${member.trustTel1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.trustTel1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.trustTel1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.trustTel1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.trustTel1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.trustTel1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.trustTel1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.trustTel1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.trustTel1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.trustTel1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.trustTel1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.trustTel1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.trustTel1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.trustTel1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.trustTel1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.trustTel1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.trustTel1 eq '0502'}">selected</c:if>>0502</option> 
									</select>
									<span>-</span>
									<input type="text" name="trustTel2" class="input_style1 phone" value="${member.trustTel2}" maxlength="4" placeholder="" />
									<span>-</span>
									<input type="text" name="trustTel3" class="input_style1 phone" value="${member.trustTel3}" maxlength="4" placeholder="" />
								</td>
							</tr>
							<tr>
								<th>
									팩스번호 
								</th>
							</tr>
							<tr>
								<td>
									<select name="trustFax1" class="select_style">
										<option value="02"   <c:if test="${member.trustFax1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.trustFax1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.trustFax1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.trustFax1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.trustFax1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.trustFax1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.trustFax1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.trustFax1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.trustFax1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.trustFax1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.trustFax1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.trustFax1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.trustFax1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.trustFax1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.trustFax1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.trustFax1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.trustFax1 eq '0502'}">selected</c:if>>0502</option> 
									</select>
									<span>-</span>
									<input type="text" name="trustFax2" class="input_style1 phone" value="${member.trustFax2}" maxlength="4" placeholder="" />
									<span>-</span>
									<input type="text" name="trustFax3" class="input_style1 phone" value="${member.trustFax3}" maxlength="4" placeholder="" />
								</td>
							</tr>
							<tr>
								<th>
									홈페이지 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="trustUrl" class="input_style1" value="${member.trustUrl}" maxlength="100" placeholder="" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="table_area">
					<table>
						<caption> 대표자정보</caption>
						<thead>
							<tr> 	 	 	
								<th>대표자정보</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>
									 대표자명 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="ceoName" class="input_style1" value="${member.ceoName}" maxlength="30" placeholder="" />
								</td>
							</tr>
							<tr>
								<th>
									 주민등록번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="ceoRegNo1" class="input_style1 resident" value="${member.ceoRegNo1}" maxlength="6" placeholder="" /><span>-</span><input 
											type="password" name="ceoRegNo2" class="input_style1 resident" maxlength="7" placeholder="" />
								</td>
							</tr>
							<tr>
								<th>
									주소 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" id="ceoZipcode" name="ceoZipcode" class="input_style1 address1 disabled" 
											value="${member.ceoZipcode}" readonly placeholder="" /><button type="button" id="findCeoAddr" class="btn_style1">주소찾기</button>
									<input type="text" id="ceoAddr" class="input_style1 margin disabled" 
											value="${member.ceoSido} ${member.ceoGugun} ${member.ceoDong} ${member.ceoBunji}" readonly placeholder="" />
									<input type="text" id="ceoDetailAddr" name="ceoDetailAddr" class="input_style1" 
											value="${member.ceoDetailAddr}" maxlength="100" placeholder="" />
								</td>
							</tr>
							<tr>
								<th>
									휴대전화번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<select name="ceoMobile1" class="select_style">
										<option value="010" <c:if test="${member.ceoMobile1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${member.ceoMobile1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${member.ceoMobile1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${member.ceoMobile1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${member.ceoMobile1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${member.ceoMobile1 eq '019'}">selected</c:if>>019</option> 
									</select>
									<span>-</span>
									<input type="text" name="ceoMobile2" class="input_style1 phone" value="${member.ceoMobile2}" maxlength="4" placeholder="" />
									<span>-</span>
									<input type="text" name="ceoMobile3" class="input_style1 phone" value="${member.ceoMobile3}" maxlength="4" placeholder="" />
								</td>
							</tr>
							<tr>
								<th>
									이메일

								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="ceoEmail1" class="input_style1 resident" 
											value="${member.ceoEmail1}" maxlength="70" placeholder="" /><span>@</span><input type="text" name="ceoEmail2" class="input_style1 resident" 
											value="${member.ceoEmail2}" maxlength="29" placeholder="" />
								</td>
							</tr>
							<tr>
								<th>
									 SMS수신동의 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${member.smsAgree eq 'Y'}"><c:set var="smsAgreeYChecked" value="checked" /></c:if>
									<c:if test="${member.smsAgree ne 'Y'}"><c:set var="smsAgreeNChecked" value="checked" /></c:if>
									<div class="btm_form_box">
									
										<input type="radio" id="smsAgreeY" name="smsAgree" value="Y" ${smsAgreeYChecked} />
										<label for="smsAgreeY">동의</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="smsAgreeN" name="smsAgree" value="N" ${smsAgreeNChecked} />
										<label for="smsAgreeN">동의안함</label> 
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
			</div>
			<!-- //aside -->
			<div class="cont">
				<div class="table_area">
					<table>
						<caption>업무선택</caption>
						<thead>
							<tr> 	 	 	
								<th>업무선택</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>
									 취급 저작물의 종류 및 취급 권리의 종류 (저작권)  <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind1" name="writingKind" value="1" ${writingKind1} />
										<label for="writingKind1">어문 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_1" name="permkind1" value="1" ${permkind1_1} />
										<label for="permkind1_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_2" name="permkind1" value="2" ${permkind1_2} />
										<label for="permkind1_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_3" name="permkind1" value="3" ${permkind1_3} />
										<label for="permkind1_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_4" name="permkind1" value="4" ${permkind1_4} />
										<label for="permkind1_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_8" name="permkind1" value="8" ${permkind1_8} />
										<label for="permkind1_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind1_0" name="permkind1" value="0" ${permkind1_0} />
										<label for="permkind1_0">기타</label> 
									</div>
									<input type="text" name="permEtc1" class="input_style1 others" value="${permEtc1}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind2" name="writingKind" value="2" ${writingKind2} />
										<label for="writingKind2">음악 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_1" name="permkind2" value="1" ${permkind2_1} />
										<label for="permkind2_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_2" name="permkind2" value="2" ${permkind2_2} />
										<label for="permkind2_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_3" name="permkind2" value="3" ${permkind2_3} />
										<label for="permkind2_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_4" name="permkind2" value="4" ${permkind2_4} />
										<label for="permkind2_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_8" name="permkind2" value="8" ${permkind2_8} />
										<label for="permkind2_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind2_0" name="permkind2" value="0" ${permkind2_0} />
										<label for="permkind2_0">기타</label> 
									</div>
									<input type="text" name="permEtc2" class="input_style1 others" value="${permEtc2}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind3" name="writingKind" value="3" ${writingKind3} />
										<label for="writingKind3">영상 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_1" name="permkind3" value="1" ${permkind3_1} />
										<label for="permkind3_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_2" name="permkind3" value="2" ${permkind3_2} />
										<label for="permkind3_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_3" name="permkind3" value="3" ${permkind3_3} />
										<label for="permkind3_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_4" name="permkind3" value="4" ${permkind3_4} />
										<label for="permkind3_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_8" name="permkind3" value="8" ${permkind3_8} />
										<label for="permkind3_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind3_0" name="permkind3" value="0" ${permkind3_0} />
										<label for="permkind3_0">기타</label> 
									</div>
									<input type="text" name="permEtc3" class="input_style1 others" value="${permEtc3}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind21" name="writingKind" value="21" ${writingKind21} />
										<label for="writingKind21">연극 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_1" name="permkind21" value="1" ${permkind21_1} />
										<label for="permkind21_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_2" name="permkind21" value="2" ${permkind21_2} />
										<label for="permkind21_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_3" name="permkind21" value="3" ${permkind21_3} />
										<label for="permkind21_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_4" name="permkind21" value="4" ${permkind21_4} />
										<label for="permkind21_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_8" name="permkind21" value="8" ${permkind21_8} />
										<label for="permkind21_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind21_0" name="permkind21" value="0" ${permkind21_0} />
										<label for="permkind21_0">기타</label> 
									</div>
									<input type="text" name="permEtc21" class="input_style1 others" value="${permEtc21}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind4" name="writingKind" value="4" ${writingKind4} />
										<label for="writingKind4">미술 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_1" name="permkind4" value="1" ${permkind4_1} />
										<label for="permkind4_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_2" name="permkind4" value="2" ${permkind4_2} />
										<label for="permkind4_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_3" name="permkind4" value="3" ${permkind4_3} />
										<label for="permkind4_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_4" name="permkind4" value="4" ${permkind4_4} />
										<label for="permkind4_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_8" name="permkind4" value="8" ${permkind4_8} />
										<label for="permkind4_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind4_0" name="permkind4" value="0" ${permkind4_0} />
										<label for="permkind4_0">기타</label> 
									</div>
									<input type="text" name="permEtc4" class="input_style1 others" value="${permEtc4}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind5" name="writingKind" value="5" ${writingKind5} />
										<label for="writingKind5">사진 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_1" name="permkind5" value="1" ${permkind5_1} />
										<label for="permkind5_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_2" name="permkind5" value="2" ${permkind5_2} />
										<label for="permkind5_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_3" name="permkind5" value="3" ${permkind5_3} />
										<label for="permkind5_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_4" name="permkind5" value="4" ${permkind5_4} />
										<label for="permkind5_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_8" name="permkind5" value="8" ${permkind5_8} />
										<label for="permkind5_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind5_0" name="permkind5" value="0" ${permkind5_0} />
										<label for="permkind5_0">기타</label> 
									</div>
									<input type="text" name="permEtc5" class="input_style1 others" value="${permEtc5}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind22" name="writingKind" value="22" ${writingKind22} />
										<label for="writingKind">건축 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_1" name="permkind22" value="1" ${permkind22_1} />
										<label for="permkind22_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_2" name="permkind22" value="2" ${permkind22_2} />
										<label for="permkind22_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_3" name="permkind22" value="3" ${permkind22_3} />
										<label for="permkind22_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_4" name="permkind22" value="4" ${permkind22_4} />
										<label for="permkind22_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_8" name="permkind22" value="8" ${permkind22_8} />
										<label for="permkind22_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind22_0" name="permkind22" value="0" ${permkind22_0} />
										<label for="permkind22_0">기타</label> 
									</div>
									<input type="text" name="permEtc22" class="input_style1 others" value="${permEtc22}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind23" name="writingKind" value="23" ${writingKind23} />
										<label for="writingKind23">도형 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind23_1" name="permkind23" value="1" ${permkind23_1} />
										<label for="permkind23_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind23_2" name="permkind23" value="2" ${permkind23_2} />
										<label for="permkind23_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind23_3" name="permkind23" value="3" ${permkind23_3} />
										<label for="permkind23_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind23_8" name="permkind23" value="8" ${permkind23_8} />
										<label for="permkind23_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind23_0" name="permkind23" value="0" ${permkind23_0} />
										<label for="permkind23_0">기타</label> 
									</div>
									<input type="text" name="permEtc23" class="input_style1 others" value="${permEtc23}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind24" name="writingKind" value="24" ${writingKind24} />
										<label for="writingKind24">컴퓨터프로그램 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_1" name="permkind24" value="1" ${permkind24_1} />
										<label for="permkind24_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_2" name="permkind24" value="2" ${permkind24_2} />
										<label for="permkind24_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_3" name="permkind24" value="3" ${permkind24_3} />
										<label for="permkind24_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_6" name="permkind24" value="6" ${permkind24_6} />
										<label for="permkind24_6">대여권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_8" name="permkind24" value="8" ${permkind24_8} />
										<label for="permkind24_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind24_0" name="permkind24" value="0" ${permkind24_0} />
										<label for="permkind24_0">기타</label> 
									</div>
									<input type="text" name="permEtc24" class="input_style1 others" value="${permEtc24}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind0" name="writingKind" value="0" ${writingKind0} />
										<label for="writingKind0">기타 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_1" name="permkind0" value="1" ${permkind0_1} />
										<label for="permkind0_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_2" name="permkind0" value="2" ${permkind0_2} />
										<label for="permkind0_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_3" name="permkind0" value="3" ${permkind0_3} />
										<label for="permkind0_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_4" name="permkind0" value="4" ${permkind0_4} />
										<label for="permkind0_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_5" name="permkind0" value="5" ${permkind0_5} />
										<label for="permkind0_5">전시권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_6" name="permkind0" value="6" ${permkind0_6} />
										<label for="permkind0_6">대여권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind0_7" name="permkind0" value="7" ${permkind0_7} />
										<label for="permkind0_7">방송권</label> 
									</div>
									<div class="btm_form_box" style="margin-left: 180px;">
										<input type="checkbox" id="permkind0_8" name="permkind0" value="8" ${permkind0_8} />
										<label for="permkind0_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind0_0" name="permkind0" value="0" ${permkind0_0} />
										<label for="permkind0_0">기타</label> 
									</div>
									<input type="text" name="permEtc0" class="input_style1 others" value="${permEtc0}" placeholder="" /> 
								</td>
							</tr>
							<tr>
								<th class="pt">
									 취급 저작물의 종류 및 취급 권리의 종류 (저작인접권) <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind10" name="writingKind" value="10" ${writingKind10} />
										<label for="writingKind10">실연자의 권리</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind10_1" name="permkind10" value="1" ${permkind10_1} />
										<label for="permkind10_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind10_2" name="permkind10" value="2" ${permkind10_2} />
										<label for="permkind10_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind10_3" name="permkind10" value="3" ${permkind10_3} />
										<label for="permkind10_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind10_4" name="permkind10" value="4" ${permkind10_4} />
										<label for="permkind10_4">공연권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind10_0" name="permkind10" value="0" ${permkind10_0} />
										<label for="permkind10_0">기타</label> 
									</div>
									<input type="text" name="permEtc10" class="input_style1 others" value="${permEtc10}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind11" name="writingKind" value="11" ${writingKind11} />
										<label for="writingKind11">음반제작자의 권리</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind11_1" name="permkind11" value="1" ${permkind11_1} />
										<label for="permkind11_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind11_2" name="permkind11" value="2" ${permkind11_2} />
										<label for="permkind11_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind11_3" name="permkind11" value="3" ${permkind11_3} />
										<label for="permkind11_3">전송권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind11_0" name="permkind11" value="0" ${permkind11_0} />
										<label for="permkind11_0">기타</label> 
									</div>
									<input type="text" name="permEtc11" class="input_style1 others" value="${permEtc11}" placeholder="" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind12" name="writingKind" value="12" ${writingKind12} />
										<label for="writingKind12">출판권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind12_1" name="permkind12" value="9" ${permkind12_1} />
										<label for="permkind12_1">출판권</label> 
									</div>
								</td>
							</tr>
							<!-- 
							<tr>
								<th class="pt">
									변경 신청사유 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<textarea class="input_style1 height"></textarea>
								</td>
							</tr>
							 -->
						</tbody>
					</table>
				</div>
				<!-- //table_area -->

				<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
					<div class="cell">
						<p class="tit">구비서류 안내 </p>
						<p class="txt">저작권대리중개업신고서 1부 <span>[현재 작성한 온라인 신청서]</span></p>
						<p class="txt">저작권대리중개업 업무규정 1부 <span>(저작권대리중개 계약약관, 저작물 이용계약 약관 각1부 포함)</span></p>
						<p class="txt">신고인<span>(단체 또는 법인인 경우에는 그 대표자 및 임원)</span>의 이력서 1부 <a href="<c:url value='/download2?filename=6.zip' />" class="btn_style1 margin" target="_blank">양식 다운로드</a></p>
					</div>
					<!-- //cell -->
					<div class="cell">
						<p class="tit">첨부파일</p>
						<div class="list_file">
							<p class="txt">저작권 대리중개업 업무에 관한 규정 <strong class="txt_point">*</strong></p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" readonly />
							<input type="file" name="file1" class="insert_file_target" data-ref-name="file1Path" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						</div>
						<div class="list_file">
							<p class="txt">신고인의 이력서 <strong class="txt_point">*</strong></p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" readonly />
							<input type="file" name="file2" class="insert_file_target" data-ref-name="file2Path" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						</div>
						<div class="list_file">
							<p class="txt">정관 또는 규약 <strong class="txt_point">*</strong></p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" readonly />
							<input type="file" name="file3" class="insert_file_target" data-ref-name="file3Path" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						</div>
						<div class="list_file">
							<p class="txt">재무제표 <strong class="txt_point">*</strong></p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" readonly />
							<input type="file" name="file4" class="insert_file_target" data-ref-name="file4Path" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						</div>
					</div>
					<!-- //cell -->
					<div class="txt_commission">
						<c:if test="${type eq 'sub_regist'}">
							<div class="num">수수료  <strong>5,000 원</strong></div>
						</c:if>
						<c:if test="${type eq 'sub_modify'}">
							<div class="num">수수료  <strong>3,000 원</strong></div>
						</c:if>
						<div class="txt"><strong class="txt_point">*</strong> 저작권법 제105조제1항에 의하여 해당 시스템에서 신고시 1,000원을 감액합니다.</div>
					</div>
					<div class="txt_b">105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</div>
				</c:if>
				<c:if test="${type eq 'trust_regist' or type eq 'trust_modify'}">
					<div class="cell">
						<p class="tit">구비서류 안내 </p>
						<p class="txt">저작권신탁관리업 허가신청서 <span>[현재 작성한 온라인 신청서]</span></p>
						<p class="txt">저작권신탁관리 업무규정 1부</p>
						<p class="txt_inner">
							- 저작권 신탁계약 약관<br>- 저작권 이용계약 약관
						</p>
						<p class="txt">신고인 <span>(단체 또는 법인인 경우에는 그 대표자 및 임원)</span>의 이력서 1부 <span class="txt_blue">[온라인 첨부가능]</span> <a href="" class="btn_style1 margin">양식 다운로드</a></p>
					</div>
					<!-- //cell -->
					<div class="cell">
						<p class="tit">첨부파일</p>
						<div class="list_file">
							<p class="txt">저작권 대리중개업 업무에 관한 규정 <strong class="txt_point">*</strong></p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" readonly />
							<input type="file" name="file1" class="insert_file_target" data-ref-name="file1Path" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						</div>
						<div class="list_file">
							<p class="txt">신고인의 이력서 <strong class="txt_point">*</strong></p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" readonly />
							<input type="file" name="file2" class="insert_file_target" data-ref-name="file2Path" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						</div>
						<div class="list_file">
							<p class="txt">정관 또는 규약 <strong class="txt_point">*</strong></p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" readonly />
							<input type="file" name="file3" class="insert_file_target" data-ref-name="file3Path" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						</div>
						<div class="list_file">
							<p class="txt">재무제표 <strong class="txt_point">*</strong></p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" readonly />
							<input type="file" name="file4" class="insert_file_target" data-ref-name="file4Path" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						</div>
					</div>
					<!-- //cell -->
					<div class="txt_commission">
						<c:if test="${type eq 'trust_regist'}">
							<div class="num">수수료  <strong>10,000 원</strong></div>
						</c:if>
						<c:if test="${type eq 'trust_modify'}">
							<div class="num">수수료  <strong>3,000 원</strong></div>
						</c:if>
						<div class="txt"><strong class="txt_point">*</strong> 저작권법 제105조제1항에 의하여 해당 시스템에서 신고시 1,000원을 감액합니다.</div>
					</div>
					<div class="txt_b">105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다. </div>
				</c:if>
				
				<div class="btn_area">
					<a href="" id="save" class="btn btn_style2">저장</a>
					<!--  
					<a href="" class="btn btn_style1">취소</a>
					-->
				</div>
			</div>
			<!-- //cont -->
		</div>
		<!-- //form_apply -->

		</form>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="comm_js" value="appl" />
	<jsp:param name="js" value="adm_appl" />
</jsp:include>
