<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="title" value="HOME" />
</jsp:include>
	
main

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="main" />
</jsp:include>
