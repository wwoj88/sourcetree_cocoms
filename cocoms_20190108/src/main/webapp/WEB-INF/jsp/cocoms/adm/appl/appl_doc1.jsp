<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="${title1}" />
	<jsp:param name="nav2" value="${title2}" />
	<jsp:param name="title" value="NONE" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />
<input type="hidden" name="conditionCd" value="${data.conditionCd}" />
<input type="hidden" name="conDetailCd" value="${data.conDetailCd}" />
<input type="hidden" name="preStatCd" value="${data.preStatCd}" />
<input type="hidden" name="statCd" value="${statCd}" />

	
		<h2 class="tit_c" >${data.coName}</h2>
		<div class="form_apply form_apply_style1">
			<div class="aside">
				<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/company_tab.jsp" flush="true" />
			</div>
			<!-- //aside -->
			<div class="cont">
				<div class="table_area">
					<table>
						<caption>저작권대리중개업</caption>
						<thead>
							<tr> 	 	 	
								<th>저작권대리중개업 신고서</th>
							</tr>
						</thead>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="declaration_preview">
					<div class="inner">
						<div class="preview">
							<p class="num_top txt_r">처리기간 : 5일</p>
							<h1 class="tit">저작권대리중개업 신고서</h1>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:129px" />
										<col style="width:222px" />
										<col style="width:158px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="3">신고인</th>
											<th>성명<p>(단체 또는 법인명)</p></th>
											<td><c:out value="${chgHistory.postCoName}" /></td>
											<th>생년월일<p>(법인/사업자등록번호)</p></th>
											<td>
												<c:if test="${!empty chgHistory.postBubNo}"><c:out value="${chgHistory.postBubNoStr}" /></c:if>
												<c:if test="${empty chgHistory.postBubNo}"><c:out value="${chgHistory.postCoNoStr}" /></c:if>
											</td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td><c:out value="${chgHistory.postTrustTel}" /></td>
											<th>팩스번호</th>
											<td><c:out value="${chgHistory.postTrustFax}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">
												<c:if test="${!empty chgHistory.postTrustZipcode}">(<c:out value="${chgHistory.postTrustZipcode}" />)</c:if>
												<c:out value="${chgHistory.postTrustSido}" />
												<c:out value="${chgHistory.postTrustGugun}"/>
												<c:out value="${chgHistory.postTrustDong}"  />
												<c:out value="${chgHistory.postTrustBunji}"  />
												<c:out value="${chgHistory.postTrustDetailAddr}"  />
											</td>
										</tr>
										<tr>
											<th class="bg" rowspan="3">대표자<p>(법인 또는<br>단체에 한함)</p></th>
											<th>성명</th>
											<td><c:out value="${chgHistory.postCeoName}" /></td>
											<th>생년월일</th>
											<td><c:out value="${birthday}" /></td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td><c:out value="${chgHistory.postCeoTel}" /></td>
											<th>이메일 주소</th>
											<td><c:out value="${chgHistory.postCeoEmail}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">
												<c:if test="${!empty chgHistory.postCeoZipcode}">(<c:out value="${chgHistory.postCeoZipcode}" />)</c:if>
												<c:out value="${chgHistory.postCeoSido}" />
												<c:out value="${chgHistory.postCeoGugun}"  />
												<c:out value="${chgHistory.postCeoDong}"  />
												<c:out value="${chgHistory.postCeoBunji}"  />
												<c:out value="${chgHistory.postCeoDetailAddr}"  />
											</td>
										</tr>
											<tr>
											<th height="50" colspan="2">취급 하고자 하는 업무의 내용</th>
											<td colspan="3" class="txt_l">
												<c:if test="${data.conDetailCd eq '1'}">
												   대리
							        		    </c:if>
												<c:if test="${data.conDetailCd eq '2'}">
												   중개
												</c:if>
												<c:if test="${data.conDetailCd eq '3'}">
												   대리 , 중개
												</c:if>
											  </td>
								 			 </tr>
										<tr>
											<th height="50" colspan="2">취급하고자 하는 저작물등의 종류</th>
											<td colspan="3" class="txt_l"><c:out value="${writingKind}" /></td>
										</tr>
										<tr>
											<th height="50" colspan="2">취급하고자 하는 권리<p>(주요 저작물 중심으로 중복선택 가능)</p></th>
											<td colspan="3" class="txt_l"><c:out value="${permKind}" /></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<p class="txt1"><strong>[저작권법]</strong> 제 105조 제 1항 및 같은 법 시행령 제 48조에 따라 위와 같이 신고합니다.</p>
							<div class="sign">
								<p>
									<c:out value="${fn:substring(rpMgm.regDate, 0, 4)}" />년 
									<c:out value="${fn:substring(rpMgm.regDate, 4, 6)}" />월 
									<c:out value="${fn:substring(rpMgm.regDate, 6, 8)}" />일
								</p>
								<p>신청인   <c:out value="${chgHistory.postCeoName}" />   (서명 또는 인)</p>
							</div>
							<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:auto" />
										<col style="width:266px" />
										<col style="width:134px" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="2">구비서류</th>
											<th>민원인 제출서류</th>
											<th>담당공무원 확인사항</th>
											<th>수수료</th>
										</tr>
										<tr>
											<td class="txt_l">
												1. 대리중개업 업무규정<br>
												<p>가. 저작권대리중개 계약약관</p><br>
												<p>나. 저작물 이용계약약관</p><br>

												2. 신고인<p>(단체 또는 법인의 대표자 및 임원)</p>의 이력서<br>

												3. 정관 또는 규약 1부<br>

												4. 재무제표<p>(법인인 경우에 한정)</p><br>

											</td>
											<td>법인등기부등본 1부</td>
											<td>5,000원</td>
										</tr>
										<tr>
										
											<th class="bg">첨부파일</th>
												<td colspan="3" class="txt_l">
												<div class="file_change">
												
													<c:if test="${!empty rpMgm.file1Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file1Path}" />"><p class="txt1">1. 저작권대리중개업 업무규정</p></a>
													</c:if>												
													<c:if test="${!empty rpMgm.file2Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file2Path}" />"><p class="txt1">2. 신고인(단체 또는 법인인 경우에는 그 대표자및 임원)의 이력서</p></a>
													</c:if>		
													<c:if test="${!empty rpMgm.interFilepath}">
														<a href="<c:url value="/download?filename=${rpMgm.interFilepath}" />"><p class="txt1">3. 대리중개계약서 및 이용허락계약서</p></a>
													</c:if>													
													<c:if test="${!empty rpMgm.file3Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file3Path}" />"><p class="txt1">4. 정관 또는 규약 1부</p></a>
													</c:if>												
													<c:if test="${!empty rpMgm.file4Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file4Path}" />"><p class="txt1">5. 재무제표(법인단체)</p></a>
													</c:if>												
													
<%-- 													<c:forEach var="item" items="${chgHistoryFileList}" varStatus="status">
														<a href="<c:url value="/download?filename=${item.filepath}" />">
															<p class="txt1">변경신고신청 첨부<c:out value="${status.count}" /></p>
															<p class="txt2"><c:out value="${item.filename}" /></p>
															<p class="txt3">(<c:out value="${item.regDateStr}" />)</p>
														</a>
													</c:forEach> --%>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
						</div>
						<!-- //preview -->
					</div>
				</div>
				<!-- //declaration_preview -->
			</div>
			<!-- //cont -->
		</div>
		<!-- //form_apply -->
		<div class="btn_area">
			<div class="right">
				<!-- 
				<a href="" class="btn_style2">PDF저장</a>
				 -->
				<a href="" id="changeStatCd_" class="btn_style3">접수</a>
				<a href="" id="changeStatCd_2" class="btn_style3">신고신청처리</a>
				<a href="" id="changeStatCd_7" class="btn_style3">보완요청</a>
				<a href="" id="changeStatCd_9" class="btn_style3">반려</a>
			    <a href="" id="print" class="btn_style2">인쇄</a>
				<a href="javascript:history.go(-1);" id="" class="btn_style1">이전</a>
			</div>
		</div>
		<!-- //btn_area -->
		
<!-- <input type="hidden" id="rpx" value=app_declaration_union_m_new" /> -->
<input type="hidden" id="rpx" value="app_declaration_c_new" />
<textarea id="xmldata" style="width: 100%; height: 500px; display: none;">
<!-- <textarea id="xmldata" style="width: 100%; height: 100%; display: none;"> -->
<!-- <textarea id="xmldata" style="visibility:hidden;height:0;width:0;"> -->
	<root>
		<day><c:out value="${fn:substring(rpMgm.regDate, 0, 4)}" />년 <c:out value="${fn:substring(rpMgm.regDate, 4, 6)}" />월 <c:out value="${fn:substring(rpMgm.regDate, 6, 8)}" />일</day><!-- 신고일자 -->
		<title>저작권대리중개업 신고서</title>
		<process_term>5일</process_term><!-- 처리test기간 -->

		<company_info><!-- 업체기본정보 -->
			<company><![CDATA[${chgHistory.postCoName}]]></company>		
			<regno><c:if test="${!empty chgHistory.postBubNo}"><c:out value="${chgHistory.postBubNoStr}" /></c:if><c:if test="${empty chgHistory.postBubNo}"><c:out value="${chgHistory.postCoNoStr}" /></c:if></regno><!-- 사업자등록번호 -->
			<tel><c:out value="${chgHistory.postTrustTel}" /></tel><!-- 전화번호 -->
			<fax><c:out value="${chgHistory.postTrustFax}" /></fax><!-- 팩스번호 -->	
			<address><c:if test="${!empty chgHistory.postTrustZipcode}">(<c:out value="${chgHistory.postTrustZipcode}" />)</c:if><![CDATA[${chgHistory.postTrustSido} ${chgHistory.postTrustGugun} ${chgHistory.postTrustDong} ${chgHistory.postTrustBunji} ${chgHistory.postTrustDetailAddr}]]></address><!-- 주소 -->
			<boss><!-- 대표자 -->
				<name><c:out value="${chgHistory.postCeoName}" /></name><!-- 성명 -->
				<birthday><c:out value="${birthday}" /></birthday><!-- 생년월일 -->
				<tel><c:out value="${chgHistory.postCeoTel}" /></tel><!-- 전화번호 -->
				<email><c:out value="${chgHistory.postCeoEmail}" /></email><!-- email -->
				<address><c:if test="${!empty chgHistory.postCeoZipcode}"><c:out value="${chgHistory.postCeoZipcode}" /></c:if>${chgHistory.postCeoSido} ${chgHistory.postCeoGugun} ${chgHistory.postCeoDong} ${chgHistory.postCeoBunji} ${chgHistory.postCeoDetailAddr}</address><!-- 주소 -->
			</boss>
		</company_info>
		<content>
			<content1><c:choose><c:when test="${member.conDetailCd eq '1' or member.conDetailCd eq '3'}">1</c:when><c:otherwise>0</c:otherwise></c:choose></content1><!-- 대리 -->
			<content2><c:choose><c:when test="${member.conDetailCd eq '2' or member.conDetailCd eq '3'}">1</c:when><c:otherwise>0</c:otherwise></c:choose></content2><!-- 중개 -->
		</content>
	<c:set var="works1" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '1'}"><c:set var="works1" value="1" /></c:if>
		</c:forEach>
		<c:set var="works2" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '2'}"><c:set var="works2" value="1" /></c:if>
		</c:forEach>
		<c:set var="works3" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '3'}"><c:set var="works3" value="1" /></c:if>
		</c:forEach>
		<c:set var="works4" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '21'}"><c:set var="works4" value="1" /></c:if>
		</c:forEach>
		<c:set var="works5" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '4'}"><c:set var="works5" value="1" /></c:if>
		</c:forEach>

		<c:set var="works6" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '5'}"><c:set var="works6" value="1" /></c:if>
		</c:forEach>
		<c:set var="works7" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '22'}"><c:set var="works7" value="1" /></c:if>
		</c:forEach>
		<c:set var="works8" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '23'}"><c:set var="works8" value="1" /></c:if>
		</c:forEach>
		<c:set var="works9" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '24'}"><c:set var="works9" value="1" /></c:if>
		</c:forEach>
		<c:set var="works10" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '0'}"><c:set var="works10" value="1" /></c:if>
		</c:forEach>

		<c:set var="works11" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '10'}"><c:set var="works11" value="1" /></c:if>
		</c:forEach>
		<c:set var="works12" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '11'}"><c:set var="works12" value="1" /></c:if>
		</c:forEach>
		<c:set var="works13" value="0" />
		<c:forEach var="item" items="${writingKindList3}" varStatus="status">
			<c:if test="${item.writingKind eq '12'}"><c:set var="works13" value="1" /></c:if>
		</c:forEach>
		<works><!-- 저작물의 종류 -->
			<works1><c:out value="${works1}" /></works1><!-- 어문 -->
			<works2><c:out value="${works2}" /></works2><!-- 음악 -->
			<works3><c:out value="${works3}" /></works3><!-- 영상 -->
			<works4><c:out value="${works4}" /></works4><!-- 연극 -->
			<works5><c:out value="${works5}" /></works5><!-- 미술 -->
			
			<works6><c:out value="${works6}" /></works6><!-- 사진 -->
			<works7><c:out value="${works7}" /></works7><!-- 건축 -->
			<works8><c:out value="${works8}" /></works8><!-- 도형 -->
			<works9><c:out value="${works9}" /></works9><!-- 컴퓨터프로그램 -->
			<works10><c:out value="${works10}" /></works10><!-- 기타 -->
			
			<works11><c:out value="${works11}" /></works11><!-- 실연자의 권리 -->
			<works12><c:out value="${works12}" /></works12><!-- 음반제작자의 권리 -->
			<works13><c:out value="${works13}" /></works13><!-- 출판권 -->
		</works>
		
		<c:set var="rights1" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '1'}"><c:set var="rights1" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights2" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '2'}"><c:set var="rights2" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights3" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '3'}"><c:set var="rights3" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights4" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '4'}"><c:set var="rights4" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights5" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '5'}"><c:set var="rights5" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights6" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '6'}"><c:set var="rights6" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights7" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '7'}"><c:set var="rights7" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights8" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '8'}"><c:set var="rights8" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights9" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '9'}"><c:set var="rights9" value="1" /></c:if>
		</c:forEach>
		<c:set var="rights0" value="0" />
		<c:forEach var="item" items="${permKindList3}" varStatus="status">
			<c:if test="${item.permKind eq '0'}"><c:set var="rights0" value="1" /></c:if>
		</c:forEach>		
			
		<rights><!-- 취급하고자 하는 권리 -->
			<rights1><c:out value="${rights1}" /></rights1><!-- 복제권 -->
			<rights2><c:out value="${rights2}" /></rights2><!-- 배포권 -->
			<rights3><c:out value="${rights3}" /></rights3><!-- 전송권 -->
			<rights4><c:out value="${rights4}" /></rights4><!-- 공연권 -->
			<rights5><c:out value="${rights5}" /></rights5><!-- 전시권 -->
			<rights6><c:out value="${rights6}" /></rights6><!-- 대여권 -->
			<rights7><c:out value="${rights7}" /></rights7><!-- 방송권 -->
			<rights8><c:out value="${rights8}" /></rights8><!-- 2차적 저작물 작성권 -->
			<rights9><c:out value="${rights9}" /></rights9><!-- 출판권 -->
			<rights_etc><c:out value="${rights0}" /></rights_etc><!-- 기타 -->
		</rights>

		<fee>5000</fee><!-- 수수료 -->
		<writer><c:out value="${chgHistory.postCeoName}" /></writer><!-- 신고인(대표자) -->
		<writer2><c:out value="${chgHistory.postCeoName}" /></writer2>
	</root>
</textarea>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/enrollment_popup_2.jsp" flush="true" />				
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_appl" />
	<jsp:param name="includes" value="company_tab" />
</jsp:include>
