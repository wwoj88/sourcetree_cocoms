<%@ page language="java" contentType="application/vnd.ms-excel; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>문화체육관광부</title>
<style>
	br{mso-data-placement:same-cell;}
	.HtdBg {
		background:#CCCCFF;
		text-align:center;
	}	
</style>
</head>
<body>

<table width="757" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center"><span style="font-size:15pt">▣ 실적보고</span></td>
	</tr>
	<tr>
		<td align="right">인쇄일 : <c:out value="${date}" /></td>
	</tr>
	<tr>
		<td align="right"><c:out value="${report.coName}" /></td>
	</tr>
	<tr>
		<td align="left">1. 현황(<c:out value="${year}" />년)</td>
	</tr>
	<tr>
		<td>
			<table width="100%" cellpadding="0" cellspacing="" border="1">
				<tr>
					<td width="237" height="25" class="HtdBg"> 회원</td>
					<td colspan="2">( <c:out value="${report.thisMember}" /> ) 명/
					 ( <c:out value="${report.thisGroup}" /> ) 단체</td>
				</tr>
				<tr>
					<td height="25" class="HtdBg">관리 저작물수</td>
					<td colspan="2">( <fmt:formatNumber value="${report.thisWorknum}" type="number" /> ) 건</td>
				</tr>
			</table>
			</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td> &nbsp;&nbsp;□ 결산 및 예산</td>
	</tr>
	<tr>
		<td align="right">(단위: 천원)</td>
	</tr>
	<tr>
		<td>
			<table width="100%" height="31" cellpadding="0" cellspacing="0" border="1">
				<tr>
					<td width="230" class="HtdBg">구분</td>
					<td width="249" class="HtdBg"><c:out value="${lastYear}" />년 결산</td>
					<td width="244" class="HtdBg"><c:out value="${year}" />년 예산</td>
				</tr>
				<tr>
					<td width="237" height="25" class="HtdBg">신탁회계</td>
					<td width="250" align="center"><fmt:formatNumber value="${report.lastTrustAccnt}" type="number" /></td>
					<td width="250" align="center"><fmt:formatNumber value="${report.thisTrustAccnt}" type="number" /></td>
				</tr>
				<tr>
					<td height="25" class="HtdBg">보상금회계</td>
					<td align="center"><fmt:formatNumber value="${report.lastIndemnityAccnt}" type="number" /></td>
					<td align="center"><fmt:formatNumber value="${report.thisIndemnityAccnt}" type="number" /></td>
				</tr>
				<tr>
					<td height="25" class="HtdBg">일반회계</td>
					<td align="center"><fmt:formatNumber value="${report.lastGeneralAccnt}" type="number" /></td>
					<td align="center"><fmt:formatNumber value="${report.thisGeneralAccnt}" type="number" /></td>
				</tr>
			</table>
		</td>
	</tr>
			
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>2. 전년도 실적(<c:out value="${year}" />년)</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp; *인원 : 신규입회 회원 수 기재, 저작물 수 : 신규등록 된 저작물 수</td>
	</tr>
	<tr>
		<td>
			<table width="100%" cellpadding="0" cellspacing="0" border="1">
				<tr>
					<td width="237" height="25" class="HtdBg">회원</td>
					<td colspan="2">( <c:out value="${report.lastMember}" /> ) 명/ 
					( <c:out value="${report.lastGroup}" /> ) 단체</td>
				</tr>
				<tr>
					<td height="25" class="HtdBg">관리 저작물수</td>
					<td colspan="2"> ( <fmt:formatNumber value="${report.lastWorknum}" type="number" /> ) 건</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp; *전년도 신탁받은 관리저작물 목록을 업로드 해주시기 바랍니다.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>3. 첨부자료(<c:out value="${year}" />년)</td>
	</tr>
	<tr>
		<td>
			<c:if test="${!empty report.filename1}">
				저작물내역: <c:out value="${report.filename1}" /><br />
			</c:if>
			<c:if test="${!empty report.filename2}">
				전년도 사업실적서: <c:out value="${report.filename2}" /><br />
			</c:if>
			<c:if test="${!empty report.filename3}">
				결산서: <c:out value="${report.filename3}" /><br />
			</c:if>
			<c:if test="${!empty report.filename4}">
				감사의 감사보고서: <c:out value="${report.filename4}" /><br />
			</c:if>
		</td>
	</tr>
</table>

</body>

</html>
