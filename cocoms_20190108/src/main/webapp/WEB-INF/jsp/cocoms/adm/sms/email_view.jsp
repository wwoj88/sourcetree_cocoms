<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="SMS/이메일 관리" />
	<jsp:param name="nav2" value="이메일 발송내역" />
</jsp:include>
	
		<div class="table_area list_top sms_step_1">
			<table>
				<caption>발송내용</caption>
				<colgroup>
					<col style="width:127px" />
					<col style="width:476px" />
					<col style="width:127px" />
					<col style="width:476px" />
				</colgroup>
				<tbody>
					<tr>
						<th>발송구분</th>
						<td>
							<c:if test="${data.sendType eq '0' }">
								자동발송
							</c:if>
							<c:if test="${data.sendType ne '0' }">
								수동발송
							</c:if>
						</td>
						<th>상태</th>
						<td>
							<strong class="txt_blue" style="color:#000">
								<c:if test="${data.sendType eq '1' }">
									즉시
								</c:if>
								<c:if test="${data.sendType eq '2' }">
									예약
									<a href="" class="btn btn_style3">예약취소</a>
								</c:if>
							</strong>
						</td>
					</tr>
					<tr>
						<th>발송일시<span>(예약일시)</span></th>
						<td>
							<c:out value="${data.sendDateStr}" />
							<c:out value="${data.sendTimeStr}" />
						</td>
						<th>전송결과</th>
						<td>
							<c:if test="${data.sendYn eq 'Y' }">
								<span class="txt_blue wauto">발송완료</span>
							</c:if>
							<c:if test="${data.sendYn ne 'Y' }">
								<span class="txt_blue wauto">발송대기중</span>							
							</c:if>
						</td>
					</tr>
					<tr>
						<th>아이디</th>
						<td>
							<c:forEach var="smsMember" items="${data.smsMemberList}" varStatus="status2">
								<c:if test="${status2.index eq 0}">
									<c:out value="${smsMember.loginId}" />
								</c:if>
							</c:forEach>
							<c:if test="${fn:length(data.smsMemberList) gt 1}">
								외 ${fn:length(data.smsMemberList)} 건
							</c:if>
						</td>
						<th>수신주소</th>
						<td>
							<c:forEach var="smsMember" items="${data.smsMemberList}" varStatus="status2">
								<c:if test="${status2.index eq 0}">
									<c:out value="${smsMember.receiver}" />
								</c:if>
							</c:forEach>
							<c:if test="${fn:length(data.smsMemberList) gt 1}">
								외 ${fn:length(data.smsMemberList)} 건
							</c:if>						
						</td>
					</tr>
					<tr>
						<th>이름</th>
						<td>
							<c:forEach var="smsMember" items="${data.smsMemberList}" varStatus="status2">
								<c:if test="${status2.index eq 0}">
									<c:out value="${smsMember.ceoName}" />
								</c:if>
							</c:forEach>
							<c:if test="${fn:length(data.smsMemberList) gt 1}">
								외 ${fn:length(data.smsMemberList)} 건
							</c:if>						
						</td>
						<th>회사명</th>
						<td>
							<c:forEach var="smsMember" items="${data.smsMemberList}" varStatus="status2">
								<c:if test="${status2.index eq 0}">
									<c:out value="${smsMember.coName}" />
								</c:if>
							</c:forEach>
							<c:if test="${fn:length(data.smsMemberList) gt 1}">
								외 ${fn:length(data.smsMemberList)} 건
							</c:if>						
						</td>
					</tr>
					<tr>
						<th>제목</th>
						<td colspan="3"><c:out value="${data.subject}" /></td>
					</tr>
					<tr>
						<th>메세지<!-- <p class="txt_byte">0/90byte</p> --></th>
						<td colspan="3"><textarea name="" id="" class="input_style1 height" readonly>${data.content}</textarea></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
	
		<div class="button_area mt22">
			<div class="right">
				<a href="<c:url value="/admin/email/histories?type=${param.type}&pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />" class="btn btn_style1">목록</a>
				<a href="" id="remove2" class="btn btn_style2">삭제</a>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_sms" />
</jsp:include>
