<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="신탁단체" />
	<jsp:param name="title" value="${member.coName}" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${memberSeqNo}" />

<div class="tab_result">
	<ul>
		<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}" />">실적정보</a></li>
		<li class="active"><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab2" />">저작권료 징수분배 내역</a></li>
		<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab3" />">사업계획</a></li>
	</ul>
</div>
<!-- //step_sub -->

<div id="print_area">

	<h2 class="tit_c_s">
		1.
		<c:out value="${year}" />
		년 신탁사용료
		<!--  <a href="" class="btn btn_style3 right">기타항목 추가</a> -->
	</h2>
	<div class="table_area list list_type2 scroll">
		<div class="inner">
			<table>
				<caption></caption>
				<colgroup>
					<col style="width: 168px" />
					<col style="width: 138px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 97px" />
					<col style="width: 500px" />
				</colgroup>
				<thead>
					<tr>
						<th>구분</th>
						<th>계약 (이용건수)</th>
						<th colspan="3">
							정수액 <span>(단위 : 천원)</span>
						</th>
						<th colspan="3">
							분배액 <span>(단위 : 천원)</span>
						</th>
						<th colspan="3">
							미분배액 <span>(단위 : 천원)</span>
						</th>
						<th colspan="3">
							수수료 <span>(단위 : 천원)</span>
						</th>
						<th colspan="4">
							수수료율 <span>(단위 : %)</span>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>-</td>
						<td>-</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>비고</td>
					</tr>
					
					<c:forEach var="item" items="${royaltyList}" varStatus="status">
						<tr>
							<td>
								<%-- <c:if test="${item.gubunKind ne '1'}">
										기타<br>
								</c:if> --%>
								<c:out value="${item.gubunName}" />
							</td>
							<td>
								<fmt:formatNumber value="${item.contract}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.inCollected}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.outCollected}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.collectedSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.inDividend}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.outDividend}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.dividendSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.inDividendOff}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.outDividendOff}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.dividendOffSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.inChrg}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.outChrg}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.chrgSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.inChrgRate}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.outChrgRate}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${item.chrgRateSum}" type="number" />
							</td>
							<td>
								<textarea readonly id="trustMemo${status.count}" name="trustMemo" class="input_style1"><c:out value="${item.trustMemo}" /></textarea>
							</td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(royaltyList) gt 0}">
						<tr class="bg_total">
							<td>총계</td>
							<td>
								<fmt:formatNumber value="${contractSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${inCollectedSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${outCollectedSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${collectedSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${inDividendSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${outDividendSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${dividendSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${inDividendOffSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${outDividendOffSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${dividendOffSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${inChrgSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${outChrgSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${chrgSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${inChrgRateSum}" type="number" />
							</td>
							<td>
								<fmt:formatNumber value="${outChrgRateSum}" type="number" />
							</td>

							<td>
								<fmt:formatNumber value="${chrgRateSum}" type="number" />
							</td>
						</tr>
					</c:if>
					<c:if test="${fn:length(royaltyList) eq '0'}">
						<tr>
							<td colspan="17">등록된 신탁사용료 내용이 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
	</div>
	<!-- //table_area -->
	<ul class="txt_exp">
		<li>※ 기타 항목 추가 가능</li>
		<li>※ 국내.해외 구분은 저작물을 기준으로 함</li>
	</ul>

	<h2 class="tit_c_s">
		2.
		<c:out value="${year}" />
		년 학교교육목적, 도서관 보상금
	</h2>
	<h2 class="tit_box tit_box_n" style="margin-top: 0">
		저작물수
		<p class="txt_unit">(단위 건, 천원, %)</p>
	</h2>

	<div class="table_area list list_type1">
		<table>
			<caption></caption>
			<colgroup>
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
			</colgroup>
			<thead>
				<tr>
					<th colspan="2">구분</th>
					<th>이용단체수</th>
					<th>징수액</th>
					<th>분배액</th>
					<th>미분배액</th>
					<th>수수료</th>
					<th>수수료율</th>
					<th>비고</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${copyrightList}" varStatus="status">
					<tr>
							<c:if test="${status.index eq '0'}">
								<td rowspan="5"> 교과용<br>도서<br>보상금</td>
							</c:if>					
							<c:if test="${status.index eq '5'}">
								<td rowspan="5"> 수업목적의<br>복제등<br>보상금</td>
							</c:if>		
							<c:if test="${status.index eq '10'}">
								<td colspan="2"> 도서관보상금</td>
							</c:if>				
							<c:if test="${status.index eq '11'}">
								<td rowspan="5">수업지원<br> 목적의<br>복제등<br>보상금</td>
							</c:if>					
							<c:if test="${status.index ne '10'}">
								<td><c:out value="${item.gubunName}" /></td>
							</c:if>			
						<td>
							<fmt:formatNumber value="${item.organization}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.collected}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.dividend}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.dividendOff}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.chrg}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.chrgRate}" type="number" />
						</td>
						<td>
							<c:out value="${item.memo}" />
						</td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(copyrightList) eq '0'}">
					<tr>
						<td colspan="8">등록된 보상금(저작권자) 내용이 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
	<!-- //table_area -->

	<h2 class="tit_box tit_box_n mt25">
		실연자
		<p class="txt_unit">(단위 건, 천원, %)</p>
	</h2>
	<div class="table_area list list_type1">
		<table>
			<caption></caption>
			<colgroup>
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
			</colgroup>
			<thead>
				<tr>
					<th colspan="2">구분</th>
					<th>이용업체</th>
					<th>정수액</th>
					<th>분배액</th>
					<th>미분배액</th>
					<th>수수료</th>
					<th>수수료율</th>
					<th>비고</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${executeList}" varStatus="status">
					<tr>
						<c:if test="${status.index eq '0'}">
							<td rowspan="5">
								판매용<br>음반방송<br>보상금
							</td>
						</c:if>
						<c:if test="${status.index eq '5'}">
							<td rowspan="5">
								음반의 디음송<br>보상금
							</td>
						</c:if>
						<c:if test="${status.index eq '10'}">
							<td rowspan="5">
								판매용<br>음반의 공연<br>보상금
							</td>
						</c:if>
						<td>${item.gubunName}</td>

						<c:if test="${status.index eq '0' or status.index eq '5' or status.index eq '10'}">
							<td rowspan="4"></td>
						</c:if>
						<c:if test="${status.index eq '4' or status.index eq '9' or status.index eq '14'}">
							<td>
								<fmt:formatNumber value="${item.organization}" type="number" />
							</td>
						</c:if>
						<c:if test="${status.index eq '0' or status.index eq '5' or status.index eq '10'}">
							<td rowspan="4"></td>
						</c:if>
						<c:if test="${status.index eq '4' or status.index eq '9' or status.index eq '14'}">
							<td>
								<fmt:formatNumber value="${item.collected}" type="number" />
							</td>
						</c:if>
						<td>
							<fmt:formatNumber value="${item.dividend}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.dividendOff}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.chrg}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.chrgRate}" type="number" />
						</td>
						<td>
							<c:out value="${item.memo}" />
						</td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(executeList) eq '0'}">
					<tr>
						<td colspan="8">등록된 보상금(실연자) 내용이 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
	<!-- //table_area -->

	<h2 class="tit_box tit_box_n mt25">
		음반제작자
		<p class="txt_unit">(단위 건, 천원, %)</p>
	</h2>
	<div class="table_area list list_type1">
		<table>
			<caption></caption>
			<colgroup>
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
				<col style="width: 151px" />
			</colgroup>
			<thead>
				<tr>
					<th>구분</th>
					<th>이용업체</th>
					<th>정수액</th>
					<th>분배액</th>
					<th>미분배액</th>
					<th>수수료</th>
					<th>수수료율</th>
					<th>비고</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${makerList}" varStatus="status">
					<tr>
						<c:if test="${status.index eq '0'}">
							<td>
								판매용음반의<br>방송보상금
							</td>
						</c:if>
						<c:if test="${status.index eq '1'}">
							<td>
								음반의<br>디음송보상금
							</td>
						</c:if>
						<c:if test="${status.index gt '1'}">
							<td>
								판매용음반의<br>공연보상금
							</td>
						</c:if>
						<td>
							<fmt:formatNumber value="${item.organization}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.collected}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.dividend}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.dividendOff}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.chrg}" type="number" />
						</td>
						<td>
							<fmt:formatNumber value="${item.chrgRate}" type="number" />
						</td>
						<td>
							<c:out value="${item.memo}" />
						</td>
					</tr>
				</c:forEach>
				<c:if test="${fn:length(makerList) eq '0'}">
					<tr>
						<td colspan="8">등록된 보상금(음반제작자) 내용이 없습니다.</td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
	<!-- //table_area -->

</div>

<div class="button_area mt25">
	<a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}" />/tab2/edit" class="btn btn_style2">수정</a>
	<div class="right">
		<a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab2/save" />" class="btn btn_style2" target="_blank">엑셀출력</a> <a href="" id="print" class="btn btn_style2">인쇄</a> <a href="<c:url value="/admin/reports/trust/${member.memberSeqNo}" />" class="btn btn_style1">목록</a>
	</div>
</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_report2" />
</jsp:include>
