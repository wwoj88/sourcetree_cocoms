<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="${title1}" />
	<jsp:param name="nav2" value="${title2}" />
	<jsp:param name="nav3" value="발급현황" />
	<jsp:param name="title" value="NONE" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />
	
		<h2 class="tit_c">발급현황</h2>
		<h2 class="tit_c_s pt0">${member.coName}</h2> 
		<div class="total_top_common pt0">
			<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
		</div>
		<div class="table_area list list_type1">
		
			<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
		
			<table>
				<caption></caption>
				<colgroup>
					<col style="width:69px" />
					<col style="width:285px" />
					<col style="width:285px" />
					<col style="width:285px" />
					<col style="width:auto" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>번호</th>
						<th>일자</th>
						<th>시간</th>
						<th>업무처리 상태</th>
						<th>내용</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><c:out value="${start - status.index}" /></td>
							<td><c:out value="${item.issDateStr}" /></td>
							<td><c:out value="${item.issTimeStr}" /></td>
							<td><c:out value="${item.statCdStr}" /></td>
							<td><c:out value="${item.reportKindDesc}" /> 발급</td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(list) eq '0'}">
						<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
					</c:if>
				</tbody>
			</table>
		
			</form>
			
		</div>
		<!-- //table_area -->
		<div id="pagination" class="pagination button_area mt22">
			<ul>
	       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
			</ul>
			<div class="right">
				<!--  
				<c:if test="${member.conditionCd eq '1' }">
					<a href="<c:url value="/admin/sub/companies" />" class="btn btn_style1">이전</a>
				</c:if>
				<c:if test="${member.conditionCd ne '1' }">
					<a href="<c:url value="/admin/trust/companies" />" class="btn btn_style1">이전</a>
				</c:if>
				-->
				<a href="javascript:history.back()" class="btn btn_style1">이전</a>
			</div>
		</div>
		
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_appl" />
	<jsp:param name="includes" value="company_tab" />
</jsp:include>
