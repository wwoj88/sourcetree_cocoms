<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="신탁단체" />
	<jsp:param name="title" value="${member.coName}" /> 
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${memberSeqNo}" />
	
		<div class="tab_result">
			<ul>
				<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}" />">실적정보</a></li>
				<li><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab2" />">저작권료 징수분배 내역</a></li>
				<li class="active"><a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab3" />">사업계획</a></li>
			</ul>
		</div>
		<!-- //step_sub -->
		
	<div id="print_area">
		
		<h2 class="tit_c_s">1. 사업목표<span>(<c:out value="${year+1}" />년)</span></h2>
		<textarea name="" class="input_style1 height_bottom_n mb0" readonly>${report.businessGoal}</textarea>
		<h2 class="tit_c_s">2. 추친방향<span>(<c:out value="${year+1}" />년)</span></h2>
		<textarea name="" class="input_style1 height_bottom_n mb0" readonly>${report.prop}</textarea>
		<h2 class="tit_c_s">3. 첨부자료<span>(<c:out value="${year+1}" />년)</span></h2>
		<div class="list_finl_link list_finl_link2 bg">
			<ul>
				<li><p>당해연도 사업계획서</p>
					<c:if test="${!empty report.filename5}">
						<a href="<c:url value="/download?filename=${report.filepath5}" />"><c:out value="${report.filename5}" /></a>
					</c:if>
					<c:if test="${empty report.filename5}">
						(첨부자료없음)
					</c:if>
				</li>
				<li><p>사용료 징수.분배계획</p>
					<c:if test="${!empty report.filename6}">
						<a href="<c:url value="/download?filename=${report.filepath6}" />"><c:out value="${report.filename6}" /></a>
					</c:if>
					<c:if test="${empty report.filename6}">
						(첨부자료없음)
					</c:if>
				</li>
				<li><p>보상금 징수.분배계획<span>(첨부자료없음)</span></p>
					<c:if test="${!empty report.filename7}">
						<a href="<c:url value="/download?filename=${report.filepath7}" />"><c:out value="${report.filename7}" /></a>
					</c:if>
					<c:if test="${empty report.filename7}">
						(첨부자료없음)
					</c:if>
				</li>
				<li><p>예산편성안</p>
					<c:if test="${!empty report.filename8}">
						<a href="<c:url value="/download?filename=${report.filepath8}" />"><c:out value="${report.filename8}" /></a>
					</c:if>
					<c:if test="${empty report.filename8}">
						(첨부자료없음)
					</c:if>
				</li>
				<li><p>정기총회 자료집 </p>
					<c:if test="${!empty report.filename9}">
						<a href="<c:url value="/download?filename=${report.filepath9}" />"><c:out value="${report.filename9}" /></a>
					</c:if>
					<c:if test="${empty report.filename9}">
						(첨부자료없음)
					</c:if>
				</li>
			</ul>
		</div>
		<!-- //list_finl_link -->

	</div>		
		
		<div class="button_area">
			<a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}" />/tab3/edit" class="btn btn_style2">수정</a>
			<div class="right">
				<a href="<c:url value="/admin/reports/trust/${memberSeqNo}/years/${year}/tab3/save" />" class="btn btn_style2" target="_blank">엑셀출력</a>
				<a href="" id="print" class="btn btn_style2">인쇄</a>
				<a href="<c:url value="/admin/reports/trust/${member.memberSeqNo}" />" class="btn btn_style1">목록</a>
			</div>
		</div>
				
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_report" />
</jsp:include>
