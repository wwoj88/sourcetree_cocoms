<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="업체현황" />
	<jsp:param name="nav2" value="통계조회" />
</jsp:include>
	
		<div class="table_area list_top list_type3">
			<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/stats_search.jsp" flush="true" />
		</div>
		<!-- //table_area -->
		<h2 class="tit_c_s"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 
		<strong class="txt_blue">신탁단체별 신탁사용료</strong></h2>
		<div class="table_area list list_type2 scroll">
			<div class="inner">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:168px" />
						<col style="width:138px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
					</colgroup>
					<thead>
						<tr> 	 
							<th>구분</th>
							<th>계약 (이용건수)</th>
							<th colspan="3">징수액 <span>(단위 : 천원)</span></th>
							<th colspan="3">분배액 <span>(단위 : 천원)</span></th>
							<th colspan="3">미분배액 <span>(단위 : 천원)</span></th>
							<th colspan="3">수수료 <span>(단위 : 천원)</span></th>
							<th colspan="3">수수료율 <span>(단위 : %)</span></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>-</td>
							<td>-</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
						</tr>
						<c:forEach var="item" items="${list}" varStatus="status">
							<c:set var="cls" value="" />
							<c:if test="${item.memberSeqNo eq '0'}">
								<c:set var="cls" value="bg_total" />
							</c:if>
							<tr class="${cls}">
								<td>
									<c:if test="${item.memberSeqNo ne '0'}">
										<%-- <c:out value="${item.name}"  escapeXml="false"/> --%>
										<c:out value="${item.name}"  escapeXml="false"/>
									<%-- <a href="<c:url value="/admin/status/stats?conditionCd=${conditionCd}&fromYear=${fromYear}&toYear=${toYear}&type=${type}&memberseqno=${item.memberSeqNo}" />" ><c:out value="${item.name}"  escapeXml="false"/></a> --%>
									</c:if>
									<c:if test="${item.memberSeqNo eq '0'}">
										합계
									</c:if>
								</td>
								<td><fmt:formatNumber value="${item.cnt}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val11}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val12}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val13}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val21}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val22}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val23}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val31}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val32}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val33}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val41}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val42}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val43}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val51}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val52}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val53}" type="number" /></td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(list) eq '0'}">
							<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //table_area -->
		<div class="button_area mt25">
			<div class="right">
				<a href="<c:url value="/admin/status/stats/save?conditionCd=${conditionCd}&fromYear=${fromYear}&toYear=${toYear}&type=${type}" />" id="excel" class="btn btn_style2">엑셀출력</a>
				<%-- 
				<a href="" id="list" class="btn btn_style1">목록</a>
				--%>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_stat" />
	<jsp:param name="includes" value="stats_search" />
</jsp:include>
