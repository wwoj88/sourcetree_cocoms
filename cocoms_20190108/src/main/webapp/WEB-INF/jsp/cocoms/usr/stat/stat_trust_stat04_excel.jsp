<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<title>저작권 종류별 보상금 - 저작권자 </title>
</head>
<body>

	<table cellpadding="0" cellspacing="1" border="1">
		<tr>
			<td colspan="8"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 저작권 종류별 보상금 - 저작권자</td>
		</tr>
		<tr>
			<td colspan="8" align="right">(단위 건, 천원, %)</td>
		</tr>
		<tr>
			<td colspan="2" align="center">구분	</td>
			<td align="center">이용단체</td>
			<td align="center">징수액(천원)</td>
			<td align="center">분배액(천원)</td>
			<td align="center">미분배액(천원)</td>
			<td align="center">수수료(천원)</td>
			<td align="center">수수료율(%)</td>
		</tr>
		<c:forEach var="item" items="${list}" varStatus="status">
			<tr align="center">
				<c:if test="${status.index eq '0'}"><td rowspan="5" align="center">교과용도서보상금</td></c:if>
				<c:if test="${status.index eq '5'}"><td rowspan="5" align="center">수업목적의복제등보상금</td></c:if>
				<c:if test="${status.index eq '10'}"><td colspan="2" align="center">도서관보상금</td></c:if>
				<c:if test="${status.index ne '10'}">
					<td align="center">
						<c:choose>
							<c:when test="${item.tit eq '1'}">어문</c:when>
							<c:when test="${item.tit eq '2'}">미술</c:when>
							<c:when test="${item.tit eq '3'}">사진</c:when>
							<c:when test="${item.tit eq '4'}">음악</c:when>
							<c:when test="${item.tit eq '5'}">계</c:when>
							<c:when test="${item.tit eq '6'}">복제</c:when>
							<c:when test="${item.tit eq '7'}">공연</c:when>
							<c:when test="${item.tit eq '8'}">방송</c:when>
							<c:when test="${item.tit eq '9'}">전송</c:when>
							<c:when test="${item.tit eq '10'}">계</c:when>
						</c:choose>
					</td>
					<td><fmt:formatNumber value="${item.organization}" type="number" /></td>
					<td><fmt:formatNumber value="${item.collected}" type="number" /></td>
					<td><fmt:formatNumber value="${item.dividend}" type="number" /></td>
					<td><fmt:formatNumber value="${item.dividendOff}" type="number" /></td>
					<td><fmt:formatNumber value="${item.chrg}" type="number" /></td>
					<td><fmt:formatNumber value="${item.chrgRate}" type="number" /></td>
				</c:if>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(list) eq '0'}">
			<tr><td align="center" colspan="8">등록된 보상금(저작권자) 내용이 없습니다.</td></tr>
		</c:if>
	</table>

</body>
</html>