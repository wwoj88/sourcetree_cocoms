<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<c:choose>
	<c:when  test="${result eq true}">
	<c:set var="msg" value="결제가 완료 되었습니다. 신고 처리시간이 소요될 수 있습니다. 신고 처리문의 ( tel. 044-203-2480 )" />
	</c:when>
	<c:otherwise>
	<c:set var="msg" value="결제중 오류가 발생하였습니다." />
	</c:otherwise>
</c:choose>


<script>
	
	<c:if test="${result ne true}">
	alert('<c:out value="${msg}" />');
		opener.location.reload();
	</c:if>

	<c:if test="${result eq true}">
	alert('<c:out value="${msg}" />');
		opener.location.href = "<c:url value='/main' />";
	</c:if>
	
	window.close();
</script>
