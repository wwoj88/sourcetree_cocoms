<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="user" />
</jsp:include>
<script>
/*  $(document).ready(function() {  
 
         $("#form0")[0].reset();    
  
 });  
 */

</script> 
<div class="container">
	<h2 class="tit_bar blue mt">통합회원가입</h2>
	<div class="join_content step_1 normal combine">
		<div class="inner">
			<form id="form0" method="post" action="?step=1">
			
				<h3 class="tit">통합회원 가입안내</h3>

				<div class="type">
					<ul>
						<li>
							<div class="icon">
								<img src="<c:url value="/img/img_combine_1.png" />" alt="" />
							</div>
						</li>
						<li>
							<div class="icon">
								<img src="<c:url value="/img/img_combine_2.png" />" alt="" />
							</div>
						</li>
					</ul>
				</div>
				<!-- //type -->
				<div class="ment_b step_1">

					<div class="box_top_common">
						<ul>
							<!-- <li>저작권위탁관리업시스템 / 권리자찾기정보시스템의 통합회원으로 가입하시면 하나의 아이디로 저작권위탁관리업시스템(www.cocoms.go.kr)과 권리자찾기정보시스템(www.findcopyright.or.kr)을 모두 이용할 수 있습니다.</li>
							<li>이미 가입되어있는 아이디가 있으신 경우 통합회원 전환하기를 통해 각 사이트의 서비스를 한 아이디로 이용하실 수 있습니다.</li> -->
							<li>공인인증서를 재발급 받으셨을 경우 공인인증서를 재등록후 로그인하실 수 있습니다.</li>
							<li>개인정보보호를 위해 서비스를 이용하신 후 반드시 로그아웃을 하시기 바랍니다.</li>
						</ul>
					</div>
					<!-- //box_top_common -->
				</div>
				<!-- //ment_b -->
				<div class="button_area">
					<input id="form0_12" type="button" class="btn btn_style2" value="개인회원"/>
					<input id="form0_13" type="button" class="btn btn_style2" value="사업자회원"/>	
					<!-- <input id="form0_14" type="button" class="btn btn_style2" value="법인회원"/> -->	
				</div>
			
			</form>
		
		</div>

	</div>
	<!-- //join_content -->
</div>
<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="user" />
	<jsp:param name="cert" value="true" />
</jsp:include>
