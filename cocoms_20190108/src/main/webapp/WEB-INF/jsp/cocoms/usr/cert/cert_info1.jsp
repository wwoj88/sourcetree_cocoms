<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">
</head>
<body >
	<div class="pop_common">
		<div class="inner">
			<h2 class="tit txt_c">인터넷 증명발급 서비스</h2>
			<div class="cont">
				<div class="list_step_id">
					<ul>
						<li class="active"><a href="<c:url value="/cert/info1" />">위 &#183;변조 방지란?</a></li>
						<li><a href="<c:url value="/cert/info2" />">위&#183;변조 방지 적용기법</a></li>
						<li><a href="<c:url value="/cert/info3" />">위&#183;변조 검증방법</a></li>
						<li><a href="<c:url value="/cert/info4" />">증명서 출력 FAQ</a></li>
					</ul>
				</div>
				<!-- //list_step -->
				<div class="txt_area">
					<p class="tit_box">인터넷으로 발행된 증명서는 인쇄된 문서를 수납한 기관의 담당자가 그 원본의 진위를 확인할 수 있도록 위&#183;변조 기술이 적용된 문서 하단부의 고밀도 바코드를 스캐너로 읽어들여 검증하게 됩니다.</p>
				</div>
				<!-- //txt_area -->
				<div class="img">
					<img src="<c:url value="/img/img_cert1_1.png" />" alt="" />
				</div>
				<div class="txt_area">
					<p class="tit_box">
						인터넷 증명발급 서비스를 여러분의 PC에서 직접 인쇄를 하게 되는 과정은 모두 안전한 보안처리를 거치게 되며, 이때 여러분의 화면캡처를 방지하고 데이터 변조를 방지하기 위한 사용상의 제약이 따르게 됩니다.<br> 사용상 제약에 대한 정보는 인터넷 증명발급 서비스 출력 FAQ를 참조하시기 바랍니다.
					</p>
				</div>
				<!-- //txt_area -->
			</div>
		</div>
	</div>
	<!-- //pop_common -->
	<script src="<c:url value='/js/jquery-1.10.2.min.js' />"></script>
	<script src="<c:url value='/js/jquery.bxslider.js' />"></script>
	<script src="<c:url value='/js/script.js' />"></script>
	<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
	<script>
		var CTX_ROOT = '/cocoms';
	</script>
	<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
	<script src="<c:url value='/js/usr/cert.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</body>
</html>
