<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="login" />
</jsp:include>
	
	<script>
	window.onload = function () {
     var chg = ${chg};
     if(chg=='1'){
    	 alert("비밀번호 변경이 완료되었습니다.")
    	 window.location.href = 'https://www.cocoms.go.kr/cocoms/login';
     }else{
    	 }
     }
	</script>
	<div class="container">
		<h2 class="tit_bar blue mt">로그인</h2>
		<div class="login_content login_content_n">
			<div class="inner">
				<div class="cell cell2">
					<h3 class="tit">회원 로그인</h3>
					<p id="message" style="color: red; margin-bottom: 20px;">${message}</p>
					<form id="form" method="post">
					<input type="hidden" name="userDn" />
					<input type="hidden" name="certSn" />
						<div class="input_login">
							<input type="text" id="loginID" name="loginId" class="input_style1" maxlength="50" placeholder="아이디" style="ime-mode:inactive;"/>
							<input type="password" autocomplete="off" name="pwd" class="input_style1" maxlength="20" placeholder="비밀번호" />
							<input type="submit" class="btn_style2" value="로그인">
						</div>
						<div class="check">
							<div class="btm_form_box">
								<input type="checkbox" id="save_id" />
								<label for="save_id">아이디저장</label> 
							</div>
						</div>
					</form>
					<div class="link">
						<a href="<c:url value="/user/new" />" class="margin">회원가입</a>
						<a href="<c:url value="/find/id" />">아이디찾기</a>	/<a href="<c:url value="/find/password" />">비밀번호찾기</a>
					 	<a href="<c:url value="/cert/enroll" />" class="right">공인인증서 등록</a>						 
					</div>
				</div>
				<!-- //cell -->	
			</div>
			<!-- //inner -->	
			<div class="box_top_common">
				<ul>
					<li>저작권위탁관리업시스템의 모든 서비스는 <strong>로그인 및 공인인증서 인증</strong>이 필요한 서비스입니다.</li>
					<li>공인인증서를 재발급 받으셨을 경우 공인인증서를 재등록후 로그인하실 수 있습니다.</li>
					<li>개인정보보호를 위해 서비스를 이용하신 후 반드시 로그아웃을 하시기 바랍니다.</li>
				</ul>
			</div>
			<!-- //box_top_common -->
		</div>
		<!-- //login_content -->
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="login" />
	<jsp:param name="cert" value="true" />
</jsp:include>