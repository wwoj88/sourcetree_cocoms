<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="excel" />
	<jsp:param name="nav1" value="위탁관리저작물 보고" />
	<jsp:param name="nav2" value="보고현황" />
</jsp:include>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>


<div class="container">
	<h2 class="tit_bar pink mt">보고현황</h2>

		<div class="table_area list_top list_type3">
			
			<form id="searchForm" method="GET" >
			<input type="hidden" name="pageIndex" value="${page}" />
			
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:125px" />
						<%-- <col style="width:auto" />
						<col style="width:192px" /> --%>
						<col style="width:auto" />
					</colgroup>
					<tbody>
						
						<tr>
							<th>년도</th>
							<td>
						
								<select name="year" class="select_style w172 ml20">
									<option selected="selected"  value="" >-선택해주세요-</option>${yeaerList}
									
									<c:forEach var="item" items="${yearList}" varStatus="status">
										<c:set var="selected" value="" />
										<c:if test="${item eq year}">
											<c:set var="selected" value="selected" />
										</c:if>
										<option value="${item}" ${selected}><c:out value="${item}" /></option>
									</c:forEach>
								</select>
						
							<a href="" id="search" class="btn_style2 btn_search" style="height: 50px;line-height: 50px;width:125px; float:right;">검색</a>	
							</td>
						</tr>		
					
					</tbody>
				</table>
				
			</form>
			
			
		</div>
		<br>
	
	<br>
	<!-- //table_area -->
	<div class="table_area list list_type1">
		<table id="data">
				<caption>보고현황</caption>
				<colgroup>
					
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:12%" />

				</colgroup>
				<thead>
					<tr> 	 
						
						<th>보고년월</th>
						<th>보고일자</th>
						<th>음악</th>
						<th>어문</th>
						<th>방송대본</th>
						<th>영화</th>
						<th>방송</th>
						<th>뉴스</th>
						<th>미술</th>
						<th>이미지</th>
						<th>사진</th>
						<th>기타</th>
						<th>계</th>
						
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty list}">
							<tr>
								<td colspan="13" class="text-center">게시물이 없습니다.(년도 검색을 해주세요.)</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:set var="sum1" value="0" />
							<c:set var="sum2" value="0" />
							<c:set var="sum3" value="0" />
							<c:set var="sum4" value="0" />
							<c:set var="sum5" value="0" />
							<c:set var="sum6" value="0" />
							<c:set var="sum7" value="0" />
							<c:set var="sum8" value="0" />
							<c:set var="sum12" value="0" />
							<c:set var="sum9" value="0" />
							<c:set var="sum10" value="0" />
							<c:forEach var="info" items="${list}" varStatus="listStatus">
								<c:set var="reptYmd2" value="${info.REPT_YMD }"></c:set>
								<tr>
									<td class="text-center">
										<a style="color: blue;" href="/cocoms/report/exceldetail/${reptYmd2}" ><b><u><c:out value="${info.REPT_YMD }" /></u></b></a>
									</td>
									<td class="text-center">
										<c:out value="${info.REPTDATE }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_1 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_2 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_3 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_4 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_5 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_6 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_7 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_8 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_12 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_9 }" />
									</td>
									<td class="text-center">
										<c:out value="${info.CNT_TOT }" />
									</td>
									<c:set var="sum1" value="${ sum1 + info.CNT1_ }" />
									<c:set var="sum2" value="${ sum2 + info.CNT2_ }" />
									<c:set var="sum3" value="${ sum3 + info.CNT3_ }" />
									<c:set var="sum4" value="${ sum4 + info.CNT4_ }" />
									<c:set var="sum5" value="${ sum5 + info.CNT5_ }" />
									<c:set var="sum6" value="${ sum6 + info.CNT6_ }" />
									<c:set var="sum7" value="${ sum7 + info.CNT7_ }" />
									<c:set var="sum8" value="${ sum8 + info.CNT8_ }" />
									<c:set var="sum12" value="${ sum12 + info.CNT12_ }" />
									<c:set var="sum9" value="${ sum9 + info.CNT9_ }" />
									<c:set var="sum10" value="${ sum10 + info.CNT10_ }" />
								</tr>
							</c:forEach>
							<tr>
								<td class="text-center">
									<b>합계</b>
								</td>
								<td></td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum1}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum2}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum3}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum4}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum5}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum6}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum7}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum8}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum12}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum9}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
								<td class="text-center">
									<b><u><fmt:formatNumber value="${sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9+sum10}" pattern="###,###,###,##0" groupingUsed="true" /></u></b>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
				
				
				
			</table>
	</div>
	<!-- //table_area -->
	<div id="pagination" class="pagination">
	
	</div>
	
</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="js" value="report" />
</jsp:include>
