<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">
</head>
<body  style=“overflow-X:hidden”>

<div  style="overflow-y:scroll;" class="pop_common">
	<div class="inner">
		<h2 class="tit txt_c">인터넷 증명발급 서비스</h2>
		<div class="cont">
			<div class="list_step_id">
				<ul>
					<li><a href="<c:url value="/cert/info1" />">위	&#183;변조 방지란?</a></li>
					<li class="active"><a href="<c:url value="/cert/info2" />">위&#183;변조 방지 적용기법</a></li>
					<li><a href="<c:url value="/cert/info3" />">위&#183;변조 검증방법</a></li>
					<li><a href="<c:url value="/cert/info4" />">증명서 출력 FAQ</a></li>
				</ul>
			</div>
			<!-- //list_step -->
			
			<div class="txt_area">
				<p class="tit_box">인터넷으로 발급된 증명서에는 문서 발급과정상에서 위&#183;변조 방지를 위해 다음과 같은 기술들이 적용되어 있습
니다.</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="<c:url value="/img/img_cert2_1.png" />" alt="" /></div>
			<div class="txt_area txt_c">
				<p class="txt_blue bold">[PC 모니터로 보이는 미리보기 화면]</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="<c:url value="/img/img_cert2_2.png" />" alt="" /></div>
			<div class="txt_area txt_c">
				<p class="txt_blue bold">[프린터로 출력된 실제 증명서의 모습]</p>
			</div>
			
			<div class="txt_area">
				<p class="tit_box"><strong>워터마크 :</strong> 증명서 전체에 워터마크를 삽입하여 “복사방지코드”와 함께 조합되어 화면캡처를 할 수 없습니다.</p>
				<p class="tit_box"><strong>복사방지 코드 :</strong> 원본과 복사한 사본을 육안으로 구분해주는 역할</p>
				<p class="tit_box"><strong>2차원바코드 :</strong> 원본증명서의 전체내용을 삽입하여 스캐너를 통해 2차원바코드를 읽어 원본문서를 복원하여
                        문서의 진위를 확인할 수 있는 요소로 다음과 같은 기술적 특징을 갖습니다.</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="<c:url value="/img/img_cert2_3.png" />" alt="" /></div>

		</div>
	</div>
</div>
<!-- //pop_common -->

<script src="<c:url value='/js/jquery-1.10.2.min.js' />"></script>
<script src="<c:url value='/js/jquery.bxslider.js' />"></script>
<script src="<c:url value='/js/script.js' />"></script>

<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script src="<c:url value='/js/usr/cert.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</body>
</html>
