<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="login" />
	<jsp:param name="nav1" value="로그인" />
	<jsp:param name="nav2" value="아이디 찾기" />
</jsp:include>

	<div class="container">
		<h2 class="tit_bar blue mt">아이디 찾기</h2>
		<div class="join_content">
			<div class="list_step_id">
				<!--  
				<ul>
					<li>내 명의로 가입된 휴대폰 인증</li>
					<li>내 정보에 등록된 휴대폰으로 찾기</li>
					<li>이메일로 찾기</li>
					<li>공인인증서 인증</li>
					<li>사업자등록번호로 찾기</li>
				</ul>
				-->
			</div>
			<!-- //list_step -->

			<div class="login_content login_content_n pw pwid">
				<div class="inner">
					<div class="cell cell2">
						<c:forEach var="item" items="${list}" varStatus="status">
							<div class="box_input">
								<p class="txt1">아이디</p><p class="txt2"><c:out value="${item.loginId}" /></p>
								<p class="txt1">가입일</p><p class="txt2"><c:out value="${item.regDateStr}" /></p>
								<p class="txt3">
									<c:choose>
										<c:when test="${item.conditionCd eq '1'}">
											대리중개업
										</c:when>
										<c:when test="${item.conditionCd eq '2'}">
											신탁관리업
										</c:when>
										<c:otherwise>
											미신청
										</c:otherwise>
									</c:choose>
								</p>
								<form method="post" action="<c:url value="/find/password" />" style="float: right; margin-right: 20px;">
									<input type="hidden" name="loginId" value="${item.loginId}" />
									<input type="hidden" name="certSn" value="" />
									<input type="submit" class="btn btn_style1" value="비밀번호 변경" />
								</form>
							</div>
						</c:forEach>
					</div>
					<!-- //cell -->	
				</div>
				<!-- //inner -->	
				<div class="box_top_common">
					<ul>
						<li>저작권위탁관리시스템은 본인명의로 두 개의 아이디를 만들 수 있습니다.</li>
						<li>새로운 아이디로 가입을 원하실 경우 기존 아이디 탈퇴 후 가입하실 수 있습니다.</li>
					</ul>
				</div>
				<!-- //box_top_common -->
				<div class="button_area">
					<a href="<c:url value="/main" />" class="btn btn_style1">홈</a>
					<a href="<c:url value="/login" />" class="btn btn_style2">로그인</a>
				</div>
			</div>
		</div>
		<!-- //join_content -->	
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="login" />
	<jsp:param name="cert" value="true" />
</jsp:include>
