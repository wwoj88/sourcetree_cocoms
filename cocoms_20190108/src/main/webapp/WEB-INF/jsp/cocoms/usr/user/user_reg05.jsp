<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="user" />
</jsp:include>

	<div class="container">
		<h2 class="tit_bar blue mt">회원가입</h2>
		<div class="join_content step_1">
			<div class="list_step">
				<ul>
					<li>
						<div class="wrap">
							<p>STEP 01</p>
							<strong>회원유형선택 및 본인인증</strong>
						</div>
					</li>
					<li>
						<div class="wrap">
							<p>STEP 02</p>
							<strong>이용약관 동의</strong>
						</div>
					</li>
					<li>
						<div class="wrap">
							<p>STEP 03</p>
							<strong>회원정보 입력</strong>
						</div>
					</li>
					<li class="active">
						<div class="wrap">
							<p>STEP 04</p>
							<strong>회원가입 완료</strong>
						</div>
					</li>
				</ul>
			</div>
			<!-- //list_step -->	
			<div class="inner">
				<div class="txt_complet">
					<div class="icon"><img src="<c:url value='/img/icon_join_complete.png' />" alt="" /></div>
					<p>회원가입신청이 완료되었습니다.</p>
					<p class="txt">로그인 후 서비스 이용이 가능합니다.</p>
				</div>
				<!-- //txt_complet -->	
				<div class="button_area">
					<a href="<c:url value='/main' />" class="btn btn_style1">홈</a>
					<a href="<c:url value='/login' />" class="btn btn_style2">로그인</a>
				</div>
			</div>
			
		</div>
		<!-- //join_content -->	
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="user" />
</jsp:include>
