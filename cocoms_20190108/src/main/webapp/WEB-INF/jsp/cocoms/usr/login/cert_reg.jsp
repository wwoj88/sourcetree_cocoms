<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="login" />
	<jsp:param name="nav1" value="로그인" />
	<jsp:param name="nav2" value="공인인증서 등록" />
</jsp:include>

	<div class="container">
		<h2 class="tit_bar blue mt">공인인증서 등록</h2>
		<div class="login_content regi">
			<div class="inner">
				<div class="cell cell1">
					<h3 class="tit">개인</h3>
					<form id="formEnroll" method="POST">
					<input type="hidden" name="certType" value="1" />
					<input type="hidden" name="userDn" />
					<input type="hidden" name="certSn" />
					<input type="hidden" name="certificate" />
					<input type="hidden" name="certStartDate" />
					<input type="hidden" name="certEndDate" />
						<div class="box_input">
						<strong style="width:73.33px">회원 아이디</strong>
							<input type="text" name="loginId" class="input_style1"  />
					      <div class="box_input">
							<strong>주민등록번호</strong>
							<input type="text" name="ceoRegNo1" class="input_style1" maxlength="6" placeholder="" title="주민등록번호 앞자리" /><span>-</span
							><input type="password" name="ceoRegNo2" class="input_style1" maxlength="7" placeholder="" title="주민등록번호 뒷자리" />
						</div>	 				
    				 </div>
    					<div class="button_area">
							<a href="" id="submit" class="btn btn_style2">등록</a>
						</div>
						
					</form>
				</div>
				<!-- //cell -->	
				<div class="cell cell2">
					<h3 class="tit">사업자 <span>(개인/법인)</span></h3>
					<form id="formEnroll2" method="POST">
					<input type="hidden" name="certType" value="2" />
					<input type="hidden" name="userDn" />
					<input type="hidden" name="certSn" />
					<input type="hidden" name="certificate" />
					<input type="hidden" name="certStartDate" />
					<input type="hidden" name="certEndDate" />
					<div class="box_input">
						<strong style="width:85.55px">회원 아이디</strong>
							<input type="text" style="width:149px"name="loginId" class="input_style1"  />
						 <div class="box_input">
							<strong>사업자등록번호</strong>
							<input type="text" name="coNo1" class="input_style1" maxlength="3" placeholder="" title="사업자번호 첫번째자리" /><span>-</span
							><input type="text" name="coNo2" class="input_style1" maxlength="2" placeholder="" title="사업자번호 두째자리" /><span>-</span
							><input type="password" name="coNo3" class="input_style1" maxlength="5" placeholder="" title="사업자번호 세째자리" />
					      </div>		 				
    				 </div>
    					<div class="button_area">
							<a href="" id="submit2" class="btn btn_style2">등록</a>
						</div>
					</form>
				</div>
				<!-- //cell -->	
			</div>
			<!-- //inner -->	
			<div class="box_top_common">
				<ul>
					<li>공인인증서를 재발급 받으셨다면 공인인증서를 재 등록 후 로그인하실 수 있습니다.</li>
					<li>공인인증서 인증을 통하여 가입한 회원은 자동으로 공인인증서가 등록됩니다.</li>
					<li>공인인증서는 가까운 은행, 우체국, 증권사에서 인터넷 뱅킹, 증권거래용 인증서를 발급 받으실 수 있습니다.</li>
				</ul>
			</div>
			<!-- //box_top_common -->
		</div>
		<!-- //login_content -->	
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="login" />
	<jsp:param name="cert" value="true" />
</jsp:include>
