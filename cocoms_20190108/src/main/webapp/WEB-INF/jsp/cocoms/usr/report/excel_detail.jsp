<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="excel" />
	<jsp:param name="nav1" value="위탁관리저작물 보고" />
	<jsp:param name="nav2" value="보고현황" />
</jsp:include>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>


<div class="container">
	<h2 class="tit_bar pink mt">보고현황</h2>

		
	<!-- //table_area -->
	<div class="table_area list list_type1">
		<table id="data">
					<caption> 대리중개업체</caption>
					<colgroup>
						<col style="width:156px" />
						<col style="width:427px" />
						<col style="width:156px" />
						<col style="width:427px" />
					</colgroup>
					<tbody>
					
				<c:forEach var="info" items="${info}" varStatus="listStatus">
						<tr>
							<th>보고기관</th>
							<td >
								<c:out value="${info.COMM_NAME}" escapeXml="false"/>
							</td>
							<th>보고일</th>
							<td >
								<c:out value="${info.RGST_DTTM}" escapeXml="false"/>
							</td>
							
						</tr>
						<tr>
							<th>담당자 직위</th>
							<td>
								<c:out value="${info.REPT_CHRR_POSI}"  escapeXml="false"/>
								
							</td>
							<th>담당자 전화번호</th>
							<td>
								<c:out value="${info.REPT_CHRR_TELX}"  escapeXml="false"/>
						
							</td>
						</tr>
						<tr>
							<th  style="    border-bottom: 1px solid #000;">담당자</th>
							<td>
								<c:out value="${info.REPT_CHRR_NAME}"  escapeXml="false"/>
							</td>
							<th  style="    border-bottom: 1px solid #000;">담당자 이메일</th>
							<td>
								<c:out value="${info.REPT_CHRR_MAIL}"  escapeXml="false"/>
								 
							</td>
							
						</tr>
							</c:forEach>
					</tbody>
				</table>
			<h2 class="tit_bar pink mt">상세조회</h2>
		<div class="total_top_common" style="padding-top:0">
			
		</div>
	
		
		<div class="table_area list list_type1">
			<table>
				<caption> 상세조회</caption>
				<colgroup>
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
					<col style="width:7%" />
				</colgroup>
				<thead>
					<tr> 	 
						
						<th>보고건수</th>						
						<th>음악</th>
						<th>어문</th>
						<th>방송대본</th>
						<th>영화</th>
						<th>방송</th>
						<th>뉴스</th>
						<th>미술</th>
						<th>이미지</th>
						<th>사진</th>
						<th>기타</th>
					
						
					</tr>
				</thead>
				<tbody>
				
				<c:choose>
				<c:when test="${empty list}">
				<td colspan="13" class="text-center">게시물이 없습니다.(년도 검색을 해주세요.)</td>
				</c:when>
				<c:otherwise>
					<c:forEach var="count" items="${list}" varStatus="listStatus">
					<tr>
						<th style="    border-bottom: 1px solid #000;"><c:out value="${count.CNT_TOT }"/></th>
						<td class="text-center"><c:out value="${count.CNT_1 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_2 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_3 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_4 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_5 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_6 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_7 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_8 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_12 }"/></td>
						<td class="text-center"><c:out value="${count.CNT_9 }"/></td>
					</tr>
					</c:forEach>
				</c:otherwise>
				</c:choose>
				
				</tbody>
			</table>
		</div>
				
				
	</div>
	<!-- //table_area -->
	<div id="pagination" class="pagination">
	
	</div>
	
</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="js" value="report" />
</jsp:include>
