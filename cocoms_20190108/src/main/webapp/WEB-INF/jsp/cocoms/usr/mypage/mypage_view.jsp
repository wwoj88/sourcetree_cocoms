<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="회원정보 수정" />
</jsp:include>
	
	<script>
	window.onload = function () {
     var chg = ${chg};
     if(chg=='1'){
    	 alert("비밀번호 변경이 완료되었습니다.")
     }else{
    	 }
     }
	</script>
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li class="active"><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<%-- <li><a href="<c:url value="/qna" />">1:1문의 내역</a></li> --%>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
		
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage">
			<h2 class="tit_bar mt">회원정보 수정</h2>
			<div style="position: relative; font-size: 18px; color: #4b4a4a; font-weight: 600; overflow: hidden; padding: 26px 0 12px;">
				회원정보
				<div style="position: absolute; right: 0; bottom: 7px;">
					<a href="<c:url value="/appl/sub/new" />" class="btn btn_style1">대리중개업신청</a>
					<a href="<c:url value="/appl/trust/new" />" class="btn btn_style1">신탁관리업신청</a>
				</div>
			</div>
			<div class="table_area list_top">
				<table>
					<caption>회원정보</caption>
					<colgroup>
						<col style="width:200px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>가입일자</th>
							<td>
								<c:out value="${login.regDateStr}" />
							</td>
						</tr>
						<tr>
							<th>회원구분</th>
							<td>
								<c:if test="${member.coGubunCd eq '1'}">
									개인회원
								</c:if>
								<c:if test="${member.coGubunCd eq '2'}">
									사업자회원
								</c:if>
							</td>
						</tr>
						<tr>
							<th>이름 <c:if test="${member.coGubunCd eq '2'}">(담당자)</c:if></th>
							<td>
								<c:out value="${member.ceoName}" />
							</td>
						</tr>
						<tr>
							<th>아이디</th>
							<td>
								<c:out value="${login.loginId}" />
							</td>
						</tr>
						<tr>
							<th>휴대전화번호</th>
							<td>
								<c:out value="${member.ceoMobile}" />
							</td>
						</tr>
						<tr>
							<th>이메일 주소</th>
							<td>
								<c:out value="${member.ceoEmail}" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div style="position: relative; font-size: 18px; color: #4b4a4a; font-weight: 600; overflow: hidden; padding: 26px 0 12px;">
				사업자정보
			</div>
			<div class="table_area list_top">
				<table>
					<caption>사업자정보</caption>
					<colgroup>
						<col style="width:200px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>회사명</th>
							<td>
								<c:out value="${member.coName}" />
							</td>
						</tr>
						<tr>
							<th>사업자등록번호</th>
							<td>
								<c:out value="${member.coNoStr}" />
							</td>
						</tr>
						<tr>
							<th>대표자명</th>
							<td>
								<c:out value="${member.ceoName}" />
							</td>
						</tr>
						<tr>
							<th>법인등록번호</th>
							<td>
								<c:out value="${member.bubNoStr}" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="button_area">
				<a href="<c:url value="/mypage/edit" />" class="btn btn_style2">수정</a>
			</div>
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 