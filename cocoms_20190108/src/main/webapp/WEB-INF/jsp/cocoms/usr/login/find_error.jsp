<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="login" />
	<jsp:param name="nav1" value="로그인" />
	<jsp:param name="nav2" value="${title}" />
</jsp:include>

	<div class="container">
		<h2 class="tit_bar blue mt"><c:out value="${title}" /></h2>
		<div class="join_content">
			<div class="list_step_id">
				<!--  
				<ul>
					<li>내 명의로 가입된 휴대폰 인증</li>
					<li>내 정보에 등록된 휴대폰으로 찾기</li>
					<li>이메일로 찾기</li>
					<li>공인인증서 인증</li>
					<li>사업자등록번호로 찾기</li>
				</ul>
				-->
			</div>
			<!-- //list_step -->	
			<div class="id_result">
				<span class="tit"></span><strong class="">인증정보와 일치하는 아이디가 없습니다.</strong>
			</div>
			<!-- //table_area -->
			<div class="button_area">
				<a href="<c:url value="/main" />" class="btn btn_style1">홈</a>
				<a href="<c:url value="/login" />" class="btn btn_style2">로그인</a>
			</div>
		</div>
		<!-- //join_content -->	
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="login" />
	<jsp:param name="cert" value="true" />
</jsp:include>
