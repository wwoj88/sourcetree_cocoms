<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="신고/허가 내역" />
</jsp:include>
	
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li class="active"><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<%-- <li><a href="<c:url value="/qna" />">1:1문의 내역</a></li> --%>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage">
			<h2 class="tit_bar mt">신고/허가 내역</h2>
			<div style="position: relative; font-size: 18px; color: #4b4a4a; font-weight: 600; overflow: hidden; padding: 26px 0 12px;">
				신고/허가 사항
			</div>
			<div class="table_area list_top">
				<table>
					<caption>신고/허가 사항</caption>
					<colgroup>
						<col style="width:174px" />
						<col style="width:286px" />
						<col style="width:174px" />
						<col style="width:286px" />
					</colgroup>
					<tbody>
						<tr>
							<th>업체구분</th>
							<td>
								<c:if test="${data.CogubunCd eq '1'}">
									개인
								</c:if>
								<c:if test="${data.CogubunCd eq '2'}">
									법인
								</c:if>
							</td>
							<th>허가신고번호</th>
							<td>
								<c:out value="${data.appNoStr}" /><br>
								
								<%-- <c:if test="${!empty data.appNoStr}"> --%>
									<c:if test="${data.down3 == false and data.down1}">
										<a href="<c:url value="/report/docs/1" />" class="btn_style2 file_s ml">신고서</a>
									</c:if>
									<c:if test="${data.down3}">
										<a href="<c:url value="/report/docs/3" />" class="btn_style2 file_s ml">변경 신고서</a>
									</c:if>
									<c:if test="${data.down4 == false and data.down2}">
										<a href="<c:url value="/report/docs/2" />" class="btn_style2 file_s">신고증</a>
									</c:if>
									<c:if test="${data.down4}">
										<a href="<c:url value="/report/docs/4" />" class="btn_style2 file_s">변경 신고증</a>
									</c:if>
				
							</td>
						</tr>
						<tr>
							<th>허가신고일</th>
							<td><c:out value="${data.appRegDateStr}" /></td>
							<th>허가/신고상태</th>
							<td><c:out value="${data.statCdStr}" /></td>
						</tr>
						<tr>
							<th>취급업무내용</th>
							<td colspan="3"><c:if test="${data.conditionCd eq '1'}">저작권 대리중개업 </c:if>
											<c:if test="${data.conditionCd eq '2'}">저작권 신탁관리업 </c:if>
											<c:if test="${empty data.conditionCd }"> </c:if>
											</td>
						</tr>
						<tr>
							<th>저작물종류/권리</th>
							<td colspan="3">
								<c:forEach var="item" items="${writingKindList}" varStatus="status">
									<c:set var="virgin" value="true" />
									<ul><li>
										<strong>${item.writingKindStr} : </strong>
										<c:forEach var="item2" items="${permKindList}" varStatus="status">
											<c:if test="${item2.writingKind eq item.writingKind}">
												<c:if test="${virgin eq false}">, </c:if>${item2.permKindStr}
												<c:set var="virgin" value="false" />
											</c:if>
										</c:forEach>
									</li></ul>
								</c:forEach>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div style="position: relative; font-size: 18px; color: #4b4a4a; font-weight: 600; overflow: hidden; padding: 26px 0 12px;">
				업체정보
			</div>
			<div class="table_area list_top">
				<table>
					<caption>업체정보</caption>
					<colgroup>
						<col style="width:174px" />
						<col style="width:286px" />
						<col style="width:174px" />
						<col style="width:286px" />
					</colgroup>
					<tbody>
						<tr>
							<th>업체명</th>
							<td><c:out value="${member.coName}" /></td>
							<th>홈페이지 </th>
							<td><c:out value="${member.trustUrl}" /></td>
						</tr>
						<tr>
							<th>사업자등록번호</th>
							<td><c:out value="${member.coNoStr}" /></td>
							<th>법인등록번호</th>
							<td><c:out value="${member.bubNoStr}" /></td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td><c:out value="${member.trustTel}" /></td>
							<th>팩스</th>
							<td><c:out value="${member.trustFax}" /></td>
						</tr>
						<tr>
							<th>주소</th>
							<td colspan="3">
								<c:out value="${member.trustZipcode}" /><br>
								<c:out value="${member.trustSido}" />
								<c:out value="${member.trustGugun}" />
								<c:out value="${member.trustDong}" />
								<c:out value="${member.trustBunji}" />
								<c:out value="${member.trustDetailAddr}" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div style="position: relative; font-size: 18px; color: #4b4a4a; font-weight: 600; overflow: hidden; padding: 26px 0 12px;">
				대표자정보
			</div>
			<div class="table_area list_top">
				<table>
					<caption>대표자정보</caption>
					<colgroup>
						<col style="width:174px" />
						<col style="width:286px" />
						<col style="width:174px" />
						<col style="width:286px" />
					</colgroup>
					<tbody>
						<tr>
							<th>대표자명</th>
							<td><c:out value="${member.ceoName}" /></td>
							<th>주민등록번호</th>
							<td><c:out value="${member.ceoRegNo}" /> - ******* </td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td><c:out value="${member.ceoTel}" /></td>
							<th>팩스</th>
							<td><c:out value="${member.ceoFax}" /></td>
						</tr>
						<tr>
							<th>휴대전화번호</th>
							<td><c:out value="${member.ceoMobile}" /></td>
							<th>이메일</th>
							<td><c:out value="${member.ceoEmail}" /></td>
						</tr>
						<tr>
							<th>주소</th>
							<td colspan="3">
								<c:out value="${member.ceoZipcode}" /><br>
								<c:out value="${member.ceoSido}" />
								<c:out value="${member.ceoGugun}" />
								<c:out value="${member.ceoDong}" />
								<c:out value="${member.ceoBunji}" />
								<c:out value="${member.ceoDetailAddr}" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="button_area">
				<c:if test="${data.conditionCd eq '1'}">
					<a href="<c:url value="/appl/sub/edit" />" class="btn btn_style2">변경신청</a>
				</c:if>
				<c:if test="${data.conditionCd eq '2'}">
					<a href="<c:url value="/appl/trust/edit" />" class="btn btn_style2">변경신청</a>
				</c:if>
			</div>
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 