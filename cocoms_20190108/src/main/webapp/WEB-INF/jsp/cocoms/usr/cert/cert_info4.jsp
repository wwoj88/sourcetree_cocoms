<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">
</head>
<body style=“overflow-X:hidden”>

<div style="overflow-y:scroll;" class="pop_common">
	<div class="inner cert4">
		<h2 class="tit txt_c">인터넷 증명발급 서비스</h2>
		<div class="cont">
			<div class="list_step_id">
				<ul>
					<li><a href="<c:url value="/cert/info1" />">위	&#183;변조 방지란?</a></li>
					<li><a href="<c:url value="/cert/info2" />">위&#183;변조 방지 적용기법</a></li>
					<li><a href="<c:url value="/cert/info3" />">위&#183;변조 검증방법</a></li>
					<li class="active"><a href="<c:url value="/cert/info4" />">증명서 출력 FAQ</a></li>
				</ul>
			</div>
			<!-- //list_step -->
			
			<div class="txt_area">
				<p class="txt_q">증명서 인쇄 화면을 어떻게 닫습니까?</p>
				<p class="txt_a">증명서 발급 프로그램의 도구메뉴중 나가기 버튼을 클릭하시면 됩니다. 발급 프로그램의 오류로 인해 나가기
버튼을 누를 수 없다면, <strong class="txt_blue">“Ctrl+Alt+Del”</strong> 키를 눌러 Windows 작업 관리자 창을 열고 <strong class="txt_blue">“인터넷 증명발급 서비스”</strong>
창을 선택하여 작업끝내기 버튼을 누르시면 발급 프로그램이 종료되면서 정상적인 동작을 취하실 수 있습니다.</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert4_1.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_q">마우스가 브라우저 밖으로 나가지 않습니다.</p>
				<p class="txt_a">인터넷 발급문서의 경우 보안상 마우스 및 키보드 제어를 위해 문서발급이 진행되는 동안 키보드와 마우스의 
동작에 제한을 두고 있습니다. 인쇄과정이 끝나면 발급화면이 닫히면서 정상적인 키보드/마우스 동작이 복구됩
니다. </p>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="txt_q">증명서 출력후 웹브라우저가 모두 닫힙니다.</p>
				<p class="txt_a">
				해당 PC의 윈도우에서 사용자 인터넷 사용을 체크하는 악성툴바가 설치된 경우입니다.
사용자의 인터넷 사용을 체크하지 않는 툴바 (야후/구글/MSN 툴바등)는 이러한 현상을 유발시키지 않습니다.
인터넷 익스플로러용 툴바의 기능을 제거하시려면 <strong class="txt_blue">Internet Explorer 추가 기능을 관리하는 방법</strong>을 읽어 보시는
것이 좋습니다. 악성 툴바와 관련된 Internet Explorer 추가 기능을 사용중지 시킨 후, 증명서 발급서비스를 이용
하시면 웹브라우저가 종료되는 현상이 제거됩니다. 
				</p>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="txt_q">‘출력’버튼이 활성화되지 않습니다.</p>
				<p class="txt_a">인터넷 발급문서의 출력은 보안상 가상 프린터나 파일로 출력할 수 없도록 설정되어 있습니다. 또한 발급기관의
인터넷 발급문서의 보안정책에 의해 네트워크 공유 프린터를 사용하여 출력할 수 없도록 되어 있습니다. PC상에
직접 연결된 로컬 프린터와 TCP/IP 네트워크 프린터를 이용하여 인터넷 증명발급 서비스를 출력하실 수 있습니다. </p>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="txt_q">증명서를 발급하려는데 스크립트 오류가 발생합니다.</p>
				<p class="txt_a">증명서 발급시 문서 위&#183;변조 방지 프로그램이 자동설치되도록 되어 있으나, 사용자 PC의 설정 (예, 바이러스 백신
의 실시간 감시기능, 보안설정, 인터넷 연결상태 등)의 영향으로 자동설치되지 않아 발생하는 현상입니다.
(MS Active X의 전반적인 오류현상)<br><br>

<span class="txt_blue">‘수동설치 프로그램’</span> (용량 : 4.09MB) 을 설치하시고 다시 출력해보십시오.<br>
수동설치 시에는 모든 인터넷 창을 닫고 실행하셔야 합니다.
</p>
				<a href="" class="btn_style1">다운로드</a>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="txt_q">증명서 발급 프로그램을 삭제하려고 합니다.</p>
				<p class="txt_a">

				수동제거 프로그램을 이용하여 제거하실 수 있습니다.<br>
아래 프로그램을 다운받고 설치 후 바탕화면에 있는 아이콘 <strong class="txt_blue">“PeportExpree 모듈 제거”</strong>을 실행하십시오.<br>
<strong class="txt_blue">“PeportExpree  Enterprise” </strong>선택하신 후 <strong class="txt_blue">“시작”</strong>버튼을 클릭하시면 모든 증명서 발급 모듈이 삭제됩니다.<br><br>

만약 문서 위&#183;변조 방지 프로그램이 제대로 설치가 되지 않는다면 <strong class="txt_blue">‘수동제거 프로그램’</strong> (용량 : 73.5KB) 을 설치
하여 제거하신 후 다시 출력해보십시오.<br>
수동제거 및 설치 시에는 모든 인터넷 창을 닫고 실행하셔야 합니다. 
</p>
				<a href="" class="btn_style1">다운로드</a>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="txt_q">‘인터넷 증명발급은 보안상 화면 캡처를 방지하고 있습니다. 화면 캡처 프로그램을 종료한 후 다시 실행
하십시오.’라는 메시지가 나옵니다.</p>
				<p class="txt_a">인터넷 증명서는 위&#183;변조 방지를 위해 화면 캡처를 할 수 없도록 되어 있습니다. 그런데 고객님의 PC에 화면 캡처
프로그램 혹은 원격제어 프로그램이 실행 중이기 때문에 이러한 메시지가 나온 것입니다. 화면 캡처 프로그램 
혹은 원격제어 프로그램을 종료하시기 바랍니다.</p>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="txt_q">‘인터넷 증명발급은 보안상 가상 OS 동작이 되는 동안에는 동작되지 않습니다.’라는 메시지가 나옵니다.</p>
				<p class="txt_a">
				인터넷 증명서는 위&#183;변조 방지를 위해 고객님의 PC상에 가상 OS 프로그램이 동작하거나 관련 프로세스가 동작
중에는 실행되지 않도록 되어 있습니다. 현재 고객님의 PC상에 Vmware나 Vitual PC 같은 가상 OS 프로그램이
설치되어 동작되고 있다면, 관련 서비스를 잠시 중단시키시고 다시 <strong class="txt_blue">‘인터넷 증명발급 서비스’</strong> 를 이용하시기 
바랍니다.
				</p>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="txt_q">‘현재 보안설정으로는 Active X 컨트롤을 실행할 수 없습니다.’는 메시지가 나옵니다. </p>
				<p class="txt_a">
				인터넷 익스플로러 보안설정이 높게 설정되어 있어 위&#183;변조 방지 프로그램을 설치할 수 없는 경우입니다.
아래와 같이 익스플로러의 <strong class="txt_blue">[도구] - [인터넷 옵션]</strong>에서 보안수준을 변경하시고 다시 실행하십시오.
그래도 계속 위의 메시지가 나오는 경우 <strong class="txt_blue">‘수동설치 프로그램’</strong> (용량 : 4.09MB) 을 설치하시기 바랍니다.
				</p>
			</div>
			<!-- //txt_area -->
			<div class="img" style="margin-left:-18px"><img src="/cocoms/img/img_cert4_2.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_q">복사방지마크에 ‘원’ 또는 ‘본’자를 식별할 수 없습니다. </p>
				<p class="txt_a">
				프린터의 토너 상태 혹은 잉크의 잔량이 부족한 경우 복사방지마크의 ‘원본’이 정확하게 나오지 않을 수 있습니다.
즉, 사용하고 계시는 프린터의 토너를 새로 교체한 경우 ‘원’자가 진하게 나오는 현상이 나타날 수 있으며, 프린터
토너의 잔량이 부족한 경우 ‘본’자가 안나오는 현상이 나타날 수 있습니다. 이런 경우, 실제 출력된 증명서의 진본
여부를 눈으로 확인할 수 없게 되므로 다른 프린터를 사용하여 다시 출력받으시기 바랍니다. 만약 프린터의 토너
나 잉크의 상태에 문제가 없는 경우에도 계속 ‘원본’ 글자가 정확하게 출력되지 않는다면, 해당 프린터의 제조사와
모델명을 <strong>(주)캡소프트 (0505-998-1888)</strong>로 알려주시면 향후 서비스에 반영될 수 있도록 하겠습니다.
				</p>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="txt_q">문서 출력이 느리고 다른 작업을 할 수 없습니다. </p>
				<p class="txt_a">
문서 위・변조 방지 솔루션은 스풀 파일을 보안정책 상의 이유로 허용하지 않습니다.<br>
인터넷 발급문서는 프린터 스풀러를 사용하지 않고 직접 프린터로 출력되기 때문에 사용하시는 프린터에 따라
서는 (GDI 방식 또는 호스트 기반의 프린터) 시스템의 자원을 문서를 출력하는 동안 점유하게 되며, 이 경우 고
객님의 PC가 전반적으로 느려지는 현상이 나타날 수 있습니다. 그러나 인쇄가 종료되면 PC속도는 원상태로 복구
되니 증명서 출력과정이 지루하더라도 잠시만 기다려 주십시오.
				</p>
			</div>
			<!-- //txt_area -->
			<div class="img" style="margin-left:-18px"><img src="/cocoms/img/img_cert4_3.png" alt="" /></div>
		</div>
	</div>
</div>
<!-- //pop_common -->

<script src="<c:url value='/js/jquery-1.10.2.min.js' />"></script>
<script src="<c:url value='/js/jquery.bxslider.js' />"></script>
<script src="<c:url value='/js/script.js' />"></script>

<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script src="<c:url value='/js/usr/cert.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</body>
</html>
