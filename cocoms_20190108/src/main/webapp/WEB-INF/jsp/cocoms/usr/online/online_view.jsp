<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="education" />
	<jsp:param name="nav1" value="교육정보" />
	<jsp:param name="nav2" value="온라인" />
</jsp:include>
	
	<div class="container">
		<h2 class="tit_bar pink mt">온라인</h2>
		<div class="table_area view">
			<table>
				<caption>온라인</caption>
				<thead>
					<tr> 	 	 	
						<th class="subject">
							<c:out value="${data.title}" />
							<div class="date">
								<c:out value="${data.regName}" />  <i>|</i>  <c:out value="${data.regDateStr}" />
							</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="cont">
						<pre style="word-wrap: break-word;white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-break:break-all;"><c:out value="${data.content2}"  escapeXml="false"/></pre>
							
						</td>
					</tr>
					<tr>
						<td>
							<c:forEach var="item" items="${data.boardFileDataList}" varStatus="status">
								<a href="<c:url value="/download?filename=${item.maskName}&boardSeqNo=${item.boardSeqNo}" />" class="link_board" target="_blank"><i class="fileDown board">파일 다운로드</i> <c:out value="${item.filename}" /><br></a>
							</c:forEach>
							<c:if test="${fn:length(data.boardFileDataList) eq '0'}">
								-
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->

		<div class="button_area">
			<a href="<c:url value="/online?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />" class="btn btn_style2">목록</a>
		</div>
		
	</div>
	<!-- //container -->
		
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="notice" />
</jsp:include>
 