<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="type" value="report" />
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="신탁단체" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${memberSeqNo}" />
	
	<div id="print_area" class="container">

		<h2 class="tit_bar yellow mt"><c:out value="${year}" />년 <c:out value="${member.coName}" /> 업무보고 정보</h2>
	
		<div class="tab_result yellow">
			<ul>
				<li class="active"><a href="<c:url value="/reports/trust/years/${year}" />">실적정보</a></li>
				<li><a href="<c:url value="/reports/trust/years/${year}/tab2" />">저작권료 징수분배 내역</a></li>
				<li><a href="<c:url value="/reports/trust/years/${year}/tab3" />">사업계획</a></li>
			</ul>
		</div>
		<!-- //step_sub -->
		
		<h2 class="tit_c_s">1. 현황 </h2>
		<div class="table_area list list_type1">
			<table>
				<caption> 매출액 목록</caption>
				<colgroup>
					<col style="width:50%" />
					<col style="width:50%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>회원</th>
						<th>관리 저작물수</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<c:out value="${report.thisMember}" /> 명 / 
							<c:out value="${report.thisGroup}" /> 단체
						</td>
						<td>
							<fmt:formatNumber value="${report.thisWorknum}" type="number" /> 건
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_box yellow mt">결산 및 예산</h2>
		<div class="table_area list list_type2 tbl_style_b">
			<table>
				<caption>결산 및 예산</caption>
				<colgroup>
					<col style="width:301px" />
					<col style="width:auto" />
					<col style="width:450px" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>구분</th>
						<th><c:out value="${lastYear+1}" /> 결산 <span>(단위 : 천원)</span></th>
						<th><c:out value="${year+1}" /> 예산 <span>(단위 : 천원)</span></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="bg_total">신탁회계</td>
						<td><fmt:formatNumber value="${report.lastTrustAccnt}" type="number" /></td>
						<td><fmt:formatNumber value="${report.thisTrustAccnt}" type="number" /></td>
					</tr>
					<tr>
						<td class="bg_total">보상금 회계</td>
						<td><fmt:formatNumber value="${report.lastIndemnityAccnt}" type="number" /></td>
						<td><fmt:formatNumber value="${report.thisIndemnityAccnt}" type="number" /></td>
					</tr>
					<tr>
						<td class="bg_total">일반회계</td>
						<td><fmt:formatNumber value="${report.lastGeneralAccnt}" type="number" /></td>
						<td><fmt:formatNumber value="${report.thisGeneralAccnt}" type="number" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_c_s">2. 전년도 실적<span>(<c:out value="${year}" />년)</span><p class="txt_red">※ <strong>인원 </strong>: 신규입회 회원 수 기재 / <strong>저작물 수</strong> : 신규등록 된 저작물 수, 전년도 신탁받은 관리저작물 목록을 업로드 해주시기 바랍니다. </p></h2>
		<div class="table_area list list_type1">
			<table>
				<caption>전년도 실적</caption>
				<colgroup>
					<col style="width:50%" />
					<col style="width:50%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>회원</th>
						<th>관리 저작물수</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<c:out value="${report.lastMember}" /> 명 / 
							<c:out value="${report.lastGroup}" /> 단체
						</td>
						<td>
							<fmt:formatNumber value="${report.lastWorknum}" type="number" /> 건
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_c_s">3. 첨부자료<span>(<c:out value="${year}" />년)</span></h2>
		<div class="list_finl_link bg">
			<ul>
				<li>
					<p>저작물내역</p>
					<c:if test="${!empty report.filename1}">
						<a href="<c:url value="/download?filename=${report.filepath1}" />"><c:out value="${report.filename1}" /></a>
					</c:if>
					<c:if test="${empty report.filename1}">
						(첨부자료없음)
					</c:if>
				</li>
				<li>
					<p>전년도 사업실적서</p>
					<c:if test="${!empty report.filename2}">
						<a href="<c:url value="/download?filename=${report.filepath2}" />"><c:out value="${report.filename2}" /></a>
					</c:if>
					<c:if test="${empty report.filename2}">
						(첨부자료없음)
					</c:if>
				</li>
				<li>
					<p>결산서</p>
					<c:if test="${!empty report.filename3}">
						<a href="<c:url value="/download?filename=${report.filepath3}" />"><c:out value="${report.filename3}" /></a>
					</c:if>
					<c:if test="${empty report.filename3}">
						(첨부자료없음)
					</c:if>
				</li>
				<li>
					<p>감사의 감사보고서</p>
					<c:if test="${!empty report.filename4}">
						<a href="<c:url value="/download?filename=${report.filepath4}" />"><c:out value="${report.filename4}" /></a>
					</c:if>
					<c:if test="${empty report.filename4}">
						(첨부자료없음)
					</c:if>
				</li>
			</ul>
		</div>
		<!-- //list_finl_link -->
		
		<div class="button_area">
			<a href="<c:url value="/reports/trust/years/${year}" />/edit" class="btn btn_style2">수정</a>
			<div class="right">
				<a href="<c:url value="/reports/trust/years/${year}/save" />" class="btn btn_style2" target="_blank">엑셀출력</a>
				<a href="" id="print" class="btn btn_style2">인쇄</a>
				<a href="<c:url value="/reports/trust" />" class="btn btn_style1">목록</a>
			</div>
		</div>
		
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="js" value="report" />
</jsp:include>
