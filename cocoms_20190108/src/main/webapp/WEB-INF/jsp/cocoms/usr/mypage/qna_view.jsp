<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="1:1문의내역" />
</jsp:include>
	
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<li class="active"><a href="<c:url value="/qna" />">1:1문의 내역</a></li>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage mypage_2">
			<h2 class="tit_bar mt">1:1문의내역</h2>
			<div class="table_area view">
				<table>
					<caption>공지사항</caption>
					<thead>
						<tr> 	 	 	
							<th class="subject">
								<c:out value="${data.title}" />
								<div class="date">
									<c:out value="${data.regDateStr}" />
								</div>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cont">
								${data.content2}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			
			<c:forEach var="item" items="${list}" varStatus="status">
				<div class="table_area view">
					<table>
						<caption>공지사항</caption>
						<thead>
							<tr> 	 	 	
								<th class="subject re">
									<strong class="txt_re">Re.</strong> 
									<c:out value="${item.title}" />
									<div class="date">
										<c:out value="${item.regDateStr}" />
									</div>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="cont">
									${item.content2}
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
			</c:forEach>
			
			<div class="button_area">
				<a href="" id="remove2" class="btn btn_style2">삭제</a>
				<a href="<c:url value="/qna" />" class="btn btn_style1">목록</a>
			</div>
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 