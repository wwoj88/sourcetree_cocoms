<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="notice" />
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="공지사항" />
</jsp:include>
	
	<div class="container">
		<h2 class="tit_bar pink mt">공지사항</h2>
		<div class="table_area view">
			<table>
				<caption>공지사항</caption>
				<thead>
					<tr> 	 	 	
						<th class="subject">
							<c:out value="${data.title}" />
							<div class="date">
								<c:out value="${data.regName}" />  <i>|</i>  <c:out value="${data.regDateStr}" />
							</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="cont">
						<pre><c:out value="${data.content2}"  escapeXml="false"/></pre>
							
						</td>
					</tr>
					<tr>
						<td>
							<c:forEach var="item" items="${data.boardFileDataList}" varStatus="status">
								<a href="<c:url value="/download?filename=${item.maskName}" />" class="link_board" target="_blank"><i class="fileDown board">파일 다운로드</i> <c:out value="${item.filename}" /><br></a>
							</c:forEach>
							<c:if test="${fn:length(data.boardFileDataList) eq '0'}">
								-
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->

		<div class="button_area">
			<a href="<c:url value="/notice?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />" class="btn btn_style2">목록</a>
		</div>
		
	</div>
	<!-- //container -->
		
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="notice" />
</jsp:include>
 