<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="type" value="report" />
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="대리중개업체" />
</jsp:include>

<style>
#amt input {width: 50px;} 
</style>

	<div class="container">
	
		<form id="form" method="post" action="<c:url value="/reports/sub/years/${year}" />">
		<c:forEach var="item" items="${list}" varStatus="status">
			<input type="hidden" name="writingKind" value="${item.writingKind}" />
			<input type="hidden" name="permKind" value=0 />
		</c:forEach>
		
		<input type="hidden" name="actrstMaskName" value="${report.actrstMaskName}" />
		<input type="hidden" name="planMaskName" value="${report.planMaskName}" />
		
		<h2 class="tit_bar yellow mt"><c:out value="${year}" />년 <c:out value="${member.coName}" /> 업무보고 정보</h2>
		<div class="table_area list_top">
			<table>
				<caption> 대리중개업체</caption>
				<colgroup>
							<col style="width:125px" />
							<col style="width:395px" />
							<col style="width:125px" />
							<col style="width:395px" />
				</colgroup>
				<tbody>
					<tr>
						<th>작성자 <strong class="txt_point">*</strong></th>
						<td>
							<input type="text" name="regName" value="${report.regName}" />
						</td>
						<th>직위</th>
						<td>
							<input type="text" name="regPosi" value="${report.regPosi}" />
						</td>
					</tr>
					<tr>
						<th>전화번호 <strong class="txt_point">*</strong></th>
						<td>
							<input type="text" name="regTel" value="${report.regTel}" />
						</td>
						<th>이메일 <strong class="txt_point">*</strong></th>
						<td>
							<input type="text" name="regEmail" value="${report.regEmail}" />
						</td>
					</tr>
					<tr>
						<th>직원수</th>
						<td>
							 <input type="text" name="staffNum" value="${report.staffNum}" />명
						</td>
						<th>상장여부</th>
						<td>
							<c:if test="${report.listingCd eq '1'}">
								<c:set var="listingCd1" value="checked" />
							</c:if>
							<c:if test="${report.listingCd eq '2'}">
								<c:set var="listingCd2" value="checked" />
							</c:if>
							<c:if test="${report.listingCd eq '3'}">
								<c:set var="listingCd3" value="checked" />
							</c:if>
							<input type="radio" name="listingCdStr" value="1" ${listingCd1} /> 코스닥
							<input type="radio" name="listingCdStr" value="2" ${listingCd2} /> 코스피
							<input type="radio" name="listingCdStr" value="3" ${listingCd3} /> 비상장
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_bar yellow mt" ><c:out value="${year}" />년 실적보고 상세</h2>

		<h2 class="tit_box yellow">저작물수</h2>
		<div class="table_area list list_type2">
			<table id="count" summary="저작물수">
				<caption>저작물수</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>종류 / 권리종류</th>
						<th>국내<a href="<c:url value="/download2?filename=9.zip" />" class="btn btn_style1">양식 다운로드</a></th>
						<th>해외<a href="<c:url value="/download2?filename=10.zip" />" class="btn btn_style1">양식 다운로드</a></th>
						<th>소계</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
					 
						<tr>
							<td><c:out value="${item.writingKindDesc}" /> <c:if test="${item.permKindDesc ne '기타' and  !empty item.permKindDesc}">/ <c:out value="${item.permKindDesc}" /></c:if></td>
							<td><input type="text"  id="inWorkNum${status.count}" name="inWorkNum" value="${item.inWorkNum}" /></td>
							<td><input type="text" id="outWorkNum${status.count}" name="outWorkNum" value="${item.outWorkNum}" /></td>
							<td><input type="text" id="workSum${status.count}" name="workSum" value="${item.workSum}" readonly/></td>
						</tr>
					</c:forEach>
					<tr class="bg_total">
						<td>합계</td>
						<td><input type="text" id="inWorkNum" value="${sum.inWorkNum}" readonly /></td>
						<td><input type="text" id="outWorkNum" value="${sum.outWorkNum}" readonly /></td>
						<td><input type="text" id="workSum" value="${sum.workSum}" readonly /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->

		<h2 class="tit_box yellow mt ">사용료 및 수수료<p3 style="color:red">(단위는 천원입니다.)</p3></h2>
		<div class="table_area list list_type2 tbl_style_b">
			<table id="amt" summary="사용료 및 수수료">
				<caption> 사용료 및 수수료</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
					<col style="width:8.333333333333333%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>종류 / 권리종류</th>
						<th colspan="3">사용료 <span style="color:red">(단위 : 천원)</span></th>
						<th colspan="3">수수료 <span style="color:red">(단위 : 천원)</span></th>
						<th colspan="3">수수료율 <span>(단위 : %)</span></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>-</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
						<td>국내</td>
						<td>해외</td>
						<td>소계</td>
					</tr>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><c:out value="${item.writingKindDesc}" /> <c:if test="${item.permKindDesc ne '기타' and  !empty item.permKindDesc}">/ <c:out value="${item.permKindDesc}" /></c:if></td>
							<td><input type="text" id="inRental${status.count}"  name="inRental" value="${item.inRental}" /></td>
							<td><input type="text" id="outRental${status.count}" name="outRental" value="${item.outRental}" /></td>
							<td><input type="text" id="rentalSum${status.count}" name="rentalSum" value="${item.rentalSum}" readonly/></td>
							<td><input type="text" id="inChrg${status.count}" name="inChrg" value="${item.inChrg}" /></td>
							<td><input type="text" id="outChrg${status.count}" name="outChrg" value="${item.outChrg}" /></td>
							<td><input type="text" id="chrgSum${status.count}" name="chrgSum" value="${item.chrgSum}" readonly/></td>
							<td><input type="text" id="inChrgRate${status.count}" name="inChrgRate" value="${item.inChrgRate}" /></td>
							<td><input type="text" id="outChrgRate${status.count}" name="outChrgRate" value="${item.outChrgRate}" /></td>
							<td><input type="text" id="avgChrgRate${status.count}" name="avgChrgRate" value="${item.avgChrgRate}" readonly/></td>
						</tr>
					</c:forEach>
					<tr class="bg_total">
						<td>합계</td>
						<td><input type="text" id="inRental" value="${sum.inRental}" readonly /></td>
						<td><input type="text" id="outRental" value="${sum.outRental}" readonly /></td>
						<td><input type="text" id="rentalSum" value="${sum.rentalSum}" readonly /></td>
						<td><input type="text" id="inChrg" value="${sum.inChrg}" readonly /></td>
						<td><input type="text" id="outChrg" value="${sum.outChrg}" readonly /></td>
						<td><input type="text" id="chrgSum" value="${sum.chrgSum}" readonly /></td>
						<td><input type="text" id="inChrgRate" value="${sum.inChrgRate}" readonly /></td>
						<td><input type="text" id="outChrgRate" value="${sum.outChrgRate}" readonly /></td>
						<td><input type="text" id="avgChrgRate" value="${sum.avgChrgRate}" readonly /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
				
		<h2 class="tit_box yellow mt">첨부파일</h2>
		<div class="list_finl_link">
			<ul>
				<li>
					국내 관리저작물 목록
					<div class="list_file">
						<input type="text" name="actrstFileName" class="input_style1 prj_file" value="${report.actrstFileName}" placeholder="" readonly />
						<input type="file" name="file1" class="insert_file_target" data-ref-name="actrstFileName" data-ref-name2="actrstMaskName" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file1" class="form_btn insert_file btn_style1" data-ref-name="actrstFileName" data-ref-name2="actrstMaskName">파일삭제</button>
					</div>
				</li>
				<li>
					해외 관리저작물 목록
					<div class="list_file">
						<input type="text" name="planFileName" class="input_style1 prj_file" value="${report.planFileName}" placeholder="" readonly />
						<input type="file" name="file2" class="insert_file_target" data-ref-name="planFileName" data-ref-name2="planMaskName" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file2" class="form_btn insert_file btn_style1" data-ref-name="planFileName" data-ref-name2="planMaskName">파일삭제</button>
					</div>
				</li>
			</ul>
		</div>
		<!-- //list_finl_link -->
		
		<h2 class="tit_box yellow mt">특이사항</h2>
		<textarea name="memo" class="input_style1 height_bottom_n" ><c:out value="${report.memo}" /></textarea>
		<div class="button_area center">
			<a href="" id="save" class="btn btn_style2">등록</a>
			<div class="right">
				<a href="<c:url value="/reports/sub" />" class="btn btn_style1">목록</a>
			</div>
		</div>

		</form>

	</div>
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="js" value="report" />
</jsp:include>
