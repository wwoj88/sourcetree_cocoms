<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="신고/허가 내역" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />

	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li class="active"><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<%-- <li><a href="<c:url value="/qna" />">1:1문의 내역</a></li> --%>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage">
			<h2 class="tit_bar mt">신고/허가 내역</h2>
			
			<div id="appl_content" class="cont" data-doc-type="4">
				<div class="table_area">
					<table>
						<caption>저작권대리중개업</caption>
						<thead>
							<tr> 	 	 	
								<th>저작권대리중개업 신고증</th>
							</tr>
						</thead>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="declaration_preview">
					<div class="inner">
						<div class="preview">
							<p class="num_top"><c:out value="${data.appNoStr}" /></p>
							<h1 class="tit">저작권대리중개업  변경신고증</h1>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:185px" />
										<col style="width:236px" />
										<col style="width:173px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr class="height">
											<th>법인<span>(또는 단체)</span>명</th>
											<td><c:out value="${data.coName}" /></td>
											<th>법인/사업자등록번호</th>
											<td>
												<c:if test="${!empty data.bubNo}"><c:out value="${data.bubNoStr}" /></c:if>
												<c:if test="${empty data.bubNo}"><c:out value="${data.coNoStr}" /></c:if>
																							
											</td>
										</tr>
										<tr>
											<th>대표자</th>
											<td><c:out value="${data.ceoName}" /></td>
											<th>전화번호</th>
											<td><c:out value="${data.trustTel}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td class="txt_l">
												<c:if test="${!empty data.trustZipcode}">(<c:out value="${data.trustZipcode}" />)</c:if>
												<c:out value="${data.trustSido}" />
												<c:out value="${data.trustGugun}" />
												<c:out value="${data.trustDong}" />
												<c:out value="${data.trustBunji}" />
												<c:out value="${data.trustDetailAddr}" />
											</td>
											<th>팩스번호</th>
											<td><c:out value="${data.trustFax}" /></td>
										</tr>
										<tr>
											<th>취급하고자 하는 <br>저작물 등의 종류</th>
											<td colspan="3" class="txt_l">
												<c:out value="${writingKind}" />
											</td>
										</tr>
										<tr>
											<th>취급하고자 하는 권리</th>
											<td colspan="3" class="txt_l">
												<c:out value="${permKind}" />
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<p class="txt1"><strong>[저작권법]</strong> 제 105조 제 1항 규정에 따라 저작권대리중개업의 신고를 하였음을 증명합니다.</p>
							<div class="sign">
								<p><c:out value="${appRegDate}" /></p>
							</div>
							<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
							<div class="table_preview">
								<table>
									<caption></caption>
									<tbody>
										<tr>
											<th>내부결제 첨부파일</th>
										</tr>
										<c:forEach var="item" items="${rpMgmList}" varStatus="status">
											<tr>
												<td>
													<p><a href="<c:url value="/download?filename=${item.interFilepath}" />"><c:out value="${item.interFilename}" /></a></p>
												</td>
											</tr>
										</c:forEach>
										<c:if test="${fn:length(rpMgmList) eq '0'}">
											<tr>
												<td>
													<p class="txt_nofile">내부결제 첨부파일이 없습니다.</p>
												</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<h2 class="tit">신고증 기재사항 변경 내역</h2>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:191px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
									<c:forEach var ="item" items="${chgList}" varStatus="status">
										<tr>
									   		<td>
									   		${item.strPre1}
											</td>
											<td>
												<c:if test="${!empty item.strPre2}">${item.strPre2}<br></c:if>
												<c:if test="${!empty item.strPre3}">${item.strPre3}<br></c:if>
												<c:if test="${!empty item.strPre4}">${item.strPre4}<br></c:if>
												<c:if test="${!empty item.strPre5}">${item.strPre5}<br></c:if>
												<c:if test="${!empty item.strPre6}">${item.strPre6}<br></c:if>
												<c:if test="${!empty item.strPre7}">${item.strPre7}<br></c:if>
												<c:if test="${!empty item.strPre8}">${item.strPre8}<br></c:if>
												<c:if test="${!empty item.strPre9}">${item.strPre9}<br></c:if>
												<c:if test="${!empty item.strPre10}">${item.strPre10}<br></c:if>
												<c:if test="${!empty item.strPre11}">${item.strPre11}<br></c:if>
												<c:if test="${!empty item.strPre12}">${item.strPre12}<br></c:if>
												<c:if test="${!empty item.strPre13}">${item.strPre13}<br></c:if>
												<c:if test="${!empty item.strPre14}">${item.strPre14}<br></c:if>
												<c:if test="${!empty item.strPre15}">${item.strPre15}<br></c:if>
											</td>
											<td>
												<c:if test="${!empty item.strPost2}">${item.strPost2}<br></c:if>
												<c:if test="${!empty item.strPost3}">${item.strPost3}<br></c:if>
												<c:if test="${!empty item.strPost4}">${item.strPost4}<br></c:if>
												<c:if test="${!empty item.strPost5}">${item.strPost5}<br></c:if>
												<c:if test="${!empty item.strPost6}">${item.strPost6}<br></c:if>
												<c:if test="${!empty item.strPost7}">${item.strPost7}<br></c:if>
												<c:if test="${!empty item.strPost8}">${item.strPost8}<br></c:if>
												<c:if test="${!empty item.strPost9}">${item.strPost9}<br></c:if>
												<c:if test="${!empty item.strPost10}">${item.strPost10}<br></c:if>
												<c:if test="${!empty item.strPost11}">${item.strPost11}<br></c:if>
												<c:if test="${!empty item.strPost12}">${item.strPost12}<br></c:if>
												<c:if test="${!empty item.strPost13}">${item.strPost13}<br></c:if>
												<c:if test="${!empty item.strPost14}">${item.strPost14}<br></c:if>
												<c:if test="${!empty item.strPost15}">${item.strPost15}<br></c:if>
											</td>
										</tr>
										</c:forEach>									
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
						</div>
					</div>
					<!-- //preview -->
				</div>
				<!-- //declaration_preview -->
			</div>
			<!-- //cont -->
			
			<div class="right">
				<!-- 
				<a href="" class="btn_style2">PDF저장</a>
				 -->
				<a href="" id="print_cert" class="btn_style2">인쇄</a>
				<a href="<c:url value="/report" />" class="btn_style1">이전</a>
			</div>
		</div>
	</div>		

<input type="hidden" id="rpx" value="contract_declaration_m_new" />	
<textarea id="xmldata" style="width: 100%; height: 500px; display: none;">
	<root>
		<title>저작권대리중개업 신고증</title>                                             
		<report_no><c:out value="${data.appNoStr}"/></report_no>   
		<company_info>                                                     
			<company><![CDATA[${data.coName}]]></company>                 
			<regno><c:if test="${!empty data.bubNo}"><c:out value="${data.bubNoStr}" /></c:if><c:if test="${empty data.bubNo}"><c:out value="${data.coNoStr}" /></c:if></regno>                                          
			<boss_name><![CDATA[${data.ceoName}]]></boss_name>            
			<tel><![CDATA[${data.trustTel}]]></tel>                      
			<fax><![CDATA[${data.trustFax}]]></fax>                      
			<address><![CDATA[${data.trustZipcode} ${data.trustSido} ${data.trustGugun} ${data.trustDong} ${data.trustBunji} ${data.trustDetailAddr}]]></address>
		</company_info>                         
		<works><![CDATA[${writingKind}]]></works>                          
		<rights><![CDATA[${permKind}]]></rights>
		
		<c:forEach var="item" items="${changeDateList}" varStatus="status">
			<change_date_${status.index}><c:out value="${item}" /></change_date_${status.index}> 
		</c:forEach>
		
		<c:forEach var="item" items="${contentsList}" varStatus="status">
			<contents_${status.index}><c:out value="${item}" /></contents_${status.index}>
		</c:forEach>		
		
		<prtdate><c:out value="${appRegDate}" /></prtdate>
	</root>
</textarea>
		
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 