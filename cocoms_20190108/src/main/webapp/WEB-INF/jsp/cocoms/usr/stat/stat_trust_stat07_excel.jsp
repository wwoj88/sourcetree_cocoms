<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% 
	response.setHeader("Content-Disposition","attachment;filename="+request.getAttribute("filename")); 
%>
<html>
<head>
<title>저작권 종류별 보상금 - 실연자 </title>
</head>
<body>

	<table cellpadding="0" cellspacing="1" border="1">
		<tr>
			<td colspan="8"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 저작권 종류별 보상금 - 실연자</td>
		</tr>
		<tr>
			<td colspan="8" align="right">(단위 건, 천원, %)</td>
		</tr>
		<tr>
			<td colspan="2" align="center">구분</td>
			<td align="center">이용단체</td>
			<td align="center">징수액(천원)</td>
			<td align="center">분배액(천원)</td>
			<td align="center">미분배액(천원)</td>
			<td align="center">수수료(천원)</td>
			<td align="center">수수료율(%)</td>
		</tr>
		<c:forEach var="item" items="${list}" varStatus="status">
			<tr align="center">
				<td></td>
				<td>
					<c:choose>
						<c:when test="${item.tit eq '1' or item.tit eq '6'  or item.tit eq '11'}">주실연자(가수)</c:when>
						<c:when test="${item.tit eq '2' or item.tit eq '7'  or item.tit eq '12'}">주실연자(연주)</c:when>
						<c:when test="${item.tit eq '3' or item.tit eq '8'  or item.tit eq '13'}">부실연자</c:when>
						<c:when test="${item.tit eq '4' or item.tit eq '9'  or item.tit eq '14'}">지휘자</c:when>
						<c:when test="${item.tit eq '5' or item.tit eq '10' or item.tit eq '15'}">소계</c:when>
					</c:choose>									
				</td>
				<td><fmt:formatNumber value="${item.organization}" type="number" /></td>
				<td><fmt:formatNumber value="${item.collected}" type="number" /></td>
				<td><fmt:formatNumber value="${item.dividend}" type="number" /></td>
				<td><fmt:formatNumber value="${item.dividendOff}" type="number" /></td>
				<td><fmt:formatNumber value="${item.chrg}" type="number" /></td>
				<td><fmt:formatNumber value="${item.chrgRate}" type="number" /></td>
			</tr>
		</c:forEach>
		<c:if test="${fn:length(list) eq '0'}">
			<tr><td align="center" colspan="8">등록된 보상금(실연자) 내용이 없습니다.</td></tr>
		</c:if>
	</table>

</body>
</html>