<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="intro" />
	<jsp:param name="nav1" value="개인정보처리방침" />
</jsp:include>
	
	<div class="container">

		<!-- //privacy_content -->	
		<div class="privacy_content">
			<h2 class="tit_bar blue">개인정보처리방침</h2>
			<div class="inner">

				문화체육관광부는 저작권 위탁업 관리시스템에서 서비스를 제공하기 위하여 아래와 같이 개인정보를 수집·이용하고자 합니다. 내용을 자세히 읽으신 후 동의 여부를 결정해 주세요.<br><br><br>

				<strong>1. 개인정보의 처리목적</strong> <br><br>
				① 문화체육관광부는 대국민 서비스 제공 및 민원처리, 소관업무 수행 등의 목적으로 필요에 의한 최소한으로 개인정보를 처리하고 있습니다. <br><br>
				② 저작권 위탁업 관리시스템은 아래의 목적을 위하여 개인정보를 처리합니다. 처리하고 개인정보는 다음의 목적 이외의 용도로 이용되지 않으며, 이용 목적이 변경될 때에는 개인정보 보호법 제18조에 따라 별도의 동의를 받는 등 필요한 조치를 이행할 예정입니다. <br><br>

				&nbsp;&nbsp;&nbsp;1) 저작권 위탁업 관리시스템 회원 가입 및 관리 <br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- 회원 가입 의사 확인, 회원제 서비스 제공에 따른 본인 식별ㆍ인증, 회원자격 유지ㆍ관리, 서비스 부정이용 방지, 각종 고지ㆍ통지 등을 목적으로 개인정보를 처리합니다. <br><br>
				&nbsp;&nbsp;&nbsp;2) 저작권 신탁업 허가 및 변경허가, 저작권 대리중개업 신고 및 변경신고, 실적보고 및 통계작성<br><br><br>

				<strong>2. 개인정보의 처리 및 보유 기간</strong> <br><br>
				① 문화체육관광부에서 처리하는 개인정보는 수집ㆍ이용 목적으로 명시한 범위 내에서 처리하며, 개인정보보호법 및 관련 법령에서 정하는 보유기간을 준용하여 이행하고 있습니다. <br><br>
				② 저작권 위탁업 관리시스템은 정보주체의 동의에 의하여 개인정보를 수집하며 회원가입에 의한 처리 및 보유 기간은 2년 입니다.<br><br>
				③ 저작권 위탁업 관리시스템은 행정안전부 표준 개인정보 보호지침 제60조에 따라 2년마다 정보주체의 재동의 절차를 거쳐 동의한 경우에만 계속하여 개인정보를 보유합니다. <br><br>
				④ 저작권 위탁업 관리시스템에서 대리중개업 신고 및 신탁관리업 허가내역 등 인·허가와 관련된 사항으로 준영구적으로 보유합니다.<br><br><br>


				<strong>3. 처리하는 개인정보 항목</strong> <br><br>
				① 저작권 위탁업 관리시스템은 다음의 개인정보 항목을 처리하고 있습니다. <br>
				&nbsp;&nbsp;&nbsp;- 회원가입시(필수) : 성명(대표자 또는 담당자), ID, PW, 전화번호<br>
				&nbsp;&nbsp;&nbsp;- 신고·허가 신청시(선택) : 성명(대표자 또는 담당자), ID, PW, 전화번호, 휴대전화번호, 사업자등록번호, 회사 주소, 팩스번호, 대표자 주민등록번호, 대표자 주소, 대표자 휴대전화번호, 이메일<br><br>

				② 서비스 이용 과정에서 IP 주소, 방문 기록 등의 개인정보 항목이 자동으로 생성되어 수집될 수 있습니다. <br><br><br>

				귀하는 개인정보의 수집·이용에 대한 동의를 거부할 권리가 있습니다. 그러나 동의를 거부할 경우 원활한 서비스 제공에 일부 제한을 받을 수 있습니다.<br>

			</div>
		</div>
		<!-- //privacy_content -->	
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="" />
</jsp:include>
 