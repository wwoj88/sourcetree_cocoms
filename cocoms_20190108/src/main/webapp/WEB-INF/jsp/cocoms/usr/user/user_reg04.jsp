<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="user" />
</jsp:include>

	<div class="container">
		<h2 class="tit_bar blue mt">회원가입</h2>
		<c:set var="joinClass" value="normal" />
		<c:if test="${member.coGubunCd eq '2'}">
			<c:set var="joinClass" value="corporate" />
		</c:if>
		<div class="join_content step_3 ${joinClass}">
			<div class="list_step">
				<ul>
					<li>
						<div class="wrap">
							<p>STEP 01</p>
							<strong>회원유형선택 및 본인인증</strong>
						</div>
					</li>
					<li>
						<div class="wrap">
							<p>STEP 02</p>
							<strong>이용약관 동의</strong>
						</div>
					</li>
					<li class="active">
						<div class="wrap">
							<p>STEP 03</p>
							<strong>회원정보 입력</strong>
						</div>
					</li>
					<li>
						<div class="wrap">
							<p>STEP 04</p>
							<strong>회원가입 완료</strong>
						</div>
					</li>
				</ul>
			</div>
			<!-- //list_step -->	
			<div class="inner">
				<h3 class="tit_top">
					회원정보 입력
					<p>회원정보를 정확하게 기입해주세요.</p>
				</h3>
				<div class="input_area margin input_area2">
					<c:if test="${member.coGubunCd eq '1'}">
						<div class="cell">
							<p class="tit">성명</p><input 
									type="text" class="input_style1 disabled" value="${member.ceoName}" disabled />
							<p class="tit ml">주민등록번호</p><input 
									type="text" class="input_style1 num1 disabled" value="${member.ceoRegNo1}" title="주민등록번호 앞자리" disabled /><span>-</span><input 
									type="text" class="input_style1 num1 disabled" value="*******" title="주민등록번호 뒷자리" disabled />
						</div>
					</c:if>
					<c:if test="${member.coGubunCd eq '2'}">
						<div class="cell cell1">
							<p class="tit">회사명</p><input 
									type="text" class="input_style1 disabled" value="${member.coName}" disabled />
							<p class="tit ml">사업자등록번호</p><input 
									type="text" class="input_style1 num2 disabled" value="${member.coNo1}" title="사업자번호 첫번째자리" disabled /><span>-</span><input 
									type="text" class="input_style1 num2 disabled" value="${member.coNo2}" title="사업자번호 두째자리" disabled /><span>-</span><input 
									type="text" class="input_style1 num2 disabled" value="${member.coNo3}" title="사업자번호 세째자리" disabled />
						</div>
					</c:if>
				</div>
				<!-- //input_area -->	
			</div>			
			
			<form id="form4" method="post" action="<c:url value='/user' />">
			<input type="hidden" name="ceoMobile" value="" />
			<input type="hidden" name="ceoEmail" value="" />

			<div class="table_area list_top">
				<table>
					<caption>회원정보 필수입력</caption>
					<colgroup>
						<col style="width:234px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<!-- 
						<tr>
							<th>이름</th>
							<td>
								<input type="text" name="ceoName" class="input_style1" maxlength="30" placeholder="" title="이름" />
							</td>
						</tr>
						 -->
						<tr>
							<th>아이디 <strong class="txt_point">*</strong></th>
							<td>
								<input type="text" name="loginId" class="input_style1" maxlength="50" placeholder="" title="아이디" /> 
								<button type="button" id="checkId" class="btn_style1" data-check="false">중복확인</button>
							</td>
						</tr>
						<tr>
							<th>비밀번호 <strong class="txt_point">*</strong></th>
							<td><input type="password" name="pwd" class="input_style1" 
									maxlength="20" placeholder="" title="비밀번호" />
									<small>영문/숫자/특수문자 8 ~ 20자리 이하</small></td>
						</tr>
						<tr>
							<th>비밀번호 확인 <strong class="txt_point">*</strong></th>
							<td><input type="password" name="pwd2" class="input_style1" 
									maxlength="20" placeholder="" title="비밀번호 확인" />
									<small>영문/숫자/특수문자 8 ~ 20자리 이하</small></td>
						</tr>
						<tr>
							<th>휴대전화번호 <strong class="txt_point">*</strong></th>
							<td>
								<select name="ceoMobile1" class="select_style" title="전화번호 앞자리">
									<option value="010" selected="selected">010</option>
									<option value="011">011</option>
									<option value="016">016</option>
									<option value="017">017</option>
									<option value="018">018</option>
									<option value="019">019</option>
								</select>
								<span>-</span>
								<input type="text" name="ceoMobile2" class="input_style1 phone" maxlength="4" placeholder="" title="전화번호 중간자리" />
								<span>-</span>
								<input type="text" name="ceoMobile3" class="input_style1 phone" maxlength="4" placeholder="" title="전화번호 뒷자리" />
							</td>
						</tr>
						<tr>
							<th>이메일 주소 <strong class="txt_point">*</strong></th>
							<td>
								<input type="text" name="ceoEmail1" class="input_style1 w169" placeholder="" title="이메일 주소 앞자리" /><span>@</span><input type="text" name="ceoEmail2" class="input_style1 w169" placeholder="" title="이메일 주소 뒷자리" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			
			<!-- 
			<div class="ment_b">
				<div class="box_top_common">
					<ul>
						<li>사업자 공인인증서 없이 가입신청한 법인사업자는 운영 정책에 따라 증빙 서류가 접수되면 영업일 기준 1일 이내 관리자 승인 후 가입처리가 완료되며 가입승인 및 완료 문자가 등록된 담당자 연락처로 발송됩니다.</li>
						<li>웹에서 서류 제출이 어려운 경우, 팩스 (044-203-3466)로 전송해 주세요.</li>
					</ul>
				</div>
				//box_top_common
			</div>
			//ment_b
			 -->
				
			<div class="button_area">
				<button type="button" id="move_step2" class="btn btn_style1">이전단계</button>
				<button type="submit" class="btn btn_style2">가입신청</button>
			</div>
			</form>
			
		</div>
		<!-- //join_content -->	
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="user" />
</jsp:include>
