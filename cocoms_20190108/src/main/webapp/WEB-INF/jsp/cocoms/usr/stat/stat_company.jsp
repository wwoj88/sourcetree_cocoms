<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="status" />
	<jsp:param name="nav1" value="업체현황" />
	<jsp:param name="nav2" value="업체조회" />
</jsp:include>
<div class="container">
	<h2 class="tit_bar pink mt">업체조회</h2>
	<div class="total_top_common">
		<p class="total">
			전체 : <strong><c:out value="${total}" />건</strong>
		</p>
	</div>
	<div class="table_area list_top">
		<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
			<table>
				<caption>업체조회</caption>
				<colgroup>
					<col style="width: 125px" />
					<col style="width: 395px" />
					<col style="width: 125px" />
					<col style="width: 395px" />
				</colgroup>
				<tbody>
					<tr>
						<th>업체선택</th>
						<td><c:if test="${data.conditionCd eq '1'}">
								<c:set var="conditionCd1" value="selected" />
							</c:if> <c:if test="${data.conditionCd eq '2'}">
								<c:set var="conditionCd2" value="selected" />
							</c:if> <select name="conditionCd" class="select_style w172">
								<option value="">전체</option>
								<option value="1" ${conditionCd1}>대리중개업체</option>
								<option value="2" ${conditionCd2}>신탁단체</option>
							</select></td>
						<th>지역선택</th>
						<td><input type="hidden" id="trustSido" value="${data.trustSido}" /> <input type="hidden" id="trustGugun" value="${data.trustGugun}" /> <select name="trustSido" class="select_style w172">
								<option value="">전체</option>
							</select> <select name="trustGugun" class="select_style w172 ml13">
								<option value="">전체</option>
							</select></td>
					</tr>
					<tr>
						<th>회사명</th>
						<td colspan="3"><input type="text" name="coName" class="input_style1 w360" value="${data.coName}" /> <a href="" id="search" class="btn_style2 btn_search" style="height: 30px; line-height: 30px;">검색</a></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
	<br>
	<!-- //table_area -->
	<div class="table_area list list_type1">
		<table>
			<caption>업체조회</caption>
			<colgroup>
				<col style="width: 171px" />
				<col style="width: 246px" />
				<col style="width: 156px" />
				<col style="width: 198px" />
				<col style="width: auto" />
			</colgroup>
			<thead>
				<tr>
					<th>신고번호</th>
					<th>회사명</th>
					<th>전화번호</th>
					<th>홈페이지</th>
					<th>주소</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${list}" varStatus="status">
					<tr>
						<td><c:out value="${item.appNoStr}" /></td>
						<td><c:out value="${item.coName}" escapeXml="false" /></td>
						<td><c:out value="${item.trustTel}" /></td>
						<td><c:out value="${item.trustUrl}" /></td>
						<td><c:if test="${!empty item.trustZipcode}">
									(<c:out value="${item.trustZipcode}" />)
								</c:if> <c:out value="${item.trustSido}" /> <c:out value="${item.trustGugun}" /> <c:out value="${item.trustDong}" /> <c:out value="${item.trustBunji}" /> <c:out value="${item.trustDetailAddr}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<!-- //table_area -->
	<div id="pagination" class="pagination">
		<ul><ui:pagination paginationInfo="${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" /></ul>
	</div>
	
</div>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="status" />
</jsp:include>
