<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="excel" />
	<jsp:param name="nav1" value="위탁관리저작물 보고" />
	<jsp:param name="nav2" value="보고/수정" />
</jsp:include>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		//$('#Progress_Loading').hide(); //첫 시작시 로딩바를 숨겨준다.
		//$('#modalBack').hide(); 
		$('#Progress_Loading').attr('style', "display:none; position: absolute; left: 40%;	top: 38%;");
		$('#modalBack').attr('style', "display:none;");
		
	});

	function chk01_change(chkVal) {
		var $obj = $("input[name='chk01']");
		var checkCount = $obj.size();
		for (var i = 0; i < checkCount; i++) {
			if ($obj.eq(i).is(":checked") == true) {
				$('#checkButton').attr('style', "display:show;");
				$('#upLoadFile').attr('style', "display:none;");
			} else {

				$('#checkButton').attr('style', "display:none;");
				$('#upLoadFile').attr('style', "display:show;");
			}
		}
	}
</script>
<div id="Progress_Loading" style="position: absolute;	left: 40%;	top: 38%; display: none;">
	<!-- 로딩바 -->
	<img src="<c:url value="/img/loading.gif" />" alt="저작권위탁관리업시스템" />
</div>

<!-- <div id="modalBack" style="position: absolute; background-color: black; z-index: 9999; width: 1920px; height: 1300px; opacity: 0.3; left: 0px; top: 0px; display: none;"></div> -->
<form name="formUpload" id="formUpload" method="post" enctype="multipart/form-data">
<div class="container">
	<div class="menu_aside">
		<h2 class="tit_bar pink mt">보고/수정</h2>
		<ul>
			<li class="active"><a href="<c:url value='/report/excel' />">일괄등록</a></li>
			<li><a href="<c:url value='/report/exceledit' />">일괄수정</a></li>
		</ul>
	</div>
	<div class="cont_notice">
		<div class="box_top_common">
			<ul>
				<li>위탁관리 저작물 보고/수정</li>
			</ul>
		</div>
		<h2 class="tit_bar pink">보고자 정보</h2>

		<div class="table_area  list_top">


			<table>
				<caption>업체현황</caption>
				<colgroup>
					<col style="width: 186px">
					<col style="width: 395px">
					<col style="width: 186px">
					<col style="width: 395px">
				</colgroup>
				<tbody>
					
					<tr>
						<th>
							담당자<strong class="txt_point">*</strong>
						</th>
						<td>
							<input type="text" id="REPT_CHRR_NAME" name="REPT_CHRR_NAME" class="input_style1" value="">
						</td>
						<th>
							담당자 직위<strong class="txt_point">*</strong>
						</th>
						<td>
							<input type="text" id="REPT_CHRR_POSI" name="REPT_CHRR_POSI" class="input_style1" value="">
						</td>
					</tr>
					<tr>
						<th>
							담당자 전화번호<strong class="txt_point">*</strong>
						</th>
						<td>
							<input type="text" id="REPT_CHRR_TELX" name="REPT_CHRR_TELX" class="input_style1" value="">
						</td>
						<th>
							담당자 이메일<strong class="txt_point">*</strong>
						</th>
						<td>
							<input type="text" id="REPT_CHRR_MAIL" name="REPT_CHRR_MAIL" class="input_style1" value="">
						</td>
					</tr>
					<tr>
						<th>보고저작물 없음</th>
						<td>
							<input type="checkbox" id="chk01" name="chk01" style="height: 26px;" onchange="javascript:chk01_change(this.value);">

						</td>
						<th><a href="#" id="checkButton" style="float: right; display: none; height: 26px;" class="btn_style2"> 보고</a></th>
						<td>
							해당월 보고저작물이 없을경우 체크 후 보고
							<!-- <input type="button" id="checkButton" name="checkButton" value="위탁관리저작물 보고 " style="float: right; display: none; height:26px;" class="btn_style2"> -->
						</td>
					</tr>
				</tbody>
			</table>

		</div>
		<h2 class="tit_bar pink mt">위탁저작물 일괄등록</h2>
		<div class="table_area list_top">
			<table>
				<caption>일반사항</caption>
				<colgroup>
					<col style="width: 148px" />
					<col style="width: 428px" />

				</colgroup>
				<tbody>
					<tr>
						<th>양식</th>
						<td>
							<div class="list_file">
								<select class="select_style w172" id="gercdDown" name="gercdDown">
									<option selected="selected" value="">-선택-</option>
									<option value="1">음악</option>
									<option value="2">어문</option>
									<option value="3">방송대본</option>
									<option value="4">영화</option>
									<option value="5">방송</option>
									<option value="6">뉴스</option>
									<option value="7">미술</option>
									<option value="8">이미지</option>
									<option value="99">기타</option>
								</select>
								<a href="#" id="downFile" class="form_btn insert_file btn_style1">파일 다운로드</a>

							</div>


						</td>
					</tr>
					<tr>

						<th>첨부파일</th>
						<td>
							<div class="list_file">


								<select class="select_style w172" id="gercd" name="gercd">
									<option selected="selected" value="">-선택-</option>
									<option value="1">음악</option>
									<option value="2">어문</option>
									<option value="3">방송대본</option>
									<option value="4">영화</option>
									<option value="5">방송</option>
									<option value="6">뉴스</option>
									<option value="7">미술</option>
									<option value="8">이미지</option>
									<option value="99">기타</option>
								</select>
								<div id="DivIdFile1" style="display: inline-block;">
									<input type="text" class="input_style1 prj_file" placeholder="파일 첨부" readonly />
									<input type="file" name="addFile2" class="insert_file_target">
									<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>

									
									<a href="#" id="upLoadFile"  class="btn_style2">보고 </a>
									<!-- onclick="javascript:file_upload(this.value);" -->

								</div>
						

							</div>


						</td>
					</tr>

				</tbody>
			</table>
			<div class="table_area list list_type1">
			
			</div>



		</div>
	</div>

</div>
</form>
<!-- //table_area -->


<!-- //table_area -->
<div class="pagination"></div>
<div class="button_area">
	<c:if test="${reg_auth}">
		<a href="<c:url value="/report/trust/years/${reg_year}/new" />" class="btn btn_style2">등록</a>
	</c:if>
</div>

<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="js" value="report" />
</jsp:include>
