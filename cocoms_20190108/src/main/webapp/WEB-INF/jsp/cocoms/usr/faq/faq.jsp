<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="notice" />
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="이용안내" />
	<jsp:param name="nav3" value="자주묻는질문(FAQ)" />
</jsp:include>
	
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar pink mt">이용안내</h2>
			<ul>
				<li><a href="<c:url value='/perm/intro1' />">공인인증서</a></li>
				<li><a href="<c:url value='/perm/intro2' />">관련저작권법</a></li>
				<li class="active"><a href="<c:url value='/faq' />">자주묻는질문(FAQ)</a></li>
				<%-- <li><a href="<c:url value='/inquiry/new' />">1:1문의</a></li> --%>
				<li><a href="<c:url value='/perm/downloads' />">문서양식 자료실</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice">
			<h2 class="tit_bar mt">자주묻는질문(FAQ)</h2>
			<div class="box_top_common">
				<p class="txt_center"> 궁금해 하시는 사항에 대한 <strong>자주 묻는질문의 답변</strong>을 보실 수 있습니다.</p>
			</div>
			<!-- //box_top_common -->
			
			<div class="list_qa">
				<c:forEach var="item" items="${list}" varStatus="status">
					<div class="tab_property">
						<a class="tab"><c:out value="${item.title}"  escapeXml="false"/></a>
						<div class="item">
							${item.content2}
							<c:if test="${status.count eq '1'}"><br>첨부파일<br><a href="<c:url value='/download2?filename=8.zip' />">저작권대리중개업 신고증 분실신고서</a></c:if> 
							<c:if test="${status.count eq '2'}"><br>첨부파일<br><a href="<c:url value='/download2?filename=5.zip' />">(안내) 저작권대리중개업_표준계약서 작성방법</a></c:if>
							<c:if test="${status.count eq '3'}"><br>첨부파일<br><a href="<c:url value='/download2?filename=2.zip' />">저작권대리중개업 폐업신고서</a></c:if>
							<c:if test="${status.count eq '4'}">
							
							<br>
							첨부파일
							<br><a href="<c:url value='/download2?filename=3.zip' />">(표준약관) 저작권대리중개업 업무규정 및 계약약관</a><br/>
							<a href="<c:url value='/download2?filename=4.zip' />">(안내) 저작권대리중개업 금지행위</a><br/>
							<a href="<c:url value='/download2?filename=6.zip' />">(작성양식) 이력서양식</a><br/>
							<a href="<c:url value='/download2?filename=7.zip' />">(작성양식) 재무제표_미제출_사유서</a>
							</c:if>
							<c:if test="${status.count eq '5'}">
							
							<br>
							첨부파일
							<br><a href="<c:url value='/download2?filename=3.zip' />">(표준약관) 저작권대리중개업 업무규정 및 계약약관</a><br/>
							<a href="<c:url value='/download2?filename=4.zip' />">(안내) 저작권대리중개업 금지행위</a><br/>
							<a href="<c:url value='/download2?filename=6.zip' />">(작성양식) 이력서양식</a><br/>
							<a href="<c:url value='/download2?filename=7.zip' />">(작성양식) 재무제표_미제출_사유서</a>
							</c:if>
						
						</div>
					</div>
					<!-- //tab_property -->
				</c:forEach>
			</div>
			
			<form id="searchForm" method="GET">
				<input type="hidden" name="pageIndex" value="${page}" />
				
				<div id="pagination" class="pagination">
		       	<ul>	<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" /></ul>
				</div>
			</form>
		
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="notice" />
</jsp:include>
 