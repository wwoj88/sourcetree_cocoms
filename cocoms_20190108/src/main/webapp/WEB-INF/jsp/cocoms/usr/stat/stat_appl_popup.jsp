<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value="/css/html5_reset.css" />">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/adm_layout.css" />">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/utill.css" />">
</head>
<body>

<div class="declaration_preview pop">
	<div class="inner">
		<div class="preview">
			${data.applContent}
		</div>
		<!-- //preview -->
	</div>
	<div class="btn_area">
		<!-- <a href="" id="print" class="btn btn_style1">PDF 다운로드</a>  -->
		<%-- 
		<a href="" class="btn btn_style2">출력</a>
		--%>
	</div>
</div>
<!-- //declaration_preview -->

<c:choose>
	<c:when test="${data.docType eq '1'}"><input type="hidden" id="rpx" value="app_declaration_c_new" /></c:when>
	<c:when test="${data.docType eq '2'}"><input type="hidden" id="rpx" value="contract_declaration_new" /></c:when>
	<c:when test="${data.docType eq '3'}"><input type="hidden" id="rpx" value="app_declaration_c_m_new" /></c:when>
	<c:when test="${data.docType eq '4'}"><input type="hidden" id="rpx" value="contract_declaration_m_new" /></c:when>
	<c:when test="${data.docType eq '5'}"><input type="hidden" id="rpx" value="app_declaration_union_c_new" /></c:when>
	<c:when test="${data.docType eq '6'}"><input type="hidden" id="rpx" value="contract_declaration_union_c_new" /></c:when>
	<c:when test="${data.docType eq '7'}"><input type="hidden" id="rpx" value="app_declaration_union_m_new" /></c:when>
	<c:when test="${data.docType eq '8'}"><input type="hidden" id="rpx" value="contract_declaration_union_m_new" /></c:when>
</c:choose>
<textarea id="xmldata" style="width: 100%; height: 500px; display: none;">${data.pdfContent}</textarea>	

<script src="<c:url value="/js/jquery-1.10.2.min.js" />"></script>
<script src="<c:url value="/js/jquery.bxslider.js" />"></script>
<script src="<c:url value="/js/adm_script.js" />"></script>
<script src="<c:url value="/js/jquery-ui.js" />"></script>

<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>

<script>
$(function () {
	
	$('#print').click(function (event) {
		
		event.preventDefault();
		
		var rpx = $('#rpx').val();
		var xmldata = $('#xmldata').text();

		printReport(rpx, xmldata);
	});
	
});
</script>
	
</body>
</html>
