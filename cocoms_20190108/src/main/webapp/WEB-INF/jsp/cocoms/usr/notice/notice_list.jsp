<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="notice" />
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="공지사항" />
</jsp:include>
	
	<div class="container">
		<h2 class="tit_bar pink mt">공지사항</h2>
		<div class="total_top_common">
			<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
		</div>
		<div class="table_area list board">
			<table>
				<caption>공지사항</caption>
				<colgroup>
					<col style="width:77px" />
					<col style="width:auto" />
					<col style="width:96px" />
					<col style="width:138px" />
					<col style="width:138px" />
					<col style="width:88px" />
				</colgroup>
				<thead>
					<tr> 	 	 	
						<th>번호</th>
						<th>제목</th>
						<th>첨부파일</th>
						<th>작성자</th>
						<th>작성일</th>
						<!-- <th>조회수</th> -->
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><c:out value="${start - status.index}" /></td>
							<td class="subject">
								<a href="<c:url value="/notice/${item.boardSeqNo}?pageIndex=${search.pageIndex}&searchCondition=${search.searchCondition}&searchKeyword=${search.searchKeyword}" />"><c:out value="${item.title}" /></a>
								<c:if test="${item.regDate eq today}">
									<strong class="new">NEW</strong>
								</c:if>  
							</td>
							<td>
								<c:if test="${fn:length(item.boardFileDataList) gt '0'}">
									<span title="fileDown" class="fileDown board">파일 다운로드</span>
								</c:if>
							</td>
							<td><c:out value="${item.regName}" /></td>
							<td><c:out value="${item.regDateStr}" /></td>
							<%-- <td><c:out value="${item.hit}" /></td> --%>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(list) eq '0'}">
						<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
					</c:if>					
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
			<input type="hidden" name="searchCondition" value="all" />
			
			<div id="pagination" class="pagination">
			 <ul>
	       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
	       		</ul>
			</div>
	
			<div class="button_area">
				<div class="search_form_common">
					<input type="text" name="searchKeyword" class="input_style1" value="${search.searchKeyword}" placeholder="검색어를 입력해 주세요" title="검색어" />
					<input type="submit" id="search" class="btn btn_style2" value="검색" />
				</div>
				<!-- //search_form_common -->
			</div>
		</form>
			
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="notice" />
</jsp:include>
 