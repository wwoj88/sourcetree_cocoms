<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="신고/허가 내역" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />

	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li class="active"><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<%-- <li><a href="<c:url value="/qna" />">1:1문의 내역</a></li> --%>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage">
			<h2 class="tit_bar mt">신고/허가 내역</h2>
			
			<div id="appl_content" class="cont" data-doc-type="7">
				<div class="table_area">
					<table>
						<caption>저작권신탁관리업 허가증 변경교부 신청서</caption>
						<thead>
							<tr> 	 	 	
								<th>저작권신탁관리업 허가증 변경교부 신청서</th>
							</tr>
						</thead>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="declaration_preview">
					<div class="inner">
						<div class="preview">
							<p class="num_top txt_r">처리기간 : 4일</p>
							<h1 class="tit">저작권신탁관리업 허가증 변경교부 신청서</h1>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:129px" />
										<col style="width:222px" />
										<col style="width:158px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="3">신고인</th>
											<th>성명<p>(단체 또는 법인명)</p></th>
											<td><c:out value="${data.coName}" /></td>
											<th>생년월일<p>(법인/사업자등록번호)</p></th>
											<td>
												<c:if test="${!empty data.bubNo}"><c:out value="${data.bubNoStr}" /></c:if>
												<c:if test="${empty data.bubNo}"><c:out value="${data.coNoStr}" /></c:if>	
																						
											</td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td><c:out value="${data.trustTel}" /></td>
											<th>팩스번호</th>
											<td><c:out value="${data.trustFax}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">
												<c:if test="${!empty data.trustZipcode}">(<c:out value="${data.trustZipcode}" />)</c:if>
												<c:out value="${data.trustSido}" />
												<c:out value="${data.trustGugun}" />
												<c:out value="${data.trustDong}" />
												<c:out value="${data.trustBunji}" />
												<c:out value="${data.trustDetailAddr}" />
											</td>											
										</tr>
										<tr>
											<th class="bg" rowspan="3">대표자<p>(법인 또는<br>단체에 한함)</p></th>
											<th>성명</th>
											<td><c:out value="${data.ceoName}" /></td>
											<th>생년월일</th>
											<td><c:out value="${birthday}" /></td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td><c:out value="${data.ceoTel}" /></td>
											<th>이메일 주소</th>
											<td><c:out value="${data.ceoEmail}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">
												<c:if test="${!empty data.ceoZipcode}">(<c:out value="${data.ceoZipcode}" />)</c:if>
												<c:out value="${data.ceoSido}" />
												<c:out value="${data.ceoGugun}" />
												<c:out value="${data.ceoDong}" />
												<c:out value="${data.ceoBunji}" />
												<c:out value="${data.ceoDetailAddr}" />
											</td>											
										</tr>
									</tbody>
								</table>
								<%-- <table class="bt0">
									<caption></caption>
									<colgroup>
										<col style="width:150px" />
										<col style="width:225px" />
										<col style="width:225px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="2">변경사항</th>
											<th>변경 전<span>(을)</span></th>
											<th>변경 후<span>(으로)</span></th>
											<th>변경사유</th>
										</tr>
										<tr>
												<td class="txt_l pl12">
												<c:out value="${before}" />
													${preWritingKind}<br>
												${prePermKind}<br>
												
											</td>
											<td class="txt_l pl12">
												<c:out value="${after}" />
														${WritingKind}<br>
												${PermKind}<br>
											</td>
											<td class="txt_l pl12">
												<c:out value="${chgHistory.chgMemo}" />
											</td>
										</tr>
									</tbody>
								</table> --%>
								<table class="bt0">
									<caption></caption>
									<colgroup>
										<col style="width:150px" />
										<col style="width:225px" />
										<col style="width:225px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="2">변경사항</th>
											<th>변경 전<span>(을)</span></th>
											<th>변경 후<span>(으로)</span></th>
											<th>변경사유</th>
										</tr>
										<tr>
											<td  class="txt_l pl12">
												<pre style="white-space: pre-wrap;"><c:out value="${before}" /></pre>
												<c:if test="${!empty preWritingKind}" >취급하고자 하는 저작물등의 종류:<br> ${preWritingKind}<br></c:if>
												<c:if test="${!empty prePermKind}" >취급하고자 하는 권리:<br> ${prePermKind}<br></c:if>
											</td>
											<td class="txt_l pl12">
												<div>
												<pre style="white-space: pre-wrap;"><c:out value="${after}" /></pre>
												<c:if test="${!empty WritingKind}" >취급하고자 하는 저작물등의 종류:<br> ${WritingKind}<br></c:if>
												<c:if test="${!empty PermKind}" >취급하고자 하는 권리:<br> ${PermKind}<br></c:if>
												</div>
											</td>
											<td class="txt_l pl12">
												<c:out value="${chgHistory.chgMemo}" escapeXml="false"/>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<p class="txt1"><strong>[저작권법] </strong>시행령 제 47조 제 2항의 저작신탁관리업 허가증 변경교부를 신청합니다.</p>
							<div class="sign">
								<p><c:out value="${regDate}" /></p>
								<p>신청인   <c:out value="${data.coName}" />   (서명 또는 인)</p>
							</div>
							<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:auto" />
										<col style="width:266px" />
										<col style="width:134px" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="2">구비서류</th>
											<th>민원인 제출서류</th>
											<th>담당공무원 확인사항</th>
											<th>수수료</th>
										</tr>
										<tr>
											<td>
												신고증

											</td>
											<td>변경사항을 증명하는 서류 1부</td>
											<td>3,000원</td>
										</tr>
										<tr>
											<th class="bg">첨부파일</th>
											<td colspan="3" class="txt_l">
												<div class="file_change">
													<c:if test="${!empty rpMgm.file1Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file1Path}" />"><p class="txt1">1. 저작권대리중개업 업무규정</p></a>
													</c:if>												
													<c:if test="${!empty rpMgm.file2Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file2Path}" />"><p class="txt1">2. 신고인(단체 또는 법인인 경우에는 그 대표자및 임원)의 이력서</p></a>
													</c:if>												
													<c:if test="${!empty rpMgm.file3Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file3Path}" />"><p class="txt1">3. 정관 또는 규약 1부</p></a>
													</c:if>												
													<c:if test="${!empty rpMgm.file4Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file4Path}" />"><p class="txt1">4. 재무제표(법인단체)</p></a>
													</c:if>												
													
													<c:forEach var="item" items="${chgHistoryFileList}" varStatus="status">
														<a href="<c:url value="/download?filename=${item.filepath}" />">
															<p class="txt1">변경신고신청 첨부<c:out value="${status.count}" /></p>
															<p class="txt2"><c:out value="${item.filename}" /></p>
															<p class="txt3">(<c:out value="${item.regDateStr}" />)</p>
														</a>
													</c:forEach>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
						</div>
						<!-- //preview -->
					</div>
				</div>
				<!-- //declaration_preview -->
			</div>
			<!-- //cont -->
			
			<div class="right">
				<!-- 
				<a href="" class="btn_style2">PDF저장</a>
				 -->
				<a href="" id="print" class="btn_style2">인쇄</a>
				<a href="<c:url value="/report" />" class="btn_style1">이전</a>
			</div>
		</div>
	</div>
		
<input type="hidden" id="rpx" value="app_declaration_union_m_new" />
<textarea id="xmldata" style="width: 100%; height: 500px; display: none;">
	<root>
		<day><c:out value="${regDate}" /></day><!-- 신고일자 -->
		<title>저작권신탁관리업 허가증 변경교부 신청서</title>
		<process_term>4일</process_term><!-- 처리기간 -->

		<company_info><!-- 업체기본정보 -->
			<company><![CDATA[${data.coName}]]></company><!-- 상호명 -->
			<regno><c:if test="${!empty data.bubNo}"><c:out value="${data.bubNoStr}" /></c:if><c:if test="${empty data.bubNo}"><c:out value="${data.coNoStr}" /></c:if></regno><!-- 사업자등록번호 -->                                          
			<tel><c:out value="${data.trustTel}" /></tel><!-- 전화번호 -->
			<fax><c:out value="${data.trustFax}" /></fax><!-- 팩스번호 -->	
			<address><![CDATA[${data.trustZipcode} ${data.trustSido} ${data.trustGugun} ${data.trustDong} ${data.trustBunji} ${data.trustDetailAddr}]]></address>
			<boss><!-- 대표자 -->
				<name><c:out value="${data.ceoName}" /></name>
				<birthday><c:out value="${birthday}" /></birthday><!-- 생년월일 -->
				<tel><c:out value="${data.ceoTel}" /></tel><!-- 전화번호 -->
				<email><c:out value="${data.ceoEmail}" /></email><!-- email -->
				<address><![CDATA[${data.ceoZipcode} ${data.ceoSido} ${data.ceoGugun} ${data.ceoDong} ${data.ceoBunji} ${data.ceoDetailAddr}]]></address><!-- 주소 -->
			</boss>
		</company_info>
		
		<change><!-- 변경사항 -->
			<change_before><![CDATA[${before}]]>
			<![CDATA[${preWritingKind}]]>
			<![CDATA[${prePermKind}]]></change_before>
			<change_after><![CDATA[${after}]]>
			<![CDATA[${WritingKind}]]>
			<![CDATA[${PermKind}]]></change_after>
			<change_reason><![CDATA[${chgHistory.chgMemo}]]></change_reason>
		</change>
		
		<fee>3000</fee><!-- 수수료 -->
		<writer><c:out value="${data.ceoName}" /></writer><!-- 신고인(대표자) -->
	</root>
</textarea>
		
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
