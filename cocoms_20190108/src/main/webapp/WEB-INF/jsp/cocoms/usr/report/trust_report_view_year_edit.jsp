<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="type" value="report" />
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="신탁단체" />
</jsp:include>

	<div class="container">
	
		<form id="form" method="post" action="<c:url value="/reports/trust/years/${year}" />">
		<input type="hidden" name="trust" value="1"/>
		<input type="hidden" name="trustInfoSeqno" value="${report.trustInfoSeqno}" />
		<input type="hidden" name="filepath1" value="${report.filepath1}" />
		<input type="hidden" name="filepath2" value="${report.filepath2}" />
		<input type="hidden" name="filepath3" value="${report.filepath3}" />
		<input type="hidden" name="filepath4" value="${report.filepath4}" />
		
		<c:if test="${regist eq true}">
			<input type="hidden" name="returnUrl" value="/reports/trust/years/${year}/tab2/new" />
		</c:if>

		<h2 class="tit_bar yellow mt"><c:out value="${member.coName}" /></h2>

		<div class="tab_result yellow">
			<ul>
				<c:if test="${regist ne true}">
					<li class="active"><a href="<c:url value="/reports/trust/years/${year}" />">실적정보</a></li>
					<li><a href="<c:url value="/reports/trust/years/${year}/tab2" />">저작권료 징수분배 내역</a></li>
					<li><a href="<c:url value="/reports/trust/years/${year}/tab3" />">사업계획</a></li>
				</c:if>
				<c:if test="${regist eq true}">
					<li class="active"><a href="<c:url value="/reports/trust/years/${year}/new" />">실적정보</a></li>
					<li><a>저작권료 징수분배 내역</a></li>
					<li><a>사업계획</a></li>
				</c:if>
			</ul>
		</div>
		<!-- //step_sub -->
		
		<h2 class="tit_c_s">1. 현황</h2>
		<div class="table_area list list_type1">
			<table>
				<caption></caption>
				<colgroup>
					<col style="width:50%" />
					<col style="width:50%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>회원</th>
						<th>관리 저작물수</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="text" name="thisMember" class="input_style1 w63" value="${report.thisMember}" /> 명 / 
							<input type="text" name="thisGroup" class="input_style1 w63" value="${report.thisGroup}" /> 단체</td>
						<td><input type="text" name="thisWorknum" class="input_style1 w87" value="${report.thisWorknum}" /> 건</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_box yellow mt">결산 및 예산</h2>
		<div class="table_area list list_type2 tbl_style_b">
			<table>
				<caption>결산 및 예산</caption>
				<colgroup>
					<col style="width:301px" />
					<col style="width:auto" />
					<col style="width:450px" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>구분</th>
						<th><c:out value="${lastYear+1}" /> 결산 <span>(단위 : 천원)</span></th>
						<th><c:out value="${year+1}" /> 예산 <span>(단위 : 천원)</span></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="bg_total">신탁회계</td>
						<td><input type="text" name="lastTrustAccnt" class="input_style1" value="${report.lastTrustAccnt}" /></td>
						<td><input type="text" name="thisTrustAccnt" class="input_style1" value="${report.thisTrustAccnt}" /></td>
					</tr>
					<tr>
						<td class="bg_total">보상금 회계</td>
						<td><input type="text" name="lastIndemnityAccnt" class="input_style1" value="${report.lastIndemnityAccnt}" /></td>
						<td><input type="text" name="thisIndemnityAccnt" class="input_style1" value="${report.thisIndemnityAccnt}" /></td>
					</tr>
					<tr>
						<td class="bg_total">일반회계</td>
						<td><input type="text" name="lastGeneralAccnt" class="input_style1" value="${report.lastGeneralAccnt}" /></td>
						<td><input type="text" name="thisGeneralAccnt" class="input_style1" value="${report.thisGeneralAccnt}" /></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_c_s">2. 전년도 실적<span>(<c:out value="${year}" />년)</span> 
			<%-- 
			<a href="" class="btn btn_style1">양식 다운로드</a>
			--%>
			<p class="txt_red">※ <strong>인원 </strong>: 신규입회 회원 수 기재 / <strong>저작물 수</strong> : 신규등록 된 저작물 수, 전년도 신탁받은 관리저작물 목록을 업로드 해주시기 바랍니다. </p></h2>
		<div class="table_area list list_type1">
			<table>
				<caption>전년도 실적</caption>
				<colgroup>
					<col style="width:50%" />
					<col style="width:50%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>회원</th>
						<th>관리 저작물수</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="text" name="lastMember" class="input_style1 w63" value="${report.lastMember}" /> 명 / 
							<input type="text" name="lastGroup" class="input_style1 w63" value="${report.lastGroup}" /> 단체</td>
						<td><input type="text" name="lastWorknum" class="input_style1 w87" value="${report.lastWorknum}" /> 건</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_c_s">3. 첨부자료<span>(<c:out value="${year}" />년)</span></h2>
		<div class="list_finl_link bg">
			<ul>
				<li>
					<p>저작물내역</p>
					<div class="list_file">
						<input type="text" name="filename1" class="input_style1 prj_file" value="${report.filename1}" placeholder="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file1" class="insert_file_target" data-ref-name="filename1" data-ref-name2="filepath1" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file1" class="form_btn insert_file btn_style1" data-ref-name="filename1" data-ref-name2="filepath1">파일삭제</button>
					</div>
				</li>
				<li>
					<p>전년도 사업실적서</p>
					<div class="list_file">
						<input type="text" name="filename2" class="input_style1 prj_file" value="${report.filename2}" placeholder="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file2" class="insert_file_target" data-ref-name="filename2" data-ref-name2="filepath2" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file2" class="form_btn insert_file btn_style1" data-ref-name="filename2" data-ref-name2="filepath2">파일삭제</button>
					</div>
				</li>
				<li>
					<p>결산서<span>(제무제표와 부속서류)</span></p>
					<div class="list_file">
						<input type="text" name="filename3" class="input_style1 prj_file" value="${report.filename3}" placeholder="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file3" class="insert_file_target" data-ref-name="filename3" data-ref-name2="filepath3" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file3" class="form_btn insert_file btn_style1" data-ref-name="filename3" data-ref-name2="filepath3">파일삭제</button>
					</div>
				</li>
				<li>
					<p>감사의 감사보고서 </p>
					<div class="list_file">
						<input type="text" name="filename4" class="input_style1 prj_file" value="${report.filename4}" placeholder="PDF, JPG 파일만 첨부" readonly />
						<input type="file" name="file4" class="insert_file_target" data-ref-name="filename4" data-ref-name2="filepath4" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" id="remove_file4" class="form_btn insert_file btn_style1" data-ref-name="filename4" data-ref-name2="filepath4">파일삭제</button>
					</div>
				</li>
			</ul>
		</div>
		<!-- //list_finl_link -->
		
		<div class="button_area">
			<a href="" id="save" class="btn btn_style2">저장</a>
			<a href="<c:url value="/reports/trust" />" class="btn btn_style1">취소</a>
		</div>
		
		</form>

	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="js" value="report" />
</jsp:include>
