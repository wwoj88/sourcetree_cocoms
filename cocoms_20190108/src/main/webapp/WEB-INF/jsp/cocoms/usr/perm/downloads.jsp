<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="type" value="notice" />
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="이용안내" />
	<jsp:param name="nav3" value="관련저작권법" />
</jsp:include>
<script>
function downLoad(name){
	var name = encodeURIComponent(name);
	alert(name);
}
</script>
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar pink mt">이용안내</h2>
			<ul>
				<li><a href="<c:url value='/perm/intro1' />">공인인증서</a></li>
				<li><a href="<c:url value='/perm/intro2' />">관련저작권법</a></li>
				<li><a href="<c:url value='/faq' />">자주묻는질문(FAQ)</a></li>
				<%-- <li><a href="<c:url value='/inquiry/new' />">1:1문의</a></li> --%>
				<li class="active"><a href="<c:url value='/perm/downloads' />">문서양식 자료실</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice">
			<h2 class="tit_bar mt">문서양식 자료실</h2>
			<div class="box_top_common">
				<p class="txt_center"> 저작권위탁관리업 서비스 이용에 필요한 문서양식을 다운로드 하실 수 있습니다.</p>
			</div>
			<!-- //box_top_common -->
			
			<div class="list_qa">
				<div class="tab_property">
					<a class="tab">업무(실적)보고 매뉴얼입니다.</a>
					<div class="item2">
						<a href="<c:url value='/download2?filename=1.zip' />">년도별 저작권대리중개업 업무보고(실적보고) 매뉴얼 안내문</a><br/>
						<!-- <a href="/cocoms/download?filename=/docs/저작권대리중개업 폐업신고서.zip">저작권대리중개업 폐업신고서.zip</a><br/> -->
					</div>
				</div>
				<!-- //tab_property -->
				<div class="tab_property">
					<a class="tab">주요양식 다운로드(폐업)</a>
					<div class="item2">
						<a href="<c:url value='/download2?filename=2.zip' />">저작권대리중개업 폐업신고서</a><br/>
					</div>
				</div>
					<div class="tab_property">
					<a class="tab">신고서 분실시 양식 다운로드</a>
					<div class="item2">
						<a href="<c:url value='/download2?filename=8.zip' />">저작권대리중개업 신고증 분실신고서</a><br/>
					</div>
				</div>
				<!-- //tab_property -->
				<div class="tab_property">
					<a class="tab">신고, 변경신고시 양식 다운로드</a>
					<div class="item2">
						<a href="<c:url value='/download2?filename=3.zip' />">(표준약관) 저작권대리중개업 업무규정 및 계약약관</a><br/>
						<a href="<c:url value='/download2?filename=4.zip' />">(안내) 저작권대리중개업 금지행위</a><br/>
						<a href="<c:url value='/download2?filename=5.zip' />">(안내) 저작권대리중개업_표준계약서 작성방법</a><br/>
						<a href="<c:url value='/download2?filename=6.zip' />">(작성양식) 이력서양식</a><br/>
						<a href="<c:url value='/download2?filename=7.zip' />">(작성양식) 재무제표_미제출_사유서</a>
						
					</div>
				</div>
			</div>
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
</jsp:include>
