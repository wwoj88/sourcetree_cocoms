<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="intro" />
	<jsp:param name="nav1" value="이메일주소 무단수집거부" />
</jsp:include>
	
	<div class="container">
		<div class="privacy_content email">
			<h2 class="tit_bar blue">이메일주소 무단수집거부</h2>
			<div class="inner">
				<strong>저작권위탁관리업시스템 홈페이지는 이메일 주소 수집을 거부합니다.</strong><br>
저작권위탁관리업시스템 홈페이지에 게시된 이메일 주소가 자동 수집되는 것을 거부하며, 이를 위반시 정보통신망법에 의해 처벌됨을 유념하시기 바랍니다. <br>
저작권위탁관리업시스템 홈페이지에 게재된 모든 내용은 저작권법에 의해서 보호됨을 알려드립니다.
			</div>
		</div>
		<!-- //privacy_content -->	
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="" />
</jsp:include>
 