<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="notice" />
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="이용안내" />
	<jsp:param name="nav3" value="1:1문의" />
</jsp:include>

	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar pink mt">이용안내</h2>
			<ul>
				<li><a href="<c:url value='/perm/intro1' />">공인인증서</a></li>
				<li><a href="<c:url value='/perm/intro2' />">관련저작권법</a></li>
				<li><a href="<c:url value='/faq' />">자주묻는질문(FAQ)</a></li>
				<li class="active"><a href="<c:url value='/inquiry/new' />">1:1문의</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice">
			<h2 class="tit_bar mt">1:1문의</h2>
			<div class="box_top_common">
				<ul>
					<li>서비스 이용 중 궁금한 사항을 문의 주시면 친절히 답변 드립니다.</li>
					<li>문의하신 내용의 답변은 <strong>마이페이지> 메뉴</strong>에서 확인 하실 수 있습니다.</li>
					<li>고객 문의가 많아 답변이 지연될 수 있는 점은 양해바라며, 빠른 처리를 위해 노력하겠습니다.</li>
				</ul>
			</div>
			<!-- //box_top_common -->
			<form id="form" method="POST" action="<c:url value="/inquiry" />">
			
			<div class="table_area list_top">
				<table>
					<caption>1:1문의</caption>
					<colgroup>
						<col style="width:220px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>제목</th>
							<td><input type="text" name="title" class="input_style1" title="제목" /></td>
						</tr>
						<tr>
							<th>내용</th>
							<td><textarea name="content" class="input_style1 height"></textarea></td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="btn_area">
				<a href="" id="submit" class="btn btn_style2">등록</a>
			</div>
			</form>
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="inquiry" />
</jsp:include>
 