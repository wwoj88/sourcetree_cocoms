<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="excel" />
	<jsp:param name="nav1" value="위탁관리저작물 보고" />
	<jsp:param name="nav2" value="등록현황" />
</jsp:include>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>


<div class="container">
	<h2 class="tit_bar pink mt">등록현황</h2>
	<div class="total_top_common">
			<p class="total">전체 : <strong>${total}건</strong></p>
			
		</div>
		<div class="table_area list_top list_type3">
			<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/excel_search.jsp" flush="true" />
		</div>
		
		
		<br>
		<div class="total_top_common">
				<a href="" id="deleteList" class="btn_style2 " style ="float:right;">위탁저작물 삭제</a>
			
		</div>


	<!-- //table_area -->
	<div class="table_area list list_type1">
		<table id="data">
				<caption>등록현황</caption>
				<colgroup>
					<col style="width:20px" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />
					<col style="width:auto" />

				</colgroup>
				<thead>
					<tr> 	 
						<th>
							<div >
								<input type="checkbox" id="checkAll">
								<label for="checkAll">선택</label> 
							</div>
						</th>
						<th>보고년월</th>
						<th>저작물 관리번호</th>
						<th>국내외구분</th>
						<th>저작물분류</th>
						<th>저작물명</th>
						<th>저작물부제</th>
						<th>창작년도(발행연월일)</th>
						<th>위탁관리구분</th>
						<th>작성기관명</th>
						
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${uploadList}" varStatus="status">
						<tr>
							<td>
								<div >
									<input type="checkbox" id="checkbox_${item.WORKS_ID}" value="${item.WORKS_ID}" />
									<label for="checkbox_${item.WORKS_ID}"></label> 
								</div>
							</td>
							<td>${item.REPT_YMD}</td>
							<td>${item.COMM_WORKS_ID}</td>
							<td>
								<c:if test="${item.NATION_CD == '1' }">국내</c:if>
								<c:if test="${item.NATION_CD == '2' }">국외</c:if>
							</td>
							<td>
								<c:if test="${item.GENRE_CD == '1' }">음악</c:if>
								<c:if test="${item.GENRE_CD == '2' }">어문</c:if>
								<c:if test="${item.GENRE_CD == '3' }">방송대본</c:if>
								<c:if test="${item.GENRE_CD == '4' }">영화</c:if>
								<c:if test="${item.GENRE_CD == '5' }">방송</c:if>
								<c:if test="${item.GENRE_CD == '6' }">뉴스</c:if>
								<c:if test="${item.GENRE_CD == '7' }">미술</c:if>
								<c:if test="${item.GENRE_CD == '8' }">이미지</c:if>
								<c:if test="${item.GENRE_CD == '99' }">기타</c:if>
							</td>
							<td>${item.WORKS_TITLE}</td>
							<td>${item.WORKS_SUB_TITLE}</td>
							<td>${item.CRT_YEAR}</td>
							<td>		
								<c:if test="${item.COMM_MGNT_CD == '0' }">없음</c:if>
								<c:if test="${item.COMM_MGNT_CD != '0' }"><c:out value="${item.COMM_MGNT_CD}"/></c:if>
								</td>
							<td>${item.RGST_ORGN_NAME}</td>
						
							
						</tr>
					</c:forEach>
					<c:if test="${empty uploadList}">
						<tr><td colspan="10">조회된 데이터가 없습니다.</td></tr>
					</c:if>
				</tbody>
			</table>
	</div>
	<!-- //table_area -->
	<div id="pagination" class="pagination">
		<ul><ui:pagination paginationInfo="${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" /></ul>
	</div>
	
</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="js" value="report" />
</jsp:include>
