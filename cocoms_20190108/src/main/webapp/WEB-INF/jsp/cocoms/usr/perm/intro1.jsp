<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="type" value="notice" />
	<jsp:param name="nav1" value="알림마당" />
	<jsp:param name="nav2" value="이용안내" />
	<jsp:param name="nav3" value="공인인증서" />
</jsp:include>
	
	<div  class="container">
		<div class="menu_aside">
			<h2 class="tit_bar pink mt">이용안내</h2>
			<ul>
				<li class="active"><a href="<c:url value='/perm/intro1' />">공인인증서</a></li>
				<li><a href="<c:url value='/perm/intro2' />">관련저작권법</a></li>
				<li><a href="<c:url value='/faq' />">자주묻는질문(FAQ)</a></li>
				<%-- <li><a href="<c:url value='/inquiry/new' />">1:1문의</a></li> --%>
				<li><a href="<c:url value='/perm/downloads' />">문서양식 자료실</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice">
			<h2 class="tit_bar mt">공인인증서</h2>
			<div class="box_top_common">
				<p class="txt_center">저작권위탁관리업 신청용 <strong>공인인증서 안내</strong>입니다.</p>
			</div>
			<!-- //box_top_common -->
			<div class="ment">
				<p class="tit_box">저작권위탁관리업(신탁, 대리중개)을 신청할때에는 실명인증과 서명을 위한 공인인증서가 필요합니다. </p>
				<p class="tit_box">공인인증서란?</p>
				<ul class="list_icon">
					<li>전자서명이란 전자문서를 작성한 사람의 신원과 전자문서의 변경여부를 확인할 수 있도록 하는 고유정보를 의미하며 전자문서 의인감과 같은 역할을 합니다.</li>
					<li>전자서명은 공개키 암호기술을 이용한 것으로 전자서명생성키(개인키)와 전자서명검증키(공개키)로 구성되는 하나의 키쌍으로 이루어집니다.</li>
					<li>전자서명을 안전하고 원활하게 사용하기 위해서 인증서를 사용하며, 전자서명법에 의해 지정된 신뢰할 수있는 제3의 기관인 공인인증기관에서 발급한 인증서를 공인인증서라고 합니다. 즉 공인인증서란 공인인증기관이 발행한 사이버 거래용 인감증명서라고 할 수 있습니다.</li>
					<li>공인인증서 내에는 가입자의 전자서명검증키, 일련번호, 소유자이름, 유효기간 등의 정보를 포함한 일련의 데이터를 포함하고 있습니다.</li>
					<li>전자거래시 공인인증서를 사용하면 신원확인, 문서의 위·변조, 거래사실의 부인 방지 등의 효과를 얻을 수 있습니다.</li>
				</ul>
				<p class="txt_b">※ 금융결제원(<a href="http://www.yessign.or.kr">http://www.yessign.or.kr</a>) 홈페이지에 접속하셔서 인증서 발급절차 안내를 참고하시기 바랍니다.</p>
			</div>
			<!-- //ment -->		
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
</jsp:include>
