<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="회원정보 수정" />
</jsp:include>
	
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li class="active"><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<%-- <li><a href="<c:url value="/qna" />">1:1문의 내역</a></li> --%>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage">
			<h2 class="tit_bar mt">회원정보 수정</h2>
			<h3 class="tit">
				회원정보
			</h3>
			
			<form id="form" action="<c:url value="/mypage" />" method="POST">
			<%-- <input type="hidden" name="loginId" value="${login.loginId}" /> --%>
			<%-- <input type="hidden" name="memberSeqNo" value="${member.memberSeqNo}" /> --%>

			<div class="table_area list_top">
				<table>
					<caption>회원정보</caption>
					<colgroup>
						<col style="width:200px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>가입일자</th>
							<td>
								<c:out value="${login.regDateStr}" />
							</td>
						</tr>
						<tr>
							<th>회원구분</th>
							<td>
								<c:if test="${member.coGubunCd eq '1'}">
									개인회원
								</c:if>
								<c:if test="${member.coGubunCd eq '2'}">
									사업자회원
								</c:if>
							</td>
						</tr>
						<tr>
							<th>이름 <c:if test="${member.coGubunCd eq '2'}">(담당자)</c:if></th>
							<td>
								<input type="text" name="ceoName" class="input_style1" value="${member.ceoName}" title="이름" disabled />
							</td>
						</tr>
						<tr>
							<th>아이디</th>
							<td>
								<c:out value="${login.loginId}" />
							</td>
						</tr>
						<tr>
							<th>현재비밀번호</th>
							<td>
								<input type="password" name="pwd" class="input_style1 pw" maxlength="20" placeholder="영문/숫자/특수문자 8~20자" />
								<p style="color: orange;"><c:out value="${message}" /></p>
							</td>
						</tr>
						<tr>
							<th>새 비밀번호</th>
							<td><input type="password" name="newPwd" class="input_style1 pw" maxlength="13" placeholder="영문/숫자/특수문자 8~20자" /></td>
						</tr>
						<tr>
							<th>새 비밀번호 확인</th>
							<td><input type="password" name="newPwd2" class="input_style1 pw" maxlength="13" placeholder="영문/숫자/특수문자 8~20자" /></td>
						</tr>
						<tr>
							<th>휴대전화번호</th>
							<td>
								<select class="select_style disabled" title="전화번호 앞자리" disabled>
									<option value="010" <c:if test="${member.ceoMobile1 eq '010'}">selected</c:if>>010</option> 
									<option value="011" <c:if test="${member.ceoMobile1 eq '011'}">selected</c:if>>011</option> 
									<option value="016" <c:if test="${member.ceoMobile1 eq '016'}">selected</c:if>>016</option> 
									<option value="017" <c:if test="${member.ceoMobile1 eq '017'}">selected</c:if>>017</option> 
									<option value="018" <c:if test="${member.ceoMobile1 eq '018'}">selected</c:if>>018</option> 
									<option value="019" <c:if test="${member.ceoMobile1 eq '019'}">selected</c:if>>019</option> 
								</select>
								<span class="txt_area">-</span>
								<input type="text" class="input_style1 phone" value="${member.ceoMobile2}" title="전화번호 중간자리" disabled />
								<span class="txt_area">-</span>
								<input type="text" class="input_style1 phone" value="${member.ceoMobile3}" title="전화번호 뒷자리" disabled />
							</td>
						</tr>
						<tr>
							<th>이메일 주소</th>
							<td>
								<input type="text" class="input_style1 email" value="${member.ceoEmail1}" title="이메일 주소 앞자리" disabled />
								<span class="txt_area">@</span>
								<input type="text" class="input_style1 email" value="${member.ceoEmail2}" title="이메일 주소 뒷자리" disabled />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<h3 class="tit">
				사업자정보
			</h3>
			<div class="table_area list_top">
				<table>
					<caption>사업자정보</caption>
					<colgroup>
						<col style="width:200px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>회사명</th>
							<td>
								<c:out value="${member.coName}" />
							</td>
						</tr>
						<tr>
							<th>사업자등록번호</th>
							<td>
								<c:out value="${member.coNoStr}" />
							</td>
						</tr>
						<tr>
							<th>대표자명</th>
							<td>
								<c:out value="${member.ceoName}" />
							</td>
						</tr>
						<tr>
							<th>법인등록번호</th>
							<td>
								<c:out value="${member.bubNoStr}" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="button_area">
				<a href="" id="submit" class="btn btn_style2">수정</a>
				<a href="<c:url value="/mypage" />" class="btn btn_style1">취소</a>
				<a href="" id="remove" class="btn btn_style1">회원탈퇴</a>
			</div>
			
			</form>
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 