<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
</jsp:include>
	
	<div class="container">
	<h2 class="tit_bar blue mt">회원정보</h2>
		<div class="login_content login_content_n">
			<div class="inner">
				<div class="cell cell2">
					<h3 class="tit">회원정보 수정</h3>
					<p id="message" style="color: red; margin-bottom: 20px; margin-left: 19px;">${message}</p>
					<form id="form_0" method="post" action="<c:url value="/mypage/check" />">   
						<div class="input_login">
							
							<input type="password" autocomplete="off" name="pwd" class="input_style1" maxlength="20" placeholder="비밀번호" />
							<input type="button" id="mypage_user" class="btn_style2" style="line-height: 41px; height:41px" value="확인">
						</div>
						
					</form>
				
				</div>
				<!-- //cell -->	
			</div>
			<!-- //inner -->	
		
			<!-- //box_top_common -->
		</div>
		<!-- //login_content -->
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 