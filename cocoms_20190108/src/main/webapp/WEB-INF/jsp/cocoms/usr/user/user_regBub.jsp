<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="user" />
</jsp:include>

<div class="container">
	<h2 class="tit_bar blue mt">회원가입</h2>
	<div class="join_content step_1">
		<div class="list_step">
			<ul>
				<li class="active">
					<div class="wrap">
						<p>STEP 01</p>
						<strong>회원유형선택 및 본인인증</strong>
					</div>
				</li>
				<li>
					<div class="wrap">
						<p>STEP 02</p>
						<strong>이용약관 동의</strong>
					</div>
				</li>
				<li>
					<div class="wrap">
						<p>STEP 03</p>
						<strong>회원정보 입력</strong>
					</div>
				</li>
				<li>
					<div class="wrap">
						<p>STEP 04</p>
						<strong>회원가입 완료</strong>
					</div>
				</li>
			</ul>
		</div>
		<!-- //list_step -->
		<div class="inner">
			<form id="form1" method="post" action="?step=2">
				<input type="hidden" name="certificate" />
				<input type="hidden" name="userDn" />
				<input type="hidden" name="certSn" />
				<input type="hidden" name="certStartDate" />
				<input type="hidden" name="certEndDate" />
				<input type="hidden" name="bubNo" value="2"/>
				<c:if test="${member.coGubunCd eq '1'}">
					<c:set var="checkCoGubunCd1" value="checked" />
					<c:set var="checkCoGubunCd2" value="disabled" />
		
					<c:set var="displayCoGubunCd1" value="block" />
					<c:set var="displayCoGubunCd2" value="none" />
				</c:if>
				<c:if test="${member.coGubunCd eq '2'}">
					<c:set var="checkCoGubunCd2" value="checked" />
					<c:set var="checkCoGubunCd1" value="disabled" />
					<c:set var="displayCoGubunCd1" value="none" />
					<c:set var="displayCoGubunCd2" value="block" />
				</c:if>

				<div class="type">
					<ul>
						<li>
							<div class="icon">
								<img src="<c:url value='/img/icon_join_normal.png' />" alt="" />
							</div>
							<div class="btm_form_box">
								<input type="radio" id="co_gubun_cd1" name="coGubunCd" value="1" ${checkCoGubunCd1} />
								<label for="co_gubun_cd1">개인회원</label>
							</div>
						</li>
						<li>
							<div class="icon">
								<img src="<c:url value='/img/icon_join_corporate.png' />" alt="" />
							</div>
							<div class="btm_form_box">
								<input type="radio" id="co_gubun_cd2" name="coGubunCd" value="2" ${checkCoGubunCd2} />
								<label for="co_gubun_cd2">사업자회원<span>(개인/법인)</span></label>
							</div>
						</li>
					</ul>
				</div>
				<!-- //type -->
				<div id="layer1" class="input_area" style="display: ${displayCoGubunCd1};">
					<p class="tit">성명</p>
					<input type="text" name="ceoName" class="input_style1" value="${member.ceoName}" maxlength="30" />
					<p class="tit ml">주민등록번호</p>
					<input type="text" name="ceoRegNo1" class="input_style1" value="${member.ceoRegNo1}" maxlength="6" />
					<span>-</span>
					<input type="password" name="ceoRegNo2" class="input_style1" value="" maxlength="7" />
				</div>
				<div id="layer2" class="input_area" style="display: ${displayCoGubunCd2};">
					<p class="tit">회사명</p>
					<input type="text" name="coName" class="input_style1" maxlength="100" value="${member.coName}" />
					<p class="tit ml">법인등록번호</p>
					<input type="text" name="bubNo1" class="input_style1" value="${member.bubNo1}" maxlength="6" /><span>-</span><input type="text" name="bubNo2" class="input_style1" value="${member.bubNo2}" maxlength="7" />
				</div>
				<!-- //input_area -->
				<div class="ment_b step_1">
					<h3 class="tit">본인인증 수단 안내</h3>
					<div class="box_top_common">
						<div class="cell pb">
							<p class="tit">개인회원</p>
							<ul>
								<li>주민등록번호로 발급한 개인명의의 공인인증서</li>
							</ul>
						</div>
						<div class="cell">
							<p class="tit">사업자회원</p>
							<ul>
								<li>사업자등록번호로 발급한 공인인증서</li>
							</ul>
						</div>
						<ul class="txt_blue">
							<li>※ 공인인증서 인증에 사용된 인증서는 바로 등록됩니다.</li>
							<li>※ 로그인 및 서비스 이용 시 등록하신 공인인증서가 사용됩니다.</li>
						</ul>
					</div>
					<!-- //box_top_common -->
				</div>
				<!-- //ment_b -->
				<div class="button_area">
					<button class="btn btn_style2">다음단계</button>
				</div>
			</form>
		</div>

	</div>
	<!-- //join_content -->
</div>
<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="user" />
	<jsp:param name="cert" value="true" />
</jsp:include>
