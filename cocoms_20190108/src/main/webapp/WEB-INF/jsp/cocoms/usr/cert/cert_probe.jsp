<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">
</head>
<body  >

<div  class="pop_common">
	<div class="inner">
		<h2 class="tit">인터넷 증명발급 원본조회</h2>
		<div class="cont">
			<div class="table_area list list_type1">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:109px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>(구) 확인번호</th>
							<td style="padding: 6px 13px 0px;">
								<form id="form_old" method="POST">
								<input type="hidden" name="mode" value="old" />
								
								<div class="box_input">
									<input type="text" name="key1" class="input_style1 w73" maxlength="4" title="구 확인번호 첫번째자리" /> 
									- <input type="text" name="key2" class="input_style1 w73" maxlength="4" title="구 확인번호 두번째자리" /> 
									- <input type="text" name="key3" class="input_style1 w73" maxlength="4" title="구 확인번호 세번째자리" /> 
									- <input type="text" name="key4" class="input_style1 w73" maxlength="4" title="구 확인번호 네번째자리" />
									<p>(예 : A111-B11R-C1P1-DEWW)</p>
									<a href="" id="check_old" class="btn_style2">확인</a>
								</div>
								</form>
							</td>
						</tr>
						<tr>
							<th>(신) 확인번호</th>
							<td style="padding: 6px 13px 0px;">
								<form id="form_new" method="POST">
								<input type="hidden" name="mode" value="new" />
								
								<div class="box_input">
									<input type="text" name="key1" class="input_style1 w75" maxlength="8" title="신 확인번호 첫번째자리" /> 
									- <input type="text" name="key2" class="input_style1 w58" maxlength="4" title="신 확인번호 두번째자리" /> 
									- <input type="text" name="key3" class="input_style1 w58" maxlength="4" title="신 확인번호 세번째자리" /> 
									- <input type="text" name="key4" class="input_style1 w58" maxlength="4" title="신 확인번호 네번째자리" /> 
									- <input type="text" name="key5" class="input_style1 w91" maxlength="12" title="신 확인번호 다섯번째자리" />
									<p>(예 : c11bb11f-1111-1c1f-1cf1-11c1f111aedb)</p>
									<a href="" id="check_new" class="btn_style2">확인</a>
								</div>
								</form>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="txt_area" style="padding: 15px 0 6px;">
				<p class="tit_box">인터넷 증명발급 시스템에서 발급받으신 증명서의 원본 확인 페이지입니다.</p>
				<div class="in">
					<p><span class="txt_blue">[방법 1]</span> 증명서 좌측 상단의 확인번호를 입력 후 확인버튼을 클릭합니다.</p>
					<p><span class="txt_blue">[방법 2]</span> 증명서 하단의 바코드를 이용하여 원본확인이 가능합니다.</p>
					<p>아래의 “스캐너용 원본확인 프로그램 다운로드 및 실행” 메뉴를 클릭합니다.</p>
					<p class="fs12 txt_blue">※ 바코드인식 프로그램을 다운받아서 설치하세요.</p>
					<p class="fs12 txt_blue">※ 고객님께 스캐너(300dpi)가 있는 경우 가능합니다.</p>
					<a href="<c:url value="/docs//rxBarcode.exe" />" class="btn_style1" style="margin:12px 0 0px">스캐너용 원본확인 프로그램 다운로드 및 실행</a>
				</div>
			</div>
			<!-- //txt_area -->
			<div class="txt_area">
				<p class="tit_box">원본확인 프로그램이 자동설치되도록 되어 있으나, 사용자 PC의 설정 <span class="txt_blue">(예, 바이러스 백신의 실시간 감시기능,보안설정, 
인터넷 연결상태 등)</span>의 영향으로 자동설치되지 않는 현상이 발생할 수 있습니다. <span class="txt_blue">(MS Active X의 전반적인 오류현상)</span>
아래의 증명서 발급안내 페이지의 FAQ를 참고하시기 바랍니다.
				</p>
				<div class="in"><a href="" id="info1" class="btn_style1"  style="margin:12px 0 0px">증명서 발급안내</a></div>
				
			</div>
			<!-- //txt_area -->
		</div>
	</div>
</div>
<!-- //pop_common -->

<script src="<c:url value='/js/jquery-1.10.2.min.js' />"></script>
<script src="<c:url value='/js/jquery.bxslider.js' />"></script>
<script src="<c:url value='/js/script.js' />"></script>

<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script src="<c:url value='/js/usr/cert.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</body>
</html>
