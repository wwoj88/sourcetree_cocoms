<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value=""/>
			<jsp:param name="type" value="sub" />
			<jsp:param name="nav1" value="저작권대리중개업 " />
			<jsp:param name="nav2" value="변경" />
		</jsp:include>	
	<!-- //step_sub -->
	<div class="container">
		<div class="box_top_common">
			<ul>
				<li>법정 민원을 신청할 때 전자우편 및 휴대폰 번호를 기재하시면 민원접수 및 처리결과를 전자우편 및 휴대폰으로 알려 드립니다.</li>
				<li>온라인 첨부가능 표시가 있는 구비서류는 컴퓨터 파일로 첨부가 가능한 서류이며, 스캔한 파일 온라인 첨부가능 표시가 있는 구비서류는 원본 서류를 스캔하여 컴퓨터 파일로 첨부가 가능한 서류입니다.</li>
				<li>온라인으로 첨부할 수 없는 서류는 방문 및 우편으로 제출할 수 있습니다. </li>
			</ul>
		</div>
		<!-- //box_top_common -->
		<h2 class="tit_bar blue">대리중개업신고 변경 신청하기</h2>
		<div class="form_apply">
			<div class="aside">
				<div class="table_area">
					<table>
						<caption> 회원정보</caption>
						<thead>
							<tr> 	 	 	
								<th> 회원정보</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>
									 회원구분 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<div class="btm_form_box">
										<input type="checkbox">
										<label for="checkbox1">개인사업자</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="checkbox2">
										<label for="checkbox2">법인사업자</label> 
									</div>
								</td>
							</tr>
							<tr>
								<th>
									 사업자 등록번호
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="123" title="사업자번호 첫번째자리">
									<span>-</span>
									<input type="text" class="input_style1 num1 disabled" value="456" title="사업자번호 두째자리">
									<span>-</span>
									<input type="text" class="input_style1 num2 disabled" value="789" title="사업자번호 세째자리">
								</td>
							</tr>
							<tr>
								<th>
									주소 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 address1" placeholder="" title="주소입력칸 1"><a href="" class="btn_style1">주소찾기</a>
									<input type="text" class="input_style1 margin" placeholder="" title="주소입력칸 2">
									<input type="text" class="input_style1" placeholder="" title="주소입력칸 3">
								</td>
							</tr>
							<tr>
								<th>
									전화번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<select class="select_style" title="전화번호 앞자리">
										<option value="" selected="selected">02</option>
										
									</select>
									<span>-</span>
									<input type="text" class="input_style1 phone" placeholder="" title="전화번호 중간자리">
									<span>-</span>
									<input type="text" class="input_style1 phone" placeholder="" title="전화번호 뒷자리">
								</td>
							</tr>
							<tr>
								<th>
									팩스번호 
								</th>
							</tr>
							<tr>
								<td>
									<select class="select_style" title="전화번호 앞자리">
										<option value="" selected="selected">02</option>
										
									</select>
									<span>-</span>
									<input type="text" class="input_style1 phone" placeholder="" title="전화번호 중간자리">
									<span>-</span>
									<input type="text" class="input_style1 phone" placeholder="" title="전화번호 뒷자리">
								</td>
							</tr>
							<tr>
								<th>
									홈페이지 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1" placeholder="" title="홈페이지 주소입력칸">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="table_area">
					<table>
						<caption> 대표자정보</caption>
						<thead>
							<tr> 	 	 	
								<th>대표자정보</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>
									 대표자명 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1" placeholder="" title="대표자명">
								</td>
							</tr>
							<tr>
								<th>
									 주민등록번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 resident" placeholder="" title="주민등록번호 앞자리"><span>-</span><input type="text" class="input_style1 resident" placeholder="" title="주민등록번호 뒷자리">
								</td>
							</tr>
							<tr>
								<th>
									주소 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 address1" placeholder="" title="주소입력칸 1"><a href="" class="btn_style1">주소찾기</a>
									<input type="text" class="input_style1 margin" placeholder="" title="주소입력칸 2">
									<input type="text" class="input_style1" placeholder="" title="주소입력칸 3">
								</td>
							</tr>
							<tr>
								<th>
									휴대전화번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<select class="select_style" title="전화번호 앞자리">
										<option value="" selected="selected">010</option>
										
									</select>
									<span>-</span>
									<input type="text" class="input_style1 phone" placeholder="" title="전화번호 중간자리">
									<span>-</span>
									<input type="text" class="input_style1 phone" placeholder="" title="전화번호 마지막자리">
								</td>
							</tr>
							<tr>
								<th>
									이메일

								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 resident" placeholder="" title="이메일 주소 앞자리"><span>@</span><input type="text" class="input_style1 resident" placeholder="" title="이메일 주소 뒷자리">
								</td>
							</tr>
							<tr>
								<th>
									 SMS수신동의 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<div class="btm_form_box">
										<input type="radio" name="agree" id="radio1" checked>
										<label for="radio1">동의</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" name="agree" id="radio2">
										<label for="radio2">동의안함</label> 
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
			</div>
			<!-- //aside -->
			<div class="cont">
				<div class="table_area">
					<table>
						<caption>신고서 미리보기</caption>
						<thead>
							<tr> 	 	 	
								<th>신고서 미리보기</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- //table_area -->
				<div class="declaration_preview">
					<div class="inner">
						<div class="preview">
							<p class="num_top txt_r">처리기간 : 4일</p>
							<h1 class="tit">저작권대리중개업 변경신고서</h1>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:129px" />
										<col style="width:222px" />
										<col style="width:158px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="3">신고인</th>
											<th>성명<p>(단체 또는 법인명)</p></th>
											<td>(주)아이티팝</td>
											<th>생년월일<p>(법인/사업자등록번호)</p></th>
											<td>580-03-00111</td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td>031-123-4567</td>
											<th>팩스번호</th>
											<td>031-123-4568</td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">(123486) 경기도 성남시 분당구 판교로 255번길 38, 111</td>
										</tr>
										<tr>
											<th class="bg" rowspan="3">대표자<p>(법인 또는<br>단체에 한함)</p></th>
											<th>성명</th>
											<td>홍길동</td>
											<th>생년월일</th>
											<td>1981.01.10</td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td>010-1234-5678</td>
											<th>이메일 주소</th>
											<td>031-123-4568</td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">(123486) 경기도 성남시 분당구 판교로 255번길 38, 111</td>
										</tr>
									</tbody>
								</table>
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:151px" />
										<col style="width:222px" />
										<col style="width:222px" />
										<col style="width:222px" />
									</colgroup>
									<tbody>
										<tr class="border_top0">
											<th class="bg" rowspan="2">변경사항</th>
											<th>변경 전<span>(을)</span></th>
											<th>변경 후<span>(으로)</span></th>
											<th>변경사유</th>
										</tr>
										<tr>
											<td class="txt_l pl12">
											대표자성명 :홍길동 <br>
											대표자주민등록번호 : 12345-67890<br>
											대표자이메일주소 : abcd@naver.com 

											</td>
											<td class="txt_l pl12">
											대표자성명 :홍길동 <br>
											대표자주민등록번호 : 12345-67890<br>
											대표자이메일주소 : abcd@naver.com 

											</td>
											<td class="txt_l pl12">
											임원 개시 (2018.7.27.~2021.7.26)로 
											대표자 변경하고자 함.

											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<p class="txt1"><strong>[저작권법 시행령]</strong> 제 105조 제 3항에 따라 위와 같이 신고합니다.</p>
							<div class="sign">
								<p>2018년 07월 03일</p>
								<p>신청인   홍길동   (서명 또는 인)</p>
							</div>
							<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:auto" />
										<col style="width:266px" />
										<col style="width:134px" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="2">구비서류</th>
											<th>민원인 제출서류</th>
											<th>담당공무원 확인사항</th>
											<th>수수료</th>
										</tr>
										<tr>
											<td>
												신고증

											</td>
											<td>변경사항을 증명하는 서류 1부</td>
											<td>3,000원</td>
										</tr>
										<tr>
											<th class="bg">첨부파일</th>
											<td colspan="3" class="txt_l">
												<div class="file_change">
													<a href=""><p class="txt1">변경신고신청 첨부1</p><p class="txt2">1. 대리중개업 업무규정</p><p class="txt3">(2018.04.30)</p></a>
													<a href=""><p class="txt1">변경신고신청 첨부2</p><p class="txt2">2. 신고인(단체 또는 법인의 대표자 및 임원)의 이력</p><p class="txt3">(2018.09.14)</p></a>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
						</div>
						<!-- //preview -->
					</div>
					<div class="btn_area">
						<a href="" class="btn btn_style1">수정</a>
						<a id="charge" class="btn btn_style2">결제</a>
					</div>
				</div>
				<!-- //cont -->
			</div>
			<!-- //cont -->
		</div>
		<!-- //form_apply -->
	</div>
	
<div id="chargeForm" class="pop_common w500" style="display: none;">
	<div class="inner">
		<h2 class="tit txt_l">저작권위탁관리업시스템 결제정보</h2>
		<a href="javascript:;" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
		<div class="cont line">
			<form>
				<div class="table_area list list_type1">
					<table>
						<caption></caption>
						<colgroup>
							<col style="width:109px" />
							<col style="width:auto" />
						</colgroup>
						<tbody>
							<tr>
								<th>결제업무</th>
								<td>대리중개업 신고</td>
							</tr>
							<tr>
								<th>결제금액</th>
								<td>
									<c:choose>
										<c:when test="${member.procType eq 'sub_regist'}">
											4,160원
											<input type="hidden" id="cappTot" value="4000" />
											<input type="hidden" id="pgFee" value="160" />
											<input type="hidden" id="remark" value="대리중개업 신고" />
											<input type="hidden" id="conditionCd" value="1" />
											<input type="hidden" id="statCd" value="1" />
										</c:when>
										<c:when test="${member.procType eq 'trust_regist'}">
											9,360원
											<input type="hidden" id="cappTot" value="9000" />
											<input type="hidden" id="pgFee" value="360" />
											<input type="hidden" id="remark" value="신탁업 허가" />
											<input type="hidden" id="conditionCd" value="2" />
											<input type="hidden" id="statCd" value="1" />
										</c:when>
										<c:when test="${member.procType eq 'sub_modify'}">
											2,090원
											<input type="hidden" id="cappTot" value="2000" />
											<input type="hidden" id="pgFee" value="90" />
											<!-- 
											<input type="hidden" id="remark" value="대리중개업 신고 변경" />
											 -->
											<input type="hidden" id="remark" value="대리중개업 신고" />
											<input type="hidden" id="conditionCd" value="1" />
											<input type="hidden" id="statCd" value="3" />
										</c:when>
										<c:when test="${member.procType eq 'trust_modify'}">
											3,120원
											<input type="hidden" id="cappTot" value="3000" />
											<input type="hidden" id="pgFee" value="120" />
											<!-- 
											<input type="hidden" id="remark" value="신탁업 허가 변경" />
											 -->
											<input type="hidden" id="remark" value="신탁업 허가" />
											<input type="hidden" id="conditionCd" value="2" />
											<input type="hidden" id="statCd" value="3" />
										</c:when>
									</c:choose>
								</td>
							</tr>
							<tr>
								<th>결제방법</th>
								<td>
									<div class="btm_form_box">
										<input type="radio" id="payMthdCd0402" name="payMthdCd" value="0402" checked />
										<label for="payMthdCd0402">카드결제</label>
									</div>
									<%-- 
									<div class="btm_form_box">
										<input type="radio" id="payMthdCd0407" name="payMthdCd" value="0407" />
										<label for="payMthdCd0407">카드포인트</label>
									</div>
									--%>
									<div class="btm_form_box">
										<input type="radio" id="payMthdCd0404" name="payMthdCd" value="0404" />
										<label for="payMthdCd0404">휴대폰</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<p class="txt_red txt_r">※ 휴대폰결제는 개인명의로된 휴대폰만 결제진행이 가능합니다.</p>
			</form>
			<div class="btn_area">
				<a href="javascript:;" name="popup_cancel" class="btn_style1">취소</a>
				<a href="javascript:;" id="submit" class="btn_style2">결제</a>
			</div>
			<div class="logt_b"><img src="<c:url value="img/logo_pop_1.png" />" alt="" /></div>
		</div>
	</div>
</div>
<!-- //pop_common -->
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="js" value="appl" />
	<jsp:param name="cert" value="true" />
</jsp:include>
