<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="login" />
	<jsp:param name="nav1" value="로그인" />
	<jsp:param name="nav2" value="아이디 찾기" />
</jsp:include>

	<div class="container">
		<h2 class="tit_bar blue mt">아이디 찾기</h2>
		<div class="join_content">
			<div class="list_step_id">
				<!--  
				<ul>
					<li>내 명의로 가입된 휴대폰 인증</li>
					<li>내 정보에 등록된 휴대폰으로 찾기</li>
					<li>이메일로 찾기</li>
					<li class="active">공인인증서 인증</li>
					<li>사업자등록번호로 찾기</li>
				</ul>
				-->
			</div>
			<!-- //list_step -->	
			<form id="formFindId" action="<c:url value="/find/id" />" method="POST">
			<input type="hidden" name="certSn" value="" />
			
			<div class="table_area list_top id_step">
				<table>
					<caption>아이디 찾기</caption>
					<colgroup>
						<col style="width:234px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>회원구분</th>
							<td>
								<div class="btm_form_box">
									<input type="radio" id="coGubunCd1" name="coGubunCd" value="1" checked />
									<label for="coGubunCd1">일반회원</label> 
								</div>
								<div class="btm_form_box">
									<input type="radio" id="coGubunCd2" name="coGubunCd" value="2" />
									<label for="coGubunCd2">사업자회원</label> 
								</div>
							</td>
						</tr>
						<tr id="ceoRegNo">
							<th style="border-bottom: 1px solid #000;">주민등록번호</th>
							<td style="border-bottom: 1px solid #000;">
								<input type="text" name="ceoRegNo1" class="input_style1 w148" maxlength="6" placeholder="" title="주민등록번호 앞자리" />
								<span>-</span>
								<input type="password" name="ceoRegNo2" class="input_style1 w148" maxlength="7" placeholder="" title="주민등록번호 뒷자리" />
							</td>
						</tr>
						<tr id="coNo" style="display: none;">
							<th>사업자등록번호</th>
							<td>
								<input type="text" name="coNo1" class="input_style1 w148" maxlength="3" placeholder="" title="사업자번호 첫번째자리" />
								<span>-</span>
								<input type="text" name="coNo2" class="input_style1 w148" maxlength="2" placeholder="" title="사업자번호 두째자리" />
								<span>-</span>
								<input type="password" name="coNo3" class="input_style1 w148" maxlength="5" placeholder="" title="사업자번호 세째자리" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="button_area">
				<a href="" id="submit" class="btn btn_style2">공인인증서 인증하기</a>
			</div>
			</form>
		</div>
		<!-- //join_content -->	
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="login" />
	<jsp:param name="cert" value="true" />
</jsp:include>
