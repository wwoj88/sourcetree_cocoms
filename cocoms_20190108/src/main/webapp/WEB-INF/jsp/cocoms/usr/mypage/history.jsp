<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="신고서 발급내역" />
</jsp:include>
	
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<%-- <li><a href="<c:url value="/qna" />">1:1문의 내역</a></li> --%>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li class="active"><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage mypage_2">
			<h2 class="tit_bar mt">신고서 발급내역</h2>
			
			<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
			<div class="box_top_common date">
				<p class="txt_dot">날짜 선택</p>
				<input type="text" name="searchFromDate"  class="event-date input_style1"  value="${data.searchFromDate}" />
				<input type="text" name="searchToDate" class="event-date2 input_style1" value="${data.searchToDate}" />
				<a href="" id="search" class="btn_style2">검색</a>
			</div>
			</form>
			<!-- //box_top_common -->
			<div class="total_top_common">
				<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
			</div>
			<div class="table_area list board">
				<table>
					<caption>결제내역</caption>
					<colgroup>
						<col style="width:67px" />
						<col style="width:154px" />
						<col style="width:auto" />
						<col style="width:281px" />
						<col style="width:161px" />
					</colgroup>
					<thead>
						<tr> 	 	 	
							<th>번호</th>
							<th>신청일자</th>
							<th>신청내용</th>
							<th>처리상태</th>
							<th>처리일자</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr>
								<td><c:out value="${start - status.index}" /></td>
								<td><c:out value="${item.regDateStr}" /></td>
								<td>
									<c:choose>
										<c:when test="${item.docType eq '1'}">저작권대리중개업 신고서</c:when>
										<c:when test="${item.docType eq '2'}">저작권중개업 신고증</c:when>
										<c:when test="${item.docType eq '3'}">저작권중개업 변경신고서</c:when>
										<c:when test="${item.docType eq '4'}">저작권중개업 신고증</c:when>
										<c:when test="${item.docType eq '5'}">저작권신탁관리업 신고서</c:when>
										<c:when test="${item.docType eq '6'}">저작권신탁관리업 신고증</c:when>
										<c:when test="${item.docType eq '7'}">저작권신탁관리업 허가증 변경교부 신청서</c:when>
										<c:when test="${item.docType eq '8'}">저작권신탁관리업 변경교부 허가증</c:when>
									</c:choose>
									<%-- <a href="" name="showPopup" class="btn btn_style1 cancel" data-appl-seq-no="${item.applSeqNo}">발급증 보기</a> --%>
								</td>
								<td><strong class="receipt">발급완료</strong></td>
								<td><c:out value="${item.regDateStr}" /></td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(list) eq '0'}">
							<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
						</c:if>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="pagination">
				<ul>
		       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
				</ul>
			</div>
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 