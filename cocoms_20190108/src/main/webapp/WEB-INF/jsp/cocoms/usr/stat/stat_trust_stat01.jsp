<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="업체현황" />
	<jsp:param name="nav2" value="업체조회" />
</jsp:include>
	
		<div class="table_area list_top list_type3">
			<jsp:include page="/WEB-INF/jsp/cocoms/comm/include/stats_search.jsp" flush="true" />
		</div>
		<!-- //table_area -->
		<h2 class="tit_c_s"><c:out value="${fromYear}" />년 ~ <c:out value="${toYear}" />년 
		<strong class="txt_blue">저작권 종류별 신탁사용료</strong></h2>
		<div class="table_area list list_type2 scroll">
			<div class="inner">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:168px" />
						<col style="width:138px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
						<col style="width:97px" />
					</colgroup>
					<thead>
						<tr> 	 
							<th>구분</th>
							<th>계약 (이용건수)</th>
							<th colspan="3">징수액 <span>(단위 : 천원)</span></th>
							<th colspan="3">분배액 <span>(단위 : 천원)</span></th>
							<th colspan="3">미분배액 <span>(단위 : 천원)</span></th>
							<th colspan="3">수수료 <span>(단위 : 천원)</span></th>
							<th colspan="3">수수료율 <span>(단위 : %)</span></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>-</td>
							<td>-</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
							<td>국내</td>
							<td>해외</td>
							<td>소계</td>
						</tr>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr class="">
								<td>
									<c:if test="${item.kind eq '1'}">
										<c:out value="${item.name}"  escapeXml="false"/>
									</c:if>
									<c:if test="${item.kind ne '1'}">
										기타(<c:out value="${item.name}" />)
									</c:if>
								</td>
								<td><fmt:formatNumber value="${item.cnt}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val11}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val12}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val13}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val21}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val22}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val23}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val31}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val32}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val33}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val41}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val42}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val43}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val51}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val52}" type="number" /></td>
								<td><fmt:formatNumber value="${item.val53}" type="number" /></td>
							</tr>
						</c:forEach>
						<c:if test="${fn:length(list) gt '1'}">
							<tr class="bg_total">
								<td>합계</td>
								<td><fmt:formatNumber value="${sum.cnt}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val11}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val12}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val13}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val21}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val22}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val23}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val31}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val32}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val33}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val41}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val42}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val43}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val51}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val52}" type="number" /></td>
								<td><fmt:formatNumber value="${sum.val53}" type="number" /></td>
							</tr>
						</c:if>
						<c:if test="${fn:length(list) eq '0'}">
							<tr><td colspan="20">조회된 데이터가 없습니다.</td></tr>
						</c:if>
					</tbody>
				</table>
			</div>
		</div>
		<!-- //table_area -->
		<div class="button_area mt25">
			<div class="right">
				<a href="<c:url value="/admin/status/stats/save?conditionCd=${conditionCd}&fromYear=${fromYear}&toYear=${toYear}&type=${type}" />" id="excel" class="btn btn_style2">엑셀출력</a>
				<%-- 
				<a href="" id="list" class="btn btn_style1">목록</a>
				--%>
			</div>
		</div>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_stat" />
	<jsp:param name="includes" value="stats_search" />
</jsp:include>
