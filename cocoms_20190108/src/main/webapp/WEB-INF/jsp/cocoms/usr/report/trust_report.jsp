<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="type" value="report" />
	<jsp:param name="nav1" value="실적보고" />
	<jsp:param name="nav2" value="신탁단체" />
</jsp:include>

	<div class="container">

		<div class="box_top_common">
			<ul>
				<li>아래 사항은 저작권법 제 108조 제 1항에 따라 요청 드리는 실적보고 내용입니다. </li>
			</ul>
		</div>
		<h2 class="tit_bar yellow">일반사항</h2>
		<div class="table_area list_top">
			<table>
				<caption> 일반사항</caption>
				<colgroup>
					<col style="width:155px" />
					<col style="width:428px" />
					<col style="width:155px" />
					<col style="width:428px" />
				</colgroup>
				<tbody>
					<tr>
						<th>업체명 / 형태</th>
						<td>
							<c:out value="${member.coName}" />
						</td>
						<th>대표자</th>
						<td>
							<c:out value="${member.ceoName}" />
						</td>
					</tr>
					<tr>
						<th>주소</th>
						<td>
							<c:out value="${member.trustZipcode}" />
							<c:out value="${member.trustSido}" />
							<c:out value="${member.trustGugun}" />
							<c:out value="${member.trustDong}" />
							<c:out value="${member.trustBunji}" />
							<c:out value="${member.trustDetailAddr}" />
						</td>
						<th>전화번호 / 팩스번호</th>
						<td>
							<c:out value="${member.trustTel}" />
							/
							<c:out value="${member.trustFax}" />
						</td>
					</tr>
					<tr>
						<th>허가 / 신고번호</th>
						<td>
							<%-- 
							<c:if test="${member.coGubunCd eq '1'}">
								개인
							</c:if>
							<c:if test="${member.coGubunCd eq '2'}">
								법인
							</c:if>
							<c:if test="${!empty member.trustRegDay}">
								/ <c:out value="${member.trustRegDayStr}" /> 
							</c:if>
							--%>
							<c:out value="${member.appNoStr}" />
						</td>
						<th>허가 / 신고일</th>
						<td>
							<c:out value="${member.appRegDateStr}" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		
		<h2 class="tit_bar yellow mt">연도별 실적 목록</h2>
		<div class="table_area list">
			<table>
				<caption> 매출액 목록</caption>
				<colgroup>
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
					<col style="width:25%" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>년도</th>
						<th>실적정보</th>
						<th>저작권료 징수분배 내역</th>
						<th>사업계획</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><a href="<c:url value="/reports/trust/years/${item.rptYear}" />"><c:out value="${item.rptYear}" /></a></td>
							<td>
								<c:if test="${item.trustInfoSeqNo gt 0}">
									<span title="fileDown" class="fileDown">파일 다운로드</span>
								</c:if>
							</td>
							<td>
								<c:if test="${item.trustCopyrightSeqNo gt 0 or item.trustExecSeqNo gt 0 or item.trustMakerSeqNo gt 0 or item.trustRoyaltySeqNo gt 0}">
									<span title="fileDown" class="fileDown">파일 다운로드</span>
								</c:if>
							</td>
							<td>
								<c:if test="${!empty item.businessGoal or !empty item.prop}">
									<span title="fileDown" class="fileDown">파일 다운로드</span>
									(<c:out value="${item.rptYear+1}" />)
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<div class="pagination">
		</div>		
		<div class="button_area">
			<c:if test="${reg_auth}">
				<a href="<c:url value="/report/trust/years/${reg_year}/new" />" class="btn btn_style2">등록</a>
			</c:if>
		</div>
			
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="js" value="report" />
</jsp:include>
