<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>



<c:choose>
	<c:when test="${type eq 'sub_regist'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value="" />
			<jsp:param name="type" value="sub" />
			<jsp:param name="nav1" value="저작권대리중개업" />
			<jsp:param name="nav2" value="신청하기" />
		</jsp:include>
	</c:when>
	<c:when test="${type eq 'trust_regist'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value="" />
			<jsp:param name="type" value="trust" />
			<jsp:param name="nav1" value="저작권신탁관리업 " />
			<jsp:param name="nav2" value="신청하기" />
		</jsp:include>
	</c:when>
	<c:when test="${type eq 'sub_modify'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value="" />
			<jsp:param name="type" value="sub" />
			<jsp:param name="nav1" value="저작권대리중개업 " />
			<jsp:param name="nav2" value="변경" />
		</jsp:include>
	</c:when>
	<c:when test="${type eq 'trust_modify'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value="" />
			<jsp:param name="type" value="trust" />
			<jsp:param name="nav1" value="저작권신탁관리업 " />
			<jsp:param name="nav2" value="변경" />
		</jsp:include>
	</c:when>
</c:choose>

<div class="container">
	<div class="box_top_common">
		<ul>
			<li>법정 민원을 신청할 때 전자우편 및 휴대폰 번호를 기재하시면 민원접수 및 처리결과를 전자우편 및 휴대폰으로 알려 드립니다.</li>
			<li>온라인 첨부가능 표시가 있는 구비서류는 컴퓨터 파일로 첨부가 가능한 서류이며, 스캔한 파일 온라인 첨부가능 표시가 있는 구비서류는 원본 서류를 스캔하여 컴퓨터 파일로 첨부가 가능한 서류입니다.</li>
			<li>온라인으로 첨부할 수 없는 서류는 방문 및 우편으로 제출할 수 있습니다.</li>
		</ul>
	</div>
	<!-- //box_top_common -->

	<c:choose>
		<c:when test="${type eq 'sub_regist'}">
			<h2 class="tit_bar blue">대리중개업신고 신청서</h2>
			<c:set var="memtype" value="checked" />
		</c:when>
		<c:when test="${type eq 'trust_regist'}">
			<h2 class="tit_bar green">신탁허가 신청하기</h2>
			<c:set var="memtype" value="checked" />
		</c:when>
		<c:when test="${type eq 'sub_modify'}">
			<h2 class="tit_bar blue">대리중개업신고 변경 신청하기</h2>
			<c:set var="memtype" value="disabled" />
		</c:when>
		<c:when test="${type eq 'trust_modify'}">
			<h2 class="tit_bar green">신탁허가 변경 신청하기</h2>
			<c:set var="memtype" value="disabled" />
		</c:when>
	</c:choose>

	<div class="form_apply">
		<div class="aside">
			<div class="table_area">
				<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
					<form id="form" method="post" action="<c:url value="/appl/sub" />">
						<input type="hidden" name="conditionCd" value="1" />
				</c:if>
				<c:if test="${type eq 'trust_regist' or type eq 'trust_modify'}">
					<form id="form" method="post" action="<c:url value="/appl/trust" />">
						<input type="hidden" name="conditionCd" value="2" />
				</c:if>
				<c:if test="${type eq 'sub_regist' or type eq 'trust_regist'}">
					<input type="hidden" name="statCd" value="1" />
				</c:if>
				<c:if test="${type eq 'sub_modify' or type eq 'trust_modify'}">
					<input type="hidden" name="statCd" value="3" />
				</c:if>


				<input type="hidden" name="procType" value="${type}" />

				<input type="hidden" id="alreadyConfirm" value="N" />

				<input type="hidden" id="conDetailCd" name="conDetailCd" value="${member.conDetailCd}" />

				<input type="hidden" id="trustSido" name="trustSido" value="${member.trustSido}" />
				<input type="hidden" id="trustGugun" name="trustGugun" value="${member.trustGugun}" />
				<input type="hidden" id="trustDong" name="trustDong" value="${member.trustDong}" />
				<input type="hidden" id="trustBunji" name="trustBunji" value="${member.trustBunji}" />
				<input type="hidden" id="ceoSido" name="ceoSido" value="${member.ceoSido}" />
				<input type="hidden" id="ceoGugun" name="ceoGugun" value="${member.ceoGugun}" />
				<input type="hidden" id="ceoDong" name="ceoDong" value="${member.ceoDong}" />
				<input type="hidden" id="ceoBunji" name="ceoBunji" value="${member.ceoBunji}" />

				<input type="hidden" name="file1Path" />
				<input type="hidden" name="file2Path" />
				<input type="hidden" name="file3Path" />
				<input type="hidden" name="file4Path" />
				<input type="hidden" name="file5Path" />
				
				<table>
					<caption>회원정보</caption>
					<thead>
						<tr>
							<th>회원정보</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>
								회원구분 <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<c:if test="${member.coGubunCd eq '1'}">
									<c:set var="coGubunCd1Checked" value="checked" />
								</c:if>
								<c:if test="${member.coGubunCd eq '2'}">
									<c:set var="coGubunCd2Checked" value="checked" />
								</c:if>
								<div class="btm_form_box">
									<input type="radio" id="coGubunCd1" name="coGubunCd" value="1" ${coGubunCd1Checked} } />
									<label for="coGubunCd1">개인사업자</label>
								</div>
								<div class="btm_form_box">
									<input type="radio" id="coGubunCd2" name="coGubunCd" value="2" ${coGubunCd2Checked} />
									<label for="coGubunCd2">법인사업자</label>
								</div>
							</td>
						</tr>
						<tr>
							<th>
								업체명 <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<input type="text" name="coName" class="input_style1" value="${member.coName}" maxlength="100" title="업체명" />
							</td>
						</tr>
						<c:if test="${member.coGubunCd eq '2'}">
							<tr>
								<th>
									사업자 등록번호 <span><strong class="txt_point"><span style='display: none' id="checkedstar">*</span></strong></span>
								</th>
							</tr>
						</c:if>
						<c:if test="${member.coGubunCd eq '1'}">
							<tr>
								<th>
									사업자 등록번호 <span><strong class="txt_point"><span style='display: none' id="checkedstar">*</span></strong></span>
								</th>
							</tr>
						</c:if>
						<tr>
							<td>
								<input type="text" name="coNo1" class="input_style1 num1" value="${member.coNo1}" maxlength="3" title="사업자번호 첫번째자리" />
								<span>-</span>
								<input type="text" name="coNo2" class="input_style1 num1" value="${member.coNo2}" maxlength="2" title="사업자번호 두째자리" />
								<span>-</span>
								<input type="text" name="coNo3" class="input_style1 num2" value="${member.coNo3}" maxlength="5" title="사업자번호 세째자리" />
							</td>
						</tr>


						<%-- <c:if test="${member.coGubunCd eq '2'}"> --%>
						<c:if test="${member.coGubunCd eq '2'}">
							<tr id="coGubunCd2_layer1">
						</c:if>
						<c:if test="${member.coGubunCd eq '1'}">
							<tr id="coGubunCd2_layer1" style='display: none'>
						</c:if>
						<th>
							법인 등록번호 <strong class="txt_point">*</strong>
						</th>
						</tr>
						<c:if test="${member.coGubunCd eq '2'}">
							<tr id="coGubunCd2_layer2">
						</c:if>
						<c:if test="${member.coGubunCd eq '1'}">
							<tr id="coGubunCd2_layer2" style='display: none'>
						</c:if>
						<td>
							<input type="text" name="bubNo1" class="input_style1 resident" value="${member.bubNo1}" maxlength="6" title="법인 등록번호 앞자리" /><span>-</span><input type="text" name="bubNo2" class="input_style1 resident" value="${member.bubNo2}" maxlength="7" title="법인 등록번호 뒷자리" />
						</td>
						</tr>
						<%-- </c:if> --%>
						<tr>
							<th>
								주소<strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<button type="button" id="findTrustAddr" class="btn_style1">주소찾기</button>
								<input type="text" id="trustZipcode" name="trustZipcode" class="input_style1 address1 disabled" value="${member.trustZipcode}" readonly placeholder="" title="주소입력칸 1" />
								<input type="text" id="trustAddr" class="input_style1 margin disabled" value="${member.trustSido} ${member.trustSido} ${member.trustDong} ${member.trustBunji}" readonly placeholder="" title="주소입력칸 2" />
								<input type="text" id="trustDetailAddr" name="trustDetailAddr" class="input_style1" value="${member.trustDetailAddr}" maxlength="100" placeholder="" title="주소입력칸 3" />
							</td>
						</tr>
						<tr>
							<th>
								전화번호(휴대전화번호) <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<%-- 									<select name="trustTel1" class="select_style" title="전화번호 앞자리" >
										<option value="02"   <c:if test="${member.trustTel1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.trustTel1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.trustTel1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.trustTel1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.trustTel1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.trustTel1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.trustTel1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.trustTel1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.trustTel1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.trustTel1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.trustTel1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.trustTel1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.trustTel1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.trustTel1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.trustTel1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.trustTel1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.trustTel1 eq '0502'}">selected</c:if>>0502</option> 
										<option value="010" <c:if test="${member.trustTel1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${member.trustTel1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${member.trustTel1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${member.trustTel1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${member.trustTel1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${member.trustTel1 eq '019'}">selected</c:if>>019</option> 
										
									</select> --%>
								<input type="text" name="trustTel1" class="input_style1 phone" value="${member.trustTel1}" maxlength="3" placeholder="" title="전화번호 앞자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="trustTel2" class="input_style1 phone" value="${member.trustTel2}" maxlength="4" placeholder="" title="전화번호 중간자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="trustTel3" class="input_style1 phone" value="${member.trustTel3}" maxlength="4" placeholder="" title="전화번호 뒷자리" onkeypress="numberOnly" />
							</td>
						</tr>
						<tr>
							<th>팩스번호</th>
						</tr>
						<tr>
							<td>
								<%-- 									<select name="trustFax1" class="select_style" title="전화번호 앞자리" >
										<option value="02"   <c:if test="${member.trustFax1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.trustFax1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.trustFax1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.trustFax1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.trustFax1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.trustFax1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.trustFax1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.trustFax1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.trustFax1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.trustFax1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.trustFax1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.trustFax1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.trustFax1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.trustFax1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.trustFax1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.trustFax1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.trustFax1 eq '0502'}">selected</c:if>>0502</option> 
									</select> --%>
								<input type="text" name="trustFax1" class="input_style1 phone" value="${member.trustFax1}" maxlength="4" placeholder="" title="전화번호 앞자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="trustFax2" class="input_style1 phone" value="${member.trustFax2}" maxlength="4" placeholder="" title="전화번호 중간자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="trustFax3" class="input_style1 phone" value="${member.trustFax3}" maxlength="4" placeholder="" title="전화번호 뒷자리" onkeypress="numberOnly" />
							</td>
						</tr>
						<tr>
							<th>홈페이지</th>
						</tr>
						<tr>
							<td>
								<input type="text" name="trustUrl" class="input_style1" value="${member.trustUrl}" maxlength="100" placeholder="" title="홈페이지 주소입력칸" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="table_area">
				<table>
					<caption>대표자정보</caption>
					<thead>
						<tr>
							<th>대표자정보</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>
								대표자명 <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<input type="text" name="ceoName" class="input_style1" value="${member.ceoName}" maxlength="30" placeholder="" title="대표자명" />
							</td>
						</tr>
						<tr>
							<th>
								주민등록번호 <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<input type="text" name="ceoRegNo1" class="input_style1 resident" value="${member.ceoRegNo1}" maxlength="6" placeholder="" title="주민등록번호 앞자리" onkeypress="numberOnly" /><span>-</span><input type="password" name="ceoRegNo2" class="input_style1 resident" value="*******" maxlength="7" placeholder="" title="주민등록번호 뒷자리" onkeypress="numberOnly" />
								
							</td>
						</tr>
						<tr>
							<th>
								주소 <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<button type="button" id="findCeoAddr" class="btn_style1">주소찾기</button>
								<input type="text" id="ceoZipcode" name="ceoZipcode" class="input_style1 address1 disabled" value="${member.ceoZipcode}" readonly placeholder="" title="주소입력칸 1" />
								<input type="text" id="ceoAddr" class="input_style1 margin disabled" value="${member.ceoSido} ${member.ceoGugun} ${member.ceoDong} ${member.ceoBunji}" readonly placeholder="" title="주소입력칸 2" />
								<input type="text" id="ceoDetailAddr" name="ceoDetailAddr" class="input_style1" value="${member.ceoDetailAddr}" maxlength="100" placeholder="" title="주소입력칸 3" />
							</td>
						</tr>
						<tr>
							<th>
								전화번호(휴대전화번호) <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<%-- 				<select name="ceoTel1" class="select_style" title="전화번호 앞자리">
										<option value="02"   <c:if test="${member.ceoTel1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.ceoTel1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.ceoTel1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.ceoTel1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.ceoTel1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.ceoTel1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.ceoTel1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.ceoTel1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.ceoTel1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.ceoTel1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.ceoTel1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.ceoTel1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.ceoTel1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.ceoTel1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.ceoTel1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.ceoTel1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.ceoTel1 eq '0502'}">selected</c:if>>0502</option> 
  										<option value="010" <c:if test="${member.ceoTel1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${member.ceoTel1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${member.ceoTel1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${member.ceoTel1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${member.ceoTel1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${member.ceoTel1 eq '019'}">selected</c:if>>019</option> 
										
									</select> --%>
								<input type="text" name="ceoTel1" class="input_style1 phone" value="${member.ceoTel1}" maxlength="3" placeholder="" title="전화번호 앞자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="ceoTel2" class="input_style1 phone" value="${member.ceoTel2}" maxlength="4" placeholder="" title="전화번호 중간자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="ceoTel3" class="input_style1 phone" value="${member.ceoTel3}" maxlength="4" placeholder="" title="전화번호 뒷자리" onkeypress="numberOnly" />
							</td>
						</tr>
						<tr>
							<th>팩스번호</th>
						</tr>
						<tr>
							<td>
								<%-- <select name="ceoFax1" class="select_style" title="전화번호 앞자리" >
										<option value="02"   <c:if test="${member.ceoFax1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.ceoFax1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.ceoFax1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.ceoFax1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.ceoFax1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.ceoFax1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.ceoFax1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.ceoFax1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.ceoFax1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.ceoFax1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.ceoFax1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.ceoFax1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.ceoFax1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.ceoFax1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.ceoFax1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.ceoFax1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.ceoFax1 eq '0502'}">selected</c:if>>0502</option> 
										
									</select> --%>
								<input type="text" name="ceoFax1" class="input_style1 phone" value="${member.ceoFax1}" maxlength="3" placeholder="" title="전화번호 앞자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="ceoFax2" class="input_style1 phone" value="${member.ceoFax2}" maxlength="4" placeholder="" title="전화번호 중간자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="ceoFax3" class="input_style1 phone" value="${member.ceoFax3}" maxlength="4" placeholder="" title="전화번호 뒷자리" onkeypress="numberOnly" />
							</td>
						</tr>
						<tr>
							<th>
								휴대전화번호 <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr>
							<td>
								<%-- <select name="ceoMobile1" class="select_style" title="전화번호 앞자리">
										<option value="010" <c:if test="${member.ceoMobile1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${member.ceoMobile1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${member.ceoMobile1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${member.ceoMobile1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${member.ceoMobile1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${member.ceoMobile1 eq '019'}">selected</c:if>>019</option> 
									</select> --%>
								<input type="text" name="ceoMobile1" class="input_style1 phone" value="${member.ceoMobile1}" maxlength="3" placeholder="" title="전화번호 앞자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="ceoMobile2" class="input_style1 phone" value="${member.ceoMobile2}" maxlength="4" placeholder="" title="전화번호 중간자리" onkeypress="numberOnly" />
								<span>-</span>
								<input type="text" name="ceoMobile3" class="input_style1 phone" value="${member.ceoMobile3}" maxlength="4" placeholder="" title="전화번호 뒷자리" onkeypress="numberOnly" />
							</td>
						</tr>
						<tr>
							<th>이메일</th>
						</tr>
						<tr>
							<td>
								<input type="text" name="ceoEmail1" class="input_style1 resident" value="${member.ceoEmail1}" maxlength="70" placeholder="" title="이메일 주소 앞자리" /><span>@</span><input type="text" name="ceoEmail2" class="input_style1 resident" value="${member.ceoEmail2}" maxlength="29" placeholder="" title="이메일 주소 뒷자리" />
							</td>
						</tr>
						<tr>
							<th>SMS수신동의</th>
						</tr>
						<tr>
							<td>
								<c:if test="${member.smsAgree eq 'Y'}">
									<c:set var="smsAgreeYChecked" value="checked" />
								</c:if>
								<c:if test="${member.smsAgree ne 'Y'}">
									<c:set var="smsAgreeNChecked" value="checked" />
								</c:if>
								<div class="btm_form_box">

									<input type="radio" id="smsAgreeY" name="smsAgree" value="Y" ${smsAgreeYChecked} />
									<label for="smsAgreeY">동의</label>
								</div>
								<div class="btm_form_box">
									<input type="radio" id="smsAgreeN" name="smsAgree" value="N" ${smsAgreeNChecked} />
									<label for="smsAgreeN">동의안함</label>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
		</div>
		<!-- //aside -->
		<div class="cont">
			<div class="table_area">
				<table>
					<caption>업무선택</caption>
					<thead>
						<tr>
							<th>업무선택</th>
						</tr>
					</thead>
					<tbody>
						<c:if test="${type eq 'sub_modify' or type eq 'trust_modify'}">
							<tr>
								<th>
									변경사항 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<div class="btm_form_box">
										<input type="checkbox" id="chgcd1" name="chgCd" value="1" />
										<label for="chgcd1">대표자</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="chgcd2" name="chgCd" value="2" />
										<label for="chgcd2">업체명</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="chgcd3" name="chgCd" value="3" />
										<label for="chgcd3">주소</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="chgcd6" name="chgCd" value="6" />
										<label for="chgcd6">취급저작물/취급저작권종류</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="chgcd7" name="chgCd" value="7" />
										<label for="chgcd7">등기임원</label>
									</div>
								</td>
							</tr>
						</c:if>
						<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
							<tr>
								<th>
									취급 하고자 하는 업무의 내용 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${member.conDetailCd eq '1'}">
										<c:set var="conDetailCd1" value="checked" />
										<c:set var="conDetailCd2" value="" />
									</c:if>
									<c:if test="${member.conDetailCd eq '2'}">
										<c:set var="conDetailCd1" value="" />
										<c:set var="conDetailCd2" value="checked" />
									</c:if>
									<c:if test="${member.conDetailCd eq '3'}">
										<c:set var="conDetailCd1" value="checked" />
										<c:set var="conDetailCd2" value="checked" />
									</c:if>
									<div class="btm_form_box">
										<input type="checkbox" id="conDetailCd1" value="1" ${conDetailCd1} />
										<label for="conDetailCd1">대리</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="conDetailCd2" value="2" ${conDetailCd2} />
										<label for="conDetailCd2">중개</label>
									</div>
								</td>
							</tr>
						</c:if>
						<tr>
							<th>
								취급 저작물의 종류 및 취급 권리의 종류 (저작권) <strong class="txt_point">*</strong>
							</th>
						</tr>
						<tr class="list_copy">
							<td>
								<div class="btm_form_box first">
									<input type="checkbox" id="writingKind1" name="writingKind" value="1" ${writingKind1}  />
									<label for="writingKind1" style="pointer-events:none;">어문 저작물</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind1_1" name="permkind1" value="1" ${permkind1_1} />
									<label for="permkind1_1">복제권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind1_2" name="permkind1" value="2" ${permkind1_2} />
									<label for="permkind1_2">배포권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind1_3" name="permkind1" value="3" ${permkind1_3} />
									<label for="permkind1_3">전송권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind1_4" name="permkind1" value="4" ${permkind1_4} />
									<label for="permkind1_4">공연권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind1_8" name="permkind1" value="8" ${permkind1_8} />
									<label for="permkind1_8">2차적저작물작성권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind1_0" name="permkind1" value="0" ${permkind1_0} />
									<label for="permkind1_0">기타</label>
								</div>
								<input type="text" name="permEtc1" class="input_style1 others" value="${permEtc1}" placeholder="" title="기타" />
							</td>
						</tr>
						<tr class="list_copy">
							<td>
								<div class="btm_form_box first">
									<input type="checkbox" id="writingKind2" name="writingKind" value="2" ${writingKind2}  />
									<label for="writingKind2" style="pointer-events:none;">음악 저작물</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind2_1" name="permkind2" value="1" ${permkind2_1} />
									<label for="permkind2_1">복제권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind2_2" name="permkind2" value="2" ${permkind2_2} />
									<label for="permkind2_2">배포권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind2_3" name="permkind2" value="3" ${permkind2_3} />
									<label for="permkind2_3">전송권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind2_4" name="permkind2" value="4" ${permkind2_4} />
									<label for="permkind2_4">공연권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind2_8" name="permkind2" value="8" ${permkind2_8} />
									<label for="permkind2_8">2차적저작물작성권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind2_0" name="permkind2" value="0" ${permkind2_0} />
									<label for="permkind2_0">기타</label>
								</div>
								<input type="text" name="permEtc2" class="input_style1 others" value="${permEtc2}" placeholder="" title="기타" />
							</td>
						</tr>
						<tr class="list_copy">
							<td>
								<div class="btm_form_box first">
									<input type="checkbox" id="writingKind3" name="writingKind" value="3" ${writingKind3}  />
									<label for="writingKind3" style="pointer-events:none;">영상 저작물</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind3_1" name="permkind3" value="1" ${permkind3_1} />
									<label for="permkind3_1">복제권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind3_2" name="permkind3" value="2" ${permkind3_2} />
									<label for="permkind3_2">배포권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind3_3" name="permkind3" value="3" ${permkind3_3} />
									<label for="permkind3_3">전송권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind3_4" name="permkind3" value="4" ${permkind3_4} />
									<label for="permkind3_4">공연권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind3_8" name="permkind3" value="8" ${permkind3_8} />
									<label for="permkind3_8">2차적저작물작성권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind3_0" name="permkind3" value="0" ${permkind3_0} />
									<label for="permkind3_0">기타</label>
								</div>
								<input type="text" name="permEtc3" class="input_style1 others" value="${permEtc3}" placeholder="" title="기타" />
							</td>
						</tr>
						<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind21" name="writingKind" value="21" ${writingKind21}  />
										<label for="writingKind21" style="pointer-events:none;">연극 저작물</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_1" name="permkind21" value="1" ${permkind21_1} />
										<label for="permkind21_1">복제권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_2" name="permkind21" value="2" ${permkind21_2} />
										<label for="permkind21_2">배포권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_3" name="permkind21" value="3" ${permkind21_3} />
										<label for="permkind21_3">전송권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_4" name="permkind21" value="4" ${permkind21_4} />
										<label for="permkind21_4">공연권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind21_8" name="permkind21" value="8" ${permkind21_8} />
										<label for="permkind21_8">2차적저작물작성권</label>
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind21_0" name="permkind21" value="0" ${permkind21_0} />
										<label for="permkind21_0">기타</label>
									</div>
									<input type="text" name="permEtc21" class="input_style1 others" value="${permEtc21}" placeholder="" title="기타" />
								</td>
							</tr>
						</c:if>
						<tr class="list_copy">
							<td>
								<div class="btm_form_box first">
									<input type="checkbox" id="writingKind4" name="writingKind" value="4" ${writingKind4}  />
									<label for="writingKind4" style="pointer-events:none;">미술 저작물</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind4_1" name="permkind4" value="1" ${permkind4_1} />
									<label for="permkind4_1">복제권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind4_2" name="permkind4" value="2" ${permkind4_2} />
									<label for="permkind4_2">배포권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind4_3" name="permkind4" value="3" ${permkind4_3} />
									<label for="permkind4_3">전송권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind4_4" name="permkind4" value="4" ${permkind4_4} />
									<label for="permkind4_4">공연권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind4_8" name="permkind4" value="8" ${permkind4_8} />
									<label for="permkind4_8">2차적저작물작성권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind4_0" name="permkind4" value="0" ${permkind4_0} />
									<label for="permkind4_0">기타</label>
								</div>
								<input type="text" name="permEtc4" class="input_style1 others" value="${permEtc4}" placeholder="" title="기타" />
							</td>
						</tr>
						<tr class="list_copy">
							<td>
								<div class="btm_form_box first">
									<input type="checkbox" id="writingKind5" name="writingKind" value="5" ${writingKind5}  />
									<label for="writingKind5" style="pointer-events:none;">사진 저작물</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind5_1" name="permkind5" value="1" ${permkind5_1} />
									<label for="permkind5_1">복제권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind5_2" name="permkind5" value="2" ${permkind5_2} />
									<label for="permkind5_2">배포권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind5_3" name="permkind5" value="3" ${permkind5_3} />
									<label for="permkind5_3">전송권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind5_4" name="permkind5" value="4" ${permkind5_4} />
									<label for="permkind5_4">공연권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind5_8" name="permkind5" value="8" ${permkind5_8} />
									<label for="permkind5_8">2차적저작물작성권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind5_0" name="permkind5" value="0" ${permkind5_0} />
									<label for="permkind5_0">기타</label>
								</div>
								<input type="text" name="permEtc5" class="input_style1 others" value="${permEtc5}" placeholder="" title="기타" />
							</td>
						</tr>
						<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind22" name="writingKind" value="22" ${writingKind22}  />
										<label for="writingKind22" style="pointer-events:none;">건축 저작물</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_1" name="permkind22" value="1" ${permkind22_1} />
										<label for="permkind22_1">복제권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_2" name="permkind22" value="2" ${permkind22_2} />
										<label for="permkind22_2">배포권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_3" name="permkind22" value="3" ${permkind22_3} />
										<label for="permkind22_3">전송권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_4" name="permkind22" value="4" ${permkind22_4} />
										<label for="permkind22_4">공연권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind22_8" name="permkind22" value="8" ${permkind22_8} />
										<label for="permkind22_8">2차적저작물작성권</label>
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind22_0" name="permkind22" value="0" ${permkind22_0} />
										<label for="permkind22_0">기타</label>
									</div>
									<input type="text" name="permEtc22" class="input_style1 others" value="${permEtc22}" placeholder="" title="기타" />
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind23" name="writingKind" value="23" ${writingKind23}  />
										<label for="writingKind23" style="pointer-events:none;">도형 저작물</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind23_1" name="permkind23" value="1" ${permkind23_1} />
										<label for="permkind23_1">복제권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind23_2" name="permkind23" value="2" ${permkind23_2} />
										<label for="permkind23_2">배포권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind23_3" name="permkind23" value="3" ${permkind23_3} />
										<label for="permkind23_3">전송권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind23_8" name="permkind23" value="8" ${permkind23_8} />
										<label for="permkind23_8">2차적저작물작성권</label>
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind23_0" name="permkind23" value="0" ${permkind23_0} />
										<label for="permkind23_0">기타</label>
									</div>
									<input type="text" name="permEtc23" class="input_style1 others" value="${permEtc23}" placeholder="" title="기타" />
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind24" name="writingKind" value="24" ${writingKind24}  />
										<label for="writingKind24" style="pointer-events:none;">컴퓨터프로그램 저작물</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_1" name="permkind24" value="1" ${permkind24_1} />
										<label for="permkind24_1">복제권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_2" name="permkind24" value="2" ${permkind24_2} />
										<label for="permkind24_2">배포권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_3" name="permkind24" value="3" ${permkind24_3} />
										<label for="permkind24_3">전송권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_6" name="permkind24" value="6" ${permkind24_6} />
										<label for="permkind24_6">대여권</label>
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind24_8" name="permkind24" value="8" ${permkind24_8} />
										<label for="permkind24_8">2차적저작물작성권</label>
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind24_0" name="permkind24" value="0" ${permkind24_0} />
										<label for="permkind24_0">기타</label>
									</div>
									<input type="text" name="permEtc24" class="input_style1 others" value="${permEtc24}" placeholder="" title="기타" />
								</td>
							</tr>
						</c:if>
						<tr class="list_copy">
							<td>
								<div class="btm_form_box first">
									<input type="checkbox" id="writingKind0" name="writingKind" value="0" ${writingKind0} />
									<label for="writingKind0" style="pointer-events:none;">기타 저작물</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind0_1" name="permkind0" value="1" ${permkind0_1} />
									<label for="permkind0_1">복제권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind0_2" name="permkind0" value="2" ${permkind0_2} />
									<label for="permkind0_2">배포권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind0_3" name="permkind0" value="3" ${permkind0_3} />
									<label for="permkind0_3">전송권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind0_4" name="permkind0" value="4" ${permkind0_4} />
									<label for="permkind0_4">공연권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind0_5" name="permkind0" value="5" ${permkind0_5} />
									<label for="permkind0_5">전시권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind0_6" name="permkind0" value="6" ${permkind0_6} />
									<label for="permkind0_6">대여권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind0_7" name="permkind0" value="7" ${permkind0_7} />
									<label for="permkind0_7">방송권</label>
								</div>
								<div class="btm_form_box" style="margin-left: 180px;">
									<input type="checkbox" id="permkind0_8" name="permkind0" value="8" ${permkind0_8} />
									<label for="permkind0_8">2차적저작물작성권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind0_0" name="permkind0" value="0" ${permkind0_0} />
									<label for="permkind0_0">기타</label>
								</div>
								<input type="text" name="permEtc0" class="input_style1 others" value="${permEtc0}" placeholder="" title="기타" />
							</td>
						</tr>
						<tr>
							<th class="pt">
								취급 저작물의 종류 및 취급 권리의 종류 (저작인접권) <br><br><strong class="txt_point">
								*취급하는 저작물 내 저작인접권(실연자의 권리, 음반제작자의권리, 출판권)은 신규,추가 신청은 불가능합니다</strong>
							</th>
						</tr>
						<tr class="list_copy" >
							<td>
								<div class="btm_form_box first">
									<input disabled type="checkbox" id="writingKind10" name="writingKind" value="10" ${writingKind10} disabled />
									<label for="writingKind10" style="pointer-events:none">실연자의 권리</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind10_1" name="permkind10" value="1" ${permkind10_1} disabled />
									<label for="permkind10_1">복제권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind10_2" name="permkind10" value="2" ${permkind10_2} disabled/>
									<label for="permkind10_2">배포권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind10_3" name="permkind10" value="3" ${permkind10_3} disabled/>
									<label for="permkind10_3">전송권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind10_4" name="permkind10" value="4" ${permkind10_4} disabled/>
									<label for="permkind10_4">공연권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind10_0" name="permkind10" value="0" ${permkind10_0} disabled/>
									<label for="permkind10_0">기타</label>
								</div>
								<%-- <input type="text" name="permEtc10" class="input_style1 others" value="${permEtc10}" placeholder="" title="기타" /> --%>
							</td>
						</tr>
						<tr class="list_copy">
							<td>
								<div class="btm_form_box first">
									<input type="checkbox" id="writingKind11" name="writingKind" value="11" ${writingKind11}  disabled/>
									<label for="writingKind11" style="pointer-events:none;">음반제작자의 권리</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind11_1" name="permkind11" value="1" ${permkind11_1} disabled/>
									<label for="permkind11_1">복제권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind11_2" name="permkind11" value="2" ${permkind11_2} disabled/>
									<label for="permkind11_2">배포권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind11_3" name="permkind11" value="3" ${permkind11_3} disabled/>
									<label for="permkind11_3">전송권</label>
								</div>
								<div class="btm_form_box last">
									<input type="checkbox" id="permkind11_0" name="permkind11" value="0" ${permkind11_0} disabled/>
									<label for="permkind11_0">기타</label>
								</div>
								<%-- <input type="text" name="permEtc11" class="input_style1 others" value="${permEtc11}" placeholder="" title="기타" /> --%>
							</td>
						</tr>
						<tr class="list_copy">
							<td>
								<div class="btm_form_box first">
									<input type="checkbox" id="writingKind12" name="writingKind" value="12" ${writingKind12} disabled/>
									<label for="writingKind12" style="pointer-events:none;">출판권</label>
								</div>
								<div class="btm_form_box">
									<input type="checkbox" id="permkind12_9" name="permkind12" value="9" ${permkind12_9} disabled/>
									<label for="permkind12_9">출판권</label>
								</div>
							</td>
						</tr>
						<!-- 
							<tr>
								<th class="pt">
									변경 신청사유 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<textarea class="input_style1 height"></textarea>
								</td>
							</tr>
							 -->
					</tbody>
				</table>
				</form>
			</div>
			<!-- //table_area -->
			<c:if test="${!empty rpMgm}">
				<div class="cell">
					<table>
					<tr>

						<p class="tit">보완/반려 이전 신고시 제출한 파일 보기</p>
						<td class="txt_l">
							<div class="file_change">
								<c:if test="${!empty rpMgm.file1Path}">
									<a href="<c:url value="/download?filename=${rpMgm.file1Path}" />"> 
										<p style="text-decoration: underline;color:#5959cc;"  class="txt">저작권대리중개업 업무규정</p>
									</a>
								</c:if>
								<c:if test="${!empty rpMgm.file2Path}">
									<a href="<c:url value="/download?filename=${rpMgm.file2Path}" />">
										<p style="text-decoration: underline; color: #5959cc;" class="txt">신고인(단체 또는 법인인 경우에는 그 대표자및 임원)의 이력서</p>
									</a>
								</c:if>
								<c:if test="${!empty rpMgm.file3Path}">
									<a href="<c:url value="/download?filename=${rpMgm.file3Path}" />">
										<p style="text-decoration: underline; color: #5959cc;" class="txt">정관 또는 규약 1부</p>
									</a>
								</c:if>
								<c:if test="${!empty rpMgm.file4Path}">
									<a href="<c:url value="/download?filename=${rpMgm.file4Path}" />">
										<p style="text-decoration: underline; color: #5959cc;" class="txt">재무제표(법인단체)</p>
									</a>
								</c:if>

								<%-- 													<c:forEach var="item" items="${chgHistoryFileList}" varStatus="status">
														<a href="<c:url value="/download?filename=${item.filepath}" />">
															<p class="txt1">변경신고신청 첨부<c:out value="${status.count}" /></p>
															<p class="txt2"><c:out value="${item.filename}" /></p>
															<p class="txt3">(<c:out value="${item.regDateStr}" />)</p>
														</a>
													</c:forEach> --%>
							</div>
						</td>
					</tr>
					</table>
				</div>
			</c:if>
			<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
				<c:if test="${type eq 'sub_regist' }">
					<div class="cell">
						<p class="tit">구비서류 안내</p>
						<p class="txt">저작권법 제105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</p>
						<p class="txt">
							저작권대리중개업신고서 1부 <span>[현재 작성한 온라인 신청서]</span>
						</p>
						<br>
						<p class="txt">
							저작권대리중개업 업무규정 1부 <span>(저작권대리중개 계약약관, 저작물 이용계약 약관 각1부 포함)</span> <a href="<c:url value='/download2?filename=3.zip' />" class="btn_style1 margin" target="_blank">샘플 다운로드</a>
						</p>
						<br>
						<p class="txt">
							정관 또는 규약<span>(법인 또는 단체인 경우에 합니다.)</span> 1부 <span>[스캔한 파일 온라인 첨부가능]</span>
						</p>
						<br>
						<p class="txt">
							재무제표<span>(법인인 경우에 한정합니다.)</span> 1부<span>[스캔한 파일 온라인 첨부가능]</span>
						</p>
						<br>
						<p class="txt">
							<span>(재무제표가 없는경우 재무제표미제출사유서를 첨부해주세요.)</span> <a href="<c:url value='/download2?filename=7.zip' />" class="btn_style1 margin" target="_blank">양식 다운로드</a>
						</p>
						<br>
						<p class="txt">
							신고인<span>(단체 또는 법인인 경우에는 그 대표자 및 임원)</span>의 이력서 1부 <a href="<c:url value='/download2?filename=6.zip' />" class="btn_style1 margin" target="_blank">양식 다운로드</a>
						</p>
						<br>
						<p class="txt">
							표준업무규정 주요거래분야 5종에 대한 대리중개계약서 및 이용허락계약서 <a href="<c:url value='/download2?filename=11.zip' />" class="btn_style1 margin" target="_blank">양식 다운로드</a>
						</p>
					</div>
				</c:if>
				<c:if test="${type eq 'sub_modify' }">
					<div class="cell">
						<p class="tit">구비서류 안내</p>
						<p class="txt">저작권법 제105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</p>
						<p class="txt">
							저작권대리중개업신고서 1부 <span>[현재 작성한 온라인 신청서]</span>
						</p>
						<p class="txt">
							신고증 반납<span>[우편으로 반납]</span>
						<p class="txt">변경사항을 증명하는 서류 1부</p>
						<p class="txt">
							대표자 변경시 : 이력서, 이사회 회의록, 사업자등록증 사본</span><a href="<c:url value='/download2?filename=6.zip' />" class="btn_style1 margin" target="_blank">양식 다운로드</a>
						</p>
						<p class="txt">등기임원변경시 : 등기임원 이력서</p>
						<br>
						<p class="txt">주소 변경시 : 사업자등록증 사본</p>
						<br>
						<p class="txt">상호 변경시 : 사업자등록증 사본, 저작권신탁관리업 업무규정 및 계약 약관</p>
						<br> <a href="<c:url value='/download2?filename=3.zip'/>" class="btn_style1 margin" target="_blank">샘플 다운로드</a>
						</p>
						<p class="txt">상호 변경시 법인일 경우에는 이사회 회의록 추가 제출</p>

					</div>
				</c:if>
				<!-- //cell -->
				<div class="cell">
					<p class="tit">첨부파일 ( 임시저장된 데이터를 불러온 경우 파일을 재업로드해주세요. )</p>
					<div class="list_file">
						<form id="tmpForm1">
							<p class="txt">
								저작권 대리중개업 업무에 관한 규정 <strong class="txt_point">*</strong>
							</p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
							<input type="file" name="file1" class="insert_file_target" data-ref-name="file1Path" title="파일첨부" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
							<button type="button" class="btn_style1" onclick="removefile('1')">파일삭제</button>
						</form>
					</div>
					<div class="list_file">
						<form id="tmpForm2">
							<p class="txt">
								신고인의 이력서 <strong class="txt_point">*</strong>
							</p>
							<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
							<input type="file" name="file2" class="insert_file_target" data-ref-name="file2Path" title="파일첨부" />
							<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
							<button type="button" class="btn_style1" onclick="removefile('2')">파일삭제</button>
						</form>
					</div>
					<div class="list_file">
								<form id="tmpForm5">
									<p class="txt">
									 대리중개계약서 및 이용허락계약서 <strong class="txt_point">*<br>(다건의 경우 ZIP압축으로 첨부)</strong>
									</p>
									<input type="text" class="input_style1 prj_file" placeholder="ZIP,HWP 파일만 첨부" title="파일첨부" readonly />
									<input type="file" name="file5" class="insert_file_target" data-ref-name="file5Path" title="파일첨부" />
									<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
									<button type="button" class="btn_style1" onclick="removefile('5')">파일삭제</button>
								</form>
							</div>
					<c:if test="${member.coGubunCd eq '1'}">
						<div id="addfile" style='display: none'>
							<div class="list_file">
								<form id="tmpForm3">
									<p class="txt">
										정관 또는 규약 <strong class="txt_point">*</strong>
									</p>
									<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
									<input type="file" name="file3" class="insert_file_target" data-ref-name="file3Path" title="파일첨부" />
									<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
									<button type="button" class="btn_style1" onclick="removefile('3')">파일삭제</button>
								</form>
							</div>
							<div class="list_file">
								<form id="tmpForm4">
									<p class="txt">
										재무제표 <strong class="txt_point">*</strong>
									</p>
									<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
									<input type="file" name="file4" class="insert_file_target" data-ref-name="file4Path" title="파일첨부" />
									<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
									<button type="button" class="btn_style1" onclick="removefile('4')">파일삭제</button>
								</form>
							</div>
							<div class="txt_b">105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</div>
					</c:if>
					<c:if test="${member.coGubunCd eq '2'}">
						<div id="addfile" style='display: display'>
							<div class="list_file">
								<form id="tmpForm3">
									<p class="txt">
										정관 또는 규약 <strong class="txt_point">*</strong>
									</p>
									<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
									<input type="file" name="file3" class="insert_file_target" data-ref-name="file3Path" title="파일첨부" />
									<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
									<button type="button" class="btn_style1" onclick="removefile('3')">파일삭제</button>
								</form>
							</div>
							<div class="list_file">
								<form id="tmpForm4">
									<p class="txt">
										재무제표 <strong class="txt_point">*</strong>
									</p>
									<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
									<input type="file" name="file4" class="insert_file_target" data-ref-name="file4Path" title="파일첨부" />
									<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
									<button type="button" class="btn_style1" onclick="removefile('4')">파일삭제</button>
								</form>
							</div>
						</div>
					</c:if>
				
				</div>
					<div class="txt_b">105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</div>
		</div>
		<!-- <div class="txt_b">105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</div> -->
		</c:if>
		<c:if test="${type eq 'trust_regist' or type eq 'trust_modify'}">
			<c:if test="${type eq 'trust_regist'}">
				<div class="cell">
					<p class="tit">구비서류 안내</p>
					<p class="txt">저작권법 제105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</p>
					<p class="txt">
						저작권신탁관리업 허가신청서 <span>[현재 작성한 온라인 신청서]</span>
					</p>
					<p class="txt">저작권신탁관리 업무규정 1부</p>
					<p class="txt_inner">
						- 저작권 신탁계약 약관<br>- 저작권 이용계약 약관
					</p>
					<p class="txt">
						신고인 <span>(단체 또는 법인인 경우에는 그 대표자 및 임원)</span>의 이력서 1부 <span>[온라인 첨부가능]</span> <a href="<c:url value='/download2?filename=6.zip' />" class="btn_style1 margin">양식 다운로드</a>
					</p>
					<br>
					<p class="txt">
						정관 또는 규약 1부<span>(법인 또는 단체인 경우에 한정합니다.) [스캔한 파일 온라인 첨부가능]</span>
					</p>
					<br>
					<p class="txt">
						재무제표<span>(법인인 경우에 한정합니다.)</span> <span>1부 [스캔한 파일 온라인 첨부가능]<span>
					</p>
					<p class="txt">
						<span>(재무제표가 없는경우 재무제표미제출사유서를 첨부해주세요.)</span> <a href="<c:url value='/download2?filename=7.zip' />" class="btn_style1 margin" target="_blank">양식 다운로드</a>
					</p>
					<br>
				</div>
			</c:if>
			<c:if test="${type eq 'trust_modify'}">
				<div class="cell">
					<p class="tit">구비서류 안내</p>
					<p class="txt">저작권법 제105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</p>
					<p class="txt">
						저작권대리중개업신고서 1부 <span>[현재 작성한 온라인 신청서]</span>
					</p>
					<p class="txt">
						신고증 반납<span>[우편으로 반납]</span>
					<p class="txt">변경사항을 증명하는 서류 1부</p>
					<p class="txt">
						대표자 변경시 : 이력서, 이사회 회의록, 사업자등록증 사본</span><a href="<c:url value='/download2?filename=6.zip'/>" class="btn_style1 margin" target="_blank">양식 다운로드</a>
					</p>
					<p class="txt">등기임원변경시 : 등기임원 이력서</p>
					<br>
					<p class="txt">주소 변경시 : 사업자등록증 사본</p>
					<br>
					<p class="txt">상호 변경시 : 사업자등록증 사본, 저작권신탁관리업 업무규정 및 계약 약관</p>
					<br>
					<p class="txt">* 상호 변경시 법인일 경우에는 이사회 회의록 추가 제출</p>
				</div>
			</c:if>
			<!-- //cell -->
			<div class="cell">
				<p class="tit">첨부파일</p>
				<div class="list_file">
					<form id="tmpForm1">
						<p class="txt">
							저작권 저작권신탁관리 업무에 관한 규정 <strong class="txt_point">*</strong>
						</p>
						<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
						<input type="file" name="file1" class="insert_file_target" data-ref-name="file1Path" title="파일첨부" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" class="btn_style1" onclick="removefile('1')">파일삭제</button>
					</form>
				</div>
				<div class="list_file">
					<form id="tmpForm2">
						<p class="txt">
							신고인의 이력서 <strong class="txt_point">*</strong>
						</p>
						<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
						<input type="file" name="file2" class="insert_file_target" data-ref-name="file2Path" title="파일첨부" />
						<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
						<button type="button" class="btn_style1" onclick="removefile('2')">파일삭제</button>
					</form>
				</div>
				<div class="list_file">
								<form id="tmpForm5">
									<p class="txt">
									 대리중개계약서 및 이용허락계약서 <strong class="txt_point">*<br>(다건의 경우 ZIP압축으로 첨부)</strong>
									</p>
									<input type="text" class="input_style1 prj_file" placeholder="ZIP,HWP 파일만 첨부" title="파일첨부" readonly />
									<input type="file" name="file5" class="insert_file_target" data-ref-name="file5Path" title="파일첨부" />
									<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
									<button type="button" class="btn_style1" onclick="removefile('5')">파일삭제</button>
								</form>
							</div>
				<c:if test="${member.coGubunCd eq '1'}">
					<div id="addfile" style='display: none'>
						<div class="list_file">
							<form id="tmpForm3">
								<p class="txt">
									정관 또는 규약 <strong class="txt_point">*</strong>
								</p>
								<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
								<input type="file" name="file3" class="insert_file_target" data-ref-name="file3Path" title="파일첨부" />
								<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
								<button type="button" class="btn_style1" onclick="removefile('3')">파일삭제</button>
							</form>
						</div>
						<div class="list_file">
							<form id="tmpForm4">
								<p class="txt">
									재무제표 <strong class="txt_point">*</strong>
								</p>
								<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
								<input type="file" name="file4" class="insert_file_target" data-ref-name="file4Path" title="파일첨부" />
								<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
								<button type="button" class="btn_style1" onclick="removefile('4')">파일삭제</button>
							</form>
						</div>
						
				</c:if>
				<c:if test="${member.coGubunCd eq '2'}">
					<div id="addfile" style='display: display'>
						<div class="list_file">
							<form id="tmpForm3">
								<p class="txt">
									정관 또는 규약 <strong class="txt_point">*</strong>
								</p>
								<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
								<input type="file" name="file3" class="insert_file_target" data-ref-name="file3Path" title="파일첨부" />
								<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
								<button type="button" class="btn_style1" onclick="removefile('3')">파일삭제</button>
							</form>
						</div>
						<div class="list_file">
							<form id="tmpForm4">
								<p class="txt">
									재무제표 <strong class="txt_point">*</strong>
								</p>
								<input type="text" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
								<input type="file" name="file4" class="insert_file_target" data-ref-name="file4Path" title="파일첨부" />
								<button type="button" class="form_btn insert_file btn_style1">파일찾기</button>
								<button type="button" class="btn_style1" onclick="removefile('4')">파일삭제</button>
							</form>
						</div>
				</c:if>
			</div>
	</div>
	<!-- //cell -->
	<%-- 		<div class="txt_commission">
						
						<c:if test="${type eq 'trust_regist'}">
							<div class="num">수수료  <strong>10,000 원</strong></div>
						</c:if>
						<c:if test="${type eq 'trust_modify'}">
							<div class="num">수수료  <strong>3,000 원</strong></div>
						</c:if>
							
						<div class="txt"><strong class="txt_point">*</strong> 저작권법 제105조제1항에 의하여 해당 시스템에서 신고시 1,000원을 감액합니다.</div>
						
					</div> --%>
	<div class="txt_b">105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다.</div>
	</c:if>

	<div class="btn_area">
		<!--  
					<a href="declaration_preview_n.html" class="btn btn_style2">저장</a>
					<a href="" class="btn btn_style1">취소</a>
					-->
		<button onclick="javascript:$('#form').submit();" class="btn btn_style2">저장</button>
	</div>
</div>
<!-- //cont -->


<!-- //form_apply -->
</div>
<!-- //container -->
</div>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="comm_js" value="appl" />
	<jsp:param name="js" value="appl" />
</jsp:include>

<script>
	$(function() {
		// 로딩될때
		$("input[name=ceoRegNo1]").val('');
		$("input[name=ceoRegNo2]").val('');
		
	});
</script>