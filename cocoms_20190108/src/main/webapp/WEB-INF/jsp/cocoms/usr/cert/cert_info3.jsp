<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">
</head>
<body style=“overflow-X:hidden”>

<div style="overflow-y:scroll;" class="pop_common">
	<div class="inner">
		<h2 class="tit txt_c">인터넷 증명발급 서비스</h2>
		<div class="cont">
			<div class="list_step_id">
				<ul>
					<li><a href="<c:url value="/cert/info1" />">위	&#183;변조 방지란?</a></li>
					<li><a href="<c:url value="/cert/info2" />">위&#183;변조 방지 적용기법</a></li>
					<li class="active"><a href="<c:url value="/cert/info3" />">위&#183;변조 검증방법</a></li>
					<li><a href="<c:url value="/cert/info4" />">증명서 출력 FAQ</a></li>
				</ul>
			</div>
			<!-- //list_step -->
			
			<div class="txt_area">
				<p class="tit_box">
					<strong>인터넷으로 발급된 증명서의 위&#183;변조 검증방법</strong><br>
				인터넷으로 발급된 증명서의 위&#183;변조 여부를 검증하는 방법에는 육안으로 검증하는 방법과 스캐너로 검증하는 
방법이 있습니다.</p><br>
				<p class="tit_box">
					<strong>육안으로 검증</strong><br>
				증명서에 “원본”이라는 글자가 모두 육안으로 확인되어야 하며 원본문서를 복사하게 되면 “원”또는 “본”자가 사라집니다.</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_1.png" alt="" /></div>
			<div class="txt_area">
				<p class="tit_box"><strong>스캐너로 검증</strong></p>
				<p class="txt_dot txt_blue bold">발급문서 위&#183;변조 검증 프로그램 다운로드</p>
				<div class="in">
					<p>발급문서 위&#183;변조 검증 프로그램을 다운로드 받은 후, <a href="" class="btn_style1">다운로드</a></p>
					<p>설치하십시오. 설치가 완료되면 검증 프로그램이 실행되며, 이후에는 바탕화면에 놓여진 위&#183;변조 검증 프로그램
아이콘을 실행하시면 됩니다.</p>
				</div>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_2.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_dot">고밀도 2차원바코드를 스캐너를 통해 읽어 들이고, 인터넷 증명발급 서비스에서 제공하는 발급문서 위&#183;변조 검증 
    프로그램에서 증명서의 원본문서와 스캔문서를 대조 확인할 수 있습니다.  </p>
				<br><br>
				<p class="txt_blue">[스캐너로 검증절차]</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_3.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_dot txt_blue bold">스캐너를 이용한 검증 절차안내</p>
				<p class="txt_num"><span>1.</span>발급문서 위&#183변조 검증 프로그램을 실행한 후, <strong class="txt_blue bold">‘바코드 스캔’</strong> 메뉴를 선택하여 <strong class="txt_blue bold">[스캔 장치 선택]</strong>을 클릭하여 검증할
    문서를 스캐닝할 장치를 선택합니다.</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_4.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_num"><span>2. </span>스캐닝할 장치를 선택한 후, 위&#183;변조 검증 프로그램의 ‘바코드 스캔’ 메뉴를 선택하여 <strong class="txt_blue bold">[이미지 스캔]</strong>을 클릭합니다.</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_5.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_num"><span>3. </span>이제 여러분의 스캐너에서 제공되는 스캐닝 소프트웨어가 구동되면서 검증할 문서를 스캐너에서 읽을 준비를
    하게 됩니다. 출력된 문서의 검증에 필요한 스캐닝 권장사양은 다음과 같습니다.<br>
				<div class="in">
					 <p class="txt_blue">인쇄된 문서의 스캐닝을 위해서는 반드시 그레이스케일이나 흑백모드 상태에서 300dpi 이상으로 스캐닝 모드를
    선택하셔야 오류가 발생하지 않습니다.</p>
				</div>
   
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_6.png" alt="" /></div>
			<div class="txt_area">
				<p>
				스캐닝할 대상이 출력된 안전증명서 하단의 ‘고밀도 바코드’ 이기 때문에 스캔할 이미지의 형식이 컬러인 경우
문서 검증과정에서 오류가 날 수 있습니다. 또한 스캐닝할 이미지의 해상도가 300dpi 이하일 경우에도 바코드
검증과정에서 오류가 날 수 있으므로 사전에 설정이 올바른지 확인한 후, 스캔과정을 진행하셔야 만 합니다.
				</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_8.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_num"><span>4. </span>검증할 문서를 스캐닝한 화면이  위&#183;변조 검증 프로그램에 불러오게 되면, 위&#183;변조 검증 프로그램의 상단에 위치한
     아이콘바에서  <strong class="txt_blue bold">‘문서의 바코드 인식’</strong> 버튼 (    <img src="img/img_cert3_b.png" alt="" class="icon"/>     ) 을 눌러 스캐닝된 문서의 아랫부분에 위치된 고밀도 2차원 바코드
     를 인식하도록 합니다.</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_9.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_num"><span>5. </span>앞의 4단계에서 <strong class="txt_blue bold"> ‘문서의 바코드 인식’</strong> 아이콘을 클릭하면, 다음과 같은 바코드 인식 옵션 대화창이 나타납니다.
    이제 고밀도 2차원바코드 인식을 위해 <strong class="txt_blue bold">‘Intacta’</strong> 를 선택한 후, <strong class="txt_blue bold">‘확인’</strong>버튼을 누르십시오.</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_10.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_num"><span>6. </span>앞의 5단계에서 인식할 바코드의 종류를 선택한 후, <strong class="txt_blue bold">‘확인’</strong>버튼을 누르면 위&#183;변조 검증 프로그램에서 발급한 문서가
    맞는 경우 <strong class="txt_blue bold">‘원본문서’</strong>를 보여주게 됩니다. 인쇄된 문서의 내용과 바코드로부터 추출된 원문의 내용을 비교해보면
    해당 문서의 위&#183;변조 여부를 확인할 수 있습니다. </p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_11.png" alt="" /></div>
			<div class="txt_area">
				<p class="tit_box"><strong>스캐너 검증 오류시 대응방법 안내</strong></p>
				<div class="in">
					<p>인터넷 증명발급 서비스에서 제공하는 원본 확인 프로그램을 이용하여 원본의 내용과 출력본의 내용을 대사하는
과정에서 스캐닝한 이미지의 상태에 따라 다음과 같은 오류가 나타날 수 있습니다. 다음에 제시되어 있는 각각의 
오류에 대한 대응방법을 살펴보시면 정확한 검증을 하도록 합니다.</p>
				</div>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_12.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_blue">
					  스캐닝할 때, 스캔의 해상도가 300dpi 이하인 경우에 발생될 수 있습니다. 만약 출력된 문서의 프린터 해상도가
  600dpi 급의 레이저 프린터라면  스캐너의 해상도가 200dpi가 되어도 고밀도 바코드 검출하는데 문제가 없지만.
  300dpi급의 잉크젯 프린터로 출력된 경우 스캐너의 스캔 해상도는 최소 300dpi 이상이 되어야만 이러한 오류를
  정정할 수 있습니다. 
				</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_13.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_blue">
증명서 인쇄시 프린터의 카드리지 상태가 나빠 흐리게 출력된 경우입니다. 안전증명서가 정상적으로 검증되기
위해서는 출력된 증명서의 복사방지 마크가 분명하게 인쇄되어야만 하며, 이와 함께 워터마크 (CI이미지)도 나타
나야 합니다. 만약, 두 개의 출력표식이 모두 출력되지 않았다면 -150 오류가 나타날 수 있습니다. 이 경우 스캐닝을
할 때 최소 600dpi 해상도로 높이고 그레이스케일이나 흑백모드로 다시 스캐닝하여 검증과정을 시도해보십시오. 
				</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_14.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_blue">
검증 프로그램 자체내에서 스캐닝되어진 이미지에 다양한 필터 처리를 하여 가독성을 높여줄 수 있습니다.
즉, 다음 페이지에 보여지는 화면과 같은 상태에서 검증 프로그램 창내에서 마우스의 왼쪽 버튼을 클릭하면 이미지
전처리 메뉴가 나타나게 됩니다. 이미지 전처리 메뉴중 가독성 향상을 위해 디더링 (Dither) 혹은 AntiAlias 효과를
준 후, 다시 문서검증 아이콘 (    <img src="/cocoms/img/img_cert3_b.png" alt="" class="icon"/>     )을 눌러 고밀도 바코드를 읽어 들이게 되면 간혹 이미지의 변형때문에 위와 같은
오류가 발생될 수 있습니다.
				</p>
			</div>
			<!-- //txt_area -->
			<div class="img"><img src="/cocoms/img/img_cert3_15.png" alt="" /></div>
			<div class="txt_area">
				<p class="txt_blue">
사용자가 잉크젯을 통해 컬러로 증명서를 출력시킨 경우, 이 증명서를 컬러모드로 스캐닝하게 되면 간혹 바코드의
셀(흑과 백으로 구성)중에 임의의 점을 컬러로 변환시킬 수 있습니다. 이러한 현상이 나타나게 되면 스캐닝을
그레이스케일 모드나 흑백모드로 변경하고 해상도를 300dpi로 맞춘 후, 다시 스캐닝을 하여 바코드 검출과정을
수행하십시오.  
				</p>
			</div>
			<!-- //txt_area -->
		</div>
	</div>
</div>
<!-- //pop_common -->

<script src="<c:url value='/js/jquery-1.10.2.min.js' />"></script>
<script src="<c:url value='/js/jquery.bxslider.js' />"></script>
<script src="<c:url value='/js/script.js' />"></script>

<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script src="<c:url value='/js/usr/cert.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</body>
</html>
