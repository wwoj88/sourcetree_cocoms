<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="신고/허가 내역" />
</jsp:include>

<input type="hidden" id="memberSeqNo" value="${data.memberSeqNo}" />

	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li class="active"><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<%-- <li><a href="<c:url value="/qna" />">1:1문의 내역</a></li> --%>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage">
			<h2 class="tit_bar mt">신고/허가 내역</h2>
			
			<div id="appl_content" class="cont" data-doc-type="5">
				<div class="table_area">
					<table>
						<caption>저작권신탁관리업</caption>
						<thead>
							<tr> 	 	 	
								<th>저작권신탁관리업 신고서</th>
							</tr>
						</thead>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="declaration_preview">
					<div class="inner">
						<div class="preview">
							<p class="num_top txt_r">처리기간 : 15일</p>
							<h1 class="tit">저작권신탁관리업 허가신청서</h1>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:129px" />
										<col style="width:222px" />
										<col style="width:158px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="4">신청인</th>
											<th>성명<p>(단체 또는 법인명)</p></th>
											<td><c:out value="${chgHistory.postCoName}" /></td>
											<th>법인(사업자)등록번호</th>
											<td>
												<c:if test="${!empty chgHistory.postBubNo}"><c:out value="${chgHistory.postBubNoStr}" /></c:if>
												<c:if test="${empty chgHistory.postBubNo}"><c:out value="${chgHistory.postCoNoStr}" /></c:if>
											</td>
										</tr>
										<tr>
											<th>대표자명</th>
											<td><c:out value="${chgHistory.postCeoName}" /></td>
											<th>생년월일</th>
											<td><c:out value="${birthday}" /></td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td><c:out value="${chgHistory.postTrustTel}" /></td>
											<th>팩스번호</th>
											<td><c:out value="${chgHistory.postTrustFax}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">
												<c:if test="${!empty chgHistory.postTrustZipcode}">(<c:out value="${chgHistory.postTrustZipcode}" />)</c:if>
												<c:out value="${chgHistory.postTrustSido}" />
												<c:out value="${chgHistory.postTrustGugun}" />
												<c:out value="${chgHistory.postTrustDong}" />
												<c:out value="${chgHistory.postTrustBunji}" />
												<c:out value="${chgHistory.postTrustDetailAddr}" />
											</td>
										</tr>
										<tr>
											<th height="50" colspan="2">취급하고자 하는 저작물등의 종류</th>
											<td colspan="3" class="txt_l"><c:out value="${writingKind}" /></td>
										</tr>
										<tr>
											<th height="50" colspan="2">취급하고자 하는 권리<p>(주요 저작물 중심으로 중복선택 가능)</p></th>
											<td colspan="3" class="txt_l"><c:out value="${permKind}" /></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
							<p class="txt1"><strong>[저작권법]</strong> 제 105조 제 1항 및 같은 법 시행령 제 48조에 따라 위와 같이 신고합니다.</p>
							<div class="sign">
								<p>
									<c:out value="${fn:substring(rpMgm.regDate, 0, 4)}" />년 
									<c:out value="${fn:substring(rpMgm.regDate, 4, 6)}" />월 
									<c:out value="${fn:substring(rpMgm.regDate, 6, 8)}" />일
								</p>
								<p>신청인   <c:out value="${chgHistory.postCeoName}" />   (서명 또는 인)</p>
							</div>
							<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:auto" />
										<col style="width:266px" />
										<col style="width:134px" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="2">구비서류</th>
											<th>민원인 제출서류</th>
											<th>담당공무원 확인사항</th>
											<th>수수료</th>
										</tr>
										<tr>
											<td class="txt_l">
												1. 대리중개업 업무규정<br>
												<p>가. 저작권 신탁계약 약관</p><br>
												<p>나. 저작물 이용계약 약관</p><br>

												2. 신고인<p>(단체 또는 법인의 대표자 및 임원)</p>의 이력서<br>

												3. 정관 또는 규약 1부<br>

												4. 재무제표<p>(법인인 경우에 한정)</p><br>

											</td>
											<td>법인등기부등본 1부</td>
											<td>10,000원</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
						</div>
						<!-- //preview -->
					</div>
				</div>
				<!-- //declaration_preview -->
			</div>
			<!-- //cont -->
		
			<div class="right">
				<!-- 
				<a href="" class="btn_style2">PDF저장</a>
				 -->
				<a href="" id="print" class="btn_style2">인쇄</a>
				<a href="<c:url value="/report" />" class="btn_style1">이전</a>
			</div>
		</div>
	</div>
		
<input type="hidden" id="rpx" value="app_declaration_union_c_new" />
<textarea id="xmldata" style="width: 100%; height: 500px; display: none;">
	<root>
		<day><c:out value="${rpMgm.regDate}" /></day><!-- 신고일자 -->
		<title>저작권신탁관리업 허가신청서</title>
		<process_term>15일</process_term><!-- 처리기간 -->
		<company_info>
			<applicant><![CDATA[${chgHistory.postCoName}]]></applicant><!-- 신청인 -->
			<regno><c:if test="${!empty chgHistory.postBubNo}"><c:out value="${chgHistory.postBubNoStr}" /></c:if><
					c:if test="${empty chgHistory.postBubNo}"><c:out value="${chgHistory.postCoNoStr}" /></c:if></regno><!-- 사업자등록번호 -->
			<boss_name><c:out value="${chgHistory.postCeoName}" /></boss_name><!-- 대표자 -->
			<birthday><c:out value="${birthday}" /></birthday><!-- 생년월일 -->
			<tel><c:out value="${chgHistory.postTrustTel}" /></tel><!-- 전화번호 -->
			<fax><c:out value="${chgHistory.postTrustFax}" /></fax><!-- 팩스번호 -->	
			<address>
				<![CDATA[<c:if test="${!empty chgHistory.postTrustZipcode}">(<c:out value="${chgHistory.postTrustZipcode}" />)</c:if
						>${chgHistory.postTrustSido} ${chgHistory.postTrustGugun} ${chgHistory.postTrustDong} ${chgHistory.postTrustBunji} ${chgHistory.postTrustDetailAddr}]]>
			</address><!-- 주소 -->
		</company_info>

		<works><c:out value="${writingKind}" /></works><!-- 저작물의 종류 -->
		<rights><c:out value="${permKind}" /></rights><!-- 취급하고자 하는 권리 -->
	
		<writer><c:out value="${chgHistory.postCeoName}" /></writer><!-- 신고인(대표자) -->
		<writer2><c:out value="${chgHistory.postCeoName}" /></writer2>
		<fee>10000</fee><!-- 수수료 -->
	</root>
</textarea>
		
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
