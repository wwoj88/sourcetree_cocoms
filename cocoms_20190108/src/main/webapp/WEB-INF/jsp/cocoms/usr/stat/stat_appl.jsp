<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_header.jsp" flush="true">
	<jsp:param name="nav1" value="업체현황" />
	<jsp:param name="nav2" value="신고서발급내역" />
</jsp:include>
	
		<div class="search_top">
			<div class="table_area list_top">
				<form id="searchForm" method="GET">
				<input type="hidden" name="pageIndex" value="${page}" />
				
					<table>
						<caption> 신고서발급내역</caption>
						<colgroup>
							<col style="width:125px" />
							<col style="width:395px" />
							<col style="width:125px" />
							<col style="width:395px" />
						</colgroup>
						<tbody>
							<tr>
								<th>검색기간</th>
								<td colspan="3">
									<input type="text" name="searchFromDate"  class="event-date input_style1"  value="${data.searchFromDate}" />
									<input type="text" name="searchToDate" class="event-date2 input_style1" value="${data.searchToDate}" />
								</td>
							</tr>
							<tr>
								<th>회사명</th>
								<td colspan="3"><input type="text" name="coName" class="input_style1 w360" value="${data.coName}" /></td>
							</tr>
						</tbody>
					</table>
					<a href="" id="search" class="btn_style2 btn_search" style="height: 84px;line-height: 84px;">검색</a>
				</form>
			</div>
			<!-- //table_area -->
		</div>
		<div class="total_top_common">
			<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
		</div>
		<div class="table_area list list_type1">
			<table>
				<caption></caption>
				<colgroup>
					<col style="width:66px" />
					<col style="width:150px" />
					<col style="width:auto" />
					<col style="width:197px" />
					<col style="width:160px" />
					<col style="width:142px" />
					<col style="width:98px" />
					<col style="width:155px" />
					<col style="width:155px" />
				</colgroup>
				<thead>
					<tr> 	 
						<th>번호</th>
						<th>신청일자</th>
						<th>신청내용</th>
						<th>회사명</th>
						<th>사업자번호</th>
						<th>처리상태</th>
						<th>발급증</th>
						<th>처리일자</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${list}" varStatus="status">
						<tr>
							<td><c:out value="${start - status.index}" /></td>
							<td><c:out value="${item.regDateStr}" /></td>
							<td>
								<c:choose>
									<c:when test="${item.docType eq '1'}">저작권대리중개업 신고서</c:when>
									<c:when test="${item.docType eq '2'}">저작권중개업 신고증</c:when>
									<c:when test="${item.docType eq '3'}">저작권중개업 변경신고서</c:when>
									<c:when test="${item.docType eq '4'}">저작권중개업 신고증</c:when>
									<c:when test="${item.docType eq '5'}">저작권신탁관리업 신고서</c:when>
									<c:when test="${item.docType eq '6'}">저작권신탁관리업 신고증</c:when>
									<c:when test="${item.docType eq '7'}">저작권신탁관리업 허가증 변경교부 신청서</c:when>
									<c:when test="${item.docType eq '8'}">저작권신탁관리업 변경교부 허가증</c:when>
								</c:choose>
							</td>
							<td><c:out value="${item.coName}" /></td>
							<td><c:out value="${item.coNoStr}" /></td>
							<td><strong class="receipt">발급완료</strong></td>
							<td><a href="" name="showPopup" class="btn_style2 view" data-appl-seq-no="${item.applSeqNo}">보기</a></td>
							<td><c:out value="${item.regDateStr}" /></td>
						</tr>
					</c:forEach>
					<c:if test="${fn:length(list) eq '0'}">
						<tr><td colspan="15">조회된 데이터가 없습니다.</td></tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<!-- //table_area -->
		<div class="pagination">
			<ul>
	       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
			</ul>
		</div>
	
		<div class="button_area">
			<a href="<c:url value="/admin/status/applications/excel" />" class="btn_style1">엑셀저장</a>
		</div>
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/adm_footer.jsp" flush="true">
	<jsp:param name="js" value="adm_stat" />
</jsp:include>
