<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="sitemap" />
	<jsp:param name="nav1" value="사이트맵" />
</jsp:include>

<div class="container">
	<div class="sitemap_content" style="padding-bottom: 0;">
		<div class="cell cell1">
			<h2 class="tit">저작권대리중개업</h2>
			<ul>
				<li><a href="<c:url value="/appl/sub/new" />">신청하기</a></li>
				<li><a href="<c:url value="/appl/sub/edit" />">변경신청하기</a></li>
			</ul>
		</div>

		<div class="cell cell2">
			<h2 class="tit">저작권신탁관리업</h2>
			<ul>
				<li><a href="<c:url value="/appl/trust/new" />">신청하기</a></li>
				<li><a href="<c:url value="/appl/trust/edit" />">변경신청하기</a></li>
			</ul>
		</div>


		<div class="cell cell3">
			<h2 class="tit">실적보고</h2>
			<ul>
				<li><a href="<c:url value="/report/sub" />">대리중개업</a></li>
				<li><a href="<c:url value="/report/trust" />">신탁관리업</a></li>
			</ul>
		</div>

		<div class="cell cell4">
			<h2 class="tit">위탁관리저작물</h2>
			<ul>
				<li><a href="<c:url value="/report/excel" />">보고/수정</a></li>
				<li><a href="<c:url value="/report/excelstatus" />">보고현황</a></li>
				<li><a href="<c:url value="/report/excellist" />">등록현황</a></li>
			</ul>
			<%-- <div class="depth_n">
				<a href="<c:url value="/perm/intro1" />">공인인증서</a> <a href="<c:url value="/perm/intro2" />">관련저작권법</a> <a href="<c:url value="/faq" />">자주묻는질문(FAQ)</a>
			</div> --%>
		</div>

		<div class="cell cell5">
			<h2 class="tit">교육정보</h2>
			<ul>
				<li><a href="<c:url value="/online" />">온라인</a></li>
				<li><a href="<c:url value="/offline" />">오프라인</a></li>
			</ul>
		</div>

		<div class="cell cell3">
			<h2 class="tit">정보센터</h2>
			<ul>
				<li><a href="<c:url value="/law" />">법/제도</a></li>
				<li><a href="<c:url value="/trend" />">최신동향</a></li>
				<li><a href="<c:url value="/relation" />">관리프로그램</a></li>
			</ul>
		</div>

		<!-- //cell -->
	</div>
	<div class="sitemap_content">
		<div class="cell cell1">
			<h2 class="tit">알림마당</h2>
			<ul>
				<li><a href="<c:url value="/notice" />">공지사항</a></li>
				<li><a href="<c:url value="/perm/intro1" />">이용안내</a></li>
			</ul>
			<div class="depth_n">
				<a href="<c:url value="/perm/intro1" />">공인인증서</a> <a href="<c:url value="/perm/intro2" />">관련저작권법</a> <a href="<c:url value="/faq" />">자주묻는질문(FAQ)</a>
			</div>
		</div>

		

		<div class="cell cell5">
			<h2 class="tit">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보수정</a></li>
				<li><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">발급내역</a></li>
			</ul>
		</div>

		
	</div>

	<!-- //sitemap_content -->
</div>
<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="" />
</jsp:include>
