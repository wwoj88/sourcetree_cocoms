<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="login" />
	<jsp:param name="nav1" value="로그인" />
	<jsp:param name="nav2" value="비밀번호 찾기" />
</jsp:include>

	<div class="container">
		<h2 class="tit_bar blue mt">비밀번호 변경</h2>
		<div class="join_content">
			<div class="list_step_id">
				<!--  
				<ul>
					<li>내 명의로 가입된 휴대폰 인증</li>
					<li>내 정보에 등록된 휴대폰으로 찾기</li>
					<li>이메일로 찾기</li>
					<li class="active">공인인증서 인증</li>
					<li>사업자등록번호로 찾기</li>
				</ul>
				-->
			</div>
			<!-- //list_step -->	
			<form id="formChangePassword" action="<c:url value="/change/password" />" method="POST">
			<input type="hidden" name="loginId" value="${data.loginId}" />
			<input type="hidden" name="certSn" value="${data.certSn}" />
			<input type="hidden" name="memberSeqNo" value="${user.memberSeqNo}" />
			
			<div class="table_area list_top id_step">
				<table>
					<caption>비밀번호 찾기</caption>
					<colgroup>
						<col style="width:234px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th rowspan="2" style="border-bottom: 1px solid #000;">신규 비밀번호</th>
							<td>
								<input type="password" name="pwd" class="input_style1 w148" style="width: 300px;" maxlength="20" placeholder="새 비밀번호를 입력해주세요." />
								<span>영문/숫자/특수문자 8~20자</span>
							</td>
						</tr>
						<tr>
							<td>
								<input type="password" name="pwd2" class="input_style1 w148" style="width: 300px;" maxlength="20" placeholder="다시 한번 비밀번호를 입력해주세요." />
								<span>영문/숫자/특수문자 8~20자</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div class="button_area">
				<a href="" id="submit" class="btn btn_style2">완료</a>
			</div>
			</form>
		</div>
		<!-- //join_content -->	
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="login" />
	<jsp:param name="cert" value="true" />
</jsp:include>
