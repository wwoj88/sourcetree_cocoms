<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="결제내역" />
</jsp:include>
	
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<%-- <li><a href="<c:url value="/qna" />">1:1문의 내역</a></li> --%>
				<li class="active"><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage mypage_2">
			<h2 class="tit_bar mt">결제내역</h2>
			<div class="total_top_common">
				<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
			</div>
			<div class="table_area list board">
				<table>
					<caption>결제내역</caption>
					<colgroup>
						<col style="width:62px" />
						<col style="width:132px" />
						<col style="width:112px" />
						<col style="width:auto" />
						<col style="width:155px" />
						<col style="width:118px" />
						<col style="width:176px" />
					</colgroup>
					<thead>
						<tr> 	 	 	
							<th>번호</th>
							<th>처리일자</th>
							<th>처리시간</th>
							<th>결제내용</th>
							<th>결제금액</th>
							<th>결제방법</th>
							<th>처리상태</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr>
								<td><c:out value="${start - status.index}" /></td>
								<td><c:out value="${item.regDateStr}" /></td>
								<td><c:out value="${item.regTimeStr}" /></td>
								<td>
									<c:if test="${item.conditionCd eq '1'}">대리중개</c:if>
									<c:if test="${item.conditionCd eq '2'}">신탁</c:if>
									
									<c:if test="${item.statCd eq '1'}">신고신청</c:if>
									<c:if test="${item.statCd eq '3'}">변경신고신청</c:if>
								</td>
								<td><fmt:formatNumber value="${item.cappTot + item.pgFee}" type="number" /></td>
								<td>
									<c:if test="${item.payMthdCd eq '0402'}">카드결제</c:if>
									<c:if test="${item.payMthdCd eq '0403'}">계좌이체</c:if>
									<c:if test="${item.payMthdCd eq '0404'}">휴대폰</c:if>
									<c:if test="${item.payMthdCd eq '0407'}">카드포인트</c:if>
								</td>
								<td>
									<c:if test="${item.isSuccess eq '1'}">
										결제완료
									</c:if>
									<c:if test="${item.isSuccess eq '2'}">
										취소완료
									</c:if>
									<!--  
									<strong class="txt_blue">결제대기</strong><a href="" class="btn btn_style1 cancel">취소</a>
									-->
								</td>
							</tr>						
						</c:forEach>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<form id="searchForm" method="GET">
				<input type="hidden" name="pageIndex" value="${page}" />
			
				<div id="pagination" class="pagination">
		       		<ul><ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" /></ul>
				</div>
			</form>			
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 