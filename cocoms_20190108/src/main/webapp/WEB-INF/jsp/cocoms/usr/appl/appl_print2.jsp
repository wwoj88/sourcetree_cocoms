<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">
</head>
<body>
<div class="declaration_preview pop">
	<div class="inner">
		<div class="preview">
			<p class="num_top">처리기간 : 4일</p>
			<h1 class="tit">저작권대리중개업 변경신고서</h1>
			<div class="table_preview">
				<table summary="신고서 미리보기">
					<caption></caption>
					<colgroup>
						<col style="width:87px" />
						<col style="width:129px" />
						<col style="width:222px" />
						<col style="width:158px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th class="bg" rowspan="3">신고인</th>
							<th>성명<p>(단체 또는 법인명)</p></th>
							<td><c:out value="${member.coName}"  escapeXml="false"/></td>
							<th>생년월일<p>(법인/사업자등록번호)</p></th>
							<td>
								<c:if test="${!empty member.bubNo}"><c:out value="${member.bubNoStr}" /></c:if>
								<c:if test="${empty member.bubNo}"><c:out value="${member.coNoStr}" /></c:if>
								   
							</td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td><c:out value="${member.trustTel}" /></td>
							<th>팩스번호</th>
							<td><c:out value="${member.trustFax}" /></td>
						</tr>
						<tr>
							<th>주소</th>
							<td colspan="3" class="txt_l">
								<c:if test="${!empty member.trustZipcode}">(<c:out value="${member.trustZipcode}" />)</c:if>
								<c:out value="${member.trustSido}" />
								<c:out value="${member.trustGugun}" />
								<c:out value="${member.trustDong}" />
								<c:out value="${member.trustBunji}" />
								<c:out value="${member.trustDetailAddr}" />
							</td>
						</tr>
						<tr>
							<th class="bg" rowspan="3">대표자<p>(법인 또는<br>단체에 한함)</p></th>
							<th>성명</th>
							<td><c:out value="${member.ceoName}" /></td>
							<th>생년월일</th>
							<td><c:out value="${birthday}" /></td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td><c:out value="${member.ceoTel}" /></td>
							<th>이메일 주소</th>
							<td><c:out value="${member.ceoEmail}" /></td>
						</tr>
						<tr>
							<th>주소</th>
							<td colspan="3" class="txt_l">
								<c:if test="${!empty member.ceoZipcode}">(<c:out value="${member.ceoZipcode}" />)</c:if>
								<c:out value="${member.ceoSido}" />
								<c:out value="${member.ceoGugun}" />
								<c:out value="${member.ceoDong}" />
								<c:out value="${member.ceoBunji}" />
								<c:out value="${member.ceoDetailAddr}" />
							</td>
						</tr>
					</tbody>
				</table>
				<table summary="신고서 미리보기" class="bt0">
					<caption></caption>
					<colgroup>
						<col style="width:150px" />
						<col style="width:225px" />
						<col style="width:225px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th class="bg" rowspan="2">변경사항</th>
							<th>변경 전<span>(을)</span></th>
							<th>변경 후<span>(으로)</span></th>
							<th>변경사유</th>
						</tr>
							<tr>
									<td class="txt_l">
										<c:forEach items="${member.chgCd}" var="list">
		                         		 	<c:choose>
														<c:when test="${list eq '1'}">
														   대표자성명 : ${premember.ceoName}<br>
														   주민등록번호 : ${premember.ceoRegNo1} - ${premember.ceoRegNo2}<br>
														   이메일 : ${premember.ceoEmail}<br>
														   대표자주소 :  ${premember.ceoSido} ${premember.ceoGugun} ${premember.ceoDong} ${premember.ceoBunji} ${premember.ceoDetailAddr}<br>
														   대표자전화번호 : ${premember.ceoTel1}-${premember.ceoTel2}-${premember.ceoTel3}<br>
														</c:when>
														<c:when test="${list eq '2'}">
														   * 업체명 : ${premember.coName}<br>
														</c:when>
														<c:when test="${list eq '3'}">
														   * 주소 :	${premember.trustSido} ${premember.trustGugun} ${premember.trustDong} ${premember.trustBunji} <br>
														   			${premember.trustDetailAddr} <br>
														 </c:when>
														<c:when test="${list eq '6'}">
														  * 취급저작물 : <br>
																			   ${prewritingKind}<br>
														  * 취급저작권종류 : <br>						   
														   						${prepermKind}<br>
														                       
													     </c:when>
														<c:when test="${list eq '7'}">
														    임원변경
													    </c:when>
												</c:choose>
		                             </c:forEach>
		 
									</td>
							
									<td class="txt_l">
		                                  <c:forEach items="${member.chgCd}" var="list">
		                         		 	<c:choose>
														<c:when test="${list eq '1'}">
														   대표자성명 : ${member.ceoName}<br>
														   주민등록번호 : ${member.ceoRegNo1} - ${member.ceoRegNo2}<br>
														   이메일 : ${member.ceoEmail}<br>
														   대표자주소 :  ${member.ceoSido} ${member.ceoGugun} ${member.ceoDong} ${member.ceoBunji} ${member.ceoDetailAddr}<br>
														   대표자전화번호 : ${member.ceoTel1}-${member.ceoTel2}-${member.ceoTel3}<br>
														</c:when>
														<c:when test="${list eq '2'}">
														   * 업체명 : ${member.coName}<br>
														</c:when>
														<c:when test="${list eq '3'}">
														   * 주소 :	${member.trustSido} ${member.trustGugun} ${member.trustDong} ${member.trustBunji} ${member.trustDetailAddr} <br>
														 </c:when>
														<c:when test="${list eq '6'}">
														   * 취급저작물 : <br>
														   						 ${postwritingKind}<br>
														   * 취급저작권종류 : <br>																   						 
														                          ${postpermKind}<br>
													     </c:when>
														<c:when test="${list eq '7'}">
														    임원변경
													    </c:when>
												</c:choose>
		                             </c:forEach> 
		                             </td>
		                             <td>
		                             ${member.chgMemo}
		                             </td>
								</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_preview -->
			<p class="txt1"><strong>[저작권법 시행령] </strong> 제48조제3항에 따라 위와 같이 신고합니다.</p>
			<div class="sign">
				<p>
					 <c:if test="${!empty member.regDate2}"> 
									<c:out value="${fn:substring(member.regDate2, 0, 4)}" />년 
									<c:out value="${fn:substring(member.regDate2, 4, 6)}" />월 
									<c:out value="${fn:substring(member.regDate2, 6, 8)}" />일
								</c:if> 
								<c:if test="${empty member.regDate2}"> 
								<jsp:useBean id="toDay" class="java.util.Date" />
								<fmt:formatDate value='${toDay}' pattern='yyyyMMdd' var="nowDate"/>
								  <c:out value="${fn:substring(nowDate,0,4)}" />년  
								  <c:out value="${fn:substring(nowDate,4,6)}" />월   
								  <c:out value="${fn:substring(nowDate,6,8)}" />일   
								 </c:if> 
				</p>
				<p>신청인   <c:out value="${member.ceoName}" />   (서명 또는 인)</p>
			</div>
			<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
			<div class="table_preview">
				<table>
					<caption></caption>
					<colgroup>
						<col style="width:87px" />
						<col style="width:auto" />
						<col style="width:266px" />
						<col style="width:134px" />
					</colgroup>
					<tbody>
						<tr>
							<th class="bg" rowspan="2">구비서류</th>
							<th>민원인 제출서류</th>
							<th>담당공무원 확인사항</th>
							<th>수수료</th>
						</tr>
						<tr>
							<td>
								신고증

							</td>
							<td>변경사항을 증명하는 서류 1부</td>
							<td>3,000원</td>
						</tr>
							<tr>
							<c:if test="${member.procType eq 'sub_modify' or  member.procType eq 'trust_modify' }">
							<th class="bg">첨부파일</th>
							<td colspan="3" class="txt_l">
								<div class="file_change">
						 			<%-- <c:if test="${!empty rpMgm.file1Path}">
										<a href="<c:url value="/download?filename=${rpMgm.file1Path}" />"><p class="txt1">1. 저작권대리중개업 업무규정</p></a>
									</c:if>												
									<c:if test="${!empty rpMgm.file2Path}">
										<a href="<c:url value="/download?filename=${rpMgm.file2Path}" />"><p class="txt1">2. 신고인(단체 또는 법인인 경우에는 그 대표자및 임원)의 이력서</p></a>
									</c:if>												
									<c:if test="${!empty rpMgm.file3Path}">
										<a href="<c:url value="/download?filename=${rpMgm.file3Path}" />"><p class="txt1">3. 정관 또는 규약 1부</p></a>
									</c:if>												
									<c:if test="${!empty rpMgm.file4Path}">
										<a href="<c:url value="/download?filename=${rpMgm.file4Path}" />"><p class="txt1">4. 재무제표(법인단체)</p></a>
									</c:if>												
									 --%>
									<c:forEach var="item" items="${chgHistoryFileList}" varStatus="status">
										<a href="<c:url value="/download?filename=${item.filepath}" />">
											<p class="txt1">변경신고신청 첨부<c:out value="${status.count}" /></p>
											<p class="txt2"><c:out value="${item.filename}" /></p>
											<p class="txt3">(<c:out value="${item.regDateStr}" />)</p>
										</a>
									</c:forEach>
								</div>
								</td>
								</c:if>
						<c:if test="${member.procType eq 'sub_regist' or  member.procType eq 'trust_regist' }">
							<th class="bg">첨부파일</th>
							<td colspan="3" class="txt_l">
								<div class="file_change">
									<c:if test="${!empty member.file1Path}">
										<a href="<c:url value="/download?filename=${member.file1Path}" />"><p class="txt1">1. 저작권대리중개업 업무규정</p></a>
									</c:if>												
									<c:if test="${!empty member.file2Path}">
										<a href="<c:url value="/download?filename=${member.file2Path}" />"><p class="txt1">2. 신고인(단체 또는 법인인 경우에는 그 대표자및 임원)의 이력서</p></a>
									</c:if>												
									<c:if test="${!empty member.file3Path}">
										<a href="<c:url value="/download?filename=${member.file3Path}" />"><p class="txt1">3. 정관 또는 규약 1부</p></a>
									</c:if>												
									<c:if test="${!empty member.file4Path}">
										<a href="<c:url value="/download?filename=${member.file4Path}" />"><p class="txt1">4. 재무제표(법인단체)</p></a>
									</c:if>												
								</div>
								</td>
							</c:if>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- //table_preview -->
		</div>
		<!-- //preview -->
	</div>
	<div class="btn_area">
		<a href="javascript:window.print();" class="btn btn_style2">출력</a>
	</div>
</div>
<!-- //declaration_preview -->

<script src="<c:url value='/js/jquery-1.10.2.min.js' />"></script>
<script src="<c:url value='/js/jquery.bxslider.js' />"></script>
<script src="<c:url value='/js/script.js' />"></script>
</body>
</html>
