<%-- <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %> --%>
<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt" %>

<input type="hidden" id="procType" value="${member.procType}" />

<c:choose>
	<c:when test="${member.procType eq 'sub_regist'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value=""/>
			<jsp:param name="type" value="sub" />
			<jsp:param name="nav1" value="저작권대리중개업" />
			<jsp:param name="nav2" value="신청하기" />
		</jsp:include>
	</c:when>
	<c:when test="${member.procType eq 'trust_regist'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value=""/>
			<jsp:param name="type" value="trust" />
			<jsp:param name="nav1" value="저작권신탁관리업 " />
			<jsp:param name="nav2" value="신청하기" />
		</jsp:include>
	</c:when>
	<c:when test="${member.procType eq 'sub_modify'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value=""/>
			<jsp:param name="type" value="sub" />
			<jsp:param name="nav1" value="저작권대리중개업 " />
			<jsp:param name="nav2" value="변경" />
		</jsp:include>	
	</c:when>
	<c:when test="${member.procType eq 'trust_modify'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value=""/>
			<jsp:param name="type" value="trust" />
			<jsp:param name="nav1" value="저작권신탁관리업 " />
			<jsp:param name="nav2" value="변경" />
		</jsp:include>
	</c:when>
</c:choose>
	
	<div class="container">
		<div class="box_top_common">
			<ul>
				<li>법정 민원을 신청할 때 전자우편 및 휴대폰 번호를 기재하시면 민원접수 및 처리결과를 전자우편 및 휴대폰으로 알려 드립니다.</li>
				<li>온라인 첨부가능 표시가 있는 구비서류는 컴퓨터 파일로 첨부가 가능한 서류이며, 스캔한 파일 온라인 첨부가능 표시가 있는 구비서류는 원본 서류를 스캔하여 컴퓨터 파일로 첨부가 가능한 서류입니다.</li>
				<li>온라인으로 첨부할 수 없는 서류는 방문 및 우편으로 제출할 수 있습니다. </li>
			</ul>
		</div>
		<!-- //box_top_common -->
		<c:choose>
			<c:when test="${member.procType eq 'sub_regist'}">
				<h2 class="tit_bar blue">대리중개업신고 신청서</h2>
			</c:when>
			<c:when test="${member.procType eq 'trust_regist'}">
				<h2 class="tit_bar green">신탁허가 신청하기</h2>
			</c:when>
			<c:when test="${member.procType eq 'sub_modify'}">
				<h2 class="tit_bar blue">대리중개업신고 변경 신청하기</h2>
			</c:when>
			<c:when test="${member.procType eq 'trust_modify'}">
				<h2 class="tit_bar green">신탁허가 변경 신청하기</h2>
			</c:when>
		</c:choose>
		<div class="form_apply">
			<div class="aside">
				<div class="table_area">
					<table>
						<caption> 회원정보</caption>
						<thead>
							<tr> 	 	 	
								<th> 회원정보</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>
									 회원구분 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${member.coGubunCd eq '1'}"><c:set var="coGubunCd1Checked" value="checked" /></c:if>
									<c:if test="${member.coGubunCd eq '2'}"><c:set var="coGubunCd2Checked" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="checkbox" id="checkbox1" ${coGubunCd1Checked} disabled />
										<label for="checkbox1">개인사업자</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="checkbox2" ${coGubunCd2Checked} disabled />
										<label for="checkbox2">법인사업자</label> 
									</div>
								</td>
							</tr>
							<tr>
								<th>
									 사업자 등록번호
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${member.coNo1}" title="사업자번호 첫번째자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num1 disabled" value="${member.coNo2}" title="사업자번호 두째자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num2 disabled" value="${member.coNo3}" title="사업자번호 세째자리" disabled />
								</td>
							</tr>
							<c:if test="${member.coGubunCd eq '2'}">
								<tr>
									<th>
										 법인 등록번호
									</th>
								</tr>
								<tr>
									<td>
										<input type="text" name="bubNo1" class="input_style1 resident disabled" value="${member.bubNo1}" title="법인 등록번호 앞자리" disabled /><span>-</span
												><input type="text" name="bubNo2" class="input_style1 resident disabled" value="${member.bubNo2}" title="법인 등록번호 뒷자리" disabled />
									</td>
								</tr>
							</c:if>							
							<tr>
								<th>
									주소 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 address1 disabled" value="${member.trustZipcode}" placeholder="" title="주소입력칸 1" disabled />
									<input type="text" class="input_style1 margin disabled" value="${member.trustSido} ${member.trustGugun} ${member.trustDong} ${member.trustBunji}" placeholder="" title="주소입력칸 2" disabled />
									<input type="text" class="input_style1 disabled" value="${member.trustDetailAddr}" placeholder="" title="주소입력칸 3" disabled />
								</td>
							</tr>
							<tr>
								<th>
									전화번호 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${member.trustTel1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.trustTel2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.trustTel3}" placeholder="" title="전화번호 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									팩스번호 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${member.trustFax1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.trustFax2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.trustFax3}" placeholder="" title="전화번호 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									홈페이지 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 disabled" value="${member.trustUrl}" placeholder="" title="홈페이지 주소입력칸" disabled />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="table_area">
					<table>
						<caption> 대표자정보</caption>
						<thead>
							<tr> 	 	 	
								<th>대표자정보</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>
									 대표자명 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 disabled" value="${member.ceoName}" placeholder="" title="대표자명" disabled />
								</td>
							</tr>
							<tr>
								<th>
									 주민등록번호 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 resident disabled" 
											value="${member.ceoRegNo1}" placeholder="" title="주민등록번호 앞자리" disabled /><span>-</span><input type="password" class="input_style1 resident disabled" value="${member.ceoRegNo2}" placeholder="" title="주민등록번호 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									주소 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 address1 disabled" value="${member.ceoZipcode}" placeholder="" title="주소입력칸 1" disabled />
									<input type="text" class="input_style1 margin disabled" value="${member.ceoSido} ${member.ceoGugun} ${member.ceoDong} ${member.ceoBunji}" placeholder="" title="주소입력칸 2" disabled />
									<input type="text" class="input_style1 disabled" value="${member.ceoDetailAddr}" placeholder="" title="주소입력칸 3" disabled />
								</td>
							</tr>
							<tr>
								<th>
									전화번호 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${member.ceoTel1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.ceoTel2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.ceoTel3}" placeholder="" title="전화번호 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									팩스번호 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${member.ceoFax1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.ceoFax2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.ceoFax3}" placeholder="" title="전화번호 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									휴대전화번호 <strong class="txt_point"></strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 num1 disabled" value="${member.ceoMobile1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.ceoMobile2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${member.ceoMobile3}" placeholder="" title="전화번호 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									이메일

								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 resident disabled" 
											value="${member.ceoEmail1}" placeholder="" title="이메일 주소 앞자리" disabled /><span>@</span><input type="text" class="input_style1 resident disabled" 
											value="${member.ceoEmail2}"placeholder="" title="이메일 주소 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									 SMS수신동의
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${member.smsAgree eq 'Y'}"><c:set var="smsAgreeYChecked" value="checked" /></c:if>
									<c:if test="${member.smsAgree ne 'Y'}"><c:set var="smsAgreeNChecked" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" ${smsAgreeYChecked} disabled />
										<label for="">동의</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" ${smsAgreeNChecked} disabled />
										<label for="">동의안함</label> 
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
			</div>
			<!-- //aside -->
			<div class="cont">
				<div class="table_area">
					<table>
						<caption>신고서 미리보기</caption>
						<thead>
							<tr> 	 	 	
								<th>신고서 미리보기</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- //table_area -->
				<div class="declaration_preview">
					<div class="inner">
						<div class="preview">
							<p class="num_top">처리기간 :
									<c:choose>
									<c:when test="${member.procType eq 'sub_modify'}">
										4일
									</c:when>
									<c:when test="${member.procType eq 'trust_modify'}">
									    4일
									</c:when>
									<c:when test="${member.procType eq 'sub_regist'}">
										5일
									</c:when>
									<c:when test="${ member.procType eq 'trust_regist'}">
										15일
									</c:when>
								</c:choose>
								 </p>
								<c:choose>
									<c:when test="${member.procType eq 'sub_modify'}">
										<h1 class="tit">저작권대리중개업 변경신고서</h1>
									</c:when>
									<c:when test="${member.procType eq 'trust_modify'}">
									    <h1 class="tit">저작권신탁관리업 변경신고서</h1>
									</c:when>
									<c:when test="${member.procType eq 'sub_regist'}">
										<h1 class="tit">저작권대리중개업 신고서</h1>
									</c:when>
									<c:when test="${ member.procType eq 'trust_regist'}">
										<h1 class="tit">저작권신탁관리업 허가신청서</h1>
									</c:when>
								</c:choose>
    						<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:129px" />
										<col style="width:222px" />
										<col style="width:158px" />
										<col style="width:auto" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="3">신고인</th>
											<th>성명<p>(단체 또는 법인명)</p></th>
											<td><c:out value="${member.coName}" /></td>
											<th>생년월일<p>(법인/사업자등록번호)</p></th>
											<td>
												<c:if test="${!empty member.bubNo}"><c:out value="${member.bubNoStr}" /></c:if>
												<c:if test="${empty member.bubNo}"><c:out value="${member.coNoStr}" /></c:if>
											</td>
										</tr>
										<tr>
											<th>전화번호</th>
											<td><c:out value="${member.trustTel}" /></td>
											<th>팩스번호</th>
											<td><c:out value="${member.trustFax}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">
												<c:if test="${!empty member.trustZipcode}">(<c:out value="${member.trustZipcode}" />)</c:if>
												<c:out value="${member.trustSido}" />
												<c:out value="${member.trustGugun}" />
												<c:out value="${member.trustDong}" />
												<c:out value="${member.trustBunji}" />
												<c:out value="${member.trustDetailAddr}" />
											</td>
										</tr>
										<tr>
											<th class="bg" rowspan="3">대표자<p>(법인 또는<br>단체에 한함)</p></th>
											<th>성명</th>
											<td><c:out value="${member.ceoName}" /></td>
											<th>생년월일</th>
											<td><c:out value="${birthday}" /></td>
    									</tr>
										<tr>
											<th>전화번호</th>
											<td><c:out value="${member.ceoTel}" /></td>
											<th>이메일 주소</th>
											<td><c:out value="${member.ceoEmail}" /></td>
										</tr>
										<tr>
											<th>주소</th>
											<td colspan="3" class="txt_l">
												<c:if test="${!empty member.ceoZipcode}">(<c:out value="${member.ceoZipcode}" />)</c:if>
												<c:out value="${member.ceoSido}" />
												<c:out value="${member.ceoGugun}" />
												<c:out value="${member.ceoDong}" />
												<c:out value="${member.ceoBunji}" />
												<c:out value="${member.ceoDetailAddr}" />
											</td>
										</tr>
									
										<c:if test="${member.procType eq 'sub_regist' }">
							    		<tr>
											<th height="50" colspan="2">취급 하고자 하는 업무의 내용</th>
												<c:if test="${member.conDetailCd eq '1'}">
							        				<c:set var="conDetailCd1" value="■" />
									        		<c:set var="conDetailCd2" value="□" />
										        </c:if>
												<c:if test="${member.conDetailCd eq '2'}">
													<c:set var="conDetailCd1" value="□" />
													<c:set var="conDetailCd2" value="■" />
												</c:if>
												<c:if test="${member.conDetailCd eq '3'}">
													<c:set var="conDetailCd1" value="■" />
													<c:set var="conDetailCd2" value="■" />
												</c:if>
											<td colspan="3" class="txt_l">
												<div class="btm_form_box">
											    	${conDetailCd1}<%-- 	<input type="checkbox" id="conDetailCd1" disabled value="1" ${conDetailCd1} /> --%>
													<label for="conDetailCd1">대리</label> 
												</div>
												<div class="btm_form_box">
												    ${conDetailCd2}<%--  <input type="checkbox" id="conDetailCd2" disabled value="2" ${conDetailCd2} /> --%>
												    <label for="conDetailCd2">중개</label> 
										        </div>
									    	</td>
										 </tr>
										
										<tr>
											<th height="50" colspan="2">신고하는 업무의 내용</th>
											<td colspan="3" class="txt_l"><c:out value="${postwritingKind}" /></td>
										</tr>
										<tr>
											<th height="50" colspan="2">취급하고자 하는 권리<p>(주요 저작물 중심으로 중복선택 가능)</p></th>
											<td colspan="3" class="txt_l"><c:out value="${postpermKind}" /></td>
										</tr>	
										</c:if>										
     								</tbody>
								</table>
								<c:if test="${member.procType eq 'sub_modify' or member.procType eq 'trust_modify'}">
							 		<table>
										<caption></caption>
										<colgroup>
											<col style="width:87px" />
											<col style="width:auto" />
											<col style="width:266px" />
											<col style="width:200px" />
										</colgroup>
										<tbody>
											<tr>
												<th class="bg" rowspan="2">변경사항</th>
												<th>변경 전<span>(을)</span></th>
												<th>변경 후<span>(으로)</span></th>
												<th>변경사유</th>
											</tr>
											<tr>
												<td class="txt_l">
													<c:forEach items="${member.chgCd}" var="list">
					                         		 	<c:choose>
																	<c:when test="${list eq '1'}">
																	   대표자성명 : ${premember.ceoName}<br>
																	   주민등록번호 : ${premember.ceoRegNo1} - ${premember.ceoRegNo2}<br>
																	   이메일 : ${premember.ceoEmail}<br>
																	   대표자주소 :  ${premember.ceoSido} ${premember.ceoGugun} ${premember.ceoDong} ${premember.ceoBunji} ${premember.ceoDetailAddr}<br>
																	   대표자전화번호 : ${premember.ceoTel1}-${premember.ceoTel2}-${premember.ceoTel3}<br>
																	</c:when>
																	<c:when test="${list eq '2'}">
																	   업체명 : ${premember.coName}<br>
																	</c:when>
																	<c:when test="${list eq '3'}">
																	   주소 :	${premember.trustSido} ${premember.trustGugun} ${premember.trustDong} ${premember.trustBunji} <br>
																	   			${premember.trustDetailAddr} <br>
																	 </c:when>
																	<c:when test="${list eq '6'}">
																	   취급저작물 : <br>
																						   ${prewritingKind}<br>
																	   취급저작권종류 : <br>  
																	   						${prepermKind}<br>
																	                       
																     </c:when>
																	<c:when test="${list eq '7'}">
																	    임원변경
																    </c:when>
															</c:choose>
					                             </c:forEach>
					 
												</td>
										
												<td class="txt_l">
					                                  <c:forEach items="${member.chgCd}" var="list">
					                         		 	<c:choose>
																	<c:when test="${list eq '1'}">
																	   대표자성명 : ${member.ceoName}<br>
																	   주민등록번호 : ${member.ceoRegNo1} - ${member.ceoRegNo2}<br>
																	   이메일 : ${member.ceoEmail}<br>
																	   대표자주소 :  ${member.ceoSido} ${member.ceoGugun} ${member.ceoDong} ${member.ceoBunji} ${member.ceoDetailAddr}<br>
																	   대표자전화번호 : ${member.ceoTel1}-${member.ceoTel2}-${member.ceoTel3}<br>
																	</c:when>
																	<c:when test="${list eq '2'}">
																	   업체명 : ${member.coName}<br>
																	</c:when>
																	<c:when test="${list eq '3'}">
																	   주소 :	${member.trustSido} ${member.trustGugun} ${member.trustDong} ${member.trustBunji} ${member.trustDetailAddr} <br>
																	 </c:when>
																	<c:when test="${list eq '6'}">
																	   취급저작물 : <br>
																	   						 ${postwritingKind}<br>
																	  취급저작권종류 : <br>
																	                          ${postpermKind}<br>
																     </c:when>
																	<c:when test="${list eq '7'}">
																	    임원변경
																    </c:when>
															</c:choose>
					                             </c:forEach> 
					                             </td>
					                             <td>
					                             ${member.chgMemo}
					                             </td>
											</tr>
										</tbody>
								   </table>
								</c:if>
							</div>
							<!-- //table_preview -->
							<p class="txt1"><strong>[저작권법]</strong> 제 105조 제 1항 및 같은 법 시행령 제 48조에 따라 위와 같이 신고합니다.</p>
							<div class="sign">
								<p>
							 <c:if test="${!empty member.regDate2}"> 
									<c:out value="${fn:substring(member.regDate2, 0, 4)}" />년 
									<c:out value="${fn:substring(member.regDate2, 4, 6)}" />월 
									<c:out value="${fn:substring(member.regDate2, 6, 8)}" />일
								</c:if> 
								<c:if test="${empty member.regDate2}"> 
								<jsp:useBean id="toDay" class="java.util.Date" />
								<fmt:formatDate value='${toDay}' pattern='yyyyMMdd' var="nowDate"/>
								  <c:out value="${fn:substring(nowDate,0,4)}" />년  
								  <c:out value="${fn:substring(nowDate,4,6)}" />월   
								  <c:out value="${fn:substring(nowDate,6,8)}" />일   
								 </c:if> 
								</p>
								<p>신청인   <c:out value="${member.ceoName}" />   (서명 또는 인)</p>
							</div>
							<p class="txt2"><strong>문화체육관광부장관</strong> 귀하</p>
							<div class="table_preview">
								<table>
									<caption></caption>
									<colgroup>
										<col style="width:87px" />
										<col style="width:auto" />
										<col style="width:266px" />
										<col style="width:134px" />
									</colgroup>
									<tbody>
										<tr>
											<th class="bg" rowspan="2">구비서류</th>
											<th>민원인 제출서류</th>
											<th>담당공무원 확인사항</th>
											<th>수수료</th>
										</tr>
										<tr>
											<td class="txt_l">
											신고증
											</td>
											<td>변경사항을 증명하는 서류 1부</td>
											<c:if test="${member.procType eq 'sub_regist'}">
												<td>5,000원</td>
											</c:if>
											<c:if test="${member.procType eq 'sub_modify'}">
												<td>3,000원</td>
											</c:if>
											<c:if test="${member.procType eq 'trust_regist'}">
												<td>10,000원</td>
											</c:if>
											<c:if test="${member.procType eq 'trust_modify'}">
												<td>3,000원</td>
											</c:if>
										</tr>
										<tr>
										<c:if test="${member.procType eq 'sub_modify' or  member.procType eq 'trust_modify' }">
											<th class="bg">첨부파일</th>
											<td colspan="3" class="txt_l">
												<div class="file_change">
										 			<%-- <c:if test="${!empty rpMgm.file1Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file1Path}" />"><p class="txt1">1. 저작권대리중개업 업무규정</p></a>
													</c:if>												
													<c:if test="${!empty rpMgm.file2Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file2Path}" />"><p class="txt1">2. 신고인(단체 또는 법인인 경우에는 그 대표자및 임원)의 이력서</p></a>
													</c:if>												
													<c:if test="${!empty rpMgm.file3Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file3Path}" />"><p class="txt1">3. 정관 또는 규약 1부</p></a>
													</c:if>												
													<c:if test="${!empty rpMgm.file4Path}">
														<a href="<c:url value="/download?filename=${rpMgm.file4Path}" />"><p class="txt1">4. 재무제표(법인단체)</p></a>
													</c:if>												
													 --%>
													<c:forEach var="item" items="${chgHistoryFileList}" varStatus="status">
														<a href="<c:url value="/download?filename=${item.filepath}" />">
															<p class="txt1">변경신고신청 첨부<c:out value="${status.count}" /></p>
															<p class="txt2"><c:out value="${item.filename}" /></p>
															<p class="txt3">(<c:out value="${item.regDateStr}" />)</p>
														</a>
													</c:forEach>
												</div>
												</td>
												</c:if>
										<c:if test="${member.procType eq 'sub_regist' or  member.procType eq 'trust_regist' }">
											<th class="bg">첨부파일</th>
											<td colspan="3" class="txt_l">
												<div class="file_change">
													<c:if test="${!empty member.file1Path}">
														<a href="<c:url value="/download?filename=${member.file1Path}" />"><p class="txt1">1. 저작권대리중개업 업무규정</p></a>
													</c:if>												
													<c:if test="${!empty member.file2Path}">
														<a href="<c:url value="/download?filename=${member.file2Path}" />"><p class="txt1">2. 신고인(단체 또는 법인인 경우에는 그 대표자및 임원)의 이력서</p></a>
													</c:if>												
													<c:if test="${!empty member.file3Path}">
														<a href="<c:url value="/download?filename=${member.file3Path}" />"><p class="txt1">3. 정관 또는 규약 1부</p></a>
													</c:if>												
													<c:if test="${!empty member.file4Path}">
														<a href="<c:url value="/download?filename=${member.file4Path}" />"><p class="txt1">4. 재무제표(법인단체)</p></a>
													</c:if>												
												</div>
												</td>
											</c:if>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- //table_preview -->
						</div>
						<!-- //preview -->
					</div>
					
					<form id="form">
						<input type="hidden" name="source" />
						<input type="hidden" name="userDn" />
						<input type="hidden" name="certSn" value="${cert.certSn}" />
					</form>
					
					<div class="btn_area left" style="margin: 25px">
						<a id="print" class="btn btn_style2">출력</a>
					</div>
					<div class="btn_area right" style="margin: 25px">
						<c:choose>
							<c:when test="${member.procType eq 'sub_regist'}">
								<a onclick="check(1);"  class="btn btn_style1">수정</a>
							</c:when>
							<c:when test="${member.procType eq 'trust_regist'}">
								<a  onclick="check(2);" class="btn btn_style1">수정</a>
							</c:when>
							<c:when test="${member.procType eq 'sub_modify'}">
								<a  onclick="check(3);"  class="btn btn_style1">수정</a>
							</c:when>
							<c:when test="${member.procType eq 'trust_modify'}">
								<a  onclick="check(4);" class="btn btn_style1">수정</a>
							</c:when>
						</c:choose>
						<c:choose>
						<c:when test ="${premember.statCd eq '7'or premember.statCd eq '9'}">
						<a href="javascript:;" id="resubmit" class="btn_style2">보완완료</a>
						</c:when>
						<c:otherwise>
						<a id="charge" class="btn btn_style2">결제</a>
						</c:otherwise>
						</c:choose>
					</div>
				</div>
				<!-- //cont -->
			</div>
			<!-- //cont -->
		</div>
		<!-- //form_apply -->
	</div>
	<!-- //container -->

<div id="chargeForm" class="pop_common w500" style="display: none;">
	<div class="inner">
		<h2 class="tit txt_l">저작권위탁관리업시스템 결제정보</h2>
		<a href="javascript:;" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
		<div class="cont line">
			<form>
				<div class="table_area list list_type1">
					<table>
						<caption></caption>
						<colgroup>
							<col style="width:109px" />
							<col style="width:auto" />
						</colgroup>
						<tbody>
							<tr>
								<th>결제업무</th>
								<td>대리중개업/신탁업 신고</td>
							</tr>
							<tr>
								<th>결제금액</th>
								<td>
									<c:choose>
										<c:when test="${member.procType eq 'sub_regist'}">
											4,160원
											<input type="hidden" id="cappTot" value="4000" />
											<input type="hidden" id="pgFee" value="160" />
											<input type="hidden" id="remark" value="저작권위탁관리시스템 대리중개업 신고 ${member.coName}"/>
											<input type="hidden" id="conditionCd" value="1" />
											<input type="hidden" id="hddReqManName" value="${member.ceoName}"/>
											<input type="hidden" id="statCd" value="1" />
										</c:when>
										<c:when test="${member.procType eq 'trust_regist'}">
											9,360원
											<input type="hidden" id="cappTot" value="9000" />
											<input type="hidden" id="pgFee" value="360" />
					    					<input type="hidden" id="remark" value="문화체육관광부 저작권위탁관리시스템 신고 ${member.coName}"/>
											<input type="hidden" id="conditionCd" value="2" />
											<input type="hidden" id="hddReqManName" value="${member.ceoName}"/>
											<input type="hidden" id="statCd" value="1" />
										</c:when>
										<c:when test="${member.procType eq 'sub_modify'}">
											2,090원
											<input type="hidden" id="cappTot" value="2000" />
											<input type="hidden" id="pgFee" value="90" />
											<!-- 
											<input type="hidden" id="remark" value="대리중개업 신고 변경" />
											 -->
											<input type="hidden" id="remark" value="문화체육관광부 저작권위탁관리시스템 중개변경 ${member.coName}" />
											<input type="hidden" id="conditionCd" value="1" />
											<input type="hidden" id="hddReqManName" value="${member.ceoName}"/>
											<input type="hidden" id="statCd" value="3" />
										</c:when>
										<c:when test="${member.procType eq 'trust_modify'}">
											3,120원
											<input type="hidden" id="cappTot" value="3000" />
											<input type="hidden" id="pgFee" value="120" />
											<!-- 
											<input type="hidden" id="remark" value="신탁업 허가 변경" />
											 -->
											<input type="hidden" id="remark" value="문화체육관광부 저작권위탁관리시스템 신탁변경 ${member.coName}"/>
											<input type="hidden" id="conditionCd" value="2" />
											<input type="hidden" id="hddReqManName" value="${member.ceoName}"/>
											<input type="hidden" id="statCd" value="3" />
										</c:when>
									</c:choose>
								</td>
							</tr>
							<tr>
								<th>결제방법</th>
								<td>
									<div class="btm_form_box">
										<input type="radio" id="payMthdCd0402" name="payMthdCd" value="0402" checked />
										<label for="payMthdCd0402">카드결제</label>
									</div>
									<div class="btm_form_box">
										<input type="radio" id="payMthdCd0404" name="payMthdCd" value="0404" />
										<label for="payMthdCd0404">휴대폰</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<p class="txt_red txt_r">※ 휴대폰결제는 개인명의로된 휴대폰만 결제진행이 가능합니다.</p>
			</form>
			<div class="btn_area">
				<a href="javascript:;" name="popup_cancel" class="btn_style1">취소</a>
				<a href="javascript:;" id="submit" class="btn_style2">결제</a>
			</div>
			<%-- <div class="logt_b"><img src="<c:url value="img/logo_pop_1.png" />" alt="" /></div> --%>
		</div>
	</div>
</div>
<!-- //pop_common -->
<script>
function check(data) {
	
	if(confirm("수정시 기본정보 및 파일을 재작성해주세요.")==false){
		 return false;
	 }else{
		 if(data==1){location.href="/cocoms/appl/sub/new";}
		 if(data==2){location.href="/cocoms/appl/trust/new";}
		 if(data==3){location.href="/cocoms/appl/sub/edit";}
		 if(data==4){location.href="/cocoms/appl/trust/edit";}
		

	 }
	
}
</script>
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="js" value="appl" />
	<jsp:param name="cert" value="true" />
</jsp:include>
