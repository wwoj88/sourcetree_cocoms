<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
	<jsp:param name="title" value="" />
	<jsp:param name="type" value="mypage" />
	<jsp:param name="nav1" value="마이페이지" />
	<jsp:param name="nav2" value="1:1문의내역" />
</jsp:include>
	
	<div class="container">
		<div class="menu_aside">
			<h2 class="tit_bar my mt">마이페이지</h2>
			<ul>
				<li><a href="<c:url value="/mypage" />">회원정보 수정</a></li>
				<li><a href="<c:url value="/report" />">신고/허가 내역</a></li>
				<li class="active"><a href="<c:url value="/qna" />">1:1문의 내역</a></li>
				<li><a href="<c:url value="/charge" />">결제내역</a></li>
				<li><a href="<c:url value="/history" />">신고서 발급내역</a></li>
			</ul>
		</div>
		<!-- //menu_aside -->
		<div class="cont_notice mypage mypage_2 list_check">
			<h2 class="tit_bar mt">1:1문의내역</h2>
			<div class="total_top_common">
				<p class="total">전체 : <strong><c:out value="${total}" />건</strong></p>
			</div>
			
			<form id="searchForm" method="GET">
			<input type="hidden" name="pageIndex" value="${page}" />
			<input type="hidden" name="searchCondition" value="all" />
			
			<div class="table_area list board">
				<table id="data" summary="1:1문의내역">
					<caption>1:1문의내역</caption>
					<colgroup>
						<col style="width:44px" />
						<col style="width:65px" />
						<col style="width:auto" />
						<col style="width:91px" />
						<col style="width:132px" />
					</colgroup>
					<thead>
						<tr> 	 	 	
							<th>
								<div class="btm_form_box">
									<input type="checkbox" id="checkAll">
									<label for="checkAll">문의전체체크</label> 
								</div>
							</th>
							<th>번호</th>
							<th>제목</th>
							<th>상태</th>
							<th>작성일</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${list}" varStatus="status">
							<tr>
								<td>
									<div class="btm_form_box">
										<input type="checkbox" id="checkbox_${item.boardSeqNo}" name="boardSeqNoList" value="${item.boardSeqNo}" />
										<label for="checkbox_${item.boardSeqNo}">문의체크</label> 
									</div>
								</td>
								<td><c:out value="${start - status.index}" /></td>
								<td class="subject">
									<a href="<c:url value="/qna/${item.boardSeqNo}" />">
										<c:out value="${item.title}" />
									</a>
								</td>
								<td>
									<c:if test="${item.replyYn eq 'Y'}">
										<span class="btn btn_style2">답변완료</span>
									</c:if>
									<c:if test="${item.replyYn ne 'Y'}">
										<span class="btn btn_style1">접수중</span>
									</c:if>
								</td>
								<td><c:out value="${item.regDateStr}" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<!-- //table_area -->
			<div id="pagination" class="pagination">
	       		<ui:pagination paginationInfo = "${paginationInfo}" type="cocoms" jsFunction="fn_egov_link_page" />
			</div>
			<div class="button_area">
				<a href="" id="remove_checked" class="btn btn_style2">선택삭제</a>
			</div>
			
			</form>
		</div>
		<!-- //cont_notice -->		
	</div>
	<!-- //container -->
	
<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="js" value="mypage" />
</jsp:include>
 