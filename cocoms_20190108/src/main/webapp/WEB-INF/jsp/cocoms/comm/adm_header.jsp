<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value="/css/html5_reset.css" />">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/adm_layout.css" />">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/utill.css" />">
<%-- <link rel="shortcut icon" href="<c:url value='/img/favicon.ico' />"> --%>
<link rel="shortcut icon" href="<c:url value='/img/favicon.ico' />">
</head>
<body>
	<div class="wrap">
		<div class="info_top">
			<div class="inner">
				<ul>
					<li><a href="<c:url value="/main" />" target="_blank">홈페이지 가기</a></li>
					<li><a href="<c:url value="/admin/logout" />">관리자 로그아웃</a></li>
				</ul>
			</div>
		</div>
		<!-- //info_top -->
		<div class="container" style="width: 1400px;">
			<div class="aside_c">
				<h1 class="logo">
					<a href="<c:url value="/admin/main" />"><img src="<c:url value="/img/adm_logo.png" />" alt="저작권위탁관리업시스템" /></a>
				</h1>
				<div class="gnb">
					<ul id="menu">
						<li class="home"><a href="<c:url value="/admin/main" />">HOME</a></li>
						<li><a href="" class="tab">회원관리</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/members1" />">개인회원</a></li>
								<li><a href="<c:url value="/admin/members2" />">사업자회원</a></li>
							</ul></li>
						<li><a href="" class="tab">저작권중개업</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/sub/companies?a=1" />">업체현황 </a></li>
								<li><a href="<c:url value="/admin/sub/statuses" />">신고현황</a></li>
							</ul></li>
						<li><a href="" class="tab">저작권신탁관리업</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/trust/companies" />">업체현황 </a></li>
								<li><a href="<c:url value="/admin/trust/statuses" />">허가신청현황</a></li>
							</ul></li>
						<li><a href="" class="tab">실적보고</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/reports/sub" />">대리중개업체</a></li>
								<li><a href="<c:url value="/admin/reports/trust" />">신탁단체</a></li>
							</ul></li>
						<li><a href="" class="tab">위탁관리업저작물 관리</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/excel/statics" />">통계 </a></li>

							</ul></li>
						<li><a href="" class="tab">업체현황</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/status/companies" />">업체조회</a></li>
								<li><a href="<c:url value="/admin/status/stats" />">통계현황</a></li>
								<li><a href="<c:url value="/admin/status/applications" />">신고서발급내역</a></li>
							</ul></li>
						<li><a href="" class="tab">교육정보</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/online" />">온라인</a></li>
								<li><a href="<c:url value="/admin/offline" />">오프라인</a></li>
							</ul></li>
						<li><a href="" class="tab">정보센터</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/law" />">관련법제도</a></li>
								<li><a href="<c:url value="/admin/trend" />">최신동향</a></li>
								<li><a href="<c:url value="/admin/relation" />">관리 프로그램 배포</a></li>
							</ul></li>
						<li><a href="" class="tab">알림마당</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/notices" />">공지사항</a></li>
								<li><a href="<c:url value="/admin/faq" />">자주묻는질문(FAQ)</a></li>
							</ul></li>
						<li><a href="" class="tab">SMS/이메일 관리</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/sms/send" />">SMS/이메일 단체발송</a></li>
								<li><a href="<c:url value="/admin/sms/setup" />">SMS 자동발송 설정</a></li>
								<li><a href="<c:url value="/admin/email/setup" />">이메일 자동발송 설정</a></li>
								<li><a href="<c:url value="/admin/sms/histories" />">SMS 발송내역</a></li>
								<li><a href="<c:url value="/admin/email/histories" />">이메일 발송내역</a></li>
							</ul></li>
						<li><a href="" class="tab">로그 검색</a>
							<ul class="depth2">
								<li><a href="<c:url value="/admin/logs" />">회원 로그 검색</a></li>
							</ul></li>
					</ul>
				</div>
				<!-- //gnb -->
			</div>
			<!-- //aside_c -->
			<c:set var="title2" value="" />
			<div class="step_sub">
				<div class="inner">
					<ul>
						<li class="home">HOME</li>
						<c:if test="${!empty param.nav1}">
							<li><c:out value="${param.nav1}" /></li>
							<c:set var="title2" value="${param.nav1}" />
						</c:if>
						<c:if test="${!empty param.nav2}">
							<li><c:out value="${param.nav2}" /></li>
							<c:set var="title2" value="${param.nav2}" />
						</c:if>
						<c:if test="${!empty param.nav3}">
							<li><c:out value="${param.nav3}" /></li>
							<c:set var="title2" value="${param.nav3}" />
						</c:if>
					</ul>
				</div>
			</div>
			<!-- //step_sub -->
			<c:if test="${empty param.title}">
				<h2 class="tit_c">${title2}</h2>
			</c:if>
			<c:if test="${!empty param.title}">
				<c:if test="${param.title ne 'NONE'}">
					<h2 class="tit_c">${param.title}</h2>
				</c:if>
			</c:if>