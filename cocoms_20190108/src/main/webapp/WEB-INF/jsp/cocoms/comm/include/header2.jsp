<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">
<link rel="shortcut icon" href="<c:url value='/img/favicon.ico' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/CrossCert/CC_WSTD_home/unisignweb/rsrc/css/certcommon.css' />">
</head>
<body>
<div class="wrap">

	<div class="info_top">
		<div class="inner">
			<ul>
				<c:if test="${empty sessionScope.SESS_USER.loginId}">
					<li><a href="<c:url value='/login' />">로그인</a></li>
					<li><a href="<c:url value='/admin/login'/>">관리자</a>
					<li><a href="<c:url value='/user/new' />">회원가입</a></li>
				</c:if>
				<c:if test="${!empty sessionScope.SESS_USER.loginId}">
					<li><a href="<c:url value='/logout' />">로그아웃</a></li>
					<li><a href="<c:url value='/mypage' />">마이페이지</a></li>
				</c:if>
				<li><a href="<c:url value='/sitemap' />">사이트맵</a></li>
			</ul> 
		</div>
	</div>
	<!-- //info_top -->
	<div class="header">
		<div class="inner">
			<h1 class="logo"><a href="<c:url value='/main' />"><img src="<c:url value='/img/logo.png' />" alt="저작권위탁관리업시스템"/></a></h1>
			<div class="gnb">
				<ul>   
					<li class="menu01">
						<a href="<c:url value='/appl/sub/new' />" class="menu">저작권대리중개업</a>
						<ul class="depth2">
							<li><a href="<c:url value='/appl/sub/new' />">신청하기</a></li>
							<li><a href="<c:url value='/appl/sub/edit' />">변경 신청하기</a></li>
						</ul>
					</li>
					<li class="menu02">
						<a href="<c:url value='/appl/trust/new' />" class="menu">저작권신탁관리업</a>
						<ul class="depth2">
							<li><a href="<c:url value='/appl/trust/new' />">신청하기</a></li>
							<li><a href="<c:url value='/appl/trust/edit' />">변경 신청하기</a></li>
						</ul>
					</li>
					<li class="menu01">
						<a href="<c:url value='/report/sub' />" class="menu">실적보고</a>
						<ul class="depth2">
							<li><a href="<c:url value='/report/sub' />">대리중개업체</a></li>
							<li><a href="<c:url value='/report/trust' />">신탁단체</a></li>
							<li><a href="<c:url value='/report/excel' />">저작물보고</a></li>
						</ul>
					</li>
					<li class="menu02">
						<c:if test="${param.type eq 'education'}">
							<a href="<c:url value='/online' />" class="menu active">교육정보</a>
						</c:if>
						<c:if test="${param.type ne 'education'}">
							<a href="<c:url value='/online' />" class="menu">교육정보</a>
						</c:if>
						<ul class="depth2">
							<li><a href="<c:url value='/online' />">온라인</a></li>
							<li><a href="<c:url value='/offline' />">오프라인</a></li>
						</ul>
					</li>
					<li class="menu03">
						<c:if test="${param.type eq 'information'}">
							<a href="<c:url value='/row' />" class="menu active">정보센터</a>
						</c:if>
						<c:if test="${param.type ne 'information'}">
							<a href="<c:url value='/row' />" class="menu">정보센터</a>
						</c:if>
						<ul class="depth2">
							<li class="mm"><a href="<c:url value='/row' />">법·제도</a>
								<ul class="depth3">
									<li><a href="<c:url value='/row' />">관련법 제도</a></li>
								</ul>
							</li>
							<li><a href="<c:url value='/trend' />">최신동향</a></li>
							<li class="mm"><a href="<c:url value='/relation' />">관리 프로그램</a>
								<ul class="depth3">
									<li><a href="<c:url value='/relation' />">관리 프로그램 배포</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="menu04">
						<c:if test="${param.type eq 'notice'}">
							<a href="<c:url value='/notice' />" class="menu active">알림마당</a>
						</c:if>
						<c:if test="${param.type ne 'notice'}">
							<a href="<c:url value='/notice' />" class="menu">알림마당</a>
						</c:if>
						<ul class="depth2">
							<li><a href="<c:url value='/notice' />">공지사항</a></li>
							<li class="mm"><a href="<c:url value='/perm/intro1' />">이용안내</a>
								<ul class="depth3">
									<li><a href="<c:url value='/perm/intro1' />">공인인증서</a></li>
									<li><a href="<c:url value='/perm/intro2' />">관련저작권법</a></li>
									<li><a href="<c:url value='/faq' />">자주묻는질문(FAQ)</a></li>
									<%-- 
									<li><a href="<c:url value='/inquiry/new' />">1:1문의</a></li>
									--%>
									<li><a href="<c:url value='/perm/downloads' />">문서양식 자료실</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="menu05">
						<a href="<c:url value="/notice/status/companies" />">업체현황</a>
					</li>
					
				</ul>
			</div>
			<!-- //gnb -->
		</div>
		<!-- //inner -->
	</div>
	<!-- //header -->
	<!-- TODO -->
	
	<c:choose>
		<c:when test="${param.type eq 'sub'}">
			<div class="tit_sub_c blue">
				저작권대리중개업
				<p>신고 및 변경신청 등을 편리하게 처리하실 수 있습니다.</p>
			</div>
			<!-- //tit_sub_c -->		
		</c:when>
		<c:when test="${param.type eq 'trust'}">
			<div class="tit_sub_c green">
				저작권신탁관리업
				<p>허가신고 및 변경신청 등을 편리하게 처리하실 수 있습니다.</p>
			</div>
			<!-- //tit_sub_c -->		
		</c:when>
		<c:when test="${param.type eq 'report'}">
			<div class="tit_sub_c yellow">
				실적보고
				<p>년도별 업무보고 내용들을 상세하게 보실 수 있습니다.</p>
			</div>
			<!-- //tit_sub_c -->		
		</c:when>
		<c:when test="${param.type eq 'excel'}">
			<div class="tit_sub_c pink">
				위탁저작물 보고
				<p>월별 위탁물저작물 보고를 편리하게 처리하실 수 있습니다.</p>
			</div>
			<!-- //tit_sub_c -->		
		</c:when>
		<c:when test="${param.type eq 'education'}">
			<div class="tit_sub_c green">
				교육정보
				<p>준비중....</p>
			</div>
			<!-- //tit_sub_c -->
		</c:when>
		<c:when test="${param.type eq 'information'}">
			<div class="tit_sub_c yellow">
				정보센터
				<p>준비중....</p>
			</div>
			<!-- //tit_sub_c -->
		</c:when>	
		<c:when test="${param.type eq 'notice'}">
			<div class="tit_sub_c pink">
				알림마당
				<p>이용안내 및 사이트의 새로운 소식을 전해 드립니다.</p>
			</div>
			<!-- //tit_sub_c -->
		</c:when>		
		<c:when test="${param.type eq 'mypage'}">
			<div class="tit_sub_c my">
				마이페이지
				<p>내 정보를 최신 정보로 관리해 주세요.</p>
			</div>
			<!-- //tit_sub_c -->
		</c:when>		
		<c:when test="${param.type eq 'sitemap'}">
			<div class="tit_sub_c sitemap">
				사이트맵
				<p>저작권위탁관리업시스템의 정보를 한 눈에 검색하실 수 있습니다.</p>
			</div>
			<!-- //tit_sub_c -->
		</c:when>		
		<c:when test="${param.type eq 'status'}">
			<div class="tit_sub_c sub05">
				업체현황
				<p>저작권위탁관리업 시스템에 등록된 업체 현황을 보실 수 있습니다.</p>
			</div>
			<!-- //tit_sub_c -->		
		</c:when>
	</c:choose>
	
	<c:choose>
		<c:when test="${param.type eq 'main'}">
		</c:when>
		<c:when test="${param.type eq 'sub' or param.type eq 'trust' or param.type eq 'report' or param.type eq 'notice' or param.type eq 'mypage' or param.type eq 'status'}">
			<div class="step_sub">
				<div class="inner">
					<ul>
						<li class="home">홈</li>
						<c:if test="${!empty param.nav1}">
							<li><c:out value="${param.nav1}" /></li>
						</c:if>
						<c:if test="${!empty param.nav2}">
							<li><c:out value="${param.nav2}" /></li>
						</c:if>
						<c:if test="${!empty param.nav3}">
							<li><c:out value="${param.nav3}" /></li>
						</c:if>
					</ul>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="step_sub line">
				<div class="inner">
					<ul>
						<li class="home">홈</li>
						<c:choose>
							<c:when test="${param.type eq 'user'}">
								<li>회원가입</li>
							</c:when>
							<c:when test="${param.type eq 'login'}">
								<li>로그인</li>
								<c:if test="${!empty param.nav2}">
									<li><c:out value="${param.nav2}" /></li>
								</c:if>
							</c:when>
							<c:otherwise>
								<li><c:out value="${param.nav1}" /></li>
								<li><c:out value="${param.nav2}" /></li>
							</c:otherwise>
						</c:choose>
					</ul>
				</div>
			</div>	
		</c:otherwise>
	</c:choose>
	<!-- //step_sub -->
	