<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

		<div class="search_top">
			<div class="table_area list_top">
			<form id="searchForm" method="GET" >
			<input type="hidden" name="pageIndex" value="${page}" />
			
				<table>
					<caption></caption>
					<colgroup>
							<col style="width:125px" />
							<col style="width:395px" />
							<%-- <col style="width:125px" />
							<col style="width:395px" /> --%>
					</colgroup>
					<tbody>
						
						<tr>
							<th>검색기간</th>
							<td>
						
								<select name="fromYear" class="event-date input_style1 w172">
									<c:forEach var="item" items="${yearList}" varStatus="status">
										<c:set var="selected" value="" />
										<c:if test="${item eq fromYear}">
											<c:set var="selected" value="selected" />
										</c:if>
										<option value="${item}" ${selected}><c:out value="${item}" /></option>
									</c:forEach>
								</select>
								~
								<select name="toYear" class="event-date input_style1 w172">
									<c:forEach var="item" items="${yearList}" varStatus="status">
										<c:set var="selected" value="" />
										<c:if test="${item eq toYear}">
											<c:set var="selected" value="selected" />
										</c:if>
										<option value="${item}" ${selected}><c:out value="${item}" /></option>
									</c:forEach>
								</select>
							
							</td>
						</tr>		
						<tr>
							<th>구분</th>
							<td>
									<select class="select_style w121" name="gubun">
											<option selected="selected"  value="" >-선택해주세요-</option>
											<option value="10" ${gubun eq '10' ?'selected' :''}>저작물종류</option>
											<option value="20" ${gubun eq '20' ?'selected' :''}>저작물명</option>
											<option value="30" ${gubun eq '30' ?'selected' :''}>저작물부제</option>
											<option value="40" ${gubun eq '40' ?'selected' :''}>창작년도</option>
											<option value="50" ${gubun eq '50' ?'selected' :''}>작성기관명</option>
										</select>
									
									
							<input type="text" name="TITLE"  class="input_style1" style="width: 236px;" value="${title}">
						<!-- <a href="" id="search" class="btn_style2 btn_search" style="height: 50px;line-height: 50px;width:125px; float:right;">검색</a> -->								
							</td>
							<!--  <td><input type="text" name="TITLE" class="input_style1" value="${list.title}"></td>-->
							</tr>
										
					</tbody>
				</table>
					<a href="" id="search" class="btn_style2 btn_search" style="height: 84px;line-height: 84px;">검색</a>
			</form>
</div></div>