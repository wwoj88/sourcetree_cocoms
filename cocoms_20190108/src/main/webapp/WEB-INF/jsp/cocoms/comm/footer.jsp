<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

	<div class="menu_tail">
		<div class="inner">
			<ul>
				<li><a href="<c:url value='/provision' />">이용약관</a></li>
				<li><a href="<c:url value='/privacy' />">개인정보처리방침</a></li>
				<li><a href="<c:url value='/email' />">이메일주소 무단수집거부</a></li>
			</ul>
		</div>
	</div>
	<div class="footer">
		<div class="inner">
			<h1 class="logo"><a href=""><img src="<c:url value='/img/logo_tail.png' />" alt="문화체육관광부" /></a></h1>
			<div class="address">
				<p>30119 세종특별자치시 갈매로 388, 15동(어진동) 문화체육관광부 저작권산업과</p>
				<p>TEL : 044-203-2486 FAX : 044-203-3466, 시스템 오류신고 : 02-3465-7660 <a href="mailto:munmi7@korea.kr">munmi7@korea.kr</a></p>
			</div>
			<p class="copy">Copyright Ⓒ  Ministry of Culture, Sports and Tourism</p>
		</div>
	</div>
	<!-- //footer -->
	<!--  044-203-2486  02-3465-7660 -->
</div>
<!-- //wrap -->
<script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js?autoload=false"></script>
<script  src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="<c:url value='/js/jquery.bxslider.js' />"></script>
<script src="<c:url value='/js/jquery.form.js' />"></script>
<script src="<c:url value='/js/script.js' />"></script>
<script src="<c:url value="/js/jquery-ui.js" />"></script>


<c:if test="${param.cert eq true}">
	<script src="<c:url value='/CrossCert/CC_WSTD_home/unisignweb/js/unisignwebclient.js?v=2.5.5.0' />"></script>
	<script src="<c:url value='/CrossCert/ccc_wstd/UniSignWeb_Multi_Init_Nim.js' />"></script>
</c:if>




<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<c:if test="${!empty param.comm_js}">
	<script src="<c:url value='/js/comm/${param.comm_js}.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</c:if>
<c:if test="${!empty param.js}">
	<script src="<c:url value='/js/usr/${param.js}.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</c:if>
<!-- <script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js?autoload=false"></script> -->
<script>
$(function () {

	/* datepicker */
	$('.event-date').datepicker({
		dateFormat:'yy-mm-dd',
		numberOfMonths:1,
		showOn: 'button',
		buttonImage: '<c:url value="/img/date-icon.png" />' 
	});  
	
	$('.event-date2').datepicker({
		dateFormat:'yy-mm-dd',
		numberOfMonths:1,
		showOn: 'button',
		buttonImage: '<c:url value="/img/date-icon.png" />' 
	});  
	
	$(".ui-datepicker-trigger").attr('style', 'margin-left:5px; vertical-align:middle; cursor:pointer; border:0');
	
	/* popup */
    $('[name="popup_close"],[name="popup_cancel"]').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();

    	$this.closest('.pop_common').hide();
    });
	
    $('[name="popup_close2"]').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();

    	$this.closest('.pop_common2').hide();
    });
	
});


</script>


</body>
</html>
