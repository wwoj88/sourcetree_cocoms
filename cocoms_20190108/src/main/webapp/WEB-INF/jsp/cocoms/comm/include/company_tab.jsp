<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" 	   uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

				<div class="tab_type">
					<ul>
						<li class="active"><a href="" id="tab1">회원정보</a></li>
						<li><a href="" id="tab2">대표자정보</a></li>
					</ul>
				</div>
				<div id="area1" class="table_area">
					<table>
						<caption> 업체현황</caption>
						<tbody>
							<tr>
								<th>
									 회원구분
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${data.coGubunCd eq '1'}"><c:set var="coGubunCd1Checked" value="checked" /></c:if>
									<c:if test="${data.coGubunCd eq '2'}"><c:set var="coGubunCd2Checked" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd1" name="coGubunCd" value="1" ${coGubunCd1Checked} disabled />
										<label for="coGubunCd1">개인사업자</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd2" name="coGubunCd" value="2" ${coGubunCd2Checked} disabled />
										<label for="coGubunCd2">법인사업자</label> 
									</div>
								</td>
							</tr>
							<tr>
								<th>
									 사업자 등록번호
								</th>
							</tr>
							<tr>
								<td>
								<c:if test="${!empty data.coNo1}">
									<input type="text" class="input_style1 num1 disabled" value="${data.coNo1}" title="사업자번호 첫번째자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num1 disabled" value="${data.coNo2}" title="사업자번호 두째자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num2 disabled" value="${data.coNo3}" title="사업자번호 세째자리" disabled />
								</c:if>
							<%-- <c:set var="str1" value="${chgHistory.postCoNo}" />
						   <c:set var="subStr1" value="${fn:substring(str1, 0, 3) }" />
  							  <c:set var="subStr2" value="${fn:substring(str1, 3, 5) }" />
  							  <c:set var="subStr3" value="${fn:substring(str1, 5, 1) }" />
   							 <c:out value="${subStr1 }" />
            				<c:out value="${subStr2 }" />
            				<c:out value="${subStr3 }" />
  --%>   
							<c:if test="${empty data.coNo1}">
									<input type="text" class="input_style1 num1 disabled" value="${fn:substring(chgHistory.postCoNo,0,3)}" title="사업자번호 첫번째자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num1 disabled" value="${fn:substring(chgHistory.postCoNo,3,5)}" title="사업자번호 두째자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num2 disabled" value="${fn:substring(chgHistory.postCoNo,5 ,fn:length(chgHistory.postCoNo))}" title="사업자번호 세째자리" disabled />
								</c:if> 
								</td>
							</tr>
							<tr>
								<th>
									법인 등록번호
								</th>
							</tr>
							<tr>
								<td>
								
								<c:if test="${!empty data.bubNoStr }">  
									<%-- <input type="text" class="input_style1 num3 disabled"  value="${data.bubNoStr}" title="법인 등록번호" disabled /> --%>
									<input type="text" class="input_style1 num2 disabled" value="${fn:substring(data.bubNoStr,0,6)}" disabled />
									<span>-</span>
									<input type="text" class="input_style1 num2 disabled" value="${fn:substring(data.bubNoStr,7,15)}" disabled />
								</c:if>
							
								<c:if test="${empty data.bubNoStr }">
								
									<c:if test="${!empty chgHistory.postBubNo}">
										<input type="text" class="input_style1 num3 disabled"  value="${chgHistory.postBubNoStr}" title="법인 등록번호" disabled />
									</c:if>
								
									<c:if test="${empty chgHistory.postBubNo}">
										<input type="text" class="input_style1 num3 disabled"  value="${chgHistory.postCoNoStr}" title="법인 등록번호" disabled />
									</c:if>
									
								</c:if>
											
								<%--  <c:choose>
								  	<c:when test ="${data.statCd eq '2' or data.statCd eq '4'}" > 
								  		<input type="text" class="input_style1 num3 disabled"  value="${data.bubNoStr}" title="법인 등록번호" disabled />
								  	</c:when>
								   	<c:otherwise>
								 		<input type="text" class="input_style1 num3 disabled"  value="${chgHistory.postBubNoStr}" title="법인 등록번호" disabled />
								 	</c:otherwise>
								 </c:choose>  --%>
								</td>
							</tr>
							<tr>
								<th>
									주소
								</th>
							</tr>
							<tr>
								<td>
								<c:if test="${!empty data.trustZipcode}">
									<input type="text" class="input_style1 address1" value="${data.trustZipcode}" placeholder="" title="주소입력칸 1" disabled />
									<input type="text" class="input_style1 margin" value="${data.trustSido} ${data.trustGugun} ${data.trustDong} ${data.trustBunji}" placeholder="" title="주소입력칸 2" disabled />
									<input type="text" class="input_style1" value="${data.trustDetailAddr}" placeholder="" title="주소입력칸 3" disabled />
								</c:if>
								<c:if test="${empty data.trustZipcode}">
								<input type="text" class="input_style1 address1" value="${chgHistory.postTrustZipcode}" placeholder="" title="주소입력칸 1" disabled />
								<input type="text" class="input_style1 margin" value="${chgHistory.postTrustSido} ${chgHistory.postTrustGugun} ${chgHistory.postTrustDong} ${chgHistory.postTrustBunji}" placeholder="" title="주소입력칸 2" disabled />
								<input type="text" class="input_style1" value="${chgHistory.postTrustDetailAddr}" placeholder="" title="주소입력칸 3" disabled />
							
								</c:if>
								</td>
							</tr>
							<tr>
								<th>
									전화번호 
								</th>
							</tr>
							<tr>
								<td>
								<c:if test="${!empty data.trustTel1}">
									<input type="text" class="input_style1 num1 disabled" value="${data.trustTel1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustTel2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustTel3}" placeholder="" title="전화번호 뒷자리" disabled />
								</c:if>
								<c:if test="${empty data.trustTel1}">
								<%-- 	<input type="text" class="input_style1 num1 disabled" value="${data.trustTel1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustTel2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustTel2}" placeholder="" title="전화번호 뒷자리" disabled /> --%>
									<input type="text" class="input_style1 margin" value="${chgHistory.postTrustTel}" placeholder="" title="전화번호 앞자리" disabled />
									<%-- <td><c:out value="${chgHistory.postTrustTel}" /></td> --%>
								</c:if>								
								</td>
							</tr>
							<tr>
								<th>
									팩스번호 
								</th>
							</tr>
							<tr>
								<td>
								<c:if test="${!empty data.turstFax1}">
									<input type="text" class="input_style1 num1 disabled" value="${data.trustFax1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustFax2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.trustFax2}" placeholder="" title="전화번호 뒷자리" disabled />
								</c:if>
								<c:if test="${empty data.turstFax1}">
									<input type="text" class="input_style1 margin" value="${chgHistory.postTrustFax}" placeholder="" title="전화번호 앞자리" disabled />
								</c:if>
								</td>
							</tr>
							<tr>
								<th>
									홈페이지 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 disabled" value="${data.trustUrl}" placeholder="" title="홈페이지 주소입력칸" disabled />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="area2" class="table_area" style="display: none;">
					<table>
						<caption>대표자정보</caption>
						<tbody>
							<tr>
								<th>
									  대표자명
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${!empty data.ceoName }">
									<input type="text" class="input_style1 disabled" value="${data.ceoName}" placeholder="" title="대표자명" disabled />
									</c:if>
									<c:if test="${empty data.ceoName }">
									<input type="text" class="input_style1 disabled" value="${chgHistory.postCeoName}" placeholder="" title="대표자명" disabled />
									</c:if>
								</td>
							</tr>
							<tr>
								<th>
									주민등록번호
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${!empty data.ceoRegNo1 }">
									<input type="text" class="input_style1 resident disabled" 	value="${data.ceoRegNo1}" placeholder="" title="주민등록번호 앞자리" disabled /><span>-</span><input type="text" class="input_style1 resident disabled"  value="${data.ceoRegNo2}" placeholder="" title="주민등록번호 뒷자리" disabled />
									</c:if>
									<c:if test="${empty data.ceoRegNo1 }">
									
									<input type="text" class="input_style1 resident disabled" 	value="${ceoRegNo1}" placeholder="" title="주민등록번호 앞자리" disabled /><span>-</span><input type="text" class="input_style1 resident disabled"  value="${ceoRegNo2}" placeholder="" title="주민등록번호 뒷자리" disabled />
									</c:if>
									
								</td>
							</tr>
							<tr>
								<th>
									주소
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${!empty data.ceoZipcode }">
									<input type="text" class="input_style1 address1 disabled" value="${data.ceoZipcode}" placeholder="" title="주소입력칸 1" disabled />
									<input type="text" class="input_style1 margin disabled" value="${data.ceoSido} ${data.ceoGugun} ${data.ceoDong} ${data.ceoBunji} " placeholder="" title="주소입력칸 2" disabled />
									<input type="text" class="input_style1 disabled" value="${data.ceoDetailAddr} " placeholder="" title="주소입력칸 3" disabled />
								
									</c:if>
									<c:if test="${empty data.ceoZipcode}">
									
									<input type="text" class="input_style1 address1 disabled" value="<c:out value="${chgHistory.postCeoZipcode}" /> " placeholder="" title="주소입력칸 1" disabled />
									
									<input type="text" class="input_style1 margin disabled" value="<c:out value="${chgHistory.postCeoSido} ${chgHistory.postCeoGugun} ${chgHistory.postCeoDong} ${chgHistory.postCeoBunji}" /> "placeholder="" title="주소입력칸 2" disabled />
									
									<input type="text" class="input_style1 disabled" value="<c:out value="${chgHistory.postCeoDetailAddr}" /> "placeholder="" title="주소입력칸 3" disabled />

									</c:if>
									
								</td>
							</tr>
							<tr>
								<th>
									휴대전화번호 
								</th>
							</tr>
							<tr>
								<td>
								<c:if test="${!empty data.ceoMobile1 }">
									<input type="text" class="input_style1 num1 disabled" value="${data.ceoMobile1}" placeholder="" title="전화번호 앞자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.ceoMobile2}" placeholder="" title="전화번호 중간자리" disabled />
									<span>-</span>
									<input type="text" class="input_style1 phone disabled" value="${data.ceoMobile3}" placeholder="" title="전화번호 뒷자리" disabled />
								</c:if>
								<c:if test="${empty data.ceoMobile1 }">
								<input type="text" class="input_style1 phone disabled" value="${chgHistory.postCeoMobile}" placeholder="" title="전화번호 뒷자리" disabled />
								</c:if>
								
								</td>
							</tr>
							<tr>
								<th>
									이메일
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" class="input_style1 resident disabled" 
											value="${data.ceoEmail1}" placeholder="" title="이메일 주소 앞자리" disabled /><span>@</span><input type="text" class="input_style1 resident disabled" 
											value="${data.ceoEmail2}"placeholder="" title="이메일 주소 뒷자리" disabled />
								</td>
							</tr>
							<tr>
								<th>
									 SMS수신동의 
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${data.smsAgree eq 'Y'}"><c:set var="smsAgreeYChecked" value="checked" /></c:if>
									<c:if test="${data.smsAgree ne 'Y'}"><c:set var="smsAgreeNChecked" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" ${smsAgreeYChecked} disabled />
										<label for="">동의</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" ${smsAgreeNChecked} disabled />
										<label for="">동의안함</label> 
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
