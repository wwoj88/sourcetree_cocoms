<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

	<div id="popupSendSms" class="pop_common" style="display: none;">
		<div class="inner">
			<h2 class="tit">SMS발송</h2>
			<a href="" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont">
				<form id="formSendSms">
				<input type="hidden" name="memberSeqNo" />
				<input type="hidden" name="receiver" />
				<input type="hidden" name="sendType" value="1" /><!-- 1: 즉시 -->
				
				<table class="last">
					<caption>SMS발송</caption>
					<colgroup>
						<col style="width:95px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>수신 전화번호</th>
							<td><input type="text" id="tel" class="input_style1" title="수신 전화번호" readonly /></td>
						</tr>
						<!-- 
						<tr>
							<th>발송 전화번호</th>
							<td><input type="text" class="input_style1" title="발송 전화번호" readonly /></td>
						</tr>
						 -->
						<tr>
							<th>메시지<p id="content_size" class="txt_byte">0/90byte</p></th>
							<td><textarea name="content" class="input_style1 height"></textarea></td>
						</tr>
					</tbody>
				</table>
				<div class="btn_area">
					<a href="" name="popup_submit" class="btn_style2">보내기</a>
					<a href="" name="popup_cancel" class="btn_style1">취소</a>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- //pop_common -->

	<div id="popupSendEmail" class="pop_common" style="display: none;">
		<div class="inner">
			<h2 class="tit">이메일발송</h2>
			<a href="" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont">
				<form id="formSendEmail">
				<input type="hidden" name="memberSeqNo" />
				<input type="hidden" name="receiver" />
				<input type="hidden" name="sendType" value="1" /><!-- 1: 즉시 -->
				
				<table class="last">
					<caption>이메일발송</caption>
					<colgroup>
						<col style="width:95px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>수신 이메일</th>
							<td><input type="text" id="email" class="input_style1" title="수신 이메일" readonly /></td>
						</tr>
						<!-- 
						<tr>
							<th>발송 이메일</th>
							<td><input type="text" class="input_style1" title="발송 이메일" readonly /></td>
						</tr>
						 -->
						<tr>
							<th>제목</th>
							<td><input type="text" name="subject" class="input_style1" title="제목" /></td>
						</tr>
						<tr>
							<th>내용</th>
							<td>
								<textarea name="content" class="input_style1 height"></textarea>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="btn_area">
					<a href="" name="popup_submit" class="btn_style2">보내기</a>
					<a href="" name="popup_cancel" class="btn_style1">취소</a>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- //pop_common -->
