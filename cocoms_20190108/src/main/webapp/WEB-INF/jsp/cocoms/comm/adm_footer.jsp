<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

	</div>
	<!-- //container -->
	<div class="footer">
		Copyright Ⓒ  Ministry of Culture, Sports and Tourism
	</div>
	<!-- //footer -->
</div>
<!-- //wrap -->

<script  src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="/cocoms/js/comm/jquery.form.min.js"></script>
<script src="<c:url value="/js/jquery-1.10.2.min.js" />"></script>
<script src="<c:url value="/js/jquery.bxslider.js" />"></script>
<script src="<c:url value="/js/adm_script.js" />"></script>
<script src="<c:url value="/js/jquery-ui.js" />"></script>

<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';
</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<c:if test="${!empty param.comm_js}">
	<script src="<c:url value='/js/comm/${param.comm_js}.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</c:if>
<c:if test="${!empty param.js}">
	<script src="<c:url value='/js/adm/${param.js}.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</c:if>

<script>
$(function () {

	/* menu */
//	var uri = $(location).attr('pathname');
	var uri = $(location).attr('href');
	//console.log(uri.indexOf("/")+1);
	//console.log(uri.lastIndexOf("/"));
	//console.log(uri.substring(uri.indexOf("/")+1,uri.lastIndexOf("/")));
//	var $target = $('#menu').find('a[href="'+uri+'"]');
//	$target.addClass('active');
//	$target.closest('ul').show();
//	$target.closest('ul').siblings('a').addClass('active');

	$('#menu').find('a').each(function (key, val) {
		var $val = $(val);
		var url = $val.attr('href');
 	 	
		//console.log($val);
		//console.log(uri);
		if(uri.match('/admin/excel') ){
			var $target = $('#menu').find('a[href="/cocoms/admin/excel/statics"]');
			$target.addClass('active');
			$target.closest('ul').show();
			$target.closest('ul').siblings('a').addClass('active');
		}
		
		if (url != '' && url.indexOf(CTX_ROOT) > -1
				&& uri.indexOf(url) > -1) {
			
			$val.addClass('active');
			$val.closest('ul').show();
			$val.closest('ul').siblings('a').addClass('active');
			
		}
	});
	
	/* datepicker */
 	$('.event-date').datepicker({
		dateFormat:'yy-mm-dd',
		maxDate: new Date,
		numberOfMonths:1,
		showOn: 'button',
		buttonImage: '<c:url value="/img/date-icon.png" />' 
	});  
	 
	$('.event-date2').datepicker({
		dateFormat:'yy-mm-dd',
		numberOfMonths:1,
		showOn: 'button',
		buttonImage: '<c:url value="/img/date-icon.png" />' 
	});  
	
	$(".ui-datepicker-trigger").attr('style', 'margin-left:5px; vertical-align:middle; cursor:pointer; border:0');
	
	/* popup */
    $('[name="popup_close"],[name="popup_cancel"]').click(function (event) {
    	var $this = $(this);
    	
		event.preventDefault();

    	$this.closest('.pop_common').hide();
    });
	
    $(function () {
        
        $('#enrollment').click(function (event) {
           var $this = $(this);
           
          event.preventDefault();

           var memberSeqNo = $('#memberSeqNo').val();
          
          $('#popupEnrollment').show();
       //   $('#popupEnrollment').find('[id="tel"]').val(tel);
          $('#popupEnrollment').find('[name="memberSeqNo"]').val(memberSeqNo);
       //   $('#popupEnrollment').find('[name="receiver"]').val(tel);
        });
                 
        
    
    });
});
</script>

<c:if test="${!empty param.includes}">
	<c:if test="${fn:contains(param.includes, 'company_tab')}">
		<script>
		$(function () {
			
			$('#tab1,#tab2').click(function (event) {
				var $this = $(this);
				var id = $this.attr('id');
				
				event.preventDefault();
		
				if (id == 'tab1') {
					$('#area1').show();
					$('#area2').hide();
				}
				else {
					$('#area1').hide();
					$('#area2').show();
				}
				
				$this.closest('li').addClass('active');
				$this.closest('li').siblings().removeClass('active');
			});
		});
		</script>
	</c:if>

	<c:if test="${fn:contains(param.includes, 'stats_search')}">
		<script>
		$(function () {
			
			$('#searchForm [name="conditionCd"]').change(function (event) {
				var $this = $(this);
				var $form = $this.closest('form');

				// init
				$form.find('[name="fromYear"] option:last').prop('selected', true);
				$form.find('[name="toYear"] option:last').prop('selected', true);

				// init
				$form.find('[name="type"]').prop('checked', false);
				$form.find('[name="type"]:first').prop('checked', true);

				$form.submit();
			});
		});
		</script>
	</c:if>
	
	<c:if test="${fn:contains(param.includes, 'sms_popup')}">
		<script>
		$(function () {
			
		    $('#sendSms').click(function (event) {
		    	var $this = $(this);
		    	
				event.preventDefault();

				var $checked = $('#data :checked');

				if ($checked.length == 0) {
					alert('처리할 데이터를 선택해 주세요');
					return;
				}
				if ($checked.length > 1) {
					alert('처리할 데이터를 한개만 선택해 주세요');
					return;
				}

				var agree = $checked.data('agree');
				var tel = $checked.data('tel');
				if (agree != 'Y' || !tel) {
					alert('발송불가합니다.');
					return;
				}
				
				$('#popupSendSms').show();
				$('#popupSendSms').find('[id="tel"]').val(tel);
				$('#popupSendSms').find('[name="memberSeqNo"]').val($checked.data('memberSeqNo'));
				$('#popupSendSms').find('[name="receiver"]').val(tel);
		    });
		    
		    $('#sendEmail').click(function (event) {
		    	var $this = $(this);
		    	
				event.preventDefault();
				
				var $checked = $('#data :checked');

				if ($checked.length == 0) {
					alert('처리할 데이터를 선택해 주세요');
					return;
				}
				if ($checked.length > 1) {
					alert('처리할 데이터를 한개만 선택해 주세요');
					return;
				}

				var agree = $checked.data('agree');
				var email = $checked.data('email');
				if (agree != 'Y' || !email) {
					alert('발송불가합니다.');
					return;
				}

				$('#popupSendEmail').show();
				$('#popupSendEmail').find('[id="email"]').val(email);
				$('#popupSendEmail').find('[name="memberSeqNo"]').val($checked.data('memberSeqNo'));
				$('#popupSendEmail').find('[name="receiver"]').val(email);
		    });
		    
		    $('#sendSms2').click(function (event) {

		    	var memberSeqNo = $('#memberSeqNo').val();
		    	var agree = $('#agree').val();
		    	var tel = $('#tel').val();
		    	
				event.preventDefault();

				if (agree != 'Y' || !tel) {
					alert('발송불가합니다.');
					return;
				}
				
				$('#popupSendSms').show();
				$('#popupSendSms').find('[id="tel"]').val(tel);
				$('#popupSendSms').find('[name="memberSeqNo"]').val(memberSeqNo);
				$('#popupSendSms').find('[name="receiver"]').val(tel);
		    });		    
		    
		    $('#sendEmail2').click(function (event) {

		    	var memberSeqNo = $('#memberSeqNo').val();
		    	var agree = $('#agree').val();
		    	var email = $('#email').val();

				event.preventDefault();

				if (agree != 'Y' || !email) {
					alert('발송불가합니다.');
					return;
				}
				
				$('#popupSendEmail').show();
				$('#popupSendEmail').find('[id="email"]').val(email);
				$('#popupSendEmail').find('[name="memberSeqNo"]').val(memberSeqNo);
				$('#popupSendEmail').find('[name="receiver"]').val(tel);
		    });
		    
			$('#formSendSms').on('submit', function (event) {
				var $form = $(this);
				var params = formToJson($form);

				event.preventDefault();
				
				// validation form
				var validForm = function (params) {
					
//					if (!params['loginId']) {
//						alert('아이디를 입력해 주세요.');
//						return false;
//					}

					return true;
				}
				
				// validation form
				if (!validForm(params)) {
					return false;
				}
		    	
				if (confirm('발송하시겠습니까?')) {
//					$form.unbind().submit();
					
					executeTransaction('POST', '/admin/sms.json', params, function (data) {
						alert('처리되었습니다.');
						location.reload();
					});
				}
			});
			
			$('#popupSendSms [name="content"]').keyup(function (event) {
				var $this = $(this);
				
				var len = $this.val().replace(/[\0-\x7f]|([0-\u07ff]|(.))/g,"$&$1$2").length;
				
				$('#content_size').text(len + '/90byte');
				
				if (len > 90) {
					$('#content_size').css({'color': 'red'});
				}
				else {
					$('#content_size').css({'color': ''});
				}
			});
			
			$('#formSendEmail').on('submit', function (event) {
				var $form = $(this);
				var params = formToJson($form);

				event.preventDefault();
				
				// validation form
				var validForm = function (params) {
					
//					if (!params['loginId']) {
//						alert('아이디를 입력해 주세요.');
//						return false;
//					}

					return true;
				}
				
				// validation form
				if (!validForm(params)) {
					return false;
				}
		    	
				if (confirm('발송하시겠습니까?')) {
//					$form.unbind().submit();
					
					executeTransaction('POST', '/admin/email.json', params, function (data) {
						alert('처리되었습니다.');
						location.reload();
					});
				}
			});
			
			$('[name="popup_submit"]').click(function (event) {
				var $this = $(this);
				var $form = $this.closest('form');

		    	event.preventDefault();
		    	
		    	$form.submit();
		    });
		});
		</script>
	</c:if>
</c:if>
	
</body>
</html>
