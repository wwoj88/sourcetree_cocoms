<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script  src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="/cocoms/js/comm/jquery.form.min.js"></script>
<script src="<c:url value="/js/jquery-1.10.2.min.js" />"></script>
<script src="<c:url value="/js/jquery.bxslider.js" />"></script>
<script src="<c:url value="/js/adm_script.js" />"></script>
<script src="<c:url value="/js/jquery-ui.js" />"></script>
<c:set var="checkCode" value="${result.checkCode }" />
<script src="<c:url value='/js/comm/utils.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<script>
var CTX_ROOT = '/cocoms';

</script>
<script src="<c:url value='/js/comm/common.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
<c:if test="${!empty param.comm_js}">
	<script src="<c:url value='/js/comm/${param.comm_js}.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</c:if>
<c:if test="${!empty param.js}">
	<script src="<c:url value='/js/adm/${param.js}.js' />?dummy=<%=System.currentTimeMillis()%>"></script>
</c:if>

<script>
	
window.onload = function() {
	var CTX_ROOT = '/cocoms';
	
	var params = {};
	var memberSeqNo = ${result.memberSeqNo };
	
	params.memberSeqNo = ${result.memberSeqNo };
	params.conditionCd = ${result.conditionCd };
	params.statCd = ${result.code};
	params.prestatCd =${result.prestatCd};
	if('<c:out value="${checkCode}" />'){
		params.checkCode='<c:out value="${checkCode}" />';
	}
	executeTransaction('POST', '/admin/sub/companies/'+memberSeqNo+'/status', params, function (data) {
	
		alert('처리되었습니다.');
		
		location.href = CTX_ROOT+'/admin/sub/statuses';
		
	}); 
    

};
</script>
