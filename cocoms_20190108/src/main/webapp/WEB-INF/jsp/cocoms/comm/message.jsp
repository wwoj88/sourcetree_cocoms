<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">
<link rel="shortcut icon" href="<c:url value='/img/favicon.ico' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/CrossCert/CC_WSTD_home/unisignweb/rsrc/css/certcommon.css' />">
</head>
<body>
<div class="wrap">
	<div class="info_top">
			<div class="inner">
				<ul>
					<c:if test="${empty sessionScope.SESS_USER.loginId}">
						<li><a href="<c:url value='/login' />">로그인</a></li>
						<li><a href="<c:url value='/admin/login'/>">관리자</a>
						<li><a href="<c:url value='/user/new' />">회원가입</a></li>
					</c:if>
					<c:if test="${!empty sessionScope.SESS_USER.loginId}">
						<li><a href="<c:url value='/logout' />">로그아웃</a></li>
						<li><a href="<c:url value='/mypage' />">마이페이지</a></li>
					</c:if>
					<li><a href="<c:url value='/sitemap' />">사이트맵</a></li>
				</ul>
			</div>
		</div>
		<!-- //info_top -->
		<div class="header">

			<div class="inner">
				<h1 class="logo">
					<a href="<c:url value='/main' />"><img src="<c:url value='/img/logo.png' />" alt="저작권위탁관리업시스템" /></a>
				</h1>
				<div class="gnb">
					<ul>
						<li class="menu01"><a href="<c:url value='/appl/sub/new' />" class="menu">저작권대리중개업</a>
							<ul class="depth2">
								<li><a href="<c:url value='/appl/sub/new' />">신청하기</a></li>
								<li><a href="<c:url value='/appl/sub/edit' />">변경 신청하기</a></li>
							</ul></li>
						<li class="menu02"><a href="<c:url value='/appl/trust/new' />" class="menu">저작권신탁관리업</a>
							<ul class="depth2">
								<li><a href="<c:url value='/appl/trust/new' />">신청하기</a></li>
								<li><a href="<c:url value='/appl/trust/edit' />">변경 신청하기</a></li>
							</ul></li>
						<li class="menu03"><a href="<c:url value='/report/sub' />" class="menu">실적보고</a>
							<ul class="depth2">
								<li><a href="<c:url value='/report/sub' />">대리중개업체</a></li>
								<li><a href="<c:url value='/report/trust' />">신탁단체</a></li>
							</ul></li>
						<li class="menu04"><a href="<c:url value='/report/excel' />" class="menu">위탁관리저작물 보고</a>
							<ul class="depth2">
								<li class="mm"><a href="<c:url value='/report/excel' />">보고/수정</a></li>

								<li class="mm"><a href="<c:url value='/report/excelstatus' />">보고현황</a></li>
								<li class="mm"><a href="<c:url value='/report/excellist' />">등록현황</a></li>
							</ul></li>
						<%-- <li class="menu05"><c:if test="${param.type eq 'education'}">
								<a href="<c:url value='/online' />" class="menu active">교육정보</a>
							</c:if> <c:if test="${param.type ne 'education'}">
								<a href="<c:url value='/online' />" class="menu">교육정보</a>
							</c:if>
							<ul class="depth2">
								<li><a href="<c:url value='/online' />">온라인</a></li>
								<li><a href="<c:url value='/offline' />">오프라인</a></li>
							</ul></li>
						<li class="menu06"><c:if test="${param.type eq 'information'}">
								<a href="<c:url value='/law' />" class="menu active">정보센터</a>
							</c:if> <c:if test="${param.type ne 'information'}">
								<a href="<c:url value='/law' />" class="menu">정보센터</a>
							</c:if>
							<ul class="depth2">
								<li class="mm"><a href="<c:url value='/law' />">법·제도</a>
									<ul class="depth3">
										<li><a href="<c:url value='/law' />">관련법 제도</a></li>
									</ul>
									</li>
								<li><a href="<c:url value='/trend' />">최신동향</a></li>
								<li class="mm"><a href="<c:url value='/relation' />">관리 프로그램</a>
									<ul class="depth3">
										<li><a href="<c:url value='/relation' />">관리 프로그램 배포</a></li>
									</ul></li>
							</ul></li> --%>
						<li class="menu07"><c:if test="${param.type eq 'notice'}">
								<a href="<c:url value='/notice' />" class="menu active">알림마당</a>
							</c:if> <c:if test="${param.type ne 'notice'}">
								<a href="<c:url value='/notice' />" class="menu">알림마당</a>
							</c:if>
							<ul class="depth2">
								<li><a href="<c:url value='/notice' />">공지사항</a></li>
								<li class="mm"><a href="<c:url value='/perm/intro1' />">이용안내</a>
									<ul class="depth3">
										<li><a href="<c:url value='/perm/intro1' />">공인인증서</a></li>
										<li><a href="<c:url value='/perm/intro2' />">관련저작권법</a></li>
										<li><a href="<c:url value='/faq' />">자주묻는질문(FAQ)</a></li>
										<li><a href="<c:url value='/perm/downloads' />">문서양식 자료실</a></li>
									</ul></li>
							</ul></li>
						<li class="menu08"><a href="<c:url value="/notice/status/companies" />">업체현황</a></li>

					</ul>
				</div>
				<!-- //gnb -->
			</div>
			<!-- //inner -->
		</div>
		<!-- //inner -->
	</div>
	<!-- //header -->
	<div class="container">
		<div class="error_content">
			<div class="icon"></div>
			<c:if test="${!empty message}">
				<p class="txt1"><c:out value="${message}" /></p>
			</c:if>
			<c:if test="${!empty subMessage}">
				<p class="txt2"><c:out value="${subMessage}" /></p>
			</c:if>
			<c:if test="${!empty url}">
				<a href="<c:url value="/${url}" />" class="btn_style2">확인</a>
			</c:if>
		</div>
		<!-- //error_content -->	
	</div>
	<!-- //container -->

	<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
		<jsp:param name="title" value=""/>
		<jsp:param name="js" value="sub" />
	</jsp:include>

</div>
<!-- //wrap -->

</body>
</html>
