<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

			<form id="searchForm" method="GET" >
				<table>
					<caption> 통계현황</caption>
					<colgroup>
						<col style="width:125px" />
						<col style="width:auto" />
						<col style="width:192px" />
					</colgroup>
					<tbody>
						<tr>
							<th>구분</th>
							<td>
								<c:if test="${conditionCd eq '2'}">
									<c:set var="conditionCd2" value="selected" />
								</c:if>
								<c:if test="${conditionCd eq '1'}">
									<c:set var="conditionCd1" value="selected" />
								</c:if>
								<select name="conditionCd" class="select_style w172 ml20">
									<option value="2" ${conditionCd2}>신탁단체</option>
									<option value="1" ${conditionCd1}>대리중개</option>
								</select>
							</td>
							<th rowspan="3">
								<a href="" id="search" class="btn_style2 btn_search" style="height: 50px;line-height: 50px;width:125px;">검색</a>
							</th>
						</tr>
						<tr>
							<th>검색기간</th>
							<td>
								<select name="fromYear" class="select_style w172 ml20">
									<c:forEach var="item" items="${yearList}" varStatus="status">
										<c:set var="selected" value="" />
										<c:if test="${item eq fromYear}">
											<c:set var="selected" value="selected" />
										</c:if>
										<option value="${item}" ${selected}><c:out value="${item}" /></option>
									</c:forEach>
								</select>
								~
								<select name="toYear" class="select_style w172 ml20">
									<c:forEach var="item" items="${yearList}" varStatus="status">
										<c:set var="selected" value="" />
										<c:if test="${item eq toYear}">
											<c:set var="selected" value="selected" />
										</c:if>
										<option value="${item}" ${selected}><c:out value="${item}" /></option>
									</c:forEach>
								</select>
							</td>
						</tr>		
						<c:if test="${conditionCd eq '2'}">
							<tr>
								<th>
									검색항목
								</th>
								<td class="list_search_check">
									<c:choose>
										<c:when test="${type eq '1'}"><c:set var="checked1" value="checked" /></c:when>
										<c:when test="${type eq '2'}"><c:set var="checked2" value="checked" /></c:when>
										<c:when test="${type eq '3'}"><c:set var="checked3" value="checked" /></c:when>
										<c:when test="${type eq '4'}"><c:set var="checked4" value="checked" /></c:when>
										<c:when test="${type eq '5'}"><c:set var="checked5" value="checked" /></c:when>
										<c:when test="${type eq '6'}"><c:set var="checked6" value="checked" /></c:when>
										<c:when test="${type eq '7'}"><c:set var="checked7" value="checked" /></c:when>
										<c:when test="${type eq '8'}"><c:set var="checked8" value="checked" /></c:when>
										<c:when test="${type eq '9'}"><c:set var="checked9" value="checked" /></c:when>
										<c:when test="${type eq '10'}"><c:set var="checked10" value="checked" /></c:when>
										<c:when test="${type eq '11'}"><c:set var="checked11" value="checked" /></c:when>
										<c:when test="${type eq '12'}"><c:set var="checked12" value="checked" /></c:when>
									</c:choose>
									<div class="btm_form_box block">
										<input type="radio" id="check1" name="type" value="1" ${checked1} />
										<label for="check1">저작권 종류별 신탁사용료</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check2" name="type" value="2" ${checked2} />
										<label for="check2">신탁단체별 신탁사용료</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check3" name="type" value="3" ${checked3} />
										<label for="check3">연도별 신탁사용료</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check4" name="type" value="4" ${checked4} />
										<label for="check4">저작권 종류별 보상금 - 저작권자</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check5" name="type" value="5" ${checked5} />
										<label for="check5">신탁단체별 보상금 - 저작권자</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check6" name="type" value="6" ${checked6} />
										<label for="check6">연도별 보상금 - 저작권자</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check7" name="type" value="7" ${checked7} />
										<label for="check7">저작권 종류별 보상금 - 실연자</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check8" name="type" value="8" ${checked8} />
										<label for="check8">신탁단체별 보상금 - 실연자</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check9" name="type" value="9" ${checked9} />
										<label for="check9">연도별 보상금 - 실연자</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check10" name="type" value="10" ${checked10} />
										<label for="check10">판매용 음반 보상금 - 음반제작자</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check11" name="type" value="11" ${checked11} />
										<label for="check11">신탁단체별 보상금 - 음반제작자</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check12" name="type" value="12" ${checked12} />
										<label for="check12">연도별 보상금 - 음반제작자</label> 
									</div>
								</td>
							</tr>
						</c:if>				
						<c:if test="${conditionCd eq '1'}">
							<tr>
								<th>
									검색항목
								</th>
								<td class="list_search_check">
									<c:choose>
										<c:when test="${type eq '1'}"><c:set var="checked1" value="checked" /></c:when>
										<c:when test="${type eq '2'}"><c:set var="checked2" value="checked" /></c:when>
										<c:when test="${type eq '3'}"><c:set var="checked3" value="checked" /></c:when>
										<c:when test="${type eq '4'}"><c:set var="checked4" value="checked" /></c:when>
										<c:when test="${type eq '5'}"><c:set var="checked5" value="checked" /></c:when>
										<c:when test="${type eq '6'}"><c:set var="checked6" value="checked" /></c:when>
										<c:when test="${type eq '7'}"><c:set var="checked7" value="checked" /></c:when>
									</c:choose>
									<div class="btm_form_box block">
										<input type="radio" id="check1" name="type" value="1" ${checked1} />
										<label for="check1">직원수별 대리중개업체수</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check2" name="type" value="2" ${checked2} />
										<label for="check2">매출액별 대리중개업체수</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check3" name="type" value="3" ${checked3} />
										<label for="check3">저작물 종류별 저작물수</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check4" name="type" value="4" ${checked4} />
										<label for="check4">저작권 유형별 저작물수</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check5" name="type" value="5" ${checked5} />
										<label for="check5">저작물 종류별 사용료</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check6" name="type" value="6" ${checked6} />
										<label for="check6">저작물 종류별 수수료</label> 
									</div>
									<div class="btm_form_box block">
										<input type="radio" id="check7" name="type" value="7" ${checked7} />
										<label for="check7">해당년도 사업실적</label> 
									</div>
								</td>
							</tr>
						</c:if>				
					</tbody>
				</table>
				
			</form>
