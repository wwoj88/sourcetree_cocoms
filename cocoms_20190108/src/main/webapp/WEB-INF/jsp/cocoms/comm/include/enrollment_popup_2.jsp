<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

	<div id="popupEnrollment" class="pop_common" style="display: none;">
		<div class="inner">
			<h2 class="tit">업체 이력 등록</h2>
			<a href="" name="popup_close" class="btn_close"><img src="<c:url value="/img/close_pop.png" />" alt="팝업닫기" /></a>
			<div class="cont">
				<!-- <form id="formEnrollment"> -->
				<form id="form" method="POST" action="<c:url value="/admin/sub/insert_enrollment2" />">
				<input type="hidden" name="memberSeqNo" value="${data.memberSeqNo}"/>
				<input type="hidden" id="code"  name="code" value=""/>
				<input type="hidden" id="conditionCd"   name="conditionCd" value=""/>
				<input type="hidden" id="prestatCd"  name="prestatCd" value=""/>
				<input type="hidden" id="checkCode"  name="checkCode" value=""/>
				
				<c:if test="${title1 == '저작권중개업'}">
					<input type="hidden" name="gubun" value="1" /><!-- 1:저작권중개업 -->
				</c:if>
				<c:if test="${title1 == '저작권신탁관리업'}">
					<input type="hidden" name="gubun" value="2" /><!-- 2:저작권신탁관리업 -->
				</c:if>
				
				<table class="last">
					<caption>업체 이력 등록</caption>
					<colgroup>
						<col style="width:140px" />
						<col style="width:auto" />
					</colgroup>
					<tbody>
						<tr>
							<th>연락일자(등록일자)</th>
							<td>
								<!-- <input type="text" id="tel" class="input_style1" title="연락 일자(등록일자)"/> -->
								<input type="text" name="regdate" class="event-date input_style1" value="" style="width:90%"/>
							</td>
						</tr>
						<tr>
							<th>업체 담당자</th>
							<td><input tyupe="text" id="" name="accountManager"class="input_style1" title="업체 담당자" /></td>
						</tr>
						<tr>
							<th>이력 유형</th>
							<td>
								<select name="type" class="select_style" style="width:100%">
									<option value="A" selected="selected">A</option>
									<option value="B">B</option>
									<option value="C">C</option>
									<option value="D">D</option>
									<option value="E">E</option>
								</select>
							</td>
						</tr>
						<!-- 
						<tr>
							<th>발송 전화번호</th>
							<td><input type="text" class="input_style1" title="발송 전화번호" readonly /></td>
						</tr>
						 -->
						<tr>
							<th>이력 내용 상세<p id="content_size" class="txt_byte">0/90byte</p></th>
							<td><textarea name="contents" class="input_style1 height"></textarea></td>
						</tr>
					</tbody>
				</table>
				<div class="btn_area">
					<!-- <a href="" id="submit" class="btn_style2">등록</a> -->
					<a href=""><button class="btn_style2" id="sbpop">보완요청 / 반려</button></a>
					
					<a href="" name="popup_cancel" class="btn_style1">취소</a>
				</div>
				</form>
			</div>
		</div>
	</div>
	<!-- //pop_common -->