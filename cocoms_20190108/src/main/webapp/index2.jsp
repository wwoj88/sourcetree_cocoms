<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="Content-Script-Type" content="text/javascript">
<meta name="Content-Style-Type" content="text/css">
<meta name="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1400">
<title>저작권위탁관리업시스템</title>
<!-- 스타일 -->
<link rel="stylesheet" type="text/css" href="<c:url value='/css/html5_reset.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/layout.css' />">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/utill.css' />">

<link rel="stylesheet" type="text/css" href="<c:url value='/CrossCert/CC_WSTD_home/unisignweb/rsrc/css/certcommon.css' />">
</head>
<body>
<div class="wrap">
	<div class="info_top">
		<div class="inner">
		
		</div>
	</div>
	<!-- //info_top -->
	<div class="header">
		
		<!-- //inner -->
	</div>
	<!-- //header -->
	<div class="container">

		<div class="error_content" style="line-height:200px;">

			<!-- //gnb -->
		
			<div class="icon"><img src="<c:url value='/img/icon_error.png' />" /></div>
			<p class="txt3">죄송합니다.<br>현재 사이트 점검중입니다.</p>
			<p class="txt4"><br>저작권 위탁관리업 시스템점검으로 인하여 다음과 같이 중단됨을 알려드립니다.</p>
			<p class="txt4" style="color:red"><br>중단일자 : 2019. 11. 15(금) 19:00 ~ 11. 16.(일) 09:00</p>
			<p class="txt4"><br>사이트 이용에 불편을 드려 죄송합니다.</p>
			
		</div>
		<!-- //error_content -->	
	</div>
	<!-- //container -->


</div>
<!-- //wrap -->

</body>
</html>
