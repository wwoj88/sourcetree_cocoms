
<%-- <%@ page contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%> --%>
<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR" %>
<%-- <%@ page language="java" contentType="text/html; charset=EUC-kR" pageEncoding="EUC-kR" %> --%>
<%@ page import="cocoms.copyright.common.*" %>
<%@ page import="cocoms.copyright.web.controllers.CertController" %>
<%@ page import="java.nio.ByteBuffer" %>
<%@ page import="java.nio.CharBuffer" %>
<%@ page import="java.nio.charset.Charset" %>

<%--
	2009.12.16
	GPKI 암복호화모듈 설치확인 화면
--%>

<%



	out.println(DateUtils.getCurDateTime());
	
	out.println("===========================표준보안 API 설치 확인 ========================= <br><br>");

	String preOrgMsg = "SVR1371000009^ou=Group of Server,o=Government of Korea,c=KR^usercertificate;binary^";
	String orgMsg = "1311000^문화체육관광부 저작권위탁관리시스템 중개변경 테스트2^1234567890123^홍길동^0402^http://abcdefg/Axis/pgGpkiTest.jsp";
	//String preOrgMsg = "SVR1371000009^ou=Group of Server,o=Government of Korea,c=KR^usercertificate;binary^";
	//String orgMsg = "1311000^홍길동 A 공공기관_민원명^1234567890123^홍길동^0402^http://abcdefg/Axis/pgGpkiTest.jsp";
	//String orgMsg = "1311000^ABCD A ABCD^1234567890123^ABCD^0402^http://abcdefg/Axis/pgGpkiTest.jsp";
	
/* 	out.println("<1. 원문> <br>");
	out.println(preOrgMsg+orgMsg+"<br>");
	out.println("<br>"); */
	
	String str = "";
	str = preOrgMsg+orgMsg;
	out.println("<1-1 원문> <br>");
	out.println(str+"<br>");
	out.println("<br>");
	
	byte[] euckrStrBuffer = str.getBytes("KSC5601");
    str = new String(euckrStrBuffer,"ISO8859-1");
	out.println("<1-2 원문> <br>");
	out.println(str+"<br>");
	out.println("<br>");
	
/* 	
		
		str  =  new String ( str.getBytes("UTF-8"), "ISO8859-1" );
		
		out.println("<br>");
		out.println("MIDMID :"+str);
		out.println("<br>"); */
/* 
		str = new String(str.getBytes("KSC5601"), "ISO8859-1");
		byte[] byteBuffUTF8=str.getBytes(Charset.forName("UTF-8"));
		str=new String(byteBuffUTF8, "UTF-8");
		out.println("<br>");
		out.println("Mid: "+str);
		out.println("<br>"); */
		/* byte[] byteBuffEuc=str.getBytes("KSC5601");
		str = new String(byteBuffEuc, "ISO8859-1");
		out.println("<br>");
		out.println("MIDMID :"+str);
		out.println("<br>");
		str = new String(str.getBytes("UTF-8")); */
/* 		out.println("<br>");
		out.println("FIN :"+str);
		out.println("<br>");
		out.println("<br>");
		out.println("MIDMID :"+str);
		out.println("<br>");  */
		
 	
	
	 
	 String charSet[] = { "UTF-8", "euc-kr", "ksc5601", "iso-8859-1", "ascii", "x-windows-949" };

		for (int i = 0; i < charSet.length; i++) {

			for (int j = 0; j < charSet.length; j++) {

				if (i == j){

					continue;

				}

			
					out.println(charSet[i] + " : " + charSet[j] + " :" + new String(str.getBytes(charSet[i]), charSet[j])+"<br>");
			

			}

		}
	//str = new String(str.getBytes(),"UTF-8");
	out.println("<테스트> <br>");
	out.println(str+"<br>");
	out.println("<br>");

	//str  =  new String ( str.getBytes("UTF-8"), "ISO8859-1" );


	/* CertController cc = new CertController();
	str = cc.toISO8859_1(str); */

 	out.println("<1-2 바이트화> <br>");
	out.println(str+"<br>");
	out.println("<br>");
 
	GPKICms cms = new GPKICms();
	String encMsg = cms.cmsEnc(str);
	
 	out.println("<2. 암호화> <br>");
	out.println(encMsg+"<br>");
	out.println("<br>"); 
     
	//encMsg="MIIHtQIBADGB/jCB+wIBADBkMFAxCzAJBgNVBAYTAktSMRwwGgYDVQQKExNHb3Zlcm5tZW50IG9mIEtvcmVhMQ0wCwYDVQQLEwRHUEtJMRQwEgYDVQQDEwtDQTEzMTAwMDAwMQIQSwTeogK5LQP5ylIgQZqcgTANBgkqhkiG9w0BAQcwAASBgEQackq7uigKMQ93UQ5FPi1Sy2LBLIjgHJ+sIRtvzxJCypyxg6wF65fHdUJ1bzpbp97kaq0vpPPCusKTlTAw4tBrc8GXWxHWYcFm4BxO09KJ55Ax0hg+18bO0+kcCqXBQcySSDlxwW7rv3NwgTWdHwMVMPhjCbGUFfzF1hgP5f/TMIIGrQYJKoZIhvcNAQcBMBwGCCqDGoyaRAEEBBAqaIEhsxoOQJBVs2zAhWIPgIIGgD42TY7qiSEb1M0+AbEnU8QQ344j/ftqqIjotX48yppQQ1Qq9uM/0hjs73UIHB2l7NVO+deQaGI6NmzDisPiPLon3/NQAlx9KezJOFcMB3j+bC8vUa1jWqICojBStaB6HBw67493UaDli4CapH/JAVqQwUQ6dVycimQsXiLTfDfSgVFwAUbaO479W3byr/ApNgYbXAhdGVGnb7U1wqmUfPbnvFxt8Ne+BkUzfXLTE6SJRHaHUre363/4kPCOlgld52F2GZti+JNp1dDTw9jUXBMVKJulWYMWNyuZnsPXEwQqJH4r7ocABCT0dOj9j3fqR2UOlVm9PKG4xURyo85pziKeFySjix3oeqHgYHGfc1RVROYuLz+UAf8gZeWGvEtP/BZDFQgIt6jrzB2bfa0opyeimfitia1Exqi1r3PV3sDn3tg1cUWb/H68rabfVESeHhJ2Z7Ve9+mXUl+3brdXgpTze7fCtBQfIbkQ4gTDN7q283HDv5D1gT24c1LkcoxJlu/xMo1t5LmCfDYHYnVxOXNPIDGcJ28Jqp1cTZQasMCRHtATjIW+2FfVcMeLmePjZ5W8hPKzADXCC+IVNltW6l2MFuSwiLJlfizIs15A+k7aC5iS541fBFxO+dc0uERVD0xlq8vWaaEeUdYB/5io8y3NeWWXaC+DmnVwM6tJr9IovcNMjYslrekae2SLKQ0JOQIv+0QoInaVfXqYb4JdkCp1JzSD/9aMiwXpOY8AAdx9KwBr8ffs+oHdpni7IZXF8s4GHqfeG3Ax1BcpV9m0y/8UPVAH+vmQKazU+2ei5cw1xv4nSG6b3VfRc/Ti8U/Ki/og2Gep7mF3S6jzBE4nOW8iZFptz359WEO/ExTyIuxqAueovreNkbBlOp2Nr2/M7WIFJn6+BSSVMxeKFizn9A+X6W18PIE4YF3bYsXafTLBwdYTFH159MC4D5Hc5ll34vUnSEQL0oyg6rmPv8GYrHbUrHYV41A4Me9UuippNIFDlEfxE2qwtayUgUQJP/BLeB3umZDH2myeMb3X5uXocV0fSH4nJYif9L+MUjoyMocjFO2jGP46z4KTA3bot3oZLNak5tJ7lYLLdESQ3BPGpykDZygESGroQX8MDgdgNsIquOq8Q0S1mjzdDia91XmCM+MuRhH7VoloSJym6PhpymC2PADxsvp6yY0IlANeq9NqFPSnWw5P/93zjZbqoEbUCqc0vFgAR2nFBFphMyYzuokAphI1Uv6u9THKgm5ofGEfz4myhs3+6epOxwW5KAY1yLWl7DPZfIo3wdgNEVgl75I8OPCBdOUVS5w/hHcVIL5Mh3+3EIQ7IlpFSSsfYQ/zs+Kj5xp8659fChuB/w7HToDnoX0ntxOXRopgMPqItdZ0AYrzxIne6IKH2oSCZXX9SP8Rhhn/2G0DFXncFOjzd+imjuLLijLyROscKNa1/q5NDmE9fHnEydEom738o4PBVwIdwfeJ3nRuNAb15FKqAdhGFKPT455D6d/iWCeE2bBsKyBfexkxCeLvAAKX1UO4a6Z8mr6Y9UX6ug3E6kkqLjOZDxkdKaiMV8wTtWaNVDY2LSIxA7OvAx0OhsBgf2Lrxq1d0Z1jBn41ZPdvjZhzySgssCf258D6+N54pNvrpGNz8L6ZQ6aIMMg0MqmrTMzM0ElwnnmRghNc9NL6zHQta9V+VhgWU1Vjm01UOpGEyC3g+CjEuA3tGtBTJyBL9atuTuB6ZMd+dpKXDXow93LGMO3r8rqbfgI6UQV0mo9g4CX9WfOGA1k+zMeGqAgM44TqBiZohiIFyc8HDx/gQr+W4XPeQvpmAx8x+X8EEwUiBiBXJVCwQimGkBw+3QPYMRgrtEGbxuOPF8XiSU6yfJUdE9kQH1LusVUFjXpU7dwLRLmRc3x5pjWabSa/5cL0F9u38HmiztUrwZ8WZJCcJ17Su53zAog6kmrEztjeoKqyMibvF8YFCYGk+iGliA44Wt3wfjROd2zn8BS5tV/48OnFLKrzht4JuDwantPaCSDfuWY8aeto2COypHUg4eFGhxtj8KaqjVJ8SMdNn9hvVnCJvv0inAI9U0/+F/DSaImxPTxIYIEvXD2/UppAlTtMqLlz0FWa8qsMBmgAl76O8w+bJ802yFpUdK7pLfTANrLLPoAWfSkWnK+gkZ9cZ9qRhPIGFF/taUxw7c772Wtn8uUhITaZ112+Zv1P5t6uhZTEOJpG";
	//encMsg ="MIILjAIBADGCAYQwggGAAgEAMGgwUDELMAkGA1UEBhMCS1IxHDAaBgNVBAoME0dvdmVybm1lbnQgb2YgS29yZWExDTALBgNVBAsMBEdQS0kxFDASBgNVBAMMC0NBMTMxMTAwMDAxAhQ1nZvuHH6MbkZaJH1XVIWK2uv9SDANBgkqhkiG9w0BAQcwAASCAQAOKaw6pqpT2Ud0CJW/GJNCwrnLuN92UTPTqxD5k6Cj5mGLxB5J1cVZEDEPa+l05JXRFww9XIrbcRc5+dQ3GeRnOc99Bgx/hpRJVaIO/eG0O+uNuNU9qVZm0AmJuM+QJYIY8aQ+MlEwd4fJXLUK+tifN6b+2pONLTd2/7rkzenMbPdGvD1t5mQzpSJoTiJuubTkcHCSqBFJwcFSqTJ2KD23H8cFyKbMO8TjwYRixQ2dedhoSBJya+TkVw8Yp9FVOsBXsU2gPcPpgTx2qvTintIcBP/vjoLldzPT4uTuZFTtbnBLKvplQqLbhCNySD8uglckr+ziFIkogYC8jMLkPAiEMIIJ/QYJKoZIhvcNAQcBMBwGCCqDGoyaRAEEBBDg8G0IpXvlOnkNxfd9x1XugIIJ0IZYTLMr6eR8srN6Sa3aMM5bYcN7loWECPq1akbWoMwLuGkm2TEkV/B4MoAD22obDFIje4PZ+y+VDEtAIrriAl+eRN/XQv1Hxj7jDDq2mU9pdc3mKgeQEEmoX4/bN+hdpOafJtR6zGNlZX31eSkXoDPMw6A/T0OzO7lCz+0g6+qBgqCbDn8EZLX1BcIztH3XmBO/bZLNVfK+K8dr/QLjWwpQ5pXu7VuJT1tn2/jIxT5Ir+UpEHzrlqm+mfJ6HKiOuXwJHpq1uiy3ueJJx66ISuiA/qrOmlWsHfUGtxKyV06zvsh++HvYoSQBt9MEzzlZCFnCS7NsezhKvaW7GaUhFeHmnJFw2Y5hWGhlQtfIGFpLwqmfrg9fT/WSIp6iRvXG+ftHSYQKWMAbJ2hzqs72Pq4inrt+o6++sUO/90BPnGLwBZ9Uww4uvvp2MsgO+Edvf8I8oAa9jcGIitJ8M9kRzSFxghmvnMQvPU5NPrYpvvGVpXogRYCSkLlhppSQRJzjdT1Dui6nfui5oAJ+wP0hHMam5GcF3cvItsPpr3AoinEpJgiqkjLaEUoy5C96anCD6BnfX7Sqli2QaXaauQbFBdlNaJY9xRhgLdKpFxEOVg7n+OvDuwdX4MTce4gSi1/byfUQ08wzsrH2NYamVfRkpS0DfhC4aMTFzuiD72PeFOmzfp0AuTRlBczCtJRfi67XAEgN3ajLXqkhsX5pwA3wSEWhap90PlfQPl3ZJAD8XQtdKFafTypO5a6uqgyYaXwhyNPtchY4xKACHJ32eYj7HLxSQ7xmmN0m7GCElMSjOIO1uWokeKRsU6XXLgQDZDu11nFu6HbmrTMsB0awr56EibkcvSU6sNdfU6H3nLm0CJuN7zxaqsu1wdpM5b8IgEQ42vcMN7pmpGcEQFIaszqTt+2aTycsmHvDKF5U1gNpu3PC8z+wa65HBPvLy4dQMflna3QD33btZZS+pSLya80+eTX6LrDpN8yOFRFsDCFaJjsWM5YCYZjxubCS4N4dv6Zj6JLNuPVRoRSQgkBxwy867B2AFP2Qsyf1nqPq9fLumwevyK1RcZHHhEejUqhCvlFJrLyeOLHUImlxPZxmBp8oG5NmVJstx7U8MJgcP9ipjlN+8hL47SNEeaUPzmSTk0G4ZzFSo8NlU8EBVDDHWOoiJpREGtHt4R9mM+OeKch+m3T+XKylg/pdhXQVF+mrC7wVUSch89bH16Yqy38uXJdZjjq56cmWm0SOwyERo9AI9zHGsWx0pOKLrpvMg2b8kvUaTE0IsiQTFPx9qJb1qNP+IoFzen2d1OpYMF18QyXnRZx1WzjLGM/NIUWAWA3tO8fRZxSfMQQqP6c2OmCWn/4OtUTO5pczIjvRlcNWz++8vuJP6rkkzTopZegGQcStMLZ7Wi3tXBKTXCJYCRhdImoh5j3FkAQU+neH8qLkcj7fp3CoO99tKZdYzfs5/G909YQG+tZBcoCOiGr5mEF3MgpgBHFO2flrk89UZ49Jz/rXSKZQI7KkJzEj+Z5cVjxoYr6iwap6pZdWTV2kwwEHYby12s8D49WQetpPV0fmOFYdjFyQ8q4cxqt6sGQslLXIPrBop4ELCXYzFkJ9whttcpRCMIA46hyhxgIjzieCMwbkQZVj1jyXYPSj5p1ALTZnVpljAFFrOtUjGm0SxKrdlwvcRYGqn1LPMHZtm8tj6Wlnek+yyUxR/R8DskjTAn53HeE72G3dUcYMhleDnkPlymoFNr8VlSEnrnQA9/rrg197vascetjrr1oHzJCd97BPQs0Hhb62f2XcGUD3q4IFnNcG5WwyLayA8U8q7l5kv3eRH9J0njV3mCeq2AlITCDosR43c7z4MWSR5hXpmqju+Wkpljb5XH3x670afNMogJyypyo4Na1F2T/5VX1vHlA9/Ng7cIhbnxQUXDZrEH/YzHhMkP1Siv24trhnnWPO//VOq8OuOG6n+/GRTsygr2WvP7qSy83nbH5r3wW7G+PKrqyUGUkUGn2X/yJC34rY4dpyHEEPVgF9Gkj/VlGf9OeqwlfqE2ICX1igOw4fPQULbDN5hSynWO4BnyCwNTr4E6d4y92U6lMx7lkLozjspFqWSvteipqP5CtU0mEv32eGhP/lKycfZwa3LFMToaAPY2mJzNOeIsCNSbcN8EHZguikWCDT6Dm7116WcV4WNvKXuFM2Fo8JO3nJR7gXLbbFTn8isLGqZv3b+mktP3Vqigj72IhYyZBGfTN86n3rxb+bCcqbEOq2TxU3fPKkOFjOyI0P5QLSFJph/5jzUi0ozV6H3Qnc+WALP8/GcUdsgicPtELHP6aGzwkAu33WqsoSdROrFrhWukP0NFPCF6MthQNJ2MnDgk6/pY9aUxJv32kV6bHSTAsGoUhw1OgokGfAG6pOjdWQk+DBN2fHakqqMiG+pFsQnz9oLLDGwX14OHMbsB4tbWR/6RWFV8DEj0M1c1ZWGHFsEOFXimawmnvbXo70xiNdSAlA9xL7+aorCkxT1ocm4UJ3ev7bptThXlrtNX9LaARsR2wC/gEJwPvMBGml24uxbV7KJR3njOKD36eqWfTjvs4BbFxRWFE3BdZqCLRbc5ZBuewheuS/4x5LncxX4B//zdCkBHZMDC8QNHMwtT3z/LwgkR8orQLf4PfoLC44rGM2JUDtCcYoMWvpaj+0BKmbJNHeUlRmBZYBescVADm7VdlJH2eFS5VX9eLghEGan+j2zbwJ3Zq1Czsx6Z5RhJM8hC+71DK7Vo88Pm0B6kz+MPYmB+A8MKU1lI9cNtxpvcYBR8t5NFn1zt2K8ThqX26LMNJ1SZoaL/tEgs+hY+aLeoaB/JTM4YwEK8mXdwn3UvnLuhTQ7VPEcB1tGnONt0aJG6w0FvUYwd1C6TXgAzGq35IP2r62ep9y5io8a7nr55cFt38I5bXQ5ojMdvEQNj6pxfgjD5sb+/oORmLRztK99jb61xn995KIcoqEXw2dibqU8qrTDmoXy8hsp8v9Y/FqditCEnTQL83bYPjngS5YipJ/sH5OVbOAWJ9z4plWCRdFO5UWsFVbpjQ/LXkqkRgIuWpu84oXRvfKNIOhLRQ4QBVnW7J2juR8bERvnaRGtcsKNB53kX1udrqvah6qHkQR9KOOQr2Kswbn2XAMxcGnuiwSf9yS8vuuyNApEcbUNo3h+PweNH9wt/XvW63bVQsppkLdngfLI1eNcTmfafR16vsmRdXofPTnEHJ7gL2suH3aN+dmGMPM6Ado7TRYYUIhvuSe6zQrsyhrlwiCsJ3hQyoZEB2Q7TxbsiDTTvWJ7jJd/ns4ZmS7ET7W0jXfYm/PtQxHrYsrnNBO6BKXIApMOug=";

	

	
	String decMsg = cms.cmsDec(encMsg);
	String decMsg2 = cms.cmsDec(encMsg);
	String decMsg4 = cms.cmsDec(encMsg);
	out.println("<3. 복호화> <br>");
	out.println(decMsg+" <br>");
	out.println(" <br>");
	//decMsg = new String(decMsg.getBytes("UTF-8"),"UTF-8");

	//decMsg = new String(decMsg.getBytes("latin1"),"UTF-8");
	decMsg = new String(decMsg.getBytes("ISO8859-1"), "KSC5601");
   // orgMsg = new String(orgMsg.getBytes("KSC5601"), "ISO8859-1");
	out.println("<4. 바이트> <br>");
	//out.println(preOrgMsg+orgMsg+"<br>");
	out.println(decMsg+"<br>");
	out.println("<br>");
	out.println("<4-1. 바이트> <br>");
	//out.println(preOrgMsg+orgMsg+"<br>");
	out.println(decMsg2+"<br>");
	out.println("<br>");
	
	 
	//String kor_str = "MIILbAIBADGCAYQwggGAAgEAMGgwUDELMAkGA1UEBhMCS1IxHDAaBgNVBAoME0dvdmVybm1lbnQgb2YgS29yZWExDTALBgNVBAsMBEdQS0kxFDASBgNVBAMMC0NBMTMxMTAwMDAxAhQ1nZvuHH6MbkZaJH1XVIWK2uv9SDANBgkqhkiG9w0BAQcwAASCAQBQTaGQzYwIO7uLjBbMGWyToSM+tkbVWvU1Iu3vaoOzYYkem1rCjryzOgxHgQ6qWW6H3eX2GPMC+HcCYgdl1EFJ7NBS3+4cB0GxsNy9Y1CW98x5ZP+NyfLOfDQwZjb9OiBQgjjYqMtpbz/MXTgh6In5Fa+dRRYjxfeR5T3ZB+b5pGsybaN6XlkAebpDzvy6DSmVD5BNTX16RmWCgpFb4wGFTqQh/+qlo9YGOz5U2aygKNShQaiHWrPcU2s/bwEu7a1E77naBgra/X+wahPWGogm8+LoEb4zAGvuv5u4tlUx0mNGXhmR6Jns4ahLFVSBk5CgAdfXy085z715SlBln+eSMIIJ3QYJKoZIhvcNAQcBMBwGCCqDGoyaRAEEBBD+3txARqGk0Qk53XrZrIYWgIIJsCAwfR7YtCNetnv125NAagRl/A3me+qDJXuEepZMrNI7soO3zR1/0O6nski4GoCphAbIwWvd5+ojm8Nj5WwN3y+/JANaiPkvqtRH81aRV3IxWbsqi+76BzKt9sNp7NjsgSTh+QlR1I3XWkcULZgHBh3n6mecIK9OY1nerwlWX6pe8/5YzFy+N7mvuTuGBkQoI9o+rgWLizBDy4x4coItDiJojqvFzgoR7Jcg7vcPb26zr+sL+PS5XYYwdDNjoPHGBQ87MCUyY6ysaTw0nEPYGzkQ3eVeDAv16gVQqskjO/jTLgwhsoeBGi11ct6kbGSxiz2nM0OhsiU42uwPRJd3vbWrjogtk67V/AKQe796217bj7b7lRHugSA+eu1FncG03VGH1LD2DMN9ZQNU68dBCvcxALV62/5tUYsS2gPDK4MghYqFt4a6bSophMnWsT0qxPj2MBgZnsbxM+MrfHKSFuUwFV3nXHIFmh1FOXSByeCcg4oAhFnz9XQPHUXVZ0YdJgSA2cMxXq6bTnDauOKGDNM4mcDgwQsMgd11pKNMiJN4GlilP++gV/Jx5GHwhMEJ8z9TWeTsheE7p+4De4TljgDaAO+Hyx7GKIcDtFl09rP8sMiFFgpPob35N4kVQOhCaroaVrGpdTIeIdCDQ2VPfY7L2NRPkWEV4iFLICklLi04FRhS0bRcPQFyKz10Gq4Cgupk97kiEJvIzWhjwGycE8y3VWLqtseVVgFci4AS9h69+WqPjy89kblNuHOovCHn9sJSBDGq7EkCviOydmxJd12qrI9jLQgTNsxfUc+SwLnj6TkWtrXV2BLfUYKEXthc2pRq6QIaLbiESZ4CYur7R+mvMmHA0i3KZgoEYj8jFEjez0HMoYshrMFaUUIb86rIHzhyQyTEuSheYWCfstvOIOlSUfNtsOp4fG2OnbuxoeQ2xKrQ2xGh8Xs+r95oaiXoZrkXu98Hz8STZ3O47a/zJg62YNxcVxAr03SoJr6bWHZ9LtXQL2HX+OSkJDamje0zAeIyWpRILz/9xi+feixF9FBhwsZZbHfQ3RVjq5jxTnHW6vcNeGYx3STDwZFuZloSNSI/5D+REmduAI+4iIlXoRGlQEUVw45GHNh82IVZsWNr6CrNMc/ozbLpXbmK7CwjWhli4VV42cmMyyq+LRqQ3N2hRC8LcSJN+uR4Ccy/oxtODDIfmv4uG84EJ7H1oi5McSB6fW0Up4HelIykQDIbz9GAk9wQh43W2z28kJnLqRmIoFtOsdYQZhE9uM9/C+Bjo9xUVi49zzeUXxVHr9Amo9uaBU3LZJCpsJ5/V5fkQlXqV1kKcmck5Mds3rh5IonJuXa2Xmn8pDovjHPqBdSnU5/zbpaNPiNmLY2apBESmdbrW1f7Ab8dJQ3anF28ro/w+jKXSImBmi8nQ1byD90dfY31LphglxOf45OQZfZYEECncSjU2FhJFCxo/zRKCBRbHxtvDDazxLlVmpvsKU238wf1yCS2r4l1eJPHqjQX818PrjDC9o6cOw0sScPnESDVMQkVawvLV14ipU3TPllWXYx8OH7f41HGdKj0wLuTcjnSnPco+mxNmvUfL5uROMrJ6NyYSzmX1dZ1Co4d0l5XpZnfs5fHXEsJ6loWQkJozOd/DijxH7l8lylD31c6TwTV98WQZLlOt5dCdH8Tw+0/Fw1FR3NbD3ra2FA97ULEp08ab4uO2XfAF2VabR/Pr7IQPOI40pt66YDiPHoZd6F43PCsQ8RIeRbMXdVQ3wn8lH9p5RAMNClIFP1h3w4cixBq+YY0DqBSQVxDGlS0W2jwPzGBPiwbxPBgFucZhGqxxLPmFaxACZCYMWfbyiPBl3jYLIkBMslLqOlucaYE3fp74BGDdM7V2uuv/eavI4sitCqoH05tCimpDj5JVPgxyqkvUCLWBqz1NAqENlwI3uJ1l3nUgvAQ8EQvGytW2hBhjIiSUq+v4g+xzhQ7nRFlW2sK0hZHdWOsIYbPTNOG4D15BbCJmmTmSJMtl99UM5WsoWUSn8A4weuoFxuznWK0He+aHA7KpZZbepsGyG7qX5ZfDRAjpObTDB1xmF4Weouej22dit/y8JM6a38dXE85PU+rwbVVOV4cfkO1zQL4zBswLz14/FsyvH6mWaVw7UN5pdhiwVqW71RvVjV4sqZ4iTuOP5mR2cqfS4V5DxX9KWjovf3YNABBE3KkW5bb9p1DhgDH/2HSkIM0KaaFRN/+zYCvpggXTSzt1B7yZSgaiGxCqInA921XBxu7hgVbxuj0eHRX3p19WzG4CWAc6oD0mUNqRssSZXKB228K1PiLcb8gzJd49UpOEdQdnI3eIRUi6Ngx9Q3eToTT8jacz9IPCLogDMaQfFYJE8ZYSOBWFNtr3AuhrvXrDc7m/tSiNzoJBEP1clESCrjRA6kh/2TOItJgNTGO4YQ3YpW82p/gU2BK9C9LuzAMkMe9pTGKybiZQ/C56k5eqf3qO/uo/n8KxQ971fWvMtbKuPYbwa3NqE7HE5Mf98YsdakA1Irs/r3wRCQifNvU+eooFKvKb8siNEpBCv5Ogh685dwZPZfdzewrIoj6WEFxbAlhIlnOQWCWag/BzspaZtpWXmEMpda8y6SoMMgsk9jSPRuF1ecUMiJ09j3d9vCAEW4XK/SKE5LRwBnr7l8iIzyLumk/iF1R/ksItCRWIDkvFEq7wMH28pFXkBI4mF0SuAkfiBOOY/9C2vSHypW3PQ94CLgRwYV5oMqlQVhO5sahk5DLz+6zoPPkTuiZxOx3qk8RfJHn2M9IDrwxKzkNmdbpHQHlyrkGOjkOuOV3u3EXLmbfeHTNEZoaMEYQNSTv0BDtjoQJu40BE1rjdqwPvGBLM1B88FtKJTaHOL3YHNV0fqcq7Cg78OrSSfDbjqfoUxAJOq4Jbv7sNj4f1+cDI8lqnxpoj7Z6S5N2GoxgcbgF4dDQow3mhAI51xXOSGQX9wyjCkTvOJHI95KBza1PB7eDH7iBvI1KUsu+YR7xJwpnLCwt0X48EyjiP1Qh3J6km16sjj/MUD5bFmW/QL06P45RW3J3GilRgCyobOxZDEAutyUECxZeKbrzBAspUt5jEkf/YTybAKXovtpX7ewQcYqWvQzwXAxPtFgaaQg0a8HuHIObthMpxZvh0Fe/E3IiNkJXCQE3tg5zN3DtXShCXnzhE+pkoyO4QzX3SED/rUl07RO5rd3HnVF5/ner0A/N3iHTHS5EwYWyfuBPVccuPH/plmQqV+k/f+1BpWLhfT5Xl/ObjM882UZtXVtmwEBiob3eLhw5lhzp0INm";

	/* String[] ary = {"euc-kr","utf-8","iso-8859-1","ksc5601","x-windows-949"};

	for( int i =0 ; i < ary.length; i++){

	for(int j=0; j < ary.length ; j++){

	if( i != j){

	out.println( ary[i]+"=>"+ ary[j]+ " \r\n ==> " +new String(decMsg4.getBytes(ary[i]),ary[j])+"<br>");

	}

	}

	}
  */

	
//	if( decMsg.equals(preOrgMsg+orgMsg) ) {
		out.println("======================== 표준보안 API 설치 확인성공 ======================== <br>");
		out.println("================================ Good Job! ============================== <br>");
//	}




		out.println("utf-8 -> euc-kr        : <br>" + new String(decMsg2.getBytes("utf-8"), "euc-kr"));// 해당부분 DECODE
		out.println("<br>");
		out.println("utf-8 -> ksc5601       :  <br>" + new String(decMsg2.getBytes("utf-8"), "ksc5601"));
		out.println("<br>");
		out.println("utf-8 -> x-windows-949 :  <br>" + new String(decMsg2.getBytes("utf-8"), "x-windows-949"));
		out.println("<br>");
		out.println("utf-8 -> iso-8859-1    :  <br>" + new String(decMsg2.getBytes("utf-8"), "iso-8859-1"));
		out.println("<br>");
		out.println("iso-8859-1 -> euc-kr        :  <br>" + new String(decMsg2.getBytes("iso-8859-1"), "euc-kr"));
		out.println("<br>");
		out.println("iso-8859-1 -> ksc5601       :  <br>" + new String(decMsg2.getBytes("iso-8859-1"), "ksc5601"));
		out.println("<br>");
		out.println("iso-8859-1 -> x-windows-949 :  <br>" + new String(decMsg2.getBytes("iso-8859-1"), "x-windows-949"));
		out.println("<br>");
		out.println("iso-8859-1 -> utf-8         :  <br>" + new String(decMsg2.getBytes("iso-8859-1"), "utf-8"));
		out.println("<br>");
		out.println("iso-8859-1 -> utf-8         :  <br>" + new String(decMsg2.getBytes("iso-8859-1"), "latin1"));
		out.println("<br>");
		out.println("euc-kr -> utf-8         :  <br>" + new String(decMsg2.getBytes("euc-kr"), "utf-8"));
		out.println("<br>");
		out.println("euc-kr -> ksc5601       :  <br>" + new String(decMsg2.getBytes("euc-kr"), "ksc5601"));
		out.println("<br>");
		out.println("euc-kr -> x-windows-949 :  <br>" + new String(decMsg2.getBytes("euc-kr"), "x-windows-949"));
		out.println("<br>");
		out.println("euc-kr -> iso-8859-1    :  <br>" + new String(decMsg2.getBytes("euc-kr"), "iso-8859-1"));
		out.println("<br>");
		out.println("ksc5601 -> euc-kr        :  <br>" + new String(decMsg2.getBytes("ksc5601"), "euc-kr"));
		out.println("<br>");
		out.println("ksc5601 -> utf-8         :  <br>" + new String(decMsg2.getBytes("ksc5601"), "utf-8"));
		out.println("<br>");
		out.println("ksc5601 -> x-windows-949 :  <br>" + new String(decMsg2.getBytes("ksc5601"), "x-windows-949"));
		out.println("<br>");
		out.println("ksc5601 -> iso-8859-1    :  <br>" + new String(decMsg2.getBytes("ksc5601"), "iso-8859-1"));
		out.println("<br>");
		out.println("x-windows-949 -> euc-kr     :  <br>" + new String(decMsg2.getBytes("x-windows-949"), "euc-kr"));
		out.println("<br>");
		out.println("x-windows-949 -> utf-8      :  <br>" + new String(decMsg2.getBytes("x-windows-949"), "utf-8"));
		out.println("<br>");
		out.println("x-windows-949 -> ksc5601    :  <br>" + new String(decMsg2.getBytes("x-windows-949"), "ksc5601"));
		out.println("<br>");
		out.println("x-windows-949 -> iso-8859-1 :  <br>" + new String(decMsg2.getBytes("x-windows-949"), "iso-8859-1"));


%>