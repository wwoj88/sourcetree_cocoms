<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import = "java.util.*" %>
<%@ page import = "com.softforum.xdbe.xCrypto" %>

<html>
<head>
	<title>test xecuredb page</title>
</head>
<body>
<%
    	String sOutput		= null;
    	String sDecoded		= null;
    	String sOutput7		= null;
    	String sDecoded7		= null;
    	String sOutput_H	= null;
    	String sString		= "5610251011012";
    	String Err =  "";
	
	try
	{
		long nStartTime    = System.currentTimeMillis();
		xCrypto.RegisterEx("normal", 2, "/home/tmax/jeus6/lib/application/xdsp_pool.properties" , "pool1", "mcst_db", "mcst_owner", "mcst_table", "normal");
		xCrypto.RegisterEx("pattern7", 2,"/home/tmax/jeus6/lib/application/xdsp_pool.properties", "pool1", "mcst_db", "mcst_owner", "mcst_table", "pattern7");
	
        Err = new String(xCrypto.GetLastError());
		sOutput    = xCrypto.Encrypt("normal", sString);
		xCrypto.GetLastError();
		sDecoded   = xCrypto.Decrypt("normal", sOutput);    
		sOutput7    = xCrypto.Encrypt("pattern7", sString);
		sDecoded7   = xCrypto.Decrypt("pattern7", sOutput7);
		sOutput_H	= xCrypto.Hash(6, sString);
		
%>

		<br><B>
		[Normal policy]asdsa
		<br>
		<br>
		<%=sString%> -> <font color=red><%=sOutput%></font>
		<br>
		<%=sOutput%> -> <font color=blue><%=sDecoded%></font>
		<br><B>
		
		<br>
		<%=Err%>
		
		[Pattern7 policy]
		<br>
		<br>
		<%=sString%> -> <font color=red><%=sOutput7%></font>
		<br>
		<%=sOutput7%> -> <font color=blue><%=sDecoded7%></font>
		
		<br>
		<br>
		[Hash policy]
		<br>
		<br>
		<%=sString%> -> <font color=green><%=sOutput_H%></font>

<%
		long nEndTime      = System.currentTimeMillis();
		long nDuration     = nEndTime - nStartTime;
%>
		<br>
		<br>
		<br>
		<br>
		<font color=red>Elasped: <%=nDuration%> ms</font>
		</B>
<%
	}
    	catch (Exception e)
    	{
        		e.printStackTrace();
		out.println(e);
    	}

%>

</body>
</html>
