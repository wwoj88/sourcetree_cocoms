<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ page import="java.io.UnsupportedEncodingException;"%>
<%@ page import="java.util.Date;"%>
<%@ page import="java.util.Properties;"%>

<%@ page import="javax.mail.Message;"%>
<%@ page import="javax.mail.MessagingException;"%>
<%@ page import="javax.mail.Session;"%>
<%@ page import="javax.mail.Transport;"%>
<%@ page import="javax.mail.internet.InternetAddress;"%>
<%@ page import="javax.mail.internet.MimeMessage;"%>
<%@ page import="javax.mail.internet.MimeUtility;"%>

<%
	String subject = "test";
	String content = "test";
	String host = "222.231.43.31"; 	//smtp서버
	String to = "sh.lee@ilv.co.kr";  	//수신인 주소
	String toName = "Lee";  	//수신인 성명
	String from = "soyg9981@korea.kr";	//발신인 주소
	String fromName = "test";	//발신인 주소

    Properties props = new Properties();
    props.put("mail.smtp.host", host);
    Session session2 = Session.getInstance(props);
    try {
    	MimeMessage msg = new MimeMessage(session2); //메세지 내용 담당 클래스인 MimeMessage 객체 생성
    
    	if(fromName != null && !fromName.equals("")){
    		msg.setFrom(new InternetAddress(from, MimeUtility.encodeText(fromName,"EUC-KR","B")));    //발신자 의 IP
    	} else {
    		msg.setFrom(new InternetAddress(from));    //발신자 의 IP
    	}
    	
    	if(toName != null && !toName.equals("")){
    		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to, MimeUtility.encodeText(toName,"EUC-KR","B")));//수신자의 IP (수신자가 다수일 경우 배열로 선언)
    	} else {
    		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));//수신자의 IP (수신자가 다수일 경우 배열로 선언)
    	}
        msg.setSubject(MimeUtility.encodeText(subject,"EUC-KR","B")); 
        msg.setSentDate(new Date());        //날짜 구함
        msg.setContent(content,"text/html; charset=EUC-KR");
        Transport.send(msg);             //메일발송
    } catch (MessagingException mex) {
        mex.printStackTrace();
    } catch( UnsupportedEncodingException ux){
    	ux.printStackTrace();
    } catch( Exception e){
    	e.printStackTrace();
    }
%>
