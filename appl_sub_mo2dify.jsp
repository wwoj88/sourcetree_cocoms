<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<c:choose>
	<c:when test="${type eq 'sub_modify'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value=""/>
			<jsp:param name="type" value="sub" />
			<jsp:param name="nav1" value="저작권대리중개업 " />
			<jsp:param name="nav2" value="변경" />
		</jsp:include>	
	</c:when>
	<c:when test="${type eq 'trust_modify'}">
		<jsp:include page="/WEB-INF/jsp/cocoms/comm/header.jsp" flush="true">
			<jsp:param name="title" value=""/>
			<jsp:param name="type" value="trust" />
			<jsp:param name="nav1" value="저작권신탁관리업 " />
			<jsp:param name="nav2" value="변경" />
		</jsp:include>
	</c:when>
</c:choose>
	
	<div class="container" >
		<div class="box_top_common">
			<ul>
				<li>법정 민원을 신청할 때 전자우편 및 휴대폰 번호를 기재하시면 민원접수 및 처리결과를 전자우편 및 휴대폰으로 알려 드립니다.</li>
				<li>온라인 첨부가능 표시가 있는 구비서류는 컴퓨터 파일로 첨부가 가능한 서류이며, 스캔한 파일 온라인 첨부가능 표시가 있는 구비서류는 원본 서류를 스캔하여 컴퓨터 파일로 첨부가 가능한 서류입니다.</li>
				<li>온라인으로 첨부할 수 없는 서류는 방문 및 우편으로 제출할 수 있습니다. </li>
			</ul>
		</div>
		<!-- //box_top_common -->
		
		<c:choose>
			<c:when test="${type eq 'sub_modify'}">
				<h2 class="tit_bar blue">대리중개업신고 변경 신청하기</h2>
			    <c:set var="memtype" value="disabled" /> 
			</c:when>
			<c:when test="${type eq 'trust_modify'}">
				<h2 class="tit_bar green">신탁허가 변경 신청하기</h2>
				<c:set var="memtype" value="disabled" />
			</c:when>
			
		</c:choose>
		
		<div class="form_apply">
			<div class="aside">
				<div class="table_area">
			   
				<form id="form" method="post"  action="<c:url value="/appl/sub" />">
					<input type="hidden" name="conditionCd" value="1" />
		 			<input type="hidden" name="statCd" value="3" />
					<input type="hidden" name="procType" value="${type}" />
					<input type="hidden" id="alreadyConfirm" value="N" />
					<input type="hidden" id="conDetailCd" name="conDetailCd" value="${member.conDetailCd}" />
					<input type="hidden" id="trustSido" name="trustSido" value="${member.trustSido}" />
					<input type="hidden" id="trustGugun" name="trustGugun" value="${member.trustGugun}" />
					<input type="hidden" id="trustDong" name="trustDong" value="${member.trustDong}" />
					<input type="hidden" id="trustBunji" name="trustBunji" value="${member.trustBunji}" />
					<input type="hidden" id="ceoSido" name="ceoSido" value="${member.ceoSido}" />
					<input type="hidden" id="ceoGugun" name="ceoGugun" value="${member.ceoGugun}" />
					<input type="hidden" id="ceoDong" name="ceoDong" value="${member.ceoDong}" />
					<input type="hidden" id="ceoBunji" name="ceoBunji" value="${member.ceoBunji}" />
     
					<input type="hidden" id="preceoName" 		name="preceoName" 		value="${member.ceoName}"/>
					<input type="hidden" id="preceoRegNo1" 		name="preceoRegNo1" 	value="${member.ceoRegNo1}"/>
					<input type="hidden" id="preceoRegNo2" 		name="preceoRegNo2" 	value="${member.ceoRegNo2}"/>
					<input type="hidden" id="preCeoEmail1" 		name="preceoEmail1"  	value="${member.ceoEmail1}"/>
					<input type="hidden" id="preCeoEmail2" 		name="preceoEmail2"  	value="${member.ceoEmail2}"/>
					<input type="hidden" id="preCeoZipcode" 	name="preceoZipcode"    value="${member.ceoZipcode}"/>
					<input type="hidden" id="preceoSido" 		name="preceoSido" 		value="${member.ceoSido}"/> 
					<input type="hidden" id="preceoGugun" 		name="preceoGugun" 		value="${member.ceoGugun}"/> 
					<input type="hidden" id="preceoDong" 		name="preceoDong" 		value="${member.ceoDong}"/> 
					<input type="hidden" id="preceoBunji" 		name="preceoBunji" 		value="${member.ceoBunji}"/> 
					<input type="hidden" id="preceoDetailAddr" 	name="preceoDetailAddr" value="${member.ceoDetailAddr}"/> 
					<input type="hidden" id="preceoTel1" 		name="preceoTel1" 		value="${member.ceoTel1}"/> 
					<input type="hidden" id="preceoTel2" 		name="preceoTel2" 		value="${member.ceoTel2}"/> 
					<input type="hidden" id="preceoTel3" 		name="preceoTel3" 		value="${member.ceoTel3}"/> 
					<input type="hidden" id="preceoFax1" 		name="preceoFax1" 		value="${member.ceoFax1}"/> 
					<input type="hidden" id="preceoFax2" 		name="preceoFax2" 		value="${member.ceoFax2}"/> 
					<input type="hidden" id="preceoFax3" 		name="preceoFax3" 		value="${member.ceoFax3}"/> 
					<input type="hidden" id="preceoMobile1"		name="preceoMobile1" 	value="${member.ceoMobile1}"/> 
					<input type="hidden" id="preceoMobile2"		name="preceoMobile2" 	value="${member.ceoMobile2}"/> 
					<input type="hidden" id="preCeoMobile3"		name="preceoMobile3" 	value="${member.ceoMobile3}"/> 
					<input type="hidden" id="pretrustUrl" 		name="pretrustUrl" 		value="${member.trustUrl}"/> 
					<input type="hidden" id="prebubNo1" 		name="prebubNo1" 		value="${member.bubNo1}"/> 
					<input type="hidden" id="prebubNo2" 		name="prebubNo2" 		value="${member.bubNo2}"/> 
					<input type="hidden" id="precoNo1" 			name="precoNo1" 		value="${member.coNo1}"/> 
					<input type="hidden" id="precoNo2" 			name="precoNo2" 		value="${member.coNo2}"/> 
					<input type="hidden" id="precoNo3" 			name="precoNo3" 		value="${member.coNo3}"/> 
					<input type="hidden" id="presms" 			name="presms" 			value="${member.sms}"/> 
					<input type="hidden" id="coGubunCd"			name="coGubunCd"        value="${member.coGubunCd}"/>
					<input type="hidden" id="precoName" 		name="precoName"  		value="${member.coName}" />
					
					<input type="hidden" id="preceoMobile" name="preceoMobile"  value="${member.ceoMobile}" />
					<input type="hidden" id="preceoEmail" name="preceoEmail"  value="${member.ceoEmail}" />
					<input type="hidden" id="preceoRegNo" name="preceoRegNo"  value="${member.ceoRegNo}" />
					<input type="hidden" id="preceoTel" name="preceoTel"  value="${member.ceoTel}" />
					<input type="hidden" id="preceoFax" name="preceoFax"  value="${member.ceoFax}" />
     				<input type="hidden" id="pretrustZipcode" name="pretrustZipcode"  value="${member.trustZipcode}" />
					<input type="hidden" id="pretrustSido" name="pretrustSido"  value="${member.trustSido}" />
					<input type="hidden" id="pretrustGugun" name="pretrustGugun"  value="${member.trustGugun}" />
					<input type="hidden" id="pretrustDong" name="pretrustDong"  value="${member.trustDong}" />
					<input type="hidden" id="pretrustBunji" name="pretrustBunji"  value="${member.trustBunji}" />
					<input type="hidden" id="pretrustDetailAddr" name="pretrustDetailAddr"  value="${member.trustDetailAddr}" />
					<input type="hidden" id="pretrustTel" name="pretrustTel"  value="${member.trustTel}" />
					<input type="hidden" id="pretrustFax" name="pretrustFax"  value="${member.trustFax}" />
					
					
<%-- 					<input type="hidden" id="conDetailCd" name="conDetailCd" value="${member.conDetailCd}" />
					<input type="hidden" id="trustSido" name="trustSido" value="${member.trustSido}" />
					<input type="hidden" id="trustGugun" name="trustGugun" value="${member.trustGugun}" />
					<input type="hidden" id="trustDong" name="trustDong" value="${member.trustDong}" />
					<input type="hidden" id="trustBunji" name="trustBunji" value="${member.trustBunji}" />
					<input type="hidden" id="ceoSido" name="ceoSido" value="${member.ceoSido}" />
					<input type="hidden" id="ceoGugun" name="ceoGugun" value="${member.ceoGugun}" />
					<input type="hidden" id="ceoDong" name="ceoDong" value="${member.ceoDong}" />
					<input type="hidden" id="ceoBunji" name="ceoBunji" value="${member.ceoBunji}" />
					<input type="hidden" id="prebubNo" name="prebubNo" value="${member.bubNo}" />
					<input type="hidden" id="precoNo" name="precoNo" value="${member.coNo}" />
					<input type="hidden" id="preSms" name="preSms" value="${member.sms}" />
								
					<input type="hidden" id="preceoMobile" name="preceoMobile"  value="${member.ceoMobile}" />
					<input type="hidden" id="preceoEmail" name="preceoEmail"  value="${member.ceoEmail}" />
					<input type="hidden" id="preceoName" name="preceoName"  value="${member.ceoName}" />
					<input type="hidden" id="preceoRegNo" name="preceoRegNo"  value="${member.ceoRegNo}" />
					<input type="hidden" id="preceoZipcode" name="preceoZipcode"  value="${member.ceoZipcode}" />
					<input type="hidden" id="preceoSido" name="preceoSido"  value="${member.ceoSido}" />
					<input type="hidden" id="preceoGugun" name="preceoGugun"  value="${member.ceoGugun}" />
					<input type="hidden" id="preceoBunji" name="preceoBunji"  value="${member.ceoBunji}" />
					<input type="hidden" id="preceoDong" name="preceoDong"  value="${member.ceoDong}" />
					<input type="hidden" id="preceoDetailAddr" name="preceoDetailAddr"  value="${member.ceoDetailAddr}" />
					<input type="hidden" id="preceoTel" name="preceoTel"  value="${member.ceoTel}" />
					<input type="hidden" id="preceoFax" name="preceoFax"  value="${member.ceoFax}" />
					<input type="hidden" id="precoName" name="precoName"  value="${member.coName}" />
					<input type="hidden" id="pretrustUrl" name="pretrustUrl"  value="${member.trustUrl}" />
					<input type="hidden" id="pretrustZipcode" name="pretrustZipcode"  value="${member.trustZipcode}" />
					<input type="hidden" id="pretrustSido" name="pretrustSido"  value="${member.trustSido}" />
					<input type="hidden" id="pretrustGugun" name="pretrustGugun"  value="${member.trustGugun}" />
					<input type="hidden" id="pretrustDong" name="pretrustDong"  value="${member.trustDong}" />
					<input type="hidden" id="pretrustBunji" name="pretrustBunji"  value="${member.trustBunji}" />
					<input type="hidden" id="pretrustDetailAddr" name="pretrustDetailAddr"  value="${member.trustDetailAddr}" />
					<input type="hidden" id="pretrustTel" name="pretrustTel"  value="${member.trustTel}" />
					<input type="hidden" id="pretrustFax" name="pretrustFax"  value="${member.trustFax}" /> --%>
		
					
					
					<table class="membinfo">
						<caption> 회원정보</caption>
						<thead>
							<tr> 	 	 	
								<th> 회원정보</th>
							</tr>
						</thead>
						<tbody >
							<tr>
								<th>
									 회원구분 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${member.coGubunCd eq '1'}"><c:set var="coGubunCd1Checked" value="checked"  /></c:if>
									<c:if test="${member.coGubunCd eq '2'}"><c:set var="coGubunCd2Checked" value="checked" /></c:if>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd1" name="coGubunCd"  value="1" ${coGubunCd1Checked} disabled/>
										<label for="coGubunCd1">개인사업자</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="coGubunCd2" name="coGubunCd" value="2" ${coGubunCd2Checked} disabled/>
										<label for="coGubunCd2">법인사업자</label> 
									</div>
								</td>
							</tr>
							<tr>
								<th>
									 업체명 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="coName" class="input_style1" value="${member.coName}" maxlength="100" title="업체명" />
								</td>
							</tr>
							<tr>
								<th>
							    	 사업자 등록번호
							    	 <c:choose>
										<c:when test="${member.coGubunCd eq '2'}">
										  <span><strong class="txt_point" >*</strong></span>
									    </c:when>
									    <c:when test="${member.coGubunCd ne '2'}">
										  <span><strong id ="checkedstar" style='display:none' class="txt_point" >*</strong></span>
									    </c:when>
									 </c:choose>                     
                               </th>
							</tr>
							<tr>
								<td>
									<input type="text" name="coNo1" class="input_style1 num1" value="${member.coNo1}" maxlength="3" title="사업자번호 첫번째자리" />
									<span>-</span>
									<input type="text" name="coNo2" class="input_style1 num1" value="${member.coNo2}" maxlength="2" title="사업자번호 두째자리" />
									<span>-</span>
									<input type="text" name="coNo3" class="input_style1 num2" value="${member.coNo3}" maxlength="5" title="사업자번호 세째자리" />
								</td>
							</tr>
							<%-- <c:if test="${member.coGubunCd eq '2'}"> --%>
							<c:if test="${member.coGubunCd eq '2'}">
								<tr id="coGubunCd2_layer1">
							</c:if>
							<c:if test="${member.coGubunCd eq '1'}">
								<tr id="coGubunCd2_layer1" style='display:none'>							
							</c:if>
									<th>
										 법인 등록번호 <strong class="txt_point">*</strong>
									</th>
								</tr>
							<c:if test="${member.coGubunCd eq '2'}">	
								<tr id="coGubunCd2_layer2">
							</c:if>
							<c:if test="${member.coGubunCd eq '1'}">	
								<tr id="coGubunCd2_layer2" style='display:none'>
							</c:if>								
									<td>
										<input type="text" name="bubNo1" class="input_style1 resident" value="${member.bubNo1}" maxlength="6" title="법인 등록번호 앞자리" /><span>-</span
												><input type="text" name="bubNo2" class="input_style1 resident" value="${member.bubNo2}" maxlength="7" title="법인 등록번호 뒷자리" />
									</td>
								</tr>
							<%-- </c:if> --%>
							<tr>
								<th>
									주소 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr name="address">
								<td>
									<input type="text" id="trustZipcode" name="trustZipcode" class="input_style1 address1 disabled" 
											value="${member.trustZipcode}" readonly placeholder="" title="주소입력칸 1" /><button type="button" id="findTrustAddr" class="btn_style1">주소찾기</button>
									<input type="text" id="trustAddr" class="input_style1 margin disabled" 
											value="${member.trustSido} ${member.trustGugun} ${member.trustDong} ${member.trustBunji}" readonly placeholder="" title="주소입력칸 2" />
									<input type="text" id="trustDetailAddr" name="trustDetailAddr" class="input_style1" 
											value="${member.trustDetailAddr}" maxlength="100" placeholder="" title="주소입력칸 3" />
								</td>
							</tr>
							<tr>
								<th>
									전화번호(휴대전화번호) <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
<%-- 									<select id= "moditrustTel" name="trustTel1" class="select_style" title="전화번호 앞자리" >
										<option value="02"   <c:if test="${member.trustTel1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.trustTel1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.trustTel1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.trustTel1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.trustTel1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.trustTel1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.trustTel1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.trustTel1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.trustTel1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.trustTel1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.trustTel1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.trustTel1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.trustTel1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.trustTel1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.trustTel1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.trustTel1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.trustTel1 eq '0502'}">selected</c:if>>0502</option> 
										<option value="010" <c:if test="${member.trustTel1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${member.trustTel1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${member.trustTel1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${member.trustTel1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${member.trustTel1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${member.trustTel1 eq '019'}">selected</c:if>>019</option> 
										
									</select> --%>
									<input type="text" name="trustTel1" class="input_style1 phone" value="${member.trustTel1}" maxlength="3" placeholder="" title="전화번호 앞자리" numberOnly/>
									<span>-</span>
									<input type="text" name="trustTel2" class="input_style1 phone" value="${member.trustTel2}" maxlength="4" placeholder="" title="전화번호 중간자리" numberOnly/>
									<span>-</span>
									<input type="text" name="trustTel3" class="input_style1 phone" value="${member.trustTel3}" maxlength="4" placeholder="" title="전화번호 뒷자리" numberOnly/>
								</td>
							</tr>
							<tr>
								<th>
									팩스번호 
								</th>
							</tr>
							<tr>
								<td>
									<%-- <select id="moditrustFax" name="trustFax1" class="select_style" title="전화번호 앞자리" >
										<option value="02"   <c:if test="${member.trustFax1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.trustFax1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.trustFax1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.trustFax1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.trustFax1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.trustFax1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.trustFax1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.trustFax1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.trustFax1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.trustFax1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.trustFax1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.trustFax1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.trustFax1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.trustFax1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.trustFax1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.trustFax1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.trustFax1 eq '0502'}">selected</c:if>>0502</option> 
									</select> --%>
									<input type="text" name="trustFax1" class="input_style1 phone" value="${member.trustFax1}" maxlength="4" placeholder="" title="전화번호 앞자리" numberOnly />
									<span>-</span>
									<input type="text" name="trustFax2" class="input_style1 phone" value="${member.trustFax2}" maxlength="4" placeholder="" title="전화번호 중간자리"  numberOnly/>
									<span>-</span>
									<input type="text" name="trustFax3" class="input_style1 phone" value="${member.trustFax3}" maxlength="4" placeholder="" title="전화번호 뒷자리" numberOnly />
								</td>
							</tr>
							<tr>
								<th>
									홈페이지 
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="trustUrl" class="input_style1" value="${member.trustUrl}" maxlength="100" placeholder="" title="홈페이지 주소입력칸" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
				<div class="table_area">
					<table class="ceoinfo">
						<caption> 대표자정보</caption>
						<thead>
							<tr> 	 	 	
								<th>대표자정보</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>
									 대표자명 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="ceoName" class="input_style1" value="${member.ceoName}" maxlength="30" placeholder="" title="대표자명" />
								</td>
							</tr>
							<tr>
								<th>
									 주민등록번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="ceoRegNo1" class="input_style1 resident" value="${member.ceoRegNo1}" maxlength="6" placeholder="" title="주민등록번호 앞자리" numberOnly/><span>-</span><input 
											type="password" name="ceoRegNo2" class="input_style1 resident" value="${member.ceoRegNo2}"  maxlength="7" placeholder="" title="주민등록번호 뒷자리" numberOnly/>
								</td>
							</tr>
							<tr>
								<th>
									주소 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<input type="text" id="ceoZipcode" name="ceoZipcode" class="input_style1 address1 disabled" 
											value="${member.ceoZipcode}" readonly placeholder="" title="주소입력칸 1" /><button type="button" id="findCeoAddr" class="btn_style1">주소찾기</button>
									<input type="text" id="ceoAddr" class="input_style1 margin disabled" 
											value="${member.ceoSido} ${member.ceoGugun} ${member.ceoDong} ${member.ceoBunji}" readonly placeholder="" title="주소입력칸 2" />
									<input type="text" id="ceoDetailAddr" name="ceoDetailAddr" class="input_style1" 
											value="${member.ceoDetailAddr}" maxlength="100" placeholder="" title="주소입력칸 3" />
								</td>
							</tr>
							<tr>
								<th>
									전화번호(휴대전화번호) <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
									<%-- <select id ="modiceoTel" name="ceoTel1" class="select_style" title="전화번호 앞자리">
										<option value="02"   <c:if test="${member.ceoTel1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.ceoTel1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.ceoTel1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.ceoTel1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.ceoTel1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.ceoTel1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.ceoTel1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.ceoTel1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.ceoTel1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.ceoTel1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.ceoTel1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.ceoTel1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.ceoTel1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.ceoTel1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.ceoTel1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.ceoTel1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.ceoTel1 eq '0502'}">selected</c:if>>0502</option> 
  										<option value="010" <c:if test="${member.ceoTel1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${member.ceoTel1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${member.ceoTel1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${member.ceoTel1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${member.ceoTel1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${member.ceoTel1 eq '019'}">selected</c:if>>019</option> 
										
									</select> --%>
									<input type="text" name="ceoTel1" class="input_style1 phone" value="${member.ceoTel1}" maxlength="3" placeholder="" title="전화번호 앞자리" numberOnly/>
									<span>-</span>
									<input type="text" name="ceoTel2" class="input_style1 phone" value="${member.ceoTel2}" maxlength="4" placeholder="" title="전화번호 중간자리" numberOnly/>
									<span>-</span>
									<input type="text" name="ceoTel3" class="input_style1 phone" value="${member.ceoTel3}" maxlength="4" placeholder="" title="전화번호 뒷자리" numberOnly/>
								</td>
							</tr>
							<tr>
								<th>
									팩스번호 
								</th>
							</tr>
							<tr>
								<td>
									<%-- <select  id ="modiceoFax" name="ceoFax1" class="select_style" title="전화번호 앞자리" >
										<option value="02"   <c:if test="${member.ceoFax1 eq '02'  }">selected</c:if>>02</option> 
										<option value="031"  <c:if test="${member.ceoFax1 eq '031' }">selected</c:if>>031</option> 
										<option value="032"  <c:if test="${member.ceoFax1 eq '032' }">selected</c:if>>032</option> 
										<option value="033"  <c:if test="${member.ceoFax1 eq '033' }">selected</c:if>>033</option> 
										<option value="041"  <c:if test="${member.ceoFax1 eq '041' }">selected</c:if>>041</option> 
										<option value="042"  <c:if test="${member.ceoFax1 eq '042' }">selected</c:if>>042</option> 
										<option value="043"  <c:if test="${member.ceoFax1 eq '043' }">selected</c:if>>043</option> 
										<option value="051"  <c:if test="${member.ceoFax1 eq '051' }">selected</c:if>>051</option> 
										<option value="052"  <c:if test="${member.ceoFax1 eq '052' }">selected</c:if>>052</option> 
										<option value="053"  <c:if test="${member.ceoFax1 eq '053' }">selected</c:if>>053</option> 
										<option value="054"  <c:if test="${member.ceoFax1 eq '054' }">selected</c:if>>054</option> 
										<option value="055"  <c:if test="${member.ceoFax1 eq '055' }">selected</c:if>>055</option> 
										<option value="061"  <c:if test="${member.ceoFax1 eq '061' }">selected</c:if>>061</option> 
										<option value="062"  <c:if test="${member.ceoFax1 eq '062' }">selected</c:if>>062</option> 
										<option value="063"  <c:if test="${member.ceoFax1 eq '063' }">selected</c:if>>063</option> 
										<option value="064"  <c:if test="${member.ceoFax1 eq '064' }">selected</c:if>>064</option> 
										<option value="0502" <c:if test="${member.ceoFax1 eq '0502'}">selected</c:if>>0502</option> 
										
									</select> --%>
									<input type="text" name="ceoFax1" class="input_style1 phone" value="${member.ceoFax1}" maxlength="3" placeholder="" title="전화번호 앞자리" numberOnly/>
									<span>-</span>
									<input type="text" name="ceoFax2" class="input_style1 phone" value="${member.ceoFax2}" maxlength="4" placeholder="" title="전화번호 중간자리" numberOnly/>
									<span>-</span>
									<input type="text" name="ceoFax3" class="input_style1 phone" value="${member.ceoFax3}" maxlength="4" placeholder="" title="전화번호 뒷자리" numberOnly/>
								</td>
							</tr>	
							<tr>
								<th>
									휴대전화번호 <strong class="txt_point">*</strong>
								</th>
							</tr>
							<tr>
								<td>
								<%-- 	<select name="ceoMobile1" class="select_style" title="전화번호 앞자리">
										<option value="010" <c:if test="${member.ceoMobile1 eq '010'}">selected</c:if>>010</option> 
										<option value="011" <c:if test="${member.ceoMobile1 eq '011'}">selected</c:if>>011</option> 
										<option value="016" <c:if test="${member.ceoMobile1 eq '016'}">selected</c:if>>016</option> 
										<option value="017" <c:if test="${member.ceoMobile1 eq '017'}">selected</c:if>>017</option> 
										<option value="018" <c:if test="${member.ceoMobile1 eq '018'}">selected</c:if>>018</option> 
										<option value="019" <c:if test="${member.ceoMobile1 eq '019'}">selected</c:if>>019</option> 
									</select> --%>
									<input type="text" name="ceoMobile1" class="input_style1 phone" value="${member.ceoMobile1}" maxlength="3" placeholder="" title="전화번호 앞자리" numberOnly/>
									<span>-</span>
									<input type="text" name="ceoMobile2" class="input_style1 phone" value="${member.ceoMobile2}" maxlength="4" placeholder="" title="전화번호 중간자리" numberOnly />
									<span>-</span>
									<input type="text" name="ceoMobile3" class="input_style1 phone" value="${member.ceoMobile3}" maxlength="4" placeholder="" title="전화번호 뒷자리" numberOnly />
								</td>
							</tr>
							<tr>
								<th>
									이메일

								</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="ceoEmail1" class="input_style1 resident" 
											value="${member.ceoEmail1}" maxlength="70" placeholder="" title="이메일 주소 앞자리" /><span>@</span><input type="text" name="ceoEmail2" class="input_style1 resident" 
											value="${member.ceoEmail2}" maxlength="29" placeholder="" title="이메일 주소 뒷자리" />
								</td>
							</tr>
							<tr>
								<th>
									 SMS수신동의
								</th>
							</tr>
							<tr>
								<td>
									<c:if test="${member.smsAgree eq 'Y'}"><c:set var="smsAgreeYChecked" value="checked" /></c:if>
									<c:if test="${member.smsAgree ne 'Y'}"><c:set var="smsAgreeNChecked" value="checked" /></c:if>
									<div class="btm_form_box">
									
										<input type="radio" id="smsAgreeY" name="smsAgree" value="Y" ${smsAgreeYChecked} />
										<label for="smsAgreeY">동의</label> 
									</div>
									<div class="btm_form_box">
										<input type="radio" id="smsAgreeN" name="smsAgree" value="N" ${smsAgreeNChecked} />
										<label for="smsAgreeN">동의안함</label> 
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_area -->
			</div>
			<!-- //aside -->
			<div class="cont">
				<div class="table_area">
					<table>
						<caption>업무선택</caption>
						<thead>
							<tr> 	 	 	
								<th>업무선택</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${type eq 'sub_modify' or type eq 'trust_modify'}">
								<tr>
									<th>
										  <strong class="txt_point">*</strong> 변경사항 ( 변경사항을 선택하시면 변경하실 항목이 활성화 됩니다.)
									</th>
								</tr>
								<tr>
									<td>
										<div class="btm_form_box">
											<input type="checkbox" id="chgcd1" name="chgCd" value="1" ${chgcd1} />
											<label for="chgcd1">대표자정보</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="chgcd2" name="chgCd" value="2" ${chgcd2}/>
											<label for="chgcd2">업체명</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="chgcd3" name="chgCd" value="3" ${chgcd3} />
											<label for="chgcd3">주소/연락처</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="chgcd6" name="chgCd" value="6" ${chgcd6}/>
											<label for="chgcd6">취급저작물/취급저작권종류</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="chgcd7" name="chgCd" value="7" ${chgcd7}/>
											<label for="chgcd7">등기임원</label> 
										</div>
									</td>
								</tr>
							</c:if>
								<tr>
									<th>
										<strong class="txt_point" >*</strong> 변경사유 
									</th>
								</tr>
								<tr>
									<td>
										<input type="text" id="chgMemo" placeholder="변경 사유와 내용을 입력해주세요." name="chgMemo" value="${member.chgMemo}"class="input_style1 height"/>
									</td>
								</tr>																
								
												
							<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
								<tr>
									<th>
										 <strong class="txt_point">*</strong> 취급 하고자 하는 업무의 내용 
									</th>
								</tr>
								<tr>
									<td >
										<c:if test="${member.conDetailCd eq '1'}">
											<c:set var="conDetailCd1" value="checked" />
											<c:set var="conDetailCd2" value="" />
										</c:if>
										<c:if test="${member.conDetailCd eq '2'}">
											<c:set var="conDetailCd1" value="" />
											<c:set var="conDetailCd2" value="checked" />
										</c:if>
										<c:if test="${member.conDetailCd eq '3'}">
											<c:set var="conDetailCd1" value="checked" />
											<c:set var="conDetailCd2" value="checked" />
										</c:if>
										<div class="btm_form_box">
											<input type="checkbox"  id="conDetailCd1" value="1" ${conDetailCd1} onclick="return false;" onkeydown="return false;"/>
											<label for="conDetailCd1">대리</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="conDetailCd2" value="2" ${conDetailCd2} onclick="return false;" onkeydown="return false;" />
											<label for="conDetailCd2">중개</label> 
										</div>
									</td>
								</tr>
							</c:if>
							<tr>
								<th>
									 <strong class="txt_point">*</strong> 취급 저작물의 종류 및 취급 권리의 종류 (저작권)  
								</th>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind1" name="writingKind" value="1" ${writingKind1} readonly />
										<label for="">어문 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_1" name="permkind1" value="1" ${permkind1_1} />
										<label for="permkind1_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_2" name="permkind1" value="2" ${permkind1_2} />
										<label for="permkind1_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_3" name="permkind1" value="3" ${permkind1_3} />
										<label for="permkind1_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_4" name="permkind1" value="4" ${permkind1_4} />
										<label for="permkind1_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind1_8" name="permkind1" value="8" ${permkind1_8} />
										<label for="permkind1_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind1_0" name="permkind1" value="0" ${permkind1_0} />
										<label for="permkind1_0">기타</label> 
									</div>
									<input type="text" name="permEtc1" class="input_style1 others" value="${permEtc1}" placeholder="" title="기타" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind2" name="writingKind" value="2" ${writingKind2} readonly />
										<label for="">음악 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_1" name="permkind2" value="1" ${permkind2_1} />
										<label for="permkind2_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_2" name="permkind2" value="2" ${permkind2_2} />
										<label for="permkind2_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_3" name="permkind2" value="3" ${permkind2_3} />
										<label for="permkind2_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_4" name="permkind2" value="4" ${permkind2_4} />
										<label for="permkind2_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind2_8" name="permkind2" value="8" ${permkind2_8} />
										<label for="permkind2_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind2_0" name="permkind2" value="0" ${permkind2_0} />
										<label for="permkind2_0">기타</label> 
									</div>
									<input type="text" name="permEtc2" class="input_style1 others" value="${permEtc2}" placeholder="" title="기타" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind3" name="writingKind" value="3" ${writingKind3} readonly />
										<label for="">영상 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_1" name="permkind3" value="1" ${permkind3_1} />
										<label for="permkind3_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_2" name="permkind3" value="2" ${permkind3_2} />
										<label for="permkind3_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_3" name="permkind3" value="3" ${permkind3_3} />
										<label for="permkind3_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_4" name="permkind3" value="4" ${permkind3_4} />
										<label for="permkind3_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind3_8" name="permkind3" value="8" ${permkind3_8} />
										<label for="permkind3_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind3_0" name="permkind3" value="0" ${permkind3_0} />
										<label for="permkind3_0">기타</label> 
									</div>
									<input type="text" name="permEtc3" class="input_style1 others" value="${permEtc3}" placeholder="" title="기타" /> 
								</td>
							</tr>
							<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
								<tr class="list_copy">
									<td>
										<div class="btm_form_box first">
											<input type="checkbox" id="writingKind21" name="writingKind" value="21" ${writingKind21} readonly />
											<label for="">연극 저작물</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind21_1" name="permkind21" value="1" ${permkind21_1} />
											<label for="permkind21_1">복제권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind21_2" name="permkind21" value="2" ${permkind21_2} />
											<label for="permkind21_2">배포권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind21_3" name="permkind21" value="3" ${permkind21_3} />
											<label for="permkind21_3">전송권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind21_4" name="permkind21" value="4" ${permkind21_4} />
											<label for="permkind21_4">공연권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind21_8" name="permkind21" value="8" ${permkind21_8} />
											<label for="permkind21_8">2차적저작물작성권</label> 
										</div>
										<div class="btm_form_box last">
											<input type="checkbox" id="permkind21_0" name="permkind21" value="0" ${permkind21_0} />
											<label for="permkind21_0">기타</label> 
										</div>
										<input type="text" name="permEtc21" class="input_style1 others" value="${permEtc21}" placeholder="" title="기타" /> 
									</td>
								</tr>
							</c:if>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind4" name="writingKind" value="4" ${writingKind4} readonly />
										<label for="">미술 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_1" name="permkind4" value="1" ${permkind4_1} />
										<label for="permkind4_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_2" name="permkind4" value="2" ${permkind4_2} />
										<label for="permkind4_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_3" name="permkind4" value="3" ${permkind4_3} />
										<label for="permkind4_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_4" name="permkind4" value="4" ${permkind4_4} />
										<label for="permkind4_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind4_8" name="permkind4" value="8" ${permkind4_8} />
										<label for="permkind4_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind4_0" name="permkind4" value="0" ${permkind4_0} />
										<label for="permkind4_0">기타</label> 
									</div>
									<input type="text" name="permEtc4" class="input_style1 others" value="${permEtc4}" placeholder="" title="기타" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind5" name="writingKind" value="5" ${writingKind5} readonly />
										<label for="">사진 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_1" name="permkind5" value="1" ${permkind5_1} />
										<label for="permkind5_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_2" name="permkind5" value="2" ${permkind5_2} />
										<label for="permkind5_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_3" name="permkind5" value="3" ${permkind5_3} />
										<label for="permkind5_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_4" name="permkind5" value="4" ${permkind5_4} />
										<label for="permkind5_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind5_8" name="permkind5" value="8" ${permkind5_8} />
										<label for="permkind5_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind5_0" name="permkind5" value="0" ${permkind5_0} />
										<label for="permkind5_0">기타</label> 
									</div>
									<input type="text" name="permEtc5" class="input_style1 others" value="${permEtc5}" placeholder="" title="기타" /> 
								</td>
							</tr>
							<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
								<tr class="list_copy">
									<td>
										<div class="btm_form_box first">
											<input type="checkbox" id="writingKind22" name="writingKind" value="22" ${writingKind22} readonly />
											<label for="">건축 저작물</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind22_1" name="permkind22" value="1" ${permkind22_1} />
											<label for="permkind22_1">복제권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind22_2" name="permkind22" value="2" ${permkind22_2} />
											<label for="permkind22_2">배포권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind22_3" name="permkind22" value="3" ${permkind22_3} />
											<label for="permkind22_3">전송권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind22_4" name="permkind22" value="4" ${permkind22_4} />
											<label for="permkind22_4">공연권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind22_8" name="permkind22" value="8" ${permkind22_8} />
											<label for="permkind22_8">2차적저작물작성권</label> 
										</div>
										<div class="btm_form_box last">
											<input type="checkbox" id="permkind22_0" name="permkind22" value="0" ${permkind22_0} />
											<label for="permkind22_0">기타</label> 
										</div>
										<input type="text" name="permEtc22" class="input_style1 others" value="${permEtc22}" placeholder="" title="기타" /> 
									</td>
								</tr>
								<tr class="list_copy">
									<td>
										<div class="btm_form_box first">
											<input type="checkbox" id="writingKind23" name="writingKind" value="23" ${writingKind23} readonly />
											<label for="">도형 저작물</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind23_1" name="permkind23" value="1" ${permkind23_1} />
											<label for="permkind23_1">복제권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind23_2" name="permkind23" value="2" ${permkind23_2} />
											<label for="permkind23_2">배포권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind23_3" name="permkind23" value="3" ${permkind23_3} />
											<label for="permkind23_3">전송권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind23_8" name="permkind23" value="8" ${permkind23_8} />
											<label for="permkind23_8">2차적저작물작성권</label> 
										</div>
										<div class="btm_form_box last">
											<input type="checkbox" id="permkind23_0" name="permkind23" value="0" ${permkind23_0} />
											<label for="permkind23_0">기타</label> 
										</div>
										<input type="text" name="permEtc23" class="input_style1 others" value="${permEtc23}" placeholder="" title="기타" /> 
									</td>
								</tr>
								<tr class="list_copy">
									<td>
										<div class="btm_form_box first" >
											<input type="checkbox" id="writingKind24" name="writingKind" value="24" ${writingKind24} readonly />
											<label for="">컴퓨터프로그램 저작물</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind24_1" name="permkind24" value="1" ${permkind24_1} />
											<label for="permkind24_1">복제권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind24_2" name="permkind24" value="2" ${permkind24_2} />
											<label for="permkind24_2">배포권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind24_3" name="permkind24" value="3" ${permkind24_3} />
											<label for="permkind24_3">전송권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind24_6" name="permkind24" value="6" ${permkind24_6} />
											<label for="permkind24_6">대여권</label> 
										</div>
										<div class="btm_form_box">
											<input type="checkbox" id="permkind24_8" name="permkind24" value="8" ${permkind24_8} />
											<label for="permkind24_8">2차적저작물작성권</label> 
										</div>
										<div class="btm_form_box last">
											<input type="checkbox" id="permkind24_0" name="permkind24" value="0" ${permkind24_0} />
											<label for="permkind24_0">기타</label> 
										</div>
										<input type="text" name="permEtc24" class="input_style1 others" value="${permEtc24}" placeholder="" title="기타" /> 
									</td>
								</tr>
							</c:if>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind0" name="writingKind" value="0" ${writingKind0} readonly />
										<label for="">기타 저작물</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_1" name="permkind0" value="1" ${permkind0_1} />
										<label for="permkind0_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_2" name="permkind0" value="2" ${permkind0_2} />
										<label for="permkind0_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_3" name="permkind0" value="3" ${permkind0_3} />
										<label for="permkind0_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_4" name="permkind0" value="4" ${permkind0_4} />
										<label for="permkind0_4">공연권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_5" name="permkind0" value="5" ${permkind0_5} />
										<label for="permkind0_5">전시권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind0_6" name="permkind0" value="6" ${permkind0_6} />
										<label for="permkind0_6">대여권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind0_7" name="permkind0" value="7" ${permkind0_7} />
										<label for="permkind0_7">방송권</label> 
									</div>
									<div class="btm_form_box" style="margin-left: 180px;">
										<input type="checkbox" id="permkind0_8" name="permkind0" value="8" ${permkind0_8} />
										<label for="permkind0_8">2차적저작물작성권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind0_0" name="permkind0" value="0" ${permkind0_0} />
										<label for="permkind0_0">기타</label> 
									</div>
									<input type="text" name="permEtc0" class="input_style1 others" value="${permEtc0}" placeholder="" title="기타" /> 
								</td>
							</tr>
							<tr>
								<th class="pt">
									<strong class="txt_point">*</strong> 취급 저작물의 종류 및 취급 권리의 종류 (저작인접권) 
								</th>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind10" name="writingKind" value="10" ${writingKind10} readonly />
										<label for="">실연자의 권리</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind10_1" name="permkind10" value="1" ${permkind10_1} />
										<label for="permkind10_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind10_2" name="permkind10" value="2" ${permkind10_2} />
										<label for="permkind10_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind10_3" name="permkind10" value="3" ${permkind10_3} />
										<label for="permkind10_3">전송권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind10_4" name="permkind10" value="4" ${permkind10_4} />
										<label for="permkind10_4">공연권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind10_0" name="permkind10" value="0" ${permkind10_0} />
										<label for="permkind10_0">기타</label> 
									</div>
									<input type="text" name="permEtc10" class="input_style1 others" value="${permEtc10}" placeholder="" title="기타" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind11" name="writingKind" value="11" ${writingKind11} readonly />
										<label for="">음반제작자의 권리</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind11_1" name="permkind11" value="1" ${permkind11_1} />
										<label for="permkind11_1">복제권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind11_2" name="permkind11" value="2" ${permkind11_2} />
										<label for="permkind11_2">배포권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind11_3" name="permkind11" value="3" ${permkind11_3} />
										<label for="permkind11_3">전송권</label> 
									</div>
									<div class="btm_form_box last">
										<input type="checkbox" id="permkind11_0" name="permkind11" value="0" ${permkind11_0} />
										<label for="permkind11_0">기타</label> 
									</div>
									<input type="text" name="permEtc11" class="input_style1 others" value="${permEtc11}" placeholder="" title="기타" /> 
								</td>
							</tr>
							<tr class="list_copy">
								<td>
									<div class="btm_form_box first">
										<input type="checkbox" id="writingKind12" name="writingKind" value="12" ${writingKind12} />
										<label for="writingKind12">출판권</label> 
									</div>
									<div class="btm_form_box">
										<input type="checkbox" id="permkind12_9" name="permkind12" value="9" ${permkind12_9} />
										<label for="permkind12_9">출판권</label> 
									</div>
								</td>
							</tr>
						</tbody>
					  </table>
					</form>
				</div>
				<!-- //table_area -->
				<div class="cell">
					<tr>
					
						<p class="tit">기존의 신고시 제출한 파일 보기</p>
							<td colspan="4" class="txt_l">
							<div class="file_change">
								<c:if test="${!empty rpMgm.file1Path}">
									 <a href="<c:url value="/download?filename=${rpMgm.file1Path}" />" ><p style="text-decoration: underline;color:#5959cc;"  class="txt">저작권대리중개업 업무규정</p></a>
								</c:if>												
								<c:if test="${!empty rpMgm.file2Path}">
									<a href="<c:url value="/download?filename=${rpMgm.file2Path}" />"> <p style="text-decoration: underline;color:#5959cc;"  class="txt">신고인(단체 또는 법인인 경우에는 그 대표자및 임원)의 이력서</p></a>
								</c:if>												
								<c:if test="${!empty rpMgm.file3Path}">
									<a href="<c:url value="/download?filename=${rpMgm.file3Path}" />"> <p style="text-decoration: underline;color:#5959cc;"  class="txt">정관 또는 규약 1부</p></a>
								</c:if>												
								<c:if test="${!empty rpMgm.file4Path}">
									<a href="<c:url value="/download?filename=${rpMgm.file4Path}" />"> <p style="text-decoration: underline;color:#5959cc;"  class="txt">재무제표(법인단체)</p></a>
								</c:if>												
<%-- 								
	<c:forEach var="item" items="${chgHistoryFileList}" varStatus="status">
										<a href="<c:url value="/download?filename=${item.filepath}" />">
												<p style="text-decoration: underline;color:#5959cc;"  class="txt">변경신고신청 첨부<c:out value="${status.count}" />: <c:out value="${item.filename}" /> (<c:out value="${item.regDateStr}" />)</p>
												<p style="text-decoration: underline;color:#5959cc;"  class="txt"><c:out value="${item.filename}" /></p>
												<p style="text-decoration: underline;color:#5959cc;"  class="txt">(<c:out value="${item.regDateStr}" />)</p>
														</a>
													</c:forEach> --%>
							</div>
						</td>
					</tr>
				</div>
			<%-- <c:if test="${!empty chgHistoryFileList}"> --%>
				<div class="cell">
					<tr>
					
						<p class="tit">이전 변경신고시 제출한 파일 보기</p>
							<td colspan="4" class="txt_l">
							<div class="file_change">
							<c:forEach var="item" items="${chgHistoryFileList}" varStatus="status">
										<%-- <a href="<c:url value="/download?filename=${item.filepath}&origfilename=${item.filename}" />"> --%>
										<a href="<c:url value="/download?filename=${item.filepath}" />">
												<p style="text-decoration: underline;color:#5959cc;"  class="txt">변경신고신청 첨부<c:out value="${status.count}" />: <c:out value="${item.filename}" /> (<c:out value="${item.regDateStr}" />)</p>
<%-- 												<p style="text-decoration: underline;color:#5959cc;"  class="txt"><c:out value="${item.filename}" /></p>
												<p style="text-decoration: underline;color:#5959cc;"  class="txt">(<c:out value="${item.regDateStr}" />)</p> --%>
														</a>
													</c:forEach>
							</div>
						</td>
					</tr>
				</div>
				<%-- </c:if> --%>
				<c:if test="${type eq 'sub_regist' or type eq 'sub_modify'}">
				   <c:if test="${type eq 'sub_regist' }">
					<div class="cell">
						<p class="tit">구비서류 안내 </p>
						<p class="txt">저작권법 제105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다. </p>
						<p class="txt">저작권대리중개업신고서 1부 <span>[현재 작성한 온라인 신청서]</span></p>
						<p class="txt">저작권대리중개업 업무규정 1부 <span>(저작권대리중개 계약약관, 저작물 이용계약 약관 각1부 포함)</span> <a href="<c:url value='/download2?filename=3.zip'/>" class="btn_style1 margin" target="_blank">샘플 다운로드</a></p>
						<p class="txt">정관 또는 규약<span>(법인 또는 단체인 경우에 합니다.)</span> 1부 <span>[스캔한 파일 온라인 첨부가능]</span></p>
						<p class="txt">재무제표<span>(법인인 경우에 한정합니다.)</span> 1부<span>[스캔한 파일 온라인 첨부가능]</span></p>
						<p class="txt"><span>(재무제표가 없는경우 재무제표미제출사유서를 첨부해주세요.)</span> <a href="<c:url value='//download2?filename=7.zip' />" class="btn_style1 margin" target="_blank">양식 다운로드</a></p><br>
						<p class="txt">신고인<span>(단체 또는 법인인 경우에는 그 대표자 및 임원)</span>의 이력서 1부 <a href="<c:url value='/download2?filename=6.zip' />" class="btn_style1 margin" target="_blank">양식 다운로드</a></p>
					</div>
					</c:if>
					 <c:if test="${type eq 'sub_modify' }">
					 	<div class="cell">
						<p class="tit">구비서류 안내 </p>
						<p class="txt">저작권법 제105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다. </p>
						<p class="txt">저작권대리중개업신고서 1부 <span>[현재 작성한 온라인 신청서]</span></p>
						<p class="txt">신고증 반납<span>[등기우편으로 반납]</span>
						<p class="txt">변경사항을 증명하는 서류 1부</p>
						<p class="txt">대표자 변경시 : 이력서, 이사회 회의록, 사업자등록증 사본</span><a href="<c:url value='/download2?filename=6.zip'/>" class="btn_style1 margin" target="_blank">양식 다운로드</a></p>
						<p class="txt">등기임원변경시 : 등기임원 이력서</p>
						<p class="txt">주소 변경시 : 사업자등록증 사본</p>
						<p class="txt">상호 변경시 : 사업자등록증 사본, 저작권신탁관리업 업무규정 및 계약 약관<a href="<c:url value='/download2?filename=3.zip'/>" class="btn_style1 margin" target="_blank">샘플 다운로드</a></p>
						<p class="txt"> * 상호 변경시 법인일 경우에는 이사회 회의록 추가 제출</p>
						
						</div>
					 </c:if>
					<!-- //cell -->
               	<!-- //cell -->
								
				</c:if>
				<div class="cell">
					<p class="tit">첨부파일 ( 임시저장된 데이터를 불러온 경우 파일을 재업로드해주세요. )</p>
						<div style="text-align: center;">
  							 <input type="button" id="addfiles" class="form_btn insert_file btn_style1" value="파일추가"/>
						</div><br>
						<div id="add">
						  <div class="list_file">
						  <form id="addForm1" >
						    <p class="txt">첨부파일 <strong class="txt_point">*</strong></p>
							<input type="text" id ="realfilename1" class="input_style1 prj_file" placeholder="PDF, JPG 파일만 첨부" title="파일첨부" readonly />
							<input type="file" name="file1" class="insert_file_target"  title="파일첨부" data-ref-name="file1Path"/>
							<button type="button" class="form_btn insert_file1 btn_style1" onclick="fileUploadActive_ww('1')">파일찾기</button>
							<button type="button" class="btn_style1" onclick="removefile('1')">파일삭제</button>
        				  </form>
    					  <input type="hidden" name="fileName2"/>
						  <input type="hidden" name="fileSize2"/>
						  <input type="hidden" name="filePath2"/>
						  </div>
						 </div>
         				
					</div>
					<div class="txt_b">105조제1항 및 동법 시행령 제48조제1항, 동법 시행규칙 제19조제1항에 따라 위와 같이 신고합니다. </div>
				<div class="btn_area">
					<!--  
					<a href="declaration_preview_n.html" class="btn btn_style2">저장</a>
					<a href="" class="btn btn_style1">취소</a>
					-->
					<button onclick="javascript:$('#form').submit();" class="btn btn_style2">저장</button>
				</div>
			</div>
			<!-- //cont -->
		
		</div>
		<!-- //form_apply -->
	</div>
	<!-- //container -->

<jsp:include page="/WEB-INF/jsp/cocoms/comm/footer.jsp" flush="true">
	<jsp:param name="title" value=""/>
	<jsp:param name="comm_js" value="appl" />
	<jsp:param name="js" value="appl" />
</jsp:include>

<script>
/* function test(){
  
	 var param = $("#form").serialize();

	 $.ajax({   
	   type: "POST"  
	  ,url: "/cocoms/appl/test"
	  ,data: param
	  ,success:function(data){
	    alert(data);
	    location.href="/cocoms/appl/sub2";
	  }
	  ,error:function(data){
	    alert("error");
	  }
	  });
	}  */
window.onload = function(){
		
	$('.select_style').prop('disabled',true);
	$('.membinfo input[type=text], .membinfo input[type=checkbox], .membinfo button[type=button]').prop('disabled',true);
	$('.ceoinfo input[type=text],  .ceoinfo input[type=checkbox], .ceoinfo input[type=password], .ceoinfo button[type=button]').prop('disabled',true);
	$('.list_copy input[type=text],  .list_copy input[type=checkbox], .list_copy input[type=password]').prop('disabled',true);
	
	  $('input:checkbox[name=chgCd]:checked').each(function () {
		     
          if($(this).val()=='1'){
 			   $('.ceoinfo input[type=text],  .ceoinfo input[type=checkbox], .ceoinfo input[type=password]').prop('disabled',false);
 			   $('#modiceoTel').prop('disabled',false);
 			   $('#modiceoFax').prop('disabled',false);
 			   $('#findCeoAddr').prop('disabled',false);
 		   }
 		   if($(this).val()=='2' ){
 				$('input:text[name="coName"]').prop('disabled',false);   
 		   }
 		   if($(this).val()=='3' ){
 				$('input:text[name="trustDetailAddr"], .membinfo button[type=button]' ).prop('disabled',false);   
 				$('input:text[name="trustZipcode"], .membinfo button[type=button]' ).prop('disabled',false);   
 				$('input:text[name="trustAddr"], .membinfo button[type=button]' ).prop('disabled',false);   
 		  	    $('#moditrustTel').prop('disabled',false);
 		  	    $('#moditrustFax').prop('disabled',false);
 		  	    $('input:text[name="trustTel2"]' ).prop('disabled',false);   
 		  	    $('input:text[name="trustTel1"]' ).prop('disabled',false);   
 		  	    $('input:text[name="trustTel3"]' ).prop('disabled',false);   
 		  	    $('input:text[name="trustFax2"]' ).prop('disabled',false);   
 		  	    $('input:text[name="trustFax3"]' ).prop('disabled',false);   
 		  	    $('input:text[name="trustFax1"]' ).prop('disabled',false);   
 		   }
 		   if($(this).val()=='6'){
 			   $('.list_copy input[type=text],  .list_copy input[type=checkbox], .list_copy input[type=password]').prop('disabled',false);
 		   }
 		   
 		})
}
/* window.onbeforeunload = function () {
	if ($('#alreadyConfirm').val() == 'N') {
		return "저장하지 않은 변경사항은 유지되지 않습니다.";
	}
} */
</script>